
insert into yw_dev_info(
table_id,
bsnum,
unit,
phone,
code,
address,
detail_addr,
project_name,
contract_code,
contract_time,
agreement_code,
contract_sign_time,
plan_permit_code,
government_plan,
issue_time,
agreement,
parcel_no,
build_area,
land_area
)
SELECT
	wbd.id AS table_id,
	wbd.bsnum AS bsnum,
	wbd.NAME AS unit,
	wbd.tel_number AS phone,
	wbd.cert_no AS CODE,
	wbd.address AS address,
	wbd.address AS detail_addr,
	wbe.project_name AS project_name,
	wbe.contract_num AS contract_code,
wbe.contract_sign_date AS contract_time,
wbe.agreement_no AS agreement_code,
	wbe.agreement_sign_date AS contract_sign_time,
	wbe.earth_use_license AS plan_permit_code,
	wbe.gov_agree_no AS government_plan,
	wbe.issued_date AS issue_time,
	wbe.regulation_agreement AS agreement,
	wbe.parcels_no AS parcel_no,
	wbe.gross_floor_area AS build_area,
	wbe.earth_area AS land_area
FROM
	wy_business_developer wbd
	LEFT JOIN wy_business_earth wbe ON wbd.bsnum = wbe.bsnum;