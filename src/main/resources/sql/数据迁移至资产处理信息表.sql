
insert into wy_asset_disposal (
approval_time
,authority
,create_time
,disposal_time
,disposal_unit
,document_code
,table_id
,remark
,update_time
,rel_id
)

SELECT
	wacd.approval_time AS approval_time,
	wacd.approval_unit AS authority,
	wacd.create_date AS create_time,
	wacd.dispose_time AS disposal_time,
	wacd.dispose_unit AS disposal_unit,
	wacd.approval_doc_no AS document_code,
	wacd.id AS table_id,
	wacd.remarks AS remark,
	wacd.update_date AS update_time,
	wacd.asset_id AS rel_id
FROM
	wy_asset_cycle_dispose wacd;