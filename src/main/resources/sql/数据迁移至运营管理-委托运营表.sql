insert into yy_commissione_operation (
                                      CREDATE
                                     ,creditcode
                                     ,endtime
                                     ,id
                                     ,operationunit
                                     ,remark
                                     ,starttime
                                     ,MODIFYDATE
                                     ,WYBASICINFOIDS
                                     ,WYBASICINFONAMES
)
SELECT
    wce.create_date AS CREDATE,
    wce.cert_no AS creditcode,
    wce.service_date_start AS endtime,
    wce.id,
    wce.use_unit AS operationunit,
    wce.remarks AS remark,
    wce.service_date_end AS starttime,
    wce.update_date AS MODIFYDATE,
    wrt.id as wybasicinfoids,
    wrt.aname as WYBASICINFONAMES
FROM
    wy_contract_entrust wce
        left join wy_contract_bind wcb on wcb.contract_id = wce.id
        left join wy_relation_tree wrt on wrt.rel_id = wcb.rel_id
