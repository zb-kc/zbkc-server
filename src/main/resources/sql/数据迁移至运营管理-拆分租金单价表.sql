INSERT INTO yy_split_rent ( create_time, table_id, is_del, price, rent, old_yy_contract_id ) SELECT
wcr.create_date AS create_time,
wcr.id AS table_id,
wcr.del_flag AS is_del,
wcr.unit_price AS price,
wcr.price AS rent,
wcr.contract_id AS old_yy_contract_id
FROM
	wy_contract_rent wcr