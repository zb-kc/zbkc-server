insert into `yw_handover_details`(
`table_id`,
`bsnum`,
`name`,
`status`,
`guidelines`,
`handover_details_img_id`,
`supplement_data_opinion`
)
SELECT
	id as table_id,
	bsnum,
	`name` AS name,
	audit_status AS status,
	audit_guide AS guidelines,
	file_id AS handover_details_img_id,
	audit_opinion AS supplement_data_opinion 
FROM
	wy_business_materials
