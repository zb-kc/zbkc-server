SELECT
    we.create_date AS CREATOR,
    we.entry_price AS ENTRYMONEY,
    we.entry_time AS entrytime,
    we.id,
    we.update_date AS MODIFYDATE,
    we.del_flag AS STATUS,
    we.entry_type AS entrytype,
    wrt.id AS WYBASICINFOIDS,
    wrt.aname AS WYBASICINFONAMES
FROM
    wy_entry we
        LEFT JOIN wy_entry_bind web ON we.id = web.entry_id
        JOIN wy_relation_tree wrt ON wrt.rel_id = web.rel_id