INSERT INTO `yw_agency_business`
(
`table_id`,
`pro_name`,
`accept_code`,
`applicant_unit`,
`process`,
`application_time`,
`stage_name`,
`sign_source`,
`remark`,
`contact_name`,
`contact_phone`,
`social_uniform_credit_code`,
`business_license_address`,
`apply_people_type`,
`business_type`,
`lease_area`
) 
SELECT
	wbi.id AS table_id,
	wbi.NAME AS pro_name,
	wbi.serial_no AS accept_code,
	wbi.apply_unit AS applicant_unit,
	wbi.STATUS AS process,
	wbi.create_date AS application_time,
	wbi.stage_name AS stage_name,
	wbi.type AS sign_source,
	wbr.remarks AS remark,
	wbu.legal_person AS contact_name,
	wbu.legal_phone AS contact_phone,
	wbu.cert_type AS social_uniform_credit_code,
	wbu.address AS business_license_address,
	wbct.applicant_type AS apply_people_type,
	wbct.business_type AS business_type,
	a.wy_name AS lease_area 
FROM
	wy_business_info wbi
	LEFT JOIN wy_business_remark wbr ON wbr.bsnum = wbi.id
	LEFT JOIN wy_business_unit wbu ON wbu.bsnum = wbi.id
	LEFT JOIN wy_business_contract_type wbct ON wbct.bsnum = wbi.id
	LEFT JOIN (
	SELECT
		wbi.id,
		GROUP_CONCAT( wbcra.wy_name ) AS wy_name 
	FROM
		wy_business_contract_rel_assets AS wbcra
		LEFT JOIN wy_business_info wbi ON wbi.id = wbcra.bsnum 
	WHERE
		wbi.id IS NOT NULL 
	GROUP BY
	wbi.id 
	) a ON a.id = wbi.id;

#更新状态
update `yw_agency_business` set process = "受理中" where process = 0;
update `yw_agency_business` set process = "已办结" where process <> "受理中" and  process = 1;

UPDATE yw_agency_business
SET agency_type = 3
WHERE
	agency_type IS NULL
	AND sign_source <> "renew"
	AND sign_source <> "rentpay"
	AND sign_source <> "free"
	AND sign_source <> "wysb"
	AND sign_source = 4;