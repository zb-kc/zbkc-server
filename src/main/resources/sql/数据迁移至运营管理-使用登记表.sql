INSERT INTO yy_use_reg (
    actualuse,
    contactname,
    contactphone,
    CREDATE,
    creditcode,
    starttime,
    endtime,
    id,
    method,
    objname,
    objtype,
    rentfree,
    STATUS,
    MODIFYDATE,
    WYBASICINFOIDS,
    WYBASICINFONAMES,
    yycontractid,
    buildarea,
    usestatus,
    CREATOR
)
SELECT
    wur.practical_use AS actualuse,
    wur.contact AS contactname,
    wur.contact_tel AS contactphone,
    wur.create_date AS CREDATE,
    wur.credit_id AS creditcode,
    wur.approve_use_start_date AS starttime,
    wur.approve_use_end_date AS endtime,
    wur.id ,
    wur.use_type AS method,
    wur.use_unit AS objname,
    CASE
        wur.contract_type
        WHEN "dw" THEN
            4
        WHEN "qy" THEN
            1
        WHEN "gr" THEN
            2
        WHEN "qt" THEN
            5
        WHEN "gt" THEN
            3
        WHEN "个体户" THEN
            3
        END AS objtype,
    CASE
        when wur.is_free = 0 then 0
        when wur.is_free = 1 then 1
        WHEN  LENGTH( trim( wur.is_free ))= 0  THEN
            0
        WHEN wur.is_free IS NULL THEN
            0
        WHEN ISNULL( wur.is_free ) = 1 THEN
            0
        WHEN wur.is_free = NULL THEN
            0
        WHEN '' THEN
            0
        END AS rentfree,

    wur.is_stop AS USESTATUS,
    wur.update_date AS MODIFYDATE,
    wrt.id AS WYBASICINFOIDS,
    wrt.aname as WYBASICINFONAMES,
    contract_id as yycontractid,
    build_area as buildarea,
    wur.del_flag as status,
    "" as CREATOR
FROM
    wy_use_rel wur
        join wy_relation_tree wrt on wrt.rel_id = wur.rel_id