INSERT INTO yy_contract (
	id,
	clientcode,
	clientperson,
	clientphone,
	clienttype,
	contactperson,
	contactphone,
	contracttype,
	depositamount,
	endrentdate,
	iseconomiccommit,
	leaser,
	leasercode,
	legaladdress,
	legalcode,
	legalperson,
	legalphone,
	legaltype,
	remark,
	rentprice,
	startrentdate,
	leasertype,
	bsnum,
	credate,
	modifydate,
  leasearea,
  businesslicenseaddr,
  proname,
  status,
  modifier,
  creator,
  leaseuse,
  firstrenttime,
  leasetype,
  wybasicinfoids,
  wybasicinfonames,
  wyaddress
)  SELECT DISTINCT
wc.id AS id,
wc.agent_cert_no AS clientcode,
wc.agent AS clientperson,
wc.agent_phone AS clientphone,
CASE
		wc.agent_cert_type 
		WHEN 1 THEN
		1 
		WHEN 2 THEN
		2 
	END AS clienttype,
CASE
		wc.source 
		WHEN "new" THEN
		1 
		WHEN "renew" THEN
		2 
	END AS contracttype,
	wc.create_date AS credate,
	wc.deposit AS depositamount,
	wc.rent_date_end AS endrentdate,
	wc.economic_rate AS iseconomiccommit,
	wc.use_unit AS leaser,
	wc.cert_no AS leasercode,
	wc.license_address AS legaladdress,
	wc.legal_cert_no AS legalcode,
	wc.legal_name AS legalperson,
	wc.legal_phone AS legalphone,
CASE
		wc.legal_cert_type 
		WHEN 1 THEN
		1 
		WHEN 2 THEN
		2 
		WHEN 3 THEN
		3 
	END AS legaltype,
	wc.remarks AS remark,
	wc.unit_price AS rentprice,
	wc.rent_date_start AS startrentdate,
	wc.update_date AS modifydate,
	1 AS leasertype,
	wc.bsnum,
	wrt.address AS leasearea,
	wc.NAME AS proname,
	wc.update_by AS modifier,
	wc.practical_use AS leaseuse,
	wc.type AS leasetype,
	GROUP_CONCAT( wrt.aname ) AS wybasicinfonames,
	GROUP_CONCAT( wrt.id ) AS wybasicinfoids,
	GROUP_CONCAT( wrt.address ) AS wyaddress,
	wcp.`value` AS businesslicenseaddr,
	wcp2.`value` AS FIRSTRENTTIME,
	wcp3.`value` AS FIRSTRENTMONEY,
	wcp4.`value` AS contactperson,
	wcp5.`value` AS contactphone,
	wc.price AS MONTHLYRENT ,
	wc.del_flag as `status`,
	"zhengfuhetong" as CREATOR,
	"A0001" as TENANTID,
	wc.update_state
FROM
	wy_contract wc
	LEFT JOIN wy_contract_bind wcb ON wcb.contract_id = wc.id
	LEFT JOIN wy_relation_tree wrt ON wrt.rel_id = wcb.rel_id
	LEFT JOIN wy_contract_property wcp ON wcp.rel_id = wc.id 
	AND wcp.`code` = "licenseAddress"
	LEFT JOIN wy_contract_property wcp2 ON wcp2.rel_id = wc.id 
	AND wcp2.`code` = "firstPayDate"
	LEFT JOIN wy_contract_property wcp3 ON wcp3.rel_id = wc.id 
	AND wcp3.`code` = "firstPayAmount"
	LEFT JOIN wy_contract_property wcp4 ON wcp4.rel_id = wc.id 
	AND wcp4.`code` = "contact"
	LEFT JOIN wy_contract_property wcp5 ON wcp5.rel_id = wc.id 
	AND wcp5.`code` = "contactPhone" 
GROUP BY
	wc.id;


# 导入合同时的问题，首期租金交付时间需要由2022-1-21格式变为2022/1/21


	-- 根据最新合同表 更新合同物业关联表 更新合同id
update yy_contract_wy_relation ycwr
LEFT JOIN yy_contract yc ON yc.table_id = ycwr.old_yy_contract_id
set ycwr.yy_contract_id = yc.id
where ycwr.old_yy_contract_id IS NOT NULL;

-- 根据最新合同表 更新合同物业关联表 更新物业id
update yy_contract_wy_relation ycwr
JOIN wy_basic_info wbi ON wbi.table_id = ycwr.old_wy_basic_info_id
set ycwr.wy_basic_info_id = wbi.id
where ycwr.old_wy_basic_info_id is not null;


SELECT
	ycwr.old_yy_contract_id,
	ycwr.old_wy_basic_info_id
	,
	wbi.id
FROM
	yy_contract_wy_relation ycwr
	 JOIN wy_basic_info wbi ON wbi.table_id = ycwr.old_wy_basic_info_id;

SELECT
	ycwr.yy_contract_id,
	yc.id
FROM
	yy_contract_wy_relation ycwr
	LEFT JOIN yy_contract yc ON yc.table_id = ycwr.old_yy_contract_id
WHERE
	ycwr.old_yy_contract_id IS NOT NULL;


INSERT INTO `zbkc_kj`.`label_manage`(`id`, `label_id`, `sys_data_type_id`) VALUES (20211222171901 , 20211222171901 , 1473588447154442241);
INSERT INTO `zbkc_kj`.`label_manage`(`id`, `label_id`, `sys_data_type_id`) VALUES (20211222171902 , 20211222171902 , 1473588567619047425);
INSERT INTO `zbkc_kj`.`label_manage`(`id`, `label_id`, `sys_data_type_id`) VALUES (20211222171903 , 20211222171903 , 1473588638574088194);
INSERT INTO `zbkc_kj`.`label_manage`(`id`, `label_id`, `sys_data_type_id`) VALUES (20211222171904 , 20211222171904 , 1473588696807804929);
INSERT INTO `zbkc_kj`.`label_manage`(`id`, `label_id`, `sys_data_type_id`) VALUES (20211222171905 , 20211222171905 , 1473588751312785409);
INSERT INTO `zbkc_kj`.`label_manage`(`id`, `label_id`, `sys_data_type_id`) VALUES (20211222171906 , 20211222171906 , 1473588806228807681);
INSERT INTO `zbkc_kj`.`label_manage`(`id`, `label_id`, `sys_data_type_id`) VALUES (20211222171907 , 20211222171907 , 1473588857978130434);


#将物业地址为南山软件园的合同类型 修改为 软件园
UPDATE yy_contract yy
    LEFT JOIN yy_contract_wy_relation ycwr ON ycwr.yy_contract_id = yy.id
    LEFT JOIN wy_basic_info wy ON ycwr.wy_basic_info_id = wy.id
SET yy.template_type = 4,
    yy.type = 6
WHERE
    wy.NAME LIKE '%南山软件园%'
