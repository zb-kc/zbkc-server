package com.zbkc.service;

import com.zbkc.model.dto.WyAssetEntryDTO;
import com.zbkc.model.dto.export.ExporProRightRegDTO;
import com.zbkc.model.dto.export.ExportWyAssetEntryDTO;
import com.zbkc.model.po.WyAssetEntry;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.dto.WyAssetEntryHomeDTO;
import com.zbkc.model.vo.ResponseVO;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;

/**
 * <p>
 * 资产入账信息表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-03
 */
public interface WyAssetEntryService extends IService<WyAssetEntry> {


    ResponseVO selectAllName(Long userId);

    ResponseVO addEntry(WyAssetEntryDTO dto) throws ParseException, Exception;

    ResponseVO getList(WyAssetEntryHomeDTO dto);

    ResponseVO seeRow(String id);

    ResponseVO updEntry(WyAssetEntryDTO dto) throws Exception;

    ResponseVO delAssetEntry(String  id);

    ResponseVO export(ExportWyAssetEntryDTO dto , HttpServletResponse response);

}
