package com.zbkc.service;

import com.zbkc.model.po.SysParamConfig;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.BaseService;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统参数配置表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-05
 */
public interface SysParamConfigService extends BaseService<SysParamConfig> {

    /**
     * @param page 页码
     * @param size 行数
     * @return  ResponseVO
     */
    ResponseVO page(Integer page,Integer size);

    /**
     * @param name 名称
     * @return ResponseVO
     */
    ResponseVO selectByInput(@Param("name") String name);

    /**
     * @param id 主键id
     * @param status 当前状态
     * @return ResponseVO
     */
    ResponseVO status(Long id,Boolean status);
}
