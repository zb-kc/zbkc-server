package com.zbkc.service;

import com.zbkc.model.po.SysDataType;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.BaseService;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 数据字典类型表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-03
 */
public interface SysDataTypeService extends BaseService<SysDataType> {



    /**
     * @param id  主键id
     * @param status 当前状态
     * @return ResponseVO
     */
    ResponseVO status(@Param("id") Long id,@Param("status") Boolean status);

    /**
     * @param page 页码
     * @param size 行数
     * @param name 名称
     * @return ResponseVO
     */
    ResponseVO pageByName(@Param("p")Integer page,@Param("size")Integer size,@Param( "name")String name);

    /**
     * @return ResponseVO
     */
    ResponseVO selectAllByCode(String code);

}
