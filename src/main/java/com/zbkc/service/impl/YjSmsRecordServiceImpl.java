package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.enums.ExportEnum;
import com.zbkc.common.utils.DateUtil;
import com.zbkc.common.utils.ExcelUtils;
import com.zbkc.common.utils.ToolUtils;
import com.zbkc.mapper.SysFilePathMapper;
import com.zbkc.mapper.WySysMsgMapper;
import com.zbkc.mapper.YjSmsRecordMapper;
import com.zbkc.mapper.YyContractMapper;
import com.zbkc.model.dto.SmsDTO;
import com.zbkc.model.dto.SmsReCordDTO;
import com.zbkc.model.dto.YjContractDTO;
import com.zbkc.model.dto.export.ExportYjContractDTO;
import com.zbkc.model.po.SysFilePath1;
import com.zbkc.model.po.YjSmsRecord;
import com.zbkc.model.vo.CallRecordVO;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.vo.YjContractVo;
import com.zbkc.service.YjSmsRecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 预警提醒——合同到期提醒_短信记录表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-15
 */
@Service
public class YjSmsRecordServiceImpl extends ServiceImpl<YjSmsRecordMapper, YjSmsRecord> implements YjSmsRecordService {

    @Autowired
    private ToolUtils toolUtils;
    @Autowired
    private DateUtil dateUtil;
    @Autowired
    private YjSmsRecordMapper yjSmsRecordMapper;
    @Autowired
    private YyContractMapper yyContractMapper;
    @Autowired
    private SysFilePathMapper sysFilePathMapper;
    @Autowired
    private WySysMsgMapper wySysMsgMapper;


    @Override
    public ResponseVO allContarct(YjContractDTO dto) {

        if(dto.getP()<=0){
            return new ResponseVO(ErrorCodeEnum.PAGE_NUMBER_ERROR,ErrorCodeEnum.PAGE_NUMBER_ERROR.getErrMsg());
        }
        dto.setP( (dto.getP()-1)*dto.getSize() );
        List<YjContractVo> yjContracts = yyContractMapper.selectAllYjContract(dto);
        for (YjContractVo yjContract : yjContracts) {
            yjContract.setContractFilePathId(yjContract.getContractFilePathId()==null?0:yjContract.getContractFilePathId());
            List<SysFilePath1> fileList = sysFilePathMapper.getBydbFileId(yjContract.getContractFilePathId());
            yjContract.setContractFileList(fileList);
        }
        PageVO<YjContractVo> vo = new PageVO<>();
        vo.setList(yjContracts);
        vo.setTotal(yyContractMapper.selectAllYjContractTotal(dto).size());
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }

    @Override
    public ResponseVO selectAllRecord(SmsReCordDTO dto) {
        List<CallRecordVO> list = yjSmsRecordMapper.selectAllWarnRecord(dto);
    return new ResponseVO(ErrorCodeEnum.SUCCESS,list);
    }

    @Override
    public ResponseVO selectWord(String name) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS,yjSmsRecordMapper.selectWord(name));
    }

    @Override
    public ResponseVO sendMessage(SmsDTO dto) {
        YjSmsRecord yjSmsRecord = new YjSmsRecord();

        yjSmsRecord.setId(IdWorker.getId());
        yjSmsRecord.setWyBasicInfoId(dto.getWyId());
        yjSmsRecord.setContractId(dto.getContractId());
        yjSmsRecord.setMsgType(dto.getMsgType());
        yjSmsRecord.setSendStatus(dto.getSendStatus());
        yjSmsRecord.setSendTime(new Timestamp(System.currentTimeMillis()));
        yjSmsRecord.setUserId(dto.getUserId());

        Integer i = yjSmsRecordMapper.insertSmsRecord(yjSmsRecord);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO  export(ExportYjContractDTO dto, HttpServletResponse response) {
        List<YjContractVo> list = yyContractMapper.selectAllYjContractTotal(dto.getYjContractDTO());

        for (YjContractVo yjContractVo : list) {
            yjContractVo.setLeaseUse(yjContractVo.getLeaseUse()==null?0:yjContractVo.getLeaseUse());
            switch (yjContractVo.getLeaseUse()){
                case 1:
                    yjContractVo.setLeaseUseStr(ExportEnum.OFFICE.getName());
                    break;
                case 2:
                    yjContractVo.setLeaseUseStr(ExportEnum.COMPREHENSIVE_RESEARCH.getName());
                    break;
                case 3:
                    yjContractVo.setLeaseUseStr(ExportEnum.INDUSTRY_RESEARCH.getName());
                    break;
                case 4:
                    yjContractVo.setLeaseUseStr(ExportEnum.RESEARCH_AND_DEVELOPMENT.getName());
                    break;
                 default:
                    break;
            }
        }
        Map<String, String> keyMap = dto.getKeyMap();
        //需要显示的字段名
        //字段名对应的中文名
        String[] headers = null;
        String[] needShowKeyArr = null;

        if (StringUtils.isEmpty(dto.getNeedShowKeys())) {
            //不传getNeedShowKeys 显示所有字段
            Set<String> keys = keyMap.keySet();
            needShowKeyArr = keys.toArray(new String[keys.size()]);
            headers = new String[keys.size()];
        } else {
            needShowKeyArr = dto.getNeedShowKeys().split(",");
            //字段名对应的中文名
            headers = new String[needShowKeyArr.length];
        }

        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        try {
            ExcelUtils.getExcel(response, headers, needShowKeyArr, list, "YjWarning.xls");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseVO getAllRemindMsgName() {
        return new ResponseVO(ErrorCodeEnum.SUCCESS,wySysMsgMapper.getAllRemindMsgName());
    }

}
