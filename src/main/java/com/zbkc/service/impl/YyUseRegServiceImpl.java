package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.zbkc.common.enums.CommonFinalData;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.enums.ExportEnum;
import com.zbkc.common.utils.ExcelUtils;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.common.utils.ToolUtils;
import com.zbkc.mapper.*;
import com.zbkc.model.dto.homeList.YyUseRegHomeList;
import com.zbkc.model.dto.export.ExportUseRegDTO;
import com.zbkc.model.dto.YyUseRegDTO;
import com.zbkc.model.dto.YyUseRegHomeDTO;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.vo.YyUseRegVo;
import com.zbkc.service.YyUseRegService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 运营管理——使用登记 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-09
 */
@Service
public class YyUseRegServiceImpl extends ServiceImpl<YyUseRegMapper, YyUseReg> implements YyUseRegService {

    @Autowired
    private YyUseRegMapper yyUseRegMapper;
    @Autowired
    private YyUseRegFilePathMapper yyUseRegFilePathMapper;
    @Autowired
    private ToolUtils toolUtils;
    @Autowired
    private SysFilePathMapper sysFilePathMapper;
    @Autowired
    private SysAttachmentMapper sysAttachmentMapper;
    @Autowired
    private WyBasicInfoMapper wyBasicInfoMapper;
    @Autowired
    private LabelManageMapper labelManageMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO addUseReg(YyUseRegDTO dto1) {

        YyUseRegDTO dto = new YyUseRegDTO();
        dto=dto1;

        YyUseReg info = new YyUseReg();
        Long id=IdWorker.getId();
        info.setId(id);

        info.setWyBasicInfoId(dto.getWyBasicInfoId());
        info.setMethod(dto.getMethod());
        info.setRentFree(dto.getRentFree());
        info.setStatus(dto.getStatus());
        info.setObjName(dto.getObjName());
        info.setObjType(dto.getObjType());
        info.setCreditCode(dto.getCreditCode());
        info.setContactName(dto.getContactName());
        info.setContactPhone(dto.getContactPhone());
        info.setStartTime(toolUtils.strFormatTimetamp(dto.getStartTime()));
        info.setEndTime(toolUtils.strFormatTimetamp(dto.getEndTime()));
        info.setActualUse(dto.getActualUse());
        info.setUserId(dto.getUserId());

        List<SysFilePath> insertSysFilePathList = new ArrayList<>();
        List<SysAttachment> insertSysAttachmentList = new ArrayList<>();

        //新增自定义文件
        long customFileId = IdWorker.getId();
        info.setCustomFileId(customFileId);
        if (null != dto.getCustomFileMap()) {
            List<String> keyList = dto.getCustomFileMap().keySet().stream().filter(k -> k != null).collect(Collectors.toList());
            for (String key : keyList) {
                List<SysFilePath> sysFilePathList = dto.getCustomFileMap().get(key);
                for (SysFilePath sysFilePath : sysFilePathList) {
                    SysFilePath file = new SysFilePath();
                    long fileId = IdWorker.getId();
                    file.setId(fileId);
                    file.setFileSize(sysFilePath.getFileSize());
                    file.setFileName(sysFilePath.getFileName());
                    file.setPath(sysFilePath.getPath());
                    file.setDbFileId(customFileId);
                    insertSysFilePathList.add(file);
                    SysAttachment sysAttachment = new SysAttachment();
                    sysAttachment.setId(IdWorker.getId());
                    sysAttachment.setName(key);
                    sysAttachment.setSysFilePathId(fileId);
                    insertSysAttachmentList.add(sysAttachment);
                }
            }
        }
        //添加使用批准文件
        long useApprovalFileId = IdWorker.getId();
        info.setUseApprovalFileId(useApprovalFileId);
        if (null != dto.getUseApprovalFileList()) {
            for (SysFilePath useApprovalFile : dto.getUseApprovalFileList()) {
                SysFilePath file = new SysFilePath();
                file.setDbFileId(useApprovalFileId);
                file.setPath(useApprovalFile.getPath());
                file.setFileName(useApprovalFile.getFileName());
                file.setFileSize(useApprovalFile.getFileSize());
                file.setId(IdWorker.getId());
                insertSysFilePathList.add(file);
            }
        }
        //添加合同附件
        long contractId = IdWorker.getId();
        info.setContractFileId(contractId);
        if (null != dto.getContractFileList()) {
            for (SysFilePath contractFile : dto.getContractFileList()) {
                SysFilePath file = new SysFilePath();
                file.setId(IdWorker.getId());
                file.setPath(contractFile.getPath());
                file.setFileName(contractFile.getFileName());
                file.setFileSize(contractFile.getFileSize());
                file.setDbFileId(contractId);
                insertSysFilePathList.add(file);
            }
        }

        info.setUserId(dto.getUserId());
        info.setCreateTime(new Timestamp(System.currentTimeMillis()));

        if(!insertSysFilePathList.isEmpty()){
            this.sysFilePathMapper.insertBatch(insertSysFilePathList);
        }
        if(!insertSysAttachmentList.isEmpty()){
            this.sysAttachmentMapper.insertBatch(insertSysAttachmentList);
        }
        int i = yyUseRegMapper.addYyUseReg(info);
        //新增登记完成后，修改物业状态为在用
        long propertyStatusId = IdWorker.getId();
        LabelManage labelManage = new LabelManage();
        labelManage.setLabelId(propertyStatusId);
        labelManage.setId(IdWorker.getId());
        //数据字典id 在用
        labelManage.setSysDataTypeId(1461637894069403649L);
        int i1 = labelManageMapper.addLabelManage(labelManage);
        Timestamp updateTime = new Timestamp(System.currentTimeMillis());
        Integer j = wyBasicInfoMapper.updWyStatus(propertyStatusId, dto.getWyBasicInfoId(),updateTime);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i+j+i1);
    }

    @Override
    public ResponseVO getList(YyUseRegHomeDTO dto) {
        if(dto.getP()<=0){
            return new ResponseVO(ErrorCodeEnum.SUCCESS,null);
        }
        dto.setP( (dto.getP()-1)*dto.getSize() );
        PageVO<YyUseRegHomeList> vo = new PageVO<>();
        List<YyUseRegHomeList> list = yyUseRegMapper.queryYyUseRegHomeList(dto);
        vo.setList(list);
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());
        vo.setTotal(yyUseRegMapper.queryYyUseRegHomeListTotal(dto).size());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO seeRow(String id) {
        YyUseRegVo vo = new YyUseRegVo();

        YyUseRegVo row = yyUseRegMapper.seeRow(id);
        vo=row;
        List<SysFilePath1> path = sysFilePathMapper.getBydbFileId(row.getUseApprovalFileId()==null?0:row.getUseApprovalFileId());
        List<SysFilePath1> path1 = sysFilePathMapper.getBydbFileId(row.getContractFileId()==null?0:row.getContractFileId());

        Map<String, List<SysFilePath1>> customFileMap = new HashMap<>();
        List<String> nameList= sysFilePathMapper.getName(row.getCustomFileId());
        for (String name : nameList) {
            customFileMap.put(name,sysFilePathMapper.getCustFile(row.getCustomFileId(),name));
        }
        vo.setUseApprovalFileList(path);
        vo.setContractFileList(path1);
        vo.setCustomFileMap(customFileMap);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO updUseReg(YyUseRegDTO dto1) {
        YyUseRegDTO dto = new YyUseRegDTO();
        dto=dto1;
        YyUseReg info = new YyUseReg();

        info.setId(dto.getId());
        info.setWyBasicInfoId(dto.getWyBasicInfoId());
        info.setMethod(dto.getMethod());
        info.setRentFree(dto.getRentFree());
        info.setStatus(dto.getStatus());
        info.setObjName(dto.getObjName());
        info.setObjType(dto.getObjType());
        info.setCreditCode(dto.getCreditCode());
        info.setContactName(dto.getContactName());
        info.setContactPhone(dto.getContactPhone());
        info.setStartTime(toolUtils.strFormatTimetamp(dto.getStartTime()));
        info.setEndTime(toolUtils.strFormatTimetamp(dto.getEndTime()));
        info.setActualUse(dto.getActualUse());
        info.setUserId(dto.getUserId());
        info.setUpdateTime(new Timestamp(System.currentTimeMillis()));

        List<SysFilePath> insertSysFilePathList = new ArrayList<>();
        List<SysAttachment> insertSysAttachmentList = new ArrayList<>();

        //修改使用批准文件
        long useApprovalFileId = IdWorker.getId();
        info.setUseApprovalFileId(useApprovalFileId);
        if (null != dto.getUseApprovalFileList()) {
            for (SysFilePath useApprovalFile : dto.getUseApprovalFileList()) {
                SysFilePath file = new SysFilePath();
                file.setDbFileId(useApprovalFileId);
                file.setFileName(useApprovalFile.getFileName());
                file.setPath(useApprovalFile.getPath());
                file.setFileSize(useApprovalFile.getFileSize());
                file.setId(IdWorker.getId());
                insertSysFilePathList.add(file);
            }
        }
        //修改合同附件
        long contractId = IdWorker.getId();
        info.setContractFileId(contractId);
        if (null != dto.getContractFileList()) {
            for (SysFilePath contractFile : dto.getContractFileList()) {
                SysFilePath file = new SysFilePath();
                file.setId(IdWorker.getId());
                file.setFileName(contractFile.getFileName());
                file.setDbFileId(contractId);
                file.setFileSize(contractFile.getFileSize());
                file.setPath(contractFile.getPath());
                insertSysFilePathList.add(file);
            }
        }

        long customFileId = IdWorker.getId();
        info.setCustomFileId(customFileId);
        if (null != dto.getCustomFileMap()) {
            List<String> keyList = dto.getCustomFileMap().keySet().stream().filter(k -> k != null).collect(Collectors.toList());
            for (String key : keyList) {
                List<SysFilePath> sysFilePathList = dto.getCustomFileMap().get(key);
                for (SysFilePath sysFilePath : sysFilePathList) {
                    SysFilePath file = new SysFilePath();
                    long fileId = IdWorker.getId();
                    file.setId(fileId);
                    file.setFileName(sysFilePath.getFileName());
                    file.setFileSize(sysFilePath.getFileSize());
                    file.setDbFileId(customFileId);
                    file.setPath(sysFilePath.getPath());
                    SysAttachment sysAttachment = new SysAttachment();
                    sysAttachment.setName(key);
                    sysAttachment.setId(IdWorker.getId());
                    sysAttachment.setSysFilePathId(fileId);
                    insertSysFilePathList.add(file);
                    insertSysAttachmentList.add(sysAttachment);
                }
            }
        }
        if(!insertSysAttachmentList.isEmpty()){
            this.sysAttachmentMapper.insertBatch(insertSysAttachmentList);
        }
        if(!insertSysFilePathList.isEmpty()){
            this.sysFilePathMapper.insertBatch(insertSysFilePathList);
        }
        //当使用状态为停用的时候，修改物业状态为空置
        if(info.getStatus()==2){
            long propertyStatusId = IdWorker.getId();
            LabelManage labelManage = new LabelManage();
            labelManage.setId(IdWorker.getId());
            labelManage.setLabelId(propertyStatusId);
            //数据字典id 空置
            labelManage.setSysDataTypeId(1461637977200508929L);
            labelManageMapper.addLabelManage(labelManage);
            Timestamp updateTime = new Timestamp(System.currentTimeMillis());
            wyBasicInfoMapper.updWyStatus(propertyStatusId, dto.getWyBasicInfoId(),updateTime);
        }
        int i = yyUseRegMapper.updUseReg(info);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO export(ExportUseRegDTO dto, HttpServletResponse response) {
        List<YyUseRegHomeList> list = yyUseRegMapper.queryYyUseRegHomeListTotal(dto.getYyUseRegHomeDTO());

        for (YyUseRegHomeList yyUseRegHomeList : list) {
            //拼接开始日期和截止日期便于导出
            if (yyUseRegHomeList.getStartTime() == null || yyUseRegHomeList.getEndTime() == null) {
                yyUseRegHomeList.setStartTimeAndEndTime("无");
            } else {
                Date startDate = TimeUtils.parseDate(yyUseRegHomeList.getStartTime(), "yyyy-MM-dd");
                Date endDate = TimeUtils.parseDate(yyUseRegHomeList.getEndTime(), "yyyy-MM-dd");
                String startTimeAndEndTime = TimeUtils.getDateString(startDate) + "至" + TimeUtils.getDateString(endDate);
                yyUseRegHomeList.setStartTimeAndEndTime(startTimeAndEndTime);
            }
            //防止数据库返回为null导致报错
            yyUseRegHomeList.setMethod(yyUseRegHomeList.getMethod() == null ? 0 : yyUseRegHomeList.getMethod());
            yyUseRegHomeList.setStatus(yyUseRegHomeList.getStatus()==null?0:yyUseRegHomeList.getStatus());
            if (yyUseRegHomeList.getMethod() == 1) {
                yyUseRegHomeList.setMethodStr(ExportEnum.LEASE.getName());
            }
            if (yyUseRegHomeList.getMethod() == 2) {
                yyUseRegHomeList.setMethodStr(ExportEnum.DEPLOYMENT.getName());
            }
            if (yyUseRegHomeList.getStatus() == 1) {
                yyUseRegHomeList.setStatusStr(ExportEnum.IN_USE.getName());
            }
            if (yyUseRegHomeList.getStatus() == 2) {
                yyUseRegHomeList.setStatusStr(ExportEnum.STOP_USE.getName());
            }
        }
        Map<String, String> keyMap = dto.getKeyMap();

        //需要显示的字段名
        //字段名对应的中文名
        String[] headers = null;
        String[] needShowKeyArr = null;

        if (StringUtils.isEmpty(dto.getNeedShowKeys())) {
            //不传getNeedShowKeys 显示所有字段
            Set<String> keys = keyMap.keySet();
            needShowKeyArr = keys.toArray(new String[keys.size()]);
            headers = new String[keys.size()];
        } else {
            needShowKeyArr = dto.getNeedShowKeys().split(",");
            //字段名对应的中文名
            headers = new String[needShowKeyArr.length];
        }

        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        try {
            ExcelUtils.getExcel(response, headers, needShowKeyArr, list, "UseRegistration.xls");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseVO updStatus(Long id, Integer status) {
        int i = yyUseRegMapper.updStatus(id, status);
        YyUseRegVo yyUseRegVo = yyUseRegMapper.seeRow(String.valueOf(id));
        Timestamp updateTime = new Timestamp(System.currentTimeMillis());
        Integer i2 = wyBasicInfoMapper.updWyStatus(CommonFinalData.PROPERTY_STATUS_2, yyUseRegVo.getWyBasicInfoId(), updateTime);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, i + i2);
    }
}
