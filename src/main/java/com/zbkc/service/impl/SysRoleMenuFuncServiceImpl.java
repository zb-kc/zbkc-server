package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.mapper.SysRoleMenuFuncMapper;
import com.zbkc.model.dto.GrantMenuDTO;
import com.zbkc.model.dto.MenuDTO;
import com.zbkc.model.po.SysRoleMenuFunc;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.SysRoleMenuFuncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统角色菜单功能表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-02
 */
@Service
public class SysRoleMenuFuncServiceImpl implements SysRoleMenuFuncService {
    @Autowired
    private SysRoleMenuFuncMapper sysRoleMenuFuncMapper;

    @Override
    public ResponseVO findSelfMenu(Long roleId) {
        List<MenuDTO> menuDTOByRoleid = sysRoleMenuFuncMapper.findMenuDTOByRoleid(roleId, (long) 0);
        if(menuDTOByRoleid.size()==0||menuDTOByRoleid == null){
            return new ResponseVO(ErrorCodeEnum.ROLE_NO_MENUS,null);
        }

        for (MenuDTO menuDTO : menuDTOByRoleid) {
            Long pid = menuDTO.getId();
            List<MenuDTO> children = sysRoleMenuFuncMapper.findMenuDTOByRoleid(roleId, pid);
            menuDTO.setChirdren(children);
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS,menuDTOByRoleid);
    }

    @Override
    public ResponseVO grantMenus(GrantMenuDTO grantMenuDTO) {
        this.sysRoleMenuFuncMapper.deleteByRoleId(grantMenuDTO.getRoleId());

        List<Long> ids = grantMenuDTO.getMenuIds();
        List<SysRoleMenuFunc> list = new ArrayList<>(ids.size());
        for (Long menuId : ids) {
            SysRoleMenuFunc roleMenuFunc = new SysRoleMenuFunc();
            roleMenuFunc.setRoleId(grantMenuDTO.getRoleId());
            roleMenuFunc.setMenuId(menuId);
            roleMenuFunc.setId(IdWorker.getId());
            list.add(roleMenuFunc);
        }
        Integer record = this.sysRoleMenuFuncMapper.insertBatch(list);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @Override
    public ResponseVO findSelfMenuIds(Long roleId) {
        List<String> selfMenuIds = sysRoleMenuFuncMapper.findSelfMenuIds(roleId);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,selfMenuIds);
    }

    @Override
    public ResponseVO insert(SysRoleMenuFunc entity) {
        return null;
    }

    @Override
    public ResponseVO deleteById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO deleteByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO delete(Wrapper<SysRoleMenuFunc> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO deleteBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO updateById(SysRoleMenuFunc entity) {
        return null;
    }

    @Override
    public ResponseVO update(SysRoleMenuFunc entity, Wrapper<SysRoleMenuFunc> updateWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO selectBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO selectOne(Wrapper<SysRoleMenuFunc> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectCount(Wrapper<SysRoleMenuFunc> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectList(Wrapper<SysRoleMenuFunc> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectMaps(Wrapper<SysRoleMenuFunc> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectObjs(Wrapper<SysRoleMenuFunc> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<SysRoleMenuFunc>> ResponseVO selectPage(P page, Wrapper<SysRoleMenuFunc> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<Map<String, Object>>> ResponseVO selectMapsPage(P page, Wrapper<SysRoleMenuFunc> queryWrapper) {
        return null;
    }



}
