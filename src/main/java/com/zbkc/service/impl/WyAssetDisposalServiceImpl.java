package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.ExcelUtils;
import com.zbkc.common.utils.ToolUtils;
import com.zbkc.mapper.*;
import com.zbkc.model.dto.WyAssetDisposalDTO;
import com.zbkc.model.dto.WyAssetDisposalHomeDTO;
import com.zbkc.model.dto.export.ExportWyAssetDisposalDTO;
import com.zbkc.model.dto.homeList.WyAssetDisposalHomeList;
import com.zbkc.model.dto.homeList.WyAssetEntryHomeList;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.vo.WyAssetDisposalVo;
import com.zbkc.service.WyAssetDisposalService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 资产处置信息表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-07
 */
@Service
@Slf4j
public class WyAssetDisposalServiceImpl extends ServiceImpl<WyAssetDisposalMapper, WyAssetDisposal> implements WyAssetDisposalService {

    @Autowired
    private WyBasicInfoMapper wyBasicInfoMapper;
    @Autowired
    private WyAssetDisposalMapper wyAssetDisposalMapper;
    @Autowired
    private SysImgMapper sysImgMapper;
    @Autowired
    private ToolUtils toolUtils;
    @Autowired
    private LabelManageMapper labelManageMapper;
    @Autowired
    private SysFilePathMapper sysFilePathMapper;
    @Autowired
    private SysAttachmentMapper sysAttachmentMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO addDisposal(WyAssetDisposalDTO dto1) throws Exception {
        WyAssetDisposalDTO dto = new WyAssetDisposalDTO();
        dto=dto1;

        long startTime = System.currentTimeMillis();
        WyAssetDisposal info = new WyAssetDisposal();

        info.setWyBasicInfoId(dto.getWyBasicInfoId());
        info.setDocumentCode(dto.getDocumentCode());
        info.setAuthority(dto.getAuthority());
        info.setApprovalTime(toolUtils.strFormatDate(dto.getApprovalTime()));
        info.setDisposalTime(toolUtils.strFormatDate(dto.getDisposalTime()));
        info.setDisposalUnit(dto.getDisposalUnit());

        Long solvingMethodId=IdWorker.getId();
        if (dto.getSolvingMethodId()!=null) {
            LabelManage solvingMethod = new LabelManage();
            solvingMethod.setId(IdWorker.getId());
            solvingMethod.setLabelId(solvingMethodId);
            solvingMethod.setSysDataTypeId(dto.getSolvingMethodId());

            info.setSolvingMethodId(solvingMethodId);
            labelManageMapper.addLabelManage(solvingMethod);
        }
        info.setRemark(dto.getRemark());
        info.setCreateTime(new Timestamp(System.currentTimeMillis()));
        info.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        info.setUserId(dto.getUserId());

        Long custRightFileId=IdWorker.getId();
        info.setCustRightFileId(custRightFileId);
        if (dto.getCustRightFile()!=null) {
            Set<String> keySet = dto.getCustRightFile().keySet();
            List<String> strings = keySet.stream().collect(Collectors.toList());
            for (String string : strings) {
                List<SysFilePath> files = dto.getCustRightFile().get(string);
                for (SysFilePath sysfile : files) {
                    SysFilePath sysFilePath = new SysFilePath();


                    Long FileId = IdWorker.getId();
                    sysFilePath.setId(FileId);
                    sysFilePath.setFileName(sysfile.getFileName());
                    sysFilePath.setFileSize(sysfile.getFileSize());
                    sysFilePath.setPath(sysfile.getPath());
                    sysFilePath.setDbFileId(custRightFileId);

                    sysFilePathMapper.addSysFilePath(sysFilePath);


                    SysAttachment attachment = new SysAttachment();
                    attachment.setId(IdWorker.getId());
                    attachment.setSysFilePathId(FileId);
                    attachment.setName(string);
                    sysAttachmentMapper.addSysAttachMent(attachment);


                }
            }
        }



        Integer i = wyAssetDisposalMapper.addDisposal(info);
        long endTime = System.currentTimeMillis();
        log.info("程序运行时间： " + (endTime - startTime) + "ms");

        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO getList(WyAssetDisposalHomeDTO dto) {
        if(dto.getP()<=0){
            return new ResponseVO(ErrorCodeEnum.SUCCESS,null);
        }
        dto.setP( (dto.getP()-1)*dto.getSize() );
        PageVO<WyAssetDisposalHomeList> vo = new PageVO<>();

        List<WyAssetDisposalHomeList> list = wyAssetDisposalMapper.queryWyAssetDisposalHomeList(dto);
        vo.setList(list);
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());
        vo.setTotal(wyAssetDisposalMapper.queryWyAssetDisposalHomeListTotal(dto).size());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO seeRow(String id) {
        WyAssetDisposalVo vo = new WyAssetDisposalVo();
        WyAssetDisposal disposal = wyAssetDisposalMapper.seeRow(id);

        vo.setWyAssetDisposal(disposal);
        vo.setSolvingMethodId(wyBasicInfoMapper.getSysDataTypeByLabelId(disposal.getSolvingMethodId()));

        HashMap<String, List<SysFilePath1>> map = new HashMap<>();
        for (String name:sysFilePathMapper.getName(disposal.getCustRightFileId())) {
            map.put(name,sysFilePathMapper.getCustFile(disposal.getCustRightFileId(), name));
        }
        vo.setCustRightFile(map);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO updDisposal(WyAssetDisposalDTO dto1) throws Exception {
        WyAssetDisposalDTO dto = new WyAssetDisposalDTO();
        dto=dto1;
        long startTime = System.currentTimeMillis();

        WyAssetDisposal info = new WyAssetDisposal();
        info.setId(dto.getId());
        info.setWyBasicInfoId(dto.getWyBasicInfoId());
        info.setDocumentCode(dto.getDocumentCode());
        info.setAuthority(dto.getAuthority());
        info.setApprovalTime(toolUtils.strFormatDate(dto.getApprovalTime()));
        info.setDisposalTime(toolUtils.strFormatDate(dto.getDisposalTime()));
        info.setDisposalUnit(dto.getDisposalUnit());


        if (dto.getSolvingMethodId()!=null) {
            Long solvingMethodId=IdWorker.getId();
            Long solvingMethodData=dto.getSolvingMethodId();
            LabelManage manage = new LabelManage();
            manage.setLabelId(solvingMethodId);
            manage.setSysDataTypeId(solvingMethodData);

            labelManageMapper.addLabelManage(manage);
            info.setSolvingMethodId(solvingMethodId);
        }

        info.setRemark(dto.getRemark());

        Long custRightFileId=IdWorker.getId();
        info.setCustRightFileId(custRightFileId);
        if (dto.getCustRightFile()!=null) {
            Set<String> keySet = dto.getCustRightFile().keySet();
            List<String> strings = keySet.stream().collect(Collectors.toList());
            for (String string : strings) {
                List<SysFilePath> files = dto.getCustRightFile().get(string);
                for (SysFilePath sysfile : files) {
                    SysFilePath sysFilePath = new SysFilePath();


                    Long FileId = IdWorker.getId();
                    sysFilePath.setId(FileId);
                    sysFilePath.setFileName(sysfile.getFileName());
                    sysFilePath.setFileSize(sysfile.getFileSize());
                    sysFilePath.setPath(sysfile.getPath());
                    sysFilePath.setDbFileId(custRightFileId);

                    sysFilePathMapper.addSysFilePath(sysFilePath);


                    SysAttachment attachment = new SysAttachment();
                    attachment.setId(IdWorker.getId());
                    attachment.setSysFilePathId(FileId);
                    attachment.setName(string);
                    sysAttachmentMapper.addSysAttachMent(attachment);


                }
            }
        }

        info.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        Integer i = wyAssetDisposalMapper.updDisposal(info);

        long endTime = System.currentTimeMillis();
        log.info("程序运行时间： " + (endTime - startTime) + "ms");
            return new ResponseVO(ErrorCodeEnum.SUCCESS, i);

    }

    @Override
    public ResponseVO delAssetDispoal(String id) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS,wyAssetDisposalMapper.delById(id));
    }

    @Override
    public ResponseVO export(ExportWyAssetDisposalDTO dto, HttpServletResponse response) {

        List<WyAssetDisposalHomeList> list = wyAssetDisposalMapper.queryWyAssetDisposalHomeListTotal(dto.getWyAssetDisposalHomeDTO());
        Map<String, String> keyMap = dto.getKeyMap();

        //需要显示的字段名
        //字段名对应的中文名
        String[] headers = null;
        String[] needShowKeyArr = null;

        if (StringUtils.isEmpty(dto.getNeedShowKeys())) {
            //不传getNeedShowKeys 显示所有字段
            Set<String> keys = keyMap.keySet();
            needShowKeyArr = keys.toArray(new String[keys.size()]);
            headers = new String[keys.size()];
        } else {
            needShowKeyArr = dto.getNeedShowKeys().split(",");
            //字段名对应的中文名
            headers = new String[needShowKeyArr.length];
        }

        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        try {
            ExcelUtils.getExcel(response, headers, needShowKeyArr, list, "WyAssetDisposalList.xls");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
