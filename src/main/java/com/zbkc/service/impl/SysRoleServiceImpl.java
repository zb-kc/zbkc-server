package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.SysRoleDataPermissionMapper;
import com.zbkc.mapper.SysRoleMapper;
import com.zbkc.model.po.SysRole;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.SysRoleDataPermissionService;
import com.zbkc.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统角色表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-07-29
 */
@Service
public class SysRoleServiceImpl implements SysRoleService {
    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private SysRoleDataPermissionMapper sysRoleDataPermissionMapper;

    @Autowired
    private SysRoleDataPermissionService sysRoleDataPermissionService;

    @Override
    public ResponseVO pageByIndex(Integer p, Integer size,String index) {
        if(null == p || p <= 0){
            throw new BusinessException(ErrorCodeEnum.PAGE_NUMBER_ERROR);
        }
        List<SysRole> list = sysRoleMapper.pageByIndex((p - 1) * size, size, index);
        PageVO<SysRole> vo = new PageVO<>();
        vo.setList( list );
        vo.setTotal(  sysRoleMapper.selectByNameTotal(index ));
        vo.setP(p);
        vo.setSize(size);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO insert(SysRole entity) {
        if (entity.getName() == null || "".equals(entity.getName())) {
            return new ResponseVO(ErrorCodeEnum.REGULARNOTACCORD, null);
        } else {
            long ct = System.currentTimeMillis();
            Timestamp timestamp = TimeUtils.LongToDate(ct);
            entity.setCreateTime(timestamp);
            entity.setStatus(1);
            int i = sysRoleMapper.insert(entity);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, i);
        }
    }

    @Override
    public ResponseVO status(Long id,Boolean status) {
        if(status) {
            Integer open = sysRoleMapper.statusOpen(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS,open);
        }else {
            Integer close = sysRoleMapper.statusClose(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, close);
        }

    }


    @Override
    public ResponseVO del(Serializable id) {
    return null;
    }

    @Override
    public ResponseVO selectList(Wrapper<SysRole> queryWrapper) {
        List<SysRole> sysRoles = sysRoleMapper.selectList(null);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,sysRoles);
    }

    @Override
    public ResponseVO updateById(SysRole entity) {
        long ct = System.currentTimeMillis();
        Timestamp timestamp = TimeUtils.LongToDate(ct);
        entity.setUpdateTime(timestamp);
        int i = sysRoleMapper.updateById(entity);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }



    @Override
    public ResponseVO deleteById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO deleteByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO delete(Wrapper<SysRole> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO deleteBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO update(SysRole entity, Wrapper<SysRole> updateWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO selectBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO selectOne(Wrapper<SysRole> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectCount(Wrapper<SysRole> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectMaps(Wrapper<SysRole> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectObjs(Wrapper<SysRole> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<SysRole>>ResponseVO selectPage(P page, Wrapper<SysRole> queryWrapper) {
      return null;
    }

    @Override
    public <P extends IPage<Map<String, Object>>> ResponseVO selectMapsPage(P page, Wrapper<SysRole> queryWrapper) {
        return null;
    }




}
