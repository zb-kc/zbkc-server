package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.mapper.SysUserGroupMapper;
import com.zbkc.model.dto.GroupDTO;
import com.zbkc.model.po.SysUserGroup;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.SysUserGroupService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.*;

/**
 * 登录service实现层
 * @author yangyan
 * @date 2021-7-16
 */
@Service
@Slf4j
public class SysUserGroupServiceImpl implements SysUserGroupService, Serializable {

    @Resource
    private SysUserGroupMapper sysUserGroupMapper;

    @Override
    public ResponseVO del(Serializable id) {
        Integer i = sysUserGroupMapper.delByInfo(id);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO updateById(SysUserGroup entity) {
        int i = sysUserGroupMapper.updateById(entity);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }


    @Override
    public ResponseVO status(Long id,Boolean status) {
        if(status) {
            Integer open = sysUserGroupMapper.statusOpen(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS,open);
        }else {
            Integer close = sysUserGroupMapper.statusClose(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, close);
        }
    }

    @Override
    public ResponseVO groupList(GroupDTO groupDTO) {

        groupDTO.setLevel( 0 );
        List<SysUserGroup> sysUserGroup = sysUserGroupMapper.groupList( groupDTO);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,sysGroupsList(sysUserGroup));
    }

    private List<SysUserGroup> sysGroupsList(List<SysUserGroup> sysUserGroups){

        for (int i = 0; i< sysUserGroups.size(); i++){
            List<SysUserGroup> list = sysUserGroupMapper.groupByPidList(sysUserGroups.get(i).getId());
            sysUserGroups.get( i ).setGroupList(list);

            sysGroupsList(sysUserGroups.get(i).getGroupList());

        }
        return sysUserGroups;

    }

    @Override
    public ResponseVO insert(SysUserGroup entity) {
        return null;
    }

    @Override
    public ResponseVO deleteById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO deleteByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO delete(Wrapper<SysUserGroup> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO deleteBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }


    @Override
    public ResponseVO update(SysUserGroup entity, Wrapper<SysUserGroup> updateWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO selectBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO selectOne(Wrapper<SysUserGroup> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectCount(Wrapper<SysUserGroup> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectList(Wrapper<SysUserGroup> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectMaps(Wrapper<SysUserGroup> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectObjs(Wrapper<SysUserGroup> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<SysUserGroup>> ResponseVO selectPage(P page, Wrapper<SysUserGroup> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<Map<String, Object>>> ResponseVO selectMapsPage(P page, Wrapper<SysUserGroup> queryWrapper) {
        return null;
    }
}
