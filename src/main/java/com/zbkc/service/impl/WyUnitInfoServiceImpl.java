package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.model.po.WyUnitInfo;
import com.zbkc.mapper.WyUnitInfoMapper;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.WyUnitInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 物业单元信息表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-09
 */
@Service
public class WyUnitInfoServiceImpl implements WyUnitInfoService {
    @Autowired
    private WyUnitInfoMapper wyUnitInfoMapper;

    @Override
    public ResponseVO pageByInput(Integer p, Integer size, String name) {
        List<WyUnitInfo> wyUnitInfos = wyUnitInfoMapper.pageByName((p - 1) * size, size, name);
        PageVO<WyUnitInfo> vo = new PageVO<>();
        vo.setList(wyUnitInfos);
        vo.setTotal(wyUnitInfoMapper.selectByNameTotal(name).size());
        vo.setP(p);
        vo.setSize(size);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO insert(WyUnitInfo entity) {
        long ct = System.currentTimeMillis();
        Timestamp timestamp = TimeUtils.LongToDate(ct);
        entity.setCreateTime(timestamp);
        int i = wyUnitInfoMapper.insert(entity);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO updateById(WyUnitInfo entity) {

        int i = wyUnitInfoMapper.updateById(entity);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO deleteById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO deleteByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO delete(Wrapper<WyUnitInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO deleteBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }


    @Override
    public ResponseVO update(WyUnitInfo entity, Wrapper<WyUnitInfo> updateWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO selectBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO selectOne(Wrapper<WyUnitInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectCount(Wrapper<WyUnitInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectList(Wrapper<WyUnitInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectMaps(Wrapper<WyUnitInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectObjs(Wrapper<WyUnitInfo> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<WyUnitInfo>> ResponseVO selectPage(P page, Wrapper<WyUnitInfo> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<Map<String, Object>>> ResponseVO selectMapsPage(P page, Wrapper<WyUnitInfo> queryWrapper) {
        return null;
    }


}
