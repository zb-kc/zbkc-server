package com.zbkc.service.impl;

import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.DesUtils;
import com.zbkc.common.utils.RedisUtil;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.SysUserOrgMapper;
import com.zbkc.model.dto.LoginDTO;
import com.zbkc.model.po.SysUser;
import com.zbkc.model.vo.AuthInfoVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.LoginService;
import com.zbkc.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 登录service实现层
 * @author yangyan
 * @date 2021-7-16
 */
@Service
@Slf4j
public class LoginServiceImpl implements LoginService, Serializable {

    @Resource
    private SysUserService sysUserService;
    @Resource
    DesUtils desUtils;
    @Resource
    RedisUtil redisUtil;
    @Resource
    SysUserOrgMapper sysUserOrgMapper;

    /**
     * @Description  用户登录
     *
     * */
    @Override
    public ResponseVO userLogin(LoginDTO userInfo) {
        log.info("请求数据:{}", new Gson().toJson(userInfo));
        AuthInfoVO authInfoVo = new AuthInfoVO();
        authInfoVo.setTokenType("bearer");
        //有效期7天
        //登录验证
        List<SysUser> sysUsers = sysUserService.queryByUserName(userInfo.getUserName());
        if (sysUsers.size() == 0 || sysUsers == null) {
            return new ResponseVO(ErrorCodeEnum.USER_NOT_FOUND, null);
        } else {
            SysUser sysUser = sysUsers.get(0);
            if (!userInfo.getPwd().equals(sysUser.getPwd())) {
                return new ResponseVO(ErrorCodeEnum.USERNAMEORPASSWORD_INPUT_ERROR, null);
            }
            long times = System.currentTimeMillis();

            String token = sysUser.getUserName() + "|" + times;
            authInfoVo.setAccessToken(DesUtils.encrypt(token));
            authInfoVo.setRefreshToken(DesUtils.encrypt(sysUser.getUserName()));
            authInfoVo.setExpiresIn(desUtils.EXPIRESIN);
            authInfoVo.setRefreshExpires(desUtils.REFRESHEXPIRES);
            authInfoVo.setUserInfoList(sysUser);
            authInfoVo.setSysOrgId(sysUserOrgMapper.findSelfOrgIds(sysUser.getId()));

            Map<String, Object> map = new HashMap<>();
            map.put("accessToken", authInfoVo.getAccessToken());
            map.put("refreshToken", authInfoVo.getRefreshToken());
            map.put("expiresIn", authInfoVo.getExpiresIn() + "");
            map.put("userinfo", new Gson().toJson(sysUser));
            map.put("times", times + "");

            redisUtil.hmset(sysUser.getUserName(), map);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, authInfoVo);
        }
    }


    /**
     * @Description  重新获取token
     *
     * */
    @Override
    public ResponseVO getRefreshToken(AuthInfoVO info) throws BusinessException{
        log.info("请求数据:{}",new Gson().toJson(info.getRefreshToken()));
        ResponseVO responseVo=new ResponseVO();


        info.setTokenType("bearer");
        //有效期7天
        info.setExpiresIn(desUtils.EXPIRESIN);
        info.setRefreshExpires(desUtils.REFRESHEXPIRES);
        String str=DesUtils.decrypt(info.getRefreshToken());

        long times=System.currentTimeMillis();
        info.setAccessToken(DesUtils.encrypt(str+"|"+times));

        info.setRefreshToken(DesUtils.encrypt(str));

        Map<Object, Object> vo=  redisUtil.hmget( str );

        Map<String, Object> map =new HashMap<>(  );
        map.put( "accessToken",info.getAccessToken() );
        map.put( "refreshToken",info.getRefreshToken() );
        map.put( "expiresIn",info.getExpiresIn()+"" );
        map.put( "userinfo",vo.get( "userinfo" ));
        map.put( "times",times+"" );

        redisUtil.hmset( str, map);

        responseVo.setData(info);
        return responseVo;
    }

    /**
     * @Description  获取匿名token
     *
     * */
    @Override
    public ResponseVO getAccessToken(){

        ResponseVO responseVo=new ResponseVO();

        AuthInfoVO authInfoVo=new AuthInfoVO();
        authInfoVo.setTokenType("basic");
        //有效期7天
        authInfoVo.setExpiresIn(desUtils.EXPIRESIN);
        authInfoVo.setRefreshExpires(desUtils.REFRESHEXPIRES);

        authInfoVo.setAccessToken(desUtils.getUuid());

        authInfoVo.setRefreshToken(null);

        responseVo.setData(authInfoVo);
        return responseVo;
    }
}
