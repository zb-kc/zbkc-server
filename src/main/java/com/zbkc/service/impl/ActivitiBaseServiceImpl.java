package com.zbkc.service.impl;

import com.zbkc.common.enums.BusinessTypeEnum;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.model.ActivitiJumpCmd;
import com.zbkc.common.utils.ActivitiUtils;
import com.zbkc.mapper.*;
import com.zbkc.model.dto.YwAgencyBusinessDTO;
import com.zbkc.model.dto.YwContractDTO;
import com.zbkc.model.po.SysUser;
import com.zbkc.model.po.YwAgencyBusinessPO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.vo.YwProcessInfoVO;
import com.zbkc.service.ActivitiBaseService;
import com.zbkc.service.YwAgencyBusinessService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.NativeHistoricProcessInstanceQuery;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.persistence.entity.HistoricTaskInstanceEntity;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.NativeProcessInstanceQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Comment;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskInfo;
import org.activiti.image.ProcessDiagramGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 工作流服务类 实现
 * @author ZB3436.xiongshibao
 * @since 2021-9-16
 */
@Service("activitiBaseService")
@Slf4j
public class ActivitiBaseServiceImpl implements ActivitiBaseService {

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private ProcessEngineConfiguration processEngineConfiguration;

    @Autowired
    private YwAgencyBusinessMapper ywAgencyBusinessMapper;

    @Autowired
    private YwAgencyBusinessService ywAgencyBusinessService;

    @Autowired
    private SysUserMapper sysUserMapper ;

    @Autowired
    private YyContractWyRelationMapper yyContractWyRelationMapper;

    @Autowired
    private WyBasicInfoMapper wyBasicInfoMapper;

    @Autowired
    private YyContractMapper yyContractMapper;

    @Autowired
    private YyUseRegMapper yyUseRegMapper;

    //移交用房申请
    public static final String DELIVERY_HOUSE_APPLY = "DELIVERY_HOUSE_APPLY";
    //合同签署申请-运营类
    public static final String CONTRACT_SIGN_APPLY_AGENCY = "CONTRACT_SIGN_APPLY_AGENCY";
    //合同签署申请-非运营类
    public static final String CONTRACT_SIGN_APPLY_NORMAL = "CONTRACT_SIGN_APPLY_NORMAL";
    //物业资产申报
    public static final String PROPERTY_APPLY = "PROPERTY_APPLY";

    @Override
    public Deployment deploymentProcess(String type) {

        String[] info1 = new String[]{"移交用房申请","process/移交用房申请.bpmn","process/移交用房申请.png"};
        String[] info2 = new String[]{"合同签署申请","process/合同签署申请.bpmn","process/合同签署申请.png"};
        String[] info3 = new String[]{"物业资产申报","process/物业资产申报.bpmn","process/物业资产申报.png"};

        String[] realInfo = new String[3];

        switch (type){
            case "1":
                realInfo = info1;
                break;
            case "2":
                realInfo = info2;
                break;
            case "3":
                realInfo = info3;
                break;
            default:
                break;
        }

       return repositoryService.createDeployment()
                .name(realInfo[0])
                .addClasspathResource(realInfo[1])
                .addClasspathResource(realInfo[2])
                .deploy();

    }

    @Override
    public ProcessInstance startProcessInstance(String processesId, String businessKey, Map<String, Object> variables) {
        return runtimeService.startProcessInstanceById(processesId, businessKey, variables);
    }

    @Override
    public void completeTask(String taskId, Map<String, Object> variables) {
        Task task = this.taskService.createTaskQuery().taskId(taskId).singleResult();
        taskService.complete(task.getId(), variables);
    }

    @Override
    public void deleteTaskProcessInstance(String deploymentId, String deleteReason) {
        repositoryService.deleteDeployment(deploymentId, true);
    }

    @Override
    public ResponseVO turnDownTaskInstance(String businessId, String opinion) {

        //获取流程实例
        ProcessInstance processInstance = this.runtimeService.createProcessInstanceQuery().processInstanceBusinessKey(businessId).singleResult();

        Task task = this.taskService.createTaskQuery().processInstanceBusinessKey(businessId).singleResult();

        if(null == task){
            return new ResponseVO(ErrorCodeEnum.ACTIVITI_TASK_NOT_FOUND ,ErrorCodeEnum.ACTIVITI_TASK_NOT_FOUND.getErrMsg());
        }

        //获取历史节点
        List<HistoricTaskInstance> list = this.historyService.createHistoricTaskInstanceQuery()
                .processDefinitionId(task.getProcessDefinitionId())
                .processInstanceBusinessKey(businessId)
                .orderByTaskCreateTime().asc()
                .list();

        //获取流程定义 找到上级节点
        ProcessDefinition processDefinition = repositoryService.getProcessDefinition(task.getProcessDefinitionId());
        Map<String, String> targetAndSourceRef = ActivitiUtils.getTargetAndSourceRef(repositoryService, (ProcessDefinitionEntity) processDefinition);

        String runningActivityId = processInstance.getActivityId();
        String fatherActivityId = targetAndSourceRef.get(runningActivityId);
        //根据上级节点ID 找到历史节点
        Optional<HistoricTaskInstance> optionalHistoricTaskInstance = list.stream().filter(e -> e.getTaskDefinitionKey().equals(fatherActivityId)).findFirst();


        String processStatusParam = null;
        if(!StringUtils.isEmpty(opinion)){
            if(opinion.contains("材料补正")){
                processStatusParam = "材料补正";
            }
            this.taskService.addComment(task.getId() , task.getProcessInstanceId() , opinion);
        }

        if(optionalHistoricTaskInstance.isPresent()){
            HistoricTaskInstance hisTask = optionalHistoricTaskInstance.get();
            //进而获取流程实例
            ProcessInstance instance = this.runtimeService.createProcessInstanceQuery().processInstanceId(hisTask.getProcessInstanceId()).singleResult();
            //取得流程定义
            ProcessDefinitionEntity definition = (ProcessDefinitionEntity) repositoryService.getProcessDefinition(hisTask.getProcessDefinitionId());
            //获取历史任务的Activity
            ActivityImpl hisActivity = definition.findActivity(hisTask.getTaskDefinitionKey());
            //实现跳转
            ManagementService managementService = this.processEngineConfiguration.getManagementService();
            ActivitiJumpCmd jumpCmd = new ActivitiJumpCmd(instance.getProcessInstanceId(), hisActivity.getId());
            managementService.executeCommand(jumpCmd.setJumpReason("turndown"));
        }

        //更新状态
        int record = updateAgencyBusiness(businessId, processStatusParam);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @Override
    public void deleteProcess(String businessId, String reason) {
        List<HistoricTaskInstance> list = this.historyService.createHistoricTaskInstanceQuery().processInstanceBusinessKey(businessId).list();

        try {
            for (HistoricTaskInstance historicTaskInstance : list) {
                //进而获取流程实例
                ProcessInstance instance = this.runtimeService.createProcessInstanceQuery().processInstanceId(historicTaskInstance.getProcessInstanceId()).singleResult();
                if(null != instance){
                    this.runtimeService.deleteProcessInstance(instance.getId() , reason);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //更新状态
        updateAgencyBusiness(businessId , "流程终止");
    }

    @Override
    public Map<String , String> findProcessCurrentStatusByBusinessKey(String businessKey) {

        Map<String , String> businessKeyProcessStatusMap = new HashMap<>();

        //历史流程实例
        HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
                .processInstanceBusinessKey(businessKey)
                .singleResult();

        if(null != historicProcessInstance && null != historicProcessInstance.getEndTime()){
            //该业务id对应的流程实例 已结束
            businessKeyProcessStatusMap.put(businessKey , "已结束");
            return businessKeyProcessStatusMap;
        }

        //正在运行的流程实例
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId(historicProcessInstance.getId())
                .singleResult();

        businessKeyProcessStatusMap.put(businessKey , processInstance.getName());

        return businessKeyProcessStatusMap;
    }

    @Override
    public Map<String, String> findProcessCurrentStatusByBusinessKeys(List<String> businessKeyList) {
        Map<String , String> businessKeyProcessStatusMap = new HashMap<>();

        StringBuffer replaceString = new StringBuffer();
        for (String s : businessKeyList) {
            replaceString.append(s);
            replaceString.append(",");
        }
        String newReplaceString = "";
        if(replaceString.toString().endsWith(",")){
            newReplaceString = replaceString.substring(0 , replaceString.length() - 1);
        }
        String historySql = "select ID_,PROC_INST_ID_,BUSINESS_KEY_,PROC_DEF_ID_ from `ACT_HI_PROCINST` where END_TIME_ is not null and BUSINESS_KEY_ in (PLACEHOLDERS)";
        historySql = historySql.replace("PLACEHOLDERS" , newReplaceString);
        //先查询历史记录
        NativeHistoricProcessInstanceQuery nativeHistoricProcessInstanceQuery = historyService.createNativeHistoricProcessInstanceQuery().sql(historySql);
        List<HistoricProcessInstance> historicProcessInstanceList = nativeHistoricProcessInstanceQuery.list();

        List<String> historyBusinessKeyList = historicProcessInstanceList.stream().map(HistoricProcessInstance::getBusinessKey).collect(Collectors.toList());
        for (String s : historyBusinessKeyList) {
            businessKeyProcessStatusMap.put(s , "已结束");
        }

        //再查询正在运行的流程实例
        businessKeyList.removeAll(historyBusinessKeyList);
        String runningSql = "";
        NativeProcessInstanceQuery nativeProcessInstanceQuery = runtimeService.createNativeProcessInstanceQuery().sql(runningSql);
        List<ProcessInstance> processInstanceList = nativeProcessInstanceQuery.list();

        for (ProcessInstance processInstance : processInstanceList) {
            businessKeyProcessStatusMap.put(processInstance.getBusinessKey() , processInstance.getName());
        }

        return businessKeyProcessStatusMap;
    }

    @Override
    public ResponseVO findTaskListByBusinessKey(String businessKey) {
        HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
                .processInstanceBusinessKey(businessKey)
                .singleResult();

        String historicProcessInstanceId = historicProcessInstance.getId();

        List<HistoricTaskInstance> historicTaskInstanceList = historyService.createHistoricTaskInstanceQuery()
                .processInstanceId(historicProcessInstanceId)
                .orderByTaskCreateTime()
                .list();

        return new ResponseVO(ErrorCodeEnum.SUCCESS,historicTaskInstanceList);
    }

    @Override
    public List<YwProcessInfoVO> findProcessAllStatusByBusinessKey(String businessId) {

        HistoricProcessInstance historicProcessInstance = this.historyService.createHistoricProcessInstanceQuery()
                .processInstanceBusinessKey(businessId)
                .singleResult();

        if(null == historicProcessInstance){
            return null;
        }

        String historicProcessInstanceId = historicProcessInstance.getId();

        List<HistoricTaskInstance> list = this.historyService.createHistoricTaskInstanceQuery()
                .processInstanceId(historicProcessInstanceId)
                .list();

        List<YwProcessInfoVO> returnList = new ArrayList<>();
        for (HistoricTaskInstance historicTaskInstance : list) {

            String opinion = "";
            String taskId = historicTaskInstance.getId();
            List<Comment> taskComments = this.taskService.getTaskComments(taskId);

            if(!taskComments.isEmpty()){
                opinion = taskComments.get(0).getFullMessage();
            }

            YwProcessInfoVO ywProcessInfoVO = new YwProcessInfoVO();
            ywProcessInfoVO.setBusinessId(businessId);
            ywProcessInfoVO.setProcess(historicTaskInstance.getName());
            String userId = ((TaskInfo) historicTaskInstance).getAssignee();
            SysUser sysUser = this.sysUserMapper.selectById(userId);

            ywProcessInfoVO.setAgentName(null == sysUser ? userId : sysUser.getUserName());
            ywProcessInfoVO.setTaskId(taskId);

            String endTime = "0";
            if(null != historicTaskInstance.getEndTime()){
                endTime = String.valueOf(historicTaskInstance.getEndTime().getTime());
            }

            ywProcessInfoVO.setOperatingTime(endTime);
            ywProcessInfoVO.setDecision(historicProcessInstance.getDeleteReason());
            ywProcessInfoVO.setOpinion(opinion);

            returnList.add(ywProcessInfoVO);
        }

        returnList.sort(new Comparator<YwProcessInfoVO>() {
            @Override
            public int compare(YwProcessInfoVO o1, YwProcessInfoVO o2) {

                String operatingTimeStr1 = o1.getOperatingTime();
                String operatingTimeStr2 = o2.getOperatingTime();

                long operatingTime1 = Long.parseLong(operatingTimeStr1);
                long operatingTime2 = Long.parseLong(operatingTimeStr2);

                if(operatingTime1 == 0){
                    return 1;
                }

                if(operatingTime2 == 0){
                    return -1;
                }

                if(operatingTime1 == operatingTime2){
                    return -1;
                }

                return (int) (operatingTime1 - operatingTime2);
            }
        });

        return returnList;

    }

    /**
     * 根据任务ID和节点ID获取活动节点 <br>
     *
     * @param taskId 任务ID
     * @param activityId
     *            活动节点ID <br>
     *            如果为null或""，则默认查询当前活动节点 <br>
     *            如果为"end"，则查询结束节点 <br>
     *
     * @return 活动节点
     * @throws Exception 找不到流程实例或节点
     */
    private ActivityImpl findActivitiImpl(String taskId, String activityId)
            throws Exception {
        // 取得流程定义
        ProcessDefinitionEntity processDefinition = findProcessDefinitionEntityByTaskId(taskId);

        // 获取当前活动节点ID
        if (StringUtils.isEmpty(activityId)) {
            activityId = findTaskById(taskId).getTaskDefinitionKey();
        }

        // 根据流程定义，获取该流程实例的结束节点
        if ("END".equals(activityId.toUpperCase())) {
            for (ActivityImpl activityImpl : processDefinition.getActivities()) {
                List<PvmTransition> pvmTransitionList = activityImpl
                        .getOutgoingTransitions();
                if (pvmTransitionList.isEmpty()) {
                    return activityImpl;
                }
            }
        }

        // 根据节点ID，获取对应的活动节点
        return processDefinition
                .findActivity(activityId);
    }

    /**
     * 根据任务ID获取流程定义
     *
     * @param taskId
     *            任务ID
     * @return 流程定义
     * @throws Exception 实例不存在异常
     */
    private ProcessDefinitionEntity findProcessDefinitionEntityByTaskId(
            String taskId) throws Exception {
        // 取得流程定义
        ProcessDefinitionEntity processDefinition = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
                .getDeployedProcessDefinition(
                        findTaskById(taskId).getProcessDefinitionId());

        if (processDefinition == null) {
            throw new Exception("流程定义未找到!");
        }

        return processDefinition;
    }

    /**
     * 根据任务ID获得任务实例
     *
     * @param taskId
     *            任务ID
     * @return  任务实例
     * @throws Exception    实例不存在异常
     */
    private TaskEntity findTaskById(String taskId) throws Exception {
        TaskEntity task = (TaskEntity) taskService.createTaskQuery().taskId(
                taskId).singleResult();
        if (task == null) {
            throw new Exception("任务实例未找到!");
        }
        return task;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void startProcessInstance(String businessKey, String applyType) {

        if (StringUtils.isEmpty(applyType)){
            log.info("业务办理-申请类型为空，业务id："+businessKey);
        }

        String bpmnStr = "";

        switch (applyType){
            case DELIVERY_HOUSE_APPLY:
                //移交用房申请
                bpmnStr = "deliveryHouseApply1";
                break;
            case CONTRACT_SIGN_APPLY_AGENCY:
                //合同签署-运营类
                bpmnStr = "contractSignApply2";
                break;
            case CONTRACT_SIGN_APPLY_NORMAL:
                //合同签署-非运营类
                bpmnStr = "contractSignApply1";
                break;
            case PROPERTY_APPLY:
                //物业资产申报
                bpmnStr = "propertyApply";
                break;
            default:
                break;
        }

        List<ProcessDefinition> list = this.repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(bpmnStr)
                .orderByProcessDefinitionVersion()
                .desc()
                .list();

        if(list.isEmpty()){
            throw new RuntimeException("流程定义为空，请输入正确的流程定义名称或联系管理员");
        }

        ProcessDefinition processDefinition = list.get(0);

        this.runtimeService.startProcessInstanceByKey(processDefinition.getKey() , businessKey);
        log.info("业务ID："+businessKey +"已开启流程，流程名称：" + processDefinition.getName());

    }

    @Override
    public void startProcessInstance(YwAgencyBusinessPO ywAgencyBusinessPO) {
        if (0 == ywAgencyBusinessPO.getAgencyType()){
            log.info("业务办理-申请类型为空，业务id："+ ywAgencyBusinessPO.getBusinessType());
        }

        String bpmnStr = "";

        switch (ywAgencyBusinessPO.getAgencyType()){
            case 1:
                //移交用房申请
                bpmnStr = "deliveryHouseApply1";
                break;
            case 3:
                //合同签署-运营类
                bpmnStr = "contractSignApply2";
                break;
            case 4:
                //合同签署-非运营类
                bpmnStr = "contractSignApply1";
                break;
            case 2:
                //物业资产申报
                bpmnStr = "propertyApply";
                break;
            default:
                break;
        }

        List<ProcessDefinition> list = this.repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(bpmnStr)
                .orderByProcessDefinitionVersion()
                .desc()
                .list();

        if(list.isEmpty()){
            throw new RuntimeException("流程定义为空，请输入正确的流程定义名称或联系管理员");
        }

        ProcessDefinition processDefinition = list.get(0);
        ProcessInstance processInstance = this.runtimeService.startProcessInstanceByKey(processDefinition.getKey(), String.valueOf(ywAgencyBusinessPO.getId()));

        HistoricTaskInstance historicTaskInstance = this.historyService.createHistoricTaskInstanceQuery()
                .processInstanceId(processInstance.getId())
                .singleResult();

        this.taskService.setOwner(historicTaskInstance.getId()  , String.valueOf(ywAgencyBusinessPO.getUserId()));

        log.info("业务ID：" + ywAgencyBusinessPO.getId() +"已开启流程，流程名称：" + processDefinition.getName());
    }

    @Override
    public void completeTask(YwProcessInfoVO processInfo) {

        if(null == processInfo || null == processInfo.getBusinessId()){
            log.info("终止流程失败，业务对象为空");
            return;
        }

        Task task = this.taskService.createTaskQuery()
                .processInstanceBusinessKey(processInfo.getBusinessId())
                .singleResult();

        if(StringUtils.isNotEmpty(processInfo.getComment())){
            this.taskService.addComment(task.getId() , task.getProcessInstanceId() , processInfo.getComment());
        }
        this.taskService.complete(task.getId());
    }

    @Override
    public void terminateProcessInstance(YwProcessInfoVO processInfoPO) {

        if(null == processInfoPO || null == processInfoPO.getBusinessId()){
            log.info("终止流程失败，业务对象为空");
            return;
        }

        Task task = this.taskService.createTaskQuery()
                .processInstanceBusinessKey(processInfoPO.getBusinessId())
                .singleResult();


        try {
            ActivityImpl end = findActivitiImpl(task.getId(), "end");

            //进而获取流程实例
            ProcessInstance instance = this.runtimeService.createProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();

            this.taskService.addComment(task.getId() , task.getProcessInstanceId() , processInfoPO.getComment());

            ManagementService managementService = processEngineConfiguration.getManagementService();
            ActivitiJumpCmd jumpCmd = new ActivitiJumpCmd(instance.getId(), end.getId());
            jumpCmd.setJumpReason("terminated");
            managementService.executeCommand(jumpCmd);

            HistoricTaskInstanceEntity historicTaskInstance = (HistoricTaskInstanceEntity)this.historyService.createHistoricTaskInstanceQuery().processInstanceId(instance.getId()).singleResult();
            historicTaskInstance.markEnded("terminated");

        } catch (Exception e) {
            e.printStackTrace();
        }

        //更新状态
        updateAgencyBusiness(processInfoPO.getBusinessId() , "流程终止");

    }

    @Override
    public String getProcessStatus(String businessId) {

        Task task = this.taskService.createTaskQuery()
                .processInstanceBusinessKey(businessId)
                .singleResult();

        if(null == task){
            List<HistoricTaskInstance> list = this.historyService.createHistoricTaskInstanceQuery()
                    .processInstanceBusinessKey(businessId)
                    .orderByTaskId()
                    .desc()
                    .list();

            if(list.isEmpty()){
                return null;
            }

            return list.get(0).getName();
        }

        return task.getName();
    }

    @Override
    public String getProcessImg(String businessKey) {
        //TODO 图片显示异常
        HistoricProcessInstance historicProcessInstance = this.historyService.createHistoricProcessInstanceQuery().processInstanceBusinessKey(businessKey).singleResult();

        if(null == historicProcessInstance){
            return ErrorCodeEnum.PICTURE_PROCESS_NOT_FOUND.getErrMsg();
        }

        String processInstanceId = historicProcessInstance.getId();

        //获取流程图
        BpmnModel bpmnModel = repositoryService.getBpmnModel(historicProcessInstance.getProcessDefinitionId());

        ProcessDiagramGenerator diagramGenerator = processEngineConfiguration.getProcessDiagramGenerator();
        ProcessDefinitionEntity definitionEntity = (ProcessDefinitionEntity) repositoryService.getProcessDefinition(historicProcessInstance.getProcessDefinitionId());

        List<HistoricActivityInstance> highLightedActivitList = historyService.createHistoricActivityInstanceQuery().processInstanceId(processInstanceId).list();

        //高亮环节id集合
        List<String> highLightedActivitis = new ArrayList<>();

        if(null != historicProcessInstance){
            //剔除驳回的节点 当前节点不高亮显示
            List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery().processInstanceBusinessKey(businessKey).list();

            list.sort(new Comparator<HistoricTaskInstance>() {
                @Override
                public int compare(HistoricTaskInstance o1, HistoricTaskInstance o2) {
                    String id1 = o1.getId();
                    String id2 = o2.getId();

                    long l1 = Long.parseLong(id1);
                    long l2 = Long.parseLong(id2);

                    return (l1 - l2 > 0) ? 1 : -1;
                }
            });

            List<String> deleteTaskInstance = new ArrayList<>();

            Map<String , String > map = new HashMap<>();
            for (HistoricTaskInstance historicTaskInstance : list) {
                map.put(historicTaskInstance.getTaskDefinitionKey() , historicTaskInstance.getDeleteReason());
            }

            for(Map.Entry<String , String> m : map.entrySet()){
                String deleteReason = m.getValue();
                if (!"completed".equals(deleteReason)){
                    deleteTaskInstance.add(m.getKey());
                }
            }

            highLightedActivitList.removeIf(next -> deleteTaskInstance.contains(next.getActivityId()));
        }

        //高亮线路id集合
        List<String> highLightedFlows = ActivitiUtils.getHighLightedFlows(definitionEntity, highLightedActivitList);

        for (HistoricActivityInstance tempActivity : highLightedActivitList) {
            String activityId = tempActivity.getActivityId();
            highLightedActivitis.add(activityId);
        }

        //配置字体
        InputStream imageStream = diagramGenerator.generateDiagram(bpmnModel, "png", highLightedActivitis, highLightedFlows, "宋体", "微软雅黑", "黑体", null, 2.0);

        BufferedImage bi = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            bi = ImageIO.read(imageStream);
            ImageIO.write(bi, "png", bos);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //转换成字节
        byte[] bytes = bos.toByteArray();
        BASE64Encoder encoder = new BASE64Encoder();
        //转换成base64串
        String pngBase64 = encoder.encodeBuffer(bytes);
        //删除 \r\n
        pngBase64 = pngBase64.replaceAll("\n", "").replaceAll("\r", "");

        try {
            bos.close();
            imageStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return pngBase64;
    }

    @Override
    public void completeTask(String businessKey, String opinion) {
        Task task = this.taskService.createTaskQuery()
                .processInstanceBusinessKey(businessKey)
                .singleResult();

        if(null == task){
            return;
        }

        if(StringUtils.isNotEmpty(opinion)){
            this.taskService.addComment(task.getId() , task.getProcessInstanceId() , opinion);
        }

        this.taskService.complete(task.getId());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addAgencyBusinessRecord(YwAgencyBusinessPO ywAgencyBusinessPO) {

        //开启工作流
        this.startProcessInstance(ywAgencyBusinessPO);

        //完成申请节点
        YwProcessInfoVO ywProcessInfoVO = new YwProcessInfoVO();
        ywProcessInfoVO.setBusinessId(String.valueOf(ywAgencyBusinessPO.getId()));
        this.completeTask(ywProcessInfoVO);

        //设置流程状态
        String processStatus = this.getProcessStatus(String.valueOf(ywAgencyBusinessPO.getId()));
        ywAgencyBusinessPO.setProcess(processStatus);

        //新增申请记录
        int id = this.ywAgencyBusinessMapper.insert(ywAgencyBusinessPO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int completeTask(YwContractDTO ywContractDTO) {
        this.completeTask(ywContractDTO.getBusinessId() , ywContractDTO.getOpinion());

        /**
         * 根据业务id判断节点阶段，如果是第二阶段，则需要更新合同表信息，否则不更新
         */
        String processStatusParam = null;
        Execution execution = this.runtimeService.createExecutionQuery().processInstanceBusinessKey(ywContractDTO.getBusinessId()).singleResult();
        if(null != execution){

            YwAgencyBusinessDTO ywAgencyBusinessDTO = new YwAgencyBusinessDTO();
            ywAgencyBusinessDTO.setId(Long.parseLong(ywContractDTO.getBusinessId()));
            String secondActivityId = this.getSecondActivityId(ywAgencyBusinessDTO);

            if(secondActivityId.equals(execution.getActivityId())){
                this.ywAgencyBusinessService.updateYyContractInProgress(ywContractDTO);
            }
        }else{
            //已办结状态
            processStatusParam = BusinessTypeEnum.PROCESS_COMPLETE.getName();
            //业务流全部完成最终处理
            this.ywAgencyBusinessService.businessFinallyHandler(ywContractDTO);
        }

        return updateAgencyBusiness(ywContractDTO.getBusinessId(), processStatusParam);
    }

    @Override
    public void deleteAllProgress() {
        List<Execution> list = this.runtimeService.createExecutionQuery().list();

        for (Execution execution : list) {
            this.runtimeService.deleteProcessInstance(execution.getProcessInstanceId() , "deleteAll");
        }
    }

    @Override
    public String getSecondActivityId(YwAgencyBusinessDTO ywAgencyBusinessDTO) {
        String secondActivityId = "";
        ProcessInstance processInstance = this.runtimeService.createProcessInstanceQuery().processInstanceBusinessKey(String.valueOf(ywAgencyBusinessDTO.getId())).singleResult();
        String processDefinitionId = processInstance.getProcessDefinitionId();

        BpmnModel model = this.repositoryService.getBpmnModel(processDefinitionId);
        if(model != null) {
            Collection<FlowElement> flowElements = model.getMainProcess().getFlowElements();
            Map<String, String> nodeMap = new HashMap<>();
            String firstFlow = "";
            List<SequenceFlow> allFlow = new ArrayList<>();
            Map<String, Integer> flowNumberMap = new HashMap<>();

            for (FlowElement e : flowElements) {
                if (e instanceof UserTask) {
                    List<SequenceFlow> incomingFlows = ((UserTask) e).getIncomingFlows();
                    List<SequenceFlow> outgoingFlows = ((UserTask) e).getOutgoingFlows();
                    allFlow.addAll(incomingFlows);
                    allFlow.addAll(outgoingFlows);
                }
            }

            for (SequenceFlow flow : allFlow) {
                String sourceRef = flow.getSourceRef();
                String targetRef = flow.getTargetRef();
                nodeMap.put(sourceRef, targetRef);

                Integer number = flowNumberMap.get(sourceRef);
                if (null == number) {
                    number = 0;
                }
                number += 1;
                flowNumberMap.put(sourceRef, number);

                Integer number2 = flowNumberMap.get(targetRef);
                if (null == number2) {
                    number2 = 0;
                }
                number2 += 1;
                flowNumberMap.put(targetRef, number2);
            }

            for (Map.Entry<String, Integer> entry : flowNumberMap.entrySet()) {
                if (entry.getValue().equals(1) && nodeMap.containsKey(entry.getKey())) {
                    firstFlow = entry.getKey();
                    break;
                }
            }

            secondActivityId = nodeMap.get(nodeMap.get(firstFlow));
        }
        return secondActivityId;
    }

    @Override
    public Boolean isSecondActivity(String businessId) {

        Execution execution = this.runtimeService.createExecutionQuery().processInstanceBusinessKey(businessId).singleResult();
        if(null != execution){

            YwAgencyBusinessDTO ywAgencyBusinessDTO = new YwAgencyBusinessDTO();
            ywAgencyBusinessDTO.setId(Long.parseLong(businessId));
            String secondActivityId = this.getSecondActivityId(ywAgencyBusinessDTO);

            if(secondActivityId.equals(execution.getActivityId())){
                return true;
            }
        }
        return false;
    }

    /**
     * 根据业务id 更新 待办业务表
     * @param businessId 业务id
     */
    private int updateAgencyBusiness(String businessId , String processStatusParam) {

        YwAgencyBusinessPO ywAgencyBusinessPO = new YwAgencyBusinessPO();

        //更新流程状态
        String processStatus = null;
        if(StringUtils.isNotEmpty(processStatusParam)){
            processStatus = processStatusParam;
            if("材料补正".equals(processStatus)){
                ywAgencyBusinessPO.setRecordStatus(Byte.parseByte(BusinessTypeEnum.PROCESS_NEW_RECORD.getCode().toString()));
            }

            if(BusinessTypeEnum.PROCESS_COMPLETE.getName().equals(processStatusParam)){
                ywAgencyBusinessPO.setRecordStatus(Byte.parseByte(BusinessTypeEnum.PROCESS_COMPLETE.getCode().toString()));
            }
        }else{
            processStatus = this.getProcessStatus(businessId);
        }

        ywAgencyBusinessPO.setId(Long.parseLong(businessId));
        ywAgencyBusinessPO.setProcess(processStatus);
        int record = this.ywAgencyBusinessMapper.updateById(ywAgencyBusinessPO);
        return record;
    }
}
