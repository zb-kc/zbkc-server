package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.model.po.WyFloorInfo;
import com.zbkc.mapper.WyFloorInfoMapper;
import com.zbkc.model.po.WyProRightReg;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.WyFloorInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 物业楼层信息表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-10
 */
@Service
public class WyFloorInfoServiceImpl  implements WyFloorInfoService {
    @Autowired
    private WyFloorInfoMapper wyFloorInfoMapper;


    @Override
    public ResponseVO pageByName(Integer p, Integer size, String name) {
        List<WyFloorInfo> wyFloorInfos = wyFloorInfoMapper.pageByName((p - 1) * size, size, name);
        PageVO<WyFloorInfo> vo = new PageVO<>();
        vo.setList(wyFloorInfos);
        vo.setTotal(wyFloorInfoMapper.selectByNameTotal(name).size());
        vo.setP(p);
        vo.setSize(size);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }
    @Override
    public ResponseVO insert(WyFloorInfo entity) {
        long ct = System.currentTimeMillis();
        Timestamp timestamp = TimeUtils.LongToDate(ct);
        entity.setCreateTime(timestamp);
        int i = wyFloorInfoMapper.insert(entity);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO updateById(WyFloorInfo entity) {
        int i = wyFloorInfoMapper.updateById(entity);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO deleteById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO deleteByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO delete(Wrapper<WyFloorInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO deleteBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }



    @Override
    public ResponseVO update(WyFloorInfo entity, Wrapper<WyFloorInfo> updateWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO selectBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO selectOne(Wrapper<WyFloorInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectCount(Wrapper<WyFloorInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectList(Wrapper<WyFloorInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectMaps(Wrapper<WyFloorInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectObjs(Wrapper<WyFloorInfo> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<WyFloorInfo>> ResponseVO selectPage(P page, Wrapper<WyFloorInfo> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<Map<String, Object>>> ResponseVO selectMapsPage(P page, Wrapper<WyFloorInfo> queryWrapper) {
        return null;
    }


}
