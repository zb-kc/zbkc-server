package com.zbkc.service.impl;

import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.enums.ExportEnum;
import com.zbkc.common.utils.ExcelUtils;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.export.ExportSmsTemplateDTO;
import com.zbkc.model.dto.WySysMsgHomeDTO;
import com.zbkc.model.po.*;
import com.zbkc.mapper.WySysMsgMapper;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.WySysMsgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 短信模板管理表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-11
 */
@Service
public class WySysMsgServiceImpl extends ServiceImpl<WySysMsgMapper, WySysMsg> implements WySysMsgService {
    @Autowired
    private WySysMsgMapper wySysMsgMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO addMsg(WySysMsg dto1) {
        WySysMsg dto = new WySysMsg();
        dto=dto1;

        WySysMsg info = new WySysMsg();
        info.setName(dto.getName());
        List<WySysMsg> msgList = wySysMsgMapper.getAllMsgName();
        for (WySysMsg msg : msgList) {
            if(dto.getName().equals(msg.getName())){
                return new ResponseVO(ErrorCodeEnum.ALREAD_EXIT,null);
            }
        }
        info.setType(dto.getType());
        info.setStatus(dto.getStatus());
        info.setWord(dto.getWord());
        info.setRemark(dto.getRemark());
        info.setCreateTime(TimeUtils.getDateString(new Date()));
        info.setUserId(dto.getUserId());

        int i = wySysMsgMapper.addMsg(info);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO getList(WySysMsgHomeDTO dto) {
        if (dto.getP() <= 0) {
            return new ResponseVO(ErrorCodeEnum.SUCCESS, null);
        }
        dto.setP((dto.getP() - 1) * dto.getSize());
        List<WySysMsgHomeList> list = wySysMsgMapper.queryWySysMsgHomeList(dto);
        PageVO<WySysMsgHomeList> vo = new PageVO<>();
        vo.setList(list);
        vo.setP(dto.getP() + 1);
        vo.setSize(dto.getSize());
        vo.setTotal(wySysMsgMapper.queryWySysMsgHomeListTotal(dto).size());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO seeRow(String id) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS, wySysMsgMapper.seeRow(id));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO updMsg(WySysMsg dto) {
        dto.setUpdateTime(TimeUtils.getDateString(new Date()));
        List<WySysMsg> msgList = wySysMsgMapper.getAllMsgName();
        for (WySysMsg msg : msgList) {
            if (dto.getName().equals(msg.getName()) && !dto.getId().equals(msg.getId())) {
                return new ResponseVO(ErrorCodeEnum.ALREAD_EXIT, null);
            }
        }
        int i = wySysMsgMapper.updMsg(dto);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, i);
    }

    @Override
    public ResponseVO export(ExportSmsTemplateDTO dto, HttpServletResponse response) {
        List<WySysMsgHomeList> wySysMsgHomeLists = wySysMsgMapper.queryWySysMsgHomeListTotal(dto.getWySysMsgHomeDTO());
        for (WySysMsgHomeList wySysMsgHomeList : wySysMsgHomeLists) {
            //防止数据库返回为null导致报错
            wySysMsgHomeList.setStatus(wySysMsgHomeList.getStatus()==null?0:wySysMsgHomeList.getStatus());
            if(wySysMsgHomeList.getStatus()==1){
                wySysMsgHomeList.setStatusStr(ExportEnum.START_USING.getName());
            }
            if(wySysMsgHomeList.getStatus()==2){
                wySysMsgHomeList.setStatusStr(ExportEnum.PROHIBIT_USING.getName());
            }
        }
        Map<String, String> keyMap = dto.getKeyMap();

        //需要显示的字段名
        //字段名对应的中文名
        String[] headers = null;
        String[] needShowKeyArr = null;

        if(StringUtils.isEmpty(dto.getNeedShowKeys())){
            //不传getNeedShowKeys 显示所有字段
            Set<String> keys = keyMap.keySet();
            needShowKeyArr = keys.toArray(new String[keys.size()]);
            headers = new String[keys.size()];

        }else{
            needShowKeyArr = dto.getNeedShowKeys().split(",");
            //字段名对应的中文名
            headers = new String[needShowKeyArr.length];
        }

        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        try {
            ExcelUtils.getExcel(response , headers ,needShowKeyArr, wySysMsgHomeLists, "smsList.xls");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
