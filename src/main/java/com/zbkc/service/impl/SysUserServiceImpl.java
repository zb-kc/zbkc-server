package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.common.utils.ToolUtils;
import com.zbkc.mapper.SysMenuMapper;
import com.zbkc.mapper.SysUserMapper;
import com.zbkc.mapper.SysUserOrgMapper;
import com.zbkc.mapper.SysUserRoleMapper;
import com.zbkc.model.dto.GrantOrgDTO;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.vo.UserVO;
import com.zbkc.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 系统用户表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-07-27
 */
@Service
@Slf4j
public class SysUserServiceImpl implements SysUserService,Serializable{

    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysUserOrgMapper sysUserOrgMapper;
    @Autowired
    private SysMenuMapper sysMenuMapper;
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    public ResponseVO selectById(Serializable id ) {
        log.info("请求数据:{}",new Gson().toJson(id));
        SysUser sysUser = sysUserMapper.selectById(id);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,sysUser);
    }

    @Override
    public <P extends IPage<SysUser>> ResponseVO selectPage(P page, Wrapper<SysUser> queryWrapper) {
        log.info("请求数据:{}",new Gson().toJson(page));
        P p = sysUserMapper.selectPage(page, queryWrapper);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,p);
    }

    @Override
    public ResponseVO insert(SysUser entity) {
        List<SysUser> sysUsers = sysUserMapper.queryByUserName(entity.getUserName());
        if (sysUsers.size() != 0) {
            return new ResponseVO(ErrorCodeEnum.ALREAD_EXIT, null);
        } else {
            String userName = entity.getUserName();
            String userPwd = entity.getPwd();
            Boolean name = new ToolUtils().RegName(userName);
            Boolean pwd = new ToolUtils().RegPwd(userPwd);
            if(name&&pwd){
                long ct = System.currentTimeMillis();
                Timestamp timestamp = TimeUtils.LongToDate(ct);
                entity.setCreateTime(timestamp);
                int i = sysUserMapper.insert(entity);
                return new ResponseVO(ErrorCodeEnum.SUCCESS, i);
            }else{
                return new ResponseVO(ErrorCodeEnum.REGULARNOTACCORD,null);
            }
        }
    }

    @Override
    public ResponseVO updateById(SysUser entity) {
        if(entity.getUserName()==null||entity.getUserName()==""){
        long ct = System.currentTimeMillis();
        Timestamp timestamp = TimeUtils.LongToDate(ct);
        entity.setUpdateTime(timestamp);
        int i = sysUserMapper.updateById(entity);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
        }else {
            return new ResponseVO(ErrorCodeEnum.INCHANGEWERECHANGE,null);
        }
    }


    @Override
    public ResponseVO status(Long id,Boolean status) {
        if(status) {
            Integer open = sysUserMapper.statusOpen(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS,open);
        }else {
            Integer close = sysUserMapper.statusClose(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, close);
        }
    }

    @Override
    public ResponseVO pageByName(Integer p, Integer size, String name) {
        int queryStartNumber = (p - 1) * size;
        List<UserVO> list=sysUserMapper.pageByName(queryStartNumber,size,name);
        for (UserVO userVO : list) {
            List<SysOrg> sysOrgs = sysUserMapper.selectByUserId(userVO.getId());
            userVO.setSysOrgs(sysOrgs);
            userVO.setSysRole(sysUserMapper.selectSelfRole(userVO.getId()));
        }
        PageVO<UserVO> vo = new PageVO<>();
        vo.setList( list );
        vo.setTotal(  sysUserMapper.selectByNameTotal(name ).size());
        vo.setP(p);
        vo.setSize(size);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO grantOrgs(GrantOrgDTO dto) {
        QueryWrapper<SysUserOrg> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",dto.getId());
        sysUserOrgMapper.delete(wrapper);
        List<Long> ids = dto.getOrgsIds();
        for(int i=0;i<ids.size();i++){
            SysUserOrg userOrg = new SysUserOrg();
            userOrg.setUserId(dto.getId());
            userOrg.setOrgId(ids.get(i));
            sysUserOrgMapper.insert(userOrg);
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS,null);
    }

    @Override
    public ResponseVO findSelfMenu(Long userId) {

        List<SysMenu> sysMenuList=sysMenuMapper.getUserMenu( userId, (long) 0 );

        //根据父节点 查询子节点及其按钮
        List<Long> sysMenuId = sysMenuList.stream().map(SysMenu::getId).collect(Collectors.toList());
        List<SysMenu> childrenList = this.sysMenuMapper.selectMenuByParentId(userId, sysMenuId);
        Map<Long , List<SysMenu>> pidAndChildrenMap = new HashMap<>();
        if(!childrenList.isEmpty()) {
            for (SysMenu sysMenu : childrenList) {
                Long pid = sysMenu.getPid();

                List<SysMenu> list = pidAndChildrenMap.get(pid);
                if(null == list){
                    list = new ArrayList<>();
                }
                list.add(sysMenu);

                pidAndChildrenMap.put( pid , list);
            }
        }

        if(!pidAndChildrenMap.isEmpty()){
            for (SysMenu sysMenu : sysMenuList) {
                sysMenu.setSysMenusList(pidAndChildrenMap.get(sysMenu.getId()));
            }
        }

        return new ResponseVO(ErrorCodeEnum.SUCCESS,sysMenuList);
    }

    private List<SysMenu> sysMenusList(List<SysMenu> sysMenus,Long userId){
        for (int i = 0; i< sysMenus.size(); i++){
            List<SysMenu> list = sysMenuMapper.getUserMenu(userId,sysMenus.get(i).getId());
            sysMenus.get( i ).setSysMenusList(list);
            sysMenusList(sysMenus.get(i).getSysMenusList(),userId);

        }
        return sysMenus;

    }


    @Override
    public ResponseVO findSelfOrgIds(Long userId) {
        List<Long> selfOrgIds = sysUserOrgMapper.findSelfOrgIds(userId);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,selfOrgIds);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO updUserRole(Long userId, Long roleId) {

        Integer i = 0;
        QueryWrapper<SysUserRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id" , userId);
        SysUserRole sysUserRole = this.sysUserRoleMapper.selectOne(queryWrapper);
        if(null == sysUserRole){
            sysUserRole = new SysUserRole();
            sysUserRole.setCreateTime(new Timestamp(System.currentTimeMillis()));
            sysUserRole.setStatus(1);
            sysUserRole.setRoleId(roleId);
            sysUserRole.setUserId(userId);
            i  = this.sysUserRoleMapper.insert(sysUserRole);
        }else{
            i = sysUserMapper.updUserRole(userId, roleId);
        }

        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO insertRecord(SysUser sysUser) {

        int record;
        int roleRecord = 0;
        int orgRecord = 0;

        record = this.sysUserMapper.insert(sysUser);

        Long sysOrgId = sysUser.getSysOrgId();
        Long sysRoleId = sysUser.getSysRoleId();

        /**
         * 对新增用户进行组织、角色添加
         */
        if(null != sysRoleId){
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setCreateTime(new Timestamp(System.currentTimeMillis()));
            sysUserRole.setStatus(1);
            sysUserRole.setRoleId(sysRoleId);
            sysUserRole.setUserId(sysUser.getId());
            roleRecord = this.sysUserRoleMapper.insert(sysUserRole);
        }

        if(null != sysOrgId){
            SysUserOrg userOrg = new SysUserOrg();
            userOrg.setUserId(sysUser.getId());
            userOrg.setOrgId(sysOrgId);
            orgRecord = this.sysUserOrgMapper.insert(userOrg);
        }


        int totalRecord = record + roleRecord + orgRecord;
        if(0 == totalRecord){
            return new ResponseVO(ErrorCodeEnum.INSERT_RECORD_ERROR , ErrorCodeEnum.INSERT_RECORD_ERROR.getErrMsg());
        }

        return new ResponseVO(ErrorCodeEnum.SUCCESS , totalRecord);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO updateRecord(SysUser data) {
        int record = 0;
        record += this.sysUserMapper.updateById(data);

        Long sysRoleId = data.getSysRoleId();
        Long sysOrgId = data.getSysOrgId();

        QueryWrapper<SysUserOrg> deleteSysUserOrg = new QueryWrapper<>();
        deleteSysUserOrg.eq("user_id" , data.getId());
        record += this.sysUserOrgMapper.delete(deleteSysUserOrg);

        SysUserOrg sysUserOrg = new SysUserOrg();
        sysUserOrg.setId(IdWorker.getId());
        sysUserOrg.setUserId(data.getId());
        sysUserOrg.setOrgId(sysOrgId);
        record += this.sysUserOrgMapper.insert(sysUserOrg);

        QueryWrapper<SysUserRole> deleteSysUserRole = new QueryWrapper<>();
        deleteSysUserRole.eq("user_id" , data.getId());
        record += this.sysUserRoleMapper.delete(deleteSysUserRole);

        SysUserRole sysUserRole = new SysUserRole();
        sysUserRole.setUserId(data.getId());
        sysUserRole.setRoleId(sysRoleId);
        record += this.sysUserRoleMapper.insert(sysUserRole);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }


    @Override
    public List<SysUser> queryByUserName(String userName) {
        List<SysUser> sysUsers = sysUserMapper.queryByUserName(userName);
        return sysUsers;
    }


    @Override
    public ResponseVO deleteById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO deleteByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO delete(Wrapper<SysUser> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO deleteBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }


    @Override
    public ResponseVO update(SysUser entity, Wrapper<SysUser> updateWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO selectOne(Wrapper<SysUser> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectCount(Wrapper<SysUser> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectList(Wrapper<SysUser> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectMaps(Wrapper<SysUser> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectObjs(Wrapper<SysUser> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<Map<String, Object>>> ResponseVO selectMapsPage(P page, Wrapper<SysUser> queryWrapper) {
        return null;
    }


}
