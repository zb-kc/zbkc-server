package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.ExcelUtils;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.common.utils.ToolUtils;
import com.zbkc.mapper.*;
import com.zbkc.model.dto.*;
import com.zbkc.model.dto.export.ExportCommissioneOperationDTO;
import com.zbkc.model.dto.homeList.YyCommissioneOperationHomeList;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.vo.YyCommissioneOperationVo;
import com.zbkc.service.YyCommissioneOperationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 运营管理——委托运营 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-10
 */
@Service
@Slf4j
public class YyCommissioneOperationServiceImpl extends ServiceImpl<YyCommissioneOperationMapper, YyCommissioneOperation> implements YyCommissioneOperationService {

    @Autowired
    private YyCommissioneOperationMapper yyCommissioneOperationMapper;
    @Autowired
    private YyUseRegFilePathMapper yyUseRegFilePathMapper;
    @Autowired
    private ToolUtils toolUtils;
    @Autowired
    private WyBasicInfoMapper wyBasicInfoMapper;
    @Autowired
    private SysFilePathMapper sysFilePathMapper;
    @Autowired
    private SysAttachmentMapper sysAttachmentMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO addCommissioneOperation(YyCommissioneOperationDTO dto1) {
        YyCommissioneOperationDTO dto = new YyCommissioneOperationDTO();
        dto=dto1;

        long startTime = System.currentTimeMillis();
        YyCommissioneOperation info = new YyCommissioneOperation();
        info.setWyBasicInfoId(dto.getWyBasicInfoId());
        info.setStartTime(toolUtils.strFormatTimetamp(dto.getStartTime()));
        info.setEndTime(toolUtils.strFormatTimetamp(dto.getEndTime()));
        info.setOperationUnit(dto.getOperationUnit());
        info.setCreditCode(dto.getCreditCode());
        info.setLegalName(dto.getLegalName());
        info.setLegalPhone(dto.getLegalPhone());
        info.setLegalCode(dto.getLegalCode());
        info.setLegalType(dto.getLegalType());
        info.setAgentName(dto.getAgentName());
        info.setAgentPhone(dto.getAgentPhone());
        info.setAgentType(dto.getAgentType());
        info.setAgentCode(dto.getAgentCode());
        info.setOperationMoney(dto.getOperationMoney());
        info.setRemark(dto.getRemark());
        info.setUserId(dto.getUserId());
        info.setCreateTime(new Timestamp(System.currentTimeMillis()));

        List<SysFilePath> insertSysFilePathList = new ArrayList<>();
        List<SysAttachment> insertSysAttachmentList = new ArrayList<>();

        //新增自定义文件
        long customFileId = IdWorker.getId();
        info.setCustomFileId(customFileId);
        if (null != dto.getCustomFileMap()) {
            List<String> keyList = dto.getCustomFileMap().keySet().stream().filter(k -> k != null).collect(Collectors.toList());
            for (String key : keyList) {
                List<SysFilePath> sysFilePathList = dto.getCustomFileMap().get(key);
                for (SysFilePath sysFilePath : sysFilePathList) {
                    SysFilePath file = new SysFilePath();
                    long fileId = IdWorker.getId();
                    file.setId(fileId);
                    file.setFileSize(sysFilePath.getFileSize());
                    file.setPath(sysFilePath.getPath());
                    file.setFileName(sysFilePath.getFileName());
                    file.setDbFileId(customFileId);
                    insertSysFilePathList.add(file);
                    SysAttachment sysAttachment = new SysAttachment();
                    sysAttachment.setId(IdWorker.getId());
                    sysAttachment.setSysFilePathId(fileId);
                    sysAttachment.setName(key);
                    insertSysAttachmentList.add(sysAttachment);
                }
            }
        }
        //添加使用批准文件
        long useApprovalFileId = IdWorker.getId();
        info.setUseApprovalFileId(useApprovalFileId);
        if (null != dto.getUseApprovalFileList()) {
            for (SysFilePath useApprovalFile : dto.getUseApprovalFileList()) {
                SysFilePath file = new SysFilePath();
                file.setPath(useApprovalFile.getPath());
                file.setDbFileId(useApprovalFileId);
                file.setFileName(useApprovalFile.getFileName());
                file.setFileSize(useApprovalFile.getFileSize());
                file.setId(IdWorker.getId());
                insertSysFilePathList.add(file);
            }
        }
        //添加合同附件
        long contractId = IdWorker.getId();
        info.setContractFileId(contractId);
        if (null != dto.getContractFileList()) {
            for (SysFilePath contractFile : dto.getContractFileList()) {
                SysFilePath file = new SysFilePath();
                file.setId(IdWorker.getId());
                file.setFileName(contractFile.getFileName());
                file.setFileSize(contractFile.getFileSize());
                file.setDbFileId(contractId);
                file.setPath(contractFile.getPath());
                insertSysFilePathList.add(file);
            }
        }
        if(!insertSysFilePathList.isEmpty()){
            this.sysFilePathMapper.insertBatch(insertSysFilePathList);
        }
        if(!insertSysAttachmentList.isEmpty()){
            this.sysAttachmentMapper.insertBatch(insertSysAttachmentList);
        }

        int i = yyCommissioneOperationMapper.addCommissioneOperation(info);

        long endTime = System.currentTimeMillis();
        log.info("程序运行时间： " + (endTime - startTime) + "ms");
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO getList(YyCommissioneOperationHomeDTO dto) {
        if(dto.getP()<=0){
            return new ResponseVO(ErrorCodeEnum.SUCCESS,null);
        }
        dto.setP( (dto.getP()-1)*dto.getSize() );
        PageVO<YyCommissioneOperationHomeList> vo = new PageVO<>();
        List<YyCommissioneOperationHomeList> list = yyCommissioneOperationMapper.queryCommissioneOperationHomeList(dto);
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());
        vo.setList(list);
        vo.setTotal(yyCommissioneOperationMapper.queryCommissioneOperationHomeListTotal(dto).size());


        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO seeRow(Long id) {

        YyCommissioneOperationVo vo = yyCommissioneOperationMapper.seeRow(id);
        List<SysFilePath1> file1 = sysFilePathMapper.getBydbFileId(vo.getContractFileId()==null?0:vo.getContractFileId());
        List<SysFilePath1> file2 = sysFilePathMapper.getBydbFileId(vo.getUseApprovalFileId()==null?0:vo.getUseApprovalFileId());
        vo.setUseApprovalFileList(file2);
        vo.setContractFileList(file1);

        List<String> nameList = sysFilePathMapper.getName(vo.getCustomFileId());
        Map<String, List<SysFilePath1>> customFileMap = new HashMap<>();
        for (String name : nameList) {
            customFileMap.put(name,sysFilePathMapper.getCustFile(vo.getCustomFileId(),name));
        }
        vo.setCustomFileMap(customFileMap);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, vo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO updCommissioneOperation(YyCommissioneOperationDTO dto1) {
        YyCommissioneOperationDTO dto = new YyCommissioneOperationDTO();
        dto=dto1;

        long startTime = System.currentTimeMillis();
        YyCommissioneOperation info = new YyCommissioneOperation();
        info.setId(dto.getId());
        info.setWyBasicInfoId(dto.getWyBasicInfoId());
        info.setEndTime(toolUtils.strFormatTimetamp(dto.getEndTime()));
        info.setStartTime(toolUtils.strFormatTimetamp(dto.getStartTime()));
        info.setCreditCode(dto.getCreditCode());
        info.setOperationUnit(dto.getOperationUnit());
        info.setLegalName(dto.getLegalName());
        info.setLegalPhone(dto.getLegalPhone());
        info.setLegalType(dto.getLegalType());
        info.setLegalCode(dto.getLegalCode());
        info.setAgentName(dto.getAgentName());
        info.setAgentPhone(dto.getAgentPhone());
        info.setAgentCode(dto.getAgentCode());
        info.setAgentType(dto.getAgentType());
        info.setOperationMoney(dto.getOperationMoney());
        info.setRemark(dto.getRemark());
        info.setUserId(dto.getUserId());
        info.setUpdateTime(new Timestamp(System.currentTimeMillis()));

        List<SysFilePath> insertSysFilePathList = new ArrayList<>();
        List<SysAttachment> insertSysAttachmentList = new ArrayList<>();
        //修改使用批准文件
        long useApprovalFileId = IdWorker.getId();
        info.setUseApprovalFileId(useApprovalFileId);
        if (null != dto.getUseApprovalFileList()) {
            for (SysFilePath useApprovalFile : dto.getUseApprovalFileList()) {
                SysFilePath file = new SysFilePath();
                file.setDbFileId(useApprovalFileId);
                file.setFileName(useApprovalFile.getFileName());
                file.setPath(useApprovalFile.getPath());
                file.setId(IdWorker.getId());
                file.setFileSize(useApprovalFile.getFileSize());
                insertSysFilePathList.add(file);
            }
        }
        //修改合同附件
        long contractId = IdWorker.getId();
        info.setContractFileId(contractId);
        if (null != dto.getContractFileList()) {
            for (SysFilePath contractFile : dto.getContractFileList()) {
                SysFilePath file = new SysFilePath();
                file.setId(IdWorker.getId());
                file.setDbFileId(contractId);
                file.setFileName(contractFile.getFileName());
                file.setFileSize(contractFile.getFileSize());
                file.setPath(contractFile.getPath());
                insertSysFilePathList.add(file);
            }
        }
        //修改自定义附件
        long customFileId = IdWorker.getId();
        info.setCustomFileId(customFileId);
        if (null != dto.getCustomFileMap()) {
            List<String> keyList = dto.getCustomFileMap().keySet().stream().filter(k -> k != null).collect(Collectors.toList());
            for (String key : keyList) {
                List<SysFilePath> sysFilePathList = dto.getCustomFileMap().get(key);
                for (SysFilePath sysFilePath : sysFilePathList) {
                    SysFilePath file = new SysFilePath();
                    long fileId = IdWorker.getId();
                    file.setId(fileId);
                    file.setFileSize(sysFilePath.getFileSize());
                    file.setDbFileId(customFileId);
                    file.setFileName(sysFilePath.getFileName());
                    file.setPath(sysFilePath.getPath());
                    SysAttachment sysAttachment = new SysAttachment();
                    sysAttachment.setName(key);
                    sysAttachment.setSysFilePathId(fileId);
                    sysAttachment.setId(IdWorker.getId());
                    insertSysFilePathList.add(file);
                    insertSysAttachmentList.add(sysAttachment);
                }
            }
        }

        if(!insertSysFilePathList.isEmpty()){
            this.sysFilePathMapper.insertBatch(insertSysFilePathList);
        }
        if(!insertSysAttachmentList.isEmpty()){
            this.sysAttachmentMapper.insertBatch(insertSysAttachmentList);
        }
        int i = yyCommissioneOperationMapper.updCommissioneOperation(info);

        long endTime = System.currentTimeMillis();
        log.info("程序运行时间： " + (endTime - startTime) + "ms");
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);


    }

    @Override
    public ResponseVO exportCommissioneOperation(ExportCommissioneOperationDTO dto, HttpServletResponse response) {
        List<YyCommissioneOperationHomeList> lists = yyCommissioneOperationMapper.queryCommissioneOperationHomeListTotal(dto.getYyCommissioneOperationHomeDTO());
        for (YyCommissioneOperationHomeList list : lists) {
            if (list.getStartTime() == null || list.getEndTime() == null) {
                String startTimeAndEndTime = "无";
                list.setStartTimeAndEndTime(startTimeAndEndTime);
            }else {
                String startTimeStr = TimeUtils.TimestampToDateStr(list.getStartTime(), "yyyy-MM-dd");
                String endTimeStr = TimeUtils.TimestampToDateStr(list.getEndTime(), "yyyy-MM-dd");
                String startTimeAndEndTime = startTimeStr + "至" + endTimeStr;
                list.setStartTimeAndEndTime(startTimeAndEndTime);
            }
        }
        Map<String, String> keyMap = dto.getKeyMap();
        //需要显示的字段名
        //字段名对应的中文名
        String[] headers = null;
        String[] needShowKeyArr = null;

        if (StringUtils.isEmpty(dto.getNeedShowKeys())) {
            //不传getNeedShowKeys 显示所有字段
            Set<String> keys = keyMap.keySet();
            needShowKeyArr = keys.toArray(new String[keys.size()]);
            headers = new String[keys.size()];

        } else {
            needShowKeyArr = dto.getNeedShowKeys().split(",");
            //字段名对应的中文名
            headers = new String[needShowKeyArr.length];
        }

        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }
        try {
            ExcelUtils.getExcel(response, headers, needShowKeyArr, lists, "entrust.xls");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
