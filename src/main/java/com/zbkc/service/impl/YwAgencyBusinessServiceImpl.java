package com.zbkc.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.common.enums.*;
import com.zbkc.common.utils.*;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.*;
import com.zbkc.model.dto.YwAgencyBusinessDTO;
import com.zbkc.model.dto.YwContractDTO;
import com.zbkc.model.dto.YwDevInfoDTO;
import com.zbkc.model.dto.YwHandoverDetailsDTO;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.*;
import com.zbkc.service.*;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.util.ListUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/9/27
 */
@Service
public class YwAgencyBusinessServiceImpl extends ServiceImpl<YwAgencyBusinessMapper, YwAgencyBusinessPO> implements YwAgencyBusinessService {

    @Autowired
    YwAgencyBusinessMapper ywAgencyBusinessMapper;

    @Autowired
    YyContractMapper yyContractMapper;

    @Autowired
    private SysFilePathMapper sysFilePathMapper;

    @Autowired
    private CreateFileUtil createFileUtil;

    @Value("${file.win-name}")
    private String winUrl;
    @Value("${file.linux-name}")
    private String linuxUrl;

    @Autowired
    private SysDataTypeMapper sysDataTypeMapper;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private WyBasicInfoMapper wyBasicInfoMapper;

    @Resource
    private ToolUtils toolUtils;

    @Autowired
    private YwHandoverDetailsMapper ywHandoverDetailsMapper;

    @Autowired
    private YySplitRentMapper yySplitRentMapper;

    @Autowired
    private YyContractWyRelationMapper yyContractWyRelationMapper;

    @Autowired
    private YwHandoverDetailsSerivce ywHandoverDetailsSerivce;

    @Autowired
    private TaskService taskService;

    @Autowired
    private WyBasicInfoService wyBasicInfoService;

    @Autowired
    private ActivitiBaseService activitiBaseService;

    @Autowired
    ResourceLoader resourceLoader;

    @Autowired
    private YwDevInfoService ywDevInfoService;

    @Autowired
    private YyContractService yyContractService;

    @Autowired
    private SysAreaMapper sysAreaMapper;

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    @Autowired
    private YyUseRegMapper yyUseRegMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int addYwAgencyBusiness(YwAgencyBusinessDTO ywAgencyBusinessDTO) {

        YwAgencyBusinessPO ywAgencyBusinessPO = new YwAgencyBusinessPO();
        BeanUtils.copyProperties(ywAgencyBusinessDTO , ywAgencyBusinessPO);

        ywAgencyBusinessPO.setId(null);
        ywAgencyBusinessPO.setBusinessType(Byte.parseByte(ywAgencyBusinessDTO.getBusinessType()));
        ywAgencyBusinessPO.setSignSource((String) BusinessTypeEnum.SIGN_SOURCE_ONLINE_APPLY.getCode());
        ywAgencyBusinessPO.setAgencyType((Integer) BusinessTypeEnum.CONTRACT_SIGN_APPLY_AGENCY.getCode());
        ywAgencyBusinessPO.setRecordStatus(Byte.parseByte(BusinessTypeEnum.HANDOVER_DETAILS_STATUS_NEW.getCode().toString()));

        /**
         * 设置受理号
         * 受理号=HT+（签订、续签）首字母+NS+年+月+日+000（三位数编号）
         */
        String webSignType = "";
        boolean newSignFlag = true;
        String proName = ywAgencyBusinessPO.getApplicantUnit();
        String propertyCode = toolUtils.getPropertyCode();
        String wyName = ywAgencyBusinessPO.getWyName();

        if ("1".equals(ywAgencyBusinessPO.getWebSignType())) {
            webSignType = "QD";
            proName += "租赁合同新签申请";
        } else {
            webSignType = "XQ";
            proName += "租赁合同续签申请";
            newSignFlag = false;
        }

        long id = IdWorker.getId();
        ywAgencyBusinessPO.setId(id);
        ywAgencyBusinessPO.setTableId(String.valueOf(id));

        String acceptCode = "HT" + webSignType + propertyCode;
        ywAgencyBusinessPO.setAcceptCode(acceptCode);
        ywAgencyBusinessPO.setProName(proName);
        ywAgencyBusinessPO.setCreateTime(new Date());

        QueryWrapper<WyBasicInfo> queryWrapper = new QueryWrapper<>();
        String[] split = ywAgencyBusinessPO.getWyName().split(",");
        queryWrapper.in("id", Arrays.asList(split));
        List<WyBasicInfo> wyBasicInfos = this.wyBasicInfoMapper.selectList(queryWrapper);
        String wyNameNew = "";
        if (null != wyBasicInfos && !wyBasicInfos.isEmpty()) {
            for (WyBasicInfo wyBasicInfo : wyBasicInfos) {
                //修改物业状态为拟使用
                wyBasicInfo.setPropertyStatusId(CommonFinalData.PROPERTY_STATUS_3);

                wyNameNew += wyBasicInfo.getName();
            }
            ywAgencyBusinessPO.setWyName(wyNameNew);

            //更新物业状态
            this.wyBasicInfoMapper.updateBatch(wyBasicInfos);
        }

        boolean save = this.save(ywAgencyBusinessPO);

        // 根据新签续签类型 生成业务办理——附件材料审核状态表
        //增加附件审核表
        MHContractFileNameEnum[] values = MHContractFileNameEnum.values();
        int length = values.length;
        if (newSignFlag) {
            length -= 1;
        }
        List<YwHandoverDetailsPO> insertList = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            YwHandoverDetailsPO ywHandoverDetailsPO = new YwHandoverDetailsPO();
            ywHandoverDetailsPO.setStatus("0");
            ywHandoverDetailsPO.setName(values[i].getFileName());
            ywHandoverDetailsPO.setYwAgencyBusinessId(ywAgencyBusinessPO.getId());
            insertList.add(ywHandoverDetailsPO);
        }
        this.ywHandoverDetailsSerivce.saveBatch(insertList);

        //物业名称中存放着物业id
        if (StringUtils.isNotEmpty(wyName)) {
            String[] wyIdArr = wyName.split(",");
            List<YyContractWyRelationPO> yyContractWyRelationPOList = new ArrayList<>();
            for (String wyId : wyIdArr) {
                YyContractWyRelationPO yyContractWyRelationPO = new YyContractWyRelationPO();
                yyContractWyRelationPO.setWyBasicInfoId(Long.parseLong(wyId));
                yyContractWyRelationPO.setYwBusinessId(ywAgencyBusinessPO.getId());
                yyContractWyRelationPOList.add(yyContractWyRelationPO);
            }
            this.yyContractWyRelationMapper.insertBatch(yyContractWyRelationPOList);
        }

        return save ? 1 : 0;
    }

    @Override
    public int removeYwAgencyBusiness(YwAgencyBusinessPO ywAgencyBusinessPO) {
        return this.ywAgencyBusinessMapper.deleteById(ywAgencyBusinessPO.getId());
    }

    @Override
    public int updateYwAgencyBusiness(YwAgencyBusinessDTO ywAgencyBusinessDTO) {

        YwAgencyBusinessPO ywAgencyBusinessPO = new YwAgencyBusinessPO();
        BeanUtils.copyProperties(ywAgencyBusinessDTO, ywAgencyBusinessPO);

        return this.ywAgencyBusinessMapper.updateById(ywAgencyBusinessPO);
    }

    @Override
    public YwAgencyBusinessPO selectYwAgencyBusinessById(YwAgencyBusinessPO ywAgencyBusinessPO) {
        return this.ywAgencyBusinessMapper.selectById(ywAgencyBusinessPO.getId());
    }

    @Override
    public PageVO<YwAgencyBusinessPO> pageYwAgencyBusinessList(YwAgencyBusinessDTO ywAgencyBusinessDTO) {

        if (ywAgencyBusinessDTO.getP() <= 0 ) {
            throw new BusinessException(ErrorCodeEnum.PAGE_NUMBER_ERROR);
        }

        int expectPage = ywAgencyBusinessDTO.getP();
        int queryStartRowNumber = (ywAgencyBusinessDTO.getP() - 1) * ywAgencyBusinessDTO.getSize();
        List<YwAgencyBusinessPO> list = new ArrayList<>();
        int total = 0;

        //超级管理员显示所有
        Long roleId = this.sysUserRoleMapper.selectRoleByUserId(ywAgencyBusinessDTO.getUserId());
        boolean isAdminUser = FinalSysAdminRoleEnum.checkIsAdmin(roleId);

        //待办业务列表通用sql
        String todoTypeCommon = " join ACT_RU_EXECUTION are on art.PROC_INST_ID_ = are.PROC_INST_ID_ " +
                " join yw_agency_business yab on yab.table_id = are.BUSINESS_KEY_ and yab.agency_type = " + ywAgencyBusinessDTO.getAgencyType() +
                " WHERE art.SUSPENSION_STATE_ = 1 ";

        int agencyType = ywAgencyBusinessDTO.getAgencyType();
        switch (agencyType) {
            case 1:
            case 2:
                break;
            case 3:
                todoTypeCommon += " AND LEFT ( art.PROC_DEF_ID_, 10 )='signonline' and art.ASSIGNEE_ IS NULL  ";
                break;
            default:
                break;
        }

        //所有业务列表通用sql
        String allTypeCommon = "LEFT JOIN ACT_HI_PROCINST acp ON acp.PROC_INST_ID_ = aht.PROC_INST_ID_ " +
                "left join ACT_RU_EXECUTION are on aht.EXECUTION_ID_ = are.ID_ and are.SUSPENSION_STATE_ = 1 " +
                "JOIN yw_agency_business yab ON yab.table_id = acp.BUSINESS_KEY_ where 1 = 1 ";

        String todoListSql = "";
        String allSql = "";

        if (isAdminUser) {
            todoListSql = "SELECT DISTINCT yab.id as ID_ FROM ACT_RU_TASK art " +
                    todoTypeCommon;

            allSql = "SELECT DISTINCT yab.id as ID_ FROM ACT_HI_TASKINST aht " + allTypeCommon;

        } else {
            todoListSql = "SELECT DISTINCT \n" +
                    "\tyab.id as ID_\n" +
                    "FROM\n" +
                    "\tACT_RU_TASK art \n" +
                    "\tleft join ACT_RU_EXECUTION are on art.PROC_INST_ID_ = are.PROC_INST_ID_\n" +
                    "\tleft join ACT_RU_IDENTITYLINK ari on ari.PROC_INST_ID_ = art.PROC_INST_ID_\n" +
                    "\tjoin yw_agency_business yab on yab.table_id = are.BUSINESS_KEY_\n" +
                    "WHERE\n" +
                    "1=1\n" +
                    "\tand (art.ASSIGNEE_ is null or art.ASSIGNEE_ = "+ ywAgencyBusinessDTO.getUserId() + ")"+
                    "\tand ari.USER_ID_ = " + ywAgencyBusinessDTO.getUserId() +
                    "\tAND art.SUSPENSION_STATE_ = 1 ";

            allSql = "SELECT DISTINCT \n" +
                    "\tyab.id as ID_\n" +
                    "FROM\n" +
                    "\tACT_HI_TASKINST aht\n" +
                    "\tLEFT JOIN ACT_HI_PROCINST ahp ON ahp.PROC_INST_ID_ = aht.PROC_INST_ID_ \n" +
                    "\tleft join ACT_HI_IDENTITYLINK ahi on ahi.PROC_INST_ID_ = aht.PROC_INST_ID_\n"+
                    "\tjoin yw_agency_business yab on yab.table_id = ahp.BUSINESS_KEY_\n" +
                    "WHERE\n" +
                    "1=1\n" +
                    "\tand (aht.ASSIGNEE_ is null or aht.ASSIGNEE_ = " + ywAgencyBusinessDTO.getUserId() + ")"+
                    "\tand ahi.USER_ID_ = "+ ywAgencyBusinessDTO.getUserId();
        }

        ywAgencyBusinessDTO.setP(queryStartRowNumber);
        String businessType = ywAgencyBusinessDTO.getBusinessType();
        if (BusinessTypeEnum.ALL.getName().equals(businessType)
                || StringUtils.isEmpty(ywAgencyBusinessDTO.getBusinessType())) {

            //扩展sql 先查询总数 及 业务id
            allSql = sqlExtend(allSql, ywAgencyBusinessDTO, queryStartRowNumber);
            //查询历史记录表
            List<HistoricTaskInstance> historicTaskInstanceList = this.historyService.createNativeHistoricTaskInstanceQuery()
                    .sql(allSql)
                    .list();

            if (null != historicTaskInstanceList && !historicTaskInstanceList.isEmpty()) {

                List<String> businessIdList = historicTaskInstanceList.stream().map(TaskInfo::getId).collect(Collectors.toList());

                QueryWrapper<YwAgencyBusinessPO> queryWrapper = new QueryWrapper<>();
                queryWrapper.in("id", businessIdList);
                list = this.ywAgencyBusinessMapper.selectList(queryWrapper);

                total = ywAgencyBusinessDTO.getTotal();

            }
        } else if (BusinessTypeEnum.TODO.getName().equals(businessType)) {
            //扩展sql
            todoListSql = sqlExtend(todoListSql, ywAgencyBusinessDTO, queryStartRowNumber);
            //查询正在处理任务表
            List<Task> tasks = this.taskService
                    .createNativeTaskQuery()
                    .sql(todoListSql)
                    .list();

            if (null != tasks && !tasks.isEmpty()) {
                List<String> businessIdList = tasks.stream().map(TaskInfo::getId).collect(Collectors.toList());

                QueryWrapper<YwAgencyBusinessPO> queryWrapper = new QueryWrapper<>();
                queryWrapper.in("id", businessIdList);
                list = this.ywAgencyBusinessMapper.selectList(queryWrapper);

                total = ywAgencyBusinessDTO.getTotal();
            }
        }

        if(null != list && list.size() > 0){
            //设置业务类型
            list.forEach(e->{
                e.setType(disposeBusinessType(e));
            });
        }

        PageVO pageVO = new PageVO();
        pageVO.setList(list);
        pageVO.setTotal(total);
        pageVO.setP(expectPage);
        pageVO.setSize(ywAgencyBusinessDTO.getSize());
        return pageVO;
    }

    @Override
    public YwAgencyBusinessPO selectOne(String businessId) {
        return this.ywAgencyBusinessMapper.selectById(businessId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SysFilePath1 saveSysFilePath(FileVo vo, String relationId) {

        //判断id是否存在于附件材料审核表中
        YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessMapper.selectById(relationId);
        if (null != ywAgencyBusinessPO) {
            YyContract yyContract = new YyContract();
            yyContract.setId(ywAgencyBusinessPO.getRelationId());
            long contractId = IdWorker.getId();
            yyContract.setContractFilePathId(contractId);
            this.yyContractMapper.updateById(yyContract);

            relationId = String.valueOf(contractId);
        }

        //修改申请材料审核状态表
        YwHandoverDetailsPO ywHandoverDetailsPO = new YwHandoverDetailsPO();
        ywHandoverDetailsPO.setId(Long.parseLong(relationId));

        //根据relationId查询，有则修改 无则新增
        QueryWrapper<SysFilePath> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("db_file_id", relationId);
        SysFilePath sysFilePathTemp = this.sysFilePathMapper.selectOne(queryWrapper);
        if (null != sysFilePathTemp) {
            sysFilePathTemp.setFileName(vo.getFileName());
            sysFilePathTemp.setPath(vo.getFilePath());
            sysFilePathTemp.setFileSize(vo.getFileSize());
            this.sysFilePathMapper.updateById(sysFilePathTemp);

            ywHandoverDetailsPO.setStatus(BusinessTypeEnum.HANDOVER_DETAILS_STATUS_NEW.getCode().toString());
        } else {
            sysFilePathTemp = new SysFilePath();
            sysFilePathTemp.setFileName(vo.getFileName());
            sysFilePathTemp.setPath(vo.getFilePath());
            sysFilePathTemp.setDbFileId(Long.parseLong(relationId));
            sysFilePathTemp.setId(IdWorker.getId());
            sysFilePathTemp.setFileSize(vo.getFileSize());
            this.sysFilePathMapper.addSysFilePath(sysFilePathTemp);
        }

        ywHandoverDetailsPO.setHandoverDetailsImgId(sysFilePathTemp.getId());
        ywHandoverDetailsPO.setSupplementDataOpinion("");
        this.ywHandoverDetailsMapper.updateById(ywHandoverDetailsPO);

        SysFilePath1 sysFilePath1 = new SysFilePath1();
        BeanUtils.copyProperties(sysFilePathTemp, sysFilePath1);
        return sysFilePath1;
    }

    @Override
    public void downloadBatchByFile(HttpServletResponse response, String downloadFileIds) {
        //封装需要下载的文件流转化为byte[]
        Map<String, byte[]> files = new HashMap<>();

        List<SysFilePath> sysFilePaths = null;
        if (StringUtils.isNotEmpty(downloadFileIds)) {
            List<Long> queryList = new ArrayList<>();
            String[] downloadFileIdsArr = downloadFileIds.split(",");
            for (String s : downloadFileIdsArr) {
                queryList.add(Long.parseLong(s));
            }

            QueryWrapper<SysFilePath> queryWrapper = new QueryWrapper<>();
            queryWrapper.in("db_file_id", queryList);
            sysFilePaths = this.sysFilePathMapper.selectList(queryWrapper);
        }

        if (null == sysFilePaths || sysFilePaths.isEmpty()) {
            return;
        }

        try {

            for (SysFilePath sysFilePath : sysFilePaths) {
                String fileName = sysFilePath.getFileName();
                String path = sysFilePath.getPath();
                //TODO 文件路径获取问题
                String preFix = createFileUtil.isWindows() ? winUrl : linuxUrl;

                byte[] remoteFile = FileUtils.getRemoteFile(preFix + "/" + path);

                files.put(fileName, remoteFile);
            }

            //调用下载
            FileUtils.downloadBatchByFile(response, files, "download");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public YwContractVO showOldContract(String businessId) {

        YwContractVO ywContractVO = this.yyContractMapper.selectYwContractIdByBusinessId(businessId);
        if (null == ywContractVO) {
            return null;
        }

        List<YwHandoverDetailsDTO> ywHandoverDetailsDTOList = ywContractVO.getYwHandoverDetailsDTOList();
        if (!ywHandoverDetailsDTOList.isEmpty()) {
            for (YwHandoverDetailsDTO ywHandoverDetailsDTO : ywHandoverDetailsDTOList) {
                long totalFileSize = getTotalFileSize(ywHandoverDetailsDTO.getHandoverDetailsImgId(), ywHandoverDetailsDTO);
                ywHandoverDetailsDTO.setFileSize(String.valueOf(totalFileSize));
            }
        }

        List<SysDataType> houseOwnerShipList = this.sysDataTypeMapper.selectAllByCodeLive("house_ownership");
        ywContractVO.setHouseOwnerShipList(houseOwnerShipList);

        List<SysDataType> houseCredentialsType = this.sysDataTypeMapper.selectAllByCodeLive("house_proof");
        ywContractVO.setPartAHouseCredentialsList(houseCredentialsType);

        List<SysDataType> houseLeaseUse = this.sysDataTypeMapper.selectAllByCodeLive("lease_use");
        ywContractVO.setLeaseUseList(houseLeaseUse);


        //申请来源 网上申请，且是第二个节点才可能修改
        ywContractVO.setCanModify(false);
        YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessMapper.selectById(businessId);
        if (null!= ywAgencyBusinessPO.getSignSource() && ywAgencyBusinessPO.getSignSource().equals(BusinessTypeEnum.SIGN_SOURCE_ONLINE_APPLY.getCode())) {
            Boolean secondActivity = this.activitiBaseService.isSecondActivity(businessId);
            if (secondActivity) {
                ywContractVO.setCanModify(true);
            }
        }

        //计算出租面积 套内建筑面积 公摊面积
        List<WyBasicInfo> wyBasicInfos = this.wyBasicInfoMapper.selectWyInfoByBusinessId(Long.parseLong(businessId));
        BigDecimal rentalArea = new BigDecimal(0);
        BigDecimal setArea = new BigDecimal(0);
        BigDecimal pubArea = new BigDecimal(0);
        BigDecimal monthlyRent = new BigDecimal(0);
        StringBuffer houseCode = new StringBuffer();
        StringBuffer wyNames = new StringBuffer();
        if (!wyBasicInfos.isEmpty()) {
            for (WyBasicInfo wyBasicInfo : wyBasicInfos) {
                double zlRentalArea = wyBasicInfo.getZlRentalArea() == null ? 0 : wyBasicInfo.getZlRentalArea();
                double zlRentUnitPrice = wyBasicInfo.getZlRentUnitPrice() == null ? 0 : wyBasicInfo.getZlRentUnitPrice().doubleValue();

                rentalArea = rentalArea.add(BigDecimal.valueOf(zlRentalArea));
                setArea = setArea.add(BigDecimal.valueOf(wyBasicInfo.getBuildArea() == null ? 0 : wyBasicInfo.getBuildArea()));
                pubArea = pubArea.add(BigDecimal.valueOf(wyBasicInfo.getShareArea() == null ? 0 : wyBasicInfo.getShareArea()));

                houseCode.append(wyBasicInfo.getPropertyCode());
                houseCode.append(",");
                wyNames.append(wyBasicInfo.getName());
                wyNames.append(",");

                BigDecimal monthly = new BigDecimal(zlRentalArea).multiply(new BigDecimal(zlRentUnitPrice));
                wyBasicInfo.setZlMonthlyRent(monthly);

                monthlyRent = monthlyRent.add(wyBasicInfo.getZlMonthlyRent() == null ? new BigDecimal(0) : wyBasicInfo.getZlMonthlyRent());
            }

        }


        ywContractVO.setRentalArea(String.valueOf(rentalArea.doubleValue()));
        ywContractVO.setSetArea(String.valueOf(setArea.doubleValue()));
        ywContractVO.setPubArea(String.valueOf(pubArea.doubleValue()));
        ywContractVO.setHouseCodes(houseCode.toString().length() > 1 ? houseCode.toString().substring(0, houseCode.length() - 1) : "");
        ywContractVO.setMonthlyRent(monthlyRent.toString());
        ywContractVO.setLeaseWyAddress(wyNames.length() > 1 ? wyNames.toString().substring(0, wyNames.length() - 1) : "");

        return ywContractVO;
    }

    @Override
    public PageVO<YwAgencyBusinessVO> pageYwAgencyBusinessVOList(YwAgencyBusinessDTO ywAgencyBusinessDTO) {
        if (ywAgencyBusinessDTO.getP() <= 0 || ywAgencyBusinessDTO.getSize() < 0) {
            throw new BusinessException(ErrorCodeEnum.PAGE_NUMBER_ERROR);
        }

        int expectPage = ywAgencyBusinessDTO.getP();
        int queryStartRowNumber = (ywAgencyBusinessDTO.getP() - 1) * ywAgencyBusinessDTO.getSize();

        ywAgencyBusinessDTO.setP(queryStartRowNumber);
        List<YwAgencyBusinessVO> list = this.ywAgencyBusinessMapper.selectYwAgencyBusinessVOByPage(ywAgencyBusinessDTO);
        int total = this.ywAgencyBusinessMapper.selectYwAgencyBusinessVOByPageCount(ywAgencyBusinessDTO);

        PageVO pageVO = new PageVO();
        pageVO.setList(list);
        pageVO.setTotal(total);
        pageVO.setP(expectPage);
        pageVO.setSize(ywAgencyBusinessDTO.getSize());
        return pageVO;
    }

    @Override
    public int cancelAuthorization(YwAgencyBusinessPO ywAgencyBusinessPO) {
        ywAgencyBusinessPO.setRecordStatus(Byte.parseByte(BusinessTypeEnum.ADMIN_CANCEL_AUTHORIZATION.getCode().toString()));
        int record = this.ywAgencyBusinessMapper.updateById(ywAgencyBusinessPO);

        //修改物业状态为空置
        QueryWrapper<YyContractWyRelationPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("yw_business_id" , ywAgencyBusinessPO.getId());
        List<YyContractWyRelationPO> wyRelationPOS = this.yyContractWyRelationMapper.selectList(queryWrapper);
        if(null != wyRelationPOS){
            List<WyBasicInfo> updateWyList = new ArrayList<>();
            for (YyContractWyRelationPO wyRelationPO : wyRelationPOS) {
                WyBasicInfo wyBasicInfo = new WyBasicInfo();
                wyBasicInfo.setId(wyRelationPO.getWyBasicInfoId());
                wyBasicInfo.setPropertyStatusId(CommonFinalData.PROPERTY_STATUS_2);
                updateWyList.add(wyBasicInfo);
            }
            record += this.wyBasicInfoMapper.updateBatch(updateWyList);
        }

        return record;
    }

    @Override
    public int chooseContractSave(String id, String contractIds) {
        YwAgencyBusinessPO ywAgencyBusinessPO = new YwAgencyBusinessPO();
        ywAgencyBusinessPO.setId(Long.parseLong(id));
        ywAgencyBusinessPO.setSelectContacts(contractIds);
        return this.ywAgencyBusinessMapper.updateById(ywAgencyBusinessPO);
    }

    @Override
    public int chooseMainContract(YwAgencyBusinessDTO ywAgencyBusinessDTO) {
        YwAgencyBusinessPO ywAgencyBusinessPO = new YwAgencyBusinessPO();
        ywAgencyBusinessPO.setId(ywAgencyBusinessDTO.getId());
        ywAgencyBusinessPO.setYyContactId(Long.parseLong(ywAgencyBusinessDTO.getMainContractId()));
        return this.ywAgencyBusinessMapper.updateById(ywAgencyBusinessPO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateYyContractInProgress(YwContractDTO ywContractDTO) {

        YyContract yyContract = new YyContract();

        String businessId = ywContractDTO.getBusinessId();
        String isEconomicCommitment = ywContractDTO.getIsEconomicCommitment();
        String economicCommitment = ywContractDTO.getEconomicCommitment();
        Date startRentDate = DateUtil.parseDate(ywContractDTO.getStartRentDate());
        Date endRentDate = DateUtil.parseDate(ywContractDTO.getEndRentDate());
        BigDecimal rentUnitPrice = new BigDecimal(ywContractDTO.getRentUnitPrice() == null ? "0" : ywContractDTO.getRentUnitPrice());

        //更新待办业务表
        YwAgencyBusinessPO ywAgencyBusinessPO = new YwAgencyBusinessPO();
        ywAgencyBusinessPO.setId(Long.parseLong(businessId));
        ywAgencyBusinessPO.setIsCommitment(isEconomicCommitment);
        ywAgencyBusinessPO.setRemark(economicCommitment);
        this.ywAgencyBusinessMapper.updateById(ywAgencyBusinessPO);

        //更新合同表数据
        copyYwContractToYyContract(ywContractDTO, yyContract, startRentDate, endRentDate, rentUnitPrice);
        //更新附件信息
        if (null != ywContractDTO.getContractFiles() && !ywContractDTO.getContractFiles().isEmpty()) {
            long contractFilePathId = IdWorker.getId();
            yyContract.setContractFilePathId(contractFilePathId);
            for (SysFilePath1 contractFile : ywContractDTO.getContractFiles()) {
                SysFilePath temp = new SysFilePath();
                temp.setId(contractFile.getId());
                temp.setDbFileId(contractFilePathId);
                this.sysFilePathMapper.updateById(temp);
            }
        }

        //物业基础信息 根据房屋编码查询
        String houseCodes = ywContractDTO.getHouseCodes();
        String[] split = houseCodes.split(",");
        List<String> list = new ArrayList<>(Arrays.asList(split));
        List<WyBasicInfo> wyBasicInfos = this.wyBasicInfoMapper.selectListByIds(list);

        //删除合同与房屋的关联关系，重新生成
        QueryWrapper<YyContractWyRelationPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("yy_contract_id", yyContract.getId());
        List<YyContractWyRelationPO> deleteList = this.yyContractWyRelationMapper.selectList(queryWrapper);
        List<Long> idList = deleteList.stream().map(YyContractWyRelationPO::getId).collect(Collectors.toList());
        if (!idList.isEmpty()) {
            this.yyContractWyRelationMapper.deleteByIds(idList);
        }


        if (null != wyBasicInfos && !wyBasicInfos.isEmpty()) {
            List<Long> collect = wyBasicInfos.stream().map(WyBasicInfo::getId).collect(Collectors.toList());
            List<YyContractWyRelationPO> insertList = new ArrayList<>();
            for (Long id : collect) {
                YyContractWyRelationPO yyContractWyRelationPO = new YyContractWyRelationPO();
                yyContractWyRelationPO.setId(IdWorker.getId());
                yyContractWyRelationPO.setWyBasicInfoId(id);
                yyContractWyRelationPO.setYyContractId(yyContract.getId());
                yyContractWyRelationPO.setYwBusinessId(Long.parseLong(businessId));
                insertList.add(yyContractWyRelationPO);
            }
            this.yyContractWyRelationMapper.insertBatch(insertList);
        }

        //更新拆分租金信息
        if (null != ywContractDTO.getSplitRent() && ywContractDTO.getSplitRent() == 1) {

            //先删除老的拆分租金信息 然后再更新新的
            this.yySplitRentMapper.deleteByYyContractId(yyContract.getId());

            List<YySplitRent> insertSplitRentList = new ArrayList<>();
            List<WyBasicInfo> updateWyBasicInfoList = new ArrayList<>();

            BigDecimal total = new BigDecimal("0");

            for (YySplitRent splitRent : ywContractDTO.getRentList()) {
                splitRent.setId(IdWorker.getId());
                splitRent.setYyContractId(yyContract.getId());
                splitRent.setCreateTime(new Timestamp(System.currentTimeMillis()));
                insertSplitRentList.add(splitRent);

                if (null != splitRent.getWyBasicInfoId()) {
                    WyBasicInfo wyBasicInfo = new WyBasicInfo();
                    wyBasicInfo.setId(splitRent.getWyBasicInfoId());
                    wyBasicInfo.setZlRentUnitPrice(splitRent.getPrice());
                    wyBasicInfo.setZlMonthlyRent(splitRent.getRent());
                    wyBasicInfo.setZlRentalArea(splitRent.getArea().doubleValue());
                    wyBasicInfo.setZlStartRentDate(startRentDate);
                    wyBasicInfo.setZlEndRentDate(endRentDate);
                    updateWyBasicInfoList.add(wyBasicInfo);
                }

                total = total.add(splitRent.getRent());
            }
            //合同表存放月租金
            yyContract.setZlMonthlyRent(total.toString());

            this.yySplitRentMapper.insertBatch(insertSplitRentList);
            this.wyBasicInfoMapper.updateBatch(updateWyBasicInfoList);

        } else {
            /**
             * 房屋编码计算出租面积  套内建筑面积 公摊面积 租金
             */
            BigDecimal total = new BigDecimal("0");
            List<WyBasicInfo> updateWyBasicInfoList = new ArrayList<>();
            for (WyBasicInfo wyBasicInfo : wyBasicInfos) {
                WyBasicInfo temp = new WyBasicInfo();
                temp.setId(wyBasicInfo.getId());
                temp.setZlRentUnitPrice(rentUnitPrice);
                BigDecimal multiply = BigDecimal.valueOf(wyBasicInfo.getBuildArea()).multiply(rentUnitPrice).setScale(4, BigDecimal.ROUND_HALF_UP);
                temp.setZlMonthlyRent(multiply);
                temp.setZlRentalArea(wyBasicInfo.getBuildArea());
                temp.setZlStartRentDate(startRentDate);
                temp.setZlEndRentDate(endRentDate);
                updateWyBasicInfoList.add(temp);

                total = total.add(multiply);
            }
            //合同表存放月租金
            yyContract.setZlMonthlyRent(total.toString());

            this.wyBasicInfoMapper.updateBatch(updateWyBasicInfoList);
        }

        return this.yyContractMapper.updateById(yyContract);
    }

    @Override
    public List<YwHandoverDetailsDTO> selectYwHandoverDetailsByBusinessId(YwContractDTO ywContractDTO) {
        //业务id
        String businessId = ywContractDTO.getBusinessId();

        YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessMapper.selectById(businessId);

        QueryWrapper<YwHandoverDetailsPO> ywHandoverDetailsPOQueryWrapper = new QueryWrapper<>();
        ywHandoverDetailsPOQueryWrapper.eq("yw_agency_business_id", businessId);
        List<YwHandoverDetailsPO> ywHandoverDetailsPOList = this.ywHandoverDetailsMapper.selectList(ywHandoverDetailsPOQueryWrapper);

        List<YwHandoverDetailsDTO> ywHandoverDetailsDTOList = new ArrayList<>();
        if (!ywHandoverDetailsPOList.isEmpty()) {

            String webSignType = ywAgencyBusinessPO.getWebSignType();
            Byte applyPeopleType = ywAgencyBusinessPO.getApplyPeopleType();

            for (YwHandoverDetailsPO ywHandoverDetailsPO : ywHandoverDetailsPOList) {
                String name = ywHandoverDetailsPO.getName();
                if (BusinessTypeEnum.NEW_SIGN.getCode().toString().equals(webSignType)) {
                    if ("续签通知书".equals(name)) {
                        continue;
                    }
                } else if (BusinessTypeEnum.RENEW_SIGN.getCode().toString().equals(webSignType)) {
                    if ("入驻通知书".equals(name)) {
                        continue;
                    }
                }

                if (null != applyPeopleType && 1 == applyPeopleType) {
                    if ("法人授权委托书".equals(name) || "经办人身份证复印件".equals(name)) {
                        continue;
                    }
                }

                YwHandoverDetailsDTO ywHandoverDetailsDTO = new YwHandoverDetailsDTO();
                long totalFileSize = getTotalFileSize(ywHandoverDetailsPO.getHandoverDetailsImgId(), ywHandoverDetailsDTO);

                BeanUtils.copyProperties(ywHandoverDetailsPO, ywHandoverDetailsDTO);
                ywHandoverDetailsDTO.setFileSize(String.valueOf(totalFileSize));
                ywHandoverDetailsDTOList.add(ywHandoverDetailsDTO);
            }

            return ywHandoverDetailsDTOList;
        }

        return null;
    }

    @Override
    public ResponseVO checkRecord(YwAgencyBusinessDTO ywAgencyBusinessDTO) {

        /**
         * 根据物业id查询yy_contract_wy_relation表
         * 在查询yy_contract表
         * 判断合同是否到期
         * 不到期不能新增
         */
        if (StringUtils.isEmpty(ywAgencyBusinessDTO.getWyName())) {
            return new ResponseVO(ErrorCodeEnum.SUCCESS, ErrorCodeEnum.SUCCESS.getErrMsg());
        }

        String[] wyIdArr = ywAgencyBusinessDTO.getWyName().split(",");
        List<String> wyIdList = Arrays.asList(wyIdArr);

        QueryWrapper<YyContractWyRelationPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("wy_basic_info_id", wyIdList);
        List<YyContractWyRelationPO> yyContractWyRelationPOList = this.yyContractWyRelationMapper.selectList(queryWrapper);

        //物业选择是否冲突标识 true为冲突 false为不冲突
        boolean flag = false;

        if (!yyContractWyRelationPOList.isEmpty()) {
            for (YyContractWyRelationPO yyContractWyRelationPO : yyContractWyRelationPOList) {
                Long yyContractId = yyContractWyRelationPO.getYyContractId();
                if (null == yyContractId) {
                    flag = true;
                    break;
                }

                YyContract yyContract = this.yyContractMapper.selectById(yyContractId);
                Date endRentDate = yyContract.getEndRentDate();
                if (null != endRentDate) {
                    long currentTimeMillis = System.currentTimeMillis();
                    if (currentTimeMillis < endRentDate.getTime()) {
                        flag = true;
                        break;
                    }
                }
            }
        }

        if (flag) {
            return new ResponseVO(ErrorCodeEnum.WY_CHOOSE_ERROR, ErrorCodeEnum.WY_CHOOSE_ERROR.getErrMsg());
        }

        return null;
    }

    @Override
    public String generateAcceptCode(YwAgencyBusinessPO ywAgencyBusinessPO) {

        String acceptCode = "";

        String propertyCode = toolUtils.getPropertyCode();

        switch (ywAgencyBusinessPO.getAgencyType()) {
            case 1:
                //移交用房申请
                acceptCode = "YJ" + propertyCode;
                break;
            case 2:
                //物业资产申报
                ywAgencyBusinessPO.setProName(ywAgencyBusinessPO.getApplicantUnit() + "物业资产申报");
                break;
            case 3:
                //合同签署申请
                //受理号=HT+（签订、续签）首字母+NS+年+月+日+000（三位数编号）
                acceptCode = "HT";
                String proName = ywAgencyBusinessPO.getApplicantUnit();
                if ("1".equals(ywAgencyBusinessPO.getWebSignType())) {
                    acceptCode += "QD";
                    proName += "租赁合同新签申请";
                } else {
                    acceptCode += "XQ";
                    proName += "租赁合同续签申请";
                }
                acceptCode += propertyCode;
                ywAgencyBusinessPO.setProName(proName);
                break;
            default:
                break;
        }

        ywAgencyBusinessPO.setAcceptCode(acceptCode);

        return acceptCode;
    }

    /**
     * 拷贝 ywContractDTO 属性值至 yyContract
     *
     * @param ywContractDTO ywContractDTO
     * @param yyContract    yyContract
     * @param startRentDate 租赁开始时间
     * @param endRentDate   租赁结束时间
     * @param rentUnitPrice 租金单价
     */
    private void copyYwContractToYyContract(YwContractDTO ywContractDTO, YyContract yyContract, Date startRentDate, Date endRentDate, BigDecimal rentUnitPrice) {
        ywContractDTO.clearCompanyInfo();
        BeanUtils.copyProperties(ywContractDTO, yyContract);

        yyContract.setHouseProofId(Long.parseLong(ywContractDTO.getPartAHouseCredentials()));
        yyContract.setCertificationNumber(ywContractDTO.getPartAHouseCredentialsNumber());
        yyContract.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        yyContract.setRentPrice(rentUnitPrice);

        if (null != ywContractDTO.getRentalArea()) {
            yyContract.setRentalArea(new BigDecimal(ywContractDTO.getRentalArea()));
        }
        if (null != ywContractDTO.getGuidePrice()) {
            yyContract.setGuidePrice(new BigDecimal(ywContractDTO.getGuidePrice()));
        }
        if (null != ywContractDTO.getMonthlyRent()) {
            yyContract.setMonthlyRent(new BigDecimal(ywContractDTO.getMonthlyRent()));
        }
        if (null != ywContractDTO.getDepositAmount()) {
            yyContract.setDepositAmount(new BigDecimal(ywContractDTO.getDepositAmount()));
        }
        if (null != ywContractDTO.getWaterCharge()) {
            yyContract.setWaterFee(new BigDecimal(ywContractDTO.getWaterCharge()));
        }
        if (null != ywContractDTO.getGasCharge()) {
            yyContract.setGasFee(new BigDecimal(ywContractDTO.getGasCharge()));
        }
        if (null != ywContractDTO.getElectricCharge()) {
            yyContract.setElectricFee(new BigDecimal(ywContractDTO.getElectricCharge()));
        }
        if (null != ywContractDTO.getPropertyManagementCharge()) {
            yyContract.setPropertyManagementFee(new BigDecimal(ywContractDTO.getPropertyManagementCharge()));
        }
        if (null != ywContractDTO.getOtherCharge()) {
            yyContract.setOtherFee(new BigDecimal(ywContractDTO.getOtherCharge()));
        }
        if (null != ywContractDTO.getFirstRentTime()) {
            Date firstRentTime = DateUtil.parseDate(ywContractDTO.getFirstRentTime());
            yyContract.setFirstRentTime(firstRentTime);
        }
        if (null != ywContractDTO.getFirstRentMoney()) {
            yyContract.setFirstRentMoney(new BigDecimal(ywContractDTO.getFirstRentMoney()));
        }
        if (null != ywContractDTO.getLeaseUse()) {
            yyContract.setLeaseUse(Integer.parseInt(ywContractDTO.getLeaseUse()));
        }
        if (null != ywContractDTO.getFreeRent()) {
            yyContract.setFreeRent(Integer.parseInt(ywContractDTO.getFreeRent()));
        }
        if (null != ywContractDTO.getSplitRent()) {
            yyContract.setIsSplitRent(ywContractDTO.getSplitRent());
        }
        yyContract.setDepositAmount(new BigDecimal(ywContractDTO.getDepositAmount()));
        yyContract.setDiscount(Integer.parseInt(ywContractDTO.getDiscount()));
        yyContract.setHouseOwnershipId(Long.parseLong(ywContractDTO.getHouseOwnership()));
        yyContract.setStartRentDate(new Timestamp(startRentDate.getTime()));
        yyContract.setEndRentDate(new Timestamp(endRentDate.getTime()));
        yyContract.setIsEconomicCommit(Integer.parseInt(ywContractDTO.getIsEconomicCommitment()));
        yyContract.setEconomicCommit(ywContractDTO.getEconomicCommitment());
    }

    /**
     * 计算文件大小
     *
     * @param handoverDetailsImgId2
     * @param ywHandoverDetailsDTO
     * @return
     */
    private long getTotalFileSize(Long handoverDetailsImgId2, YwHandoverDetailsDTO ywHandoverDetailsDTO) {
        Long handoverDetailsImgId = handoverDetailsImgId2;

        //计算文件大小
        QueryWrapper<SysFilePath> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", handoverDetailsImgId);
        List<SysFilePath> sysFilePaths = this.sysFilePathMapper.selectList(queryWrapper);
        long totalFileSize = 0;
        if (!sysFilePaths.isEmpty()) {

            List<SysFilePath1> list = new ArrayList<>();

            for (SysFilePath sysFilePath : sysFilePaths) {

                SysFilePath1 sysFilePath1 = new SysFilePath1();
                BeanUtils.copyProperties(sysFilePath, sysFilePath1);
                list.add(sysFilePath1);

                String fileSize = sysFilePath.getFileSize();
                if (StringUtils.isEmpty(fileSize)) {
                    continue;
                }
                totalFileSize += Long.parseLong(fileSize);
            }

            ywHandoverDetailsDTO.setSysFilePathList(list);
        }
        return totalFileSize;
    }

    /**
     * 根据list 查询代办业务表
     *
     * @param ywAgencyBusinessDTO dto
     * @param queryStartRowNumber 开始条数
     * @param businessIdList      list
     * @return list
     */
    private List<YwAgencyBusinessPO> selectBusinessListByIdList(YwAgencyBusinessDTO ywAgencyBusinessDTO, int queryStartRowNumber, List<String> businessIdList) {
        List<YwAgencyBusinessPO> list;//待办业务查询工作流相关表
        QueryWrapper<YwAgencyBusinessPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("id", businessIdList);

        if (0 != ywAgencyBusinessDTO.getAgencyType()) {
            queryWrapper.eq("agency_type", ywAgencyBusinessDTO.getAgencyType());
        }
        if (StringUtils.isNotEmpty(ywAgencyBusinessDTO.getProName())) {
            queryWrapper.eq("pro_name", ywAgencyBusinessDTO.getProName());
        }
        if (StringUtils.isNotEmpty(ywAgencyBusinessDTO.getAcceptCode())) {
            queryWrapper.eq("accept_code", ywAgencyBusinessDTO.getAcceptCode());
        }
        if (StringUtils.isNotEmpty(ywAgencyBusinessDTO.getApplicantUnit())) {
            queryWrapper.eq("applicant_unit", ywAgencyBusinessDTO.getApplicantUnit());
        }
        if (StringUtils.isNotEmpty(ywAgencyBusinessDTO.getStageName())) {
            queryWrapper.eq("stage_name", ywAgencyBusinessDTO.getStageName());
        }
        if (StringUtils.isNotEmpty(ywAgencyBusinessDTO.getProcess())) {
            queryWrapper.eq("process", ywAgencyBusinessDTO.getProcess());
        }
        if (StringUtils.isNotEmpty(ywAgencyBusinessDTO.getQueryStartTime()) && StringUtils.isNotEmpty(ywAgencyBusinessDTO.getQueryEndTime())) {
            queryWrapper.between("application_time ", ywAgencyBusinessDTO.getQueryStartTime(), ywAgencyBusinessDTO.getQueryEndTime());
        }
        if (null != ywAgencyBusinessDTO.getSort() && ywAgencyBusinessDTO.getSort() == 1) {
            queryWrapper.orderByAsc("application_time");
        } else if (null != ywAgencyBusinessDTO.getSort() && ywAgencyBusinessDTO.getSort() == 2) {
            queryWrapper.orderByDesc("application_time");
        } else {
            queryWrapper.orderByAsc("id");
        }
        if (queryStartRowNumber >= 0 && ywAgencyBusinessDTO.getSize() != 0) {
            queryWrapper.last("limit " + queryStartRowNumber + "," + ywAgencyBusinessDTO.getSize());
        }

        list = this.ywAgencyBusinessMapper.selectList(queryWrapper);
        return list;
    }


    /**
     * 获取word替换数据
     *
     * @param businessId
     * @param ywAgencyBusinessPO
     * @return
     */
    @Override
    public Map<String, String> getWordInfoMap(@RequestParam("businessId") String businessId, YwAgencyBusinessPO ywAgencyBusinessPO) {
        Map<String, String> testMap = new HashMap<String, String>();
        testMap.put("tenant", ywAgencyBusinessPO.getApplicantUnit());

        YwAgencyBusinessPO temp = this.ywAgencyBusinessMapper.selectById(businessId);
        Long relationId = temp.getRelationId();
        QueryWrapper<YyContract> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", relationId);
        YyContract yyContract = this.yyContractMapper.selectOne(queryWrapper);

        if (null == yyContract) {
            testMap.put("mainTermsOfTheContract", "租赁地址+面积+租金单价+月租金+合同期限+合同起始日期+审核人+经办人");
        } else {

            List<WyBaseInfoVO> wyBaseInfoVOS = this.wyBasicInfoService.selectAllWyBaseInfo(String.valueOf(yyContract.getId()), null,null );
            StringBuffer wordInfo = new StringBuffer();
            if (!wyBaseInfoVOS.isEmpty()) {
                for (WyBaseInfoVO wyBaseInfoVO : wyBaseInfoVOS) {
                    wordInfo.append("租赁地址：");
                    wordInfo.append(wyBaseInfoVO.getWyAddress());
                    wordInfo.append("面积：");
                    wordInfo.append(wyBaseInfoVO.getRentalArea());
                    wordInfo.append("租金单价：");
                    wordInfo.append(wyBaseInfoVO.getRentPrice());
                    wordInfo.append("月租金：");
                    wordInfo.append(wyBaseInfoVO.getMonthlyRent());
                    wordInfo.append("合同起始日期：");
                    wordInfo.append(yyContract.getStartRentDate());
                    wordInfo.append("至");
                    wordInfo.append(yyContract.getEndRentDate());
                }
            }

            testMap.put("mainTermsOfTheContract", wordInfo.toString());
        }

        List<YwHandoverDetailsPO> ywHandoverDetailsPOSList = this.ywHandoverDetailsSerivce.selectList(businessId);

        StringBuffer stringBuffer = new StringBuffer();
        if (!ywHandoverDetailsPOSList.isEmpty()) {
            int id = 1;
            for (YwHandoverDetailsPO ywHandoverDetailsPO : ywHandoverDetailsPOSList) {
                stringBuffer.append(id + ". " + ywHandoverDetailsPO.getName());
                stringBuffer.append("\r");
                id++;
            }
        }
        testMap.put("fileList", stringBuffer.toString());
        return testMap;
    }

    @Override
    public ResponseVO saveSupplementData(Map<String, String> opinionMap) {

        /**
         * 保存材料补正信息 只存在于申请节点的下一个节点
         * 逻辑：
         * 根据业务id 查询 附件详情表 ，找到对应的附件列表数据
         * 遍历列表，根据yw_handover_details表id设置 材料补正意见
         * 驳回节点
         */

        if (!opinionMap.isEmpty()) {
            String businessId = opinionMap.get("businessId");
            opinionMap.remove("businessId");

            List<YwHandoverDetailsPO> ywHandoverDetailsPOS = this.ywHandoverDetailsSerivce.selectList(businessId);
            List<Long> ywHandoverDetailsIdList = new ArrayList<>();
            Map<Long, YwHandoverDetailsPO> ywHandoverDetailsPOMap = new HashMap<>();
            if (!ListUtils.isEmpty(ywHandoverDetailsPOS)) {
                ywHandoverDetailsIdList = ywHandoverDetailsPOS.stream().map(YwHandoverDetailsPO::getId).collect(Collectors.toList());

                for (YwHandoverDetailsPO ywHandoverDetailsPO : ywHandoverDetailsPOS) {
                    ywHandoverDetailsPOMap.put(ywHandoverDetailsPO.getId(), ywHandoverDetailsPO);
                }
            }

            List<YwHandoverDetailsPO> list = new ArrayList<>();
            for (Map.Entry<String, String> map : opinionMap.entrySet()) {
                String id = map.getKey();

                if (!ywHandoverDetailsIdList.contains(Long.parseLong(id))) {
                    continue;
                }

                String supplementDataOpinion = map.getValue();
                YwHandoverDetailsPO ywHandoverDetailsPO = new YwHandoverDetailsPO();
                ywHandoverDetailsPO.setId(Long.parseLong(id));
                ywHandoverDetailsPO.setStatus(BusinessTypeEnum.HANDOVER_DETAILS_STATUS_ERROR.getCode().toString());
                ywHandoverDetailsPO.setSupplementDataOpinion(supplementDataOpinion);
                list.add(ywHandoverDetailsPO);
            }
            this.ywHandoverDetailsSerivce.updateBatchById(list);

            return new ResponseVO(ErrorCodeEnum.SUCCESS, list.size());
        }

        return null;
    }

    /**
     * 移交用房呈批表 流程进度
     *
     * @param businessId 业务id
     * @param testList   拼接数组
     */
    @Override
    public void getProcessInfoList(@RequestParam("businessId") String businessId, List<String[]> testList) {
        List<YwProcessInfoVO> processInfoVOList = this.activitiBaseService.findProcessAllStatusByBusinessKey(businessId);
        if (null != processInfoVOList) {
            for (YwProcessInfoVO ywProcessInfoVO : processInfoVOList) {
                if (StringUtils.isEmpty(ywProcessInfoVO.getOperatingTime())) {
                    continue;
                }
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String time = "";
                if (Long.parseLong(ywProcessInfoVO.getOperatingTime()) > 1) {
                    time = sdf.format(new Date(Long.parseLong(ywProcessInfoVO.getOperatingTime())));
                }

                testList.add(new String[]{ywProcessInfoVO.getProcess(), ywProcessInfoVO.getComment(), time});
            }
        }
    }

    @Override
    public void export(YwAgencyBusinessDTO ywAgencyBusinessDTO, HttpServletResponse response) {
        if (null == ywAgencyBusinessDTO.getAgencyType()) {
            ywAgencyBusinessDTO.setAgencyType(Integer.parseInt(BusinessTypeEnum.CONTRACT_SIGN_APPLY_AGENCY.getCode().toString()));
        }

        PageVO<YwAgencyBusinessPO> pageYwAgencyBusinessList = this.pageYwAgencyBusinessList(ywAgencyBusinessDTO);

        Map<String, String> keyMap = ywAgencyBusinessDTO.getKeyMap();

        //需要显示的字段名
        //字段名对应的中文名
        String[] headers = null;
        String[] needShowKeyArr = null;

        if (StringUtils.isEmpty(ywAgencyBusinessDTO.getNeedShowKeys())) {
            //不传getNeedShowKeys 显示所有字段
            Set<String> keys = keyMap.keySet();
            needShowKeyArr = keys.toArray(new String[keys.size()]);
            headers = new String[keys.size()];
        } else {
            needShowKeyArr = ywAgencyBusinessDTO.getNeedShowKeys().split(",");
            //字段名对应的中文名
            headers = new String[needShowKeyArr.length];
        }

        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        List<YwAgencyBusinessPO> list = pageYwAgencyBusinessList.getList();
        if(null != list && !list.isEmpty()){
            for (YwAgencyBusinessPO ywAgencyBusinessPO : list) {
                String webSignTypeStr = ywAgencyBusinessPO.getWebSignType();
                int webSignType = Integer.parseInt(webSignTypeStr == null ? "-1" :webSignTypeStr);

                Byte businessTypeByte = ywAgencyBusinessPO.getBusinessType();
                int businessType = Integer.parseInt(businessTypeByte == null ? "-1" : businessTypeByte + "");

                switch (webSignType){
                    case 1:
                        ywAgencyBusinessPO.setWebSignTypeStr("新签");
                        break;
                    case 2:
                        ywAgencyBusinessPO.setWebSignTypeStr("续签");
                        break;
                    default:
                        break;
                }

                //申请业务类型 1产业用房租赁 2商业用房租赁 3住宅用房租赁 4南山软件园租赁
                switch (businessType){
                    case 1:
                        ywAgencyBusinessPO.setBusinessTypeStr("产业用房租赁");
                        break;
                    case 2:
                        ywAgencyBusinessPO.setBusinessTypeStr("商业用房租赁");
                        break;
                    case 3:
                        ywAgencyBusinessPO.setBusinessTypeStr("住宅用房租赁");
                        break;
                    case 4:
                        ywAgencyBusinessPO.setBusinessTypeStr("南山软件园租赁");
                        break;
                    default:
                        break;
                }

                //代办类型 1移交用房申请；2物业资产申报; 3合同签署申请；
                switch (ywAgencyBusinessPO.getAgencyType()){
                    case 1:
                        ywAgencyBusinessPO.setAgencyTypeStr("移交用房申请");
                        break;
                    case 2:
                        ywAgencyBusinessPO.setAgencyTypeStr("物业资产申报");
                        break;
                    case 3:
                        ywAgencyBusinessPO.setAgencyTypeStr("合同签署申请");
                        break;
                    default:
                        break;
                }
            }
        }

        //组装数据 导出文件
        //String[] headers = new String[]{"序号","项目名称","受理号","阶段名称","申请单位","流程进度","申请时间","用户ID" , "待办类型"};
        //String[] values = new String[]{"id" , "proName" , "acceptCode","stageName","applicantUnit","process","applicationTime","userId","agencyType"};
        try {

            ExcelUtils.getExcel(response, headers, needShowKeyArr, list, "exportList");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void downloadAgencyApplyTable(HttpServletResponse response, String businessId) {
        YwAgencyBusinessPO ywAgencyBusinessPO = this.selectOne(businessId);

        Map<String, String> testMap = new HashMap<String, String>();
        testMap.put("proName", ywAgencyBusinessPO.getProName());
        testMap.put("applicantUnit", ywAgencyBusinessPO.getApplicantUnit());
        testMap.put("stageName", ywAgencyBusinessPO.getStageName());

        List<String[]> testList = new ArrayList<String[]>();
        this.getProcessInfoList(businessId, testList);
        org.springframework.core.io.Resource resource = this.resourceLoader.getResource("classpath:excel/agencyApply.docx");
        try {
            WordUtils.changWord("移交用房呈批表.docx", testMap, testList, response, resource.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void downloadSignApplicationTable(HttpServletResponse response, String businessId, YwAgencyBusinessPO ywAgencyBusinessPO) {

        Map<String, String> testMap = this.getWordInfoMap(businessId, ywAgencyBusinessPO);
        org.springframework.core.io.Resource resource = resourceLoader.getResource("classpath:excel/signApplication.docx");
        try {
            WordUtils.changWord("物业租赁合同呈批表.docx", testMap, new ArrayList<>(), response, resource.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ResponseVO dealWith(String businessId) {
        YwAgencyBusinessPO ywAgencyBusinessPO = this.selectOne(businessId);
        if (null == ywAgencyBusinessPO) {
            return new ResponseVO(ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND, ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND.getErrMsg());
        }

        //代办类型
        int agentType = ywAgencyBusinessPO.getAgencyType();
        if (((int) BusinessTypeEnum.DELIVERY_HOUSE_APPLY.getCode()) == agentType) {
            //移交用房申请 查询开发商基本信息表
            YwDevInfoDTO ywDevInfoDTO = this.ywDevInfoService.selectOneByBusinessId(Long.parseLong(businessId));
            ywDevInfoDTO.setCanModify(this.activitiBaseService.isSecondActivity(businessId));

            return new ResponseVO(ErrorCodeEnum.SUCCESS, ywDevInfoDTO);

        } else if (((int) BusinessTypeEnum.CONTRACT_SIGN_APPLY_AGENCY.getCode()) == agentType
                || ((int) BusinessTypeEnum.CONTRACT_SIGN_APPLY_NORMAL.getCode()) == agentType) {
            //合同签署申请 查询合同表
            return this.yyContractService.selectOneDTOByBusinessId(businessId);
        } else if (((int) BusinessTypeEnum.PROPERTY_APPLY.getCode()) == agentType) {
            //物业资产申请
            List<YwHandoverDetailsPO> ywHandoverDetailsPOS = this.ywHandoverDetailsSerivce.selectList(businessId);
            ywAgencyBusinessPO.setYwHandoverDetailsPOList(ywHandoverDetailsPOS);

            ywAgencyBusinessPO.setCanModify(this.activitiBaseService.isSecondActivity(businessId));

            return new ResponseVO(ErrorCodeEnum.SUCCESS, ywAgencyBusinessPO);
        }

        return new ResponseVO(ErrorCodeEnum.AGENCY_TYPE_ERROR, ErrorCodeEnum.AGENCY_TYPE_ERROR.getErrMsg());
    }

    @Override
    public void downLoadPaymentNotice(HttpServletResponse response, String businessId) {
        YyContract yyContract = this.yyContractService.selectOneByBusinessId(businessId);

        List<WyBaseInfoVO> wyBaseInfoVOList = this.wyBasicInfoService.selectAllWyBaseInfo(String.valueOf(yyContract.getId()), null, null);

        BigDecimal monthlyRent = new BigDecimal(0);
        BigDecimal rentalArea = new BigDecimal(0);

        if (!wyBaseInfoVOList.isEmpty()) {
            for (WyBaseInfoVO wyBaseInfoVO : wyBaseInfoVOList) {
                monthlyRent.add(new BigDecimal(wyBaseInfoVO.getMonthlyRent() == null ? "0" : wyBaseInfoVO.getMonthlyRent()));
                rentalArea.add(new BigDecimal(wyBaseInfoVO.getRentalArea() == null ? "0" : wyBaseInfoVO.getRentalArea()));
            }
        }

        Calendar today = Calendar.getInstance();
        int year = today.get(Calendar.YEAR);
        int month = today.get(Calendar.MONTH);
        int dayOfMonth = today.get(Calendar.DAY_OF_MONTH);

        Map<String, String> testMap = new HashMap<String, String>();
        testMap.put("leaser", yyContract.getLeaser() == null ? "" : yyContract.getLeaser());
        testMap.put("houseCode", yyContract.getHouseCode() == null ? "" : yyContract.getHouseCode());
        testMap.put("contactPerson ", yyContract.getContactPerson() == null ? "" : yyContract.getContactPerson());
        testMap.put("contactPhone", yyContract.getContactPhone() == null ? "" : yyContract.getContactPhone());
        testMap.put("monthlyRent", monthlyRent == null ? "" : monthlyRent.toString());
        testMap.put("depositAmount", yyContract.getDepositAmount() == null ? "0" : yyContract.getDepositAmount().toString());
        testMap.put("rentalArea", rentalArea == null ? "" : rentalArea.toString());
        testMap.put("startRentDate", TimeUtils.getDateString(yyContract.getStartRentDate()) == null ? "" : TimeUtils.getDateString(yyContract.getStartRentDate()));
        testMap.put("endRentDate", TimeUtils.getDateString(yyContract.getEndRentDate()) == null ? "" : TimeUtils.getDateString(yyContract.getEndRentDate()));
        yyContract.setFreeRent(yyContract.getFreeRent() == null ? 0 : yyContract.getFreeRent());
        //免租期限 0无，1十五日，2一个月，3两个月，4三个月，5四个月
        switch (yyContract.getFreeRent()) {
            case 0:
                testMap.put("freeRent", "无");
                break;
            case 1:
                testMap.put("freeRent", "十五日");
                break;
            case 2:
                testMap.put("freeRent", "一个月");
                break;
            case 3:
                testMap.put("freeRent", "两个月");
                break;
            case 4:
                testMap.put("freeRent", "三个月");
                break;
            case 5:
                testMap.put("freeRent", "四个月");
                break;
            default:
                break;
        }
        testMap.put("firstRentMoney", yyContract.getFirstRentMoney() == null ? "" : yyContract.getFirstRentMoney().toString());
        testMap.put("year", String.valueOf(year));
        testMap.put("month", String.valueOf(month + 1));
        testMap.put("day", String.valueOf(dayOfMonth));

        org.springframework.core.io.Resource resource = resourceLoader.getResource("classpath:excel/paymentNotice.docx");
        try {
            WordUtils.changWord("租赁缴款通知单.docx", testMap, new ArrayList<>(), response, resource.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void downLoadContactTemplate(HttpServletResponse response, String businessId) {
        YyContract yyContract = this.yyContractService.selectOneByBusinessId(businessId);

        List<WyBaseInfoVO> wyBaseInfoVOList = this.wyBasicInfoService.selectAllWyBaseInfo(String.valueOf(yyContract.getId()), null, null);

        BigDecimal buildArea = new BigDecimal(0);
        BigDecimal shareArea = new BigDecimal(0);
        BigDecimal monthlyRent = new BigDecimal(0);

        StringBuffer mainPurpos = new StringBuffer();
        StringBuffer rentName = new StringBuffer();
        StringBuffer houseCodes = new StringBuffer();
        StringBuffer detailAddrs = new StringBuffer();

        Map<String, String> testMap = new HashMap<String, String>();

        if (!wyBaseInfoVOList.isEmpty()) {

            for (WyBaseInfoVO wyBaseInfoVO : wyBaseInfoVOList) {

                buildArea.add(new BigDecimal(wyBaseInfoVO.getRentalArea() == null ? "0" : wyBaseInfoVO.getRentalArea()));
                shareArea.add(new BigDecimal(wyBaseInfoVO.getShareArea()));
                monthlyRent.add(new BigDecimal(wyBaseInfoVO.getMonthlyRent() == null ? "0" : wyBaseInfoVO.getMonthlyRent()));
                String areaCode = wyBaseInfoVO.getAreaCode();

                String mainPurposStr = wyBaseInfoVO.getMainPurposStr();
                mainPurpos.append(mainPurposStr);
                mainPurpos.append(",");

                String wyName =  wyBaseInfoVO.getWyName();
                rentName.append(wyName);
                rentName.append(",");

                String houseCode = wyBaseInfoVO.getHouseCode();
                houseCodes.append(houseCode);
                houseCodes.append(",");

                String wyAddress = wyBaseInfoVO.getAddress() + wyBaseInfoVO.getWyAddress();
                detailAddrs.append(wyAddress);
                detailAddrs.append(",");

                if(StringUtils.isNotEmpty(areaCode)){
                    Map<String, String> nameMap = this.sysAreaMapper.selectNameByCommitteeCode(areaCode);
                    String areaName = nameMap.get("area_name");
                    testMap.put("areaName" , areaName);
                }
            }
        }

        testMap.put("legalCode", yyContract.getLegalCode() == null ? "" : yyContract.getLegalCode());
        testMap.put("detailAddr", detailAddrs.length() > 1 ? detailAddrs.toString().substring(0, detailAddrs.length() - 1) : "");
        testMap.put("phone ", yyContract.getLegalPhone() == null ? "" : yyContract.getLegalPhone());
        testMap.put("leaser", yyContract.getLeaser() == null ? "" : yyContract.getLeaser());
        testMap.put("socialCode", yyContract.getLeaserCode() == null ? "" : yyContract.getLeaserCode());
        testMap.put("leaseAddress", yyContract.getLegalAddress() == null ? "" : yyContract.getLegalAddress());
        testMap.put("leasePhone", yyContract.getLeaserPhone() == null ? "" : yyContract.getLeaserPhone());

        testMap.put("buildArea", buildArea == null ? "" : buildArea.toString());
        testMap.put("shareArea", shareArea == null ? "" : shareArea.toString());
        testMap.put("electricFee", yyContract.getElectricFee() == null ? "" : yyContract.getElectricFee().toString());
        testMap.put("waterRent", yyContract.getWaterFee() == null ? "" : yyContract.getWaterFee().toString());
        testMap.put("propertyManagementFee", yyContract.getPropertyManagementFee().toString());
        testMap.put("otherFee", yyContract.getOtherFee() == null ? "" : yyContract.getOtherFee().toString());
        testMap.put("gasFee", yyContract.getGasFee() == null ? "" : yyContract.getGasFee().toString());
        testMap.put("mainPurposId", mainPurpos.length() > 1 ? mainPurpos.toString().substring(0, mainPurpos.length() - 1) : "");

        if(null == yyContract.getClientPerson() || StringUtils.isEmpty(yyContract.getClientPerson())){
            testMap.put("applyPersonType","法定代表人");
            if (yyContract.getLegalType() == 1) {
                testMap.put("type", "身份证");
            }
            if (yyContract.getLegalType() == 2) {
                testMap.put("type", "护照");
            }
            testMap.put("code", yyContract.getLegalCode());
            testMap.put("address", yyContract.getLegalAddress());
            testMap.put("phone", yyContract.getLegalPhone());
            testMap.put("person", yyContract.getLegalPerson() == null ? "" : yyContract.getLegalPerson());
        }else{
            testMap.put("applyPersonType","委托代理人");
            if (yyContract.getClientType() == 1) {
                testMap.put("type", "身份证");
            }
            if (yyContract.getClientType() == 2) {
                testMap.put("type", "护照");
            }
            testMap.put("code", yyContract.getClientCode());
            testMap.put("address", yyContract.getClientAddress());
            testMap.put("phone", yyContract.getClientPhone());
            testMap.put("person", yyContract.getClientPerson() == null ? "" : yyContract.getClientPerson());
        }

        switch (yyContract.getLeaseUse()) {
            case 1:
                testMap.put("leaseUse", ExportEnum.OFFICE.getName());
                break;
            case 2:
                testMap.put("leaseUse", ExportEnum.COMPREHENSIVE_RESEARCH.getName());
                break;
            case 3:
                testMap.put("leaseUse", ExportEnum.INDUSTRY_RESEARCH.getName());
                break;
            case 4:
                testMap.put("leaseUse", ExportEnum.RESEARCH_AND_DEVELOPMENT.getName());
                break;
            default:
                testMap.put("leaseUse", "");
        }
        testMap.put("houseCode", houseCodes.length() > 1 ? houseCodes.toString().substring(0, houseCodes.length() - 1) : "");

        Date startRentDate = yyContract.getStartRentDate();
        Date endRentDate = yyContract.getEndRentDate();

        Calendar startCalender = Calendar.getInstance();
        startCalender.setTime(startRentDate);
        testMap.put("startYear", String.valueOf(startCalender.get(Calendar.YEAR)));
        testMap.put("startMonth", String.valueOf(startCalender.get(Calendar.MONTH)));
        testMap.put("startDay", String.valueOf(startCalender.get(Calendar.DAY_OF_MONTH)));
        if (null != endRentDate) {
            Calendar startCalender2 = Calendar.getInstance();
            startCalender2.setTime(endRentDate);
            testMap.put("endYear", String.valueOf(startCalender2.get(Calendar.YEAR)));
            testMap.put("endMonth", String.valueOf(startCalender2.get(Calendar.MONTH)));
            testMap.put("endDay", String.valueOf(startCalender2.get(Calendar.DAY_OF_MONTH)));

            testMap.put("totalYear", String.valueOf(startCalender2.get(Calendar.YEAR) - startCalender.get(Calendar.YEAR)));
            testMap.put("totalMonth", String.valueOf(startCalender2.get(Calendar.MONTH) - startCalender.get(Calendar.MONTH)));
        }

        Calendar freeStartCalender = Calendar.getInstance();
        freeStartCalender.setTime(startRentDate);
        switch (yyContract.getFreeRent()) {
            case 0:
                testMap.put("freeRent", "无");
                break;
            case 1:
                testMap.put("freeRent", "十五日");
                freeStartCalender.add(Calendar.DAY_OF_MONTH , 15);
                break;
            case 2:
                testMap.put("freeRent", "一个月");
                freeStartCalender.add(Calendar.MONTH , 1);
                break;
            case 3:
                testMap.put("freeRent", "两个月");
                freeStartCalender.add(Calendar.MONTH , 2);
                break;
            case 4:
                testMap.put("freeRent", "三个月");
                freeStartCalender.add(Calendar.MONTH , 3);
                break;
            case 5:
                testMap.put("freeRent", "四个月");
                freeStartCalender.add(Calendar.MONTH , 4);
                break;
            default:
                break;
        }

        if(!"无".equals(testMap.get("freeRent"))){
            testMap.put("freeStartYear", String.valueOf(startCalender.get(Calendar.YEAR)));
            testMap.put("freeStartMonth", String.valueOf(startCalender.get(Calendar.MONTH)));
            testMap.put("freeStartDay", String.valueOf(startCalender.get(Calendar.DAY_OF_MONTH)));

            testMap.put("freeEndYear", String.valueOf(freeStartCalender.get(Calendar.YEAR)));
            testMap.put("freeEndMonth", String.valueOf(freeStartCalender.get(Calendar.MONTH)));
            testMap.put("freeEndDay", String.valueOf(freeStartCalender.get(Calendar.DAY_OF_MONTH)));
        }

        BigDecimal guidePrice = yyContract.getGuidePrice();

        testMap.put("guidePrice" , guidePrice == null ? "": guidePrice.toString());
        String discount = "";
        switch (yyContract.getDiscount()){
            case 1:
                discount = "100";
                break;
            case 2:
                discount = "30";
                break;
            case 3:
                discount = "50";
                break;
            case 4:
                discount = "70";
                break;
            case 5:
                discount = "90";
                break;
            default:
                discount = "100";
                break;
        }
        testMap.put("discount" ,discount);
        testMap.put("disPrice" , null == guidePrice ? "" : (guidePrice.multiply(new BigDecimal(discount)).divide(new BigDecimal(100))).toString());

        testMap.put("monthleyRent", monthlyRent == null ? "" : monthlyRent.toString());
        testMap.put("monthleyRentBig", Convert.digitToChinese(monthlyRent == null ? 0 : monthlyRent));
        testMap.put("rentName", rentName.length() > 1 ? rentName.toString().substring(0, rentName.length() - 1) : "");
        testMap.put("depositAmount",yyContract.getDepositAmount().toString());
        testMap.put("depositAmountBig",Convert.digitToChinese(yyContract.getDepositAmount() == null ? 0 : yyContract.getDepositAmount()));
        testMap.put("certificationNumber",yyContract.getCertificationNumber());
        //违约金
        testMap.put("doubleIndemnity" , monthlyRent == null ? "" : monthlyRent.multiply(new BigDecimal(2)).toString());

        //甲方信息补充
        if(null == yyContract.getType() && null != yyContract.getId()){
            QueryWrapper<YwAgencyBusinessPO> ywAgencyBusinessPOQueryWrapper = new QueryWrapper<>();
            ywAgencyBusinessPOQueryWrapper.eq("relation_id" , yyContract.getId());
            YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessMapper.selectOne(ywAgencyBusinessPOQueryWrapper);
            if(null != ywAgencyBusinessPO && null != ywAgencyBusinessPO.getBusinessType()){
                DownloadContractPartAInfoUtils.addPartAInfo(testMap, ywAgencyBusinessPO.getBusinessType());
            }
        }else if(null != yyContract.getType()){
            DownloadContractPartAInfoUtils.addPartAInfo(testMap, Byte.parseByte(yyContract.getType() + ""));
        }

        org.springframework.core.io.Resource resource = resourceLoader.getResource("classpath:excel/contactTemplate.docx");
        try {
            WordUtils.changWord("合同模板.docx", testMap, new ArrayList<>(), response, resource.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    @Override
    public ResponseVO checkApplyStatus(String businessId) {

        YwHandoverDetailsPO ywHandoverDetailsPO = this.ywHandoverDetailsMapper.selectById(businessId);
        if (null != ywHandoverDetailsPO && null != ywHandoverDetailsPO.getYwAgencyBusinessId()) {
            businessId = ywHandoverDetailsPO.getYwAgencyBusinessId().toString();
        }

        YwAgencyBusinessPO ywAgencyBusinessPO = this.selectOne(businessId);

        byte processApplyUndo = Byte.parseByte(BusinessTypeEnum.PROCESS_APPLY_UNDO.getCode().toString());
        if (null != ywAgencyBusinessPO.getRecordStatus() && ywAgencyBusinessPO.getRecordStatus().equals(processApplyUndo)) {
            return new ResponseVO(ErrorCodeEnum.OPERATE_ERROR, ErrorCodeEnum.OPERATE_ERROR.getErrMsg());
        }

        return null;
    }

    @Override
    public ResponseVO supplementData(Map<String, String> opinionMap) {
        if (!opinionMap.isEmpty()) {
            String businessId = opinionMap.get("businessId");

            opinionMap.remove("businessId");

            List<YwHandoverDetailsPO> ywHandoverDetailsPOS = this.ywHandoverDetailsSerivce.selectList(businessId);
            List<Long> ywHandoverDetailsIdList = new ArrayList<>();
            Map<Long, YwHandoverDetailsPO> ywHandoverDetailsPOMap = new HashMap<>();
            if (!ListUtils.isEmpty(ywHandoverDetailsPOS)) {
                ywHandoverDetailsIdList = ywHandoverDetailsPOS.stream().map(YwHandoverDetailsPO::getId).collect(Collectors.toList());

                for (YwHandoverDetailsPO ywHandoverDetailsPO : ywHandoverDetailsPOS) {
                    ywHandoverDetailsPOMap.put(ywHandoverDetailsPO.getId(), ywHandoverDetailsPO);
                }
            }

            StringBuffer stringBuffer = new StringBuffer();
            List<YwHandoverDetailsPO> list = new ArrayList<>();
            for (Map.Entry<String, String> map : opinionMap.entrySet()) {
                String id = map.getKey();

                if (!ywHandoverDetailsIdList.contains(Long.parseLong(id))) {
                    continue;
                }

                String supplementDataOpinion = map.getValue();
                YwHandoverDetailsPO ywHandoverDetailsPO = new YwHandoverDetailsPO();
                ywHandoverDetailsPO.setId(Long.parseLong(id));
                ywHandoverDetailsPO.setStatus(BusinessTypeEnum.HANDOVER_DETAILS_STATUS_ERROR.getCode().toString());
                ywHandoverDetailsPO.setSupplementDataOpinion(supplementDataOpinion);
                list.add(ywHandoverDetailsPO);

                stringBuffer.append(ywHandoverDetailsPOMap.get(Long.parseLong(id)).getName()).append(":").append(supplementDataOpinion).append(";");
            }
            this.ywHandoverDetailsSerivce.updateBatchById(list);

            return this.activitiBaseService.turnDownTaskInstance(businessId, "材料补正;" + stringBuffer.toString());
        }
        return new ResponseVO(ErrorCodeEnum.NULLERROR, ErrorCodeEnum.NULLERROR.getErrMsg());
    }

    /**
     * sql扩展
     *
     * @param sql                 原sql
     * @param ywAgencyBusinessDTO
     * @param queryStartRowNumber
     */
    private String sqlExtend(String sql, YwAgencyBusinessDTO ywAgencyBusinessDTO, int queryStartRowNumber) {
        StringBuffer extendSql = new StringBuffer();

        if (0 != ywAgencyBusinessDTO.getAgencyType()) {
            extendSql.append(" and yab.agency_type= ").append(ywAgencyBusinessDTO.getAgencyType());
        }
        if (StringUtils.isNotEmpty(ywAgencyBusinessDTO.getProName())) {
            extendSql.append(" and yab.pro_name= '").append(ywAgencyBusinessDTO.getProName()).append("'");
        }
        if (StringUtils.isNotEmpty(ywAgencyBusinessDTO.getAcceptCode())) {
            extendSql.append(" and yab.accept_code= '").append(ywAgencyBusinessDTO.getAcceptCode()).append("'");
        }
        if (StringUtils.isNotEmpty(ywAgencyBusinessDTO.getApplicantUnit())) {
            extendSql.append(" and yab.applicant_unit= '").append(ywAgencyBusinessDTO.getApplicantUnit()).append("'");
        }
        if (StringUtils.isNotEmpty(ywAgencyBusinessDTO.getStageName())) {
            extendSql.append(" and yab.stage_name= '").append(ywAgencyBusinessDTO.getStageName()).append("'");
        }
        if (StringUtils.isNotEmpty(ywAgencyBusinessDTO.getProcess())) {
            extendSql.append(" and yab.process= '").append(ywAgencyBusinessDTO.getProcess()).append("'");
        }
        if (StringUtils.isNotEmpty(ywAgencyBusinessDTO.getQueryStartTime()) && StringUtils.isNotEmpty(ywAgencyBusinessDTO.getQueryEndTime())) {

            //开始与结束时间相等
            if(ywAgencyBusinessDTO.getQueryStartTime().equals(ywAgencyBusinessDTO.getQueryEndTime())){
                Calendar instance = Calendar.getInstance();
                instance.setTime(DateUtil.parseDate(ywAgencyBusinessDTO.getQueryEndTime()));
                instance.add(Calendar.DAY_OF_MONTH , 1);

                String newQueryEndTime = DateUtil.format(instance.getTime(), "yyyy-MM-dd");

                ywAgencyBusinessDTO.setQueryEndTime(newQueryEndTime);
            }

            extendSql.append(" and yab.application_time >='").append(ywAgencyBusinessDTO.getQueryStartTime()).append("'");
            extendSql.append(" and yab.application_time <='").append(ywAgencyBusinessDTO.getQueryEndTime()).append("'");
        }

        if(ywAgencyBusinessDTO.getBusinessType().equals(BusinessTypeEnum.TODO.getName())){
            extendSql.append(" and yab.process != '已办结' ");
        }

        if (null != ywAgencyBusinessDTO.getSort() && ywAgencyBusinessDTO.getSort() == 1) {
            extendSql.append(" order by yab.application_time asc");
        } else if (null != ywAgencyBusinessDTO.getSort() && ywAgencyBusinessDTO.getSort() == 2) {
            extendSql.append(" order by yab.application_time desc");
        } else {
            extendSql.append(" order by yab.id asc");
        }

        List<HistoricTaskInstance> list = this.historyService.createNativeHistoricTaskInstanceQuery()
                .sql(sql + extendSql.toString())
                .list();
        ywAgencyBusinessDTO.setTotal(list.size());

        if (queryStartRowNumber >= 0 && ywAgencyBusinessDTO.getSize() != 0) {
            extendSql.append(" limit ").append(queryStartRowNumber).append(",").append(ywAgencyBusinessDTO.getSize());
        }

        return sql + extendSql.toString();
    }

    @Override
    public void exportCompanyAuthorization(YwAgencyBusinessDTO ywAgencyBusinessDTO, HttpServletResponse response) {
        if (null == ywAgencyBusinessDTO.getAgencyType()) {
            ywAgencyBusinessDTO.setAgencyType(Integer.parseInt(BusinessTypeEnum.CONTRACT_SIGN_APPLY_AGENCY.getCode().toString()));
        }

        PageVO<YwAgencyBusinessVO> ywAgencyBusinessPOPageVO = this.pageYwAgencyBusinessVOList(ywAgencyBusinessDTO);

        YwAgencyBusinessVO ywAgencyBusinessVO = new YwAgencyBusinessVO();
        Map<String, String> keyMap = ywAgencyBusinessVO.getKeyMap();

        //需要显示的字段名
        //字段名对应的中文名
        String[] headers = null;
        String[] needShowKeyArr = null;

        if (StringUtils.isEmpty(ywAgencyBusinessDTO.getNeedShowKeys())) {
            //不传getNeedShowKeys 显示所有字段
            Set<String> keys = keyMap.keySet();
            needShowKeyArr = keys.toArray(new String[keys.size()]);
            headers = new String[keys.size()];
        } else {
            needShowKeyArr = ywAgencyBusinessDTO.getNeedShowKeys().split(",");
            //字段名对应的中文名
            headers = new String[needShowKeyArr.length];
        }

        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        List<YwAgencyBusinessVO> list = ywAgencyBusinessPOPageVO.getList();
        if(null != list && !list.isEmpty()){
            for (YwAgencyBusinessVO agencyBusinessVO : list) {
                Integer webSignType = agencyBusinessVO.getWebSignType();

                Integer businessType = agencyBusinessVO.getBusinessType();

                switch (webSignType){
                    case 1:
                        agencyBusinessVO.setWebSignTypeStr("新签");
                        break;
                    case 2:
                        agencyBusinessVO.setWebSignTypeStr("续签");
                        break;
                    default:
                        break;
                }

                //申请业务类型 1产业用房租赁 2商业用房租赁 3住宅用房租赁 4南山软件园租赁
                switch (businessType){
                    case 1:
                        agencyBusinessVO.setBusinessTypeStr("产业用房租赁");
                        break;
                    case 2:
                        agencyBusinessVO.setBusinessTypeStr("商业用房租赁");
                        break;
                    case 3:
                        agencyBusinessVO.setBusinessTypeStr("住宅用房租赁");
                        break;
                    case 4:
                        agencyBusinessVO.setBusinessTypeStr("南山软件园租赁");
                        break;
                    default:
                        break;
                }

                switch (agencyBusinessVO.getIsCommitment()){
                    case 1:
                        agencyBusinessVO.setIsCommitmentTypeStr("是");
                        break;
                    case 2:
                        agencyBusinessVO.setIsCommitmentTypeStr("否");
                        break;
                    default:
                        break;
                }


            }
        }

        //组装数据 导出文件
        //String[] headers = new String[]{"序号","项目名称","受理号","阶段名称","申请单位","流程进度","申请时间","用户ID" , "待办类型"};
        //String[] values = new String[]{"id" , "proName" , "acceptCode","stageName","applicantUnit","process","applicationTime","userId","agencyType"};
        try {

            ExcelUtils.getExcel(response, headers, needShowKeyArr, list, "exportList");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ResponseVO dealWithBefore(String businessId) {

        YwAgencyBusinessPO ywAgencyBusinessPO = this.selectOne(businessId);
        if (null == ywAgencyBusinessPO) {
            return new ResponseVO(ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND, ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND.getErrMsg());
        }

        //代办类型
        int agentType = ywAgencyBusinessPO.getAgencyType();
        if (((int) BusinessTypeEnum.DELIVERY_HOUSE_APPLY.getCode()) == agentType) {

        } else if (((int) BusinessTypeEnum.CONTRACT_SIGN_APPLY_AGENCY.getCode()) == agentType
                || ((int) BusinessTypeEnum.CONTRACT_SIGN_APPLY_NORMAL.getCode()) == agentType) {

            YwContractVO ywContractVO = this.showOldContract(businessId);
            if (null == ywContractVO) {
                throw new BusinessException(ErrorCodeEnum.CONTRACT_NOT_FOUND);
            }

        } else if (((int) BusinessTypeEnum.PROPERTY_APPLY.getCode()) == agentType) {

        }else{
            throw new BusinessException(ErrorCodeEnum.AGENCY_TYPE_ERROR);
        }

        return new ResponseVO(ErrorCodeEnum.SUCCESS , true);
    }

    @Override
    public void businessFinallyHandler(YwContractDTO ywContractDTO) {

        Integer agencyType = this.ywAgencyBusinessMapper.selectAgencyType(ywContractDTO.getBusinessId());
        if(agencyType.equals(CommonFinalData.YW_BUSINESS_AGENCY_TYPE_2)){
            businessAgencyType2Handler(ywContractDTO);
        }

    }

    /**
     * 【合同签署申请】业务流全部完成最终处理
     * @param ywContractDTO dto
     */
    private void businessAgencyType2Handler(YwContractDTO ywContractDTO) {
        //修改物业状态为在用
        QueryWrapper<YyContractWyRelationPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("yw_business_id" , ywContractDTO.getBusinessId());
        List<YyContractWyRelationPO> yyContractWyRelationPOS = this.yyContractWyRelationMapper.selectList(queryWrapper);
        if(!yyContractWyRelationPOS.isEmpty()){
            List<WyBasicInfo> updateWyList = new ArrayList<>();
            for (YyContractWyRelationPO yyContractWyRelationPO : yyContractWyRelationPOS) {
                WyBasicInfo wyBasicInfo = new WyBasicInfo();
                wyBasicInfo.setId(yyContractWyRelationPO.getWyBasicInfoId());
                wyBasicInfo.setPropertyStatusId(CommonFinalData.PROPERTY_STATUS_1);

                updateWyList.add(wyBasicInfo);
            }
            this.wyBasicInfoMapper.updateBatch(updateWyList);

            YyContract yyContract = this.yyContractMapper.selectOneByBusinessId(Long.parseLong(ywContractDTO.getBusinessId()));
            //生成使用登记表
            List<YyUseReg> insertYyUseRegList = new ArrayList<>();
            for (YyContractWyRelationPO yyContractWyRelationPO : yyContractWyRelationPOS) {
                YyUseReg yyUseReg = new YyUseReg();
                yyUseReg.setId(IdWorker.getId());
                yyUseReg.setWyBasicInfoId(yyContractWyRelationPO.getWyBasicInfoId());
                yyUseReg.setMethod(CommonFinalData.USE_REG_METHOD_1);
                yyUseReg.setStatus(CommonFinalData.USE_REG_STATUS_1);
                yyUseReg.setRentFree(CommonFinalData.USE_REG_RENT_FREE_2);
                yyUseReg.setObjName(yyContract.getLeaser());
                yyUseReg.setCreditCode(yyContract.getLeaserCode());
                yyUseReg.setContactName(yyContract.getContactPerson());
                yyUseReg.setContactPhone(yyContract.getContactPhone());
                yyUseReg.setStartTime(new Timestamp(yyContract.getStartRentDate().getTime()));
                yyUseReg.setEndTime(new Timestamp(yyContract.getEndRentDate().getTime()));
                yyUseReg.setCreateTime(new Timestamp(System.currentTimeMillis()));

                insertYyUseRegList.add(yyUseReg);
            }
            this.yyUseRegMapper.insertBatch(insertYyUseRegList);
        }
    }

    /**
     * 处理业务信息的业务类型
     * @param ywAgencyBusinessPO po
     * @return 业务类型
     */
    private String disposeBusinessType(YwAgencyBusinessPO ywAgencyBusinessPO) {
        Byte businessTypeTemp = ywAgencyBusinessPO.getBusinessType();
        String webSignType = ywAgencyBusinessPO.getWebSignType();
        Byte applyPeopleType = ywAgencyBusinessPO.getApplyPeopleType();

        //1产业用房租赁 2商业用房租赁 3住宅用房租赁 4南山软件园租赁
        String type = "";
        if(null != businessTypeTemp){
            switch (businessTypeTemp){
                case 1:
                    type = "产业用房租赁-";
                    break;
                case 2:
                    type = "商业用房租赁-";
                    break;
                case 3:
                    type = "住宅用房租赁-";
                    break;
                case 4:
                    type = "南山软件园租赁-";
                    break;
                default:
                    break;
            }
        }

        //1 新签 2 续签
        if(!StringUtils.isEmpty(webSignType)){
            type += webSignType.equals(BusinessTypeEnum.NEW_SIGN.getCode().toString()) ?
                    BusinessTypeEnum.NEW_SIGN.getName() :
                    (webSignType.equals(BusinessTypeEnum.RENEW_SIGN.getCode().toString()) ? BusinessTypeEnum.RENEW_SIGN.getName() : "");
        }

        //1 法人申请 2委托人申请
        if(null != applyPeopleType){
            type += applyPeopleType.equals(Byte.parseByte("1")) ?
                    "法人申请" :
                    (applyPeopleType.equals(Byte.parseByte("2")) ? "委托人申请" : "");
        }
        return type;
    }
}
