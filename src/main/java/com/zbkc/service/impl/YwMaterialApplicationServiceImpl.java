package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.mapper.YwMaterialApplicationMapper;
import com.zbkc.model.dto.YwMaterialApplicationDTO;
import com.zbkc.model.po.YwAgencyBusinessPO;
import com.zbkc.model.po.YwMaterialApplicationPO;
import com.zbkc.model.vo.PageVO;
import com.zbkc.service.YwMaterialApplicationService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Wrapper;
import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/14
 */
@Service
public class YwMaterialApplicationServiceImpl extends ServiceImpl<YwMaterialApplicationMapper, YwMaterialApplicationPO> implements YwMaterialApplicationService {

    @Autowired
    private YwMaterialApplicationMapper ywMaterialApplicationMapper;


    @Override
    public int insertMaterialApplication(YwMaterialApplicationDTO ywMaterialApplicationDTO) {

        YwMaterialApplicationPO ywMaterialApplicationPO = new YwMaterialApplicationPO();
        BeanUtils.copyProperties(ywMaterialApplicationDTO , ywMaterialApplicationPO);

        return this.ywMaterialApplicationMapper.insert(ywMaterialApplicationPO);
    }

    @Override
    public PageVO<YwMaterialApplicationPO> pageYwAgencyBusinessList(YwMaterialApplicationDTO ywMaterialApplicationDTO) {

        if(ywMaterialApplicationDTO.getP()<=0){
            return null;
        }

        int expectPage = ywMaterialApplicationDTO.getP();
        int queryStartRowNumber = (ywMaterialApplicationDTO.getP() - 1) * ywMaterialApplicationDTO.getSize();
        List<YwMaterialApplicationPO> list = null;
        int total = 0;

        ywMaterialApplicationDTO.setP(queryStartRowNumber);
        list = this.ywMaterialApplicationMapper.selectYwMaterialApplicationByPage(ywMaterialApplicationDTO);
        total = this.ywMaterialApplicationMapper.selectYwMaterialApplicationByPageCount(ywMaterialApplicationDTO);

        PageVO pageVO = new PageVO();
        pageVO.setList(list);
        pageVO.setTotal(total);
        pageVO.setP(expectPage);
        pageVO.setSize(ywMaterialApplicationDTO.getSize());
        return pageVO;
    }

}
