package com.zbkc.service.impl;


import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.common.enums.BusinessTypeEnum;
import com.zbkc.common.enums.ContractFileNameEnum;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.enums.ExportEnum;
import com.zbkc.common.utils.*;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.*;
import com.zbkc.model.dto.*;
import com.zbkc.model.dto.export.*;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.*;
import com.zbkc.service.ActivitiBaseService;
import com.zbkc.service.YwAgencyBusinessService;
import com.zbkc.service.YyContractService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 运营管理——合同管理 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-13
 */
@Service
@Slf4j
public class YyContractServiceImpl extends ServiceImpl<YyContractMapper, YyContract> implements YyContractService {

    @Autowired
    private ToolUtils toolUtils;
    @Autowired
    private YyContractMapper yyContractMapper;
    @Autowired
    private SysFilePathMapper sysFilePathMapper;
    @Autowired
    private YySplitRentMapper yySplitRentMapper;
    @Autowired
    private YyBillManagementMapper yyBillManagementMapper;

    @Autowired
    private YwHandoverDetailsMapper ywHandoverDetailsMapper;

    @Autowired
    private ActivitiBaseService activitiBaseService;

    @Autowired
    private WyBasicInfoMapper wyBasicInfoMapper;

    @Autowired
    private LabelManageMapper labelManageMapper;

    @Autowired
    private SysAttachmentMapper sysAttachmentMapper;

    @Autowired
    private SysDataTypeMapper sysDataTypeMapper;

    @Autowired
    private YwAgencyBusinessService ywAgencyBusinessService;
    @Autowired
    ResourceLoader resourceLoader;
    @Autowired
    private SysAreaMapper sysAreaMapper;


    @Value("${file.win-name}")
    private String winUrl;
    @Value("${file.linux-name}")
    private String linuxUrl;
    @Autowired
    private CreateFileUtil createFileUtil;

    @Autowired
    private YyContractWyRelationMapper yyContractWyRelationMapper;

    @Autowired
    private YwAgencyBusinessMapper ywAgencyBusinessMapper;

    private final static ExecutorService executorService = new ThreadPoolExecutor(4,
            13,
            60,
            TimeUnit.SECONDS,
            new LinkedBlockingQueue<Runnable>(256), Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.AbortPolicy());


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO addAndUpdContract(YyContractDTO dto1) throws BusinessException {

        List<LabelManage> insertLabelManageList = new ArrayList<>();
        List<YySplitRent> insertSplitRentList = new ArrayList<>();
        List<YySplitRent> updateSplitRentList = new ArrayList<>();

        YyContractDTO dto = new YyContractDTO();
        dto = dto1;
        if (dto.getInfo().getId() == null || dto.getInfo().getId() == 0) {
            //新增合同
            long startTime = System.currentTimeMillis();
            dto.setInfo(dto1.getInfo());

            YyContract info = new YyContract();
            info.setWyBasicInfoId(dto.getInfo().getWyBasicInfoId());


            Long id = IdWorker.getId();

            wyBasicInfoMapper.updWord(dto.getInfo().getWyBasicInfoId(), dto.getInfo().getMonthlyRent());

            info.setId(id);
            info.setTableId(String.valueOf(id));
            info.setTemplateType(dto.getInfo().getTemplateType());
            info.setType(dto.getInfo().getType());
            info.setLeaseType(dto.getInfo().getLeaseType());
            info.setContractType(dto.getInfo().getContractType());
            info.setApplicationType(dto.getInfo().getApplicationType());
            info.setLeaser(dto.getInfo().getLeaser());
            info.setLeaserType(dto.getInfo().getLeaserType());
            info.setLeaserCode(dto.getInfo().getLeaserCode());
            info.setLeaserPhone(dto.getInfo().getLeaserPhone());
            info.setLegalPerson(dto.getInfo().getLegalPerson());
            info.setLegalPhone(dto.getInfo().getLegalPhone());
            info.setLegalType(dto.getInfo().getLegalType());
            info.setLegalCode(dto.getInfo().getLegalCode());
            info.setLegalAddress(dto.getInfo().getLegalAddress());
            info.setContactPerson(dto.getInfo().getContactPerson());
            info.setContactPhone(dto.getInfo().getContactPhone());
            info.setMonthlyRent(dto.getInfo().getMonthlyRent());
            info.setUserId(dto.getInfo().getUserId());
            info.setBusinessLicenseAddr(dto.getInfo().getBusinessLicenseAddr());

            if (dto.getInfo().getApplicationType() == 2) {
                info.setClientPerson(dto.getInfo().getClientPerson());
                info.setClientPhone(dto.getInfo().getClientPhone());
                info.setClientType(dto.getInfo().getClientType());
                info.setClientCode(dto.getInfo().getClientCode());
                info.setClientAddress(dto.getInfo().getClientAddress());
            }
            info.setCompanyInfoRemark(dto.getInfo().getCompanyInfoRemark());

            info.setIsEconomicCommit(dto.getInfo().getIsEconomicCommit()==null?2:dto.getInfo().getIsEconomicCommit());
            info.setEconomicCommit(dto.getInfo().getEconomicCommit()==null?"":dto.getInfo().getEconomicCommit());
            Long houseOwnershipId = IdWorker.getId();
            info.setHouseOwnershipId(houseOwnershipId);
            if (dto.getInfo().getHouseOwnershipId() != null) {

                LabelManage manage = new LabelManage();
                manage.setId(IdWorker.getId());
                manage.setLabelId(houseOwnershipId);
                manage.setSysDataTypeId(dto.getInfo().getHouseOwnershipId());

                insertLabelManageList.add(manage);
            }

            Long houseProofId = IdWorker.getId();
            info.setHouseProofId(houseProofId);
            if (dto.getInfo().getHouseProofId() != null) {
                if (dto.getInfo().getHouseProofId() == 86) {
                    info.setCertificationNumber(dto.getInfo().getCertificationNumber());
                }
                LabelManage manage1 = new LabelManage();
                manage1.setId(IdWorker.getId());
                manage1.setLabelId(houseProofId);
                manage1.setSysDataTypeId(dto.getInfo().getHouseProofId());
                insertLabelManageList.add(manage1);
            }

            info.setCertificationNumber(dto.getInfo().getCertificationNumber());
            info.setGuidePrice(dto.getInfo().getGuidePrice());
            info.setDiscount(dto.getInfo().getDiscount());

            if (dto.getInfo().getIsSplitRent() == 1) {
                info.setIsSplitRent(dto.getInfo().getIsSplitRent());
                BigDecimal total = new BigDecimal("0");
                for (YySplitRent rent : dto.getRentList()) {
                    if (rent.getId() == null) {
                        YySplitRent splitRent = new YySplitRent();
                        splitRent.setId(IdWorker.getId());
                        splitRent.setWyBasicInfoId(rent.getWyBasicInfoId());
                        splitRent.setYyContractId(id);
                        splitRent.setPrice(rent.getPrice());
                        splitRent.setArea(rent.getArea());
                        splitRent.setRent(rent.getRent());
                        splitRent.setCreateTime(new Timestamp(System.currentTimeMillis()));
                        insertSplitRentList.add(splitRent);
                    } else {
                        YySplitRent splitRent = new YySplitRent();
                        splitRent.setId(rent.getId());
                        splitRent.setWyBasicInfoId(rent.getWyBasicInfoId());
                        splitRent.setPrice(rent.getPrice());
                        splitRent.setArea(rent.getArea());
                        splitRent.setRent(rent.getRent());
                        updateSplitRentList.add(splitRent);


                    }
                    total = total.add(rent.getRent());
                }
                //合同表存放月租金
                info.setZlMonthlyRent(total.toString());
            } else {
                info.setIsSplitRent(dto.getInfo().getIsSplitRent());
                info.setRentPrice(dto.getInfo().getRentPrice());
                //合同表存放月租金
                info.setZlMonthlyRent(dto.getInfo().getMonthlyRent().toString());
            }

            info.setFreeRent(dto.getInfo().getFreeRent());
            info.setStartRentDate(TimeUtils.parseDate(dto.getStartRentDate(),"yyyy-MM-dd"));
            info.setEndRentDate(TimeUtils.parseDate(dto.getEndRentDate(),"yyyy-MM-dd"));
            info.setFirstRentTime(TimeUtils.parseDate(dto.getFirstRentTime(),"yyyy-MM-dd"));
            info.setFirstRentMoney(dto.getInfo().getFirstRentMoney() == null ? new BigDecimal("0") : dto.getInfo().getFirstRentMoney());
            info.setDepositAmount(dto.getInfo().getDepositAmount() == null ? new BigDecimal("0") : dto.getInfo().getDepositAmount());
            info.setLeaseUse(dto.getInfo().getLeaseUse() == null ? 0 : dto.getInfo().getLeaseUse());
            info.setEconomicCommit(dto.getInfo().getEconomicCommit() == null ? "" : dto.getInfo().getEconomicCommit());
            info.setRemark(dto.getInfo().getRemark() == null ? "" : dto.getInfo().getRemark());
            info.setWaterFee(dto.getInfo().getWaterFee() == null ? new BigDecimal("0") : dto.getInfo().getWaterFee());
            info.setElectricFee(dto.getInfo().getElectricFee() == null ? new BigDecimal("0") : dto.getInfo().getElectricFee());
            info.setGasFee(dto.getInfo().getGasFee() == null ? new BigDecimal("0") : dto.getInfo().getGasFee());
            info.setPropertyManagementFee(dto.getInfo().getPropertyManagementFee() == null ? new BigDecimal("0") : dto.getInfo().getPropertyManagementFee());
            info.setOtherFee(dto.getInfo().getOtherFee() == null ? new BigDecimal("0") : dto.getInfo().getOtherFee());
            info.setUserId(dto.getInfo().getUserId() == null ? 0 : dto.getInfo().getUserId());
            info.setCreateTime(new Timestamp(System.currentTimeMillis()));
            info.setUpdateTime(new Timestamp(System.currentTimeMillis()));
            info.setHouseCode(dto.getInfo().getHouseCode() == null ? "" : dto.getInfo().getHouseCode());
            info.setRentalArea(dto.getInfo().getRentalArea() == null ? new BigDecimal("0") : dto.getInfo().getRentalArea());
            info.setMonthlyRent(dto.getInfo().getMonthlyRent() == null ? new BigDecimal("0") : dto.getInfo().getMonthlyRent());

            //新增业务办理-待办业务表记录
            long ywAgencyBusinessId = IdWorker.getId();
            addBusinessRecordInContract(info, ywAgencyBusinessId);

            //附件处理
            disposeFile(dto, info, ywAgencyBusinessId);
            yyContractMapper.addContract(info);
            /**
             * 批量插入、更新 标签、分组
             */
            this.labelManageMapper.insertBatch(insertLabelManageList);
            if(!insertSplitRentList.isEmpty()){
                this.yySplitRentMapper.insertBatch(insertSplitRentList);
            }
            if(!updateSplitRentList.isEmpty()){
                this.yySplitRentMapper.updateBatch(updateSplitRentList);
            }
            //合同物业关联表插入 兼容多个物业
            String wyBasicInfoIdStr = dto.getInfo().getWyBasicInfoIdStr();
            if(!wyBasicInfoIdStr.isEmpty()){
                String[] wyIdArr = wyBasicInfoIdStr.split(",");
                List<YyContractWyRelationPO> insertRelationList = new ArrayList<>();
                for (String wyId : wyIdArr) {
                    if(wyId.isEmpty()){
                        continue;
                    }
                    YyContractWyRelationPO yyContractWyRelationPO = new YyContractWyRelationPO();
                    yyContractWyRelationPO.setId(IdWorker.getId());
                    yyContractWyRelationPO.setYyContractId(id);
                    yyContractWyRelationPO.setWyBasicInfoId(Long.parseLong(wyId));
                    yyContractWyRelationPO.setYwBusinessId(ywAgencyBusinessId);
                    insertRelationList.add(yyContractWyRelationPO);
                }
                this.yyContractWyRelationMapper.insertBatch(insertRelationList);
            }

            long endTime = System.currentTimeMillis();
            log.info("程序运行时间： " + (endTime - startTime) + "ms");
        } else {
            //修改合同
            dto.setInfo(dto1.getInfo());
            YyContract info = new YyContract();
            Long id = dto.getInfo().getId();

            wyBasicInfoMapper.updWord(dto.getInfo().getWyBasicInfoId(), dto.getInfo().getMonthlyRent());

            info.setId(id);
            info.setTableId(String.valueOf(id));
            info.setTemplateType(dto.getInfo().getTemplateType());
            info.setType(dto.getInfo().getType());
            info.setLeaseType(dto.getInfo().getLeaseType());
            info.setContractType(dto.getInfo().getContractType());
            info.setApplicationType(dto.getInfo().getApplicationType());
            info.setLeaser(dto.getInfo().getLeaser());
            info.setLeaserType(dto.getInfo().getLeaserType());
            info.setLeaserCode(dto.getInfo().getLeaserCode());
            info.setLeaserPhone(dto.getInfo().getLeaserPhone());
            info.setLegalPerson(dto.getInfo().getLegalPerson());
            info.setLegalPhone(dto.getInfo().getLegalPhone());
            info.setLegalType(dto.getInfo().getLegalType());
            info.setLegalCode(dto.getInfo().getLegalCode());
            info.setLegalAddress(dto.getInfo().getLegalAddress());
            info.setContactPerson(dto.getInfo().getContactPerson());
            info.setContactPhone(dto.getInfo().getContactPhone());
            info.setUserId(dto.getInfo().getUserId());
            info.setBusinessLicenseAddr(dto.getInfo().getBusinessLicenseAddr());
            if (dto.getInfo().getApplicationType() == 2) {
                info.setClientPerson(dto.getInfo().getClientPerson());
                info.setClientPhone(dto.getInfo().getClientPhone());
                info.setClientType(dto.getInfo().getClientType());
                info.setClientCode(dto.getInfo().getClientCode());
                info.setClientAddress(dto.getInfo().getClientAddress());
            }
            info.setCompanyInfoRemark(dto.getInfo().getCompanyInfoRemark());
            info.setWyBasicInfoId(dto.getInfo().getWyBasicInfoId());
            info.setIsEconomicCommit(dto.getInfo().getIsEconomicCommit()==null?2:dto.getInfo().getIsEconomicCommit());
            info.setEconomicCommit(dto.getInfo().getEconomicCommit()==null?"":dto.getInfo().getEconomicCommit());

            Long houseOwnershipId = IdWorker.getId();
            info.setHouseOwnershipId(houseOwnershipId);
            if (dto.getInfo().getHouseOwnershipId() != null) {
                LabelManage manage = new LabelManage();
                manage.setId(IdWorker.getId());
                manage.setLabelId(houseOwnershipId);
                manage.setSysDataTypeId(dto.getInfo().getHouseOwnershipId());
                insertLabelManageList.add(manage);
            }

            Long houseProofId = IdWorker.getId();
            info.setHouseProofId(houseProofId);
            if (dto.getInfo().getHouseProofId() != null) {
                if (dto.getInfo().getHouseProofId() == 86) {
                    info.setCertificationNumber(dto.getInfo().getCertificationNumber());
                }
                LabelManage manage1 = new LabelManage();
                manage1.setId(IdWorker.getId());
                manage1.setLabelId(houseProofId);
                manage1.setSysDataTypeId(dto.getInfo().getHouseProofId());
                insertLabelManageList.add(manage1);

            }

            info.setCertificationNumber(dto.getInfo().getCertificationNumber());
            info.setGuidePrice(dto.getInfo().getGuidePrice());
            info.setDiscount(dto.getInfo().getDiscount());

            if (dto.getInfo().getIsSplitRent() == 1) {
                info.setIsSplitRent(dto.getInfo().getIsSplitRent());

                BigDecimal total = new BigDecimal("0");
                for (YySplitRent rent : dto.getRentList()) {
                    if (rent.getId() == null) {
                        YySplitRent splitRent = new YySplitRent();
                        splitRent.setId(IdWorker.getId());
                        splitRent.setWyBasicInfoId(rent.getWyBasicInfoId());
                        splitRent.setYyContractId(id);
                        splitRent.setPrice(rent.getPrice());
                        splitRent.setArea(rent.getArea());
                        splitRent.setRent(rent.getRent());
                        splitRent.setCreateTime(new Timestamp(System.currentTimeMillis()));
                        insertSplitRentList.add(splitRent);
                    } else {
                        YySplitRent splitRent = new YySplitRent();
                        splitRent.setId(rent.getId());
                        splitRent.setWyBasicInfoId(rent.getWyBasicInfoId());
                        splitRent.setYyContractId(dto.getInfo().getId());
                        splitRent.setPrice(rent.getPrice());
                        splitRent.setArea(rent.getArea());
                        splitRent.setRent(rent.getRent());
                        updateSplitRentList.add(splitRent);
                    }
                    total = total.add(rent.getRent());
                }
                //合同表存放月租金
                info.setZlMonthlyRent(total.toString());
            } else {
                info.setIsSplitRent(dto.getInfo().getIsSplitRent());
                info.setRentPrice(dto.getInfo().getRentPrice());
                //合同表存放月租金
                info.setZlMonthlyRent(dto.getInfo().getMonthlyRent().toString());
            }

            info.setFreeRent(dto.getInfo().getFreeRent());
            info.setStartRentDate(TimeUtils.parseDate(dto.getStartRentDate(),"yyyy-MM-dd"));
            info.setEndRentDate(TimeUtils.parseDate(dto.getEndRentDate(),"yyyy-MM-dd"));
            info.setFirstRentTime(TimeUtils.parseDate(dto.getFirstRentTime(),"yyyy-MM-dd"));
            info.setFirstRentMoney(dto.getInfo().getFirstRentMoney());
            info.setDepositAmount(dto.getInfo().getDepositAmount());
            info.setLeaseUse(dto.getInfo().getLeaseUse());
            info.setEconomicCommit(dto.getInfo().getEconomicCommit());
            info.setRemark(dto.getInfo().getRemark());
            info.setWaterFee(dto.getInfo().getWaterFee());
            info.setElectricFee(dto.getInfo().getElectricFee());
            info.setGasFee(dto.getInfo().getGasFee());
            info.setPropertyManagementFee(dto.getInfo().getPropertyManagementFee());
            info.setOtherFee(dto.getInfo().getOtherFee());
            info.setRemark(dto.getInfo().getRemark());
            info.setUserId(dto.getInfo().getUserId());
            info.setCreateTime(new Timestamp(System.currentTimeMillis()));
            info.setUpdateTime(new Timestamp(System.currentTimeMillis()));
            info.setUserId(dto.getInfo().getUserId());
            info.setWyBasicInfoId(dto.getInfo().getWyBasicInfoId());
            info.setLeaseType(dto.getInfo().getLeaseType());

            //附件处理
            disposeFile(dto, info, -1);
            yyContractMapper.updContract(info);
            /**
             * 批量插入、更新 标签、分组
             */
            this.labelManageMapper.insertBatch(insertLabelManageList);
            if(!insertSplitRentList.isEmpty()){
                this.yySplitRentMapper.insertBatch(insertSplitRentList);
            }
            if(!updateSplitRentList.isEmpty()){
                this.yySplitRentMapper.updateBatch(updateSplitRentList);
            }
            //合同物业关联表 先删除旧的数据，再插入新的数据
            this.yyContractWyRelationMapper.deleteByContractId(id);
            Long businessId = this.ywAgencyBusinessMapper.selectIdByRelationId(id);

            //合同物业关联表插入 兼容多个物业
            String wyBasicInfoIdStr = dto.getInfo().getWyBasicInfoIdStr();
            if(!wyBasicInfoIdStr.isEmpty()){
                String[] wyIdArr = wyBasicInfoIdStr.split(",");
                List<YyContractWyRelationPO> insertRelationList = new ArrayList<>();
                for (String wyId : wyIdArr) {
                    if(wyId.isEmpty()){
                        continue;
                    }
                    YyContractWyRelationPO yyContractWyRelationPO = new YyContractWyRelationPO();
                    yyContractWyRelationPO.setId(IdWorker.getId());
                    yyContractWyRelationPO.setYyContractId(id);
                    yyContractWyRelationPO.setWyBasicInfoId(Long.parseLong(wyId));
                    yyContractWyRelationPO.setYwBusinessId(businessId);
                    insertRelationList.add(yyContractWyRelationPO);
                }
                this.yyContractWyRelationMapper.insertBatch(insertRelationList);
            }
        }

        return new ResponseVO(ErrorCodeEnum.SUCCESS, null);

    }


    @Override
    public ResponseVO queryContractList(YyContractIndexDTO dto) {

        if (dto.getP() <= 0) {
            return new ResponseVO(ErrorCodeEnum.PAGE_NUMBER_ERROR, ErrorCodeEnum.PAGE_NUMBER_ERROR.getErrMsg());
        }
        dto.setP((dto.getP() - 1) * dto.getSize());
        List<YyContractList> contractList = yyContractMapper.queryContractList(dto);
        PageVO<YyContractList> listVo = new PageVO<>();
        listVo.setList(contractList);
        listVo.setP(dto.getP());
        listVo.setSize(dto.getSize());
        listVo.setTotal(yyContractMapper.queryContractListTotal(dto).size());
        return new ResponseVO(ErrorCodeEnum.SUCCESS, listVo);
    }

    @Override
    public ResponseVO seeRow(String id) {

        YyContract yyContract = yyContractMapper.seeRow(id);
        YyContractDetail word = yyContractMapper.getWord(id);

        QueryWrapper<YyContractWyRelationPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("yy_contract_id" , id);
        List<YyContractWyRelationPO> relationPOList = this.yyContractWyRelationMapper.selectList(queryWrapper);
        if(null != relationPOList && relationPOList.size() > 0){
            StringBuilder wyBasicInfos = new StringBuilder();
            BigDecimal totalZlMonthlyRent = new BigDecimal(0);
            for (YyContractWyRelationPO yyContractWyRelationPO : relationPOList) {
                Long wyBasicInfoId = yyContractWyRelationPO.getWyBasicInfoId();
                if(null == wyBasicInfoId){
                    continue;
                }
                wyBasicInfos.append(wyBasicInfoId);
                wyBasicInfos.append(",");

                WyBasicInfo wyBasicInfo = this.wyBasicInfoMapper.selectById(wyBasicInfoId);
                BigDecimal zlMonthlyRent = null == wyBasicInfo.getZlMonthlyRent() ? new BigDecimal("0") : wyBasicInfo.getZlMonthlyRent();
                totalZlMonthlyRent = totalZlMonthlyRent.add(zlMonthlyRent);
            }
            
            if(!wyBasicInfos.toString().isEmpty()){
                yyContract.setWyBasicInfoIdStr(wyBasicInfos.toString().substring(0 , wyBasicInfos.toString().length() - 1));
            }

            String zlMonthlyRent = yyContract.getZlMonthlyRent() == null ? "0" : yyContract.getZlMonthlyRent();
            yyContract.setMonthlyRent(new BigDecimal(zlMonthlyRent));
        }

        YyContractDetail dto = new YyContractDetail();
        dto.setId(yyContract.getId());
        dto.setInfo(yyContract);

        if (null != word) {
            dto.setHouseOwnership(word.getHouseOwnership() == null ? 0 : word.getHouseOwnership());
            dto.setHouseProof(word.getHouseProof() == null ? 0 : word.getHouseProof());
        }
        List<YySplitRent> splitRents = yySplitRentMapper.selectAllSplitRent(id);
        dto.setRentList(splitRents);

        List<SysFilePath1> filePaths = sysFilePathMapper.getBydbFileId(dto.getInfo().getResidentNoticeFilePathId());
        dto.setResidentNoticeFile(filePaths);

        List<SysFilePath1> filePaths1 = sysFilePathMapper.getBydbFileId(dto.getInfo().getBusinessLicenseFilePathId());
        dto.setBusinessLicenseFile(filePaths1);

        List<SysFilePath1> filePaths2 = sysFilePathMapper.getBydbFileId(dto.getInfo().getLegalPersonFilePathId());
        dto.setLegalPersonFile(filePaths2);

        List<SysFilePath1> filePaths3 = sysFilePathMapper.getBydbFileId(dto.getInfo().getShareholderFilePathId());
        dto.setShareholderFile(filePaths3);

        List<SysFilePath1> filePaths4 = sysFilePathMapper.getBydbFileId(dto.getInfo().getLegalRepresentativeFilePathId());
        dto.setLegalRepresentativeFile(filePaths4);

        List<SysFilePath1> filePaths5 = sysFilePathMapper.getBydbFileId(dto.getInfo().getLegalPersonPowerFilePathId());
        dto.setLegalPersonPowerFile(filePaths5);

        List<SysFilePath1> filePaths6 = sysFilePathMapper.getBydbFileId(dto.getInfo().getHandlingPersonCardFilePathId());
        dto.setHandlingPersonCardFile(filePaths6);

        List<SysFilePath1> filePaths7 = sysFilePathMapper.getBydbFileId(dto.getInfo().getMeetingMinutesFilePathId());
        dto.setMeetingMinutesFile(filePaths7);

        List<SysFilePath1> filePaths8 = sysFilePathMapper.getBydbFileId(dto.getInfo().getBuildingAreaFilePathId());
        dto.setBuildingAreaFile(filePaths8);

        List<SysFilePath1> filePaths9 = sysFilePathMapper.getBydbFileId(dto.getInfo().getCommitmentFilePathId());
        dto.setCommitmentFile(filePaths9);

        List<SysFilePath1> filePaths10 = sysFilePathMapper.getBydbFileId(dto.getInfo().getSettlementNoticeFilePathId());
        dto.setSettlementNoticeFile(filePaths10);

        List<SysFilePath1> filePaths11 = sysFilePathMapper.getBydbFileId(dto.getInfo().getMeetingFilePathId());
        dto.setMeetingFile(filePaths11);

        List<SysFilePath1> filePaths12 = sysFilePathMapper.getBydbFileId(dto.getInfo().getRentReportFilePathId());
        dto.setRentReportFile(filePaths12);

        List<SysFilePath1> filePaths13 = sysFilePathMapper.getBydbFileId(dto.getInfo().getPersonFilePathId());
        dto.setPersonFile(filePaths13);

        List<SysFilePath1> filePaths14 = sysFilePathMapper.getBydbFileId(dto.getInfo().getDigitalNoticeFilePathId());
        dto.setDigitalNoticeFile(filePaths14);

        List<SysFilePath1> filePaths15 = sysFilePathMapper.getBydbFileId(dto.getInfo().getContractFilePathId());
        dto.setContractFile(filePaths15);
        //自定义附件
        List<String> nameList = sysFilePathMapper.getName(dto.getInfo().getCustRightFileId());
        Map<String, List<SysFilePath1>> custRightFileMap = new HashMap<>();
        for (String name : nameList) {
            custRightFileMap.put(name, sysFilePathMapper.getCustFile(dto.getInfo().getCustRightFileId(), name));
        }
        dto.setCustomFileMap(custRightFileMap);
        List<SysFilePath1> filePaths16 = sysFilePathMapper.getBydbFileId(dto.getInfo().getRenewalSignFilePathId());
        dto.setRenewalSignFile(filePaths16);

        return new ResponseVO(ErrorCodeEnum.SUCCESS, dto);
    }

    @Override
    public ResponseVO queryDepositList(DepositDTO dto) {
        if (dto.getP() <= 0) {
            return new ResponseVO(ErrorCodeEnum.SUCCESS, null);
        }
        dto.setP((dto.getP() - 1) * dto.getSize());
        List<YyDepositHome> list = yyContractMapper.queryDepositList(dto);
        PageVO<YyDepositHome> pageVO = new PageVO<>();
        pageVO.setList(list);
        pageVO.setP(dto.getP());
        pageVO.setSize(dto.getSize());
        pageVO.setTotal(yyContractMapper.queryDepositListTotal(dto).size());
        return new ResponseVO(ErrorCodeEnum.SUCCESS, pageVO);
    }

    @Override
    public ResponseVO seeDepositRow(String id, Long wyId) {
        DepositDetail row = yyContractMapper.queryDepositDetail(id, wyId);
        List<SysFilePath1> file = sysFilePathMapper.getBydbFileId(row.getContractFilePathId());
        row.setContractFile(file);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, row);
    }

    @Override
    public ResponseVO editDeposit(EditDepositDTO dto) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS, yyContractMapper.editDeposit(dto));
    }

    @Override
    public ResponseVO selectPreContract(YjContractDTO dto) {
        if (dto.getP() <= 0) {
            return new ResponseVO(ErrorCodeEnum.PAGE_NUMBER_ERROR, ErrorCodeEnum.PAGE_NUMBER_ERROR.getErrMsg());
        }
        dto.setP((dto.getP() - 1) * dto.getSize());
        List<YjContractReport> list = yyContractMapper.selectPreContract(dto);
        if (CollectionUtils.isEmpty(list)) {
            return new ResponseVO();
        }

        ReportPageVO<YjContractReport> reportPageVO = new ReportPageVO<>();
        List<YjContractReport> yjContractReportList = yyContractMapper.selectPreContractTotal(dto);
        reportPageVO.setList(list);
        reportPageVO.setP(dto.getP());
        reportPageVO.setSize(dto.getSize());
        reportPageVO.setTotal(yjContractReportList.size());

        List<String> collect = yjContractReportList.stream().filter(yjContractReport -> yjContractReport.getWyName() != null).map(YjContractReport::getWyName).collect(Collectors.toList());
        int size = new HashSet<>(collect).size();
        BigDecimal totalRentalArea = yjContractReportList.stream()
                .filter(yjContractReport -> yjContractReport.getRentalArea() != null)
                .map(YjContractReport::getRentalArea).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal totalMonthlyRent = yjContractReportList.stream()
                .filter(yjContractReport -> yjContractReport.getMonthlyRent() != null)
                .map(YjContractReport::getMonthlyRent).reduce(BigDecimal.ZERO, BigDecimal::add);
        reportPageVO.setWyLeaseNum(size);
        reportPageVO.setTotalRentalArea(totalRentalArea);
        reportPageVO.setTotalMonthlyRent(totalMonthlyRent);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, reportPageVO);

    }

    @Override
    public ResponseVO selectIndustReview(YjContractDTO dto) {
        if (dto.getP() <= 0) {
            return new ResponseVO(ErrorCodeEnum.PAGE_NUMBER_ERROR, ErrorCodeEnum.PAGE_NUMBER_ERROR.getErrMsg());
        }

        dto.setP((dto.getP() - 1) * dto.getSize());
        List<IndReviewReport> list = yyContractMapper.selectIndustReview(dto);

        if (CollectionUtils.isEmpty(list)) {
            return new ResponseVO();
        }
        for (IndReviewReport indReviewReport : list) {
            indReviewReport.setType(indReviewReport.getType() == null ? 0 : indReviewReport.getType());
            if (indReviewReport.getType() == 1) {
                indReviewReport.setIsCommit("是");
            } else {
                indReviewReport.setIsCommit("否");
            }
        }
        ReportPageVO<IndReviewReport> reportPageVO = new ReportPageVO<>();
        List<IndReviewReport> indReviewReportList = yyContractMapper.selectIndustReviewTotal(dto);
        BigDecimal totalRentalArea = indReviewReportList.stream().filter(IndReviewReport -> IndReviewReport.getRentalArea() != null).map(IndReviewReport::getRentalArea).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal totalMonthlyRent = indReviewReportList.stream().filter(IndReviewReport -> IndReviewReport.getMonthlyRent() != null).map(IndReviewReport::getMonthlyRent).reduce(BigDecimal.ZERO, BigDecimal::add);
        int contributionNum = indReviewReportList.stream().filter(IndReviewReport -> IndReviewReport.getType() != null).filter(IndReviewReport -> IndReviewReport.getType() == 1).collect(Collectors.toList()).size();
        List<String> nameList = indReviewReportList.stream().filter(indReviewReport -> indReviewReport.getWyName() != null).map(IndReviewReport::getWyName).collect(Collectors.toList());
        int size = new HashSet<>(nameList).size();

        reportPageVO.setWyLeaseNum(size);
        reportPageVO.setTotalRentalArea(totalRentalArea);
        reportPageVO.setTotalMonthlyRent(totalMonthlyRent);
        reportPageVO.setContributionNum(contributionNum);
        reportPageVO.setList(list);
        reportPageVO.setP(dto.getP());
        reportPageVO.setSize(dto.getSize());
        reportPageVO.setTotal(indReviewReportList.size());
        return new ResponseVO(ErrorCodeEnum.SUCCESS, reportPageVO);
    }

    @Override
    public ResponseVO leaseDetail(LeaseDetailDTO dto) {
        if (dto.getP() <= 0) {
            return new ResponseVO(ErrorCodeEnum.SUCCESS, null);
        }
        dto.setP((dto.getP() - 1) * dto.getSize());
        List<LeaseDetailVo> list = yyContractMapper.selectleaseDetail(dto);

        ReportPageVO<LeaseDetailVo> reportPageVO = new ReportPageVO<>();
        List<LeaseDetailVo> leaseDetailVoList = yyContractMapper.selectleaseDetailTotal(dto);
        List<String> nameList = leaseDetailVoList.stream().filter(leaseDetailVo -> leaseDetailVo.getWyName() != null).map(LeaseDetailVo::getWyName).collect(Collectors.toList());
        int size = new HashSet<>(nameList).size();
        BigDecimal totalRentalArea = leaseDetailVoList.stream().filter(leaseDetailVo -> leaseDetailVo.getRentalArea() != null).map(LeaseDetailVo::getRentalArea).reduce(BigDecimal.ZERO, BigDecimal::add);

        reportPageVO.setList(list);
        reportPageVO.setWyLeaseNum(size);
        reportPageVO.setTotalRentalArea(totalRentalArea);
        reportPageVO.setP(dto.getP());
        reportPageVO.setSize(dto.getSize());
        reportPageVO.setTotal(leaseDetailVoList.size());
        return new ResponseVO(ErrorCodeEnum.SUCCESS, reportPageVO);
    }

    @Override
    public ResponseVO emptyDetail(EmptyDetailDTO dto) {
        if (dto.getP() <= 0) {
            return new ResponseVO(ErrorCodeEnum.SUCCESS, null);
        }
        dto.setP((dto.getP() - 1) * dto.getSize());
        List<EmptyDetailVo> list = yyContractMapper.selectEmptyDetail(dto);
        ReportPageVO<EmptyDetailVo> reportPageVO = new ReportPageVO<>();

        List<EmptyDetailVo> emptyDetailVoList = yyContractMapper.selectEmptyDetailTotal(dto);
        List<String> nameList = emptyDetailVoList.stream().filter(emptyDetailVo -> emptyDetailVo.getWyName() != null).map(EmptyDetailVo::getWyName).collect(Collectors.toList());
        int size = new HashSet<>(nameList).size();
        //空置面积
        BigDecimal totalEmptylArea = emptyDetailVoList.stream().filter(emptyDetailVo -> emptyDetailVo.getRentalArea() != null).map(EmptyDetailVo::getRentalArea).reduce(BigDecimal.ZERO, BigDecimal::add);
        reportPageVO.setP(dto.getP());
        reportPageVO.setSize(dto.getSize());
        reportPageVO.setTotal(emptyDetailVoList.size());
        reportPageVO.setList(list);
        reportPageVO.setVacantArea(totalEmptylArea);
        reportPageVO.setWyVacantNum(size);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, reportPageVO);
    }


    @Override
    public ResponseVO exportPreContract(ExportPreContactDTO dto, HttpServletResponse response) {

        List<YjContractReport> vo = yyContractMapper.selectPreContractTotal(dto.getYjContractDTO());
        for (YjContractReport yjContractReport : vo) {
            yjContractReport.setLeaseUse(yjContractReport.getLeaseUse() == null ? 0 : yjContractReport.getLeaseUse());
            if (yjContractReport.getLeaseUse() == 1) {
                yjContractReport.setLeaseUseStr(ExportEnum.OFFICE.getName());
            }
            if (yjContractReport.getLeaseUse() == 2) {
                yjContractReport.setLeaseUseStr(ExportEnum.COMPREHENSIVE_RESEARCH.getName());
            }
            if (yjContractReport.getLeaseUse() == 3) {
                yjContractReport.setLeaseUseStr(ExportEnum.INDUSTRY_RESEARCH.getName());
            }
            if (yjContractReport.getLeaseUse() == 4) {
                yjContractReport.setLeaseUseStr(ExportEnum.RESEARCH_AND_DEVELOPMENT.getName());
            }


        }

        Map<String, String> keyMap = dto.getKeyMap();
        //需要显示的字段名
        String[] needShowKeyArr = dto.getNeedShowKeys().split(",");
        //字段名对应的中文名
        String[] headers = new String[needShowKeyArr.length];
        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }


        Calendar now = Calendar.getInstance();
        String fileName = now.get(Calendar.YEAR) + "-" + (now.get(Calendar.MONTH) + 1) + "-" + now.get(Calendar.DAY_OF_MONTH) + "CONTRACT.xls";

        try {
            ExcelUtils.getExcel(response, headers, needShowKeyArr, vo, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    public ResponseVO exportIndust(ExportPreContactDTO dto, HttpServletResponse response) {

        List<IndReviewReport> vo = yyContractMapper.selectIndustReviewTotal(dto.getYjContractDTO());
        //剩余天数与合同年限
        for (IndReviewReport indReviewReport : vo) {
            indReviewReport.setLeaseUse(indReviewReport.getLeaseUse() == null ? 0 : indReviewReport.getLeaseUse());
            if (indReviewReport.getLeaseUse() == 1) {
                indReviewReport.setLeaseUseStr(ExportEnum.OFFICE.getName());
            }
            if (indReviewReport.getLeaseUse() == 2) {
                indReviewReport.setLeaseUseStr(ExportEnum.COMPREHENSIVE_RESEARCH.getName());
            }
            if (indReviewReport.getLeaseUse() == 3) {
                indReviewReport.setLeaseUseStr(ExportEnum.INDUSTRY_RESEARCH.getName());
            }
            if (indReviewReport.getLeaseUse() == 4) {
                indReviewReport.setLeaseUseStr(ExportEnum.RESEARCH_AND_DEVELOPMENT.getName());
            }
        }

        Map<String, String> keyMap = dto.getKeyMap();
        //需要显示的字段名
        String[] needShowKeyArr = dto.getNeedShowKeys().split(",");
        //字段名对应的中文名
        String[] headers = new String[needShowKeyArr.length];
        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        try {
            ExcelUtils.getExcel(response, headers, needShowKeyArr, vo, "产业用房报表" +
                    ".xls");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseVO exportLease(ExportLeaseDTO dto, HttpServletResponse response) {


        List<LeaseDetailVo> vo = yyContractMapper.selectleaseDetailTotal(dto.getLeaseDetailDTO());
        Map<String, String> keyMap = dto.getKeyMap();
        //需要显示的字段名
        String[] needShowKeyArr = dto.getNeedShowKeys().split(",");
        //字段名对应的中文名
        String[] headers = new String[needShowKeyArr.length];
        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        try {
            ExcelUtils.getExcel(response, headers, needShowKeyArr, vo, "租赁明细报表.xls");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseVO exportEmpty(ExportEmptyDTO dto, HttpServletResponse response) {
        List<EmptyDetailVo> vo = yyContractMapper.selectEmptyDetailTotal(dto.getEmptyDetailDTO());
        Map<String, String> keyMap = dto.getKeyMap();
        //需要显示的字段名
        String[] needShowKeyArr = dto.getNeedShowKeys().split(",");
        //字段名对应的中文名
        String[] headers = new String[needShowKeyArr.length];
        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        try {
            ExcelUtils.getExcel(response, headers, needShowKeyArr, vo, "空置明细报表.xls");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ResponseVO selectWyNameByType(String wyType) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS, yyBillManagementMapper.selectWyNameByType(wyType));
    }

    @Override
    public YyContract selectOneByBusinessId(String businessId) {
        if(StringUtils.isEmpty(businessId)){
            throw new BusinessException(ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND);
        }
        YyContract yyContract = yyContractMapper.selectOneByBusinessId(Long.parseLong(businessId));
        if(null==yyContract){
          throw new BusinessException(ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND);
        }
        return yyContract;
    }

    @Override
    public ResponseVO exportDeposit(ExportDepositDTO dto, HttpServletResponse response) {
        List<YyDepositHome> list = yyContractMapper.queryDepositListTotal(dto.getInfo());

        for (YyDepositHome yyDepositHome : list) {
            //防止数据库返回字段值为null报异常
            yyDepositHome.setPaymentStatus(yyDepositHome.getPaymentStatus() == null ? 0 : yyDepositHome.getPaymentStatus());
            //将缴费状态转为中文方便导出
            switch (yyDepositHome.getPaymentStatus()) {
                case 1:
                    yyDepositHome.setPaymentStatusStr(ExportEnum.NOT_PAYMENT.getName());
                    break;
                case 2:
                    yyDepositHome.setPaymentStatusStr(ExportEnum.PARTIAL_PAYMENT.getName());
                    break;
                case 3:
                    yyDepositHome.setPaymentStatusStr(ExportEnum.ALREADY_PAYMENT.getName());
                    break;
                case 4:
                    yyDepositHome.setPaymentStatusStr(ExportEnum.OWE_PAYMENT.getName());
                    break;
                case 5:
                    yyDepositHome.setPaymentStatusStr(ExportEnum.ALREADY_MAKE_UP_PAYMENT.getName());
                    break;
                case 6:
                    yyDepositHome.setPaymentStatusStr(ExportEnum.PREPARE_PAYMENT.getName());
                    break;
                default:
                    break;
            }
        }
        Map<String, String> keyMap = dto.getKeyMap();

        //需要显示的字段名
        //字段名对应的中文名
        String[] headers = null;
        String[] needShowKeyArr = null;

        if (StringUtils.isEmpty(dto.getNeedShowKeys())) {
            //不传getNeedShowKeys 显示所有字段
            Set<String> keys = keyMap.keySet();
            needShowKeyArr = keys.toArray(new String[keys.size()]);
            headers = new String[keys.size()];
        } else {
            needShowKeyArr = dto.getNeedShowKeys().split(",");
            //字段名对应的中文名
            headers = new String[needShowKeyArr.length];
        }

        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        try {
            ExcelUtils.getExcel(response, headers, needShowKeyArr, list, "deposit.xls");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void industReviewThreeMonth(ExportPreContactDTO dto, HttpServletResponse response) {

        List<IndReviewReport> vo = yyContractMapper.industReviewThreeMonth();
        //剩余天数与合同年限
        for (IndReviewReport indReviewReport : vo) {
            String startRentDate = indReviewReport.getStartRentDate();
            String endRentDate = indReviewReport.getEndRentDate();
            if (startRentDate != null && endRentDate != null) {
                Long betweenYears = TimeUtils.getBetweenYears(startRentDate, endRentDate);
                int years = Math.toIntExact(betweenYears);
                indReviewReport.setYear(years);

                String now = TimeUtils.getDateString(new Date());
                Long betweenDays = TimeUtils.getBetweenDays(now, endRentDate);
                int limitDay = Math.toIntExact(betweenDays);
                indReviewReport.setLimitDay(limitDay);
            }
        }

        Map<String, String> keyMap = dto.getKeyMap();
        //需要显示的字段名
        String[] needShowKeyArr = dto.getNeedShowKeys().split(",");
        //字段名对应的中文名
        String[] headers = new String[needShowKeyArr.length];
        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        try {
            Calendar now = Calendar.getInstance();
            String fileName = now.get(Calendar.YEAR) + "-" + (now.get(Calendar.MONTH) + 1) + "-" + now.get(Calendar.DAY_OF_MONTH) + " FH.xls";
            ExcelUtils.getExcel(response, headers, needShowKeyArr, vo, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void preContractTwoMonth(ExportPreContactDTO dto, HttpServletResponse response) {
        List<YjContractReport> vo = yyContractMapper.preContractTwoMonth();
        //剩余天数与合同年限
        for (YjContractReport yjContractReport : vo) {
            String startRentDate = yjContractReport.getStartRentDate();
            String endRentDate = yjContractReport.getEndRentDate();
            if (startRentDate != null && endRentDate != null) {
                Long betweenYears = TimeUtils.getBetweenYears(startRentDate, endRentDate);
                int years = Math.toIntExact(betweenYears);
                yjContractReport.setYear(years);
                String now = TimeUtils.getDateString(new Date());
                Long betweenDays = TimeUtils.getBetweenDays(now, endRentDate);
                int limitDay = Math.toIntExact(betweenDays);
                yjContractReport.setLimitDay(limitDay);
            }
        }

        Map<String, String> keyMap = dto.getKeyMap();
        //需要显示的字段名
        String[] needShowKeyArr = dto.getNeedShowKeys().split(",");
        //字段名对应的中文名
        String[] headers = new String[needShowKeyArr.length];
        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }


        Calendar now = Calendar.getInstance();
        String fileName = now.get(Calendar.YEAR) + "-" + (now.get(Calendar.MONTH) + 1) + "-" + now.get(Calendar.DAY_OF_MONTH) + "HT.xls";

        try {
            ExcelUtils.getExcel(response, headers, needShowKeyArr, vo, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public ResponseVO selectOneDTOByBusinessId(String businessId) {
        YwContractVO ywContractVO = this.ywAgencyBusinessService.showOldContract(businessId);
        if (null == ywContractVO) {
            return new ResponseVO(ErrorCodeEnum.CONTRACT_NOT_FOUND, ErrorCodeEnum.CONTRACT_NOT_FOUND.getErrMsg());
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS, ywContractVO);
    }

    @Override
    public int deleteContractFile(String fileId) {
        int i = 0;
        SysFilePath sysFilePath = this.sysFilePathMapper.selectById(fileId);
        if (null != sysFilePath) {
            String path = sysFilePath.getPath();
            String preFix = createFileUtil.isWindows() ? winUrl : linuxUrl;

            WordUtils.deleteFile(preFix + "/" + path);
            i = this.sysFilePathMapper.deleteById(fileId);
        }

        return i;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int chooseWy(String businessId, String wyIds) {
        int record = 0;

        YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessService.selectOne(businessId);

        //查询并清除原有数据库数据
        QueryWrapper<YyContractWyRelationPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("yy_contract_id", ywAgencyBusinessPO.getRelationId());
        List<YyContractWyRelationPO> yyContractWyRelationPOS = this.yyContractWyRelationMapper.selectList(queryWrapper);
        if (!yyContractWyRelationPOS.isEmpty()) {
            this.yyContractWyRelationMapper.delete(queryWrapper);
        }

        String wyNames = "";
        if (!StringUtils.isEmpty(wyIds)) {
            String[] split = wyIds.split(",");
            //新增数据库数据
            for (String wyId : split) {
                YyContractWyRelationPO yyContractWyRelationPO = new YyContractWyRelationPO();
                yyContractWyRelationPO.setYyContractId(ywAgencyBusinessPO.getRelationId());
                yyContractWyRelationPO.setWyBasicInfoId(Long.parseLong(wyId));
                yyContractWyRelationPO.setYwBusinessId(Long.parseLong(businessId));
                record += this.yyContractWyRelationMapper.insert(yyContractWyRelationPO);
            }

            //根据多个物业id获取物业名称
            wyNames = this.wyBasicInfoMapper.selectWyNameByIds(wyIds);
        }

        ywAgencyBusinessPO.setWyName(wyNames);
        this.ywAgencyBusinessService.updateById(ywAgencyBusinessPO);

        return record;
    }

    @Override
    public PageVO<YyContractVO> selectYyContractListByPage(YwAgencyBusinessDTO ywAgencyBusinessDTO) {
        int expectPage = ywAgencyBusinessDTO.getP();
        int queryStartRowNumber = (ywAgencyBusinessDTO.getP() - 1) * ywAgencyBusinessDTO.getSize();

        ywAgencyBusinessDTO.setP(queryStartRowNumber);

        List<YyContractVO> yyContractList = this.yyContractMapper.selectYyContractListByPage(ywAgencyBusinessDTO);

        if (yyContractList.isEmpty()) {
            return null;
        }

        YwAgencyBusinessPO ywAgencyBusinessPO = null;
        if (null != ywAgencyBusinessDTO.getId()) {
            ywAgencyBusinessPO = this.ywAgencyBusinessService.selectOne(ywAgencyBusinessDTO.getId().toString());
        }

        for (YyContractVO yyContractVO : yyContractList) {

            List<WyBaseInfoVO> wyBaseInfoVOList = yyContractVO.getWyBaseInfoVOList();

            BigDecimal totalRentalArea = new BigDecimal(0);
            BigDecimal totalMonthlyRent = new BigDecimal(0);
            StringBuffer wyAddress = new StringBuffer();

            for (WyBaseInfoVO wyBaseInfoVO : wyBaseInfoVOList) {
                BigDecimal rentalArea = new BigDecimal(wyBaseInfoVO.getRentalArea() == null ? "0" : wyBaseInfoVO.getRentalArea());
                BigDecimal monthlyRent = new BigDecimal(wyBaseInfoVO.getMonthlyRent() == null ? "0" : wyBaseInfoVO.getMonthlyRent());

                totalRentalArea.add(rentalArea);
                totalMonthlyRent.add(monthlyRent);
                wyAddress.append(wyBaseInfoVO.getWyAddress());
                wyAddress.append(",");
            }

            yyContractVO.setMonthlyRent(totalMonthlyRent.toString());
            yyContractVO.setRentalArea(totalRentalArea.toString());
            yyContractVO.setLeaseWyAddress(wyAddress.length() > 1 ? wyAddress.toString().substring(0, wyAddress.length() - 1) : "");

            if (null == ywAgencyBusinessPO || null == ywAgencyBusinessPO.getYyContactId() || yyContractVO.getId() != ywAgencyBusinessPO.getYyContactId()) {
                yyContractVO.setMainContract(false);
            } else {
                yyContractVO.setMainContract(true);
            }
        }

        int total = this.yyContractMapper.selectYyContractListByPageCount(ywAgencyBusinessDTO);

        PageVO pageVO = new PageVO();
        pageVO.setList(yyContractList);
        pageVO.setTotal(total);
        pageVO.setP(expectPage);
        pageVO.setSize(ywAgencyBusinessDTO.getSize());

        return pageVO;
    }

    @Override
    public ResponseVO downLoadPaymentNotice(HttpServletResponse response,DownLoadDTO dto) {
        if (null == dto.getInfo().getId()||dto.getInfo().getId()==0) {
            Calendar today = Calendar.getInstance();
            int year = today.get(Calendar.YEAR);
            int dayOfMonth = today.get(Calendar.DAY_OF_MONTH);
            int month = today.get(Calendar.MONTH);

            Map<String, String> testMap = new HashMap<String, String>();
            testMap.put("leaser", dto.getInfo().getLeaser()==null?"":dto.getInfo().getLeaser());
            testMap.put("contactPerson ", dto.getInfo().getContactPerson()==null?"":dto.getInfo().getContactPerson());
            testMap.put("contactPhone", dto.getInfo().getContactPhone()==null?"":dto.getInfo().getContactPhone());
            testMap.put("depositAmount", dto.getInfo().getDepositAmount() == null ? "0" : dto.getInfo().getDepositAmount().toString());
            testMap.put("startRentDate", TimeUtils.getDateString(dto.getInfo().getStartRentDate())==null?"":TimeUtils.getDateString(dto.getInfo().getStartRentDate()));
            testMap.put("endRentDate", TimeUtils.getDateString(dto.getInfo().getEndRentDate())==null?"":TimeUtils.getDateString(dto.getInfo().getEndRentDate()));
            testMap.put("year", String.valueOf(year));
            testMap.put("month", String.valueOf(month + 1));
            testMap.put("day", String.valueOf(dayOfMonth));
            //免租期限 0无，1十五日，2一个月，3两个月，4三个月，5四个月
            dto.getInfo().setFreeRent(dto.getInfo().getFreeRent()==null?0:dto.getInfo().getFreeRent());
            switch (dto.getInfo().getFreeRent()){
                case 0:
                    testMap.put("freeRent","无");
                    break;
                case 1:
                    testMap.put("freeRent","十五日");
                    break;
                case 2:
                    testMap.put("freeRent","一个月");
                    break;
                case 3:
                    testMap.put("freeRent","两个月");
                    break;
                case 4:
                    testMap.put("freeRent","三个月");
                    break;
                case 5:
                    testMap.put("freeRent","四个月");
                    break;
                 default:
                    break;
            }
            testMap.put("firstRentMoney", dto.getInfo().getFirstRentMoney()==null?"":dto.getInfo().getFirstRentMoney().toString());
            testMap.put("houseCode", dto.getHouseCode()==null?"":dto.getInfo().getHouseCode());
            testMap.put("monthlyRent", String.valueOf(dto.getMonthlyRent()==null?"":dto.getMonthlyRent()));
            testMap.put("rentalArea", String.valueOf(dto.getRentalArea()==null?"":dto.getRentalArea()));
            Resource resource = resourceLoader.getResource("classpath:excel/paymentNotice.docx");
            try {
                WordUtils.changWord("租赁缴款通知单.docx", testMap, new ArrayList<>(), response, resource.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Long businessId = yyContractWyRelationMapper.getBusinessId(dto.getInfo().getId());
            ywAgencyBusinessService.downLoadPaymentNotice(response, businessId == null ? "" : String.valueOf(businessId));
        }
        return null;
    }

    @Override
    public ResponseVO downLoadContactTemplate(HttpServletResponse response, DownLoadDTO dto) {
        if (null == dto.getInfo().getId() || dto.getInfo().getId() == 0) {
            Map<String, String> testMap = new HashMap<String, String>();
            testMap.put("houseCode", dto.getHouseCode() == null ? "" : dto.getHouseCode());
            testMap.put("buildArea", dto.getBuildArea() == null ? "" : dto.getBuildArea().toString());
            testMap.put("insideArea", dto.getInsideArea() == null ? "" : dto.getInsideArea().toString());
            testMap.put("shareArea", dto.getShareArea() == null ? "" : dto.getShareArea().toString());
            testMap.put("depositAmount", dto.getInfo().getDepositAmount() == null ? "" : dto.getInfo().getDepositAmount().toString());
            testMap.put("legalCode", dto.getInfo().getLegalCode() == null ? "" : dto.getInfo().getLegalCode());
            testMap.put("phone ", dto.getInfo().getLegalPhone() == null ? "" : dto.getInfo().getLegalPhone());
            testMap.put("leaser", dto.getInfo().getLeaser() == null ? "" : dto.getInfo().getLeaser());
            testMap.put("socialCode", dto.getInfo().getLeaserCode() == null ? "" : dto.getInfo().getLeaserCode());
            testMap.put("leaseAddress", dto.getInfo().getLegalAddress() == null ? "" : dto.getInfo().getLegalAddress());
            testMap.put("leasePhone", dto.getInfo().getLeaserPhone() == null ? "" : dto.getInfo().getLeaserPhone());
            testMap.put("waterRent", dto.getInfo().getWaterFee() == null ? "" : dto.getInfo().getWaterFee().toString());
            testMap.put("electricFee", dto.getInfo().getElectricFee() == null ? "" : dto.getInfo().getElectricFee().toString());
            testMap.put("gasFee", dto.getInfo().getGasFee() == null ? "" : dto.getInfo().getGasFee().toString());
            testMap.put("propertyManagementFee", dto.getInfo().getPropertyManagementFee() == null ? "" : dto.getInfo().getPropertyManagementFee().toString());
            testMap.put("otherFee", dto.getInfo().getOtherFee() == null ? "" : dto.getInfo().getOtherFee().toString());
            testMap.put("certificationNumber", dto.getInfo().getCertificationNumber());

            dto.setAreaCode(dto.getAreaCode() == null ? "" : dto.getAreaCode());
            if (StringUtils.isNotEmpty(dto.getAreaCode())) {
                Map<String, String> nameMap = sysAreaMapper.selectNameByCommitteeCode(dto.getAreaCode());
                if (null != nameMap) {
                    String areaName = nameMap.get("area_name");
                    testMap.put("areaName", areaName.substring(0, areaName.length() - 1));
                } else {
                    testMap.put("areaName", "");
                }
            }

            testMap.put("rentName", dto.getName() == null ? "" : dto.getName());
            if (null == dto.getInfo().getClientPerson() || StringUtils.isEmpty(dto.getInfo().getClientPerson())) {
                testMap.put("applyPersonType", "法定代表人");
                if (dto.getInfo().getLegalType() == 1) {
                    testMap.put("type", "身份证");
                }
                if (dto.getInfo().getLegalType() == 2) {
                    testMap.put("type", "护照");
                }
                testMap.put("code", dto.getInfo().getLegalCode());
                testMap.put("address", dto.getInfo().getLegalAddress());
                testMap.put("phone", dto.getInfo().getLegalPhone());
                testMap.put("person", dto.getInfo().getLegalPerson() == null ? "" : dto.getInfo().getLegalPerson());
            } else {
                testMap.put("applyPersonType", "委托代理人");
                if (dto.getInfo().getClientType() == 1) {
                    testMap.put("type", "身份证");
                }
                if (dto.getInfo().getClientType() == 2) {
                    testMap.put("type", "护照");
                }
                testMap.put("code", dto.getInfo().getClientCode());
                testMap.put("address", dto.getInfo().getClientAddress());
                testMap.put("phone", dto.getInfo().getClientPhone());
                testMap.put("person", dto.getInfo().getClientPerson() == null ? "" : dto.getInfo().getClientPerson());
            }

            dto.getInfo().setLeaseUse(dto.getInfo().getLeaseUse() == null ? 0 : dto.getInfo().getLeaseUse());
            switch (dto.getInfo().getLeaseUse()) {
                case 1:
                    testMap.put("leaseUse", ExportEnum.OFFICE.getName());
                    break;
                case 2:
                    testMap.put("leaseUse", ExportEnum.COMPREHENSIVE_RESEARCH.getName());
                    break;
                case 3:
                    testMap.put("leaseUse", ExportEnum.INDUSTRY_RESEARCH.getName());
                    break;
                case 4:
                    testMap.put("leaseUse", ExportEnum.RESEARCH_AND_DEVELOPMENT.getName());
                    break;
                default:
                    testMap.put("leaseUse", "");
                    break;
            }

            Date startRentDate = dto.getInfo().getStartRentDate();
            Date endRentDate = dto.getInfo().getEndRentDate();
            Calendar startCalender = Calendar.getInstance();
            startCalender.setTime(startRentDate);
            testMap.put("startMonth", String.valueOf(startCalender.get(Calendar.MONTH)));
            testMap.put("startDay", String.valueOf(startCalender.get(Calendar.DAY_OF_MONTH)));
            testMap.put("startYear", String.valueOf(startCalender.get(Calendar.YEAR)));
            if (null != endRentDate) {
                Calendar startCalender2 = Calendar.getInstance();
                startCalender2.setTime(endRentDate);
                testMap.put("endDay", String.valueOf(startCalender2.get(Calendar.DAY_OF_MONTH)));
                testMap.put("endMonth", String.valueOf(startCalender2.get(Calendar.MONTH)));
                testMap.put("endYear", String.valueOf(startCalender2.get(Calendar.YEAR)));
                testMap.put("totalMonth", String.valueOf(startCalender2.get(Calendar.MONTH) - startCalender.get(Calendar.MONTH)));
                testMap.put("totalYear", String.valueOf(startCalender2.get(Calendar.YEAR) - startCalender.get(Calendar.YEAR)));
            }

            Calendar freeStartCalender = Calendar.getInstance();
            freeStartCalender.setTime(startRentDate);
            dto.getInfo().setFreeRent(dto.getInfo().getFreeRent() == null ? 0 : dto.getInfo().getFreeRent());
            switch (dto.getInfo().getFreeRent()) {
                case 0:
                    testMap.put("freeRent", "无");
                    break;
                case 1:
                    testMap.put("freeRent", "十五日");
                    freeStartCalender.add(Calendar.DAY_OF_MONTH, 15);
                    break;
                case 2:
                    testMap.put("freeRent", "一个月");
                    freeStartCalender.add(Calendar.MONTH, 1);
                    break;
                case 3:
                    testMap.put("freeRent", "两个月");
                    freeStartCalender.add(Calendar.MONTH, 2);
                    break;
                case 4:
                    testMap.put("freeRent", "三个月");
                    freeStartCalender.add(Calendar.MONTH, 3);
                    break;
                case 5:
                    testMap.put("freeRent", "四个月");
                    freeStartCalender.add(Calendar.MONTH, 4);
                    break;
                default:
                    break;
            }

            if (!"无".equals(testMap.get("freeRent"))) {
                testMap.put("freeStartDay", String.valueOf(startCalender.get(Calendar.DAY_OF_MONTH)));
                testMap.put("freeStartMonth", String.valueOf(startCalender.get(Calendar.MONTH)));
                testMap.put("freeStartYear", String.valueOf(startCalender.get(Calendar.YEAR)));

                testMap.put("freeEndDay", String.valueOf(freeStartCalender.get(Calendar.DAY_OF_MONTH)));
                testMap.put("freeEndYear", String.valueOf(freeStartCalender.get(Calendar.YEAR)));
                testMap.put("freeEndMonth", String.valueOf(freeStartCalender.get(Calendar.MONTH)));
            }

            testMap.put("guidePrice", dto.getInfo().getGuidePrice() == null ? "" : dto.getInfo().getGuidePrice().toString());
            String discount = "";
            switch (dto.getInfo().getDiscount()) {
                case 1:
                    discount = "100";
                    break;
                case 2:
                    discount = "30";
                    break;
                case 3:
                    discount = "50";
                    break;
                case 4:
                    discount = "70";
                    break;
                case 5:
                    discount = "90";
                    break;
                default:
                    discount = "100";
                    break;
            }
            testMap.put("discount", discount);
            testMap.put("disPrice", null == dto.getInfo().getGuidePrice() ? "" : (dto.getInfo().getGuidePrice().multiply(new BigDecimal(discount)).divide(new BigDecimal(100))).toString());
            testMap.put("monthleyRent", dto.getMonthlyRent() == null ? "" : dto.getMonthlyRent().toString());
            testMap.put("monthleyRentBig", Convert.digitToChinese(dto.getMonthlyRent() == null ? 0 : dto.getMonthlyRent()));
            testMap.put("doubleIndemnity", dto.getMonthlyRent() == null ? "" : dto.getMonthlyRent().multiply(new BigDecimal(2)).toString());
            testMap.put("depositAmountBig", Convert.digitToChinese(dto.getInfo().getDepositAmount() == null ? 0 : dto.getInfo().getDepositAmount()));

            //甲方信息补充
            if(null == dto.getInfo().getType() && null != dto.getInfo().getId()){
                QueryWrapper<YwAgencyBusinessPO> ywAgencyBusinessPOQueryWrapper = new QueryWrapper<>();
                ywAgencyBusinessPOQueryWrapper.eq("relation_id" , dto.getInfo().getId());
                YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessMapper.selectOne(ywAgencyBusinessPOQueryWrapper);
                if(null != ywAgencyBusinessPO && null != ywAgencyBusinessPO.getBusinessType()){
                    DownloadContractPartAInfoUtils.addPartAInfo(testMap, ywAgencyBusinessPO.getBusinessType());
                }
            }else if(null != dto.getInfo().getType()){
                DownloadContractPartAInfoUtils.addPartAInfo(testMap, Byte.parseByte(dto.getInfo().getType() + ""));
            }

            Resource resource = resourceLoader.getResource("classpath:excel/contactTemplate.docx");
            try {
                WordUtils.changWord("合同模板.docx", testMap, new ArrayList<>(), response, resource.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Long businessId = yyContractWyRelationMapper.getBusinessId(dto.getInfo().getId());
            ywAgencyBusinessService.downLoadContactTemplate(response, businessId == null ? "" : String.valueOf(businessId));
        }
        return null;
    }

    @Override
    public ResponseVO exportContractHome(ExportContractDTO dto, HttpServletResponse response) {
        List<YyContractList> list = yyContractMapper.queryContractListTotal(dto.getYyContractIndexDTO());


        for (YyContractList contract : list) {
            //房屋租赁用途
            contract.setLeaseUse(contract.getLeaseUse() == null ? 0 : contract.getLeaseUse());
            switch (contract.getLeaseUse()) {
                case 1:
                    contract.setLeaseUsing("办公");
                    break;
                case 2:
                    contract.setLeaseUsing("综合（研发)");
                    break;
                case 3:
                    contract.setLeaseUsing("工业研发");
                    break;
                case 4:
                    contract.setLeaseUsing("研发");
                    break;
                default:
                    break;
            }

            //是否免租
            contract.setFreeRent(contract.getFreeRent() == null ? -1 : contract.getFreeRent());
            if (contract.getFreeRent() != 0) {
                contract.setIsFree("是");
            } else {
                contract.setIsFree("否");
            }

        }

        Map<String, String> keyMap = dto.getKeyMap();

        //需要显示的字段名
        //字段名对应的中文名
        String[] headers = null;
        String[] needShowKeyArr = null;

        if (StringUtils.isEmpty(dto.getNeedShowKeys())) {
            //不传getNeedShowKeys 显示所有字段
            Set<String> keys = keyMap.keySet();
            needShowKeyArr = keys.toArray(new String[keys.size()]);
            headers = new String[keys.size()];
        } else {
            needShowKeyArr = dto.getNeedShowKeys().split(",");
            //字段名对应的中文名
            headers = new String[needShowKeyArr.length];
        }

        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        try {
            ExcelUtils.getExcel(response, headers, needShowKeyArr, list, "Contract.xls");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 在合同新增中 开启待办业务表
     *
     * @param info
     * @param ywAgencyBusinessId
     */
    private void addBusinessRecordInContract(YyContract info, long ywAgencyBusinessId) {
        //合同新增 新增记录：业务办理——代办业务表 开始
        WyBasicInfo wyBasicInfo = this.wyBasicInfoMapper.selectById(info.getWyBasicInfoId());
        YwAgencyBusinessPO ywAgencyBusinessPO = new YwAgencyBusinessPO();
        ywAgencyBusinessPO.setId(ywAgencyBusinessId);
        ywAgencyBusinessPO.setTableId(String.valueOf(ywAgencyBusinessId));
        ywAgencyBusinessPO.setProName(info.getLeaser() + "租赁合同新签申请");
        ywAgencyBusinessPO.setApplicantUnit(info.getLeaser());
        ywAgencyBusinessPO.setApplicationTime(new Timestamp(System.currentTimeMillis()));
        ywAgencyBusinessPO.setUserId(info.getUserId());
        ywAgencyBusinessPO.setAgencyType(Integer.parseInt(BusinessTypeEnum.CONTRACT_SIGN_APPLY_AGENCY.getCode().toString()));
        ywAgencyBusinessPO.setContactName(info.getContactPerson());
        ywAgencyBusinessPO.setContactPhone(info.getContactPhone());
        ywAgencyBusinessPO.setWebSignType("1");
        //法人申请 1是；2否 --->申请人类型 1法人 2委托代理人
        ywAgencyBusinessPO.setApplyPeopleType(Byte.parseByte(info.getApplicationType().toString()));
        ywAgencyBusinessPO.setApplyObj(info.getLeaseType().toString());
        ywAgencyBusinessPO.setSocialUniformCreditCode(info.getLeaserCode());
        ywAgencyBusinessPO.setBusinessLicenseAddress(info.getLegalAddress());
        ywAgencyBusinessPO.setWyName(wyBasicInfo == null ? "" : wyBasicInfo.getName());
        ywAgencyBusinessPO.setLeaseArea(info.getRentalArea() == null ? "" : info.getRentalArea().toString());
        ywAgencyBusinessPO.setIsCommitment(info.getIsEconomicCommit().toString());
        ywAgencyBusinessPO.setBusinessType(Byte.parseByte(info.getType().toString()));
        ywAgencyBusinessPO.setRemark(info.getCompanyInfoRemark());
        ywAgencyBusinessPO.setRelationId(info.getId());
        //申请来源：1现场申请（管理端合同新增） 2线上申请（门户）
        ywAgencyBusinessPO.setSignSource("1");
        ywAgencyBusinessPO.setCreateTime(new Date());
        this.activitiBaseService.addAgencyBusinessRecord(ywAgencyBusinessPO);
        //合同新增 新增记录：业务办理——代办业务表 结束
    }

    /**
     * 方法抽取
     *
     * @param paramList
     * @param dbFileId
     * @param sysFilePathList
     */
    private void commonAddSysFilePath(List<SysFilePath> paramList, Long dbFileId, List<SysFilePath> sysFilePathList) {
        for (SysFilePath businessLicenseFilePath : sysFilePathList) {
            SysFilePath filePath = new SysFilePath();
            filePath.setId(IdWorker.getId());
            filePath.setDbFileId(dbFileId);
            filePath.setPath(businessLicenseFilePath.getPath());
            filePath.setFileName(businessLicenseFilePath.getFileName());
            filePath.setFileSize(businessLicenseFilePath.getFileSize());
            paramList.add(filePath);
        }
    }

    /**
     * 附件处理
     *  @param dto
     * @param info
     * @param ywAgencyBusinessId
     */
    private void disposeFile(YyContractDTO dto, YyContract info, long ywAgencyBusinessId) {

        Map<Long, String> needAddRecordIdMap = new HashMap<>();
        List<SysFilePath> insertSysFilePathList = new ArrayList<>();
        List<SysAttachment> insertSysAttachmentList = new ArrayList<>();
        List<YwHandoverDetailsPO> insertHandoverDetailsList = new ArrayList<>();

        //入驻通知书
        long residentNoticeFilePathId = IdWorker.getId();
        info.setResidentNoticeFilePathId(residentNoticeFilePathId);
        if (dto.getResidentNoticeFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, residentNoticeFilePathId, dto.getResidentNoticeFile());
        }

        /**
         * 营业执照复印件 sys_file_path字段db_file_id
         */
        Long businessLicenseFilePathId = IdWorker.getId();
        info.setBusinessLicenseFilePathId(businessLicenseFilePathId);
        if (dto.getBusinessLicenseFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, businessLicenseFilePathId, dto.getBusinessLicenseFile());
        }

        /**
         * 法人身份证复印件 sys_file_path字段db_file_id
         */
        Long legalPersonFilePathId = IdWorker.getId();
        info.setLegalPersonFilePathId(legalPersonFilePathId);
        if (dto.getLegalPersonFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, legalPersonFilePathId, dto.getLegalPersonFile());
        }

        /**
         * 法人代表证明书 sys_file_path字段db_file_id
         */
        Long legalRepresentativeFilePathId = IdWorker.getId();
        info.setLegalRepresentativeFilePathId(legalRepresentativeFilePathId);
        if (dto.getLegalRepresentativeFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, legalRepresentativeFilePathId, dto.getLegalRepresentativeFile());
        }

        /**
         *  股东证明书 sys_file_path字段db_file_id
         */
        long shareHolderFilePathId = IdWorker.getId();
        info.setShareholderFilePathId(shareHolderFilePathId);
        if (dto.getShareholderFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, shareHolderFilePathId, dto.getShareholderFile());
        }

        /**
         * 法人授权委托书 sys_file_path字段db_file_id
         */
        Long legalPersonPowerFilePathId = IdWorker.getId();
        info.setLegalPersonPowerFilePathId(legalPersonPowerFilePathId);
        if (dto.getLegalPersonPowerFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, legalPersonPowerFilePathId, dto.getLegalPersonPowerFile());
        }

        /**
         * 经办人身份证复印件 sys_file_path字段db_file_id
         */
        Long handlingPersonCardFilePathId = IdWorker.getId();
        info.setHandlingPersonCardFilePathId(handlingPersonCardFilePathId);
        if (dto.getHandlingPersonCardFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, handlingPersonCardFilePathId, dto.getHandlingPersonCardFile());
        }

        /**
         * 南山区产业用房建设和管理工作领导小组会议纪要 sys_file_path字段db_file_id
         */
        Long meetingMinutesFilePathId = IdWorker.getId();
        info.setMeetingMinutesFilePathId(meetingMinutesFilePathId);
        if (dto.getMeetingMinutesFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, meetingMinutesFilePathId, dto.getMeetingMinutesFile());
        }

        /**
         *房屋建筑面积总表，房屋建筑面积分户汇总表及房屋建筑面积分户位置图 sys_file_path字段db_file_id
         */
        Long buildingAreaFilePathId = IdWorker.getId();
        info.setBuildingAreaFilePathId(buildingAreaFilePathId);
        if (dto.getBuildingAreaFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, buildingAreaFilePathId, dto.getBuildingAreaFile());
        }

        /**
         * 有经济贡献率的企业的经济贡献承诺书 sys_file_path字段db_file_id
         */
        Long commitmentFilePathId = IdWorker.getId();
        info.setCommitmentFilePathId(commitmentFilePathId);
        if (dto.getCommitmentFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, commitmentFilePathId, dto.getCommitmentFile());
        }

        /**
         * 南山区政策性产业用房入驻通知书 sys_file_path字段db_file_id
         */
        Long settlementNoticeFilePathId = IdWorker.getId();
        info.setSettlementNoticeFilePathId(settlementNoticeFilePathId);
        if (dto.getSettlementNoticeFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, settlementNoticeFilePathId, dto.getSettlementNoticeFile());
        }


        /**
         * 会议纪要 sys_file_path字段db_file_id
         */
        Long meetingFilePathId = IdWorker.getId();
        info.setMeetingFilePathId(meetingFilePathId);
        if (dto.getMeetingFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, meetingFilePathId, dto.getMeetingFile());
        }
        /**
         * 租金价格评估报告 sys_file_path字段db_file_id
         */
        Long rentReportFilePathId = IdWorker.getId();
        info.setRentReportFilePathId(rentReportFilePathId);
        if (dto.getRentReportFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, rentReportFilePathId, dto.getRentReportFile());
        }
        /**
         * 身份证复印件 sys_file_path字段db_file_id
         */
        Long personFilePathId = IdWorker.getId();
        info.setPersonFilePathId(personFilePathId);
        if (dto.getPersonFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, personFilePathId, dto.getPersonFile());
        }
        /**
         * 南山数字文化产业基地入驻通知书 sys_file_path字段db_file_id
         */
        Long digitalNoticeFilePathId = IdWorker.getId();
        info.setDigitalNoticeFilePathId(digitalNoticeFilePathId);
        if (dto.getDigitalNoticeFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, digitalNoticeFilePathId, dto.getDigitalNoticeFile());
        }
        /**
         * 合同 sys_file_path字段db_file_id
         */
        Long contractFilePathId = IdWorker.getId();
        info.setContractFilePathId(contractFilePathId);
        if (dto.getContractFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, contractFilePathId, dto.getContractFile());
        }

        /**
         * 续签通知书 sys_file_path字段db_file_id
         */
        Long renewalSignFilePathId = IdWorker.getId();
        info.setRenewalSignFilePathId(renewalSignFilePathId);
        if (dto.getRenewalSignFile() != null) {
            commonAddSysFilePath(insertSysFilePathList, renewalSignFilePathId, dto.getRenewalSignFile());
        }

        /**
         * 自定义附件
         */
        Long custRightFileId = IdWorker.getId();
        info.setCustRightFileId(custRightFileId);
        if (dto.getCustRightFile() != null) {
            for(Map.Entry<String,List<SysFilePath>> entry : dto.getCustRightFile().entrySet()){
                String customName = entry.getKey();
                List<SysFilePath> sysFilePathList = entry.getValue();
                for (SysFilePath sysfile : sysFilePathList) {
                    SysFilePath sysFilePath = new SysFilePath();
                    Long FileId = IdWorker.getId();
                    sysFilePath.setId(FileId);
                    sysFilePath.setFileName(sysfile.getFileName());
                    sysFilePath.setFileSize(sysfile.getFileSize());
                    sysFilePath.setPath(sysfile.getPath());
                    sysFilePath.setDbFileId(custRightFileId);
                    insertSysFilePathList.add(sysFilePath);

                    SysAttachment attachment = new SysAttachment();
                    attachment.setId(IdWorker.getId());
                    attachment.setSysFilePathId(FileId);
                    attachment.setName(customName);
                    insertSysAttachmentList.add(attachment);
                }
                //添加附件信息
                needAddRecordIdMap.put(custRightFileId, customName);
            }
        }

        /**
         * 批量插入 更新
         */
        this.sysFilePathMapper.insertBatch(insertSysFilePathList);
        if(!insertSysAttachmentList.isEmpty()){
            this.sysAttachmentMapper.insertBatch(insertSysAttachmentList);
        }

        //没有业务id 后续跳过
        if (ywAgencyBusinessId < 1) {
            return;
        }

        needAddRecordIdMap.put(residentNoticeFilePathId, ContractFileNameEnum.File_1.getFileName());
        needAddRecordIdMap.put(businessLicenseFilePathId, ContractFileNameEnum.File_2.getFileName());
        needAddRecordIdMap.put(legalPersonFilePathId, ContractFileNameEnum.File_3.getFileName());
        needAddRecordIdMap.put(legalRepresentativeFilePathId, ContractFileNameEnum.File_4.getFileName());
        needAddRecordIdMap.put(shareHolderFilePathId, ContractFileNameEnum.File_5.getFileName());
        needAddRecordIdMap.put(legalPersonPowerFilePathId, ContractFileNameEnum.File_6.getFileName());
        needAddRecordIdMap.put(handlingPersonCardFilePathId, ContractFileNameEnum.File_7.getFileName());
        needAddRecordIdMap.put(meetingMinutesFilePathId, ContractFileNameEnum.File_8.getFileName());
        needAddRecordIdMap.put(buildingAreaFilePathId, ContractFileNameEnum.File_9.getFileName());
        needAddRecordIdMap.put(commitmentFilePathId, ContractFileNameEnum.File_10.getFileName());
        needAddRecordIdMap.put(settlementNoticeFilePathId, ContractFileNameEnum.File_11.getFileName());
        needAddRecordIdMap.put(meetingFilePathId, ContractFileNameEnum.File_12.getFileName());
        needAddRecordIdMap.put(rentReportFilePathId, ContractFileNameEnum.File_13.getFileName());
        needAddRecordIdMap.put(personFilePathId, ContractFileNameEnum.File_14.getFileName());
        needAddRecordIdMap.put(digitalNoticeFilePathId, ContractFileNameEnum.File_15.getFileName());
        needAddRecordIdMap.put(contractFilePathId, ContractFileNameEnum.File_16.getFileName());

        if (info.getContractType().equals(Integer.parseInt(BusinessTypeEnum.RENEW_SIGN.getCode().toString()))) {
            needAddRecordIdMap.put(renewalSignFilePathId, ContractFileNameEnum.File_17.getFileName());
        }

        for (Map.Entry<Long, String> entry : needAddRecordIdMap.entrySet()) {
            YwHandoverDetailsPO ywHandoverDetailsPO = new YwHandoverDetailsPO();
            ywHandoverDetailsPO.setName(entry.getValue());
            ywHandoverDetailsPO.setYwAgencyBusinessId(ywAgencyBusinessId);
            ywHandoverDetailsPO.setYyContractId(info.getId());
            ywHandoverDetailsPO.setHandoverDetailsImgId(entry.getKey());
            insertHandoverDetailsList.add(ywHandoverDetailsPO);
        }

        this.ywHandoverDetailsMapper.insertBatch(insertHandoverDetailsList);

    }
}