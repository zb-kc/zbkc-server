package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.mapper.SysDataTypeMapper;
import com.zbkc.model.po.SysDataType;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.SysDataTypeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 数据字典类型表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-03
 */
@Service
public class SysDataTypeServiceImpl  implements SysDataTypeService {
    @Autowired
    private SysDataTypeMapper sysDataTypeMapper;

    @Override
    public ResponseVO selectList(Wrapper<SysDataType> queryWrapper) {
        List<SysDataType> sysDataValues = sysDataTypeMapper.selectList(null);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,sysDataValues);
    }

    @Override
    public ResponseVO status(Long id,Boolean status) {
        if(status) {
            Integer open = sysDataTypeMapper.statusOpen(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS,open);
        }else {
            Integer close = sysDataTypeMapper.statusClose(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS,close);
        }
    }

    @Override
    public ResponseVO pageByName(Integer p, Integer size, String name) {
        if(p<=0){
            return new ResponseVO(ErrorCodeEnum.SUCCESS,null);
        }
        List<SysDataType> list=sysDataTypeMapper.pageByName( (p-1)*size,size, StringUtils.isEmpty(name) ? null : name);
        PageVO<SysDataType> vo = new PageVO<>();
        vo.setList( list );
        vo.setTotal(  sysDataTypeMapper.selectByNameTotal(name ).size());
        vo.setP(p);
        vo.setSize(size);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO selectAllByCode(String code) {
        List<SysDataType> list = sysDataTypeMapper.selectAllByCode(code);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,list);
    }

    @Override
    public ResponseVO insert(SysDataType entity) {
        if (entity.getName() == null || "".equals(entity.getName())) {
            return new ResponseVO(ErrorCodeEnum.REGULARNOTACCORD, null);
        } else {
            entity.setStatus(1);
            long ct = System.currentTimeMillis();
            Timestamp timestamp = TimeUtils.LongToDate(ct);
            entity.setCreateTime(timestamp);
            int i = sysDataTypeMapper.insert(entity);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, i);
        }
    }

    @Override
    public ResponseVO updateById(SysDataType entity) {
        long ct = System.currentTimeMillis();
        Timestamp timestamp = TimeUtils.LongToDate(ct);
        entity.setUpdateTime(timestamp);
        int i = sysDataTypeMapper.updateById(entity);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO deleteById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO deleteByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO delete(Wrapper<SysDataType> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO deleteBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO update(SysDataType entity, Wrapper<SysDataType> updateWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO selectBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO selectOne(Wrapper<SysDataType> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectCount(Wrapper<SysDataType> queryWrapper) {
        return null;
    }


    @Override
    public ResponseVO selectMaps(Wrapper<SysDataType> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectObjs(Wrapper<SysDataType> queryWrapper) {
        return null;
    }


    @Override
    public <P extends IPage<Map<String, Object>>> ResponseVO selectMapsPage(P page, Wrapper<SysDataType> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<SysDataType>> ResponseVO selectPage(P page, Wrapper<SysDataType> queryWrapper) {
        return null;
    }

}
