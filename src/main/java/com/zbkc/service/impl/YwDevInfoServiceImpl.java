package com.zbkc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.mapper.YwDevInfoMapper;
import com.zbkc.model.dto.YwDevInfoDTO;
import com.zbkc.model.po.YwDevInfoPO;
import com.zbkc.service.YwDevInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/13
 */
@Service
public class YwDevInfoServiceImpl extends ServiceImpl<YwDevInfoMapper, YwDevInfoPO> implements YwDevInfoService {

    @Autowired
    private YwDevInfoMapper ywDevInfoMapper;

    @Override
    public int addYwDevInfo(YwDevInfoPO ywDevInfoPO) {
        return this.ywDevInfoMapper.insert(ywDevInfoPO);
    }

    @Override
    public YwDevInfoDTO selectOneByBusinessId(long id) {

        YwDevInfoPO ywDevInfoPO = this.ywDevInfoMapper.selectOneByBusinessId(id);
        if(null == ywDevInfoPO){
            return null;
        }

        YwDevInfoDTO ywDevInfoDTO = new YwDevInfoDTO();
        BeanUtils.copyProperties(ywDevInfoPO , ywDevInfoDTO);

        return ywDevInfoDTO;
    }
}
