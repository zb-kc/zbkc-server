package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.google.common.base.Supplier;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.SetOptUtils;
import com.zbkc.common.utils.StructureUtils;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.common.utils.ToolUtils;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.*;
import com.zbkc.model.dto.*;
import com.zbkc.model.dto.homeList.WyBasicInfoHomeList;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.*;
import com.zbkc.service.WyBasicInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 物业基本(主体)信息表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-09
 */
@Service
@Slf4j
public class WyBasicInfoServiceImpl implements WyBasicInfoService {
    @Autowired
    private WyBasicInfoMapper wyBasicInfoMapper;
    @Resource
    private ToolUtils toolUtils;
    @Autowired
    private LabelManageMapper labelManageMapper;
    @Autowired
    private SysFilePathMapper sysFilePathMapper;
    @Autowired
    private WyFacilitiesInfoMapper wyFacilitiesInfoMapper;
    @Autowired
    private SysImgMapper sysImgMapper;
    @Autowired
    private YyContractMapper yyContractMapper;
    @Autowired
    private SysAttachmentMapper sysAttachmentMapper;
    @Autowired
    private SysRoleDataPermissionMapper sysRoleDataPermissionMapper;
    @Autowired
    private SysDataTypeMapper sysDataTypeMapper;
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    @Autowired
    private SysUserOrgMapper sysUserOrgMapper;
    @Autowired
    private YyUseRegMapper yyUseRegMapper;


    private final static ExecutorService executorService = new ThreadPoolExecutor(4, 30, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO addBasic(WyBasicInfoVo vo1) throws Exception {

        List<LabelManage> insertLabelManageList = new ArrayList<>();
        List<SysFilePath> insertSysFilePathList = new ArrayList<>();
        List<SysAttachment> insertSysAttachmentList = new ArrayList<>();
        List<SysRoleDataPermission> insertSysRoleDataPermissionList = new ArrayList<>();

        WyBasicInfoVo vo = new WyBasicInfoVo();
        vo = vo1;

        if (vo.getUserId() == 0||vo.getUserId()==null) {
            throw new BusinessException(ErrorCodeEnum.USER_NOT_FOUND);
        }

        if (vo.getType() == 1 || vo.getType() == 6 || vo.getType() == 7) {
            vo.setPid((long) 0);
        }

//        if (wyBasicInfoMapper.selectTotalByNameAndPid(vo.getName(), vo.getPid()) > 0) {
//            return new ResponseVO(ErrorCodeEnum.ALREAD_EXIT, ErrorCodeEnum.ALREAD_EXIT.getErrMsg());
//        }

        List<Future<?>> list = new ArrayList<>();
        long startTime = System.currentTimeMillis();

        WyBasicInfo info = new WyBasicInfo();
        Long wyBasicId = IdWorker.getId();
        info.setId(wyBasicId);
        info.setPid(vo.getPid() == null ? 0 : vo.getPid());
        info.setAssetType(vo.getAssetType());
        info.setPropertyType(vo.getPropertyType());
        info.setType(vo.getType());
        info.setBelongPerson(vo.getBelongPerson());


        Long propertyTagId = IdWorker.getId();
        info.setPropertyTagId(propertyTagId);
        if (vo.getPropertyTagId() != null) {
            LabelManage manage1 = new LabelManage();
            manage1.setLabelId(propertyTagId);
            manage1.setSysDataTypeId(vo.getPropertyTagId());
            insertLabelManageList.add(manage1);

        }

        info.setName(vo.getName());
        if (vo.getType() == 1 || vo.getType() == 6 || vo.getType() == 7) {
            info.setUnitName(vo.getName());
        }else {
            info.setUnitName(vo.getUnitName());
        }

        info.setShortName(vo.getShortName());
        info.setAliasName(vo.getAliasName());

        Long propertyScaleId = IdWorker.getId();
        info.setPropertyScaleId(propertyScaleId);
        if (vo.getPropertyScaleId() != null) {
            LabelManage manage2 = new LabelManage();
            manage2.setLabelId(propertyScaleId);
            manage2.setSysDataTypeId(vo.getPropertyScaleId());
            insertLabelManageList.add(manage2);

        }

        info.setPropertyCode(vo.getPropertyCode());
        info.setHouseCode(vo.getHouseCode());

        Long propertyStatusId = IdWorker.getId();
        info.setPropertyStatusId(propertyStatusId);
        if (vo.getPropertyStatusId() != null) {
            LabelManage manage3 = new LabelManage();
            manage3.setLabelId(propertyStatusId);
            manage3.setSysDataTypeId(vo.getPropertyStatusId());
            insertLabelManageList.add(manage3);

        }else {
            LabelManage manage10 = new LabelManage();
            manage10.setLabelId(propertyStatusId);
            manage10.setSysDataTypeId(1461637977200508929L);
            insertLabelManageList.add(manage10);

        }

        Long propertyTypeId = IdWorker.getId();
        info.setPropertyTypeId(propertyTypeId);
        if (vo.getPropertyTypeId() != null) {
            for (int i = 0; i < vo.getPropertyTypeId().length; i++) {

                LabelManage manage4 = new LabelManage();
                manage4.setLabelId(propertyTypeId);
                manage4.setSysDataTypeId(vo.getPropertyTypeId()[i]);
                insertLabelManageList.add(manage4);
            }
        }

        Long mainPurposId = IdWorker.getId();
        info.setMainPurposId(mainPurposId);
        if (vo.getMainPurposId() != null) {
            LabelManage manage5 = new LabelManage();
            manage5.setLabelId(mainPurposId);
            manage5.setSysDataTypeId(vo.getMainPurposId());
            insertLabelManageList.add(manage5);
        }


        info.setAreaCode(vo.getAreaCode());
        info.setAddress(vo.getAddress());
        info.setDetailAddr(vo.getDetailAddr());
        info.setBuildArea(vo.getBuildArea());
        info.setLandArea(vo.getLandArea());
        info.setPracticalArea(vo.getPracticalArea());
        info.setShareArea(vo.getShareArea());

        Long propertySourceId = IdWorker.getId();
        info.setPropertySourceId(propertySourceId);
        if (vo.getPropertySourceId() != null) {
            for (int i = 0; i < vo.getPropertySourceId().length; i++) {
                LabelManage manage6 = new LabelManage();
                manage6.setLabelId(propertySourceId);
                manage6.setSysDataTypeId(vo.getPropertySourceId()[i]);
                insertLabelManageList.add(manage6);
            }
        }

        info.setPropertyPlan(vo.getPropertyPlan());

        Long wyTagId = IdWorker.getId();
        info.setWyTagId(wyTagId);
        if (vo.getWyTagId() != null) {
            for (int i = 0; i < vo.getWyTagId().length; i++) {
                LabelManage manage7 = new LabelManage();
                manage7.setLabelId(wyTagId);
                manage7.setSysDataTypeId(vo.getWyTagId()[i]);
                insertLabelManageList.add(manage7);
            }
        }

        Long propertyImgId = IdWorker.getId();
        info.setPropertyImgId(propertyImgId);
        if (vo.getPropertyImg() != null) {
            for (SysImg propertyImgPath : vo.getPropertyImg()) {
                SysImg propertyImg = new SysImg();
                propertyImg.setId(IdWorker.getId());
                propertyImg.setName(propertyImgPath.getName());
                propertyImg.setPrimaryTableId(propertyImgId);
                propertyImg.setPath(propertyImgPath.getPath());

                    sysImgMapper.addSysImg(propertyImg);

            }
        }


        info.setDesignUnit(vo.getDesignUnit());
        info.setBuildUnit(vo.getBuildUnit());
        info.setBuildDescription(vo.getBuildDescription());
        info.setBuildRemark(vo.getBuildRemark());


        Long acceptanceReportFileId = IdWorker.getId();
        info.setAcceptanceReportFileId(acceptanceReportFileId);
        if (vo.getAcceptanceReportFile() != null) {
            for (SysFilePath acceptanceReportFile : vo.getAcceptanceReportFile()) {
                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(acceptanceReportFileId);
                sysFilePath.setFileName(acceptanceReportFile.getFileName());
                sysFilePath.setPath(acceptanceReportFile.getPath());
                sysFilePath.setFileSize(acceptanceReportFile.getFileSize());

            insertSysFilePathList.add(sysFilePath);
            }
        }


        Long constructPermitFileId = IdWorker.getId();
        info.setConstructPermitFileId(constructPermitFileId);
        if (vo.getConstructPermitFile() != null) {
            for (SysFilePath constructPermitFile : vo.getConstructPermitFile()) {
                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(constructPermitFileId);
                sysFilePath.setFileName(constructPermitFile.getFileName());
                sysFilePath.setPath(constructPermitFile.getPath());
                sysFilePath.setFileSize(constructPermitFile.getFileSize());

                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long buildCustFileId = IdWorker.getId();
        info.setBuildCustFileId(buildCustFileId);
        if (vo.getBuildCustFile() != null) {
            Set<String> keySet = vo.getBuildCustFile().keySet();
            List<String> strings = keySet.stream().collect(Collectors.toList());
            for (String string : strings) {
                List<SysFilePath> vos = vo.getBuildCustFile().get(string);
                for (SysFilePath sysfile : vos) {
                    SysFilePath sysFilePath = new SysFilePath();
                    Long FileId = IdWorker.getId();
                    sysFilePath.setId(FileId);
                    sysFilePath.setFileName(sysfile.getFileName());
                    sysFilePath.setFileSize(sysfile.getFileSize());
                    sysFilePath.setPath(sysfile.getPath());
                    sysFilePath.setDbFileId(buildCustFileId);


                    insertSysFilePathList.add(sysFilePath);

                    SysAttachment attachment = new SysAttachment();
                    attachment.setId(IdWorker.getId());
                    attachment.setSysFilePathId(FileId);
                    attachment.setName(string);

                  insertSysAttachmentList.add(attachment);

                }
            }

        }

        info.setLandStartTime(StringUtils.isBlank(vo.getLandStartTime()) ? null : toolUtils.strFormatDate(vo.getLandStartTime()));
        info.setLandEndTime(StringUtils.isBlank(vo.getLandEndTime()) ? null : toolUtils.strFormatDate(vo.getLandEndTime()));
        info.setConstructionUnit(vo.getConstructionUnit());
        info.setLandDescription(vo.getLandDescription());
        info.setLandRemark(vo.getLandRemark());

        Long landTransferContractFileId = IdWorker.getId();
        info.setLandTransferContractFileId(landTransferContractFileId);
        if (vo.getLandTransferContractFile() != null) {
            for (SysFilePath landTransferContractFilePath : vo.getLandTransferContractFile()) {
                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(landTransferContractFileId);//
                sysFilePath.setFileName(landTransferContractFilePath.getFileName());
                sysFilePath.setPath(landTransferContractFilePath.getPath());
                sysFilePath.setFileSize(landTransferContractFilePath.getFileSize());

                insertSysFilePathList.add(sysFilePath);
            }
        }

        info.setNum(vo.getNum());
        info.setStoreyHeight(vo.getStoreyHeight());
        info.setLoadBearer(vo.getLoadBearer());
        info.setLongitude(vo.getLongitude());
        info.setLatitude(vo.getLatitude());
        info.setPropertyIntroduction(vo.getPropertyIntroduction());

        Long redLineFileId = IdWorker.getId();
        info.setRedLineFileId(redLineFileId);
        if (vo.getRedLineFile() != null) {
            for (SysFilePath redLineImgPath : vo.getRedLineFile()) {
                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(redLineFileId);
                sysFilePath.setFileName(redLineImgPath.getFileName());
                sysFilePath.setPath(redLineImgPath.getPath());
                sysFilePath.setFileSize(redLineImgPath.getFileSize());

                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long landPermitFileId = IdWorker.getId();
        info.setLandPermitFileId(landPermitFileId);
        if (vo.getLandPermitFile() != null) {
            for (SysFilePath landPermitFile : vo.getLandPermitFile()) {
                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(landPermitFileId);
                sysFilePath.setFileName(landPermitFile.getFileName());
                sysFilePath.setPath(landPermitFile.getPath());
                sysFilePath.setFileSize(landPermitFile.getFileSize());

                insertSysFilePathList.add(sysFilePath);
            }
        }


        Long siteCustFileId = IdWorker.getId();
        info.setSiteCustFileId(siteCustFileId);
        if (vo.getSiteCustFile() != null) {
            Set<String> keySet = vo.getSiteCustFile().keySet();
            List<String> strings = keySet.stream().collect(Collectors.toList());
            for (String string : strings) {
                List<SysFilePath> vos = vo.getSiteCustFile().get(string);
                for (SysFilePath sysfile : vos) {
                    SysFilePath sysFilePath = new SysFilePath();
                    Long FileId = IdWorker.getId();
                    sysFilePath.setId(FileId);
                    sysFilePath.setFileName(sysfile.getFileName());
                    sysFilePath.setFileSize(sysfile.getFileSize());
                    sysFilePath.setPath(sysfile.getPath());
                    sysFilePath.setDbFileId(siteCustFileId);


                    insertSysFilePathList.add(sysFilePath);

                    SysAttachment attachment = new SysAttachment();
                    attachment.setId(IdWorker.getId());
                    attachment.setSysFilePathId(FileId);
                    attachment.setName(string);
                  insertSysAttachmentList.add(attachment);
                }
            }
        }


        info.setCompanyName(vo.getCompanyName());
        info.setContacts(vo.getContacts());
        info.setPhone(vo.getPhone());
        info.setManageFee(new BigDecimal(vo.getManageFee()));
        info.setRemark(vo.getRemark());


        info.setCreateTime(new Timestamp(System.currentTimeMillis()));
        info.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        info.setTowards(vo.getTowards());
        info.setThreeDimen(vo.getThreeDimen());

        Long houseImgId = IdWorker.getId();
        info.setHouseImgId(houseImgId);
        if (vo.getHouseImg() != null) {
            for (SysImg houseImg : vo.getHouseImg()) {

                SysImg sysImg = new SysImg();
                sysImg.setId(IdWorker.getId());
                sysImg.setPath(houseImg.getPath());
                sysImg.setName(houseImg.getName());
                sysImg.setPrimaryTableId(houseImgId);

                    sysImgMapper.addSysImg(sysImg);

            }
        }

        info.setUserId(vo.getUserId());

//        if (!sysUserOrgMapper.findSelfOrgIds(vo.getUserId()).contains(vo.getOpreatesGroupId())){
//            throw new BusinessException(ErrorCodeEnum.UNAUTHORIZED);
//        }

        info.setOperatesType(vo.getOperatesType());
        info.setOperatesUnitId(vo.getOperatesUnitId());
        info.setOpreatesGroupId(vo.getOpreatesGroupId());
        info.setOtherUnitId(vo.getOtherUnitId());


        if (vo.getWyFacilitiesInfoList() != null&&(vo.getType()==1||vo.getType()==6||vo.getType()==7)) {
            for (WyFacilitiesInfo wyFacilitiesInfo : vo.getWyFacilitiesInfoList()) {
                wyFacilitiesInfo.setWyBasicInfoId(wyBasicId);
                wyFacilitiesInfoMapper.addWyFacilitiesInfo(wyFacilitiesInfo);
            }
        }

        wyBasicInfoMapper.insert(info);


        if ((vo.getType() == 1 || vo.getType() == 6 || vo.getType() == 7) && null != vo.getOperatesType() && vo.getOperatesType() != 1) {

            List<Long> roles = sysUserOrgMapper.findRolesByOrg(vo.getOpreatesGroupId());
            for (Long id : roles) {
                SysRoleDataPermission permissions = new SysRoleDataPermission();
                permissions.setId(IdWorker.getId());
                permissions.setWyBasicInfoId(wyBasicId);
                permissions.setRoleId(id);
              insertSysRoleDataPermissionList.add(permissions);

            }
        } else {



            Long role = sysUserRoleMapper.selectRoleByUserId(vo.getUserId());
            SysRoleDataPermission permission = new SysRoleDataPermission();
            permission.setId(IdWorker.getId());
            permission.setWyBasicInfoId(wyBasicId);
            permission.setRoleId(role);
          insertSysRoleDataPermissionList.add(permission);
        }
        /**
         * 批量插入、更新 标签、分组
         */
        if(!insertSysAttachmentList.isEmpty()){
            this.sysAttachmentMapper.insertBatch(insertSysAttachmentList);
        }
        if(!insertSysFilePathList.isEmpty()){
            this.sysFilePathMapper.insertBatch(insertSysFilePathList);
        }
        if(!insertSysRoleDataPermissionList.isEmpty()){
            this.sysRoleDataPermissionMapper.insertBatch(insertSysRoleDataPermissionList);
        }
        if(!insertLabelManageList.isEmpty()) {
            this.labelManageMapper.insertBatch(insertLabelManageList);
        }
//        Boolean flag=true;
        for (Future<?> future : list) {
            try {
                future.get();
            } catch (Exception e) {
                log.error("添加资产登记异常:{}", e.toString());
                throw new RuntimeException(ErrorCodeEnum.ERROR_BACK.getErrMsg());
//                flag=false;
            }
        }

        long endTime = System.currentTimeMillis();
        log.info("程序运行时间： " + (endTime - startTime) + "ms");

        return new ResponseVO(ErrorCodeEnum.SUCCESS, info);

    }

    @Override
    public ResponseVO getList(WyBasicInfoHomeDTO dto) {

        long startTime = System.currentTimeMillis();
        if (dto.getP() <= 0) {
            return new ResponseVO(ErrorCodeEnum.SUCCESS, null);
        }
        dto.setP((dto.getP() - 1) * dto.getSize());

        List<WyBasicInfoHomeList> list = wyBasicInfoMapper.queryWyBasicInfoHomeList(dto);

        if (null != list && !list.isEmpty()) {
            List<Long> wyIdList = list.stream().map(WyBasicInfoHomeList::getId).collect(Collectors.toList());
            List<WyBasicInfo> sysDataTagList = this.wyBasicInfoMapper.getSysDataTagByIdList(wyIdList, "property_tag_id");
            List<WyBasicInfo> propertyScaleList = this.wyBasicInfoMapper.getSysDataTagByIdList(wyIdList, "property_scale_id");
            List<WyBasicInfo> propertyStatusList = this.wyBasicInfoMapper.getSysDataTagByIdList(wyIdList, "property_status_id");
            List<WyBasicInfo> propertyTypeList = this.wyBasicInfoMapper.getSysDataTagByIdList(wyIdList, "property_type_id");

            if (null == sysDataTagList) {
                sysDataTagList = new ArrayList<>();
            }
            if (null == propertyScaleList) {
                propertyScaleList = new ArrayList<>();
            }
            if (null == propertyStatusList) {
                propertyStatusList = new ArrayList<>();
            }
            if (null == propertyTypeList) {
                propertyTypeList = new ArrayList<>();
            }

            Map<Long, String> tagMap = sysDataTagList.stream().collect(Collectors.toMap(WyBasicInfo::getId, e -> e.getAssetType() == null ? "" : e.getAssetType()));
            Map<Long, String> scaleMap = propertyScaleList.stream().collect(Collectors.toMap(WyBasicInfo::getId, e -> e.getAssetType() == null ? "" : e.getAssetType()));
            Map<Long, String> statusMap = propertyStatusList.stream().collect(Collectors.toMap(WyBasicInfo::getId, e -> e.getAssetType() == null ? "" : e.getAssetType()));
            Map<Long, String> typeMap = propertyTypeList.stream().collect(Collectors.toMap(WyBasicInfo::getId, e -> e.getAssetType() == null ? "" : e.getAssetType()));

            for (WyBasicInfoHomeList wyBasicInfoHome : list) {
                Long id = wyBasicInfoHome.getId();
                wyBasicInfoHome.setPropertyTagName(tagMap.get(id));
                wyBasicInfoHome.setPropertyScaleName(scaleMap.get(id));
                wyBasicInfoHome.setPropertyStatusName(statusMap.get(id));
                wyBasicInfoHome.setPropertyTypeName(typeMap.get(id));

                //计算空置面积
                double emptyArea = this.countEmptyAreaByPropertyStructure(id, dto.getUserId());
                wyBasicInfoHome.setEmptyArea(emptyArea);

            }
        }

        PageVO<WyBasicInfoHomeList> listVo = new PageVO<>();
        listVo.setList(list);
        listVo.setP(dto.getP() / dto.getSize() + 1);
        listVo.setSize(dto.getSize());
        listVo.setTotal(wyBasicInfoMapper.queryWyBasicInfoHomeListTotal(dto).size());


        long endTime = System.currentTimeMillis();
        log.info((startTime - endTime) + "毫秒");

        return new ResponseVO(ErrorCodeEnum.SUCCESS, listVo);


    }

    @Override
    public ResponseVO batch(Long[] ids) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS, wyBasicInfoMapper.batch(ids));
    }

    @Override
    public ResponseVO getDetails(Long id) {

        WyBasicInfoDetailsList details = new WyBasicInfoDetailsList();
        WyBasicInfo info = wyBasicInfoMapper.getDetailsList(id);
        if (null != info) {
            details.setInfo(info);

            details.setWyTypes(wyBasicInfoMapper.selectWyTypes(info.getPid()));

            details.setPropertyTag(wyBasicInfoMapper.getSysDataTypesByLabelId(info.getPropertyTagId() == null ? 0 : info.getPropertyTagId()));
            details.setPropertyScale(wyBasicInfoMapper.getSysDataTypesByLabelId(info.getPropertyScaleId() == null ? 0 : info.getPropertyScaleId()));
            details.setPropertyStatus(wyBasicInfoMapper.getSysDataTypesByLabelId(info.getPropertyStatusId() == null ? 0 : info.getPropertyStatusId()));
            details.setPropertyType(wyBasicInfoMapper.getSysDataTypesByLabelId(info.getPropertyTypeId() == null ? 0 : info.getPropertyTypeId()));
            details.setMainPurpos(wyBasicInfoMapper.getSysDataTypesByLabelId(info.getMainPurposId() == null ? 0 : info.getMainPurposId()));
            details.setPropertySource(wyBasicInfoMapper.getSysDataTypesByLabelId(info.getPropertySourceId() == null ? 0 : info.getPropertySourceId()));
            details.setWyTag(wyBasicInfoMapper.getSysDataTypesByLabelId(info.getWyTagId() == null ? 0 : info.getWyTagId()));

            details.setPropertyImg(sysImgMapper.getBydbFileId(info.getPropertyImgId() == null ? 0 : info.getPropertyImgId()));
//            details.setFloorList(wyBasicInfoMapper.getFloorListByPid(info.getId() == null ? 0 : info.getId()));

            details.setAcceptanceReportFile(sysFilePathMapper.getBydbFileId(info.getAcceptanceReportFileId() == null ? 0 : info.getAcceptanceReportFileId()));
            details.setConstructPermitFile(sysFilePathMapper.getBydbFileId(info.getConstructPermitFileId() == null ? 0 : info.getConstructPermitFileId()));
            details.setLandTransferContractFile(sysFilePathMapper.getBydbFileId(info.getLandTransferContractFileId() == null ? 0 : info.getLandTransferContractFileId()));
            details.setRedLineFile(sysFilePathMapper.getBydbFileId(info.getRedLineFileId() == null ? 0 : info.getRedLineFileId()));
            details.setLandPermitFile(sysFilePathMapper.getBydbFileId(info.getLandPermitFileId() == null ? 0 : info.getLandPermitFileId()));

            details.setEmptyTotal(
                    wyBasicInfoMapper.queryEmptyList(id.toString(),null,null,null,null)
                            .stream()
                            .mapToDouble(EmptyList::getBuildArea)
                            .sum()
            );
            details.setUsedAreaTotal(
                 wyBasicInfoMapper.selectByWyId(
                         selectWyIdUsedList(id.toString())
                                 .toArray(new Long[selectWyIdUsedList(id.toString()).size()]))
                                 .stream()
                                 .filter(wyBasicInfo -> wyBasicInfo.getBuildArea()!=null)
                                 .mapToDouble(WyBasicInfo::getBuildArea)
                                 .sum()
            );

            details.setProrightAreaTotal(
                    wyBasicInfoMapper.queryProrightList(id.toString(),null,null,null,null)
                            .stream()
                            .mapToDouble(ProRightList::getBuildArea)
                            .sum()
            );
            details.setEntryAreaTotal(
                    wyBasicInfoMapper.queryEntryList(id.toString(),null,null,null,null)
                            .stream()
                            .mapToDouble(EntryList::getEntryArea)
                            .sum()
            );
            details.setNum(
                    wyBasicInfoMapper.queryFloorList(id.toString(),null,null,null).size()
            );

            HashMap<String, List<SysFilePath1>> map = new HashMap<>();
            for (String name : sysFilePathMapper.getName(info.getBuildCustFileId())) {
                map.put(name, sysFilePathMapper.getCustFile(info.getBuildCustFileId(), name));
            }
            details.setBuildCustFile(map);

            HashMap<String, List<SysFilePath1>> map1 = new HashMap<>();
            for (String name : sysFilePathMapper.getName(info.getSiteCustFileId())) {
                map1.put(name, sysFilePathMapper.getCustFile(info.getSiteCustFileId(), name));
            }
            details.setSiteCustFile(map1);

            details.setHouseImg(sysImgMapper.getBydbFileId(info.getHouseImgId() == null ? 0 : info.getHouseImgId()));

            if (info.getType()==2){
                List<WyFacilitiesInfo> wyFacilitiesInfos = wyFacilitiesInfoMapper.queryBywyBasicInfoIdByFloor(info.getPid(),details.getInfo().getUnitName());
                details.setWyFacilitiesInfoList(wyFacilitiesInfos);
            }else{
                List<WyFacilitiesInfo> wyFacilitiesInfoList = wyFacilitiesInfoMapper.queryBywyBasicInfoId(id);
                details.setWyFacilitiesInfoList(wyFacilitiesInfoList);
            }

            String relationName = wyBasicInfoMapper.getDetail(id).getRelationName();
            details.setRelationName(relationName);
            details.setRelationScale(wyBasicInfoMapper.getDetail(id).getRelationScale());
            if(details.getInfo().getPid()!=0){
                details.getInfo().setName(relationName+info.getUnitName());
            }


//            details.setWyName(wyBasicInfoMapper.getDetail(id).getWyName());

            return new ResponseVO(ErrorCodeEnum.SUCCESS, details);
        } else {
            return null;
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO updateInfo(WyBasicInfoDTO dto1) throws Exception {

        WyBasicInfoDTO dto = new WyBasicInfoDTO();
        dto = dto1;
        List<LabelManage> insertLabelManageList = new ArrayList<>();
        List<SysFilePath> insertSysFilePathList = new ArrayList<>();
        List<SysAttachment> insertSysAttachmentList = new ArrayList<>();
        List<SysRoleDataPermission> insertSysRoleDataPermissionList = new ArrayList<>();

        if (dto.getInfo().getUserId() == 0) {
            return new ResponseVO(ErrorCodeEnum.USERID_NOT_FOUND, ErrorCodeEnum.USERID_NOT_FOUND.getErrMsg());
        }
        if (dto.getInfo().getId() == 0) {
            return new ResponseVO(ErrorCodeEnum.IDISNULL, ErrorCodeEnum.IDISNULL.getErrMsg());
        }

        if (dto.getInfo().getType() == 1 || dto.getInfo().getType() == 6 || dto.getInfo().getType() == 7) {
            dto.getInfo().setPid((long) 0);
        }

        //数据库名称  与dto名称不等 需要校验名称是否重复
//        if (!dto.getInfo().getName().equals(wyBasicInfoMapper.selectNameByIdAndPid(dto.getInfo().getId(), dto.getInfo().getPid()))) {
//            Integer record = this.wyBasicInfoMapper.selectByNameAndPid( dto.getInfo().getPid(),dto.getInfo().getName());
//            if (record > 0) {
//                throw new BusinessException(ErrorCodeEnum.NAME_ALREADY_EXISTS);
//            }
//        }

        List<Future<?>> list = new ArrayList<>();
        long startTime = System.currentTimeMillis();

        WyBasicInfo info = dto.getInfo();
        info.setId(dto.getInfo().getId());
        info.setName(dto.getInfo().getName());
        if (dto.getInfo().getType() == 1 || dto.getInfo().getType() == 6 || dto.getInfo().getType() == 7) {
            info.setUnitName(dto.getInfo().getName());
        }else {
            info.setUnitName(dto.getInfo().getUnitName());
        }
        info.setBuildArea(dto.getInfo().getBuildArea()==null?null:dto.getInfo().getBuildArea());
        info.setPracticalArea(dto.getInfo().getPracticalArea()==null?null:dto.getInfo().getPracticalArea());
        info.setShareArea(dto.getInfo().getShareArea()==null?null:dto.getInfo().getShareArea());
        info.setLandArea(dto.getInfo().getLandArea()==null?null:dto.getInfo().getLandArea());
        Long propertyStatusId = IdWorker.getId();
        info.setPropertyStatusId(propertyStatusId);
        if (dto.getPropertyStatus() != null) {
            Long propertyStatusData = dto.getPropertyStatus();
            LabelManage propertyStatus = new LabelManage();
            propertyStatus.setSysDataTypeId(propertyStatusData);
            propertyStatus.setLabelId(propertyStatusId);

            insertLabelManageList.add(propertyStatus);
        }

        Long propertyTagId = IdWorker.getId();
        info.setPropertyTagId(propertyTagId);
        if (dto.getPropertyTag() != null) {
            Long propertyTagData = dto.getPropertyTag();

            LabelManage propertyTag = new LabelManage();
            propertyTag.setSysDataTypeId(propertyTagData);
            propertyTag.setLabelId(propertyTagId);
            System.out.println(propertyTag);

            insertLabelManageList.add(propertyTag);
        }

        Long mainPurposeId = IdWorker.getId();
        info.setMainPurposId(mainPurposeId);
        if (dto.getMainPurpos() != null) {
            Long mainPurposData = dto.getMainPurpos();
            LabelManage mainPurpos = new LabelManage();
            mainPurpos.setSysDataTypeId(mainPurposData);
            mainPurpos.setLabelId(mainPurposeId);

            insertLabelManageList.add(mainPurpos);
        }

        Long propertyTypeId = IdWorker.getId();
        info.setPropertyTypeId(propertyTypeId);
        if (dto.getPropertyType() != null) {
            for (Long propertyTypeData : dto.getPropertyType()) {
                LabelManage propertyType = new LabelManage();
                propertyType.setId(IdWorker.getId());
                propertyType.setSysDataTypeId(propertyTypeData);
                propertyType.setLabelId(propertyTypeId);

                insertLabelManageList.add(propertyType);
            }
        }

        Long propertySourceId = IdWorker.getId();
        info.setPropertySourceId(propertySourceId);
        if (dto.getPropertySource() != null) {
            for (Long propertySourceData : dto.getPropertySource()) {
                LabelManage propertySource = new LabelManage();
                propertySource.setLabelId(propertySourceId);
                propertySource.setSysDataTypeId(propertySourceData);

                insertLabelManageList.add(propertySource);
            }

        }

        Long wyTagId = IdWorker.getId();
        info.setWyTagId(wyTagId);
        if (dto.getWyTag() != null) {
            for (Long wyTagData : dto.getWyTag()) {
                LabelManage wyTag = new LabelManage();
                wyTag.setLabelId(wyTagId);
                wyTag.setSysDataTypeId(wyTagData);

                insertLabelManageList.add(wyTag);
            }
        }

        Long propertyImgId = IdWorker.getId();
        info.setPropertyImgId(propertyImgId);
        if (dto.getPropertyImg() != null) {
            for (SysImg propertyImgPath : dto.getPropertyImg()) {
                SysImg propertyImg = new SysImg();
                propertyImg.setId(IdWorker.getId());
                propertyImg.setName(propertyImgPath.getName());
                propertyImg.setPrimaryTableId(propertyImgId);
                propertyImg.setPath(propertyImgPath.getPath());

                Future<?> submit5 = executorService.submit(() -> {
                    sysImgMapper.addSysImg(propertyImg);
                });
                list.add(submit5);
            }
        }

        Long acceptanceReportFileId = IdWorker.getId();
        info.setAcceptanceReportFileId(acceptanceReportFileId);
        if (dto.getAcceptanceReportFile() != null) {
            for (SysFilePath acceptanceReportfile : dto.getAcceptanceReportFile()) {
                SysFilePath sysFilePath = new SysFilePath();

                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(acceptanceReportFileId);
                sysFilePath.setPath(acceptanceReportfile.getPath());
                sysFilePath.setFileName(acceptanceReportfile.getFileName());
                sysFilePath.setFileSize(acceptanceReportfile.getFileSize());
                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long constructPermitFileId = IdWorker.getId();
        info.setConstructPermitFileId(constructPermitFileId);
        if (dto.getConstructPermitFile() != null) {
            for (SysFilePath constructPermitFile : dto.getConstructPermitFile()) {
                SysFilePath sysFilePath = new SysFilePath();

                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(constructPermitFileId);
                sysFilePath.setPath(constructPermitFile.getPath());
                sysFilePath.setFileName(constructPermitFile.getFileName());
                sysFilePath.setFileSize(constructPermitFile.getFileSize());

                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long buildCustFileId = IdWorker.getId();
        info.setBuildCustFileId(buildCustFileId);
        if (dto.getBuildCustFile() != null) {
            Set<String> keySet = dto.getBuildCustFile().keySet();
            List<String> strings = keySet.stream().collect(Collectors.toList());
            for (String string : strings) {
                List<SysFilePath> vos = dto.getBuildCustFile().get(string);
                for (SysFilePath sysfile : vos) {
                    SysFilePath sysFilePath = new SysFilePath();
                    Long FileId = IdWorker.getId();
                    sysFilePath.setId(FileId);
                    sysFilePath.setFileName(sysfile.getFileName());
                    sysFilePath.setFileSize(sysfile.getFileSize());
                    sysFilePath.setPath(sysfile.getPath());
                    sysFilePath.setDbFileId(buildCustFileId);

                    insertSysFilePathList.add(sysFilePath);

                    SysAttachment attachment = new SysAttachment();
                    attachment.setId(IdWorker.getId());
                    attachment.setSysFilePathId(FileId);
                    attachment.setName(string);
                    insertSysAttachmentList.add(attachment);
                }
            }
        }

        Long landTransferContractFileId = IdWorker.getId();
        info.setLandTransferContractFileId(landTransferContractFileId);
        if (dto.getLandTransferContractFile() != null) {
            for (SysFilePath landTransferContractFile : dto.getLandTransferContractFile()) {


                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(landTransferContractFileId);

                sysFilePath.setPath(landTransferContractFile.getPath());
                sysFilePath.setFileName(landTransferContractFile.getFileName());
                sysFilePath.setFileSize(landTransferContractFile.getFileSize());

                insertSysFilePathList.add(sysFilePath);
            }
        }


        Long redLineFileId = IdWorker.getId();
        info.setRedLineFileId(redLineFileId);
        if (dto.getRedLineFile() != null) {
            for (SysFilePath redLineFile : dto.getRedLineFile()) {


                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(redLineFileId);
                sysFilePath.setPath(redLineFile.getPath());
                sysFilePath.setFileName(redLineFile.getFileName());
                sysFilePath.setFileSize(redLineFile.getFileSize());

                insertSysFilePathList.add(sysFilePath);
            }
        }


        Long landPermitFileId = IdWorker.getId();
        info.setLandPermitFileId(landPermitFileId);
        if (dto.getLandPermitFile() != null) {
            for (SysFilePath landPermitFile : dto.getLandPermitFile()) {


                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(landPermitFileId);

                sysFilePath.setPath(landPermitFile.getPath());
                sysFilePath.setFileName(landPermitFile.getFileName());
                sysFilePath.setFileSize(landPermitFile.getFileSize());

                insertSysFilePathList.add(sysFilePath);
            }

        }

        Long siteCustFileId = IdWorker.getId();
        info.setSiteCustFileId(siteCustFileId);
        if (dto.getSiteCustFile() != null) {
            Set<String> keySet = dto.getSiteCustFile().keySet();
            List<String> strings = keySet.stream().collect(Collectors.toList());
            for (String string : strings) {
                List<SysFilePath> vos = dto.getSiteCustFile().get(string);
                for (SysFilePath sysfile : vos) {
                    SysFilePath sysFilePath = new SysFilePath();
                    Long FileId = IdWorker.getId();
                    sysFilePath.setId(FileId);
                    sysFilePath.setFileName(sysfile.getFileName());
                    sysFilePath.setFileSize(sysfile.getFileSize());
                    sysFilePath.setPath(sysfile.getPath());
                    sysFilePath.setDbFileId(siteCustFileId);

                    insertSysFilePathList.add(sysFilePath);

                    SysAttachment attachment = new SysAttachment();
                    attachment.setId(IdWorker.getId());
                    attachment.setSysFilePathId(FileId);
                    attachment.setName(string);
                    insertSysAttachmentList.add(attachment);
                }
            }
        }

        Long houseImgId = IdWorker.getId();
        info.setHouseImgId(houseImgId);
        if (dto.getHouseImg() != null) {
            for (SysImg houseImg : dto.getHouseImg()) {

                SysImg sysImg = new SysImg();
                sysImg.setId(IdWorker.getId());
                sysImg.setPath(houseImg.getPath());
                sysImg.setName(houseImg.getName());
                sysImg.setPrimaryTableId(houseImgId);

                Future<?> submit17 = executorService.submit(() -> {
                    sysImgMapper.addSysImg(sysImg);
                });
                list.add(submit17);
            }
        }


        info.setLandStartTime(StringUtils.isBlank(dto.getInfo().getLandStartTimeStr()) ? null : toolUtils.strFormatDate(dto.getInfo().getLandStartTimeStr()));
        info.setLandEndTime(StringUtils.isBlank(dto.getInfo().getLandStartTimeStr()) ? null : toolUtils.strFormatDate(dto.getInfo().getLandEndTimeStr()));
        info.setPropertyIntroduction(dto.getInfo().getPropertyIntroduction());
        info.setHouseCode(dto.getInfo().getHouseCode());
        info.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        info.setUserId(dto.getInfo().getUserId());


//        if (!sysUserOrgMapper.findSelfOrgIds(dto.getInfo().getUserId()).contains(dto.getInfo().getOpreatesGroupId())){
//            throw new BusinessException(ErrorCodeEnum.UNAUTHORIZED);
//        }

        WyBasicInfo vo = dto.getInfo();
        if ((vo.getType() == 1 || vo.getType() == 6 || vo.getType() == 7) && null != vo.getOperatesType() && vo.getOperatesType() != 1) {

            sysRoleDataPermissionMapper.deleteByWyBasicInfoIdAfter(vo.getId());

            List<Long> roles = sysUserOrgMapper.findRolesByOrg(vo.getOpreatesGroupId());
            for (Long id : roles) {
                SysRoleDataPermission permissions = new SysRoleDataPermission();
                permissions.setId(IdWorker.getId());
                permissions.setWyBasicInfoId(vo.getId());
                permissions.setRoleId(id);
                insertSysRoleDataPermissionList.add(permissions);
            }
        } else if(null != vo.getOperatesType()){
            sysRoleDataPermissionMapper.deleteByWyBasicInfoIdAfter(vo.getId());

            Long role = sysUserRoleMapper.selectRoleByUserId(vo.getUserId());
            SysRoleDataPermission permission = new SysRoleDataPermission();
            permission.setId(IdWorker.getId());
            permission.setWyBasicInfoId(vo.getId());
            permission.setRoleId(role);
            sysRoleDataPermissionMapper.insertSelf(permission);
        }


        if (dto.getWyFacilitiesInfo() != null) {
            wyFacilitiesInfoMapper.deleteByWyBasicInfoId(dto.getInfo().getId());
            for (WyFacilitiesInfo wyFacilitiesInfo : dto.getWyFacilitiesInfo()) {
                wyFacilitiesInfo.setWyBasicInfoId(dto.getInfo().getId());
                wyFacilitiesInfoMapper.addWyFacilitiesInfo(wyFacilitiesInfo);
            }
        }

        if(!insertSysAttachmentList.isEmpty()){
            this.sysAttachmentMapper.insertBatch(insertSysAttachmentList);
        }
        if(!insertSysFilePathList.isEmpty()){
            this.sysFilePathMapper.insertBatch(insertSysFilePathList);
        }
        if(!insertSysRoleDataPermissionList.isEmpty()){
            this.sysRoleDataPermissionMapper.insertBatch(insertSysRoleDataPermissionList);
        }

        long task1 = System.currentTimeMillis();
        System.out.println("task1+" + (task1 - startTime));

        wyBasicInfoMapper.updateById(info);

        /**
         * 批量插入、更新 标签、分组
         */
        Long id = info.getId();
        List<Long> wyIds = this.wyBasicInfoMapper.selectAllSonPid(id);
        List<WyBasicInfo> updateWyInfoList = new ArrayList<>();

        for (Long wyId : wyIds) {
            WyBasicInfo wyBasicInfo = new WyBasicInfo();
            wyBasicInfo.setId(wyId);

            long tagId = IdWorker.getId();
            wyBasicInfo.setPropertyTagId(tagId);
            updateWyInfoList.add(wyBasicInfo);

            LabelManage labelManageTemp = new LabelManage();
            labelManageTemp.setId(IdWorker.getId());
            labelManageTemp.setLabelId(tagId);
            labelManageTemp.setSysDataTypeId(dto.getPropertyTag());
            insertLabelManageList.add(labelManageTemp);
        }
        this.wyBasicInfoMapper.updateBatch(updateWyInfoList);
        this.labelManageMapper.insertBatch(insertLabelManageList);

        for (Future<?> future : list) {
            try {
                future.get();
            } catch (Exception e) {
                log.error("修改资产登记异常:{}", e.toString());
                throw new RuntimeException(ErrorCodeEnum.ERROR_BACK.getErrMsg());
            }

        }

        long endTime = System.currentTimeMillis();
        log.info("程序运行时间： " + (endTime - startTime) + "ms");

        return new ResponseVO(ErrorCodeEnum.SUCCESS, null);

    }

    @Override
    public ResponseVO propertyStructure(Long id, Long userId) {

        List<PropertyStructureVo> propertyStructureVos = getPropertyStructureHandler(id, userId);

        return new ResponseVO(ErrorCodeEnum.SUCCESS, propertyStructureVos);
    }

    @Override
    public List<PropertyStructureVo> getPropertyStructureHandler(Long id, Long userId) {
        /**
         * 查询出所有的结构
         */
        List<PropertyStructureVo> vos = wyBasicInfoMapper.getPropertyStructureByUserId(id, userId);

        List<Long> hasUseWyIdList = new ArrayList<>();
        List<Long> wyIdList = vos.stream().map(PropertyStructureVo::getId).collect(Collectors.toList());

        QueryWrapper<YyUseReg> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("wy_basic_info_id" , wyIdList);
        List<YyUseReg> yyUseRegs = this.yyUseRegMapper.selectList(queryWrapper);
        if(null != yyUseRegs && !yyUseRegs.isEmpty()){
            hasUseWyIdList = yyUseRegs.stream().map(e -> e.getWyBasicInfoId()).collect(Collectors.toList());
        }

        for (PropertyStructureVo vo : vos) {
            if(!hasUseWyIdList.contains(vo.getId())){
                vo.setIsEmpty(true);
            }
            vo.setWyTypes(wyBasicInfoMapper.getSysDataTypesByLabelId(vo.getWyTypeId()));
            if (vo.getId().equals(id)) {
                vo.setFacilitiesInfos(wyBasicInfoMapper.selectWyFacilitiesByWyId(id));
            }
        }

        Long minId = Long.parseLong(Long.MAX_VALUE + "");
        Map<Long, List<PropertyStructureVo>> map = new HashMap<>();
        for (PropertyStructureVo propertyStructureVo : vos) {
            Long pid = propertyStructureVo.getPid();

            if (pid < minId) {
                minId = pid;
            }

            List<PropertyStructureVo> list = new ArrayList<>();
            if (map.containsKey(pid)) {
                list = map.get(pid);
            }
            list.add(propertyStructureVo);

            map.put(pid, list);
        }

        List<PropertyStructureVo> pidList = map.get(minId);
        List<PropertyStructureVo> propertyStructureVos = propertyStructureList(pidList, map);
        return propertyStructureVos;
    }

    @Override
    public double countEmptyAreaByPropertyStructure(Long id, Long userId) {
        //根据父节点 获取所有子节点
        List<PropertyStructureVo> vos = wyBasicInfoMapper.getPropertyStructureByUserId(id, userId);

        //获取物业id
        List<Long> hasUseWyIdList = new ArrayList<>();
        List<Long> wyIdList = vos.stream().map(PropertyStructureVo::getId).collect(Collectors.toList());

        //根据物业id 查询使用登记表
        QueryWrapper<YyUseReg> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("wy_basic_info_id" , wyIdList);
        List<YyUseReg> yyUseRegs = this.yyUseRegMapper.selectList(queryWrapper);
        if(null != yyUseRegs && !yyUseRegs.isEmpty()){
            hasUseWyIdList = yyUseRegs.stream().map(YyUseReg::getWyBasicInfoId).collect(Collectors.toList());
        }

        //设置空置状态
        for (PropertyStructureVo vo : vos) {
            if(!hasUseWyIdList.contains(vo.getId())){
                vo.setIsEmpty(true);
            }
        }

        //将集合 转换为 结构树 开始
        Long minId = Long.parseLong(Long.MAX_VALUE + "");
        Map<Long, List<PropertyStructureVo>> map = new HashMap<>();
        for (PropertyStructureVo propertyStructureVo : vos) {
            Long pid = propertyStructureVo.getPid();

            if (pid < minId) {
                minId = pid;
            }

            List<PropertyStructureVo> list = new ArrayList<>();
            if (map.containsKey(pid)) {
                list = map.get(pid);
            }
            list.add(propertyStructureVo);

            map.put(pid, list);
        }

        List<PropertyStructureVo> pidList = map.get(minId);
        List<PropertyStructureVo> propertyStructureVos = propertyStructureList(pidList, map);
        //将集合 转换为 结构树 结束

        //利用结构树 计算 空置面积
        Double total = StructureUtils.countTotalEmptyArea(propertyStructureVos);
        BigDecimal returnTotal = new BigDecimal(total);

        return returnTotal.setScale(2 , BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    @Override
    public ResponseVO getEmptyWyInfo(Long userId) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS,wyBasicInfoMapper.getEmptyWyInfo(userId));
    }

    /**
     * 结构化树
     *
     * @param dto
     * @param map
     * @return
     */
    private List<PropertyStructureVo> propertyStructureList(List<PropertyStructureVo> dto, Map<Long, List<PropertyStructureVo>> map) {

        if (null == dto || dto.isEmpty()) {
            return null;
        }

        for (PropertyStructureVo vo : dto) {
            List<PropertyStructureVo> list = map.get(vo.getId());
            vo.setChildren(list);
            propertyStructureList(vo.getChildren(), map);
        }
        return dto;
    }

    @Override
    public ResponseVO nameCheck(Long pid,String name,String unitName) {

        Integer i = wyBasicInfoMapper.selectByNameAndPid(pid, name,unitName);
        if (i>0){
            return new ResponseVO(ErrorCodeEnum.ALREAD_EXIT,ErrorCodeEnum.ALREAD_EXIT.getErrMsg());
        }else {
            return new ResponseVO(ErrorCodeEnum.SUCCESS,ErrorCodeEnum.SUCCESS.getErrMsg());
        }
    }

    @Override
    public ResponseVO getPropertyName(String name) {

        return new ResponseVO(ErrorCodeEnum.SUCCESS, wyBasicInfoMapper.selectByName(name));
    }

    @Override
    public ResponseVO logicDel(Long id) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS, wyBasicInfoMapper.logicDel(id));
    }

    @Override
    public ResponseVO getRentalSocialWyList(SocicalWyDTO dto) {
        if (dto.getP() <= 0) {
            return new ResponseVO(ErrorCodeEnum.SUCCESS, null);
        }
        dto.setP((dto.getP() - 1) * dto.getSize());
        List<RentalSocialWyVo> list = wyBasicInfoMapper.getRentalSocialWyList(dto);
        PageVO<RentalSocialWyVo> vo = new PageVO<>();
        vo.setP(dto.getP() / dto.getSize() + 1);
        vo.setSize(dto.getSize());

        vo.setTotal(wyBasicInfoMapper.getRentalSocialWyListTotal(dto).size());
        vo.setList(list);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, vo);
    }

    @Override
    public ResponseVO seeRow(Long id) {
        RentalSosialVo seeRow = wyBasicInfoMapper.seeRow(id);
        ShWyVO shWyVO = new ShWyVO();
        if (null != seeRow.getPropertyImgId()) {
            List<SysImg1> imgList = sysImgMapper.getBydbFileId(seeRow.getPropertyImgId());
            shWyVO.setPropertyImgPath(imgList);
        }
        if (null != seeRow.getHouseImgId()) {
            List<SysImg1> houseImgs = sysImgMapper.getBydbFileId(seeRow.getHouseImgId());
            shWyVO.setHouseImgs(houseImgs);
        }
        shWyVO.setRentalSosialVo(seeRow);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, shWyVO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO addRentalSocialWy(RentalSocialWyInfoDTO dto1) throws Exception {
        RentalSocialWyInfoDTO dto = new RentalSocialWyInfoDTO();
        dto = dto1;
        if (null == wyBasicInfoMapper.getDetailsList(dto.getId())) {
            WyBasicInfo wyBasicInfo = new WyBasicInfo();
            long wyId = IdWorker.getId();
            wyBasicInfo.setId(wyId);

            if (null != wyBasicInfoMapper.queryByName(dto.getName())) {
                throw new BusinessException(ErrorCodeEnum.NAME_ALREADY_EXISTS);
            }
            wyBasicInfo.setAssetType(dto.getAssetType());
            wyBasicInfo.setPid((long) 0);
            wyBasicInfo.setName(dto.getName());
            wyBasicInfo.setAliasName(dto.getAliasName());
            wyBasicInfo.setPropertyCode(dto.getPropertyCode());
            wyBasicInfo.setAddress(dto.getAddress());
            wyBasicInfo.setAreaCode(dto.getAreaCode());
            wyBasicInfo.setHouseCode(dto.getHouseCode());
            wyBasicInfo.setDetailAddr(dto.getDetailAddr());
            wyBasicInfo.setZlRentalArea(dto.getZlRentalArea());
            wyBasicInfo.setZlStartRentDate(toolUtils.strFormatDate(dto.getZlStartRentDate()));
            wyBasicInfo.setZlEndRentDate(toolUtils.strFormatDate(dto.getZlEndRentDate()));
            wyBasicInfo.setZlMonthlyRent(dto.getZlMonthlyRent());
            wyBasicInfo.setZlRentUnitPrice(dto.getZlRentUnitPrice());
            wyBasicInfo.setLessor(dto.getLessor());
            wyBasicInfo.setLessorPhone(dto.getLessorPhone());
            wyBasicInfo.setCompanyName(dto.getCompanyName());
            wyBasicInfo.setUserId(dto.getUserId());
            wyBasicInfo.setStoreyHeight(dto.getStoreyHeight() == null ? 0 : dto.getStoreyHeight());
            wyBasicInfo.setLoadBearer(dto.getLoadBearer() == null ? 0 : dto.getLoadBearer());
            wyBasicInfo.setLongitude(dto.getLongitude());
            wyBasicInfo.setLatitude(dto.getLatitude());
            wyBasicInfo.setTowards(dto.getTowards() == null ? 0 : dto.getTowards());
            wyBasicInfo.setThreeDimen(dto.getThreeDimen() == null ? null : dto.getThreeDimen());

            wyBasicInfo.setCreateTime(new Timestamp(System.currentTimeMillis()));

            long zlLeaseUseId = IdWorker.getId();
            LabelManage labelManage = new LabelManage();
            wyBasicInfo.setZlLeaseUseId(zlLeaseUseId);
            labelManage.setId(IdWorker.getId());
            labelManage.setSysDataTypeId(dto.getLeaseUseId());
            labelManage.setLabelId(zlLeaseUseId);

            int i = labelManageMapper.addLabelManage(labelManage);

            //图片添加
            long wyImgId = IdWorker.getId();
            if (dto.getPropertyImgPath() != null) {
                for (SysImg wyImgPath : dto.getPropertyImgPath()) {
                    SysImg img = new SysImg();
                    img.setId(IdWorker.getId());
                    img.setPrimaryTableId(wyImgId);

                    wyBasicInfo.setPropertyImgId(wyImgId);
                    img.setPath(wyImgPath.getPath());
                    img.setName(wyImgPath.getName());
                    sysImgMapper.addSysImg(img);
                }
            }

            Long houseImgId = IdWorker.getId();
            wyBasicInfo.setHouseImgId(houseImgId);
            if (dto.getHouseImgs() != null) {
                for (SysImg sysImg : dto.getHouseImgs()) {
                    SysImg img = new SysImg();

                    img.setId(IdWorker.getId());
                    img.setPrimaryTableId(houseImgId);
                    img.setName(sysImg.getName());
                    img.setPath(sysImg.getPath());
                    sysImgMapper.addSysImg(img);
                }
            }


            sysRoleDataPermissionMapper.deleteByWyBasicInfoIdAfter(dto.getId());

            Long role = sysUserRoleMapper.selectRoleByUserId(dto.getUserId());
            SysRoleDataPermission permission = new SysRoleDataPermission();
            permission.setId(IdWorker.getId());
            permission.setWyBasicInfoId(wyId);
            permission.setRoleId(role);

            Integer u = sysRoleDataPermissionMapper.insertSelf(permission);


            //物业添加
            int w = wyBasicInfoMapper.addBasic(wyBasicInfo);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, i + w + u);
        }
        return new ResponseVO(ErrorCodeEnum.ERROR_BACK, ErrorCodeEnum.ERROR_BACK.getErrMsg());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO updRentalSocialWy(RentalSocialWyInfoDTO dto1) throws Exception {
        RentalSocialWyInfoDTO dto = new RentalSocialWyInfoDTO();
        dto = dto1;
        WyBasicInfo info = wyBasicInfoMapper.getDetailsList(dto.getId());

        if(null == info){
            throw new BusinessException(ErrorCodeEnum.WY_INFO_IS_NOT_FOUND);
        }

        if (dto.getId() == 0) {
            throw new BusinessException(ErrorCodeEnum.IDISNULL);
        }

        info.setName(dto.getName());
        info.setAliasName(dto.getAliasName());
        info.setPropertyCode(dto.getPropertyCode());
        info.setHouseCode(dto.getHouseCode());
        info.setAddress(dto.getAddress());
        info.setDetailAddr(dto.getDetailAddr());
        info.setZlRentalArea(dto.getZlRentalArea());
        info.setZlStartRentDate(toolUtils.strFormatDate(dto.getZlStartRentDate()));
        info.setZlEndRentDate(toolUtils.strFormatDate(dto.getZlEndRentDate()));
        info.setZlMonthlyRent(dto.getZlMonthlyRent());
        info.setZlRentUnitPrice(dto.getZlRentUnitPrice());
        info.setLessor(dto.getLessor());
        info.setLessorPhone(dto.getLessorPhone());
        info.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        info.setUserId(dto.getUserId());
        info.setCompanyName(dto.getCompanyName());
        info.setStoreyHeight(dto.getStoreyHeight() == null ? 0 : dto.getStoreyHeight());
        info.setLoadBearer(dto.getLoadBearer() == null ? 0 : dto.getLoadBearer());
        info.setLongitude(dto.getLongitude());
        info.setLatitude(dto.getLatitude());
        info.setTowards(dto.getTowards() == null ? 0 : dto.getTowards());
        info.setThreeDimen(dto.getThreeDimen() == null ? null : dto.getThreeDimen());


        //租赁用途修改
        long zlLeaseUseId = IdWorker.getId();
        LabelManage labelManage = new LabelManage();
        info.setZlLeaseUseId(zlLeaseUseId);
        labelManage.setId(IdWorker.getId());
        labelManage.setSysDataTypeId(dto.getLeaseUseId());
        labelManage.setLabelId(zlLeaseUseId);

        long propertyTagId = IdWorker.getId();
        LabelManage labelManage1 = new LabelManage();
        info.setPropertyTagId(propertyTagId);
        labelManage1.setId(IdWorker.getId());
        labelManage1.setLabelId(propertyTagId);
        labelManage1.setSysDataTypeId(81L);

        int i = labelManageMapper.addLabelManage(labelManage);
        int i1 = labelManageMapper.addLabelManage(labelManage1);

        long wyImgId = IdWorker.getId();
        info.setPropertyImgId(wyImgId);
        if (dto.getPropertyImgPath() != null) {
            for (SysImg wyImgPath : dto.getPropertyImgPath()) {
                SysImg img = new SysImg();

                img.setId(IdWorker.getId());
                img.setPrimaryTableId(wyImgId);

                img.setPath(wyImgPath.getPath());
                img.setName(wyImgPath.getName());
                sysImgMapper.addSysImg(img);
            }
        }

        Long houseImgId = IdWorker.getId();
        info.setHouseImgId(houseImgId);
        if (dto.getHouseImgs() != null) {
            for (SysImg sysImg : dto.getHouseImgs()) {
                SysImg img = new SysImg();

                img.setId(IdWorker.getId());
                img.setPrimaryTableId(houseImgId);
                img.setName(sysImg.getName());
                img.setPath(sysImg.getPath());
                sysImgMapper.addSysImg(img);
            }
        }

        Long role = sysUserRoleMapper.selectRoleByUserId(dto.getUserId());
        SysRoleDataPermission permission = new SysRoleDataPermission();
        permission.setId(IdWorker.getId());
        permission.setWyBasicInfoId(dto.getId());
        permission.setRoleId(role);

        Integer u = sysRoleDataPermissionMapper.insertSelf(permission);

        int j = wyBasicInfoMapper.updateById(info);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, i + i1 + j + u);

    }

    @Override
    public ResponseVO delRentalSocialWy(Long[] id) {
        for (Long aLong : id) {
            wyBasicInfoMapper.DelShWy(aLong);
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null);
    }

    @Override
    public List<WyBaseInfoVO> selectAllWyBaseInfo(String contractId, String userId, Boolean isUseReg) {

        //根据用户id对应的权限角色查询物业信息
        if (StringUtils.isNotBlank(userId)) {
            List<Long> wyBasicIdList = this.sysRoleDataPermissionMapper.selectDataPermissionByUserId(userId, isUseReg);
            return this.wyBasicInfoMapper.selectByWyIdList(wyBasicIdList);
        }

        //根据合同id查询所有物业信息
        return this.wyBasicInfoMapper.selectAllWyInfo(contractId);
    }

    @Override
    public ResponseVO selectByWyId(Long[] wyIds) {
        WyBasicInfoLeaseVO vo = new WyBasicInfoLeaseVO();

        List<WyBasicInfo> infos = wyBasicInfoMapper.selectByWyId(wyIds);

        vo.setLeaserAddress(infos.stream().map(WyBasicInfo::getAddress).collect(Collectors.toList()));
        vo.setHouseCode(infos.stream().map(WyBasicInfo::getHouseCode).collect(Collectors.toList()));

        vo.setShareArea(infos.stream().mapToDouble(WyBasicInfo::getBuildArea).sum());
        vo.setRentArea(infos.stream().mapToDouble(WyBasicInfo::getShareArea).sum());


        return new ResponseVO(ErrorCodeEnum.SUCCESS, vo);
    }

    @Override
    public WyBasicInfoInDataPermissionVO selectListInDataPermission(WyBasicInfoInDataPermissionDTO wyBasicInfoInDataPermissionDTO) {

        Long roleId = wyBasicInfoInDataPermissionDTO.getRoleId();

        wyBasicInfoInDataPermissionDTO.setRoleId(null);
        List<WyBasicInfoInDataPermissionPO> wyBasicInfoInDataPermissionPOList = this.wyBasicInfoMapper.selectListInDataPermission(wyBasicInfoInDataPermissionDTO);

        WyBasicInfoInDataPermissionVO returnVO = new WyBasicInfoInDataPermissionVO();
        returnVO.setAllWyInfoList(wyBasicInfoInDataPermissionPOList);

        List<WyBasicInfoInDataPermissionPO> chooseList = this.wyBasicInfoMapper.selectListInDataPermissionByRoleId(roleId);
        returnVO.setChooseList(chooseList);

        return returnVO;
    }

    @Override
    public ResponseVO queryEmptyList(String id,String wyName,String preCompany,Integer p,Integer size) {

        if (p <= 0) {
            return new ResponseVO(ErrorCodeEnum.NULLERROR, ErrorCodeEnum.NULLERROR.getErrMsg());
        }

        WyBasicInfo wyBasicInfo = this.wyBasicInfoMapper.selectById(id);

        p=(p-1)*size;
        List<EmptyList> list = wyBasicInfoMapper.queryEmptyList(id, wyName, preCompany, p, size);

        for (EmptyList emptyList : list) {
            emptyList.setThreeDimen(wyBasicInfo.getThreeDimen());
        }

        PageVO<EmptyList> vo = new PageVO<>();
        vo.setList(list);
        vo.setP(p/size + 1);
        vo.setSize(size);
        vo.setTotal(wyBasicInfoMapper.queryEmptyList(id, wyName, preCompany,null,null).size());
        return  new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO queryUsedList(String id,String wyName,String objName,Integer p,Integer size) {

        if (p <= 0) {
            return new ResponseVO(ErrorCodeEnum.NULLERROR, ErrorCodeEnum.NULLERROR.getErrMsg());
        }
        p=(p-1)*size;
        List<UsedList> list = wyBasicInfoMapper.queryUsedList(id, wyName, objName, p, size);
        PageVO<UsedList> vo = new PageVO<>();
        vo.setList(list);
        vo.setP(p/size + 1);
        vo.setSize(size);
        vo.setTotal(wyBasicInfoMapper.queryUsedList(id, wyName, objName,null,null).size());
        return  new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }

    @Override
    public ResponseVO queryProrightList(String id,String wyName,String certificateNo,Integer p,Integer size) {
        if (p <= 0) {
            return new ResponseVO(ErrorCodeEnum.NULLERROR, ErrorCodeEnum.NULLERROR.getErrMsg());
        }
        p=(p-1)*size;
        List<ProRightList> list = wyBasicInfoMapper.queryProrightList(id, wyName, certificateNo, p, size);
        PageVO<ProRightList> vo = new PageVO<>();
        vo.setList(list);
        vo.setP(p/size + 1);
        vo.setSize(size);
        vo.setTotal(wyBasicInfoMapper.queryProrightList(id, wyName, certificateNo,null,null).size());
        return  new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }

    @Override
    public ResponseVO queryEntryList(String id,String wyName,String entryType,Integer p,Integer size) {
        if (p <= 0) {
            return new ResponseVO(ErrorCodeEnum.NULLERROR, ErrorCodeEnum.NULLERROR.getErrMsg());
        }
        p=(p-1)*size;
        List<EntryList> list = wyBasicInfoMapper.queryEntryList(id, wyName, entryType, p, size);
        PageVO<EntryList> vo = new PageVO<>();
        vo.setList(list);
        vo.setP(p/size + 1);
        vo.setSize(size);
        vo.setTotal(wyBasicInfoMapper.queryEntryList(id, wyName, entryType,null,null).size());
        return  new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO queryBuildingList(String id, String wyName,Integer p,Integer size) {
        if (p <= 0) {
            return new ResponseVO(ErrorCodeEnum.NULLERROR, ErrorCodeEnum.NULLERROR.getErrMsg());
        }
        p=(p-1)*size;
        List<BuildingList> lists = wyBasicInfoMapper.queryBuildingList(id, wyName,p,size);
        for (BuildingList list : lists) {

            Long[] longs = selectWyIdUsedList(id).toArray(new Long[selectWyIdUsedList(id).size()]);
            List<EmptyList> emptyLists = wyBasicInfoMapper.queryEmptyList(id, null, null,null,null);
            double emptyArea = emptyLists.stream().mapToDouble(EmptyList::getBuildArea).sum();
            double usedArea = wyBasicInfoMapper.selectByWyId(longs).stream().mapToDouble(WyBasicInfo::getBuildArea).sum();
            list.setUsedArea(usedArea);
            list.setEmptyArea(emptyArea);
        }
        PageVO<BuildingList> vo = new PageVO<>();
        vo.setList(lists);
        vo.setP(p/size + 1);
        vo.setSize(size);
        vo.setTotal(wyBasicInfoMapper.queryBuildingList(id, wyName,null,null).size());
        return  new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO queryFloorList(String id,String wyName,Integer p,Integer size) {
        if (p <= 0) {
            return new ResponseVO(ErrorCodeEnum.NULLERROR, ErrorCodeEnum.NULLERROR.getErrMsg());
        }

        WyBasicInfo wyBasicInfo = this.wyBasicInfoMapper.selectById(id);

        p=(p-1)*size;
        List<FloorList> lists = wyBasicInfoMapper.queryFloorList(id, wyName,p,size);
        for (FloorList list : lists) {
            Long[] longs = selectWyIdUsedList(id).toArray(new Long[selectWyIdUsedList(id).size()]);
            List<EmptyList> emptyLists = wyBasicInfoMapper.queryEmptyList(id, null,null,null,null);
            double emptyArea = emptyLists.stream().mapToDouble(EmptyList::getBuildArea).sum();
            double usedArea = wyBasicInfoMapper.selectByWyId(longs).stream().mapToDouble(WyBasicInfo::getBuildArea).sum();
            list.setUsedArea(usedArea);
            list.setEmptyArea(emptyArea);
            list.setThreeDimen(wyBasicInfo.getThreeDimen());

        }
        PageVO<FloorList> vo = new PageVO<>();
        vo.setList(lists);
        vo.setP(p/size + 1);
        vo.setSize(size);
        vo.setTotal(wyBasicInfoMapper.queryFloorList(id, wyName,null,null).size());
        return  new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO queryUnitList(String id, String wyName, String company, String status,Integer p,Integer size) {
        if (p <= 0) {
            return new ResponseVO(ErrorCodeEnum.NULLERROR, ErrorCodeEnum.NULLERROR.getErrMsg());
        }

        WyBasicInfo wyBasicInfo = this.wyBasicInfoMapper.selectById(id);

        p=(p-1)*size;
        List<UnitList> lists = wyBasicInfoMapper.queryUnitList(id, wyName, company,status,p,size);

        for (UnitList list : lists) {
            list.setThreeDimen(wyBasicInfo.getThreeDimen());
        }

        PageVO<UnitList> vo = new PageVO<>();
        vo.setList(lists);
        vo.setP(p/size + 1);
        vo.setSize(size);
        vo.setTotal(wyBasicInfoMapper.queryUnitList(id, wyName, status,null,null,null).size());
        return  new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }


    private List<Long> selectWyIdUsedList(String maxPid) {

        /**
         * 根据物业id 查询其下所有子节点（包含自身） 得到物业id集合A
         * 根据集合A 内关联查询 使用登记表 得到物业id集合B
         * 根据集合A 内关联查询 合同表关联物业基础信息表 得到物业id集合C
         * 取集合B与集合C的并集
         */

        List<Long> yyUserRegWyIdList = this.wyBasicInfoMapper.selectAllSonPidWithYyUseReg(Long.parseLong(maxPid));
        List<Long> yyContractWyIdList = this.wyBasicInfoMapper.selectAllSonPidWithYyContractWyRelation(Long.parseLong(maxPid));

        HashSet<Long> yyUserRegWyIdSet = new HashSet<>(yyUserRegWyIdList);
        HashSet<Long> yyContractWyIdSet = new HashSet<>(yyContractWyIdList);
        //并集
        Set<Long> union = SetOptUtils.union(yyUserRegWyIdSet, yyContractWyIdSet);

        return new ArrayList<>(union);
    }


    private List<PropertyStructureVo> propertyStructureList(List<PropertyStructureVo> dto) {
        for (int i = 0; i < dto.size(); i++) {
            List<PropertyStructureVo> list = wyBasicInfoMapper.getPropertyStructureById(dto.get(i).getId());

            dto.get(i).setChildren(list);
            propertyStructureList(dto.get(i).getChildren());
        }
        return dto;
    }

    @Override
    public ResponseVO insert(WyBasicInfo entity) {
        long ct = System.currentTimeMillis();
        Timestamp timestamp = TimeUtils.LongToDate(ct);
        entity.setCreateTime(timestamp);

        String propertyCode = new ToolUtils().getPropertyCode();
        entity.setPropertyCode(propertyCode);
        int i = wyBasicInfoMapper.insert(entity);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, i);
    }

    @Override
    public ResponseVO updateById(WyBasicInfo entity) {
        int i = wyBasicInfoMapper.updateById(entity);
        return new ResponseVO(ErrorCodeEnum.SUCCESS, i);
    }

    @Override
    public ResponseVO deleteById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO deleteByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO delete(Wrapper<WyBasicInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO deleteBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }


    @Override
    public ResponseVO update(WyBasicInfo entity, Wrapper<WyBasicInfo> updateWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO selectBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO selectOne(Wrapper<WyBasicInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectCount(Wrapper<WyBasicInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectList(Wrapper<WyBasicInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectMaps(Wrapper<WyBasicInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectObjs(Wrapper<WyBasicInfo> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<WyBasicInfo>> ResponseVO selectPage(P page, Wrapper<WyBasicInfo> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<Map<String, Object>>> ResponseVO selectMapsPage(P page, Wrapper<WyBasicInfo> queryWrapper) {
        return null;
    }


}
