package com.zbkc.service.impl;



import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.enums.ExportEnum;
import com.zbkc.common.utils.ExcelUtils;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.mapper.*;
import com.zbkc.model.dto.*;
import com.zbkc.model.dto.export.ExportBillManagementDTO;
import com.zbkc.model.po.SysFilePath1;
import com.zbkc.model.po.YjSmsRecord;
import com.zbkc.model.po.YyBillManagement;
import com.zbkc.model.po.YyContract;
import com.zbkc.model.vo.*;
import com.zbkc.service.YyBillManagementService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

//import com.zbkc.service.AliyunSmsSenderService;

/**
 * <p>
 * 运营管理——账单管理 服务实现类
 * </p>
 *
 * @author caobiyang
 * @since 2021-09-14
 */
@Service
public class YyBillManagementServiceImpl extends ServiceImpl<YyBillManagementMapper, YyBillManagement> implements YyBillManagementService {
    @Autowired
    private YyBillManagementMapper yyBillManagementMapper;
    @Autowired
    private YyContractMapper yyContractMapper;
    @Autowired
    private YyUseRegFilePathMapper yyUseRegFilePathMapper;
    @Autowired
    private YjSmsRecordMapper yjSmsRecordMapper;
    @Autowired
    private SysFilePathMapper sysFilePathMapper;


//    @Autowired
//    private AliyunSmsSenderService aliyunSmsSenderService;



    @Override
    public ResponseVO saveBill(YyBillManagementDTO dto) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS,yyBillManagementMapper.saveBill(dto));
    }

    @Override
    public ResponseVO seeRow(String id,Long wyId) {
        YyBillManagementVo vo = new YyBillManagementVo();
        AllWordVo seeRow = yyBillManagementMapper.seeRow(id,wyId);
        vo.setAllWordVo(seeRow);
        List<FeeRecord> feeRecords = yyBillManagementMapper.queryAllFeeRecord(seeRow.getContractId()==null?0:seeRow.getContractId(),seeRow.getWyId()==null?0:seeRow.getWyId());
        for (FeeRecord feeRecord : feeRecords) {
           feeRecord.setProject("租金");
        }
        vo.setFeeRecord(feeRecords);
//        List<YyUseRegFilePath1> filePath = yyUseRegFilePathMapper.getByYyId(seeRow.getContractFilePathId());
        List<SysFilePath1> filePath = sysFilePathMapper.getBydbFileId(seeRow.getContractFilePathId());
        vo.setFilePaths(filePath);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO collecMoneySave(List<CollecMoneyDTO> dto) {

        for (CollecMoneyDTO collecMoneyDTO : dto) {
            yyBillManagementMapper.wordSave(collecMoneyDTO);
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS,null);

    }

    @Override
    public ResponseVO collecInit(CollecDTO dto) {
        if (dto.getBillStatus()==2) {
            return new ResponseVO(ErrorCodeEnum.SUCCESS,yyBillManagementMapper.collecInit(dto));
        }else{
            CollecVo vo = yyBillManagementMapper.collecInit(dto);
            //收款日期
            vo.setReceivedDate(TimeUtils.getDateString(new Date()));
            List<PendingRecept> beReceived = yyBillManagementMapper.beReceived(vo.getContractId()==null?0:vo.getContractId(),vo.getWyId()==null?0:vo.getWyId());
            vo.setPendingRecept(beReceived);
            return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
        }
    }

    @Override
    public ResponseVO homeList(BillManagementDTO dto) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        Calendar aft = Calendar.getInstance();
        try {
            aft.setTime(sdf.parse(dto.getBillTime()==null||dto.getBillTime()==""?"9999-99":dto.getBillTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
       Integer year = aft.get(Calendar.YEAR);
       Integer month=aft.get(Calendar.MONTH)+ 1;
        dto.setBYear(year.toString());
        dto.setBMonth(month.toString());

        if(dto.getP()<=0){
            return new ResponseVO(ErrorCodeEnum.PAGE_NUMBER_ERROR,ErrorCodeEnum.PAGE_NUMBER_ERROR.getErrMsg());
        }
        dto.setP( (dto.getP()-1)*dto.getSize() );
        List<YyBillManagementHomeList> list = yyBillManagementMapper.homeList(dto);
        PageVO<YyBillManagementHomeList> pageVO = new PageVO<>();
        pageVO.setSize(dto.getSize());
        pageVO.setP(dto.getP());
        pageVO.setList(list);
        pageVO.setTotal(yyBillManagementMapper.homeListTotal(dto).size());
        return new ResponseVO(ErrorCodeEnum.SUCCESS,pageVO);
    }

    @Override
    public ResponseVO selectAllCallRecord(SmsReCordDTO dto) {
        List<CallRecordVO> callRecordList = yyBillManagementMapper.selectAllRecord(dto);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,callRecordList);
    }


    @Override
    public ResponseVO selectWyNameByType(String wyType) {
        List<String> wyNameList = yyBillManagementMapper.selectWyNameByType(wyType);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,wyNameList);
    }

    @Override
    public ResponseVO exportBillMAnagement(ExportBillManagementDTO dto, HttpServletResponse response) {
        List<YyBillManagementHomeList> list = yyBillManagementMapper.homeListTotal(dto.getInfo());

        for (YyBillManagementHomeList yyBillManagementHomeList : list) {
            //防止数据库返回字段值为null报异常
            yyBillManagementHomeList.setInvoiceStatus(yyBillManagementHomeList.getInvoiceStatus()==null?0:yyBillManagementHomeList.getInvoiceStatus());
            yyBillManagementHomeList.setPaymentStatus(yyBillManagementHomeList.getPaymentStatus()==null?0:yyBillManagementHomeList.getPaymentStatus());
            //将缴费状态转为中文方便导出
            switch (yyBillManagementHomeList.getPaymentStatus()) {
                case 1:
                    yyBillManagementHomeList.setPaymentStatusStr(ExportEnum.NOT_PAYMENT.getName());
                    break;
                case 2:
                    yyBillManagementHomeList.setPaymentStatusStr(ExportEnum.PARTIAL_PAYMENT.getName());
                    break;
                case 3:
                    yyBillManagementHomeList.setPaymentStatusStr(ExportEnum.ALREADY_PAYMENT.getName());
                    break;
                case 4:
                    yyBillManagementHomeList.setPaymentStatusStr(ExportEnum.OWE_PAYMENT.getName());
                    break;
                case 5:
                    yyBillManagementHomeList.setPaymentStatusStr(ExportEnum.ALREADY_MAKE_UP_PAYMENT.getName());
                    break;
                case 6:
                    yyBillManagementHomeList.setPaymentStatusStr(ExportEnum.PREPARE_PAYMENT.getName());
                    break;
                default:
                    break;
            }
            //将发票状态转为中文方便导出
            switch (yyBillManagementHomeList.getInvoiceStatus()) {
                case 2:
                    yyBillManagementHomeList.setInvoiceStatusStr(ExportEnum.ALREADY_INVOICED.getName());
                    break;
                case 1:
                default:
                    yyBillManagementHomeList.setInvoiceStatusStr(ExportEnum.NOT_INVOICED.getName());
                    break;
            }

        }
        Map<String, String> keyMap = dto.getKeyMap();

        //需要显示的字段名
        //字段名对应的中文名
        String[] headers = null;
        String[] needShowKeyArr = null;

        if (StringUtils.isEmpty(dto.getNeedShowKeys())) {
            //不传getNeedShowKeys 显示所有字段
            Set<String> keys = keyMap.keySet();
            needShowKeyArr = keys.toArray(new String[keys.size()]);
            headers = new String[keys.size()];
        } else {
            needShowKeyArr = dto.getNeedShowKeys().split(",");
            //字段名对应的中文名
            headers = new String[needShowKeyArr.length];
        }

        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        try {
            ExcelUtils.getExcel(response, headers, needShowKeyArr, list, "billList.xls");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public ResponseVO getBillCount(String billTime) {

        Integer year = TimeUtils.getYear(billTime, "yyyy-MM");
        HashMap<Integer, ZdPayInfoVO> zdPayInfo = new HashMap<>();

        for (int i = 1; i < 13; i++) {
            List<YyBillManagement> list = yyBillManagementMapper.getBillCount(year, i);
            //未缴账单数
            int wjBillCount = list.stream().filter(yyBillManagement -> yyBillManagement.getPaymentStatus() != null)
                    .filter(yyBillManagement -> yyBillManagement.getPaymentStatus() == 1)
                    .collect(Collectors.toList()).size();
            //已缴账单数
            int yjBillCount = list.stream().filter(yyBillManagement -> yyBillManagement.getPaymentStatus() != null)
                    .filter(yyBillManagement -> yyBillManagement.getPaymentStatus() == 3 || yyBillManagement.getPaymentStatus() == 5 || yyBillManagement.getPaymentStatus() == 6)
                    .collect(Collectors.toList()).size();
            //欠缴账单数
            int qjBillCount = list.stream().filter(yyBillManagement -> yyBillManagement.getPaymentStatus() != null)
                    .filter(yyBillManagement -> yyBillManagement.getPaymentStatus() == 2 || yyBillManagement.getPaymentStatus() == 4)
                    .collect(Collectors.toList()).size();
            System.out.println(qjBillCount);
            ZdPayInfoVO zdPayInfoVO = new ZdPayInfoVO();
            zdPayInfoVO.setWjBillCount(wjBillCount);
            zdPayInfoVO.setYjBillCount(yjBillCount);
            zdPayInfoVO.setQjBillCount(qjBillCount);
            zdPayInfo.put(i, zdPayInfoVO);
        }

        return new ResponseVO(ErrorCodeEnum.SUCCESS, zdPayInfo);
    }

    @Override
    public ResponseVO getWordByName(String name) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS,yyBillManagementMapper.getWordByName(name));
    }

    @Override
    public ResponseVO manyCollection(Long contractId,Integer billStatus) {

        List<ManyReceivedVO> list = yyBillManagementMapper.waitReceived(contractId);
        BillInfoVo vo = new BillInfoVo();

        YyContract yyContract = yyContractMapper.seeRow(String.valueOf(contractId));
        vo.setContractId(yyContract.getId());
        vo.setLeaser(yyContract.getLeaser()==null?"":yyContract.getLeaser());
        vo.setLeaserPhone(yyContract.getLeaserPhone()==null?"":yyContract.getLeaserPhone());
        vo.setReceivedDate(TimeUtils.getDateString(new Date()));
        if (billStatus==2){
          return  new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
        }
        vo.setReceivedList(list);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO saveManyCollection(List<SaveCollectionDTO> dto) {
        for (SaveCollectionDTO saveCollectionDTO : dto) {
            yyBillManagementMapper.saveManyCollection(saveCollectionDTO);
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS,null);
    }

    @Override
    public ResponseVO callSms(SmsDTO dto) {
        YjSmsRecord yjSmsRecord = new YjSmsRecord();

        yjSmsRecord.setId(IdWorker.getId());
        yjSmsRecord.setWyBasicInfoId(dto.getWyId());
        yjSmsRecord.setContractId(dto.getContractId());
        yjSmsRecord.setMsgType(dto.getMsgType());
        yjSmsRecord.setSendStatus(dto.getSendStatus());
        yjSmsRecord.setSendTime(new Timestamp(System.currentTimeMillis()));
        yjSmsRecord.setUserId(dto.getUserId());

        Integer i = yjSmsRecordMapper.insertSmsRecord(yjSmsRecord);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);

    }

    @Override
    public ResponseVO batchCallSms(List<String> ids) {
//        BatchSmsVO batchSmsVO = new BatchSmsVO();
//        batchSmsVO.setWySize(ids.size());
//        batchSmsVO.setName(getAllSmsName());
//        //短信催缴模板
//        batchSmsVO.setType(2);
//        Map<String, String> smsTo = null;
//        for (String id : ids) {
//            AllWordVo seeRow = yyBillManagementMapper.seeRow(id,);
//            StringBuffer stringBuffer = new StringBuffer();
//            stringBuffer.append("尊敬的")
//                    .append(seeRow.getLeaser())
//                    .append("，您好！根据你方与我中心签订的【")
//                    .append(seeRow.getWyName())
//                    .append("】《深圳市房屋租赁合同》及补充协议，将于【")
//                    .append(seeRow.getEndRentDate())
//                    .append("到期，请及时前往【")
//                    .append(seeRow.getWyName())
//                    .append("】办理续签，特此提醒！")
//                    .append(seeRow.getWyName());
//            String word = stringBuffer.toString();
//            smsTo.put(id,word);
//        }
        return null;
    }

    @Override
    public List<String> getAllSmsName() {
        return yyBillManagementMapper.getAllSmsName();
    }

    @Override
    public String getWordByType() {
        return null;
    }


    public static void main(String[] args) {
//        Calendar cal = Calendar.getInstance();
//        int year = cal.get(Calendar.YEAR);
//        int month = cal.get(Calendar.MONTH )+1;
//        String str="s1";
//        StringBuffer stringBuffer = new StringBuffer();
//        stringBuffer.append("尊敬的【").append(str).append("】，您好！");
//        System.out.println(stringBuffer);
//            System.out.println(TimeUtils.LongToDate(System.currentTimeMillis()));
        BigDecimal bigDecimal1 = new BigDecimal("5");
        BigDecimal bigDecimal2 = new BigDecimal("2");
        System.out.println(bigDecimal2.subtract(bigDecimal1).abs());
    }
}
