package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.enums.ExportEnum;
import com.zbkc.common.utils.ExcelUtils;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.common.utils.ToolUtils;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.*;
import com.zbkc.model.dto.WyProRightHomeDTO;
import com.zbkc.model.dto.export.ExporProRightRegDTO;
import com.zbkc.model.dto.homeList.WyProRightHomeList;
import com.zbkc.model.dto.homeList.YyUseRegHomeList;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.dto.WyProRightRegDTO;
import com.zbkc.model.vo.WyproRightVo;
import com.zbkc.service.WyProRightRegService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 产权登记表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-06
 */
@Service
@Slf4j
public class WyProRightRegServiceImpl implements WyProRightRegService {
    @Autowired
    private WyProRightRegMapper wyProRightRegMapper;
    @Autowired
    private LabelManageMapper labelManageMapper;
    @Autowired
    private SysFilePathMapper sysFilePathMapper;
    @Autowired
    private WyBasicInfoMapper wyBasicInfoMapper;
    @Autowired
    private SysAttachmentMapper sysAttachmentMapper;
    @Resource
    private ToolUtils toolUtils;


    private final static ExecutorService executorService = new ThreadPoolExecutor(4, 20,
            60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.AbortPolicy());


    @Override
    public ResponseVO insert(WyProRightReg entity) {
        return null;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO addProRight(WyProRightRegDTO dto1) {

        WyProRightRegDTO dto = new WyProRightRegDTO();
        dto = dto1;

        if (dto.getUserId() == 0) {
            throw new BusinessException(ErrorCodeEnum.USERID_NOT_FOUND);
        }

        List<LabelManage> insertLabelManageList = new ArrayList<>();
        List<SysFilePath> insertSysFilePathList = new ArrayList<>();
        List<SysAttachment> insertSysAttachmentList = new ArrayList<>();

        WyProRightReg info = new WyProRightReg();
        info.setUnitCode(dto.getUnitCode());
        info.setWyBasicInfoId(dto.getWyBasicInfoId());
        info.setCertificateNo(dto.getCertificateNo());
        info.setEstateUnitNo(dto.getEstateUnitNo());
        info.setCompletionDate(toolUtils.strFormatTimetamp(dto.getCompletionDate()));
        info.setObligee(dto.getObligee());

        long startTime = System.currentTimeMillis();

        Long rightTypeId = IdWorker.getId();
        info.setRightTypeId(rightTypeId);
        if (dto.getRightType() != null) {
            LabelManage manage = new LabelManage();
            manage.setId(IdWorker.getId());
            manage.setLabelId(rightTypeId);
            manage.setSysDataTypeId(dto.getRightType());
            insertLabelManageList.add(manage);
        }

        info.setRegisterPrice(dto.getRegisterPrice());
        info.setRegTime(toolUtils.strFormatTimetamp(dto.getRegTime()));


        Long landPurposeId = IdWorker.getId();
        info.setLandPurposeId(landPurposeId);
        if (dto.getLandPurpose() != null) {
            for (int i = 0; i < dto.getLandPurpose().length; i++) {
                LabelManage manage = new LabelManage();
                manage.setId(IdWorker.getId());
                manage.setLabelId(landPurposeId);
                manage.setSysDataTypeId(dto.getLandPurpose()[i]);
                insertLabelManageList.add(manage);
            }
        }

        info.setLandLocation(dto.getLandLocation());
        info.setAreaCode(dto.getAreaCode());
        info.setDetailAddr(dto.getDetailAddr());
        info.setBuildingArea(dto.getBuildingArea());
        info.setInsideArea(dto.getInsideArea());
        info.setRegisterPurpose(dto.getRegisterPurpose());

        Long commonSituationId = IdWorker.getId();
        info.setCommonSituationId(commonSituationId);
        if (dto.getCommonSituation() != null) {
            LabelManage manage = new LabelManage();
            manage.setId(IdWorker.getId());
            manage.setLabelId(commonSituationId);
            manage.setSysDataTypeId(dto.getCommonSituation());
            insertLabelManageList.add(manage);
        }


        info.setUseLimitStart(toolUtils.strFormatTimetamp(dto.getUseLimitStart()));
        info.setUseLimitEnd(toolUtils.strFormatTimetamp(dto.getUseLimitEnd()));
        info.setRemark(dto.getRemark());


        Long proRightFileId = IdWorker.getId();
        info.setProRightFileId(proRightFileId);
        if (dto.getProRightFile() != null) {
            for (SysFilePath proRightFile : dto.getProRightFile()) {
                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(proRightFileId);
                sysFilePath.setFileName(proRightFile.getFileName());
                sysFilePath.setPath(proRightFile.getPath());
                sysFilePath.setFileSize(proRightFile.getFileSize());
                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long custRightFileId = IdWorker.getId();
        info.setCustRightFileId(custRightFileId);
        if (dto.getCustRightFile() != null) {
            Set<String> keySet = dto.getCustRightFile().keySet();
            List<String> strings = keySet.stream().collect(Collectors.toList());
            for (String string : strings) {
                List<SysFilePath> dtos = dto.getCustRightFile().get(string);
                for (SysFilePath sysfile : dtos) {
                    SysFilePath sysFilePath = new SysFilePath();
                    Long FileId = IdWorker.getId();
                    sysFilePath.setId(FileId);
                    sysFilePath.setFileName(sysfile.getFileName());
                    sysFilePath.setFileSize(sysfile.getFileSize());
                    sysFilePath.setPath(sysfile.getPath());
                    sysFilePath.setDbFileId(custRightFileId);
                    insertSysFilePathList.add(sysFilePath);

                    SysAttachment attachment = new SysAttachment();
                    attachment.setId(IdWorker.getId());
                    attachment.setSysFilePathId(FileId);
                    attachment.setName(string);
                    insertSysAttachmentList.add(attachment);
                }
            }
        }
        info.setCreateTime(new Timestamp(System.currentTimeMillis()));
        info.setUserId(dto.getUserId());
        info.setCreateTime(new Timestamp(System.currentTimeMillis()));
        info.setUpdateTime(new Timestamp(System.currentTimeMillis()));

        this.wyProRightRegMapper.addProRight(info);
        this.labelManageMapper.insertBatch(insertLabelManageList);
        if(!insertSysFilePathList.isEmpty()){
            this.sysFilePathMapper.insertBatch(insertSysFilePathList);
        }
        if(!insertSysAttachmentList.isEmpty()){
            this.sysAttachmentMapper.insertBatch(insertSysAttachmentList);
        }

        long endTime = System.currentTimeMillis();
        log.info("程序运行时间： " + (endTime - startTime) + "ms");
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null);
    }

    @Override
    public ResponseVO selectAllName(Long userId) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS, wyBasicInfoMapper.queryAllName(userId));
    }

    @Override
    public ResponseVO getList(WyProRightHomeDTO dto) {
        if (dto.getP() <= 0) {
            return new ResponseVO(ErrorCodeEnum.SUCCESS, null);
        }
        dto.setP((dto.getP() - 1) * dto.getSize());
        List<WyProRightHomeList> list = wyProRightRegMapper.queryWyProRightHomeList(dto);

        PageVO<WyProRightHomeList> vo = new PageVO<>();
        vo.setList(list);

        vo.setP(dto.getP() / dto.getSize() + 1);
        vo.setSize(dto.getSize());
        vo.setTotal(wyProRightRegMapper.queryWyProRightHomeListTotal(dto).size());
        return new ResponseVO(ErrorCodeEnum.SUCCESS, vo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO updProright(WyProRightRegDTO dto1) {

        WyProRightRegDTO dto = new WyProRightRegDTO();
        dto = dto1;

        long startTime = System.currentTimeMillis();

        List<LabelManage> insertLabelManageList = new ArrayList<>();
        List<SysFilePath> insertSysFilePathList = new ArrayList<>();
        List<SysAttachment> insertSysAttachmentList = new ArrayList<>();

        WyProRightReg info = new WyProRightReg();

        Long rightTypeId = IdWorker.getId();
        info.setRightTypeId(rightTypeId);
        if (dto.getRightType() != null) {

            LabelManage rightType = new LabelManage();
            rightType.setId(IdWorker.getId());
            rightType.setLabelId(rightTypeId);
            rightType.setSysDataTypeId(dto.getRightType());

            insertLabelManageList.add(rightType);
        }


        Long commonSituationId = IdWorker.getId();
        info.setCommonSituationId(commonSituationId);
        if (dto.getCommonSituation() != null) {
            Long commonSituationData = dto.getCommonSituation();
            LabelManage commonSituation = new LabelManage();

            commonSituation.setId(IdWorker.getId());
            commonSituation.setLabelId(commonSituationId);
            commonSituation.setSysDataTypeId(commonSituationData);
            insertLabelManageList.add(commonSituation);
        }

        Long landPurposeId = IdWorker.getId();
        info.setLandPurposeId(landPurposeId);
        if (dto.getLandPurpose() != null) {
            for (Long landPurposeData : dto.getLandPurpose()) {
                LabelManage landPurpose = new LabelManage();

                landPurpose.setId(IdWorker.getId());
                landPurpose.setLabelId(landPurposeId);
                landPurpose.setSysDataTypeId(landPurposeData);

                insertLabelManageList.add(landPurpose);
            }
        }

        Long proRightFileId = IdWorker.getId();
        info.setProRightFileId(proRightFileId);
        if (dto.getProRightFile() != null) {
            for (SysFilePath proRightFile : dto.getProRightFile()) {
                SysFilePath sysFilePath = new SysFilePath();

                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(proRightFileId);
                sysFilePath.setFileName(proRightFile.getFileName());
                sysFilePath.setPath(proRightFile.getPath());
                sysFilePath.setFileSize(proRightFile.getFileSize());

                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long custRightFileId = IdWorker.getId();
        info.setCustRightFileId(custRightFileId);
        if (dto.getCustRightFile() != null) {
            Set<String> keySet = dto.getCustRightFile().keySet();
            List<String> strings = keySet.stream().collect(Collectors.toList());
            for (String string : strings) {
                List<SysFilePath> dtos = dto.getCustRightFile().get(string);
                for (SysFilePath sysfile : dtos) {
                    SysFilePath sysFilePath = new SysFilePath();
                    Long FileId = IdWorker.getId();
                    sysFilePath.setId(FileId);
                    sysFilePath.setFileName(sysfile.getFileName());
                    sysFilePath.setFileSize(sysfile.getFileSize());
                    sysFilePath.setPath(sysfile.getPath());
                    sysFilePath.setDbFileId(custRightFileId);

                    insertSysFilePathList.add(sysFilePath);

                    SysAttachment attachment = new SysAttachment();
                    attachment.setId(IdWorker.getId());
                    attachment.setSysFilePathId(FileId);
                    attachment.setName(string);
                    insertSysAttachmentList.add(attachment);
                }
            }
        }

        info.setId(dto.getId());
        info.setUnitCode(dto.getUnitCode());
        info.setWyBasicInfoId(dto.getWyBasicInfoId());
        info.setCertificateNo(dto.getCertificateNo());
        info.setEstateUnitNo(dto.getEstateUnitNo());
        info.setCompletionDate(toolUtils.strFormatTimetamp(dto.getCompletionDate()));
        info.setObligee(dto.getObligee());
        info.setRegisterPrice(dto.getRegisterPrice());
        info.setRegTime(toolUtils.strFormatTimetamp(dto.getRegTime()));
        info.setUseLimitStart(toolUtils.strFormatTimetamp(dto.getUseLimitStart()));
        info.setUseLimitEnd(toolUtils.strFormatTimetamp(dto.getUseLimitEnd()));
        info.setLandLocation(dto.getLandLocation());
        info.setAreaCode(dto.getAreaCode());
        info.setDetailAddr(dto.getDetailAddr());
        info.setBuildingArea(dto.getBuildingArea());
        info.setInsideArea(dto.getInsideArea());
        info.setRegisterPurpose(dto.getRegisterPurpose());
        info.setRemark(dto.getRemark());
        info.setUpdateTime(new Timestamp(System.currentTimeMillis()));

        this.wyProRightRegMapper.updWyProRight(info);
        if(!insertSysAttachmentList.isEmpty()){
            this.sysAttachmentMapper.insertBatch(insertSysAttachmentList);
        }
        if(!insertSysFilePathList.isEmpty()){
            this.sysFilePathMapper.insertBatch(insertSysFilePathList);
        }
        this.labelManageMapper.insertBatch(insertLabelManageList);

        long endTime = System.currentTimeMillis();
        log.info("程序运行时间： " + (endTime - startTime) + "ms");
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null);
    }

    @Override
    public ResponseVO seeRow(String id) {
        WyproRightVo vo = new WyproRightVo();
        WyProRightReg reg = wyProRightRegMapper.selectById(id);

        vo.setName(wyBasicInfoMapper.getNameById(reg.getWyBasicInfoId()));
        vo.setRightType(wyBasicInfoMapper.getSysDataTypeByLabelId(reg.getRightTypeId()));
        vo.setLandPurposeList(wyBasicInfoMapper.getSysDataTypesByLabelId(reg.getLandPurposeId()));
        vo.setCommonSituation(wyBasicInfoMapper.getSysDataTypeByLabelId(reg.getCommonSituationId()));
        vo.setProRightFile(sysFilePathMapper.getBydbFileId(reg.getProRightFileId()));

        HashMap<String, List<SysFilePath1>> map = new HashMap<>();
        for (String name : sysFilePathMapper.getName(reg.getCustRightFileId())) {
            map.put(name, sysFilePathMapper.getCustFile(reg.getCustRightFileId(), name));
        }
        vo.setCustRightFile(map);

        vo.setReg(reg);

        return new ResponseVO(ErrorCodeEnum.SUCCESS, vo);
    }

    @Override
    public ResponseVO delProright(String id) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS, wyProRightRegMapper.delById(id));
    }

    @Override
    public ResponseVO export(ExporProRightRegDTO dto, HttpServletResponse response) {
        List<WyProRightHomeList> list = wyProRightRegMapper.queryWyProRightHomeListTotal(dto.getWyProRightHomeDTO());
        Map<String, String> keyMap = dto.getKeyMap();

        for (WyProRightHomeList wyProRightHomeList : list) {
            if(wyProRightHomeList.getUseLimitStart()==null||wyProRightHomeList.getUseLimitEnd()==null){
                wyProRightHomeList.setTermStr("无");
            }else {
                String startTime = TimeUtils.TimestampToDateStr(wyProRightHomeList.getUseLimitStart(), "yyyy-MM-dd");
                String endTime = TimeUtils.TimestampToDateStr(wyProRightHomeList.getUseLimitEnd(), "yyyy-MM-dd");
                String termStr =startTime+"至"+endTime;
                wyProRightHomeList.setTermStr(termStr);
            }
        }
        //需要显示的字段名
        //字段名对应的中文名
        String[] headers = null;
        String[] needShowKeyArr = null;

        if (StringUtils.isEmpty(dto.getNeedShowKeys())) {
            //不传getNeedShowKeys 显示所有字段
            Set<String> keys = keyMap.keySet();
            needShowKeyArr = keys.toArray(new String[keys.size()]);
            headers = new String[keys.size()];
        } else {
            needShowKeyArr = dto.getNeedShowKeys().split(",");
            //字段名对应的中文名
            headers = new String[needShowKeyArr.length];
        }

        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        try {
            ExcelUtils.getExcel(response, headers, needShowKeyArr, list, "wyProRightList.xls");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }


    @Override
    public ResponseVO status(Long id, Boolean status) {
        if (status) {
            Integer open = wyProRightRegMapper.statusOpen(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, open);
        } else {
            Integer close = wyProRightRegMapper.statusClose(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, close);
        }
    }


    @Override
    public ResponseVO updateById(WyProRightReg entity) {
        return null;
    }

    @Override
    public ResponseVO deleteById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO deleteByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO delete(Wrapper<WyProRightReg> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO deleteBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }


    @Override
    public ResponseVO update(WyProRightReg entity, Wrapper<WyProRightReg> updateWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO selectBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO selectOne(Wrapper<WyProRightReg> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectCount(Wrapper<WyProRightReg> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectList(Wrapper<WyProRightReg> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectMaps(Wrapper<WyProRightReg> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectObjs(Wrapper<WyProRightReg> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<WyProRightReg>> ResponseVO selectPage(P page, Wrapper<WyProRightReg> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<Map<String, Object>>> ResponseVO selectMapsPage(P page, Wrapper<WyProRightReg> queryWrapper) {
        return null;
    }
}
