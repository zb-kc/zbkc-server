package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.mapper.SysMenuMapper;
import com.zbkc.model.dto.MenuDTO;
import com.zbkc.model.po.SysMenu;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.SysMenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;

/**
 * <p>
 * 系统菜单表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-07-30
 */
@Service
public class SysMenuServiceImpl implements SysMenuService {
    @Resource
    private SysMenuMapper sysMenuMapper;

    @Override
    public ResponseVO status(Long id,Boolean status) {
        if(status) {
            Integer open = sysMenuMapper.statusOpen(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS,open);
        }else {
            Integer close = sysMenuMapper.statusClose(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, close);
        }
    }


    @Override
    public ResponseVO updateById(SysMenu entity) {
        long ct = System.currentTimeMillis();
        Timestamp timestamp = TimeUtils.LongToDate(ct);
        entity.setUpdateTime(timestamp);
        int i = sysMenuMapper.updateById(entity);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO del(Long id) {
        Integer i = sysMenuMapper.delByInfo(id);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO insert(SysMenu entity) {
        if(entity.getName()==null||"".equals(entity.getName())){
            return new ResponseVO(ErrorCodeEnum.REGULARNOTACCORD,null);
        }else{
            entity.setStatus(1);
            long ct = System.currentTimeMillis();
            Timestamp timestamp = TimeUtils.LongToDate(ct);
            entity.setCreateTime(timestamp);
            int i = sysMenuMapper.insert(entity);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, i);
        }
    }

    @Override
    public ResponseVO menusList(MenuDTO menuDTO) {

        List<SysMenu> sysMenuList = this.sysMenuMapper.selectAllMenuByPid(0L);
        Map<Long , List<SysMenu>> sysMenusMap = new HashMap<>();

        for (SysMenu sysMenu : sysMenuList) {
            List<SysMenu> sysMenus = sysMenusMap.get(sysMenu.getPid());
            if(null == sysMenus){
                sysMenus = new ArrayList<>();
            }
            sysMenus.add(sysMenu);
            sysMenusMap.put(sysMenu.getPid() , sysMenus);
        }

        List<SysMenu> returnMenuList = new ArrayList<>(sysMenusMap.get(0L));
        propertyList(returnMenuList, sysMenusMap);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,returnMenuList);
    }

    /**
     * 结构化树
     *
     * @param dto dto
     * @param map map
     * @return list
     */
    private List<SysMenu> propertyList(List<SysMenu> dto, Map<Long, List<SysMenu>> map) {

        if (null == dto || dto.isEmpty()) {
            return null;
        }

        for (SysMenu vo : dto) {
            List<SysMenu> list = map.get(vo.getId());
            vo.setSysMenusList(list);
            propertyList(vo.getSysMenusList(), map);
        }
        return dto;
    }

    @Override
    public ResponseVO deleteById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO deleteByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO delete(Wrapper<SysMenu> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO deleteBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }


    @Override
    public ResponseVO update(SysMenu entity, Wrapper<SysMenu> updateWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO selectBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO selectOne(Wrapper<SysMenu> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectCount(Wrapper<SysMenu> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectList(Wrapper<SysMenu> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectMaps(Wrapper<SysMenu> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectObjs(Wrapper<SysMenu> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<SysMenu>> ResponseVO selectPage(P page, Wrapper<SysMenu> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<Map<String, Object>>> ResponseVO selectMapsPage(P page, Wrapper<SysMenu> queryWrapper) {
        return null;
    }


}
