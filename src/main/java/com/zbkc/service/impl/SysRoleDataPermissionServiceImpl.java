package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.SetOptUtils;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.SysRoleDataPermissionMapper;
import com.zbkc.mapper.WyBasicInfoMapper;
import com.zbkc.model.dto.SysRoleDataPermissionDTO;
import com.zbkc.model.po.SysRoleDataPermission;
import com.zbkc.model.po.WyBasicInfo;
import com.zbkc.service.SysRoleDataPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/11/8
 */
@Service
public class SysRoleDataPermissionServiceImpl  extends ServiceImpl<SysRoleDataPermissionMapper, SysRoleDataPermission> implements SysRoleDataPermissionService {

    @Autowired
    private SysRoleDataPermissionMapper sysRoleDataPermissionMapper;

    @Autowired
    private WyBasicInfoMapper wyBasicInfoMapper;

    @Override
    public int insertBatch(List<SysRoleDataPermission> sysRoleDataPermissionList) {
        boolean flag = this.saveBatch(sysRoleDataPermissionList);
        if(flag){
            return sysRoleDataPermissionList.size();
        }
        return 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer saveDataPermission(SysRoleDataPermissionDTO sysRoleDataPermissionDTO) {

        int totalRecord = 0;

        List<Long> wyBasicInfoListTemp = sysRoleDataPermissionDTO.getWyBasicInfoList();
        //isAll为false，list为null
        if( !sysRoleDataPermissionDTO.getIsAll() && null == wyBasicInfoListTemp){
            throw new BusinessException(ErrorCodeEnum.OPERATE_ERROR3);
        }

        //isAll为true , list不为空
        if( sysRoleDataPermissionDTO.getIsAll() && !wyBasicInfoListTemp.isEmpty()){
            throw new BusinessException(ErrorCodeEnum.OPERATE_ERROR3);
        }

        //先获取需要删除的老数据
        QueryWrapper<SysRoleDataPermission> deleteWrapper = new QueryWrapper<>();
        deleteWrapper.eq("role_id" , sysRoleDataPermissionDTO.getRoleId());

        //list为空 直接删除所有物业权限
        if(wyBasicInfoListTemp.isEmpty() && !sysRoleDataPermissionDTO.getIsAll()){
            return this.sysRoleDataPermissionMapper.delete(deleteWrapper);
        }
        //查询
        deleteWrapper.select("wy_basic_info_id");
        List<SysRoleDataPermission> sysRoleDataPermissionsOldList = this.sysRoleDataPermissionMapper.selectList(deleteWrapper);

        //再获取需要插入的新数据
        List<Long> wyBasicInfoList = new ArrayList<>();
        if(sysRoleDataPermissionDTO.getIsAll()){
            //授权 所有物业信息
            QueryWrapper<WyBasicInfo> queryWrapper = new QueryWrapper<>();
            queryWrapper.select("id");
            queryWrapper.eq("is_del" , 1);
            List<WyBasicInfo> wyIdList = this.wyBasicInfoMapper.selectList(queryWrapper);
            wyBasicInfoList = wyIdList.stream().map(WyBasicInfo::getId).collect(Collectors.toList());
        }else{
            wyBasicInfoList = sysRoleDataPermissionDTO.getWyBasicInfoList();
        }

        /**
         * 删除的物业id集合 与 新增的物业id集合
         * 交集不作处理
         * 然后取删除集合的差集 删除 ；取新增集合的差集 新增
         */
        List<Long> oldWyIdList = new ArrayList<>();
        if(null != sysRoleDataPermissionsOldList && sysRoleDataPermissionsOldList.size() > 0){
            oldWyIdList = sysRoleDataPermissionsOldList.stream().map(SysRoleDataPermission::getWyBasicInfoId).collect(Collectors.toList());
        }

        HashSet<Long> oldWyIdSet = new HashSet<>(oldWyIdList);
        HashSet<Long> newWyIdSet = new HashSet<>(wyBasicInfoList);

        //如果旧集没有数据，直接新增新集并返回
        if(oldWyIdList.isEmpty()){
            return insertRoleData(sysRoleDataPermissionDTO, totalRecord, newWyIdSet);
        }

        //新集与旧集的差集，需要删除
        Set<Long> oldDiff = SetOptUtils.diff(newWyIdSet, oldWyIdSet);

        //旧集与新集的差集，需要新增
        Set<Long> newDiff = SetOptUtils.diff(oldWyIdSet, newWyIdSet);

        //删除旧物业权限
        if(!oldDiff.isEmpty()){
            totalRecord += this.sysRoleDataPermissionMapper.deleteBatchByWyIds(sysRoleDataPermissionDTO.getRoleId(), oldDiff);
        }

        //新增新物业权限
        if(!newDiff.isEmpty()){
            totalRecord = insertRoleData(sysRoleDataPermissionDTO, totalRecord, newDiff);
        }

        return totalRecord;
    }

    /**
     * 新增数据
     * @param sysRoleDataPermissionDTO dto
     * @param totalRecord 影响总数
     * @param newDiff 需要插入的集合
     * @return 影响总数
     */
    private int insertRoleData(SysRoleDataPermissionDTO sysRoleDataPermissionDTO, int totalRecord, Set<Long> newDiff) {
        List<SysRoleDataPermission> insertList = new ArrayList<>();
        for (Long wyBasicId : newDiff) {
            //每一千条数数据就插入数据库
            if(insertList.size() >= 1000){
                totalRecord += this.sysRoleDataPermissionMapper.insertBatch(insertList);
                insertList.clear();
            }

            SysRoleDataPermission sysRoleDataPermission = new SysRoleDataPermission();
            sysRoleDataPermission.setId(IdWorker.getId());
            sysRoleDataPermission.setRoleId(sysRoleDataPermissionDTO.getRoleId());
            sysRoleDataPermission.setWyBasicInfoId(wyBasicId);
            insertList.add(sysRoleDataPermission);
        }
        //剩余数据 也需要插入数据库
        if(insertList.size() > 0){
            totalRecord += this.sysRoleDataPermissionMapper.insertBatch(insertList);
        }
        return totalRecord;
    }
}
