package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.common.enums.PropertyApplyFileNameEnum;
import com.zbkc.common.enums.YJHouseFileNameEnum;
import com.zbkc.common.utils.CreateFileUtil;
import com.zbkc.common.utils.FileUtils;
import com.zbkc.mapper.SysFilePathMapper;
import com.zbkc.mapper.YwAgencyBusinessMapper;
import com.zbkc.mapper.YwHandoverDetailsMapper;
import com.zbkc.mapper.YyContractMapper;
import com.zbkc.model.po.SysFilePath;
import com.zbkc.model.po.YwAgencyBusinessPO;
import com.zbkc.model.po.YwHandoverDetailsPO;
import com.zbkc.model.po.YyContract;
import com.zbkc.service.YwHandoverDetailsSerivce;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/12
 */
@Service
public class YwHandoverDetailsSerivceImpl extends ServiceImpl<YwHandoverDetailsMapper, YwHandoverDetailsPO> implements YwHandoverDetailsSerivce {

    @Autowired
    private YwHandoverDetailsMapper ywHandoverDetailsMapper;

    @Autowired
    private SysFilePathMapper sysFilePathMapper;

    @Value( "${file.win-name}")
    private String winUrl;

    @Value( "${file.linux-name}" )
    private String linuxUrl;

    @Autowired
    private CreateFileUtil createFileUtil;

    @Autowired
    private YwAgencyBusinessMapper ywAgencyBusinessMapper;

    @Autowired
    private YyContractMapper yyContractMapper;

    @Override
    public List<YwHandoverDetailsPO> selectList(String businessId) {
        QueryWrapper<YwHandoverDetailsPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("yw_agency_business_id" , businessId);
        return this.ywHandoverDetailsMapper.selectList(queryWrapper);
    }

    @Override
    public int dataPass(String handoverDetailsId) {
        YwHandoverDetailsPO ywHandoverDetailsPO = new YwHandoverDetailsPO();
        ywHandoverDetailsPO.setId(Long.parseLong(handoverDetailsId));
        ywHandoverDetailsPO.setStatus("1");
        ywHandoverDetailsPO.setSupplementDataOpinion("");
        return this.ywHandoverDetailsMapper.updateById(ywHandoverDetailsPO);
    }

    @Override
    public void downloadFileOne(HttpServletResponse response,String ywHandlerDetailsId) {

        YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessMapper.selectById(ywHandlerDetailsId);
        if (null != ywAgencyBusinessPO) {
            QueryWrapper<YyContract> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("id" , ywAgencyBusinessPO.getRelationId());
            YyContract yyContract = this.yyContractMapper.selectOne(queryWrapper);
            ywHandlerDetailsId =  yyContract.getContractFilePathId().toString();
        }

        QueryWrapper<SysFilePath> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("db_file_id" , ywHandlerDetailsId);
        SysFilePath sysFilePath = this.sysFilePathMapper.selectOne(queryWrapper);

        String path = sysFilePath.getPath();
        //TODO 文件路径获取问题
        String preFix = createFileUtil.isWindows() ? winUrl : linuxUrl;
        String urlString = preFix + "/" + path;
        InputStream is = null;
        try {
            if(!urlString.startsWith("http")) {
                File file = new File(urlString);
                is = new BufferedInputStream(new FileInputStream(file));
            }else{
                // 构造URL
                java.net.URL url = new java.net.URL(urlString);
                // 打开连接
                URLConnection con = url.openConnection();
                // 输入流
                is = con.getInputStream();
            }

            FileUtils.buildFile(sysFilePath.getFileName() , is , response);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != is){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void generateHandoverDetailsList(YwAgencyBusinessPO ywAgencyBusinessPO) {

        List<YwHandoverDetailsPO> list = null;
        Integer agencyType = ywAgencyBusinessPO.getAgencyType();
        switch (agencyType){
            case 1:
                //移交用房
                YJHouseFileNameEnum[] values = YJHouseFileNameEnum.values();

                list = new ArrayList<>(values.length);

                for (YJHouseFileNameEnum value : values) {
                    String fileName = value.getFileName();
                    if(StringUtils.isEmpty(fileName)){
                        continue;
                    }
                    YwHandoverDetailsPO ywHandoverDetailsPO = new YwHandoverDetailsPO();
                    ywHandoverDetailsPO.setId(IdWorker.getId());
                    ywHandoverDetailsPO.setTableId(ywHandoverDetailsPO.getId().toString());
                    ywHandoverDetailsPO.setName(fileName);
                    ywHandoverDetailsPO.setYwAgencyBusinessId(ywAgencyBusinessPO.getId());
                    list.add(ywHandoverDetailsPO);
                }
                break;
            case 2:
                PropertyApplyFileNameEnum[] propertyApplyFileNameEnums = PropertyApplyFileNameEnum.values();

                list = new ArrayList<>(propertyApplyFileNameEnums.length);

                for (PropertyApplyFileNameEnum propertyApplyFileNameEnum : propertyApplyFileNameEnums) {
                    String fileName = propertyApplyFileNameEnum.getFileName();

                    YwHandoverDetailsPO ywHandoverDetailsPO = new YwHandoverDetailsPO();
                    ywHandoverDetailsPO.setId(IdWorker.getId());
                    ywHandoverDetailsPO.setTableId(ywHandoverDetailsPO.getId().toString());
                    ywHandoverDetailsPO.setName(fileName);
                    ywHandoverDetailsPO.setYwAgencyBusinessId(ywAgencyBusinessPO.getId());
                    list.add(ywHandoverDetailsPO);
                }
                break;
            case 3:
                break;
            default:
                break;
        }

        this.ywHandoverDetailsMapper.insertBatch(list);

    }

}
