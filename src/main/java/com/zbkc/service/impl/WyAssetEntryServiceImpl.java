package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.ExcelUtils;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.common.utils.ToolUtils;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.*;
import com.zbkc.model.dto.WyAssetEntryDTO;
import com.zbkc.model.dto.WyAssetEntryHomeDTO;
import com.zbkc.model.dto.export.ExportWyAssetEntryDTO;
import com.zbkc.model.dto.homeList.WyAssetEntryHomeList;
import com.zbkc.model.dto.homeList.WyProRightHomeList;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.vo.WyAssetEntryVo;
import com.zbkc.service.WyAssetEntryService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 资产入账信息表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-03
 */
@Service
@Slf4j
public class WyAssetEntryServiceImpl extends ServiceImpl<WyAssetEntryMapper, WyAssetEntry> implements WyAssetEntryService {

    @Autowired
    private WyBasicInfoMapper wyBasicInfoMapper;
    @Autowired
    private ToolUtils toolUtils;
    @Autowired
    private LabelManageMapper labelManageMapper;
    @Autowired
    private SysImgMapper sysImgMapper;
    @Autowired
    private  SysFilePathMapper sysFilePathMapper;
    @Autowired
    private WyAssetEntryMapper wyAssetEntryMapper;
    @Autowired
    private SysAttachmentMapper sysAttachmentMapper;




    private final static ExecutorService executorService = new ThreadPoolExecutor(4, 20,
            60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.AbortPolicy());

    @Override
    public ResponseVO selectAllName(Long userId) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS, wyBasicInfoMapper.queryAllName(userId));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO addEntry(WyAssetEntryDTO dto1) throws Exception {
        WyAssetEntryDTO dto = new WyAssetEntryDTO();
        dto = dto1;

        if (dto.getUserId() == 0) {
            throw new BusinessException(ErrorCodeEnum.USERID_NOT_FOUND);
        }

        List<LabelManage> insertLabelManageList = new ArrayList<>();
        List<SysFilePath> insertSysFilePathList = new ArrayList<>();
        List<SysAttachment> insertSysAttachmentList = new ArrayList<>();

        WyAssetEntry info = new WyAssetEntry();
        info.setWyBasicInfoId(dto.getWyBasicInfoId());

        List<Future<?>> list = new ArrayList<>();
        long startTime = System.currentTimeMillis();

        Long entryTypeId = IdWorker.getId();
        info.setEntryTypeId(entryTypeId);
        if (dto.getEntryType() != null) {
            LabelManage entryType = new LabelManage();
            entryType.setId(IdWorker.getId());
            entryType.setLabelId(entryTypeId);
            entryType.setSysDataTypeId(dto.getEntryType());
            insertLabelManageList.add(entryType);
        }


        Long propertyRegistrationFileId = IdWorker.getId();
        info.setPropertyRegistrationFileId(propertyRegistrationFileId);
        if (dto.getPropertyRegistrationFile() != null) {
            for (SysFilePath propertyRegistrationFile : dto.getPropertyRegistrationFile()) {

                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(propertyRegistrationFileId);
                sysFilePath.setPath(propertyRegistrationFile.getPath());
                sysFilePath.setFileName(propertyRegistrationFile.getFileName());
                sysFilePath.setFileSize(propertyRegistrationFile.getFileSize());
                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long projectApprovalFileId = IdWorker.getId();
        info.setProjectApprovalFileId(projectApprovalFileId);
        if (dto.getProjectApprovalFile() != null) {
            for (SysFilePath projectApprovalFile : dto.getProjectApprovalFile()) {
                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(projectApprovalFileId);
                sysFilePath.setPath(projectApprovalFile.getPath());
                sysFilePath.setFileName(projectApprovalFile.getFileName());
                sysFilePath.setFileSize(projectApprovalFile.getFileSize());

                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long financialApprovalFileId = IdWorker.getId();
        info.setFinancialApprovalFileId(financialApprovalFileId);
        if (dto.getFinancialApprovalFile() != null) {
            for (SysFilePath financialApprovalFile : dto.getFinancialApprovalFile()) {
                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(financialApprovalFileId);
                sysFilePath.setPath(financialApprovalFile.getPath());
                sysFilePath.setFileName(financialApprovalFile.getFileName());
                sysFilePath.setFileSize(financialApprovalFile.getFileSize());
                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long auditReportFileId = IdWorker.getId();
        info.setAuditReportFileId(auditReportFileId);
        if (dto.getAuditReportFile() != null) {
            for (SysFilePath auditReportFile : dto.getAuditReportFile()) {
                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(auditReportFileId);
                sysFilePath.setPath(auditReportFile.getPath());
                sysFilePath.setFileName(auditReportFile.getFileName());
                sysFilePath.setFileSize(auditReportFile.getFileSize());
                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long otherMaterialsFileId = IdWorker.getId();
        info.setOtherMaterialsFileId(otherMaterialsFileId);
        if (dto.getOtherMaterialsFile() != null) {
            for (SysFilePath otherMaterialsIFile : dto.getOtherMaterialsFile()) {

                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(otherMaterialsFileId);
                sysFilePath.setPath(otherMaterialsIFile.getPath());
                sysFilePath.setFileName(otherMaterialsIFile.getFileName());
                sysFilePath.setFileSize(otherMaterialsIFile.getFileSize());
                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long custRightFileId=IdWorker.getId();
        info.setCustRightFileId(custRightFileId);
        if (dto.getCustRightFile()!=null) {
            Set<String> keySet = dto.getCustRightFile().keySet();
            List<String> strings = keySet.stream().collect(Collectors.toList());
            for (String string : strings) {
                List<SysFilePath> files = dto.getCustRightFile().get(string);
                for (SysFilePath sysfile : files) {
                    SysFilePath sysFilePath = new SysFilePath();

                    Long FileId = IdWorker.getId();
                    sysFilePath.setId(FileId);
                    sysFilePath.setFileName(sysfile.getFileName());
                    sysFilePath.setFileSize(sysfile.getFileSize());
                    sysFilePath.setPath(sysfile.getPath());
                    sysFilePath.setDbFileId(custRightFileId);

                    insertSysFilePathList.add(sysFilePath);

                    SysAttachment attachment = new SysAttachment();
                    attachment.setId(IdWorker.getId());
                    attachment.setSysFilePathId(FileId);
                    attachment.setName(string);
                    insertSysAttachmentList.add(attachment);
                }
            }
        }

        info.setEntryMoney(dto.getEntryMoney());
        info.setEntryTime(toolUtils.strFormatDate(dto.getEntryTime()));
        info.setCreateTime(new Timestamp(System.currentTimeMillis()));
        info.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        info.setUserId(dto.getUserId());

        this.wyAssetEntryMapper.addEntry(info);
        this.labelManageMapper.insertBatch(insertLabelManageList);
        if(!insertSysAttachmentList.isEmpty()){
            this.sysAttachmentMapper.insertBatch(insertSysAttachmentList);
        }
        if(!insertSysFilePathList.isEmpty()){
            this.sysFilePathMapper.insertBatch(insertSysFilePathList);
        }

        long endTime = System.currentTimeMillis();
        log.info("程序运行时间： " + (endTime - startTime) + "ms");
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null);
    }

    @Override
    public ResponseVO getList(WyAssetEntryHomeDTO dto) {
        if(dto.getP()<=0){
            return new ResponseVO(ErrorCodeEnum.SUCCESS,null);
        }
        dto.setP( (dto.getP()-1)*dto.getSize() );
        List<WyAssetEntryHomeList> list = wyAssetEntryMapper.queryWyAssetEntryHomeList(dto);
        PageVO<WyAssetEntryHomeList> vo = new PageVO<>();
        vo.setList(list);
        vo.setP(dto.getP()/dto.getSize()+1);
        vo.setSize(dto.getSize());
        vo.setTotal(wyAssetEntryMapper.queryWyAssetEntryHomeListTotal(dto).size());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO seeRow(String id) {
        WyAssetEntryVo vo = new WyAssetEntryVo();
        WyAssetEntry entry = wyAssetEntryMapper.selectByID(id);

        vo.setEntryType(wyBasicInfoMapper.getSysDataTypeByLabelId(entry.getEntryTypeId()));
        vo.setPropertyRegistrationFile(sysFilePathMapper.getBydbFileId(entry.getPropertyRegistrationFileId()));
        vo.setFinancialApprovalFile(sysFilePathMapper.getBydbFileId(entry.getFinancialApprovalFileId()));
        vo.setProjectApprovalFile(sysFilePathMapper.getBydbFileId(entry.getProjectApprovalFileId()));
        vo.setAuditReportFile(sysFilePathMapper.getBydbFileId(entry.getAuditReportFileId()));
        vo.setOtherMaterialsFile(sysFilePathMapper.getBydbFileId(entry.getOtherMaterialsFileId()));

        HashMap<String, List<SysFilePath1>> map = new HashMap<>();
        for (String name:sysFilePathMapper.getName(entry.getCustRightFileId())) {
            map.put(name,sysFilePathMapper.getCustFile(entry.getCustRightFileId(), name));
        }
        vo.setCustRightFile(map);

        vo.setWyAssetEntry(entry);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseVO updEntry(WyAssetEntryDTO dto1) throws Exception{

        WyAssetEntryDTO dto = new WyAssetEntryDTO();
        dto=dto1;

        long startTime = System.currentTimeMillis();

        WyAssetEntry info = new WyAssetEntry();

        List<LabelManage> insertLabelManageList = new ArrayList<>();
        List<SysFilePath> insertSysFilePathList = new ArrayList<>();
        List<SysAttachment> insertSysAttachmentList = new ArrayList<>();

        Long entryTypeId = IdWorker.getId();
        info.setEntryTypeId(entryTypeId);
        if (dto.getEntryType() != null) {

            LabelManage entryType = new LabelManage();
            entryType.setId(IdWorker.getId());
            entryType.setLabelId(entryTypeId);
            entryType.setSysDataTypeId(dto.getEntryType());
            insertLabelManageList.add(entryType);
        }

        Long propertyRegistrationFileId = IdWorker.getId();
        info.setPropertyRegistrationFileId(propertyRegistrationFileId);
        if (dto.getPropertyRegistrationFile() != null) {
            for (SysFilePath propertyRegistrationFile : dto.getPropertyRegistrationFile()) {

                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());

                sysFilePath.setDbFileId(propertyRegistrationFileId);
                sysFilePath.setPath(propertyRegistrationFile.getPath());
                sysFilePath.setFileName(propertyRegistrationFile.getFileName());
                sysFilePath.setFileSize(propertyRegistrationFile.getFileSize());
                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long projectApprovalFileId = IdWorker.getId();
        info.setProjectApprovalFileId(projectApprovalFileId);
        if (dto.getProjectApprovalFile() != null) {
            for (SysFilePath projectApprovalFile : dto.getProjectApprovalFile()) {
                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(projectApprovalFileId);
                sysFilePath.setPath(projectApprovalFile.getPath());
                sysFilePath.setFileName(projectApprovalFile.getFileName());
                sysFilePath.setFileSize(projectApprovalFile.getFileSize());

                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long financialApprovalFileId = IdWorker.getId();
        info.setFinancialApprovalFileId(financialApprovalFileId);
        if (dto.getFinancialApprovalFile() != null) {
            for (SysFilePath financialApprovalFile : dto.getFinancialApprovalFile()) {
                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(financialApprovalFileId);
                sysFilePath.setPath(financialApprovalFile.getPath());
                sysFilePath.setFileName(financialApprovalFile.getFileName());
                sysFilePath.setFileSize(financialApprovalFile.getFileSize());

                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long auditReportFileId = IdWorker.getId();
        info.setAuditReportFileId(auditReportFileId);
        if (dto.getAuditReportFile() != null) {
            for (SysFilePath auditReportFile : dto.getAuditReportFile()) {
                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(auditReportFileId);
                sysFilePath.setPath(auditReportFile.getPath());
                sysFilePath.setFileName(auditReportFile.getFileName());
                sysFilePath.setFileSize(auditReportFile.getFileSize());

                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long otherMaterialsFileId = IdWorker.getId();
        info.setOtherMaterialsFileId(otherMaterialsFileId);
        if (dto.getOtherMaterialsFile() != null) {
            for (SysFilePath otherMaterialsIFile : dto.getOtherMaterialsFile()) {

                SysFilePath sysFilePath = new SysFilePath();
                sysFilePath.setId(IdWorker.getId());
                sysFilePath.setDbFileId(otherMaterialsFileId);
                sysFilePath.setPath(otherMaterialsIFile.getPath());
                sysFilePath.setFileName(otherMaterialsIFile.getFileName());
                sysFilePath.setFileSize(otherMaterialsIFile.getFileSize());

                insertSysFilePathList.add(sysFilePath);
            }
        }

        Long custRightFileId=IdWorker.getId();
        info.setCustRightFileId(custRightFileId);
        if (dto.getCustRightFile()!=null) {
            Set<String> keySet = dto.getCustRightFile().keySet();
            List<String> strings = keySet.stream().collect(Collectors.toList());
            for (String string : strings) {
                List<SysFilePath> files = dto.getCustRightFile().get(string);
                for (SysFilePath sysfile : files) {
                    SysFilePath sysFilePath = new SysFilePath();
                    Long FileId = IdWorker.getId();
                    sysFilePath.setId(FileId);
                    sysFilePath.setFileName(sysfile.getFileName());
                    sysFilePath.setFileSize(sysfile.getFileSize());
                    sysFilePath.setPath(sysfile.getPath());
                    sysFilePath.setDbFileId(custRightFileId);

                    insertSysFilePathList.add(sysFilePath);

                    SysAttachment attachment = new SysAttachment();
                    attachment.setId(IdWorker.getId());
                    attachment.setSysFilePathId(FileId);
                    attachment.setName(string);
                    insertSysAttachmentList.add(attachment);
                }
            }
        }

        info.setId(dto.getId());
        info.setWyBasicInfoId(dto.getWyBasicInfoId());
        info.setEntryMoney(dto.getEntryMoney());
        info.setEntryTime(toolUtils.strFormatDate(dto.getEntryTime()));
        info.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        info.setUserId(dto.getUserId());

        this.wyAssetEntryMapper.updEntry(info);
        this.labelManageMapper.insertBatch(insertLabelManageList);
        if(!insertSysFilePathList.isEmpty()){
            this.sysFilePathMapper.insertBatch(insertSysFilePathList);
        }
        if(!insertSysAttachmentList.isEmpty()){
            this.sysAttachmentMapper.insertBatch(insertSysAttachmentList);
        }


        long endTime = System.currentTimeMillis();
        log.info("程序运行时间： " + (endTime - startTime) + "ms");
        return new ResponseVO(ErrorCodeEnum.SUCCESS, null);
    }

    @Override
    public ResponseVO delAssetEntry(String id) {
        return new ResponseVO(ErrorCodeEnum.SUCCESS,wyAssetEntryMapper.delById(id));
    }

    @Override
    public ResponseVO export(ExportWyAssetEntryDTO dto, HttpServletResponse response) {

        List<WyAssetEntryHomeList> list = wyAssetEntryMapper.queryWyAssetEntryHomeListTotal(dto.getWyAssetEntryHomeDTO());
        Map<String, String> keyMap = dto.getKeyMap();

        //需要显示的字段名
        //字段名对应的中文名
        String[] headers = null;
        String[] needShowKeyArr = null;

        if (StringUtils.isEmpty(dto.getNeedShowKeys())) {
            //不传getNeedShowKeys 显示所有字段
            Set<String> keys = keyMap.keySet();
            needShowKeyArr = keys.toArray(new String[keys.size()]);
            headers = new String[keys.size()];
        } else {
            needShowKeyArr = dto.getNeedShowKeys().split(",");
            //字段名对应的中文名
            headers = new String[needShowKeyArr.length];
        }

        for (int i = 0; i < headers.length; i++) {
            headers[i] = keyMap.get(needShowKeyArr[i]);
        }

        try {
            ExcelUtils.getExcel(response, headers, needShowKeyArr, list, "WyAssetEntryList.xls");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
