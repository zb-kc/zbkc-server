package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.mapper.WyAssetInfoMapper;
import com.zbkc.model.dto.*;
import com.zbkc.model.po.WyAssetHomeInfo;
import com.zbkc.model.po.WyAssetStreet;
import com.zbkc.model.po.YdqList;
import com.zbkc.model.po.ZfWyAssetInfo;
import com.zbkc.model.vo.*;
import com.zbkc.service.WyAssetInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;


/**
 *
 * @author gmding
 * @date 2021-09-11
 */
@Service
@Slf4j
public class WyAssetInfoImpl implements WyAssetInfoService {

    @Autowired
    private WyAssetInfoMapper wyAssetInfoMapper;


    @Override
    public ResponseVO getHomeInfo(String type){

        String searchPropertyTag = "";
        if(StringUtils.isBlank(type)){
            type=  "首页";
            searchPropertyTag = "1,2";
        }

        if("首页".equals(type)){
            type = "政府物业,国企物业";
            searchPropertyTag = "1,2";
        }else if ("政府".equals(type)){
            type = "政府物业";
            searchPropertyTag = "1";
        }else if ("国企".equals(type)){
            type = "国企物业";
            searchPropertyTag = "2";
        }

        WyAssetHomeInfo info=new WyAssetHomeInfo();
        info.setCyNum(wyAssetInfoMapper.wyAssetInfoTypeNum( type,"产业用房" ));
        info.setBgNum(wyAssetInfoMapper.wyAssetInfoTypeNum( type,"办公用房" ));
        info.setGgNum(wyAssetInfoMapper.wyAssetInfoTypeNum( type,"公共服务设施" ));
        info.setSyNum(wyAssetInfoMapper.wyAssetInfoTypeNum( type,"商业" ));
        info.setZzNum(wyAssetInfoMapper.wyAssetInfoTypeNum( type,"住宅" ));
        info.setZlNum(wyAssetInfoMapper.wyAssetInfoZlNum("租赁社会物业")  );
        //资产入账
        info.setRzNum( wyAssetInfoMapper.rzToatl());


        Double gqBuildArea=wyAssetInfoMapper.wyAssetInfoGQBuildArea();
        Double val = wyAssetInfoMapper.wyAssetInfoAreaNum(type, "产业用房");
        if(null == val){
            val = 0D;
        }
        info.setCyArea(new BigDecimal(val).setScale(4, RoundingMode.UP));
        Double gqUserArea=wyAssetInfoMapper.wyAssetInfoCZGQBuildArea();

        info.setGqUserArea( gqUserArea );
        info.setGqBuildArea( gqBuildArea );

        info.setBgArea(new BigDecimal(wyAssetInfoMapper.wyAssetInfoAreaNum( type,"办公用房" )).setScale(4, RoundingMode.UP));
        info.setGgArea(new BigDecimal(wyAssetInfoMapper.wyAssetInfoAreaNum( type,"公共服务设施" )).setScale(4, RoundingMode.UP));
        info.setSyArea(new BigDecimal(wyAssetInfoMapper.wyAssetInfoAreaNum( type,"商业" )).setScale(4, RoundingMode.UP));
        info.setZzArea(new BigDecimal(wyAssetInfoMapper.wyAssetInfoAreaNum( type,"住宅" )).setScale(4, RoundingMode.UP));


        info.setKzArea( new BigDecimal(wyAssetInfoMapper.wyAssetInfoKzArea( type )).setScale(4, RoundingMode.UP));
        info.setCzArea( new BigDecimal(wyAssetInfoMapper.wyAssetInfoCzArea( type )).setScale(4, RoundingMode.UP));


        info.setSupportNum( wyAssetInfoMapper.wyAssetInfoSupportNum().size() );
        //info.setSupportNum( 168 );
        Double rent=wyAssetInfoMapper.wyAssetInfoCZMoney()/wyAssetInfoMapper.wyAssetInfoCZArea();

        //info.setCertHasNum(wyAssetInfoMapper.wyAssetInfoCertHas( "政府物业"));
        //产权办理
        info.setCertHasNum(wyAssetInfoMapper.newwyequitylistTotal());
        info.setAverageRent( new BigDecimal( rent).setScale(4, RoundingMode.UP));

        info.setActArea(new BigDecimal(wyAssetInfoMapper.wyAssetInfoActArea(type) ).setScale(4, RoundingMode.UP));
        info.setBuildArea(new BigDecimal(wyAssetInfoMapper.wyAssetInfoBuildArea(type)).setScale(4, RoundingMode.UP));
        info.setPubArea(new BigDecimal(wyAssetInfoMapper.wyAssetInfoPubArea(type) ).setScale(4, RoundingMode.UP));

        Double zhengfu = wyAssetInfoMapper.wyAssetInfoZdArea("政府物业");
        if(null == zhengfu){
            zhengfu = 0D;
        }
        info.setZdArea(new BigDecimal(zhengfu).setScale(4, RoundingMode.UP));


        List<WyAssetStreet> wyAssetStreetList = wyAssetInfoMapper.getStreetList(type);
        //删除南山区之外的街道
        Iterator<WyAssetStreet> iterator = wyAssetStreetList.iterator();
        while (iterator.hasNext()){
            WyAssetStreet next = iterator.next();
            String street = next.getStreet();
            if(null == street || "大浪街道".equals(street) || "新安街道".equals(street) || "其他".equals(street)){
                iterator.remove();
            }
        }

        info.setStreetList(wyAssetStreetList);
        for (int i=0;i<info.getStreetList().size();i++){
            info.getStreetList().get( i ).setPercentage(new BigDecimal(Double.valueOf( info.getStreetList().get( i ).getArea())/info.getBuildArea().doubleValue()).setScale(4, RoundingMode.UP).doubleValue());
        }

        //资产未入账
        int wyentrynamenumber = this.wyAssetInfoMapper.selectWyEntryTotal();
        info.setNotRzNum(wyentrynamenumber);

        //未办房产证
        int wyequitynumber = this.wyAssetInfoMapper.selectNoCertHasWyInfoTotal("","政府物业","");
        info.setNotCertHasNum(wyequitynumber);

        //出租租期长(不含产业用房)
        info.setLongContractNotCY(this.wyAssetInfoMapper.selectNotCyLongWyContractInfoTotal(5, 99));

        //合同预到期(不含产业用房)
        info.setDelayContractNotCY(34);

        //空置未使用(不含产业用房)
        info.setEmptyNumberNotCY(this.wyAssetInfoMapper.selectNotCyEmptyWyInWyAssetInfoV2Total(type));

        //总物业数
        Integer toalWyNumber = this.wyAssetInfoMapper.wyAssetInfoTypeNum2(type);
        info.setTotalWyNumber(toalWyNumber);

        //空置数
        int kzNum = this.wyAssetInfoMapper.selectEmptyNumberInKJ(searchPropertyTag);
        info.setKzNum(kzNum);

        //扶持企业数量
        int fcCompanyNumber = this.wyAssetInfoMapper.enterpriseListByBelongTypeTotal(null, null, null, null, null, type, null, null , "产业用房");
        info.setFcCompanyNumber(fcCompanyNumber);

        return new ResponseVO( ErrorCodeEnum.SUCCESS,info);
    }


    @Override
    public ResponseVO wyFivelist(String index){
        WyAssetHomeInfo info=new WyAssetHomeInfo();
        info.setCyNum(wyAssetInfoMapper.wyAssetInfoTypeNum( "政府物业",index ));
        info.setBgNum(wyAssetInfoMapper.wyAssetInfoTypeNum( "政府物业",index ));
        info.setGgNum(wyAssetInfoMapper.wyAssetInfoTypeNum( "政府物业",index ));
        info.setSyNum(wyAssetInfoMapper.wyAssetInfoTypeNum( "政府物业",index));
        info.setZzNum(wyAssetInfoMapper.wyAssetInfoTypeNum( "政府物业",index));
        info.setRzNum( wyAssetInfoMapper.rzToatl());

        Double zhengfu = wyAssetInfoMapper.wyAssetInfoAreaNum("政府物业", index);
        if(null == zhengfu){
            zhengfu = 0D;
        }
        info.setCyArea(new BigDecimal(zhengfu).setScale(4, RoundingMode.UP));
        Double zhengfu2 = wyAssetInfoMapper.wyAssetInfoAreaNum("政府物业", index);
        if(null == zhengfu2){
            zhengfu2 = 0D;
        }
        info.setBgArea(new BigDecimal(zhengfu2).setScale(4, RoundingMode.UP));
        info.setGgArea(new BigDecimal(wyAssetInfoMapper.wyAssetInfoAreaNum( "政府物业",index )).setScale(4, RoundingMode.UP));
        info.setSyArea(new BigDecimal(wyAssetInfoMapper.wyAssetInfoAreaNum( "政府物业",index )).setScale(4, RoundingMode.UP));
        info.setZzArea(new BigDecimal(wyAssetInfoMapper.wyAssetInfoAreaNum( "政府物业",index )).setScale(4, RoundingMode.UP));
        Double kzarea=wyAssetInfoMapper.wyAssetInfoKzArea1( "政府物业" ,index);
        if (kzarea==null){
            kzarea= Double.valueOf( 0 );
        }
        info.setKzArea( new BigDecimal(kzarea).setScale(4, RoundingMode.UP));
        info.setKzNum(wyAssetInfoMapper.wyAssetInfoKzNum("政府物业" ,index));

        info.setCzArea( new BigDecimal(wyAssetInfoMapper.wyAssetInfoCzArea1( "政府物业" ,index)).setScale(4, RoundingMode.UP));
        info.setSupportNum( 168 );
        Double czyzj=wyAssetInfoMapper.wyAssetInfoCzyzj1("政府物业" ,index);
        if (czyzj==null){
            czyzj= Double.valueOf( 0 );
        }
        Double czyArea1=wyAssetInfoMapper.wyAssetInfoCzyArea1("政府物业" ,index);
        if (czyArea1==null){
            czyArea1= Double.valueOf( 0 );
        }
        Double rent;
        if (czyArea1==0){
            rent= Double.valueOf( 0 );
        }else {
            rent=czyzj/czyArea1;
        }
        if (rent==null){
            rent= Double.valueOf( 0 );
        }
        info.setAverageRent( new BigDecimal( rent).setScale(4, RoundingMode.UP));


        Double zhengfu3 = wyAssetInfoMapper.wyAssetInfoActArea1("政府物业", index);
        if(null == zhengfu3){
            zhengfu3 = 0D;
        }
        info.setActArea(new BigDecimal(zhengfu3).setScale(4, RoundingMode.UP));
        info.setBuildArea(new BigDecimal(wyAssetInfoMapper.wyAssetInfoBuildArea1("政府物业",index)).setScale(4, RoundingMode.UP));
        Double zhengfu5 = wyAssetInfoMapper.wyAssetInfoPubArea1("政府物业", index);
        if(null == zhengfu5){
            zhengfu5 = 0D;
        }
        info.setPubArea(new BigDecimal(zhengfu5).setScale(4, RoundingMode.UP));
        Double zdArea=wyAssetInfoMapper.wyAssetInfoZdArea1("政府物业",index);
        if (zdArea==null){
            zdArea= Double.valueOf( 0 );
        }
        info.setZdArea(new BigDecimal(zdArea).setScale(4, RoundingMode.UP));


        info.setStreetList( wyAssetInfoMapper.getStreetList1("政府物业",index));
        for (int i=0;i<info.getStreetList().size();i++){
            info.getStreetList().get( i ).setPercentage(new BigDecimal(Double.valueOf( info.getStreetList().get( i ).getArea())/info.getBuildArea().doubleValue()).setScale(4, RoundingMode.UP).doubleValue());
        }

        //扶持企业数量
        int fcCompanyNumber = this.wyAssetInfoMapper.enterpriseListByBelongTypeTotal(null, null, null, null, null, "政府物业", null, null , index);
        info.setFcCompanyNumber(fcCompanyNumber);

        return new ResponseVO( ErrorCodeEnum.SUCCESS,info);
    }



    @Override
    public  ResponseVO analysis(){

        AnalysisVo vo=new AnalysisVo();
        vo.setZfNum( 18 );
        vo.setGqNum( 3 );
        List<AnalysisListVo> zfList=new ArrayList<>(  );
        AnalysisListVo listVo01=new AnalysisListVo();
        listVo01.setYear( "2016" );
        listVo01.setNum( 0 );
        zfList.add( listVo01 );
        AnalysisListVo listVo02=new AnalysisListVo();
        listVo02.setYear( "2017" );
        listVo02.setNum( 0 );
        zfList.add( listVo02);
        AnalysisListVo listVo=new AnalysisListVo();
        listVo.setYear( "2018" );
        listVo.setNum( 5 );
        zfList.add(listVo);
        AnalysisListVo listVo1=new AnalysisListVo();
        listVo1.setYear( "2019" );
        listVo1.setNum( 125 );
        zfList.add(listVo1);
        AnalysisListVo listVo2=new AnalysisListVo();
        listVo2.setYear( "2020" );
        listVo2.setNum( 183 );
        zfList.add(listVo2);

        vo.setZfList( zfList );

        Calendar date = Calendar.getInstance();
        int year=date.get(Calendar.YEAR);

        List<AnalysisListVo> gqList=new ArrayList<>(  );
        for (int i=0;i<5;i++){
            AnalysisListVo gqlistVo3=new AnalysisListVo();
            gqlistVo3.setYear( (year-5+i)+"" );
            gqlistVo3.setNum( wyAssetInfoMapper.wyAssetInfoGQCZNum((year-5+i)+"-01-01",(year-5+i+1)+"-01-01"));
            gqList.add( gqlistVo3 );
        }

        vo.setGqList( gqList );

        return new ResponseVO( ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public  ResponseVO special(){
        return new ResponseVO( ErrorCodeEnum.SUCCESS,null);
    }


    @Override
    public  ResponseVO zfAnalysis(){
        ZfAnalysisVo vo=new ZfAnalysisVo();
        KZVo kzMsg=new KZVo();

        kzMsg.setCyArea(wyAssetInfoMapper.wyAssetInfoKzArea1( "政府物业" ,"产业用房"));
        kzMsg.setGgArea(wyAssetInfoMapper.wyAssetInfoKzArea1( "政府物业" ,"公共服务设施"));
        kzMsg.setZzArea(wyAssetInfoMapper.wyAssetInfoKzArea1( "政府物业" ,"住宅"));
        kzMsg.setSyArea(wyAssetInfoMapper.wyAssetInfoKzArea1( "政府物业" ,"商业"));
        kzMsg.setBgArea(wyAssetInfoMapper.wyAssetInfoKzArea1( "政府物业" ,"办公用房"));
        Double kzArea=wyAssetInfoMapper.wyAssetInfoKzArea("政府物业");

        kzMsg.setCyRate( (kzMsg.getCyArea()==null?0:kzMsg.getCyArea())/kzArea );
        kzMsg.setGgRate( (kzMsg.getGgArea()==null?0:kzMsg.getGgArea())/kzArea );
        kzMsg.setZzRate( (kzMsg.getZzArea()==null?0:kzMsg.getZzArea())/kzArea );
        kzMsg.setSyRate( (kzMsg.getSyArea()==null?0:kzMsg.getSyArea())/kzArea );
        kzMsg.setBgRate( (kzMsg.getBgArea()==null?0:kzMsg.getBgArea())/kzArea );
        vo.setKzList(kzMsg );
        RZVo rz=new RZVo();
//        rz.setCyNum( wyAssetInfoMapper.wyAssetInfoCzyzj1("政府物业" ,"产业用房") );
//        rz.setGgNum( wyAssetInfoMapper.wyAssetInfoCzyzj1("政府物业" ,"公共服务设施") );
//        rz.setZzNum( wyAssetInfoMapper.wyAssetInfoCzyzj1("政府物业" ,"住宅") );
//        rz.setSyNum( wyAssetInfoMapper.wyAssetInfoCzyzj1("政府物业" ,"商业") );
//        rz.setBgNum( wyAssetInfoMapper.wyAssetInfoCzyzj1("政府物业" ,"办公用房") );

        rz.setCyNum( wyAssetInfoMapper.wyAssetInfoRz("政府物业" ,"产业用房") );
        rz.setGgNum( wyAssetInfoMapper.wyAssetInfoRz("政府物业" ,"公共服务设施") );
        rz.setZzNum( wyAssetInfoMapper.wyAssetInfoRz("政府物业" ,"住宅") );
        rz.setSyNum( wyAssetInfoMapper.wyAssetInfoRz("政府物业" ,"商业") );
        rz.setBgNum( wyAssetInfoMapper.wyAssetInfoRz("政府物业" ,"办公用房") );



        vo.setRzList( rz );

        CqVo cq=new CqVo();

        cq.setCyNum( wyAssetInfoMapper.wyAssetInfoCertHas1("政府物业" ,"产业用房") );
        cq.setGgNum( wyAssetInfoMapper.wyAssetInfoCertHas1("政府物业" ,"公共服务设施") );
        cq.setZzNum( wyAssetInfoMapper.wyAssetInfoCertHas1("政府物业" ,"住宅") );
        cq.setSyNum( wyAssetInfoMapper.wyAssetInfoCertHas1("政府物业" ,"商业") );
        cq.setBgNum( wyAssetInfoMapper.wyAssetInfoCertHas1("政府物业" ,"办公用房") );
        vo.setCqList( cq );
        HouseCodeVo houseCodeVo=new HouseCodeVo();
        houseCodeVo.setNoCode( wyAssetInfoMapper.wyAssetInfoNoCode() );
        houseCodeVo.setIneligibleCode( wyAssetInfoMapper.wyAssetInfoIneligibleCode() );
        houseCodeVo.setHaveCode( wyAssetInfoMapper.wyAssetInfoIneHouseCode()-houseCodeVo.getNoCode()-houseCodeVo.getIneligibleCode() );

//        houseCodeVo.setHaveCode( wyAssetInfoMapper.houseCode("已补充编码") );
//        houseCodeVo.setNoCode( wyAssetInfoMapper.houseCode("未补充编码") );
//        houseCodeVo.setIneligibleCode( wyAssetInfoMapper.houseCode("不符合编码条件") );

        vo.setHouseCodeList( houseCodeVo );


        List<ZcVo> zcVos=wyAssetInfoMapper.zcList();
        int tatal=0;
        for (int i=0;i<zcVos.size();i++){
            tatal+=zcVos.get( i ).getNum();
        }
        ZcVo zc=new ZcVo();
        zc.setNum( wyAssetInfoMapper.zcTotal()-tatal );
        zc.setUnitName( "其它" );
        zcVos.add( zc );
        vo.setZcList( zcVos );

        vo.setZq5Year( wyAssetInfoMapper.zq5Year() );
        vo.setZq10Year( wyAssetInfoMapper.zq10Year() );
        vo.setZq15Year( wyAssetInfoMapper.zq15Year() );

        vo.setZq1Month( wyAssetInfoMapper.zq1Month() );
        vo.setZq3Month( wyAssetInfoMapper.zq3Month() );
        vo.setZq6Month( wyAssetInfoMapper.zq6Month() );


        return new ResponseVO( ErrorCodeEnum.SUCCESS,vo);
    }




    @Override
    public ResponseVO ydqList(String index,int p,int size) {
        List<YdqList> list=wyAssetInfoMapper.wyAssetInfoYdqArea( index,(p-1)*size,size);

        PageVO<YdqList> vo = new PageVO<>();
        vo.setList( list );
        vo.setTotal(  wyAssetInfoMapper.wyAssetInfoYdqAreaTotal(index ).size());
        vo.setP(p);
        vo.setSize(size);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }

    @Override
    public ResponseVO cywyList(CywyDTO dto) {
        String belongsType = dto.getBelongsType();
        if(StringUtils.isBlank(belongsType)){
            belongsType = "政府物业";
        }

        String streetName = dto.getStreetName();
        if(null == streetName){
            dto.setStreetName("");
        }

        String wy_kind = dto.getWy_kind();
        if(null == wy_kind){
            dto.setWy_kind("");
        }

        String manUnit = dto.getManUnit();
        if (null == manUnit){
            dto.setManUnit("");
        }else {
            if("大沙河".equals(manUnit)){
                manUnit = "深圳市大沙河投资建设有限公司";
            }else if("深汇通".equals(manUnit)){
                manUnit = "深圳市深汇通投资控股有限公司";
            }
            dto.setManUnit(manUnit);
        }

        List<YdqList> list=wyAssetInfoMapper.wyAssetInfoCywyList1( belongsType,
                dto.getType(),
                dto.getIndex(),
                (dto.getP()-1)*dto.getSize(),
                dto.getSize() ,
                dto.getManUnit() ,
                dto.getWy_kind(),dto.getStreetName());
        int size = wyAssetInfoMapper.wyAssetInfoCywyListTotal1(belongsType, dto.getType(), dto.getIndex(),dto.getManUnit() ,dto.getWy_kind() ,dto.getStreetName()).size();

        //String str= "艺晶工业园,阳光工业园,河源高新区共建产业园起步园,河源高新区共建产业园（二期）,南山智造深汕高新产业园,南山智造（红花岭基地）城市更新项目一期";
        //int subNumber = 0;
        //if(belongsType.equals("国企物业")){
        //    Iterator<YdqList> iterator = list.iterator();
        //    while (iterator.hasNext()){
        //        YdqList next = iterator.next();
        //        String name = next.getName();
        //        boolean b = str.indexOf(name) > -1;
        //        if(b){
        //            iterator.remove();
        //            subNumber++;
        //        }
        //    }
        //    size -= subNumber;
        //}

        PageVO<YdqList> vo = new PageVO<>();
        vo.setList( list );

        vo.setTotal( size );
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }


    @Override
    public ResponseVO zdqy() {

        ZdqyVo vo=new ZdqyVo();
        vo.setLdgdNum(wyAssetInfoMapper.zdqyBasicInfo("领导挂点") );
        vo.setGybqNum( wyAssetInfoMapper.zdqyBasicInfo("工业百强"));
        vo.setFwybqNum( wyAssetInfoMapper.zdqyBasicInfo("服务业百强"));
        vo.setJzyNum( wyAssetInfoMapper.zdqyBasicInfo("建筑业五十强"));
        vo.setLsbqNum( wyAssetInfoMapper.zdqyBasicInfo("纳税百强"));
        vo.setShqyNum( wyAssetInfoMapper.zdqyBasicInfo("上市企业"));
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }

    @Override
    public ResponseVO zdqyList(CywyDTO dto) {

        String type = dto.getType();
        //if(type.equals("领导挂点")){
        //    List<ZdqyListVo> list=wyAssetInfoMapper.zdqyBasicInfoList2( dto.getType(),dto.getIndex(),(dto.getP()-1)*dto.getSize(),dto.getSize());
        //    PageVO<ZdqyListVo> vo = new PageVO<>();
        //    vo.setList( list );
        //    vo.setTotal(  wyAssetInfoMapper.zdqyBasicInfoListTotal2(dto.getType(),dto.getIndex()).size());
        //    vo.setP(dto.getP());
        //    vo.setSize(dto.getSize());
        //    return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
        //}

        List<ZdqyListVo> list=wyAssetInfoMapper.zdqyBasicInfoList( dto.getType(),dto.getIndex(),(dto.getP()-1)*dto.getSize(),dto.getSize());

        PageVO<ZdqyListVo> vo = new PageVO<>();
        vo.setList( list );
        vo.setTotal(  wyAssetInfoMapper.zdqyBasicInfoListTotal(dto.getType(),dto.getIndex()).size());
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());


        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }

    @Override
    public ResponseVO details(String code) {
        DetailsVo vo=new DetailsVo();

        QyDetailsVo qyDto=new QyDetailsVo();
        List<QyDetailsVo> qyDto1=wyAssetInfoMapper.qyBasicInfo(code);

        if (qyDto1.size()>0){
            qyDto=qyDto1.get( 0 );
        }
        qyDto.setCode( code );

        List<CBpersonVo>cbList=wyAssetInfoMapper.cbList(code);

        List<String> listName=wyAssetInfoMapper.comName( code );
        String name="";
        if (listName.size()>0){
            name=listName.get( 0 );
        }
        vo.setComName( name );

        //通过公司名称查询
        Integer cbNum = wyAssetInfoMapper.selectCompanyCbNumber(name);
        qyDto.setCbNum(null == cbNum ? 0 : cbNum);
        vo.setFcList(  wyAssetInfoMapper.fcListByCompanyName( name ));
        qyDto.setFcMoney(  wyAssetInfoMapper.fcListNumByCompanyName(name ));

        qyDto.setScale(  wyAssetInfoMapper.scale(code));

        List<QylhrcinfoVo> qyrcList = wyAssetInfoMapper.qyrcList(code);
        Iterator<QylhrcinfoVo> iterator1 = qyrcList.iterator();
        while (iterator1.hasNext()){
            QylhrcinfoVo next = iterator1.next();
            StringBuilder stringBuilder = new StringBuilder(next.getName());
            StringBuilder replace = stringBuilder.replace(1, 2, "*");
            next.setName(replace.toString());
        }
        vo.setQyrcList(qyrcList);

        //入驻信息
        StringBuilder newUseWyName = new StringBuilder("");
        CywyDTO queryDto = new CywyDTO();
        queryDto.setCompanyName(name);
        List<ZPDetailsVo> maps1 = this.wyAssetInfoMapper.selectNewCompanyUseWyInfoByCompanyName(queryDto);
        Iterator<ZPDetailsVo> iterator2 = maps1.iterator();
        while (iterator2.hasNext()){
            ZPDetailsVo next = iterator2.next();
            String wyName = next.getWyName();
            newUseWyName.append(wyName).append(",");
        }
        //List<ZPDetailsVo> zpList = wyAssetInfoMapper.zpList(code);
        vo.setZpList(maps1);



        List<String> listIntroduction=wyAssetInfoMapper.introduction( code );
        String introduction="";
        if (listIntroduction.size()>0){
            introduction=listIntroduction.get( 0 );
        }
        vo.setIntroduction(introduction );

        vo.setCbList( cbList==null?new ArrayList<>(  ):cbList );

        //扶持人才房 修改名字为星号
        List<FCRCFListVo> fcrcfList = wyAssetInfoMapper.FCRCFListByCompanyName(name);
        Iterator<FCRCFListVo> iterator = fcrcfList.iterator();
        while (iterator.hasNext()){
            FCRCFListVo next = iterator.next();
            StringBuilder str = new StringBuilder(next.getUserName());
            next.setUserName(str.replace(1, 2, "*").toString());
        }
        vo.setFcrcfList(fcrcfList);
        qyDto.setFcrcNum(vo.getFcrcfList().size()+"");
        qyDto.setRcNum( vo.getQyrcList().size()+"" );

        //租赁信息
        String zpNum = this.wyAssetInfoMapper.selectWyNameByCompanyName(name);
        if(null != newUseWyName.toString()){
            zpNum = newUseWyName.toString();
        }
        qyDto.setZpNum(zpNum);

        //设置公司简介
        Map map = this.wyAssetInfoMapper.selectNewCompanyInfo(code);
        if(null != map){
            Object companyinfo = map.get("companyinfo");
            if(null != companyinfo){
                vo.setIntroduction(companyinfo.toString());
            }
            Object address = map.get("address");
            if(null != address){
                qyDto.setAddress(address.toString());
            }
        }


        vo.setQyDetails( qyDto );
        vo.setRegTaxList( wyAssetInfoMapper.regTaxList(code) );

        vo.setNsTotal( wyAssetInfoMapper.nsTotal(name) );
        //分析
        vo.setFcxyList( wyAssetInfoMapper.fcxyList(code) );

        //设置退租原因
        CywyDTO dto = new CywyDTO();
        dto.setCompanyName(name);
        List<Map> maps = this.wyAssetInfoMapper.selectRefundReasonList(dto);
        vo.setRefundReasonList(maps);

        //设置经济贡献承诺
        Map economicInfo = this.wyAssetInfoMapper.selectEconomicInfo(name);
        if(null == economicInfo){
            vo.setIsEconomicCommit("否");
        }else {
            vo.setIsEconomicCommit("是");
            Object economiccommit = economicInfo.get("economiccommit");
            if(null != economiccommit){
                vo.setEconomicCommit(economiccommit.toString());
            }
        }

        //产业监管模块 2022年8月30日 新增
        vo.setRegulatorySituationMap(returnRegulatorySituationMap(vo.getComName()));

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }

    //产业监管map
    public Map<String , Object> returnRegulatorySituationMap(String companyName) {
        Map<String , Object> returnMap = new HashMap<>();

        //转租分租情况
        returnMap.put("hasSubletLeaseList" , this.wyAssetInfoMapper.hasNewCompanySubletLeaseNumber(companyName) > 0 ? "有" : "无");

        //合同列表
        List<Map> companyContractList = this.wyAssetInfoMapper.selectCompanyContractList(companyName);
        returnMap.put("companyContractList" , companyContractList);

        //企业荣誉
        //获奖信息34 证书34 专利34 软件著作权 34
        Map<String , Object> honorMap = new HashMap<>();
        //获奖信息
        honorMap.put("rewardnumber" , this.wyAssetInfoMapper.countNewCompanyRewardNumber(companyName));
        //证书
        honorMap.put("certificatenumber" , this.wyAssetInfoMapper.countNewCompanyCertificateNumber(companyName));
        //专利
        honorMap.put("patentnumber" , this.wyAssetInfoMapper.countNewCompanyPatentNumber(companyName));
        //软著
        honorMap.put("softwarecopyrightnumber" , this.wyAssetInfoMapper.countCompanySoftwareCopyrightNumber(companyName));
        returnMap.put("honorinfo" , honorMap);

        //详情
        Map map = this.wyAssetInfoMapper.selectNewCompanyDetailInfo(companyName);
        if (null != map){
            map.put("patentnumber" , honorMap.get("patentnumber"));
            returnMap.put("newcompanyinfo" , map);
        }

        /**
         * xAxisData: ['2019年', '2020年', '2021年'],
         *  legendData: ['退租数', '入驻数'],
         *  datas: [
         *   [58, 67, 44],
         *   [60, 78, 33]
         *  ]
         */
        List<Map> czNsList = this.wyAssetInfoMapper.selectCompanyCzNsList(companyName);
        if (null != czNsList && czNsList.size() > 0 ){

            List<String> yearList = new ArrayList<>(czNsList.size());
            String[] czlegendDataArr = new String[]{"产值承诺", "实际产值"};
            List<String> czCommitList = new ArrayList<>();
            List<String> czCompletionList = new ArrayList<>();

            String[] nsLegendDataArr = new String[]{"纳税承诺", "实际纳税"};
            List<String> nsCommitList = new ArrayList<>();
            List<String> nsCompletionList = new ArrayList<>();

            List<String> czBufferList = new ArrayList<>();
            List<String> nsBufferList = new ArrayList<>();

            for (Map czNs : czNsList) {
                String year = czNs.get("year").toString();
                yearList.add(year);

                Object czCommitment = czNs.get("cz_commitment");
                czCommitList.add(null == czCommitment ? "0" : czCommitment.toString());

                Object czCompletion = czNs.get("cz_completion");
                czCompletionList.add(null == czCompletion ? "0" : czCompletion.toString());

                Object nsCommitment = czNs.get("ns_commitment");
                nsCommitList.add(null == nsCommitment ? "0" : nsCommitment.toString());

                Object nsCompletion = czNs.get("ns_completion");
                nsCompletionList.add(null == nsCompletion ? "0" : nsCompletion.toString());

                Object czPercent = czNs.get("cz_percent");
                czBufferList.add(year + "年经济贡献完成率为" + czPercent + ";");

                Object nsPercent = czNs.get("ns_percent");
                nsBufferList.add(year + "年经济贡献完成率为" + nsPercent + ";");

            }

            //产值对比分析
            Map czMap = new HashMap();
            czMap.put("xAxisData" , yearList);
            czMap.put("legendData" , czlegendDataArr);
            List allCzArr = new ArrayList();
            allCzArr.add(czCommitList);
            allCzArr.add(czCompletionList);
            czMap.put("datas" , allCzArr);
            czMap.put("conclusion" ,czBufferList);
            returnMap.put("czMap" , czMap);

            //纳税对比分析
            Map nsMap = new HashMap();
            nsMap.put("xAxisData" , yearList);
            nsMap.put("legendData" , nsLegendDataArr);
            List allNsArr = new ArrayList();
            allNsArr.add(nsCommitList);
            allNsArr.add(nsCompletionList);
            nsMap.put("datas" , allNsArr);
            nsMap.put("conclusion" ,nsBufferList);
            returnMap.put("nsMap" , nsMap);


            String conclusion = "该企业未达到监管协议约定要求。根据《创新型产业用房管理办法》，建议该企业按以下标准续租:\n\n" +
                    "（1）该企业为非上市企业，租赁REPLACE1㎡＜8000㎡，符合《创新型产业用房管理办法》\n\n" +
                    "（2）上年度在南山贡献产值为REPLACE2万元，且租赁期内较上一年度产值（营业收入）未实现翻番增长，因此租金折扣为50%\n\n" +
                    "（3）续租期限不超过3年\n\n";

            Object czCompletion = czNsList.get(czNsList.size() - 1).get("cz_completion");
            conclusion = conclusion.replace("REPLACE2" , null == czCompletion ? "-" : czCompletion.toString());

            if (null != companyContractList){
                Object buildarea = companyContractList.get(0).get("出租面积");
                conclusion = conclusion.replace("REPLACE1" , null == buildarea ? "-" : buildarea.toString().replace("㎡" , ""));
            }

            returnMap.put("conclusion" , conclusion);

        }

        return returnMap;
    }


    @Override
    public ResponseVO regTax(String id) {

       String v1Id=wyAssetInfoMapper.V1Id(id);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,wyAssetInfoMapper.regTax(v1Id));
    }

    @Override
    public ResponseVO cyReTax() {

        CyReTaxVo vo=new CyReTaxVo();
        vo.setRegTaxList( wyAssetInfoMapper.cyReTax());

        List<ZlRegTaxVo> zlRegTaxList=wyAssetInfoMapper.zlRegTaxList();
        for (int i=0;i<zlRegTaxList.size();i++){
            zlRegTaxList.get( i ).setRegTaxList( wyAssetInfoMapper.regTax(zlRegTaxList.get( i ).getId()) );
        }
        vo.setZlRegTaxVoList(zlRegTaxList);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }


    @Override
    public ResponseVO cyDetails(String id) {
        CyDetailsVo vo=wyAssetInfoMapper.cyDetails( id );

        //可出租面积 已出租面积 空置户 空置面积 空置率
        //04da55c172444a17951233a217ee08e0 百旺信
        if("04da55c172444a17951233a217ee08e0".equals(id)){
            //百旺信高科技工业园,空置面积68727.81,使用面积173308.63,产权登记面积0.00,资产入账面积0.00,建筑面积243823.620000,sonnumber25
            vo = new CyDetailsVo();
            vo.setBuildArea(243823.62);
            vo.setRentableArea(243823.62);
            vo.setRentedArea(243823.62);
            vo.setBuildingCount(18D);
            vo.setVacantArea(68727.81);
            vo.setVacantCount(0);
        }
        //101f53f1ddd04d95aed5e0ff064ba1cb 创新大厦
        if("101f53f1ddd04d95aed5e0ff064ba1cb".equals(id)){
            //创新大厦,空置面积13662.35,使用面积93935.00,产权登记面积0.00,资产入账面积0.00,建筑面积108141.280000,sonnumber4
            vo = new CyDetailsVo();
            vo.setBuildArea(108141.28);
            vo.setRentableArea(108141.28);
            vo.setRentedArea(93935.00);
            vo.setBuildingCount(1D);
            vo.setVacantArea(13662.35);
            vo.setVacantCount(10);

        }
        //9dc46edd7c9142f5a65589c368eb5b8f 汇通大厦
        if("9dc46edd7c9142f5a65589c368eb5b8f".equals(id)){
            //汇通大厦（大沙河）,空置面积5240.00,使用面积13835.00,产权登记面积0.00,资产入账面积0.00,建筑面积19075.000000,sonnumber64
            vo = new CyDetailsVo();
            vo.setBuildArea(19075.0);
            vo.setRentableArea(19075.0);
            vo.setRentedArea(13835.0);
            vo.setBuildingCount(1D);
            vo.setVacantArea(5240.0);
            vo.setVacantCount(15);
        }

        //南山智谷

        Map<String,String> tempTemp = new HashMap();
        tempTemp.put("百旺信高科技工业园","深圳市南山区西丽街道阳光社区松白路1002号");
        tempTemp.put("创新大厦","深圳市南山区西丽街道马家龙社区大新路178号");
        tempTemp.put("金桃园大厦裙楼","深圳市南山区南头街道大新社区桃园路193号");
        tempTemp.put("侨城一号广场","深圳市南山区沙河街道高发社区深云路2号侨城一号广场");
        tempTemp.put("绿景美景广场1栋","深圳市南山区沙河街道高发社区侨香路4088号");
        tempTemp.put("南山智园（二期）","深圳市南山区桃源街道长源社区学苑路");
        tempTemp.put("阳光科创中心一期A座","深圳市南山区南山街道南山社区南新路1024号");
        tempTemp.put("创意大厦","深圳市南山区南头街道莲城社区南海大道3025号");
        tempTemp.put("侨城坊8号楼","深圳市南山区沙河街道文昌社区侨香路4080号");
        tempTemp.put("兆邦基科技中心B座8层","深圳市南山区南山街道荔湾社区前海月亮湾大道与东滨路交汇处");
        tempTemp.put("汇通大厦(政府)","深圳市南山区粤海街道海珠社区文心五路与海德一道交叉路口西侧(学府中学东侧)");
        tempTemp.put("南山智园（三期）崇文园区","深圳市南山区桃源街道塘朗社区留仙大道3370号");
        tempTemp.put("南山软件园","深圳市南山区南头街道莲城社区深南大道10128号");
        tempTemp.put("水木一方大厦","深圳市南山区南头街道大汪山社区南光路268号");
        tempTemp.put("南山智园（一期）","深圳市南山区桃源街道长源社区学苑大道1001号");
        tempTemp.put("汇通大厦","深圳市南山区粤海街道海珠社区文心五路11号");
        tempTemp.put("塘朗城广场（西区）A座","深圳市南山区桃源街道塘朗社区留仙大道3333号");
        tempTemp.put("南山金融大厦","深圳市南山区粤海街道大冲社区科发路11号");
        tempTemp.put("方大城（方大广场）","深圳市南山区桃源街道龙珠社区龙珠四路2号");
        tempTemp.put("百兴科技大厦","深圳市南山区粤海街道科技园社区科技路13号");
        tempTemp.put("源政创新大楼","深圳市南山区西丽街道松坪山社区朗山路19号");
        tempTemp.put("南山云科技大厦","深圳市南山区西丽街道西丽社区万科云城六期一栋");
        tempTemp.put("南山智造（红花岭基地）二期","深圳市南山区桃源街道留仙大道南侧");
        tempTemp.put("南山智谷","深圳市南山区西丽街道沙河西路3157号");
        tempTemp.put("艺晶工业园","深圳市南山区西丽街道阳光社区松白路1008号");
        tempTemp.put("河源高新区共建产业园起步园","河源市源城区和谐路260号");
        tempTemp.put("河源高新区共建产业园（二期）","河源市源城区和谐路261号");
        tempTemp.put("南山智造深汕高新产业园","");
        tempTemp.put("南山区高新区北区联合大厦建设项目","深圳市南山区西丽社区朗山二路与科苑路交汇处西北侧");


        List<String> list=wyAssetInfoMapper.imgUrl( id );
        if (null != list && list.size()>0){
            vo.setUrl( list );
        }
        if(null != vo){
            //换成企服的数据
            String searchid=wyAssetInfoMapper.V1Id(id);
            //Integer qyNumber = wyAssetInfoMapper.rzQyDetailsListTotalNumber(searchid , "");
            String wyname = this.wyAssetInfoMapper.selectCurrentWyNameByV1Id(id);
            if("塘朗城广场（西区）A座".equals(wyname)){
                wyname= "塘朗城广场";
            }

            if(tempTemp.containsKey(wyname)){
                vo.setAddress(tempTemp.get(wyname));
            }

            Integer qyNumber = wyAssetInfoMapper.enterCompanyNumber(wyname);
            vo.setQyCount(qyNumber);

            int totalmonth = 0;
            //List<String> useUnitList = wyAssetInfoMapper.ydq1Month( searchid );
            //for (int i=0;i<useUnitList.size();i++){
            //    if (wyAssetInfoMapper.ydq1MonthNum(useUnitList.get( i ),0,3)>0){
            //        totalmonth+=1;
            //    }
            //    if (wyAssetInfoMapper.ydq1MonthNum(useUnitList.get( i ),3,6)>0){
            //        totalmonth+=1;
            //    }
            //    if (wyAssetInfoMapper.ydq1MonthNum(useUnitList.get( i ),6,12)>0){
            //        totalmonth+=1;
            //    }
            //}


            List<Map> maps = wyAssetInfoMapper.ydq1MonthMap(null, 0, 12, wyname);
            if(null != maps){
                totalmonth+= maps.size();
            }

            vo.setBeExpire(totalmonth);

            if(!"04da55c172444a17951233a217ee08e0".equals(id)){
                Integer emptynumber = this.wyAssetInfoMapper.getEmptyInfo(id);
                vo.setVacantCount(emptynumber);

                //重新计算空置面积 直接取企服的空置表
                String topWyName = wyAssetInfoMapper.selectCurrentWyNameByV1Id(id);
                Double totalEmptyArea = this.wyAssetInfoMapper.selectTotalEmptyByTopWyName(topWyName);
                if(null == totalEmptyArea){
                    totalEmptyArea = 0D;
                }
                vo.setRentedArea(vo.getBuildArea() - totalEmptyArea);
                vo.setVacantArea(totalEmptyArea);
            }else{
                //百旺信特殊处理
                vo.setVacantCount(0);
                vo.setVacantArea(0D);
            }
            vo.setUserTime( wyAssetInfoMapper.userTime( vo.getName() ) );



            Map<String , String[]> map = new HashMap<>();
            map.put("百旺信高科技工业园",new String[]{"222533.1","222533.1"});
            map.put("创新大厦",new String[]{"16623.34","14578.27"});
            map.put("金桃园大厦裙楼",new String[]{"3921.58","3921.58"});
            map.put("侨城一号广场",new String[]{"8730.56","6673.88"});
            map.put("绿景美景广场1栋",new String[]{"2481.78","1599.97"});
            map.put("南山智园（二期）",new String[]{"164056.05","55407.41"});
            map.put("阳光科创中心一期A座",new String[]{"7624.12","7624.12"});
            map.put("创意大厦",new String[]{"2550.05","2550.05"});
            map.put("侨城坊8号楼",new String[]{"9975.4","9975.4"});
            map.put("兆邦基科技中心B座8层",new String[]{"1800.64","0"});
            map.put("汇通大厦",new String[]{"13103","13103"});
            map.put("南山智园（三期）崇文园区",new String[]{"179586.47","134747.44"});
            map.put("水木一方大厦",new String[]{"1501.28","0"});
            map.put("南山智园（一期）",new String[]{"309319.16","294734.36"});
            map.put("汇通大厦",new String[]{"13103","13103"});
            map.put("塘朗城广场（西区）A座",new String[]{"48973.54","45190.08"});
            map.put("南山金融大厦",new String[]{"30039.3","17899.98"});
            map.put("方大城（方大广场）",new String[]{"3065.94","3065.94"});
            map.put("百兴科技大厦配套创新型产业用房",new String[]{"811.23","0"});
            map.put("源政创新大楼",new String[]{"2493.75","0"});
            map.put("南山云科技大厦",new String[]{"149881.68","18399"});
            map.put("南山智造（红花岭基地）二期",new String[]{"95320","72072"});
            if(map.containsKey(vo.getName())){
                //可出租 已出租
                vo.setRentedArea(Double.valueOf(map.get(vo.getName())[1]));
                vo.setVacantArea(Double.valueOf(map.get(vo.getName())[0]) - Double.valueOf(map.get(vo.getName())[1]));
                vo.setRentableArea(Double.valueOf(map.get(vo.getName())[0]));
            }

            String showTab = this.wyAssetInfoMapper.selectWyShowTab(wyname);
            if(null != showTab && StringUtils.isNotBlank(showTab)){
                String[] split = showTab.split(",");
                vo.setShowTab(split);
            }else{
                vo.setShowTab(new String[0]);
            }

            Map analysisMap = this.wyAssetInfoMapper.selectTopWyAnalysis(wyname);
            if(null == analysisMap){
                analysisMap = new HashMap();
            }
            List<Map> analysisMapList = this.wyAssetInfoMapper.selectTopWyAnalysis2(wyname);
            Iterator<Map> iterator = analysisMapList.iterator();
            while (iterator.hasNext()){
                Map next = iterator.next();
                /**
                 * sum( sjzj ) AS sjzj,
                 * 		sum( jmzj ) AS jyzj,
                 * 		sum( syjx ) AS syjx,
                 * 		sum( avgarea ) AS avgarea,
                 * 		sum( num ) AS num
                 */
                Object year = next.get("year");
                Object sjzj = next.get("sjzj");
                Object jyzj = next.get("jyzj");
                Object syjx = next.get("syjx");
                Object avgarea = next.get("avgarea");
                Object num = next.get("num");

                analysisMap.put("sjzj"+year , sjzj);
                analysisMap.put("jyzj"+year , jyzj);
                analysisMap.put("syjx"+year , syjx);
                analysisMap.put("avgarea"+year , null == avgarea ? 0 : num);
                analysisMap.put("num"+year , null == num ? 0 : num);
            }

            Integer cz2021Number = this.wyAssetInfoMapper.countCz2021Number(wyname);
            Integer tax2021Number = this.wyAssetInfoMapper.countTax2021Number(wyname);
            Integer fuchi2021Number = this.wyAssetInfoMapper.countFuchi2021Number(wyname);
            Integer contri2021Number = this.wyAssetInfoMapper.countContri2021Number(wyname);
            Integer sjzj2021Number = this.wyAssetInfoMapper.countSjzj2021Number(wyname);
            Integer jyzj2021Number = this.wyAssetInfoMapper.countJyzj2021Number(wyname);
            Integer syjx2021Number = this.wyAssetInfoMapper.countSyjx2021Number(wyname);
            Integer avgarea2021Number = this.wyAssetInfoMapper.countAvgarea2021Number(wyname);
            Integer num2021Number = this.wyAssetInfoMapper.countNum2021Number(wyname);

            analysisMap.put("cz2021Number" , cz2021Number);
            analysisMap.put("tax2021Number" , tax2021Number);
            //analysisMap.put("fuchi2021Number" , fuchi2021Number);
            analysisMap.put("contri2021Number" , contri2021Number);
            analysisMap.put("sjzj2021Number" , sjzj2021Number);
            //analysisMap.put("jyzj2021Number" , jyzj2021Number);
            analysisMap.put("syjx2021Number" , syjx2021Number);
            //analysisMap.put("avgarea2021Number" , avgarea2021Number);
            analysisMap.put("num2021Number" , num2021Number);

            //设置扶持效益分析
            vo.setAnalysisMap(analysisMap);
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }




    @Override
    public ResponseVO cyDetailsHouseInfo(CywyDTO dto) {

        List<CyDetailsHouseInfoVo> list=wyAssetInfoMapper.cyDetailsHouseInfoList(dto.getIndex(),(dto.getP()-1)*dto.getSize(),dto.getSize() );

        PageVO<CyDetailsHouseInfoVo> vo = new PageVO<>();
        vo.setList( list );
        vo.setTotal(  wyAssetInfoMapper.cyDetailsHouseInfoListTotal(dto.getIndex()));
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());


        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }
    @Override
    public ResponseVO KzDetailsList(CywyDTO dto) {

        List<KzDetailsVo> list = new ArrayList<>();
        int total = 0;

        if(null != dto.getType() && ("04da55c172444a17951233a217ee08e0".equals(dto.getType()) || "产业用房".equals(dto.getType()))  ){

            if("产业用房".equals(dto.getType())){
                dto.setType(null);
            }

            //百旺信工业园单独处理
            list = wyAssetInfoMapper.kzDetailsList2( dto.getType(),dto.getIndex(),(dto.getP()-1)*dto.getSize(),dto.getSize() ,dto.getStartNumber() , dto.getEndNumber(),dto.getPropertytag());
            total = wyAssetInfoMapper.kzDetailsListTotal2(dto.getType(),dto.getIndex(),dto.getStartNumber() , dto.getEndNumber(),dto.getPropertytag());
        }else{
            list = wyAssetInfoMapper.kzDetailsList( dto.getType(),dto.getIndex(),(dto.getP()-1)*dto.getSize(),dto.getSize() ,dto.getStartNumber() , dto.getEndNumber());
            total = wyAssetInfoMapper.kzDetailsListTotal(dto.getType(),dto.getIndex(),dto.getStartNumber() , dto.getEndNumber());
        }

        PageVO<KzDetailsVo> vo = new PageVO<>();
        vo.setList( list );
        vo.setTotal( total );
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }

    @Override
    public ResponseVO KzDetailsList2(CywyDTO dto) {
        List<KzDetailsVo> list = new ArrayList<>();
        int total = 0;

        if(null == dto.getPropertytag()){
            dto.setPropertytag(1);
        }
        list = wyAssetInfoMapper.kzDetailsList2( dto.getType(),dto.getIndex(),(dto.getP()-1)*dto.getSize(),dto.getSize(),dto.getStartNumber() , dto.getEndNumber(),dto.getPropertytag());
        total = wyAssetInfoMapper.kzDetailsListTotal2(dto.getType(),dto.getIndex(),dto.getStartNumber() , dto.getEndNumber(),dto.getPropertytag());

        PageVO<KzDetailsVo> vo = new PageVO<>();
        vo.setList( list );
        vo.setTotal( total );
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO showWyInfoByCompanyName(CywyDTO dto) {

        String companyName = dto.getCompanyName();
        //
        //int p = (dto.getP() - 1) * dto.getSize();
        //dto.setP(p);
        //List<Map> maps = this.wyAssetInfoMapper.selectNewCompanyUseWyInfoByCompanyName(dto);
        //
        //dto.setP(null);
        //int size = this.wyAssetInfoMapper.selectNewCompanyUseWyInfoByCompanyName(dto).size();
        //
        //PageVO<Map> vo = new PageVO<>();
        //vo.setList( maps );
        //vo.setTotal( size );
        //vo.setP(dto.getP());
        //vo.setSize(dto.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,null);
    }

    @Override
    public ResponseVO selectWyInfoByManUnit(CywyDTO dto) {

        int p = (dto.getP() - 1) * dto.getSize();
        List<Map> maps = this.wyAssetInfoMapper.selectWyInfoByManUnit(dto, p, dto.getSize());

        int size = this.wyAssetInfoMapper.selectWyInfoByManUnit(dto, null, null).size();

        Double total = this.wyAssetInfoMapper.selectWyInfoByManUnitTotalBuildArea(dto);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( maps );
        vo.setTotal( size );
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        Map tempMap = new HashMap();
        tempMap.put("totalbuildarea" , total);
        vo.setOtherInfo(tempMap);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO selectWyInfoGuoqi(CywyDTO dto) {

        if(null == dto.getBelongsType()){
            dto.setBelongsType("国企物业");
        }
        if(null != dto.getManUnit() && StringUtils.isNotBlank(dto.getManUnit())){
            if("大沙河".equals(dto.getManUnit())){
                dto.setManUnit("深圳市大沙河投资建设有限公司");
            }else if("深汇通".equals(dto.getManUnit())){
                dto.setManUnit("深圳市深汇通投资控股有限公司");
            }
        }

        Map map = this.wyAssetInfoMapper.selectWyInfoByGuoqi(dto);

        List<Map> wyFiveMap = this.wyAssetInfoMapper.selectWyFiveByBelongType(dto.getBelongsType() , dto.getManUnit());
        Iterator<Map> iterator = wyFiveMap.iterator();
        Map temp = new HashMap();
        while (iterator.hasNext()){
            Map next = iterator.next();
            temp.put(next.get("wy_five") , next.get("buildarea"));
        }
        map.put("wyFiveMap" , temp);


        int fcCompanyNumber = this.wyAssetInfoMapper.enterpriseListByBelongTypeTotal(null, null, null, null, null, "国企物业", null,  dto.getManUnit() , null);
        map.put("fcCompanyNumber" , fcCompanyNumber);


        Double buildarea = this.wyAssetInfoMapper.selectGuoqiBuildAreaByUseStatus(dto);
        dto.setTopWyManUnit(dto.getManUnit());
        dto.setManUnit(null);
        List<Map> groupByUseStatusMapList = this.wyAssetInfoMapper.selectGuoqiBuildAreaGroupByUseStatus(dto);

        Map returnMap = new HashMap();
        returnMap.put("buildarea" , buildarea);
        returnMap.put("managearea" , buildarea);
        //自用
        returnMap.put("useselfarea" , 0);
        //调配
        returnMap.put("changearea" , 0);
        //出租
        returnMap.put("rentarea" , 0);
        //空置
        returnMap.put("emptyarea" , 0);

        Iterator<Map> iterator2 = groupByUseStatusMapList.iterator();
        while (iterator2.hasNext()){
            Map next = iterator2.next();
            String usestatus = next.get("usestatus").toString();
            String key = "";
            switch (usestatus){
                case "出租":
                    key = "rentarea";
                    break;
                case "调配":
                    key = "changearea";
                    break;
                case "自用":
                    key = "useselfarea";
                    break;
                case "空置":
                    key = "emptyarea";
                    break;
                default:
                    break;
            }

            if(StringUtils.isNotBlank(key)){
                returnMap.put(key , next.get("totalusearea"));
            }
        }
        map.putAll(returnMap);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,map);
    }

    @Override
    public ResponseVO RzDetailsList(String id) {
        RzDetailsListVo vo =new RzDetailsListVo();
        //List<RzListVo>rzList=wyAssetInfoMapper.rz5List(id);
        List<RzListVo>rzList=new ArrayList<>();
        RzListVo rz=new RzListVo();
        rz.setName( "其它" );
        int total=0;
        String v1Id=wyAssetInfoMapper.V1Id(id);

        //行业统计
        String wyname = wyAssetInfoMapper.selectCurrentWyNameByV1Id(id);
        if("塘朗城广场（西区）A座".equals(wyname)){
            wyname= "塘朗城广场";
        }

        List<RzListVo> listVos = wyAssetInfoMapper.rz5ListOtherTotal(wyname);
        for (int i=0;i<listVos.size();i++) {
            total+=listVos.get( i ).getNum();
            //展示全部行业，使用的是企业的数据，就只有6个行业
            rzList.add( listVos.get( i ) );
        }
        rz.setNum( total );

        rzList.sort(new Comparator<RzListVo>() {
            @Override
            public int compare(RzListVo o1, RzListVo o2) {
                return o1.getNum() - o2.getNum();
            }
        });

        //预到期统计

        //rzList.add( rz );
        vo.setRzList(rzList);
        int month1=0;
        int month3=0;
        int month6=0;

        //List<String> list=wyAssetInfoMapper.ydq1Month( v1Id );
        //for (int i=0;i<list.size();i++){
        //    if (wyAssetInfoMapper.ydq1MonthMapNumber(null,0,3 , wyname)>0){
                month1+=wyAssetInfoMapper.ydq1MonthMapNumber(null,0,3 , wyname);
            //}
            //if (wyAssetInfoMapper.ydq1MonthMapNumber(null,3,6, wyname)>0){
                month3+=wyAssetInfoMapper.ydq1MonthMapNumber(null,3,6, wyname);
            //}
            //if (wyAssetInfoMapper.ydq1MonthMapNumber(null,6,12, wyname)>0){
                month6+=wyAssetInfoMapper.ydq1MonthMapNumber(null,6,12, wyname);
            //}
        //}

        vo.setYdq1Month( month1);
        vo.setYdq3Month( month3 );
        vo.setYdq6Month( month6 );

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }

    @Override
    public ResponseVO RzQyDetailsList(CywyDTO dto) {

        //String id=wyAssetInfoMapper.V1Id(dto.getType());
        //
        //List<RzQyDetailsListVo> list=wyAssetInfoMapper.rzQyDetailsListVo( id,dto.getIndex(),(dto.getP()-1)*dto.getSize(),dto.getSize());
        //
        //PageVO<RzQyDetailsListVo> vo = new PageVO<>();
        //
        //for (int i=0;i<list.size();i++){
        //    list.get( i ).setWyNum( wyAssetInfoMapper.comNum(list.get( i ).getName()));
        //    list.get( i ).setRcNum( wyAssetInfoMapper.qyLhrcinfoNum(list.get( i ).getName()));
        //    list.get( i ).setFcNum( wyAssetInfoMapper.syncProposalNum(list.get( i ).getName()));
        //
        //    //list.get( i ).setRcfNum( wyAssetInfoMapper.rcfcNym(list.get( i ).getName()));
        //    list.get( i ).setRcfNum( wyAssetInfoMapper.rcfcNym1(list.get( i ).getName()));
        //    list.get( i ).setCode( wyAssetInfoMapper.code(list.get( i ).getName()));
        //    if (list.get( i ).getCz()==null && list.get( i ).getTotal()==null &&list.get( i ).getBknum()==null){
        //        FCXYVo fCXYVo=wyAssetInfoMapper.rzQyFCRCFList( list.get( i ).getCode() );
        //        if (fCXYVo!=null){
        //            list.get( i ).setBknum( fCXYVo.getNum() );
        //            list.get( i ).setCz( fCXYVo.getCz() );
        //            list.get( i ).setTotal( fCXYVo.getNum() );
        //            list.get( i ).setNs( fCXYVo.getNs() );
        //            list.get( i ).setClgx( fCXYVo.getClgx() );
        //
        //            list.get( i ).setBuildArea( fCXYVo.getArea() );
        //        }
        //    }
        //}
        //
        //vo.setList( list );
        //vo.setTotal(  wyAssetInfoMapper.rzQyDetailsListTotalVo(id,dto.getIndex()).size());
        //vo.setP(dto.getP());
        //vo.setSize(dto.getSize());

        String name = wyAssetInfoMapper.selectCurrentWyNameByV1Id(dto.getType());
        if("塘朗城广场（西区）A座".equals(name)){
            name= "塘朗城广场";
        }
        List<Map> list=wyAssetInfoMapper.enterpriseList2( 0,dto.getIndex(),name,(dto.getP()-1)*dto.getSize(),dto.getSize());

        Iterator<Map> iterator = list.iterator();
        while (iterator.hasNext()){
            //buildArea a.tax2021 as total
            HashMap next = (HashMap) iterator.next();
            BigDecimal buildarea = new BigDecimal(next.get("buildArea").toString());
            Object total1 = next.get("total");
            if(null == total1){
                next.put("ns" , 0);
                continue;
            }
            BigDecimal total = new BigDecimal(total1.toString());
            BigDecimal divide = total.divide(buildarea,2, BigDecimal.ROUND_HALF_UP);
            next.put("ns" , divide);
        }

        PageVO<Map> vo = new PageVO<>();
        vo.setList(  list  );
        vo.setTotal(wyAssetInfoMapper.enterpriseListTotal2(0,dto.getIndex(),name));
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }



    @Override
    public ResponseVO wyAssetsList(WyAssetsListDTO dto) {

        PageVO<WyAssetsListVo> vo = new PageVO<>();

        if (dto.getType()==1){
            List<WyAssetsListVo> list=wyAssetInfoMapper.wyAssetsList( dto.getIndex(),(dto.getP()-1)*dto.getSize(),dto.getSize());
            vo.setList( list );
            vo.setTotal(  wyAssetInfoMapper.wyAssetsListTotal(dto.getIndex()).size());

        }else if (dto.getType()==2){

            List<WyAssetsListVo> list=wyAssetInfoMapper.wyAssetsInfoList( dto.getIndex(),(dto.getP()-1)*dto.getSize(),dto.getSize());
            vo.setList( list );
            vo.setTotal(  wyAssetInfoMapper.wyAssetsInfoListTotal(dto.getIndex()).size());
        }

        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }
    @Override
    public ResponseVO streetNameList() {

        String notShow = "新安街道,大浪街道";

        List<String> data = wyAssetInfoMapper.streetNameList();
        Iterator<String> iterator = data.iterator();
        while (iterator.hasNext()){
            String next = iterator.next();
            if(notShow.indexOf(next) > -1){
                iterator.remove();
            }
        }

        return new ResponseVO(ErrorCodeEnum.SUCCESS, data);

    }


    @Override
    public ResponseVO communityNameList() {

        return new ResponseVO(ErrorCodeEnum.SUCCESS,wyAssetInfoMapper.communityNameList());

    }
    @Override
    public ResponseVO wyTypeList() {

        return new ResponseVO(ErrorCodeEnum.SUCCESS,wyAssetInfoMapper.wyTypeList());

    }


    @Override
    public ResponseVO wyList(WyListDTO dto) {

        PageVO<WyListVo> vo = new PageVO<>();

        dto.setP((dto.getP()-1)*dto.getSize());

        List<WyListVo> list=wyAssetInfoMapper.wyList( dto);
        vo.setList(list);

        vo.setTotal(wyAssetInfoMapper.wyListTotal(dto).size());

        vo.setP(dto.getP()/dto.getSize()+1);
        vo.setSize(dto.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }


    @Override
    public ResponseVO wyAnalysis(wyAnalysisDTO dto) {

        PageVO<WyAnalysisVo> vo = new PageVO<>();

        dto.setP((dto.getP()-1)*dto.getSize());

        List<WyAnalysisVo> list=wyAssetInfoMapper.wyAnalysis( dto.getWyname(),dto.getTztype(),dto.getTzresion(),dto.getName(),dto.getYear(),dto.getP(),dto.getSize());
        vo.setList(list);

        Integer total = wyAssetInfoMapper.wyAnalysisTotal(dto.getWyname(), dto.getTztype(), dto.getTzresion(), dto.getName(), dto.getYear());
        vo.setTotal(total);

        vo.setP(dto.getP()/dto.getSize()+1);
        vo.setSize(dto.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }



    @Override
    public ResponseVO MoreAnalysis(String v1Id) {


        String id=wyAssetInfoMapper.V1Id(v1Id);

        List<YQMoreAnalysisVo> list=wyAssetInfoMapper.YQMoreAnalysisList( id);

        YQMoreAnalysisVo vo=new YQMoreAnalysisVo();
        for (int i=0;i<list.size();i++){
            vo.setV1( vo.getV1()+list.get( i ).getV1() );
            vo.setV2( vo.getV2()+list.get( i ).getV2() );
            vo.setV3( vo.getV3()+list.get( i ).getV3() );
            vo.setV4( vo.getV4()+list.get( i ).getV4() );
            vo.setV5( vo.getV5()+list.get( i ).getV5() );
            vo.setV6( vo.getV6()+list.get( i ).getV6() );
            vo.setV7( vo.getV7()+list.get( i ).getV7() );
            vo.setV8( vo.getV8()+list.get( i ).getV8() );
            vo.setV9( vo.getV9()+list.get( i ).getV9() );
            vo.setV10( vo.getV10()+list.get( i ).getV10() );
            vo.setV11( vo.getV11()+list.get( i ).getV11() );
            vo.setV12( vo.getV12()+list.get( i ).getV12() );
            vo.setV13( vo.getV13()+list.get( i ).getV13() );
            vo.setV14( vo.getV14()+list.get( i ).getV14() );
        }
        vo.setTop10List(  wyAssetInfoMapper.Top10List( id ));


        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }





    @Override
    public ResponseVO cyWy(String index , Integer p , Integer size, Integer sort) {

        if(null == p ){
            p = 1;
        }

        int originp = p;
        if(null == size){
            size = 10;
        }
        p = (p - 1) * size;

        List<CyWyVo> data = wyAssetInfoMapper.cyWy(index, p, size , sort);

        int total = wyAssetInfoMapper.cyWy(index, 0, 99 , sort).size();


        PageVO<CyWyVo> vo = new PageVO<>();
        vo.setList( data );
        vo.setTotal(  total);
        vo.setP(originp);
        vo.setSize(size);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }
    @Override
    public ResponseVO basicInfo() {
        ZlBasicInfoVo vo=wyAssetInfoMapper.basicInfo();
        Double price=vo.getPrice();
        if (price==null){
            price= Double.valueOf( 0 );
        }
        vo.setAvgPrice( price/vo.getNum() );
        vo.setZlList( wyAssetInfoMapper.zlList() );

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);

    }

    @Override
    public ResponseVO zlAnalysis(CywyDTO dto) {
        String streetName = dto.getStreetName();
        if(null == streetName){
            dto.setStreetName("");
        }

        String wy_kind = dto.getWy_kind();
        if(null == wy_kind){
            dto.setWy_kind("");
        }

        String manUnit = dto.getManUnit();
        if (null == manUnit){
            dto.setManUnit("");
        }
        List<YdqList> list=wyAssetInfoMapper.wyAssetInfoCywyList1( "租赁社会物业",dto.getType(),dto.getIndex(),(dto.getP()-1)*dto.getSize(),dto.getSize(),
                dto.getManUnit() , dto.getWy_kind(),dto.getStreetName());

        PageVO<YdqList> vo = new PageVO<>();
        vo.setList( list );
        vo.setTotal(  wyAssetInfoMapper.wyAssetInfoCywyListTotal1("租赁社会物业",dto.getType(),dto.getIndex(),
                dto.getManUnit() ,dto.getWy_kind() ,dto.getStreetName()).size());
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO moreAnalysis() {
        MoreAnalysisVo vo=new MoreAnalysisVo();
        List<ZcVo> zbList=wyAssetInfoMapper.zlwyList();
        ZcVo zcVo=new ZcVo();
        zcVo.setUnitName( "其它" );
        int num=0;
        List<ZcVo> zbListTotal =wyAssetInfoMapper.zlwyListTotal();
        for (ZcVo zc:zbListTotal){
            num+=zc.getNum();
        }
        int zb=0;
        for (ZcVo zc1:zbList){
            zb+=zc1.getNum();
        }
        zcVo.setNum(num-zb);
        zbList.add(zcVo);

        vo.setZlPriceList( wyAssetInfoMapper.zlPriceList());

        vo.setZbList(zbList);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO moreUnit(String index) {
        if (index=="全部物业"|| "全部物业".equals(index)||index==null){
            index="";
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS,wyAssetInfoMapper.zlUnitList(index));
    }

    @Override
    public ResponseVO mapStreet(String index) {
        Double buildArea=wyAssetInfoMapper.wyAssetInfoBuildArea1("政府物业",index);

        List<WyAssetStreet> list= wyAssetInfoMapper.getStreetList1("政府物业",index);
        for (int i=0;i<list.size();i++){
            list.get( i ).setPercentage(new BigDecimal(Double.valueOf( list.get( i ).getArea())/buildArea).setScale(4, RoundingMode.UP).doubleValue());
        }

        return new ResponseVO(ErrorCodeEnum.SUCCESS,list);
    }

    @Override
    public ResponseVO cyStreet() {

        List<WyAssetStreet> list= wyAssetInfoMapper.cyStreet("政府物业","产业用房");
        Double cyStreetTatol=wyAssetInfoMapper.cyStreetTatol("政府物业","产业用房");
        for (int i=0;i<list.size();i++){
            list.get( i ).setPercentage(new BigDecimal(Double.valueOf( list.get( i ).getArea())/cyStreetTatol).setScale(4, RoundingMode.UP).doubleValue());
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS,list);
    }

    @Override
    public ResponseVO wylistDetails(String id) {

        return new ResponseVO(ErrorCodeEnum.SUCCESS,wyAssetInfoMapper.wylistDetails( id ));
    }


    @Override
    public ResponseVO houseCode() {
        HouseCodeNumVo vo=new HouseCodeNumVo();
        vo.setHaveCode( wyAssetInfoMapper.houseCode("已补充编码") );
        vo.setNoCode( wyAssetInfoMapper.houseCode("未补充编码") );
        vo.setIncompatible( wyAssetInfoMapper.houseCode("不符合编码条件") );
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }


    /**
     *
     * @return ResponseVO
     */
    @Override
    public ResponseVO houseCodeList(HouseCodeDTO dto){

        List<HouseCodeListVo> list=wyAssetInfoMapper.houseCodeList( dto.getType(),dto.getName(),dto.getAddress(),(dto.getP()-1)*dto.getSize(),dto.getSize());

        PageVO<HouseCodeListVo> vo = new PageVO<>();
        vo.setList( list );
        vo.setTotal(  wyAssetInfoMapper.houseCodeListTotal(dto.getType(),dto.getName(),dto.getAddress() ));
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());


        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    };

    /**
     *
     * @return ResponseVO
     */
    @Override
    public ResponseVO enterpriseSidewalk(int type){

        List<EnterpriseSidewalkVo> list=wyAssetInfoMapper.enterpriseSidewalk(type);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,list);
    };

    @Override
    public ResponseVO enterpriseList(EnterpriseDTO dto){

        Integer p = (dto.getP() - 1) * dto.getSize();
        Integer size = dto.getSize();

        String belongsType = dto.getBelongsType();
        String parkName = dto.getParkName();

        Integer startNumber = dto.getStartNumber();
        Integer endNumber = dto.getEndNumber();

        List<Map> list=wyAssetInfoMapper.enterpriseList( dto.getSort(),dto.getName(), parkName, p, size , startNumber , endNumber , belongsType , dto.getCompanyindustry() );

        PageVO<Object> vo = new PageVO<>();
        vo.setList( Collections.singletonList( list ) );
        int total = wyAssetInfoMapper.enterpriseListTotal(dto.getSort(), dto.getName(), dto.getParkName(), startNumber, endNumber, belongsType, dto.getCompanyindustry());
        vo.setTotal(total);
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        BigDecimal totalBuildArea  = new BigDecimal("0");
        List<Map> maps = this.wyAssetInfoMapper.enterpriseListTotalBuildArea(dto.getSort(), dto.getName(), dto.getParkName(), startNumber, endNumber, belongsType);
        Iterator<Map> iterator = maps.iterator();
        while (iterator.hasNext()){
            Map next = iterator.next();
            String buildArea = next.get("e1").toString();
            totalBuildArea = new BigDecimal(buildArea).add(totalBuildArea);
        }

        Map map = new HashMap();
        map.put("totalbuildarea" , totalBuildArea);

        Double totalDouble = this.wyAssetInfoMapper.selectTotalBuildArea( belongsType, dto.getManUnit(), dto.getWyFive());
        BigDecimal totalBuildArea2  = new BigDecimal(totalDouble);
        map.put("totalDouble" , totalDouble);
        map.put("percent" , totalBuildArea.divide(totalBuildArea2 , 4 , BigDecimal.ROUND_HALF_UP));


        vo.setOtherInfo(map);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO enterpriseGqList(EnterpriseDTO dto){

        Integer p = (dto.getP() - 1) * dto.getSize();
        Integer size = dto.getSize();

        String belongsType = dto.getBelongsType();
        String parkName = dto.getParkName();
        String companyName = dto.getCompanyName();
        if("大沙河".equals(companyName)){
            companyName = "深圳市大沙河投资建设有限公司";
            //parkName = "百旺信高科技工业园,汇通大厦,南山云科技大厦,创新大厦";
        }else if( "深汇通".equals(companyName)){
            companyName = "深圳市深汇通投资控股有限公司";
            //parkName = "红花岭工业园";
        }

        List<Map> list=wyAssetInfoMapper.enterpriseList3( dto.getSort(),dto.getName(), parkName, p, size ,companyName,dto.getCompanyindustry());
        List<Map> maps = wyAssetInfoMapper.enterpriseList3(dto.getSort(), dto.getName(), parkName, 0, 1000 ,companyName,dto.getCompanyindustry());

        PageVO<Object> vo = new PageVO<>();
        vo.setList( Collections.singletonList( list ) );
        vo.setTotal(maps.size());
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        BigDecimal total = new BigDecimal("0");
        Iterator<Map> iterator = maps.iterator();
        while (iterator.hasNext()){
            Map next = iterator.next();
            BigDecimal bigDecimal = new BigDecimal(next.get("buildArea").toString());
            total = total.add(bigDecimal);
        }
        Map tempMap = new HashMap();
        tempMap.put("totalbuildarea" , total.doubleValue());
        vo.setOtherInfo(tempMap);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO enterpriseTopWyName(EnterpriseDTO dto) {

        List<Map> list = this.wyAssetInfoMapper.enterpriseTopWyName();

        return null;
    }

    @Override
    public ResponseVO enterpriseTalent(EnterpriseDTO dto) {
        List<Map> list = this.wyAssetInfoMapper.enterpriseTalent(dto.getName(),(dto.getP()-1)*dto.getSize(),dto.getSize());

        PageVO<Map> vo = new PageVO<>();
        vo.setList( list);
        vo.setTotal(wyAssetInfoMapper.enterpriseTalentTotal(dto.getSort(),dto.getName(),dto.getParkName()));
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO newrzqy(CywyDTO dto) throws ParseException {

        Integer searchYear = dto.getYear();
        Integer p = dto.getP();
        Integer size = dto.getSize();
        String name = dto.getName();
        List<Map> list = this.wyAssetInfoMapper.selectNewRzList(name);

        if(null == list){
            list = new ArrayList<>();
        }

        List<Map> returnList = new ArrayList<>();

        Iterator<Map> iterator = list.iterator();
        while (iterator.hasNext()){
            Map next = iterator.next();
            String startrentdate = next.get("startrentdate").toString();
            String endrentdate = next.get("endrentdate").toString();
            String wyname = next.get("wyname").toString();
            String newWyName = wyname.replace(name, "");
            next.put("wyname" , newWyName);

            int year1 = Integer.parseInt(startrentdate.substring(0 , 4));
            int year2 = Integer.parseInt(endrentdate.substring(0 , 4));

            if(year1<= searchYear && year2 >=searchYear){
                returnList.add(next);
            }
        }

        int startIndex = (p - 1) * size ;
        int endIndex = startIndex + size > returnList.size() ? returnList.size() : startIndex + size;

        List<Map> newMapList = returnList.subList(startIndex,  endIndex);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( newMapList);
        vo.setTotal(returnList.size());
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO newrzqyfx(CywyDTO dto) {

        int[] yearArr = new int[]{2019,2020,2021};
        Map<Integer , Integer> map = new HashMap<>();

        String name = dto.getName();
        List<Map> list = this.wyAssetInfoMapper.selectNewRzList(name);
        Iterator<Map> iterator = list.iterator();
        while (iterator.hasNext()){
            Map next = iterator.next();
            String startrentdate = next.get("startrentdate").toString();
            String endrentdate = next.get("endrentdate").toString();

            int year1 = Integer.parseInt(startrentdate.substring(0 , 4));
            int year2 = Integer.parseInt(endrentdate.substring(0 , 4));

            for (int year : yearArr) {
                if(year1 <= year && year2 >= year){
                    Integer integer = map.get(year);
                    if(null == integer){
                        integer = 0;
                    }
                    integer+=1;
                    map.put(year , integer);
                }
            }
        }

        return new ResponseVO(ErrorCodeEnum.SUCCESS,map);
    }

    @Override
    public ResponseVO newdqht(CywyDTO dto) {

        //month1 month3 month6
        String type = dto.getType();
        String id = dto.getId();
        String topanme = dto.getName();
        String v1Id=wyAssetInfoMapper.V1Id(id);

        List returnList = new ArrayList();
        //List<String> list=wyAssetInfoMapper.ydq1Month( v1Id );

        if(null == type || StringUtils.isBlank(type)){
            //查询所有
            //for (int i=0;i<list.size();i++){
            //    List<Map> map = wyAssetInfoMapper.ydq1MonthMap(list.get(i), 0, 3 , topanme);
            //    if(null != map){
            //        returnList.addAll(map);
            //    }
            //    List<Map> map2 = wyAssetInfoMapper.ydq1MonthMap(list.get(i), 3, 6, topanme);
            //    if(null != map2){
            //        returnList.addAll(map2);
            //    }
            //    List<Map> map3 = wyAssetInfoMapper.ydq1MonthMap(list.get(i), 6, 12, topanme);
            //    if(null != map3){
            //        returnList.addAll(map3);
            //    }
            //}

            List<Map> map = wyAssetInfoMapper.ydq1MonthMap(null, 0, 12 , topanme);
            if(null != map){
                returnList.addAll(map);
            }
        }else{
            //for (int i=0;i<list.size();i++){
                if("month1".equals(type)){
                    List<Map> map = wyAssetInfoMapper.ydq1MonthMap(null, 0, 3 , topanme);
                    if(null != map){
                        returnList.addAll(map);
                    }
                }
                if("month2".equals(type)){
                    List<Map> map = wyAssetInfoMapper.ydq1MonthMap(null, 3, 6, topanme);
                    if(null != map){
                        returnList.addAll(map);
                    }
                }
                if("month3".equals(type)){
                    List<Map> map = wyAssetInfoMapper.ydq1MonthMap(null, 6, 12, topanme);
                    if(null != map){
                        returnList.addAll(map);
                    }
                }

            //}
        }

        Iterator iterator = returnList.iterator();
        while (iterator.hasNext()){
            Map next = (Map) iterator.next();
            String use_unit = next.get("use_unit").toString();
            String rent_date_start = next.get("rent_date_start2").toString();
            String rent_date_end = next.get("rent_date_end2").toString();
            //Integer year1 = Integer.parseInt(rent_date_start.substring(0, 4));
            //Integer year2 = Integer.parseInt(rent_date_end.substring(0, 4));
            //
            //int yearIndex1 = rent_date_start.indexOf("年");
            //int monthIndex1 = rent_date_start.indexOf("月");
            //int month1 = Integer.parseInt(rent_date_start.substring(yearIndex1 + 1, monthIndex1));
            //
            //int yearIndex2 = rent_date_end.indexOf("年");
            //int monthIndex2 = rent_date_end.indexOf("月");
            //int month2 = Integer.parseInt(rent_date_end.substring(yearIndex2 + 1, monthIndex2));
            //
            //String between = "";
            //if(year2 - year1 == 0){
            //    between = (month2 - month1) + "月";
            //}else if(year2 - year1 > 0){
            //    Integer tempYear = year2 - year1;
            //
            //    if(month1 > month2 ){
            //        tempYear--;
            //    }
            //    between += tempYear + "年";
            //
            //
            //    if(month1 > month2 ){
            //        between += (12 - month1 + month2) + "月";
            //    }else if(month1 < month2){
            //        between = tempYear + "年" + (month2 - month1) + "月";
            //    }
            //}
            next.put("between" , dayComparePrecise(rent_date_start , rent_date_end));



            //DateTime parse = DateUtil.parse(rent_date_end);
            //next.put("remain" , DateUtil.betweenDay(new Date() , parse ,true));

        }


        return new ResponseVO(ErrorCodeEnum.SUCCESS,returnList);
    }

    /**
     * 计算2个日期之间相差的  相差多少年月日
     * 比如：2011-02-02 到  2017-03-02 相差 6年，1个月，0天
     * @param fromDate YYYY-MM-DD
     * @param toDate YYYY-MM-DD
     * @return 年,月,日 例如 1,1,1
     */
    public static String dayComparePrecise(String fromDate, String toDate){

        Period period = Period.between(LocalDate.parse(fromDate), LocalDate.parse(toDate));

        StringBuffer sb = new StringBuffer();
        if(period.getYears() > 0){
            sb.append(period.getYears()).append("年");
        }

        if(period.getMonths() > 0){
            sb.append(period.getMonths()).append("月");
        }

        //if(period.getDays() > 0){
        //    sb.append(period.getDays()).append("日");
        //}

        return sb.toString();
    }

    @Override
    public ResponseVO newyqhy(CywyDTO dto) {

        String name = dto.getName();
        String industry = dto.getIndustry();

        List<Map> returnList = this.wyAssetInfoMapper.selectWyIndustry(name, industry);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,returnList);
    }

    @Override
    public ResponseVO newwyentry(CywyDTO dto) {


        Integer p = dto.getP();
        Integer size = dto.getSize();

        int startIndex = (p - 1) * size ;
        int endIndex = size;

        List<Map> newwyentrylist = this.wyAssetInfoMapper.newwyentrylist(startIndex, endIndex);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( newwyentrylist);
        vo.setTotal(this.wyAssetInfoMapper.newwyentrylistTotal());
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        Map map = this.wyAssetInfoMapper.selectTotalWyEntryInfo();
        vo.setOtherInfo(map);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO newwyequity(CywyDTO dto) {

        Integer p = dto.getP();
        Integer size = dto.getSize();

        int startIndex = (p - 1) * size ;
        int endIndex =  size;

        List<Map> newwyentrylist = this.wyAssetInfoMapper.newwyequitylist(startIndex, endIndex);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( newwyentrylist);
        vo.setTotal(this.wyAssetInfoMapper.newwyequitylistTotal());
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        Map map = this.wyAssetInfoMapper.selectToatlWyEquityInfo();
        vo.setOtherInfo(map);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public int wyentrynamenumber(int type) {

        //type 1 入账数  type 2 未入账数
        //获取所有入账的物业parentids
        List<String> wyentryname = this.wyAssetInfoMapper.wyentryname();
        if(type == 1){
            return wyentryname.size();
        }

        Set<String> hasEntrySet = new HashSet<>();

        Iterator<String> iterator = wyentryname.iterator();
        while (iterator.hasNext()){
            String next = iterator.next();

            String substring = next.substring(0, 1);
            if(!",".equals(substring)){
                next = "," + next;
            }

            String newNext = next.replace(",,", ",");

            String[] split = newNext.split(",");
            if(split.length > 3){
                hasEntrySet.add(split[3]);
            }
        }

        List<String> allTopWyIdList = this.wyAssetInfoMapper.selectAllTopWyId();
        Set<String> allSet = new HashSet<>(allTopWyIdList);

        allSet.retainAll(hasEntrySet);

        return allSet.size();
    }

    @Override
    public int wyequitynumber(int type) {

        //type 1 入账数  type 2 未入账数
        List<String> wyequityname = this.wyAssetInfoMapper.wyequityname();
        if(type == 1){
            return wyequityname.size();
        }

        Set<String> hasEntrySet = new HashSet<>();

        Iterator<String> iterator = wyequityname.iterator();
        while (iterator.hasNext()){
            String next = iterator.next();

            String substring = next.substring(0, 1);
            if(!",".equals(substring)){
                next = "," + next;
            }

            String newNext = next.replace(",,", ",");

            String[] split = newNext.split(",");
            if(split.length > 3){
                hasEntrySet.add(split[3]);
            }
        }

        List<String> allTopWyIdList = this.wyAssetInfoMapper.selectAllTopWyId();
        Set<String> allSet = new HashSet<>(allTopWyIdList);

        allSet.retainAll(hasEntrySet);

        return allSet.size();
    }

    @Override
    public ResponseVO allLongContract(CywyDTO dto) {

        Integer p = dto.getP();
        Integer size = dto.getSize();
        String belongsType = dto.getBelongsType();
        if(StringUtils.isBlank(belongsType)){
            belongsType = "zhengfu,guoqi";
        }else if("政府物业".equals(belongsType)){
            belongsType = "zhengfu";
        }else if("国企物业".equals(belongsType)){
            belongsType = "guoqi";
        }

        int startIndex = (p - 1) * size ;
        int endIndex =  size;

        //1:5年到10年  2：10到15年  3 ：大于15年
        String type = dto.getType();
        int start = 0, end = 0;
        if("1".equals(type)){
            start = 5;
            end = 10;
        }else if("2".equals(type)){
            start = 10;
            end = 15;
        }else if("3".equals(type)){
            start = 15;
            end = 99;
        }

        List<Map> maps = this.wyAssetInfoMapper.selectLongWyContractInfo(startIndex, endIndex , start , end , belongsType);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( maps);
        vo.setTotal(this.wyAssetInfoMapper.selectLongWyContractInfoTotal(start , end , belongsType));
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO allNotCyLongContract(CywyDTO dto) {
        Integer p = dto.getP();
        Integer size = dto.getSize();

        int startIndex = (p - 1) * size ;
        int endIndex =  size;

        List<Map> maps = this.wyAssetInfoMapper.selectNotCyLongWyContractInfo(startIndex, endIndex , 5 , 99);

        Iterator<Map> iterator = maps.iterator();
        while (iterator.hasNext()){
            Map next = iterator.next();
            String wyname = next.get("wyname").toString();

            String manUnit = this.wyAssetInfoMapper.selectManUnitByWyName(wyname);
            next.put("manunit" , manUnit);
        }

        PageVO<Map> vo = new PageVO<>();
        vo.setList( maps);
        Integer total = this.wyAssetInfoMapper.selectNotCyLongWyContractInfoTotal(5, 99);
        //if(null == total){
        //    total = 0;
        //}
        vo.setTotal(total);
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        //5至10 10至15 大于15
        Map tempmap = new HashMap();
        //Integer value1 = this.wyAssetInfoMapper.selectNotCyLongWyContractInfoTotal(5, 10);
        //if(null == value1){
        //    value1 = 0;
        //}
        //Integer value2 = this.wyAssetInfoMapper.selectNotCyLongWyContractInfoTotal(10, 15);
        //if(null == value2){
        //    value2 = 0;
        //}
        tempmap.put("five" , 2);
        tempmap.put("ten" , 2);
        tempmap.put("fifteen" , 6);

        vo.setOtherInfo(tempmap);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO alldelayContract(CywyDTO dto) {
        Integer p = dto.getP();
        Integer size = dto.getSize();

        int startIndex = (p - 1) * size ;
        int endIndex =  size;

        //1: 3个月 2: 3到 6个月 3：6到12个月
        String type = dto.getType();
        int start = 0, end = 0;
        if("1".equals(type)){
            start = 1;
            end = 3;
        }else if("2".equals(type)){
            start = 3;
            end = 6;
        }else if("3".equals(type)){
            start = 6;
            end = 12;
        }

        List<Map> maps = this.wyAssetInfoMapper.selectAllWyContractInfo(startIndex, endIndex , start , end);

        Iterator<Map> iterator = maps.iterator();
        while (iterator.hasNext()){
            Map next = iterator.next();
            String wyname = next.get("wyname").toString();

            String manUnit = this.wyAssetInfoMapper.selectManUnitByWyName(wyname);
            next.put("manunit" , manUnit);
        }

        PageVO<Map> vo = new PageVO<>();
        vo.setList( maps);
        int total = this.wyAssetInfoMapper.selectAllWyContractInfoTotal(start, end);
        vo.setTotal(total);
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        //3个月 3到 6个月 6到12个月
        Map tempmap = new HashMap();
        int value1 = this.wyAssetInfoMapper.selectAllWyContractInfoTotal(0, 3);
        int value2 = this.wyAssetInfoMapper.selectAllWyContractInfoTotal(3, 6);
        int value3 = total - value1 - value2;
        tempmap.put("three" , value1);
        tempmap.put("six" , value2);
        tempmap.put("twelve" , value3);

        vo.setOtherInfo(tempmap);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO allNotCyDelayContract(CywyDTO dto) {
        Integer p = dto.getP();
        Integer size = dto.getSize();

        int startIndex = (p - 1) * size ;
        int endIndex =  size;

        List<Map> maps = this.wyAssetInfoMapper.selectNotCyAllWyContractInfo(startIndex, endIndex , 0 , 12);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( maps);
        int total = this.wyAssetInfoMapper.selectNotCyAllWyContractInfoTotal(0, 12);
        vo.setTotal(total);
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        Map tempmap = new HashMap();
        //int value1 = this.wyAssetInfoMapper.selectNotCyAllWyContractInfoTotal(0, 3);
        //int value2 = this.wyAssetInfoMapper.selectNotCyAllWyContractInfoTotal(3, 6);
        //int value3 = total - value1 - value2;
        tempmap.put("three" , 13);
        tempmap.put("six" , 9);
        tempmap.put("twelve" , 12);

        vo.setOtherInfo(tempmap);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO emptyNotCyList(CywyDTO dto) {

        Integer p = dto.getP();
        Integer size = dto.getSize();

        int startIndex = (p - 1) * size ;
        int endIndex =  size;

        List<Map> maps = this.wyAssetInfoMapper.selectNotCyEmptyWyInWyAssetInfoV2(dto.getType() , startIndex, endIndex );

        PageVO<Map> vo = new PageVO<>();
        vo.setList( maps);
        vo.setTotal(this.wyAssetInfoMapper.selectNotCyEmptyWyInWyAssetInfoV2Total(dto.getType()));
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }



    @Override
    public ResponseVO selectChildrenByTopWyName(CywyDTO dto) {

        List<Map> maps = this.wyAssetInfoMapper.selectChildrenByTopWyName(dto.getName());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,maps);
    }

    @Override
    public ResponseVO selectWySimpleNameByAssetType(CywyDTO dto) {

        String type = dto.getType();

        List<Map> maps = this.wyAssetInfoMapper.selectWySimpleInfoByAssetType(type);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,maps);
    }

    @Override
    public ResponseVO selectAllWyInfo(CywyDTO dto) {

        Integer p = dto.getP();
        Integer size = dto.getSize();

        int startIndex = (p - 1) * size ;
        int endIndex =  size;

        String name = dto.getName();
        String belongType = dto.getBelongsType();
        String wyFive = dto.getWyFive();
        String streetName = dto.getStreetName();
        if(null == streetName){
            streetName = "";
        }
        Integer sort = dto.getSort();
        String companyName = dto.getCompanyName();
        if(null == companyName){
            companyName = "";
        }
        if(null == sort ){
            sort = 0;
        }

        if(StringUtils.isBlank(belongType)){
            belongType = "政府物业,国企物业";
        }

        if("大沙河".equals(companyName) ){
            //endIndex = 100;
            dto.setManUnit("深圳市大沙河投资建设有限公司");
        }else if("深汇通".equals(companyName)){
            dto.setManUnit("深圳市深汇通投资控股有限公司");
            //endIndex = 100;
        }

        //查空置
        if(null != dto.getStartNumber() && dto.getStartNumber() > 0){
            endIndex = 2000;

        }

        //8小类
        String wyKind = dto.getWy_kind();
        List<Map> returnWyInfoList = this.wyAssetInfoMapper.selectWyInfo(name, belongType, wyFive, startIndex, endIndex,streetName,sort , wyKind , dto.getBuildAreaStart() , dto.getBuildAreaEnd() , dto.getManUnit());

        //String str= "艺晶工业园,阳光工业园,河源高新区共建产业园起步园,河源高新区共建产业园（二期）,南山智造深汕高新产业园,南山智造（红花岭基地）城市更新项目一期";

        //Iterator<Map> returnWyInfoListIterator1 = returnWyInfoList.iterator();
        //while (returnWyInfoListIterator1.hasNext()){
        //    Map next = returnWyInfoListIterator1.next();
        //    String name1 = next.get("name").toString();
        //
        //    if(companyName.equals("大沙河")){
        //        //删除str中的物业
        //        if(str.indexOf(name1) > -1){
        //            returnWyInfoListIterator1.remove();
        //        }
        //    }else if(companyName.equals("深汇通")){
        //        //删除str外的物业
        //        if(str.indexOf(name1) < 0){
        //            returnWyInfoListIterator1.remove();
        //        }
        //    }
        //}

        //TODO  空置筛选
        Map<String , String[]> map = new HashMap<>();
        //可出租 已出租
        map.put("百旺信高科技工业园",new String[]{"222533.1","222533.1"});
        map.put("创新大厦",new String[]{"16623.34","14578.27"});
        map.put("金桃园大厦裙楼",new String[]{"3921.58","3921.58"});
        map.put("侨城一号广场",new String[]{"8730.56","6673.88"});
        map.put("绿景美景广场1栋",new String[]{"2481.78","1599.97"});
        map.put("南山智园（二期）",new String[]{"164056.05","55407.41"});
        map.put("阳光科创中心一期A座",new String[]{"7624.12","7624.12"});
        map.put("创意大厦",new String[]{"2550.05","2550.05"});
        map.put("侨城坊8号楼",new String[]{"9975.4","9975.4"});
        map.put("兆邦基科技中心B座8层",new String[]{"1800.64","0"});
        map.put("汇通大厦",new String[]{"13103","13103"});
        map.put("南山智园（三期）崇文园区",new String[]{"179586.47","134747.44"});
        map.put("水木一方大厦",new String[]{"1501.28","0"});
        map.put("南山智园（一期）",new String[]{"309319.16","294734.36"});
        map.put("塘朗城广场（西区）A座",new String[]{"48973.54","45190.08"});
        map.put("南山金融大厦",new String[]{"30039.3","17899.98"});
        map.put("方大城（方大广场）",new String[]{"3065.94","3065.94"});
        map.put("百兴科技大厦配套创新型产业用房",new String[]{"811.23","0"});
        map.put("源政创新大楼",new String[]{"2493.75","0"});
        map.put("南山云科技大厦",new String[]{"149881.68","18399"});
        map.put("南山智造（红花岭基地）二期",new String[]{"95320","72072"});

        Iterator<Map> returnWyInfoListIterator2 = returnWyInfoList.iterator();
        while (returnWyInfoListIterator2.hasNext()){
            Map next = returnWyInfoListIterator2.next();
            String name1 = next.get("name").toString();
            if(map.containsKey(name1)){
                String[] strings = map.get(name1);
                BigDecimal canRentArea = new BigDecimal(strings[0]);
                BigDecimal useArea = new BigDecimal(strings[1]);
                BigDecimal remainArea = canRentArea.subtract(useArea);
                //可出租面积
                next.put("canRentArea" , canRentArea);
                //已出租面积
                next.put("useArea" , useArea);
                //空置面积
                next.put("remainArea" , remainArea);
                //空置率
                next.put("remainPer" , remainArea.divide(canRentArea , 2 , BigDecimal.ROUND_HALF_UP));
            }
        }

        if(null != dto.getStartNumber() && dto.getStartNumber() > 0){

            Iterator<Map> returnWyInfoListIterator3 = returnWyInfoList.iterator();
            while (returnWyInfoListIterator3.hasNext()){
                Map next = returnWyInfoListIterator3.next();
                Object remainArea = next.get("remainArea");
                if(null == remainArea){
                    returnWyInfoListIterator3.remove();
                    continue;
                }
                Double value = Double.valueOf(remainArea.toString());
                if(value < dto.getStartNumber()){
                    returnWyInfoListIterator3.remove();
                }
                if(null != dto.getEndNumber() && value > dto.getEndNumber()){
                    returnWyInfoListIterator3.remove();
                }
            }
        }

        //计算总数
        int total = calTotalNumber(dto, endIndex, name, belongType, wyFive, streetName, sort, companyName, wyKind, null, map);

        //计算总面积
        Double value = calTotalBuildArea(dto, name, belongType, wyFive, streetName, companyName, null, map);

        //如果是搜索空置面积 ，需要走此逻辑
        if(null != dto.getStartNumber() && dto.getStartNumber() > 0){
            int p1 = (dto.getP() - 1) * dto.getSize();
            int size1 = dto.getSize();
            if(returnWyInfoList.size() > size1 && returnWyInfoList.size() <= p1){
                returnWyInfoList = returnWyInfoList.subList(p1 , size1);
            }
        }


        PageVO<Map> vo = new PageVO<>();
        vo.setList( returnWyInfoList);

        vo.setTotal(total);
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        Map otherMap = new HashMap();
        otherMap.put("totalbuildarea" , value);

        if(null != dto.getWyFive() && StringUtils.isNotBlank(dto.getWyFive())){
            List<Map> map1 = this.wyAssetInfoMapper.selectTypeNumberByBelongType(dto.getWyFive());
            Iterator<Map> iterator = map1.iterator();
            Map tempMap = new HashMap();
            while (iterator.hasNext()){
                Map next = iterator.next();
                tempMap.put(next.get("belong_type") , next.get("number"));
            }
            otherMap.put("belongTypeMap" , tempMap);
        }

        vo.setOtherInfo(otherMap);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    private Double calTotalBuildArea(CywyDTO dto, String name, String belongType, String wyFive, String streetName, String companyName, String str, Map<String, String[]> map) {
        dto.setBelongsType(belongType);

        List<Map> calTotalBuildAreaList = this.wyAssetInfoMapper.selectWyInfoTotalBuildArea(dto);
        //Iterator<Map> calTotalBuildAreaListIterator1 = calTotalBuildAreaList.iterator();
        //while (calTotalBuildAreaListIterator1.hasNext()){
        //    Map next = calTotalBuildAreaListIterator1.next();
        //    String name1 = next.get("name").toString();
        //
        //    if(companyName.equals("大沙河")){
        //        //删除str中的物业
        //        if(str.indexOf(name1) > -1){
        //            calTotalBuildAreaListIterator1.remove();
        //        }
        //    }else if(companyName.equals("深汇通")){
        //        //删除str外的物业
        //        if(str.indexOf(name1) < 0){
        //            calTotalBuildAreaListIterator1.remove();
        //        }
        //    }
        //}

        if(null != dto.getStartNumber() && dto.getStartNumber() > 0){
            Iterator<Map> calTotalBuildAreaListIterator2 = calTotalBuildAreaList.iterator();
            while (calTotalBuildAreaListIterator2.hasNext()){
                Map next = calTotalBuildAreaListIterator2.next();
                String name1 = next.get("name").toString();
                if(map.containsKey(name1)){
                    String[] strings = map.get(name1);
                    BigDecimal canRentArea = new BigDecimal(strings[0]);
                    BigDecimal useArea = new BigDecimal(strings[1]);
                    BigDecimal remainArea = canRentArea.subtract(useArea);
                    //可出租面积
                    next.put("canRentArea" , canRentArea);
                    //已出租面积
                    next.put("useArea" , useArea);
                    //空置面积
                    next.put("remainArea" , remainArea);
                    //空置率
                    next.put("remainPer" , remainArea.divide(canRentArea , 2 , BigDecimal.ROUND_HALF_UP));
                }
            }

            Iterator<Map> calTotalBuildAreaListIterator3 = calTotalBuildAreaList.iterator();
            while (calTotalBuildAreaListIterator3.hasNext()){
                Map next = calTotalBuildAreaListIterator3.next();
                Object remainArea = next.get("remainArea");
                if(null == remainArea){
                    calTotalBuildAreaListIterator3.remove();
                    continue;
                }
                Double remainAreaTemp = Double.valueOf(remainArea.toString());
                if(remainAreaTemp < dto.getStartNumber()){
                    calTotalBuildAreaListIterator3.remove();
                }
                if(null != dto.getEndNumber() && remainAreaTemp > dto.getEndNumber()){
                    calTotalBuildAreaListIterator3.remove();
                }
            }
        }

        Double value = 0D;
        Iterator<Map> calTotalBuildAreaListIterator4 = calTotalBuildAreaList.iterator();
        while (calTotalBuildAreaListIterator4.hasNext()){
            Map next = calTotalBuildAreaListIterator4.next();
            Object build_area = next.get("build_area");
            if(null == build_area || StringUtils.isBlank(build_area.toString())){
                continue;
            }
            value = new BigDecimal(build_area.toString()).add(new BigDecimal(value)).doubleValue();
        }
        return value;
    }

    private int calTotalNumber(CywyDTO dto, int endIndex, String name, String belongType, String wyFive, String streetName, Integer sort, String companyName, String wyKind, String str, Map<String, String[]> map) {
        List<Map> calTotalNumberList = this.wyAssetInfoMapper.selectWyInfo(name, belongType, wyFive, -1, endIndex, streetName, sort, wyKind, dto.getBuildAreaStart() , dto.getBuildAreaEnd(), dto.getManUnit());
        //Iterator<Map> calTotalNumberListIterator1 = calTotalNumberList.iterator();
        //while (calTotalNumberListIterator1.hasNext()){
        //    Map next = calTotalNumberListIterator1.next();
        //    String name1 = next.get("name").toString();
        //
        //    if(companyName.equals("大沙河")){
        //        //删除str中的物业
        //        if(str.indexOf(name1) > -1){
        //            calTotalNumberListIterator1.remove();
        //        }
        //    }else if(companyName.equals("深汇通")){
        //        //删除str外的物业
        //        if(str.indexOf(name1) < 0){
        //            calTotalNumberListIterator1.remove();
        //        }
        //    }
        //}

        if(null != dto.getStartNumber() && dto.getStartNumber() > 0){
            Iterator<Map> calTotalNumberListIterator2 = calTotalNumberList.iterator();
            while (calTotalNumberListIterator2.hasNext()){
                Map next = calTotalNumberListIterator2.next();
                String name1 = next.get("name").toString();
                if(map.containsKey(name1)){
                    String[] strings = map.get(name1);
                    BigDecimal canRentArea = new BigDecimal(strings[0]);
                    BigDecimal useArea = new BigDecimal(strings[1]);
                    BigDecimal remainArea = canRentArea.subtract(useArea);
                    //可出租面积
                    next.put("canRentArea" , canRentArea);
                    //已出租面积
                    next.put("useArea" , useArea);
                    //空置面积
                    next.put("remainArea" , remainArea);
                    //空置率
                    next.put("remainPer" , remainArea.divide(canRentArea , 2 , BigDecimal.ROUND_HALF_UP));
                }
            }

            Iterator<Map> calTotalNumberListIterator3 = calTotalNumberList.iterator();
            while (calTotalNumberListIterator3.hasNext()){
                Map next = calTotalNumberListIterator3.next();
                Object remainArea = next.get("remainArea");
                if(null == remainArea){
                    calTotalNumberListIterator3.remove();
                    continue;
                }
                Double value = Double.valueOf(remainArea.toString());
                if(value < dto.getStartNumber()){
                    calTotalNumberListIterator3.remove();
                }
                if(null != dto.getEndNumber() && value > dto.getEndNumber()){
                    calTotalNumberListIterator3.remove();
                }
            }
        }
        int total = null == calTotalNumberList ? 0 : calTotalNumberList.size();
        return total;
    }

    @Override
    public ResponseVO notEntryWyInfo(CywyDTO dto) {

        Integer p = dto.getP();
        Integer size = dto.getSize();

        int startIndex = (p - 1) * size ;
        int endIndex =  size;

        String name = dto.getName();
        String belongType = dto.getBelongsType();
        String wyFive = dto.getWyFive();

        List<Map> maps = this.wyAssetInfoMapper.selectNotEntryWyInfo(name, belongType, wyFive, startIndex, endIndex);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( maps);
        vo.setTotal(this.wyAssetInfoMapper.selectNotEntryWyInfoTotal(name, belongType, wyFive));
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        Map map = new HashMap();
        map.put("totalbuildarea" , this.wyAssetInfoMapper.selectNotEntryWyInfoTotalBuildArea(name, belongType, wyFive));
        vo.setOtherInfo(map);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO notCertHasWyInfo(CywyDTO dto) {
        Integer p = dto.getP();
        Integer size = dto.getSize();

        int startIndex = (p - 1) * size ;
        int endIndex =  size;

        String name = dto.getName();
        String belongType = dto.getBelongsType();
        String wyFive = dto.getWyFive();

        List<Map> maps = this.wyAssetInfoMapper.selectNoCertHasWyInfo(name, belongType, wyFive, startIndex, endIndex);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( maps);
        vo.setTotal(this.wyAssetInfoMapper.selectNoCertHasWyInfoTotal(name, belongType, wyFive));
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        Map map = new HashMap();
        map.put("totalbuildarea" , this.wyAssetInfoMapper.selectNoCertHasWyInfoTotalBuildArea(name, belongType, wyFive));
        vo.setOtherInfo(map);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO showLDInfo(CywyDTO dto) {

        String name = dto.getName();

        List<Map> maps = this.wyAssetInfoMapper.selectChildrenByTopWyName(name);

        boolean flag = false;

        Iterator<Map> iterator = maps.iterator();
        while (iterator.hasNext()){
            Map next = iterator.next();
            String wyName = next.get("text").toString();
            next.put("name" , wyName);

            if(wyName.contains("地下室") || wyName.contains("其它")){
                iterator.remove();
                continue;
            }


            if(flag){
                continue;
            }

            List<Map> emptylist = this.wyAssetInfoMapper.selectWyEmptyByLD(wyName, 0, 10);
            int emptyByLD = this.wyAssetInfoMapper.selectWyEmptyByLDTotal(wyName);
            PageVO<Map> vo = new PageVO<>();
            vo.setList( emptylist);
            vo.setTotal(emptyByLD);
            vo.setP(1);
            vo.setSize(10);
            next.put("emptyInfo" , vo);

            List<Map> enterList = this.wyAssetInfoMapper.selectWyEnterCompany(wyName, 0, 10);
            int enterTotal = this.wyAssetInfoMapper.selectWyEnterCompanyTotal(wyName);
            PageVO<Map> vo1 = new PageVO<>();
            vo1.setList( enterList);
            vo1.setTotal(enterTotal);
            vo1.setP(1);
            vo1.setSize(10);
            next.put("enterInfo" , vo1);

            //只显示第一个楼栋的空置和入驻情况，后面的点击再请求接口
            flag = true;
        }

        return new ResponseVO(ErrorCodeEnum.SUCCESS,maps);
    }

    @Override
    public ResponseVO showLDEntryAndEmptyInfo(CywyDTO dto) {
        String name = dto.getName();

        Map returnMap = new HashMap();

        List<Map> emptylist = this.wyAssetInfoMapper.selectWyEmptyByLD(name, 0, 10);
        int emptyByLD = this.wyAssetInfoMapper.selectWyEmptyByLDTotal(name);
        PageVO<Map> vo = new PageVO<>();
        vo.setList( emptylist);
        vo.setTotal(emptyByLD);
        vo.setP(1);
        vo.setSize(10);
        returnMap.put("emptyInfo" , vo);

        List<Map> enterList = this.wyAssetInfoMapper.selectWyEnterCompany(name, 0, 10);
        int enterTotal = this.wyAssetInfoMapper.selectWyEnterCompanyTotal(name);
        PageVO<Map> vo1 = new PageVO<>();
        vo1.setList( enterList);
        vo1.setTotal(enterTotal);
        vo1.setP(1);
        vo1.setSize(10);
        returnMap.put("enterInfo" , vo1);

        returnMap.put("text" , name);
        returnMap.put("name" , name);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,returnMap);
    }

    @Override
    public ResponseVO showLDEnterInfo(CywyDTO dto) {

        Integer p = dto.getP();
        Integer size = dto.getSize();

        int startIndex = (p - 1) * size ;
        int endIndex =  size;

        String name = dto.getName();

        List<Map> maps = this.wyAssetInfoMapper.selectWyEnterCompany(name,  startIndex, endIndex);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( maps);
        vo.setTotal(this.wyAssetInfoMapper.selectWyEnterCompanyTotal(name));
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO showLDEmptyInfo(CywyDTO dto) {
        Integer p = dto.getP();
        Integer size = dto.getSize();

        int startIndex = (p - 1) * size ;
        int endIndex =  size;

        String name = dto.getName();

        List<Map> maps = this.wyAssetInfoMapper.selectWyEmptyByLD(name,  startIndex, endIndex);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( maps);
        vo.setTotal(this.wyAssetInfoMapper.selectWyEmptyByLDTotal(name));
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO selectWyDetailInfoGuoqi(CywyDTO dto) {

        //Integer p = dto.getP();
        //Integer size = dto.getSize();
        //int startIndex = (p - 1) * size;
        //dto.setP(startIndex);

        Double buildarea = this.wyAssetInfoMapper.selectGuoqiBuildAreaByUseStatus(dto);

        List<Map> groupByUseStatusMapList = this.wyAssetInfoMapper.selectGuoqiBuildAreaGroupByUseStatus(dto);

        Map returnMap = new HashMap();
        returnMap.put("buildarea" , buildarea);
        returnMap.put("managearea" , buildarea);
        //自用
        returnMap.put("useselfarea" , 0);
        //调配
        returnMap.put("changearea" , 0);
        //出租
        returnMap.put("rentarea" , 0);
        //空置
        returnMap.put("emptyarea" , 0);
        //物业地址
        returnMap.put("address" , this.wyAssetInfoMapper.selectGuoqiAddressByWyName(dto.getName()));

        Iterator<Map> iterator = groupByUseStatusMapList.iterator();
        while (iterator.hasNext()){
            Map next = iterator.next();
            String usestatus = next.get("usestatus").toString();
            String key = "";
            switch (usestatus){
                case "出租":
                    key = "rentarea";
                    break;
                case "调配":
                    key = "changearea";
                    break;
                case "自用":
                    key = "useselfarea";
                    break;
                case "空置":
                    key = "emptyarea";
                    break;
                default:
                    break;
            }

            if(StringUtils.isNotBlank(key)){
                returnMap.put(key , next.get("totalusearea"));
            }
        }

        return new ResponseVO(ErrorCodeEnum.SUCCESS,returnMap);
    }

    @Override
    public ResponseVO selectWyInfoGuoqiByUseStatus(CywyDTO dto) {

        Integer p = dto.getP();
        Integer size = dto.getSize();
        int startIndex = (p - 1) * size;
        dto.setP(startIndex);

        List<Map> maps = this.wyAssetInfoMapper.selectGuoqiWyInfoByUseStatus(dto);

        Integer total = this.wyAssetInfoMapper.selectGuoqiWyInfoByUseStatusTotal(dto);


        PageVO<Map> vo = new PageVO<>();
        vo.setList( maps);
        vo.setTotal(total);
        vo.setP(p);
        vo.setSize(dto.getSize());

        Double totalbuildarea = this.wyAssetInfoMapper.selectGuoqiBuildAreaByUseStatus(dto);
        Map temp = new HashMap();
        temp.put("totalbuildarea" ,totalbuildarea);
        vo.setOtherInfo(temp);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO enterpriseByBelongType(EnterpriseDTO dto) {

        Integer p = (dto.getP() - 1) * dto.getSize();
        Integer size = dto.getSize();

        String belongsType = dto.getBelongsType();
        String parkName = dto.getParkName();

        Integer startNumber = dto.getStartNumber();
        Integer endNumber = dto.getEndNumber();

        String manUnit = dto.getManUnit();
        if(null != manUnit) {
            if("大沙河".equals(manUnit)){
                manUnit = "深圳市大沙河投资建设有限公司";
            }else if("深汇通".equals(manUnit)){
                manUnit = "深圳市深汇通投资控股有限公司";
            }
            dto.setManUnit(manUnit);
        }

        List<Map> list=wyAssetInfoMapper.enterpriseListByBelongType( dto.getSort(),dto.getName(), parkName, p, size , startNumber , endNumber , belongsType , dto.getCompanyindustry() , dto.getManUnit() , dto.getWyFive() );

        PageVO<Object> vo = new PageVO<>();
        vo.setList( Collections.singletonList( list ) );
        int total = wyAssetInfoMapper.enterpriseListByBelongTypeTotal(dto.getSort(), dto.getName(), dto.getParkName(), startNumber, endNumber, belongsType, dto.getCompanyindustry(), dto.getManUnit(), dto.getWyFive());
        vo.setTotal(total);
        vo.setP(dto.getP());
        vo.setSize(dto.getSize());

        //总面积
        BigDecimal currentTotalBuildArea  = new BigDecimal("0");
        List<Map> maps = this.wyAssetInfoMapper.enterpriseListByBelongTypeTotalBuildArea(dto.getSort(), dto.getName(), dto.getParkName(), startNumber, endNumber, belongsType, dto.getCompanyindustry(), dto.getManUnit(), dto.getWyFive());
        Iterator<Map> iterator = maps.iterator();
        while (iterator.hasNext()){
            Map next = iterator.next();
            String buildArea = next.get("e1").toString();
            currentTotalBuildArea = new BigDecimal(buildArea).add(currentTotalBuildArea);
        }

        //面积占比
        Double totalDouble = this.wyAssetInfoMapper.selectTotalBuildArea( belongsType, dto.getManUnit(), dto.getWyFive());
        BigDecimal totalBuildArea  = new BigDecimal(totalDouble);

        Map map = new HashMap();
        map.put("totalbuildarea" , currentTotalBuildArea);
        map.put("totalarea" , totalBuildArea);
        map.put("percent" , currentTotalBuildArea.divide(totalBuildArea , 4 , BigDecimal.ROUND_HALF_UP));
        vo.setOtherInfo(map);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO insert(ZfWyAssetInfo entity) {
        return null;
    }

    @Override
    public ResponseVO deleteById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO deleteByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO delete(Wrapper<ZfWyAssetInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO deleteBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO updateById(ZfWyAssetInfo entity) {
        return null;
    }

    @Override
    public ResponseVO update(ZfWyAssetInfo entity, Wrapper<ZfWyAssetInfo> updateWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO selectBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO selectOne(Wrapper<ZfWyAssetInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectCount(Wrapper<ZfWyAssetInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectList(Wrapper<ZfWyAssetInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectMaps(Wrapper<ZfWyAssetInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectObjs(Wrapper<ZfWyAssetInfo> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<ZfWyAssetInfo>> ResponseVO selectPage(P page, Wrapper<ZfWyAssetInfo> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<Map<String, Object>>> ResponseVO selectMapsPage(P page, Wrapper<ZfWyAssetInfo> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO rewardList(String index, Integer p, Integer size) {

        Integer page = (p - 1) * size;

        List<Map> data = this.wyAssetInfoMapper.newCompanyRewardList(index , page , size);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( data );
        vo.setTotal(  wyAssetInfoMapper.countNewCompanyRewardNumber(index));
        vo.setP(p);
        vo.setSize(size);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO certificateList(String index, Integer p, Integer size) {
        Integer page = (p - 1) * size;

        List<Map> data = this.wyAssetInfoMapper.newCompanyCertificateList(index , page , size);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( data );
        vo.setTotal(  wyAssetInfoMapper.countNewCompanyCertificateNumber(index));
        vo.setP(p);
        vo.setSize(size);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO patentList(String index, Integer p, Integer size) {
        Integer page = (p - 1) * size;

        List<Map> data = this.wyAssetInfoMapper.newCompanyPatentList(index , page , size);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( data );
        vo.setTotal(  wyAssetInfoMapper.countNewCompanyPatentNumber(index));
        vo.setP(p);
        vo.setSize(size);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO softWareCopyRightList(String index, Integer p, Integer size) {
        Integer page = (p - 1) * size;

        List<Map> data = this.wyAssetInfoMapper.newCompanySoftwareCopyrightList(index , page , size);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( data );
        vo.setTotal(  wyAssetInfoMapper.countCompanySoftwareCopyrightNumber(index));
        vo.setP(p);
        vo.setSize(size);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }

    @Override
    public ResponseVO subletLeaseList(String index, Integer p, Integer size) {
        Integer page = (p - 1) * size;

        List<Map> data = this.wyAssetInfoMapper.selectNewCompanySubletLeaseList(index , page , size);

        PageVO<Map> vo = new PageVO<>();
        vo.setList( data );
        vo.setTotal(  wyAssetInfoMapper.countNewCompanySubletLeaseNumber(index));
        vo.setP(p);
        vo.setSize(size);

        return new ResponseVO(ErrorCodeEnum.SUCCESS,vo);
    }
}
