package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.TimeUtils;
import com.zbkc.model.po.SysParamConfig;
import com.zbkc.mapper.SysParamConfigMapper;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.SysParamConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统参数配置表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-05
 */
@Service
public class SysParamConfigServiceImpl  implements SysParamConfigService {

    @Autowired
    private SysParamConfigMapper sysParamConfigMapper;
    @Override
    public ResponseVO page(Integer page, Integer size) {
        Page<SysParamConfig> page1 = new Page<>(page, size);
        Page<SysParamConfig> page2 = sysParamConfigMapper.selectPage(page1, null);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,page2);
    }

    @Override
    public ResponseVO selectByInput(String name) {
        List<SysParamConfig> sysParamConfigs = sysParamConfigMapper.selectByInput(name);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,sysParamConfigs);
    }

    @Override
    public ResponseVO insert(SysParamConfig entity) {
        if (entity.getName() == null || "".equals(entity.getName())) {
            return new ResponseVO(ErrorCodeEnum.REGULARNOTACCORD, null);
        } else {
            entity.setStatus(1);
            long ct = System.currentTimeMillis();
            Timestamp timestamp = TimeUtils.LongToDate(ct);
            entity.setCreateTime(timestamp);
            int i = sysParamConfigMapper.insert(entity);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, i);
        }
    }

    @Override
    public ResponseVO updateById(SysParamConfig entity) {
        long ct = System.currentTimeMillis();
        Timestamp timestamp = TimeUtils.LongToDate(ct);
        entity.setUpdateTime(timestamp);
        int i = sysParamConfigMapper.updateById(entity);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO status(Long id,Boolean status) {
        if(status) {
            Integer open = sysParamConfigMapper.statusOpen(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS,open);
        }else {
            Integer close = sysParamConfigMapper.statusClose(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, close);
        }
    }


    @Override
    public ResponseVO selectList(Wrapper<SysParamConfig> queryWrapper) {
        List<SysParamConfig> sysParamConfigs = sysParamConfigMapper.selectList(null);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,sysParamConfigs);
    }

    @Override
    public ResponseVO deleteById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO deleteByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO delete(Wrapper<SysParamConfig> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO deleteBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }



    @Override
    public ResponseVO update(SysParamConfig entity, Wrapper<SysParamConfig> updateWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO selectBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO selectOne(Wrapper<SysParamConfig> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectCount(Wrapper<SysParamConfig> queryWrapper) {
        return null;
    }


    @Override
    public ResponseVO selectMaps(Wrapper<SysParamConfig> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectObjs(Wrapper<SysParamConfig> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<SysParamConfig>> ResponseVO selectPage(P page, Wrapper<SysParamConfig> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<Map<String, Object>>> ResponseVO selectMapsPage(P page, Wrapper<SysParamConfig> queryWrapper) {
        return null;
    }
}
