package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.mapper.SysOrgMapper;
import com.zbkc.mapper.SysUserOrgMapper;
import com.zbkc.model.dto.GrantUserDTO;
import com.zbkc.model.dto.OrgDTO;
import com.zbkc.model.po.SysOrg;
import com.zbkc.model.po.SysUserOrg;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.vo.SysUserVO;
import com.zbkc.service.SysOrgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 组织管理 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-11
 */
@Service
public class SysOrgServiceImpl  implements SysOrgService {
    @Autowired
    private SysOrgMapper sysOrgMapper;
    @Autowired
    private SysUserOrgMapper sysUserOrgMapper;

    @Override
    public ResponseVO status(Long id, Boolean status) {
        if(status) {
            Integer open = sysOrgMapper.statusOpen(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS,open);
        }else {
            Integer close = sysOrgMapper.statusClose(id);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, close);
        }
    }

    @Override
    public ResponseVO orgList(OrgDTO orgDTO) {
        orgDTO.setParentid((long)0);
        List<SysOrg> sysOrgs = sysOrgMapper.orgsByPidList(orgDTO.getParentid());

        return new ResponseVO(ErrorCodeEnum.SUCCESS,orgsList(sysOrgs));
    }

    @Override
    public ResponseVO userList() {
        List<SysUserVO> users = sysOrgMapper.userList();
        return new ResponseVO(ErrorCodeEnum.SUCCESS,users);
    }

    @Override
    public ResponseVO findSelfUser(Long orgId) {
        List<SysUserVO> vos = sysUserOrgMapper.findSelfUser(orgId);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,vos);
    }

    @Override
    public ResponseVO grantUser(GrantUserDTO dto) {
        QueryWrapper<SysUserOrg> wrapper = new QueryWrapper<>();
        wrapper.eq("org_id",dto.getId());
        sysUserOrgMapper.delete(wrapper);
        List<Long> ids = dto.getUsersIds();
        for(int i=0;i<ids.size();i++){
            SysUserOrg userOrg = new SysUserOrg();
            userOrg.setOrgId(dto.getId());
            userOrg.setUserId(ids.get(i));
            sysUserOrgMapper.insert(userOrg);
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS,null);
    }

    @Override
    public ResponseVO parentOrg() {
        return new ResponseVO(ErrorCodeEnum.SUCCESS,sysOrgMapper.parentOrg());
    }

    @Override
    public ResponseVO sonOrg(Long id) {
        List<SysOrg> sysOrgs = sysOrgMapper.sonOrg(id);
        if (null!=sysOrgs){
            return new ResponseVO(ErrorCodeEnum.SUCCESS,sysOrgs);
        }else{
            return new ResponseVO(ErrorCodeEnum.SUCCESS,null);
        }
    }



    @Override
    public List<SysOrg> selectAllListNoPage() {
        QueryWrapper<SysOrg> queryWrapper = new QueryWrapper<>();
        queryWrapper.isNotNull("id");
        return this.sysOrgMapper.selectList(queryWrapper);
    }


    private List<SysOrg> orgsList(List<SysOrg> orgs){
        for (int i = 0; i< orgs.size(); i++){
            List<SysOrg> list = sysOrgMapper.orgsByPidList(orgs.get(i).getId());
            orgs.get( i ).setOrgsList(list);

            orgsList(orgs.get(i).getOrgsList());

        }
        return orgs;

    }

    @Override
    public ResponseVO updateById(SysOrg entity) {
        int i = sysOrgMapper.updateById(entity);
        return new ResponseVO(ErrorCodeEnum.SUCCESS,i);
    }

    @Override
    public ResponseVO insert(SysOrg entity) {
        if (entity.getOrgName() == null || "".equals(entity.getOrgName())) {
            return new ResponseVO(ErrorCodeEnum.REGULARNOTACCORD, null);
        } else {
            entity.setStatus(1);
            int i = sysOrgMapper.insert(entity);
            return new ResponseVO(ErrorCodeEnum.SUCCESS, i);
        }
    }
    @Override
    public ResponseVO deleteById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO deleteByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO delete(Wrapper<SysOrg> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO deleteBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }



    @Override
    public ResponseVO update(SysOrg entity, Wrapper<SysOrg> updateWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO selectBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO selectOne(Wrapper<SysOrg> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectCount(Wrapper<SysOrg> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectList(Wrapper<SysOrg> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectMaps(Wrapper<SysOrg> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectObjs(Wrapper<SysOrg> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<SysOrg>> ResponseVO selectPage(P page, Wrapper<SysOrg> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<Map<String, Object>>> ResponseVO selectMapsPage(P page, Wrapper<SysOrg> queryWrapper) {
        return null;
    }


}
