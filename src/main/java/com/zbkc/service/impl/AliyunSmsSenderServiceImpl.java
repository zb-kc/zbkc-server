package com.zbkc.service.impl;


import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.AliyunSMSConfig;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.vo.SmsRecordVO;
//import com.zbkc.service.AliyunSmsSenderService;
import lombok.Data;;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


/**
 * 短信服务实现层
 * @author yangyan
 * @date 2021/7/21
 */
//@Data
//@Slf4j
//@Service
public class AliyunSmsSenderServiceImpl{
//public class AliyunSmsSenderServiceImpl implements AliyunSmsSenderService {
//    private static final Logger logger = LoggerFactory.getLogger(AliyunSmsSenderServiceImpl.class);
//    /**
//     * 注入yaml中的参数
//     */
//    @Value("${aliyun.sms.access-key-id}")
//    private String accessKeyID;
//    @Value("${aliyun.sms.access-key-secret}")
//    private String accessKeySecret;
//    @Value("${aliyun.sms.regionId}")
//    private String regionId;
//    @Value("${aliyun.sms.domain}")
//    private String domain;
//    @Value("${aliyun.sms.signName}")
//    private String signName;
//
//    @Autowired
//    private AliyunSMSConfig aliyunSMSConfig;
//
//    @Override
//    public void sendSms(String phoneNum) {
//        log.info("请求数据:{}",new Gson().toJson(phoneNum));
//        DefaultProfile profile = DefaultProfile.getProfile(regionId, accessKeyID, accessKeySecret);
//        IAcsClient client = new DefaultAcsClient(profile);
//
//        CommonRequest request = new CommonRequest();
//        request.setMethod(MethodType.POST);
//        request.setDomain(domain);
//        request.setVersion("2017-05-25");
//        request.setAction("SendSms");
//        request.putQueryParameter("RegionId", regionId);
//        request.putQueryParameter("SignName", signName);
//        request.putQueryParameter("PhoneNumbers", phoneNum);
//        request.putQueryParameter("TemplateCode", "SMS_219754708");
//
//        Map<String, Object> map = new HashMap<>();
//        String code=getRandCode(6);
//        map.put("code", code);
//
//        Gson gson = new Gson();
//        String json = gson.toJson(map);
//        request.putQueryParameter("TemplateParam", json);
//        CommonResponse response = null;
//        try {
//            // 短信的结果
//            response = client.getCommonResponse(request);
//            System.out.println(response.getData());
//        } catch (Exception e) {
//            throw new BusinessException(ErrorCodeEnum.SMS_ERROR);
//        }
//
//        // 获得结果
//        Map<String, String> map1 = gson.fromJson(response.getData(), Map.class);
//        String responseCode = map1.get("Code");
//
//        if ("isv.BUSINESS_LIMIT_CONTROL".equals(responseCode)) {
//            throw new BusinessException(ErrorCodeEnum.SMS_SEND_ERROR_BUSINESS_LIMIT_CONTROL);
//        }
//
//        if (!"OK".equals(responseCode)) {
//            throw new BusinessException(ErrorCodeEnum.SMS_ERROR);
//        }
//    }
//
//    @Override
//    public void sedBillSms(SmsRecordVO smsRecordVO) {
//        log.info("请求数据:{}",new Gson().toJson(smsRecordVO));
//        DefaultProfile profile = DefaultProfile.getProfile(regionId, accessKeyID, accessKeySecret);
//        IAcsClient client = new DefaultAcsClient(profile);
//
//        CommonRequest request = new CommonRequest();
//        request.setMethod(MethodType.POST);
//        request.setDomain(domain);
//        request.setVersion("2017-05-25");
//        request.setAction("SendSms");
//        request.putQueryParameter("RegionId", regionId);
//        request.putQueryParameter("SignName", signName);
//        request.putQueryParameter("PhoneNumbers", smsRecordVO.getPhone());
//        request.putQueryParameter("TemplateCode", smsRecordVO.getWord());
//
//        Map<String, Object> map = new HashMap<>();
//        String code=getRandCode(6);
//        map.put("code", code);
//
//        Gson gson = new Gson();
//        String json = gson.toJson(map);
//        request.putQueryParameter("TemplateParam", json);
//        CommonResponse response = null;
//        try {
//            // 短信的结果
//            response = client.getCommonResponse(request);
//            System.out.println(response.getData());
//        } catch (Exception e) {
//            throw new BusinessException(ErrorCodeEnum.SMS_ERROR);
//        }
//
//        // 获得结果
//        Map<String, String> map1 = gson.fromJson(response.getData(), Map.class);
//        String responseCode = map1.get("Code");
//
//        if ("isv.BUSINESS_LIMIT_CONTROL".equals(responseCode)) {
//            throw new BusinessException(ErrorCodeEnum.SMS_SEND_ERROR_BUSINESS_LIMIT_CONTROL);
//        }
//
//        if (!"OK".equals(responseCode)) {
//            throw new BusinessException(ErrorCodeEnum.SMS_ERROR);
//        }
//    }
//
//    /**
//     * 生成随机验证码
//     * @param digits
//     * @return
//     */
//    public static String getRandCode(int digits) {
//        StringBuilder sBuilder = new StringBuilder();
//        Random rd = new Random((new Date()).getTime());
//
//        for(int i = 0; i < digits; ++i) {
//            sBuilder.append(String.valueOf(rd.nextInt(9)));
//        }
//
//        return sBuilder.toString();
//    }
}



