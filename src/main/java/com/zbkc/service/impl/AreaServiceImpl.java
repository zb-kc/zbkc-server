package com.zbkc.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.mapper.SysAreaMapper;
import com.zbkc.model.po.SysArea;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.AreaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;


/**
 *
 * @author gmding
 * @date 2021-08-18
 */
@Service
@Slf4j
public class AreaServiceImpl implements AreaService, Serializable {

    @Autowired
    private SysAreaMapper sysAreaMapper;

    @Override
    public ResponseVO getProvinceCode() {
        return new ResponseVO( ErrorCodeEnum.SUCCESS,sysAreaMapper.getProvinceCode());
    }

    @Override
    public ResponseVO getCityCodeByProvinceCode(Long provinceCode) {
        return new ResponseVO( ErrorCodeEnum.SUCCESS,sysAreaMapper.getCityCodeByProvinceCode(provinceCode));
    }

    @Override
    public ResponseVO getAreaCodeByProvinceCode(Long cityCode) {
        return new ResponseVO( ErrorCodeEnum.SUCCESS,sysAreaMapper.getAreaCodeByProvinceCode(cityCode));
    }

    @Override
    public ResponseVO getStreetCodeCodeByProvinceCode(Long areaCode) {
        return new ResponseVO( ErrorCodeEnum.SUCCESS,sysAreaMapper.getStreetCodeCodeByProvinceCode(areaCode));
    }

    @Override
    public ResponseVO getCommitteeCodeCodeByProvinceCode(Long streetCode) {
        return new ResponseVO( ErrorCodeEnum.SUCCESS,sysAreaMapper.getCommitteeCodeCodeByProvinceCode(streetCode));
    }

    @Override
    public ResponseVO getDetailsByCode(Long code) {
        return new ResponseVO( ErrorCodeEnum.SUCCESS,sysAreaMapper.getDetailsByCode(code));
    }


    @Override
    public ResponseVO insert(SysArea entity) {
        return null;
    }

    @Override
    public ResponseVO deleteById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO deleteByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO delete(Wrapper<SysArea> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO deleteBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO updateById(SysArea entity) {
        return null;
    }

    @Override
    public ResponseVO update(SysArea entity, Wrapper<SysArea> updateWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectById(Serializable id) {
        return null;
    }

    @Override
    public ResponseVO selectBatchIds(Collection<? extends Serializable> idList) {
        return null;
    }

    @Override
    public ResponseVO selectByMap(Map<String, Object> columnMap) {
        return null;
    }

    @Override
    public ResponseVO selectOne(Wrapper<SysArea> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectCount(Wrapper<SysArea> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectList(Wrapper<SysArea> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectMaps(Wrapper<SysArea> queryWrapper) {
        return null;
    }

    @Override
    public ResponseVO selectObjs(Wrapper<SysArea> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<SysArea>> ResponseVO selectPage(P page, Wrapper<SysArea> queryWrapper) {
        return null;
    }

    @Override
    public <P extends IPage<Map<String, Object>>> ResponseVO selectMapsPage(P page, Wrapper<SysArea> queryWrapper) {
        return null;
    }
}
