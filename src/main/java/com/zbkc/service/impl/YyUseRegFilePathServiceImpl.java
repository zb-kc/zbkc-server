package com.zbkc.service.impl;

import com.zbkc.model.po.YyUseRegFilePath;
import com.zbkc.mapper.YyUseRegFilePathMapper;
import com.zbkc.service.YyUseRegFilePathService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运营管理——使用登记/委托运营——自定义附件表 服务实现类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-09
 */
@Service
public class YyUseRegFilePathServiceImpl extends ServiceImpl<YyUseRegFilePathMapper, YyUseRegFilePath> implements YyUseRegFilePathService {

}
