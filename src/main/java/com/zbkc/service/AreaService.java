package com.zbkc.service;


import com.zbkc.model.po.SysArea;
import com.zbkc.model.vo.ResponseVO;
import org.apache.ibatis.annotations.Param;


/**
 * <p>
 * 省市区街道社区
 * </p>
 *
 * @author gmding
 * @since 2021-08-18
 */
public interface AreaService extends BaseService<SysArea>{

    /**
     *
     * @return ResponseVO 获取所有省直辖市
     */
    ResponseVO getProvinceCode();

    /**
     *
     * @return ResponseVO 根据省编码获取所有市编码
     */
    ResponseVO getCityCodeByProvinceCode(Long provinceCode);

    /**
     *
     * @return ResponseVO 根据市编码获取所有区编码
     */
    ResponseVO getAreaCodeByProvinceCode(Long cityCode);

    /**
     *
     * @return ResponseVO 根据区编码获取所有街道编码
     */
    ResponseVO getStreetCodeCodeByProvinceCode(Long areaCode);


    /**
     *
     * @return ResponseVO 根据区编码获取所有街道编码
     */
    ResponseVO getCommitteeCodeCodeByProvinceCode(Long streetCode);

    /**
     *
     * @return ResponseVO 根据区编码获取所有街道编码
     */
    ResponseVO getDetailsByCode(Long code);


}
