package com.zbkc.service;

import com.zbkc.model.dto.*;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.*;

import java.text.ParseException;
import java.util.List;

/**
 * <p>
 * 物业基本(主体)信息表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-09
 */
public interface WyBasicInfoService extends BaseService<WyBasicInfo> {


    ResponseVO addBasic(WyBasicInfoVo vo) throws ParseException, Exception;

    ResponseVO getList(WyBasicInfoHomeDTO vo);

    ResponseVO batch(Long[] ids);

    /**
     * @param id
     * @return ResponseVO
     */
    ResponseVO getDetails(Long id);

    /**
     * @param dto
     * @return ResponseVO
     */
    ResponseVO updateInfo(WyBasicInfoDTO dto) throws ParseException, Exception;

    /**
     * @param id 用户id
     * @param userId
     * @return ResponseVO
     */
    ResponseVO propertyStructure(Long id, Long userId );

    /**
     * @param name 物业名称
     * @return ResponseVO
     */
    ResponseVO nameCheck(Long pid,String name,String unitName);

    ResponseVO getPropertyName(String name);

    ResponseVO logicDel(Long id);

    ResponseVO getRentalSocialWyList(SocicalWyDTO dto);

    ResponseVO seeRow(Long id);

    ResponseVO addRentalSocialWy(RentalSocialWyInfoDTO dto) throws  Exception;

    ResponseVO updRentalSocialWy(RentalSocialWyInfoDTO dto) throws  Exception;

    ResponseVO delRentalSocialWy(Long[] id);

    /**
     * 资产管理-选择物业资产 显示所有物业信息
     * @param contractId 合同id
     * @param userId    用户id
     * @param isUseReg
     * @return List
     */
    List<WyBaseInfoVO> selectAllWyBaseInfo(String contractId, String userId, Boolean isUseReg);

    ResponseVO selectByWyId(Long[] wyIds);


    /**
     * 分页查询物业信息
     * @param wyBasicInfoInDataPermissionDTO dto
     * @return  list
     */
    WyBasicInfoInDataPermissionVO selectListInDataPermission(WyBasicInfoInDataPermissionDTO wyBasicInfoInDataPermissionDTO);

    ResponseVO queryEmptyList(String id,String wyName,String preCompany,Integer p,Integer size);

    ResponseVO queryUsedList(String id,String wyName,String objName,Integer p,Integer size);

    ResponseVO queryProrightList(String id,String wyName,String certificateNo,Integer p,Integer size);

    ResponseVO queryEntryList(String id,String wyName,String entryType,Integer p,Integer size);

    ResponseVO queryBuildingList(String id,String wyName,Integer p,Integer size);

    ResponseVO queryFloorList(String id,String wyName,Integer p,Integer size);

    ResponseVO queryUnitList(String id,String wyName,String company,String status,Integer p,Integer size);

    /**
     * 查询物业使用情况
     * @param maxPid 物业顶级id
     * @return 物业id
     */
//    List<Long> selectWyIdUsedList(String maxPid);

    /**
     *
     * @param id 物业id
     * @param userId 用户id
     * @return 结构树
     */
    List<PropertyStructureVo> getPropertyStructureHandler(Long id, Long userId);

    ResponseVO getEmptyWyInfo(Long userId);

    /**
     * 结构树获取 方式2 计算是否租用
     * @param id 物业id
     * @param userId 用户id
     * @return 空置面积
     */
    double countEmptyAreaByPropertyStructure(Long id, Long userId);
}
