package com.zbkc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.dto.SysRoleDataPermissionDTO;
import com.zbkc.model.po.SysOrg;
import com.zbkc.model.po.SysRoleDataPermission;

import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/11/8
 */
public interface SysRoleDataPermissionService extends IService<SysRoleDataPermission> {

    /**
     * 批量插入
     * @param sysRoleDataPermissionList list
     * @return 影响条数
     */
    int insertBatch(List<SysRoleDataPermission> sysRoleDataPermissionList);

    /*
    保存数据权限
     */
    Integer saveDataPermission(SysRoleDataPermissionDTO sysRoleDataPermissionDTO);
}
