package com.zbkc.service;

import com.zbkc.model.dto.*;
import com.zbkc.model.dto.export.ExportUseRegDTO;
import com.zbkc.model.po.YyUseReg;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.vo.ResponseVO;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 运营管理——使用登记 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-09
 */
public interface YyUseRegService extends IService<YyUseReg> {

    ResponseVO addUseReg(YyUseRegDTO dto);

    ResponseVO getList(YyUseRegHomeDTO dto);

    ResponseVO seeRow(String id);

    ResponseVO updUseReg(YyUseRegDTO dto);

    ResponseVO export(ExportUseRegDTO dto , HttpServletResponse response);

    ResponseVO updStatus(Long id ,Integer status);
}
