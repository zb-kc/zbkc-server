package com.zbkc.service;

import com.zbkc.model.vo.AuthInfoVO;
import com.zbkc.model.dto.LoginDTO;
import com.zbkc.model.vo.ResponseVO;

/**
 * 登录service接口
 * @author yangyan
 * @date 2021-7-16
 */
public interface LoginService {
    /**
     * 用户登录
     * @apiParam userInfo
     * @return ResponseVO
     */
    ResponseVO userLogin(LoginDTO userInfo);

    /**
     * 重新获取token
     * @apiParam info
     * @return ResponseVO
     */
    ResponseVO getRefreshToken(AuthInfoVO info);

    /**
     * 获取匿名token
     * @return  ResponseVO
     */
    ResponseVO getAccessToken();
}
