package com.zbkc.service;

import com.zbkc.model.dto.GrantMenuDTO;
import com.zbkc.model.po.SysRoleMenuFunc;
import com.zbkc.model.vo.ResponseVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统角色菜单功能表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-02
 */

public interface SysRoleMenuFuncService extends BaseService<SysRoleMenuFunc> {
    /**
     * @param roleId
     * @return ResponseVO
     */
    ResponseVO findSelfMenu(Long roleId);

    /**
     * @param grantMenuDTO 传输对象
     * @return ResponseVO
     */
    ResponseVO grantMenus(GrantMenuDTO grantMenuDTO);

    /**
     * @param roleId 角色id
     * @return ResponseVO
     */
    ResponseVO findSelfMenuIds(@Param("roleId") Long roleId);
}
