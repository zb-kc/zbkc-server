package com.zbkc.service;

import com.zbkc.model.dto.YwAgencyBusinessDTO;
import com.zbkc.model.dto.YwContractDTO;
import com.zbkc.model.po.YwAgencyBusinessPO;
import com.zbkc.model.vo.YwProcessInfoVO;
import com.zbkc.model.vo.ResponseVO;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 工作流服务类
 * @author ZB3436.xiongshibao
 * @since 2021-9-16
 */
public interface ActivitiBaseService {

    /**
     * 流程部署
     * @param type 类型（移交用房、合同签署、物业资产）
     * @return  部署状态值
     * @throws IOException  异常
     */
    Deployment deploymentProcess(String type) throws IOException;

    /**
     * 启动流程定义
     * @param processesId 流程ID
     * @param businessId 关联ID
     * @param variables 流程变量map
     * @return  流程实例
     */
    ProcessInstance startProcessInstance(String processesId , String businessId , Map<String , Object> variables);

    /**
     * 完成节点任务
     * @param taskId taskId
     * @param variables 流程变量
     */
    void completeTask(String taskId , Map<String, Object> variables);

    /**
     * 删除流程实例 根据流程ID
     * `act_ge_bytearray`
     * `act_re_deployment`
     * `act_re_procdef`
     * @param deploymentId  部署ID
     * @param deleteReason  删除原因
     */
    void deleteTaskProcessInstance(String deploymentId, String deleteReason);


    /**
     * 终止节点任务-从当前节点跳转值尾节点
     * @param businessId    任务ID
     * @param reason 终止原因
     */
    void deleteProcess(String businessId, String reason);


    /**
     * 根据业务id 查询 流程当前状态（业务id为唯一值，当前节点）
     * @param businessId 业务id
     * @return Map<businessId , 当前业务状态>
     */
    Map<String ,String> findProcessCurrentStatusByBusinessKey(String businessId);

    /**
     * 批量查询业务集合的状态
     * @param businessIdList 业务id集合
     * @return Map<businessId , 当前业务状态>
     */
    Map<String ,String> findProcessCurrentStatusByBusinessKeys(List<String> businessIdList);

    /**
     * 根据业务id 查询该业务当前所有流转过的节点信息
     * @param businessId   业务id
     * @return  ResponseVO
     */
    ResponseVO findTaskListByBusinessKey(String businessId);

    /**
     * 根据业务id 查询该业务的所有流程的流转情况（节点所有流转信息）
     * @param businessId 业务id
     * @return List<YwProcessInfoPO>
     */
    List<YwProcessInfoVO> findProcessAllStatusByBusinessKey(String businessId);

    /**
     * 根据业务id 创建流程实例
     * @param businessId 业务id
     * @param applyType 申请类型 特殊：合同申请类型（运营类、非运营类）
     */
    void startProcessInstance(String businessId , String applyType);

    /**
     * 根据业务id 创建流程实例
     * @param ywAgencyBusinessPO 移交用房申请DTO
     */
    void startProcessInstance(YwAgencyBusinessPO ywAgencyBusinessPO);

    /**
     * 完成节点任务
     *  审核通过/驳回任务
     * @param processInfo 流程信息
     */
    void completeTask(YwProcessInfoVO processInfo);

    /**
     * 终止流程
     * @param processInfoPO 流程信息
     */
    void terminateProcessInstance(YwProcessInfoVO processInfoPO);

    /**
     * 根据业务id 查询流程状态
     * @param businessId 业务id
     * @return 流程状态
     */
    String getProcessStatus(String businessId);

    /**
     * 根据业务id获取流程图
     * @param businessId 业务id
     * @return  图片的base64编码
     */
    String getProcessImg(String businessId);

    /**
     * 审核通过-完成节点
     * @param businessId 业务id
     * @param opinion 审核意见
     */
    void completeTask(String businessId , String opinion);

    /**
     *  驳回节点任务
     * @param businessId    业务ID
     * @param deleteReason  驳回原因
     */
    ResponseVO turnDownTaskInstance(String businessId , String deleteReason);

    /**
     * 新增业务办理-待办业务表，并开启流程
     * @param ywAgencyBusinessPO ywAgencyBusinessPO
     */
    void addAgencyBusinessRecord(YwAgencyBusinessPO ywAgencyBusinessPO);

    /**
     * 完成节点任务
     * @param ywContractDTO ywContractDTO
     */
    int completeTask(YwContractDTO ywContractDTO);

    void deleteAllProgress();

    /**
     * 根据业务id 查询流程中第二个activityId
     * @param ywAgencyBusinessDTO dto
     * @return 第二个activityId
     */
    String getSecondActivityId(YwAgencyBusinessDTO ywAgencyBusinessDTO);

    /**
     * 校验是否是第二节点
     * @param businessId 业务id
     * @return true 是 false否
     */
    Boolean isSecondActivity(String businessId);

}
