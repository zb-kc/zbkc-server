package com.zbkc.service;

import com.zbkc.model.dto.GrantOrgDTO;
import com.zbkc.model.po.SysUser;
import com.zbkc.model.vo.ResponseVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统用户表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-07-27
 */
public interface SysUserService extends BaseService<SysUser> {

    List<SysUser> queryByUserName(String userName);

    /**
     * @param id 当前行的id
     * @param status 当前状态
     * @return ResponseVO
     */
    ResponseVO status(@Param("id") Long id, @Param("status") Boolean status);

    /**
     * @param page 页码
     * @param size 行数
     * @param name 名称
     * @return ResponseVO
     */
    ResponseVO pageByName(@Param("p")Integer page,@Param("size")Integer size,@Param( "name")String name);


    /**
     * @param dto 传输对象
     * @return ResponseVO
     */
    ResponseVO grantOrgs(@Param("dto") GrantOrgDTO dto);

    /**
     * @param userId  用户Id
     * @return ResponseVO
     */
    ResponseVO findSelfOrgIds(@Param("userId") Long userId);


    /**
     * @param userId  用户Id
     * @return ResponseVO
     */
    ResponseVO findSelfMenu(Long userId);

    /**
     * @param userId 用户id
     * @param roleId 角色id
     * @return ResponseVO
     */
    ResponseVO updUserRole(@Param("userId") Long userId, @Param("roleId") Long roleId);

    /**
     * 新增用户信息
     * @param sysUser 用户信息
     * @return 影响行数
     */
    ResponseVO insertRecord(SysUser sysUser);

    /**
     * 更新用户数据
     * @param data data
     * @return ResponseVO
     */
    ResponseVO updateRecord(SysUser data);
}
