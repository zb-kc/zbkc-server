package com.zbkc.service;

import com.zbkc.model.dto.*;
import com.zbkc.model.dto.export.ExportBillManagementDTO;
import com.zbkc.model.po.YyBillManagement;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.vo.ResponseVO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 运营管理——账单管理 服务类
 * </p>
 *
 * @author caobiyang
 * @since 2021-09-14
 */
public interface YyBillManagementService extends IService<YyBillManagement> {

    ResponseVO saveBill(YyBillManagementDTO dto);

    ResponseVO seeRow(String id,Long wyId);


    ResponseVO collecMoneySave(List<CollecMoneyDTO> dto);

    ResponseVO collecInit(CollecDTO dto);

//    ResponseVO beReceived();

    ResponseVO callSms(SmsDTO dto);

    ResponseVO batchCallSms(List<String> ids);

    List<String> getAllSmsName();

    //根据短信类型获取短信内容模板
    String  getWordByType();

    ResponseVO homeList(BillManagementDTO dto);

    ResponseVO selectAllCallRecord(SmsReCordDTO dto);

    ResponseVO selectWyNameByType(String wyType);

    ResponseVO exportBillMAnagement(ExportBillManagementDTO dto, HttpServletResponse response);

    ResponseVO getBillCount(String billTime);

//    ResponseVO sendSmsIndex(Long contractId);
    ResponseVO getWordByName(String name);

    ResponseVO manyCollection(Long contractId,Integer billStatus);

    ResponseVO saveManyCollection(List<SaveCollectionDTO> dto);
}

