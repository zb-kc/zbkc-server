package com.zbkc.service;

import com.zbkc.model.dto.WyAssetDisposalDTO;
import com.zbkc.model.dto.WyAssetDisposalHomeDTO;
import com.zbkc.model.dto.export.ExportWyAssetDisposalDTO;
import com.zbkc.model.dto.export.ExportWyAssetEntryDTO;
import com.zbkc.model.po.WyAssetDisposal;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.vo.ResponseVO;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;

/**
 * <p>
 * 资产处置信息表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-07
 */
public interface WyAssetDisposalService extends IService<WyAssetDisposal> {

    ResponseVO addDisposal(WyAssetDisposalDTO dto) throws ParseException, Exception;

    ResponseVO getList(WyAssetDisposalHomeDTO dto);

    ResponseVO seeRow(String id);

    ResponseVO updDisposal(WyAssetDisposalDTO dto) throws ParseException, Exception;

    ResponseVO delAssetDispoal(String id);

    ResponseVO export(ExportWyAssetDisposalDTO dto , HttpServletResponse response);

}
