package com.zbkc.service;

import com.zbkc.model.dto.WyProRightHomeDTO;
import com.zbkc.model.dto.export.ExporProRightRegDTO;
import com.zbkc.model.po.WyProRightReg;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.dto.WyProRightRegDTO;
import org.apache.ibatis.annotations.Param;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 产权登记表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-06
 */
public interface WyProRightRegService extends BaseService<WyProRightReg> {
    /**
     * @param id  主键id
     * @param status 当前状态
     * @return ResponseVO
     */
    ResponseVO status(@Param("id") Long id, @Param("status") Boolean status);



    /**
     * @param dto 传输对象
     * @return ResponseVO
     */
    ResponseVO addProRight( WyProRightRegDTO dto);

    /**
     * @return List<UnitNameVo>
     * @param userId
     */
    ResponseVO selectAllName(Long userId);

    ResponseVO getList(WyProRightHomeDTO dto);

    ResponseVO updProright(WyProRightRegDTO dto);

    ResponseVO seeRow(String id);

    ResponseVO delProright(String id);

    ResponseVO export(ExporProRightRegDTO dto , HttpServletResponse response);
}
