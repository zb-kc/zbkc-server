package com.zbkc.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zbkc.model.vo.ResponseVO;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 封装BaseService
 * @author yangyan
 * @date 2021/7/19
 */
public interface BaseService<T> {
     /**
      * 插入一条记录
      * @param entity 实体对象
      * @return ResponseVO
      */
     ResponseVO insert(T entity);

     /**
      * 通用删除操作  通过ID删除
      * @param id 主键ID
      * @return ResponseVO
      */
     ResponseVO deleteById(Serializable id);

     /**
      * 根据 columnMap 条件，删除记录
      * @param columnMap 表字段 map 对象
      * @return ResponseVO
      */
     ResponseVO deleteByMap(@Param("cm") Map<String, Object> columnMap);

     /**
      * 根据 entity 条件，删除记录
      * @param queryWrapper 实体对象封装操作类（可以为 null,里面的 entity 用于生成 where 语句）
      * @return ResponseVO
      */
     ResponseVO delete(@Param("ew") Wrapper<T> queryWrapper);

     /**
      * 通用查询操作 deleteBatchIds 通过多个ID进行删除
      * @param idList 主键ID列表(不能为 null 以及 empty)
      * @return ResponseVO
      */
     ResponseVO deleteBatchIds(@Param("coll")  Collection<? extends Serializable> idList);

     /**
      * 根据输入数据的id更新
      * @param entity 实体对象
      * @return ResponseVO
      */
     ResponseVO updateById(@Param("et") T entity);

     /**
      * 根据 whereEntity 条件，更新记录
      * @param entity        实体对象 (set 条件值,可以为 null)
      * @param updateWrapper 实体对象封装操作类（可以为 null,里面的 entity 用于生成 where 语句）
      * @return ResponseVO
      */
     ResponseVO update(@Param("et") T entity,@Param("ew") Wrapper<T> updateWrapper);

     /**
      * 通用查询操作 根据id查询
      * @param id
      * @return ResponseVO
      */
     ResponseVO selectById(Serializable id);

     /**
      * 通用查询操作 selectBatchIds 通过多个ID进行查询
      * @param idList 主键ID列表(不能为 null 以及 empty)
      * @return ResponseVO
      */
     ResponseVO selectBatchIds(@Param("coll") Collection<? extends Serializable> idList);

     /**
      * 通用查询操作 selectByMap  map要写列名条件 不能是实体属性名
      * @param columnMap 表字段 map 对象
      * @return ResponseVO
      */
     ResponseVO selectByMap(@Param("cm") Map<String, Object> columnMap);

     /**
      * 根据 entity 条件，查询一条记录 最多只能查到一个
      * @param queryWrapper 实体对象封装操作类（可以为 null）
      * @return ResponseVO
      */
     ResponseVO selectOne(@Param("ew") Wrapper<T> queryWrapper);

     /**
      * 根据 Wrapper 条件，查询总记录数
      * @param queryWrapper 实体对象封装操作类（可以为 null）
      * @return ResponseVO
      */
     ResponseVO selectCount(@Param("ew") Wrapper<T> queryWrapper);

     /**
      * 根据 entity 条件，查询全部记录
      * @param queryWrapper 实体对象封装操作类（可以为 null）
      * @return ResponseVO
      */
     ResponseVO selectList(@Param("ew") Wrapper<T> queryWrapper);

     /**
      * 根据 Wrapper 条件，查询全部记录
      * @param queryWrapper 实体对象封装操作类（可以为 null）
      * @return ResponseVO
      */
     ResponseVO selectMaps(@Param("ew") Wrapper<T> queryWrapper);

     /**
      * 根据 Wrapper 条件，查询全部记录
      * <p>注意： 只返回第一个字段的值</p>
      * @param queryWrapper 实体对象封装操作类（可以为 null）
      * @return ResponseVO
      */
     ResponseVO selectObjs(@Param("ew") Wrapper<T> queryWrapper);

     /**
      * 根据 entity 条件，查询全部记录（并翻页）
      * @param page         分页查询条件（可以为 RowBounds.DEFAULT）
      * @param queryWrapper 实体对象封装操作类（可以为 null）
      * @return <P extends IPage<T>>ResponseVO
      */
     <P extends IPage<T>>ResponseVO selectPage(P page,@Param("ew") Wrapper<T> queryWrapper);

     /**
      * 根据 Wrapper 条件，查询全部记录（并翻页）
      * @param page         分页查询条件
      * @param queryWrapper 实体对象封装操作类
      * @return ResponseVO
      */
     <P extends IPage<Map<String, Object>>>ResponseVO  selectMapsPage(P page, @Param("ew") Wrapper<T> queryWrapper);

}
