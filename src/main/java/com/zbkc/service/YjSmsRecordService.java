package com.zbkc.service;


import com.zbkc.model.dto.export.ExportYjContractDTO;
import com.zbkc.model.dto.YjContractDTO;
import com.zbkc.model.dto.SmsDTO;
import com.zbkc.model.dto.SmsReCordDTO;
import com.zbkc.model.po.YjSmsRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.vo.ResponseVO;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 预警提醒——合同到期提醒_短信记录表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-15
 */
public interface YjSmsRecordService extends IService<YjSmsRecord> {

    ResponseVO allContarct(YjContractDTO dto);


    ResponseVO selectAllRecord(SmsReCordDTO dto);

    ResponseVO selectWord(String name);

    ResponseVO sendMessage(SmsDTO dto);

    ResponseVO export(ExportYjContractDTO dto , HttpServletResponse response);

    ResponseVO getAllRemindMsgName();

}
