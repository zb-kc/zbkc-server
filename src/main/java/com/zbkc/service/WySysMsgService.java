package com.zbkc.service;


import com.zbkc.model.dto.export.ExportSmsTemplateDTO;
import com.zbkc.model.dto.WySysMsgHomeDTO;
import com.zbkc.model.po.WySysMsg;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.vo.ResponseVO;

import javax.servlet.http.HttpServletResponse;


/**
 * <p>
 * 短信模板管理表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-11
 */
public interface WySysMsgService extends IService<WySysMsg> {

    ResponseVO addMsg(WySysMsg dto);

    ResponseVO getList(WySysMsgHomeDTO dto);

    ResponseVO seeRow(String id);

    ResponseVO updMsg(WySysMsg dto);

    ResponseVO export(ExportSmsTemplateDTO dto, HttpServletResponse response);

}
