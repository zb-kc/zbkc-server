package com.zbkc.service;

import com.zbkc.model.po.WyFloorInfo;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.BaseService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 物业楼层信息表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-10
 */
public interface WyFloorInfoService extends BaseService<WyFloorInfo> {

    /**
     * @param p 页码
     * @param size 行数
     * @param name 关键字
     * @return  ResponseVO
     */
   ResponseVO pageByName(@Param("p") Integer p, @Param("size") Integer size, @Param("name") String name);
}
