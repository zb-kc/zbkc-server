package com.zbkc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.dto.YwDevInfoDTO;
import com.zbkc.model.po.YwDevInfoPO;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/9/27
 */
public interface YwDevInfoService extends IService<YwDevInfoPO> {

    int addYwDevInfo(YwDevInfoPO ywDevInfoPO);

    YwDevInfoDTO selectOneByBusinessId(long id);
}
