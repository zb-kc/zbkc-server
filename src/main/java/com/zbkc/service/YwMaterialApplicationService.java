package com.zbkc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.dto.YwAgencyBusinessDTO;
import com.zbkc.model.dto.YwMaterialApplicationDTO;
import com.zbkc.model.po.YwAgencyBusinessPO;
import com.zbkc.model.po.YwHandoverDetailsPO;
import com.zbkc.model.po.YwMaterialApplicationPO;
import com.zbkc.model.vo.PageVO;

import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/14
 */
public interface YwMaterialApplicationService extends IService<YwMaterialApplicationPO> {

    int insertMaterialApplication(YwMaterialApplicationDTO ywMaterialApplicationDTO);

    /**
     * 分页查询
     * @param ywMaterialApplicationDTO 业务办理——物业资产申报-申请材料DTO
     * @return ResponseVO
     */
    PageVO<YwMaterialApplicationPO> pageYwAgencyBusinessList(YwMaterialApplicationDTO ywMaterialApplicationDTO);

}
