package com.zbkc.service;

import com.zbkc.model.dto.MenuDTO;
import com.zbkc.model.po.SysMenu;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.BaseService;

import java.io.Serializable;

/**
 * <p>
 * 系统菜单表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-07-30
 */
public interface SysMenuService extends BaseService<SysMenu> {

    /**
     * @param id 当前行的id
     * @return ResponseVO
     */
    ResponseVO status(Long id,Boolean status);

    /**
     * @param menuDTO
     * @return ResponseVO
     */
    ResponseVO menusList(MenuDTO menuDTO);

    /**
     * @param id id
     * @return ResponseVO
     */
    ResponseVO del(Long id);
}
