package com.zbkc.service;

import com.zbkc.model.dto.*;
import com.zbkc.model.dto.export.ExportCommissioneOperationDTO;
import com.zbkc.model.po.YyCommissioneOperation;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.vo.ResponseVO;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 运营管理——委托运营 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-10
 */
public interface YyCommissioneOperationService extends IService<YyCommissioneOperation> {

    ResponseVO addCommissioneOperation(YyCommissioneOperationDTO dto);

    ResponseVO getList(YyCommissioneOperationHomeDTO dto);

    ResponseVO seeRow(Long id);

    ResponseVO updCommissioneOperation(YyCommissioneOperationDTO dto);

    ResponseVO exportCommissioneOperation(ExportCommissioneOperationDTO dto, HttpServletResponse response);

}
