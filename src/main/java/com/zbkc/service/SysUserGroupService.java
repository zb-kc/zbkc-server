package com.zbkc.service;

import com.zbkc.model.dto.GroupDTO;
import com.zbkc.model.po.SysUserGroup;
import com.zbkc.model.vo.ResponseVO;

import java.io.Serializable;

/**
 * 组织管理
 * @author gmding
 * @date 2021-7-28
 */
public interface SysUserGroupService extends BaseService<SysUserGroup>{

    /**
     * 组织列表
     *
     * @return ResponseVO
     * @apiParam userInfo
     */
    ResponseVO groupList(GroupDTO groupDTO);

    /**
     * @param id 当前行的id
     * @param status 当前状态
     * @return ResponseVO
     */
    ResponseVO status(Long id,Boolean status);

    /**
     * @param id id
     * @return ResponseVO
     */
    ResponseVO del(Serializable id);
}
