package com.zbkc.service;

import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.*;
import com.zbkc.model.dto.export.*;
import com.zbkc.model.po.YyContract;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.vo.YyContractVO;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 运营管理——合同管理 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-13
 */
public interface YyContractService extends IService<YyContract> {

    ResponseVO addAndUpdContract(YyContractDTO dto) throws BusinessException;

    ResponseVO queryContractList(YyContractIndexDTO indexDTO);

    ResponseVO seeRow(String id);

    ResponseVO queryDepositList(DepositDTO dto);

    ResponseVO seeDepositRow(String id,Long wyId);

    ResponseVO editDeposit(EditDepositDTO dto);

    ResponseVO selectPreContract(YjContractDTO dto);

    ResponseVO selectIndustReview(YjContractDTO dto);

    ResponseVO leaseDetail(LeaseDetailDTO dto);

    ResponseVO emptyDetail(EmptyDetailDTO dto);

    ResponseVO exportPreContract(ExportPreContactDTO dto, HttpServletResponse response) ;

    ResponseVO exportIndust(ExportPreContactDTO dto, HttpServletResponse response) ;

    ResponseVO exportLease(ExportLeaseDTO dto, HttpServletResponse response);

    ResponseVO exportEmpty(ExportEmptyDTO dto, HttpServletResponse response) ;

    ResponseVO selectWyNameByType(String wyType);
    /**
     * 根据代办业务id 查询合同信息
     * @param businessId 代办业务id
     * @return  合同
     */
    YyContract selectOneByBusinessId(String businessId);

    /**
     * 导出押金
     * @return
     */
    ResponseVO  exportDeposit(ExportDepositDTO dto, HttpServletResponse response);

    void industReviewThreeMonth(ExportPreContactDTO dto, HttpServletResponse response);

    void preContractTwoMonth(ExportPreContactDTO dto, HttpServletResponse response);

    ResponseVO selectOneDTOByBusinessId(String bussinessId);

    /**
     * 根据文件id删除附件
     * @param fileId    文件id
     * @return  int
     */
    int deleteContractFile(String fileId);

    /**
     * 选择物业
     * @param businessId 业务id
     * @param wyIds 物业ids
     */
    int chooseWy(String businessId, String wyIds);

    ResponseVO exportContractHome(ExportContractDTO dto,HttpServletResponse response);

    /**
     * 根据社会唯一信用代码查询合同信息
     * @param ywAgencyBusinessDTO dto
     * @return list
     */
    PageVO<YyContractVO> selectYyContractListByPage(YwAgencyBusinessDTO ywAgencyBusinessDTO);

    ResponseVO downLoadPaymentNotice(HttpServletResponse response, DownLoadDTO dto);

    ResponseVO downLoadContactTemplate(HttpServletResponse response, DownLoadDTO dto);
}