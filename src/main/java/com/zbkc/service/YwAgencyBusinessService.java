package com.zbkc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkc.model.dto.YwAgencyBusinessDTO;
import com.zbkc.model.dto.YwContractDTO;
import com.zbkc.model.dto.YwHandoverDetailsDTO;
import com.zbkc.model.vo.*;
import com.zbkc.model.po.SysFilePath1;
import com.zbkc.model.po.YwAgencyBusinessPO;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/9/27
 */
public interface YwAgencyBusinessService extends IService<YwAgencyBusinessPO> {

    /**
     * 增加
     * @param ywAgencyBusinessDTO 移交用房申请dto
     * @return id
     */
    int addYwAgencyBusiness(YwAgencyBusinessDTO ywAgencyBusinessDTO);
    /**
     * 删除
     * @param ywAgencyBusinessPO 移交用房申请po
     * @return id
     */
    int removeYwAgencyBusiness(YwAgencyBusinessPO ywAgencyBusinessPO);
    /**
     * 更新
     * @param ywAgencyApplicationPO 移交用房申请po
     * @return id
     */
    int updateYwAgencyBusiness(YwAgencyBusinessDTO ywAgencyApplicationPO);
    /**
     * 查询根据id
     * @param ywAgencyBusinessPO 移交用房申请po
     * @return id
     */
    YwAgencyBusinessPO selectYwAgencyBusinessById(YwAgencyBusinessPO ywAgencyBusinessPO);
    /**
     * 分页查询
     * @param ywAgencyBusinessDTO 移交用房申请po
     * @return ResponseVO
     */
    PageVO<YwAgencyBusinessPO> pageYwAgencyBusinessList(YwAgencyBusinessDTO ywAgencyBusinessDTO);

    /**
     * 查询一条申请记录
     * @param businessId 业务id
     * @return  申请对象
     */
    YwAgencyBusinessPO selectOne(String businessId);

    /**
     * 保存文件路径到数据库
     * @param vo 文件vo
     * @param ywHandlerDetailsId 申请材料id
     * @return  文件路径
     */
    SysFilePath1 saveSysFilePath(FileVo vo, String ywHandlerDetailsId);

    /**
     * 打包下载文件
     * @param response HttpServletResponse
     * @param downloadFileIds 要下载的文件id
     */
    void downloadBatchByFile(HttpServletResponse response, String downloadFileIds);

    /**
     * 根据业务id 查询合同信息
     * @param businessId 业务id
     * @return YwContractDTO
     */
    YwContractVO showOldContract(String businessId);

    /**
     * 分页查询
     * @param ywAgencyBusinessDTO 移交用房申请po
     * @return ResponseVO
     */
    PageVO<YwAgencyBusinessVO> pageYwAgencyBusinessVOList(YwAgencyBusinessDTO ywAgencyBusinessDTO);

    /*
    删除记录
     */
    int cancelAuthorization(YwAgencyBusinessPO ywAgencyBusinessPO);

    /**
     * 企业授权管理-新增授权-选择合同
     * @param id 授权id/待办业务表id
     * @param contractIds   合同id id1,id2
     * @return  record
     */
    int chooseContractSave(String id, String contractIds);

    /**
     * 选择主合同
     * @param ywAgencyBusinessDTO dto
     * @return record
     */
    int chooseMainContract(YwAgencyBusinessDTO ywAgencyBusinessDTO);

    /**
     * 在审核流中更新合同信息
     * @param ywContractDTO 合同信息
     */
    int updateYyContractInProgress(YwContractDTO ywContractDTO);

    /**
     * 根据业务id查询附件材料审核表
     * @param ywContractDTO dto
     * @return list
     */
    List<YwHandoverDetailsDTO> selectYwHandoverDetailsByBusinessId(YwContractDTO ywContractDTO);

    /**
     * 企业授权校验
     * @param ywAgencyBusinessDTO
     * @return
     */
    ResponseVO checkRecord(YwAgencyBusinessDTO ywAgencyBusinessDTO);

    /**
     * 根据申请类型 生成受理号
     * @param ywAgencyBusinessPO 申请类型/代办类型 1移交用房申请；2物业资产申报; 3合同签署申请；
     * @return
     */
    String generateAcceptCode(YwAgencyBusinessPO ywAgencyBusinessPO);


    /**
     * 呈批表 组装数据
     * @param businessId 业务id
     * @param ywAgencyBusinessPO po
     * @return map
     */
    Map<String, String> getWordInfoMap(@RequestParam("businessId") String businessId, YwAgencyBusinessPO ywAgencyBusinessPO);

    /**
     * 材料补正保存信息
     * @param opinionMap 信息
     * @return vo
     */
    ResponseVO saveSupplementData(Map<String, String> opinionMap);

    /**
     * 材料补正
     * @param opinionMap 信息
     * @return vo
     */
    ResponseVO supplementData(Map<String, String> opinionMap);

    /**
     * 移交用房呈批表 流程进度
     * @param businessId 业务id
     * @param testList
     */
    void getProcessInfoList(String businessId, List<String[]> testList);

    /**
     * 导出方法 抽取
     * @param ywAgencyBusinessDTO dto
     * @param response response
     */
    void export(YwAgencyBusinessDTO ywAgencyBusinessDTO, HttpServletResponse response);

    /**
     * 业务办理-下载-移交用房呈批表
     * @param response response
     * @param businessId 业务id
     */
    void downloadAgencyApplyTable(HttpServletResponse response, String businessId);

    /**
     * 业务办理-下载-物业租赁合同呈批表
     * @param response response
     * @param businessId 业务id
     * @param ywAgencyBusinessPO po
     */
    void downloadSignApplicationTable(HttpServletResponse response, String businessId , YwAgencyBusinessPO ywAgencyBusinessPO);

    /**
     * 业务办理
     * @param businessId 业务id
     * @return ResponseVO
     */
    ResponseVO dealWith(String businessId);

    /**
     * 业务办理-下载租赁交款通知单-待修改
     * @param response response
     * @param businessId  业务id
     */
    void downLoadPaymentNotice(HttpServletResponse response, String businessId);

    /**
     * 业务办理-下载房屋租赁合同-待修改
     * @param response response
     * @param businessId 业务id
     */
    void downLoadContactTemplate(HttpServletResponse response, String businessId);

    /**
     * 校验业务是否撤回
     * @param businessId 业务id
     * @return  ResponseVO
     */
    ResponseVO checkApplyStatus(String businessId);

    /**
     * 企业授权导出方法 抽取
     * @param ywAgencyBusinessDTO dto
     * @param response response
     */
    void exportCompanyAuthorization(YwAgencyBusinessDTO ywAgencyBusinessDTO, HttpServletResponse response);

    /**
     * 判断业务信息是否存在 业务id
     * @param businessId 业务id
     * @return vo
     */
    ResponseVO dealWithBefore(String businessId);

    /**
     * 业务流全部完成最终处理
     * @param ywContractDTO dto
     */
    void businessFinallyHandler(YwContractDTO ywContractDTO);
}
