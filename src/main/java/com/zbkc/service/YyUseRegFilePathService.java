package com.zbkc.service;

import com.zbkc.model.po.YyUseRegFilePath;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 运营管理——使用登记/委托运营——自定义附件表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-09-09
 */
public interface YyUseRegFilePathService extends IService<YyUseRegFilePath> {

}
