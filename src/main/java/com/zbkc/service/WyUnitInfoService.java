package com.zbkc.service;

import com.zbkc.model.po.WyUnitInfo;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.BaseService;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 物业单元信息表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-09
 */
public interface WyUnitInfoService extends BaseService<WyUnitInfo> {

    ResponseVO pageByInput(@Param("p") Integer page, @Param("size")Integer size,@Param( "name")String name);
}
