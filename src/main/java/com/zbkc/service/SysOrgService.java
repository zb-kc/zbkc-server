package com.zbkc.service;

import com.zbkc.model.dto.GrantUserDTO;
import com.zbkc.model.dto.MenuDTO;
import com.zbkc.model.dto.OrgDTO;
import com.zbkc.model.po.SysOrg;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.BaseService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 组织管理 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-08-11
 */
public interface SysOrgService extends BaseService<SysOrg> {

    /**
     * @param id 当前行的id
     * @return ResponseVO
     */
    ResponseVO status(Long id, Boolean status);

    /**
     * @param orgDTO 组织传输对象
     * @return ResponseVO
     */
    ResponseVO orgList(OrgDTO orgDTO);

    /**
     * @return ResponseVO
     */
    ResponseVO userList();

    /**
     * @param orgId 组织id
     * @return  ResponseVO
     */
    ResponseVO findSelfUser(@Param("orgId") Long orgId);

    /**
     * @param dto 传输的组织用户包
     * @return ResponseVO
     */
    ResponseVO grantUser(@Param("dto") GrantUserDTO dto);

    ResponseVO parentOrg();

    ResponseVO sonOrg(Long id);

    /**
     * 查询所有组织信息
     * @return list
     */
    List<SysOrg> selectAllListNoPage();
}
