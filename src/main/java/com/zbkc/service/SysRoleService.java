package com.zbkc.service;

import com.zbkc.model.dto.SysRoleDataPermissionDTO;
import com.zbkc.model.po.SysRole;
import com.zbkc.model.vo.ResponseVO;
import java.io.Serializable;


/**
 * <p>
 * 系统角色表 服务类
 * </p>
 *
 * @author yangyan
 * @since 2021-07-29
 */
public interface SysRoleService extends BaseService<SysRole> {

    /**
     * @param id 当前行的id
     * @return ResponseVO
     */
    ResponseVO del(Serializable id);

    /**
     * @param id 当前行的id
     * @param status 当前状态
     * @return ResponseVO
     */
    ResponseVO status(Long id,Boolean status);


    /**
     * @param p 页码
     * @param size 行数
     * @param  index 索引
     * @return ResponseVO
     */
    ResponseVO pageByIndex(Integer p, Integer size,String index);

}
