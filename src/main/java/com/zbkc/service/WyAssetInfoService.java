package com.zbkc.service;


import com.zbkc.model.dto.*;
import com.zbkc.model.po.ZfWyAssetInfo;
import com.zbkc.model.vo.ResponseVO;

import java.text.ParseException;


/**
 *
 * @author gmding
 * @since 2021-09-11
 */
public interface WyAssetInfoService extends BaseService<ZfWyAssetInfo>{

    /**
     *
     * @return ResponseVO
     * @param type
     */
    ResponseVO getHomeInfo(String type);
    /**
     *
     * @return ResponseVO
     */
    ResponseVO ydqList(String index,int p,int size);
    /**
     *
     * @return ResponseVO
     */
    ResponseVO cywyList(CywyDTO dto);


    /**
     *
     * @return ResponseVO
     */
    ResponseVO wyFivelist(String index);

    /**
     *
     * @return ResponseVO
     */
    ResponseVO analysis();
    /**
     *
     * @return ResponseVO
     */
    ResponseVO special();

    /**
     *
     * @return ResponseVO
     */
    ResponseVO zdqy();

    /**
     *
     * @return ResponseVO
     */
    ResponseVO zdqyList(CywyDTO dto);
    /**
     *
     * @return ResponseVO
     */
    ResponseVO details(String code);

    /**
     *
     * @return ResponseVO
     */
    ResponseVO regTax(String id);

    /**
     *
     * @return ResponseVO
     */
    ResponseVO cyReTax();


    /**
     *
     * @return ResponseVO
     */
    ResponseVO zfAnalysis();

    /**
     *
     * @return ResponseVO
     */
    ResponseVO cyDetails(String id);

    /**
     *
     * @return ResponseVO
     */
    ResponseVO cyDetailsHouseInfo(CywyDTO dto);



    /**
     *
     * @return ResponseVO
     */
    ResponseVO cyWy(String index , Integer p , Integer size , Integer sort);

    /**
     *
     * @return ResponseVO
     */
    ResponseVO basicInfo();

    /**
     *
     * @return ResponseVO
     */
    ResponseVO zlAnalysis(CywyDTO dto);

    /**
     *
     * @return ResponseVO
     */
    ResponseVO moreAnalysis();
    /**
     *
     * @return ResponseVO
     */
    ResponseVO moreUnit(String index);

    /**
     *
     * @return ResponseVO
     */
    ResponseVO cyStreet();

    /**
     *
     * @return ResponseVO
     */
    ResponseVO mapStreet(String index);



    /**
     *
     * @return ResponseVO
     */
    ResponseVO KzDetailsList(CywyDTO dto);

    /**
     *
     * @return ResponseVO
     */
    ResponseVO MoreAnalysis(String  id);


    /**
     *
     * @return ResponseVO
     */
    ResponseVO RzDetailsList(String id);
    /**
     *
     * @return ResponseVO
     */
    ResponseVO RzQyDetailsList(CywyDTO dto);

    /**
     *
     * @return ResponseVO
     */
    ResponseVO wylistDetails(String id);
    /**
     *
     * @return ResponseVO
     */
    ResponseVO houseCode();

    /**
     *
     * @return ResponseVO
     */
    ResponseVO houseCodeList(HouseCodeDTO dto);



    /**
     *
     * @return ResponseVO
     */
    ResponseVO wyAssetsList(WyAssetsListDTO dto);


    /**
     *
     * @return ResponseVO
     */
    ResponseVO streetNameList();

    /**
     *
     * @return ResponseVO
     */
    ResponseVO communityNameList();


    /**
     *
     * @return ResponseVO
     */
    ResponseVO wyTypeList();

    /**
     *
     * @return ResponseVO
     */
    ResponseVO wyList(WyListDTO dto);


    /**
     *
     * @return ResponseVO
     */
    ResponseVO wyAnalysis(wyAnalysisDTO dto);

    /**
     *
     * @return ResponseVO
     */
    ResponseVO enterpriseSidewalk(int dto);

    /**
     *
     * @return ResponseVO
     */
    ResponseVO enterpriseList(EnterpriseDTO dto);

    ResponseVO enterpriseGqList(EnterpriseDTO dto);


    /**
     * 顶级园区接口
     * @param dto
     * @return
     */
    ResponseVO enterpriseTopWyName(EnterpriseDTO dto);

    ResponseVO enterpriseTalent(EnterpriseDTO dto);

    /**
     *
     * @param dto
     * @return
     */
    ResponseVO newrzqy(CywyDTO dto) throws ParseException;

    ResponseVO newrzqyfx(CywyDTO dto);

    ResponseVO newdqht(CywyDTO dto);

    ResponseVO newyqhy(CywyDTO dto);

    /**
     * 资产入账列表
     * @param dto
     * @return
     */
    ResponseVO newwyentry(CywyDTO dto);

    ResponseVO newwyequity(CywyDTO dto);

    /**
     * 统计资产入账数量
     * @param type
     * @return
     */
    int wyentrynamenumber(int type);

    /**
     * 统计产权办理数量
     * @param type
     * @return
     */
    int wyequitynumber(int type);

    ResponseVO allLongContract(CywyDTO dto);

    ResponseVO alldelayContract(CywyDTO dto);

    ResponseVO selectChildrenByTopWyName(CywyDTO dto);

    ResponseVO selectWySimpleNameByAssetType(CywyDTO dto);

    ResponseVO selectAllWyInfo(CywyDTO dto);

    ResponseVO notEntryWyInfo(CywyDTO dto);

    ResponseVO notCertHasWyInfo(CywyDTO dto);

    ResponseVO showLDInfo(CywyDTO dto);

    ResponseVO showLDEnterInfo(CywyDTO dto);

    ResponseVO showLDEmptyInfo(CywyDTO dto);

    ResponseVO showLDEntryAndEmptyInfo(CywyDTO dto);

    ResponseVO allNotCyLongContract(CywyDTO dto);

    ResponseVO allNotCyDelayContract(CywyDTO dto);

    ResponseVO emptyNotCyList(CywyDTO dto);

    ResponseVO KzDetailsList2(CywyDTO dto);

    ResponseVO showWyInfoByCompanyName(CywyDTO dto);

    ResponseVO selectWyInfoByManUnit(CywyDTO dto);

    ResponseVO selectWyInfoGuoqi(CywyDTO dto);

    ResponseVO selectWyDetailInfoGuoqi(CywyDTO dto);

    ResponseVO selectWyInfoGuoqiByUseStatus(CywyDTO dto);

    ResponseVO enterpriseByBelongType(EnterpriseDTO dto);

    ResponseVO rewardList(String index, Integer p, Integer size);

    ResponseVO certificateList(String index, Integer p, Integer size);

    ResponseVO patentList(String index, Integer p, Integer size);

    ResponseVO softWareCopyRightList(String index, Integer p, Integer size);

    ResponseVO subletLeaseList(String index, Integer p, Integer size);
}
