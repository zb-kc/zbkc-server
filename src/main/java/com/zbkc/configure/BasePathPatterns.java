package com.zbkc.configure;

import org.springframework.beans.factory.annotation.Value;

/**
 * 过滤路径
 * @author gmding
 * @date 2021-7-16
 * */

public class BasePathPatterns {


    //public final static  String urlPath="http://192.168.1.75:10008/api/v1/zbkc/";
    public final static  String urlPath="http://192.168.1.76:10005/api/v1/zbkc/";
    //public final static  String urlPath="http://10.200.62.5:7333/api/v1/zbkc/";
    //public final static  String urlPath="http://10.200.94.4:10005/api/v1/zbkc/";

    /**
     * 过滤拦截器路径,适合正则
     * @apiParam string
     *
     * */
    public final static String[] URL = {
            "/api/v1/zbkc/text/login(.*)",
            //"/api/v1/zbkc/text(.*)",
            "/api/v1/zbkc/swagger-resources(.*)",
            "/api/v1/zbkc/error",
            "/api/v1/zbkc/userInfo/login",
            "/api/v1/zbkc/userInfo/(.*)",
            "/api/v1/zbkc/userInfo/refreshToken",
            "/api/v1/zbkc/sms/(.*)",
            "/api/v1/zbkc/email/(.*)",
            "/api/v1/zbkc/(.*)",
            "/druid/*"

    };

}
