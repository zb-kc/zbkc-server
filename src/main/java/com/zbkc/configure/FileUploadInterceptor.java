package com.zbkc.configure;

import com.zbkc.common.enums.ErrorCodeEnum;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/11/12
 */
@Component
public class FileUploadInterceptor implements HandlerInterceptor {
    /**
     * 文件最大大小
     */
    private final long MAX_FILE_SIZE = 30 * 1024 * 1024;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (null != request && ServletFileUpload.isMultipartContent(request)) {
            long requestSize = new ServletRequestContext(request).contentLength();
            if (requestSize > MAX_FILE_SIZE) {
                throw new BusinessException(ErrorCodeEnum.FILE_MAX_SIZE_ERROR);
            }
        }
        return true;
    }
}
