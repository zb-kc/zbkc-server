package com.zbkc.configure;

import com.zbkc.common.utils.CreateFileUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import javax.annotation.Resource;

/**
 * 自定义拦截器配置
 *  @author gmding
 *  @date 2021-7-16
 * */

@Configuration
public class   MyInterceptorConfig extends WebMvcConfigurationSupport {

    @Resource
    private CreateFileUtil createFileUtil;
    /**
     * 提前new出 防止redis失效
     * @return
     */
    @Bean
    public MyInterceptor getMyInterceptor(){
        return new MyInterceptor();
    }

    @Bean
    public FileUploadInterceptor getFileUploadInterceptor(){
        return new FileUploadInterceptor();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getMyInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(getFileUploadInterceptor()).addPathPatterns("/**");
        super.addInterceptors(registry);
    }

    /**
     * 用来指定静态资源不被拦截，否则继承 WebMvcConfigurationSupport 这种方式会导致静态资源无法直接访问
     * @apiParam registry
     */
    @Override
    public  void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/static/");

        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");

        registry.addResourceHandler("/lib/**")
                .addResourceLocations("classpath:/META-INF/resources/lib/");

        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/templates/**").addResourceLocations("classpath:/templates/");

        // 或者从配置文件中取static-locations
        if(createFileUtil.isWindows()){
            registry.addResourceHandler("/**").addResourceLocations("file:D:/data/");
        }else {
            registry.addResourceHandler("/**").addResourceLocations("file:/home/file/");
        }

        super.addResourceHandlers(registry);
    }

}