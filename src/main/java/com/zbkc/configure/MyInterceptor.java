package com.zbkc.configure;

import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.DesUtils;
import com.zbkc.common.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 自定义拦截器
 * @author gmding
 * @date 2021-7-16
 */

@Slf4j
public class MyInterceptor implements HandlerInterceptor {

    @Resource
    private RedisUtil redisUtil;
    @Resource
    private DesUtils utils;

    public final SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss'.'SSS");
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws BusinessException {
        log.info("startTime:{}",formatter.format(new Date(System.currentTimeMillis())));
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        HandlerMethod handlerMethod = null;
        
        try {
            handlerMethod= (HandlerMethod) handler;
        }catch (Exception e){
            return true;
        }
        
        Method method = handlerMethod.getMethod();
        String methodName = method.getName();

        String path=request.getContextPath()+request.getServletPath();

        log.info("path:{}",path);
        log.info("methodName:{}",methodName);

        for (String url:BasePathPatterns.URL){
            boolean flag=path.matches(url);
            if (flag){
                return true;
            }
        }

        String authorization = request.getHeader("Authorization");
        String token=authorization.replaceAll( "bearer","").trim();
        if (null == token || "".equals(token)){
            throw  new BusinessException(ErrorCodeEnum.UNAUTHORIZED);
        }
        String[] str=DesUtils.decrypt(token).split( "\\|" );

        Map<Object, Object> vo=  redisUtil.hmget( str[0] );
        if (null==vo||"".equals(vo)){
            throw  new BusinessException(ErrorCodeEnum.TOKEN_NOT_FOUND);
        }
        if (!vo.get( "accessToken" ).toString().equals(token)){
            throw  new BusinessException(ErrorCodeEnum.TOKEN_NOT_FOUND);
        }

        long tr= (Long.valueOf( str[1])-System.currentTimeMillis())/1000;
        if (tr>utils.EXPIRESIN){
            throw  new BusinessException(ErrorCodeEnum.OFFSITELOGIN);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        //log.info("执行完方法之后进执行(Controller方法调用之后)，但是此时还没进行视图渲染");
        //log.info("方法执行完时间:{}",formatter.format(new Date(System.currentTimeMillis())));
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if(log.isDebugEnabled()) {
            log.info("endTime:{}", formatter.format(new Date(System.currentTimeMillis())));
        }


    }



}
