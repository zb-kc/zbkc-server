package com.zbkc.common.utils;

import ch.ethz.ssh2.*;
import com.mysql.cj.util.StringUtils;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 文件下载工具类
 * @author ZB3436.xiongshibao
 * @date 2021/10/14
 */
@Component
public class FileUtils {

    /**
     * 将流转为字节数组
     * @param inStream InputStream
     * @return 字节数组
     * @throws Exception io异常
     */
    public static byte[] readStream(InputStream inStream) throws Exception{
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = -1;
        while((len = inStream.read(buffer)) != -1){
            outStream.write(buffer, 0, len);
        }
        outStream.close();
        inStream.close();
        return outStream.toByteArray();
    }

    /**
     * 打包下载文件夹
     * @param response HttpServletResponse
     * @param files 文件map
     * @param zipName 压缩文件名
     */
    public static void downloadBatchByFile(HttpServletResponse response, Map<String, byte[]> files, String zipName){
        BufferedOutputStream bos = null;
        try{
            response.reset();
            zipName = java.net.URLEncoder.encode(zipName, "UTF-8");
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setHeader("Content-Disposition", "attachment;filename=" + zipName + ".zip");

            ZipOutputStream zos = new ZipOutputStream(response.getOutputStream());
            bos = new BufferedOutputStream(zos);

            for(Map.Entry<String, byte[]> entry : files.entrySet()){
                //每个zip文件名
                String fileName = entry.getKey();
                //这个zip文件的字节
                byte[] file = entry.getValue();

                BufferedInputStream bis = new BufferedInputStream(new ByteArrayInputStream(file));
                zos.putNextEntry(new ZipEntry(fileName));

                int len = 0;
                byte[] buf = new byte[10 * 1024];
                while( (len=bis.read(buf, 0, buf.length)) != -1){
                    bos.write(buf, 0, len);
                }
                bis.close();
                bos.flush();
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            try {
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 根据urlString获取文件
     * @param urlString http://XXXX
     * @return 字节数组
     */
    @SneakyThrows
    public static byte[] getRemoteFile(String urlString) {

        byte[] temp = null;
        InputStream is = null;

        if(!urlString.startsWith("http")) {
            File file = new File(urlString);
            is = new BufferedInputStream(new FileInputStream(file));
            return readStream(is);
        }

        try {
            // 构造URL
            java.net.URL url = new java.net.URL(urlString);
            // 打开连接
            URLConnection con = url.openConnection();
            // 输入流
            is = con.getInputStream();
            temp = readStream(is);

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (null != is){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return temp;
    }

    /**
     * 远程下载服务器文件
     * @param ip    ip
     * @param port 端口
     * @param userName  用户名
     * @param password  密码
     * @param sourceFile    源文件
     * @param targetFile    目标文件
     * @param targetFileName    指定文件名
     * @return boolean
     */
    public static boolean copyFile(String ip,int port,String userName,String password,String sourceFile,String targetFile,String targetFileName){
        boolean bool = false;
        Connection conn = null;
        Session session = null;
        try {
            if (StringUtils.isNullOrEmpty(ip) || StringUtils.isNullOrEmpty(userName) || StringUtils.isNullOrEmpty(password) ||
                    StringUtils.isNullOrEmpty(sourceFile) || StringUtils.isNullOrEmpty(targetFile)){
                return bool;
            }
            conn = new Connection(ip,port);
            conn.connect();
            boolean isAuth = conn.authenticateWithPassword(userName,password);
            if (!isAuth){
                return bool;
            }
            //执行命令
            session = conn.openSession();

            //执行命令并打印执行结果
            session.execCommand("df -h");
            InputStream staout = new StreamGobbler(session.getStdout());
            BufferedReader br = new BufferedReader(new InputStreamReader(staout));
            String line = null;
            while ((line = br.readLine()) != null){
                System.out.println(line);
            }
            br.close();

            //下载文件到本地
            SCPClient scpClient = conn.createSCPClient();
            SCPInputStream scpis = scpClient.get(sourceFile);

            //判断指定目录是否存在，不存在则先创建目录
            File file = new File(targetFile);
            if (!file.exists()) {
                file.mkdirs();
            }

            FileOutputStream fos = new FileOutputStream(targetFile + targetFileName);
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = scpis.read(buffer)) != -1){
                fos.write(buffer,0,len);
            }
            fos.close();
            bool = true;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (null != session){
                session.close();
            }
            if (null != conn) {
                conn.close();
            }
        }

        return bool;
    }

    /**
     * 浏览器下载文件
     * @param filename 文件名
     * @param inputStream 输入流
     * @param response response对象
     * @throws Exception 异常
     */
    public static void buildFile(String filename, InputStream inputStream, HttpServletResponse response) throws Exception {
        byte[] buffer = new byte[1024];
        int len;

        response.setContentType("application/x-download");
        // 处理编码问题
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-Disposition",
                "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1"));
        OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());

        while((len = inputStream.read(buffer) ) >0){
            outputStream.write(buffer , 0 , len);
        }

        inputStream.close();
        outputStream.flush();
        outputStream.close();
    }


}
