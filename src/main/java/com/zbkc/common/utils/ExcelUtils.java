package com.zbkc.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/12
 */
@Component
public class ExcelUtils {
    /**
     * 将实体数据拼接为excel表
     * @param headers   表头
     * @param values    与表头关联的属性信息
     * @param list  实体数据
     * @param fileName  文件名称
     */
    public static void getExcel(HttpServletResponse response, String[] headers, String[] values, List<?> list, String fileName) throws Exception {

        //创建一个HSSFWorkbook，对应一个Excel文件
        HSSFWorkbook wb = new HSSFWorkbook();

        //在workbook中添加一个sheet,对应Excel文件中的sheet
        HSSFSheet hssfSheet = wb.createSheet("sheet1");

        // 设置标题样式
        HSSFCellStyle style = wb.createCellStyle();


        //设置为居中加粗
        HSSFFont font = wb.createFont();
        font.setBold(true);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFont(font);

        // 设置单元格边框样式
        // 上边框 细边线 下边框 细边线 左边框 细边线 右边框 细边线
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);

        // 设置字体样式  字体高度
        Font titleFont = wb.createFont();
        titleFont.setFontHeightInPoints((short) 15);
        titleFont.setFontName("黑体");
        style.setFont(titleFont);



        //产生表头
        HSSFRow row1 = hssfSheet.createRow(0);
        //设置列宽，setColumnWidth的第二个参数要乘以256，这个参数的单位是1/256个字符宽度

//        hssfSheet.setColumnWidth(1, 12 * 256);
//        hssfSheet.setColumnWidth(3, 17 * 256);

        //设置行高40px
        row1.setHeight((short)(20*40));
        row1.setHeightInPoints((float)40);

        //设置列宽100px
        hssfSheet.setColumnWidth(0, (int)20*300);




        for (int i = 0; i < headers.length; i++) {
            HSSFCell hssfCell = row1.createCell(i);
            hssfCell.setCellValue(headers[i]);
            hssfCell.setCellStyle(style);
        }

        //需要显示的字段名
        List<String> valueList = Arrays.asList(values);

        HSSFCell hssfCell = null;
        Field[] declaredFields = null;

        Map<String , Object> declaredMap = null;
        for (int i = 0; i < list.size(); i++) {
            row1 = hssfSheet.createRow(i + 1);

            Object obj = list.get(i);
            declaredFields = obj.getClass().getDeclaredFields();

            declaredMap = new HashMap<>(declaredFields.length);
            for (Field declaredField : declaredFields) {
                declaredField.setAccessible(true);
                String name = declaredField.getName();
                if (!valueList.contains(name)) {
                    continue;
                }

                Object tempObj = declaredField.get(obj);
                if (null == tempObj) {
                    tempObj = " ";
                }

                declaredMap.put(name, tempObj);
            }

            int j = 0;
            for (String value : values) {
                hssfCell = row1.createCell(j);
                Object o = declaredMap.get(value);
                hssfCell.setCellValue(null != o ? o.toString() : "");
                hssfCell.setCellStyle(style);
                j++;
            }

        }

        if(StringUtils.isEmpty(fileName)){
            fileName = String.valueOf(System.currentTimeMillis());
        }

        if (response!=null) {
            //生成excel文件
            buildExcelFile(fileName, wb);

            //浏览器下载excel
            buildExcelDocument(fileName, wb, response);

            //删除临时文件
            WordUtils.deleteFile(fileName);
        }else {
            //生成excel文件
            buildExcelFile(fileName, wb);
            System.out.println("success");
        }


    }

    /**
     * 生成excel文件
     * @param filename 文件名
     * @param workbook  组装的excel对象
     * @throws Exception 异常信息
     */
    private static void buildExcelFile(String filename, HSSFWorkbook workbook) throws Exception {
        FileOutputStream fos = new FileOutputStream(filename);
        workbook.write(fos);
        fos.flush();
        fos.close();
    }

    /**
     * 浏览器下载excel
     * @param filename  文件名
     * @param workbook  组装的excel对象
     * @param response  response对象
     * @throws Exception    异常
     */
    private static void buildExcelDocument(String filename, HSSFWorkbook workbook, HttpServletResponse response) throws Exception {


        response.setContentType("application/x-download");
        // 处理编码问题
        response.setCharacterEncoding("utf-8");
//        response.setHeader("Content-Disposition",
//                "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1") + ".xls");
        response.setHeader("Content-Disposition",
                "attachment;filename=" + new String(filename.getBytes("gbk"), "iso8859-1"));



        OutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }

}
