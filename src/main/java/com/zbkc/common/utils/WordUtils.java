package com.zbkc.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTFonts;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/12
 */
@Component
public class WordUtils {

    /**
     * 根据模板生成新word文档
     * 判断表格是需要替换还是需要插入，判断逻辑有$为替换，表格无$为插入
     * @param outputUrl 新文档存放地址
     * @param textMap 需要替换的信息集合
     * @param tableList 需要插入的表格信息集合
     * @param inputStream 模板存放地址
     */
    public static boolean changWord(String outputUrl,
                                    Map<String, String> textMap, List<String[]> tableList, HttpServletResponse response, InputStream inputStream) {
        //模板转换默认成功
        boolean changeFlag = true;
        try {
            //获取docx解析对象
            XWPFDocument document = new XWPFDocument(inputStream);
            //解析替换文本段落对象
            WordUtils.changeText(document, textMap);
            //解析替换表格对象
            WordUtils.changeTable(document, textMap, tableList);
            //生成新的word
            File file = new File(outputUrl);
            FileOutputStream stream = new FileOutputStream(file);
            document.write(stream);
            stream.flush();
            stream.close();

            //浏览器下载
            response.setContentType("application/x-download");
            // 处理编码问题
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-Disposition",
                    "attachment;filename=" + new String(outputUrl.getBytes("gbk"), "iso8859-1"));
            OutputStream outputStream = response.getOutputStream();
            document.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            changeFlag = false;

        }finally {
            //删除临时文件
            deleteFile(outputUrl);
        }

        return changeFlag;

    }

    /**
     * 文件删除
     * @param fileName 文件目录+名称
     * @return
     */
    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                System.out.println("删除单个文件" + fileName + "成功！");
                return true;
            } else {
                System.out.println("删除单个文件" + fileName + "失败！");
                return false;
            }
        } else {
            System.out.println("删除单个文件失败：" + fileName + "不存在！");
            return false;
        }
    }

    /**
     * 替换段落文本
     * @param document docx解析对象
     * @param textMap 需要替换的信息集合
     */
    public static void changeText(XWPFDocument document, Map<String, String> textMap){
        //获取段落集合
        List<XWPFParagraph> paragraphs = document.getParagraphs();
        for (XWPFParagraph paragraph : paragraphs) {
            //判断此段落时候需要进行替换
            String text = paragraph.getText();
            if(checkText(text)){
                List<XWPFRun> runs = paragraph.getRuns();
                for (XWPFRun run : runs) {
                    //替换模板原来位置
                    // run.setText(changeValue(run.toString(), textMap),0);
                    String textValue = changeValue(run.toString(), textMap);
                    run.setText(textValue,0);
                }
            }
        }
    }

    /**
     * 替换表格对象方法
     * @param document docx解析对象
     * @param textMap 需要替换的信息集合
     * @param tableList 需要插入的表格信息集合
     */
    public static void changeTable(XWPFDocument document, Map<String, String> textMap,
                                   List<String[]> tableList){
        //获取表格对象集合
        List<XWPFTable> tables = document.getTables();

        if(null == tables || tables.isEmpty()){
            return;
        }

        /**==============租赁缴款通知单的文档替换逻辑=============**/
        if(tables.size() > 1){
            for (XWPFTable table : tables) {
                if(table.getRows().size()>1){
                    //判断表格是需要替换还是需要插入，判断逻辑有#为替换，表格无#为插入
                    if(checkText(table.getText())){
                        List<XWPFTableRow> rows = table.getRows();
                        //遍历表格,并替换模板
                        eachTable(rows, textMap);
                    }
                }
            }
            return;
        }

        /**==============合同签署呈批表的文档替换逻辑=============**/
        //只处理行数大于等于2的表格，且不循环表头
        XWPFTable table = tables.get(0);
        if(table.getRows().size()>1){
            //判断表格是需要替换还是需要插入，判断逻辑有#为替换，表格无#为插入
            if(checkText(table.getText())){
                List<XWPFTableRow> rows = table.getRows();
                //遍历表格,并替换模板
                eachTable(rows, textMap);
            }
        }

        insertTable(table, tableList);
    }


    /**
     * 遍历表格
     * @param rows 表格行对象
     * @param textMap 需要替换的信息集合
     */
    public static void eachTable(List<XWPFTableRow> rows ,Map<String, String> textMap){
        for (XWPFTableRow row : rows) {
            List<XWPFTableCell> cells = row.getTableCells();
            for (XWPFTableCell cell : cells) {
                //判断单元格是否需要替换
                if(checkText(cell.getText())){
                    List<XWPFParagraph> paragraphs = cell.getParagraphs();
                    for (XWPFParagraph paragraph : paragraphs) {
                        List<XWPFRun> runs = paragraph.getRuns();
                        for (XWPFRun run : runs) {
                            String value = changeValue(run.toString(), textMap);

                            if(value.contains("\r")){
                                String[] splitArr = value.split("\r");
                                //先重置单元格内容
                                run.setText("" , 0);
                                for (String s : splitArr) {
                                    run.getCTR().addNewT().setStringValue(s);
                                    run.getCTR().addNewBr();
                                }
                            }else{
                                run.setText(value , 0);
                            }


                        }
                    }
                }
            }
        }
    }

    /**
     * 为表格插入数据，行数不够添加新行
     * @param table 需要插入数据的表格
     * @param tableList 插入数据集合
     */
    public static void insertTable(XWPFTable table, List<String[]> tableList){

        int originSize = table.getRows().size();

        //创建行,根据需要插入的数据添加新行，不处理表头
        for(int i = 0; i < tableList.size(); i++){
            XWPFTableRow row =table.createRow();
        }
        //遍历表格插入数据
        List<XWPFTableRow> rows = table.getRows();
        for(int i = originSize; i < rows.size(); i++){
            XWPFTableRow newRow = table.getRow(i);

            List<XWPFTableCell> cells = newRow.getTableCells();

            for(int j = 0; j < cells.size(); j++){
                XWPFTableCell cell = cells.get(j);

                XWPFParagraph xwpfParagraph1 = cell.addParagraph();
                XWPFRun run = xwpfParagraph1.createRun();

                //垂直居中
                xwpfParagraph1.setVerticalAlignment(TextAlignment.CENTER);
                if(j == 0){
                    //水平居中
                    xwpfParagraph1.setAlignment(ParagraphAlignment.CENTER);
                }

                String value = tableList.get(i - originSize)[j];
                run.setText(value, 0);
                run.getCTR().addNewBr();

                if(j == 1 && tableList.get(i - originSize).length > 2){
                    XWPFParagraph xwpfParagraph2 = cell.addParagraph();
                    xwpfParagraph2.setVerticalAlignment(TextAlignment.CENTER);
                    xwpfParagraph2.setAlignment(ParagraphAlignment.RIGHT);
                    //字体对齐方式：1左对齐 2居中3右对齐
                    xwpfParagraph2.setFontAlignment(3);
                    XWPFRun run2 = xwpfParagraph2.createRun();
                    run2.setText(tableList.get(i - originSize)[j+1] , 0);
                    CTFonts font = run2.getCTR().addNewRPr().addNewRFonts();
                    //中文 改变中文字体设置这个
                    font.setEastAsia("微软雅黑");
                    font.setAscii("微软雅黑");
                }

                CTFonts font = run.getCTR().addNewRPr().addNewRFonts();
                //中文 改变中文字体设置这个
                font.setEastAsia("微软雅黑");
                font.setAscii("微软雅黑");

            }
        }
    }
    /**
     * 判断文本中时候包含$
     * @param text 文本
     * @return 包含返回true,不包含返回false
     */
    public static boolean checkText(String text){
        boolean check  =  false;
        if(text.indexOf("#")!= -1){
            check = true;
        }
        return check;
    }
    /**
     * 匹配传入信息集合与模板
     * @param value 模板需要替换的区域
     * @param textMap 传入信息集合
     * @return 模板需要替换区域信息集合对应值
     */
    public static String changeValue(String value, Map<String, String> textMap){
        Set<Map.Entry<String, String>> textSets = textMap.entrySet();
        for (Map.Entry<String, String> textSet : textSets) {
            if(null == textSet.getValue() || StringUtils.isEmpty(textSet.getValue().replace(" " , ""))){
                continue;
            }

            //匹配模板与替换值 格式#key
            String key = "#"+textSet.getKey();
            if(value.indexOf(key)!= -1){
                //仅替换参数
                value = value.replace(key, textSet.getValue());
            }
        }
        //模板未匹配到区域替换为空
        if(checkText(value)){
            value = "";
        }
        return value;
    }

    public static void main(String[] args) {
        //模板文件地址
        String inputUrl = "F://test0.docx";
        //新生产的模板文件
        String outputUrl = "F://testBoke.docx";

        Map<String, String> testMap = new HashMap<String, String>();
        testMap.put("proName", "这是项目名称");
        testMap.put("applicantUnit", "这是申请单位");
        testMap.put("stageName", "这是阶段名称");

        List<String[]> testList = new ArrayList<String[]>();
        testList.add(new String[]{"申请","提交申请"});
        testList.add(new String[]{"材料预审","材料预审已通过"});
        testList.add(new String[]{"受理","XXXXXXXXXX"});
        testList.add(new String[]{"设计联审","请补充材料"});
        testList.add(new String[]{"申请","提交"});

//        WordUtils.changWord(inputUrl, outputUrl, testMap, testList , null, );
    }

}
