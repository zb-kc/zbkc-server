package com.zbkc.common.utils;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.Process;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author ZB3436.xiongshibao
 * @since 2021-9-16
 */
@Component
public class ActivitiUtils {

    /**
     * 获取节点map <目标节点， 源节点>
     * @param repositoryService 工作流资源服务类
     * @param processDefinitionEntity 流程定义实体
     * @return  map <目标节点， 源节点>
     */
    public static Map<String, String> getTargetAndSourceRef(RepositoryService repositoryService, ProcessDefinitionEntity processDefinitionEntity) {
        Collection<FlowElement> flowElements = getAllFlowElements(repositoryService, processDefinitionEntity);

        Map<String ,String > map = new HashMap<>();
        Iterator<FlowElement> iterator = flowElements.iterator();
        while (iterator.hasNext()){
            FlowElement next = iterator.next();
            if(next instanceof SequenceFlow){
                String sourceRef = ((SequenceFlow) next).getSourceRef();
                String targetRef = ((SequenceFlow) next).getTargetRef();

                map.put(targetRef , sourceRef);
            }
        }
        return map;
    }

    /**
     * 获取所有流转节点
     * @param repositoryService 工作流资源服务类
     * @param processDefinitionEntity   流程定义实体
     * @return  流程节点
     */
    public static Collection<FlowElement> getAllFlowElements(RepositoryService repositoryService, ProcessDefinitionEntity processDefinitionEntity) {
        BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinitionEntity.getId());
        Process process = bpmnModel.getProcesses().get(0);
        //获取所有节点
        return process.getFlowElements();
    }


    /**
     * 获取高亮线
     * @param processDefinitionEntity
     * @param historicActivityInstances
     * @return
     */
    public static List<String> getHighLightedFlows(ProcessDefinitionEntity processDefinitionEntity, List<HistoricActivityInstance> historicActivityInstances) {

        // 用以保存高亮的线flowId
        List<String> highFlows = new ArrayList<>();
        // 对历史流程节点进行遍历
        for (int i = 0; i < historicActivityInstances.size() - 1; i++) {
            // 得到节点定义的详细信息
            ActivityImpl activityImpl = processDefinitionEntity
                    .findActivity(historicActivityInstances.get(i)
                            .getActivityId());

            if(null == activityImpl){
                continue;
            }

            // 用以保存后需开始时间相同的节点
            List<ActivityImpl> sameStartTimeNodes = new ArrayList<>();
            ActivityImpl sameActivityImpl1 = processDefinitionEntity
                    .findActivity(historicActivityInstances.get(i + 1)
                            .getActivityId());
            // 将后面第一个节点放在时间相同节点的集合里
            sameStartTimeNodes.add(sameActivityImpl1);
            for (int j = i + 1; j < historicActivityInstances.size() - 1; j++) {
                // 后续第一个节点
                HistoricActivityInstance activityImpl1 = historicActivityInstances
                        .get(j);
                // 后续第二个节点
                HistoricActivityInstance activityImpl2 = historicActivityInstances
                        .get(j + 1);
                // 如果第一个节点和第二个节点开始时间相同保存
                if (activityImpl1.getStartTime().equals(
                        activityImpl2.getStartTime())) {
                    ActivityImpl sameActivityImpl2 = processDefinitionEntity
                            .findActivity(activityImpl2.getActivityId());
                    sameStartTimeNodes.add(sameActivityImpl2);
                } else {
                    // 有不相同跳出循环
                    break;
                }
            }
            // 取出节点的所有出去的线
            List<PvmTransition> pvmTransitions = activityImpl
                    .getOutgoingTransitions();
            // 对所有的线进行遍历
            for (PvmTransition pvmTransition : pvmTransitions) {
                ActivityImpl pvmActivityImpl = (ActivityImpl) pvmTransition
                        .getDestination();
                // 如果取出的线的目标节点存在时间相同的节点里，保存该线的id，进行高亮显示
                if (sameStartTimeNodes.contains(pvmActivityImpl)) {
                    highFlows.add(pvmTransition.getId());
                }
            }
        }
        return highFlows;
    }
}
