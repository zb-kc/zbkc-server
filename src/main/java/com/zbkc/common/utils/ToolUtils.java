package com.zbkc.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 文件工具类
 * @author gmding
 * @date 2021-08-10
 */
@Slf4j
@Component
public class ToolUtils {

    private final String regex ="[0-9A-Za-z]{5,20}$";
    private final String regex1 ="[0-9A-Za-z]{6,20}$";

    @Autowired
    private RedisUtil redisUtil;
    /**
     * 正则表达式 用户名5~20位数字或字母
     *
     * */
    public Boolean RegName(String username){
        if (username.matches( regex )){
            return true;
        }
        return false;

    }

    public Boolean RegPwd(String password){
        if (password.matches( regex1 )){
            return true;
        }
        return false;

    }

    /**
     *
     * 物业编号生成规则
     * */
    public String getPropertyCode(){
        String code="NS"+new DateUtil().getYear()+new DateUtil().getMonth();
        String num= redisUtil.get(code);
        if (num==null||"".equals(num)){
            redisUtil.set(code,"1",60*24*60*60);
            return code+"0001";
        }else {
            int count=Integer.valueOf(num)+1;
            redisUtil.set(code,""+count,60*24*60*60);
            return code+getCode(count);
        }


    }

    private String getCode(int count){
        if (count<10){
            return "000"+count;
        }else if (count<100&&count>=10){
            return "00"+count;
        }else if (count>=100&&count<1000){
            return "0"+count;
        }else if (count>=1000&&count<10000){
            return ""+count;
        }else {
            return str2HexStr(count);
        }
    }

    /**
     * 字符串转换成为16进制(无需Unicode编码)
     * @param str
     * @return
     */
    public String str2HexStr(int str) {

        String str1=Integer.toHexString(str).toUpperCase();
        for (int i=0;i<4;i++){
            if (i>str1.length()){
                str1="0"+str1.toString().trim();
            }
        }
        return str1;
    }

    /**
     * 将时间串转成Timestamp类型
     * @param str yyyy-MM-dd
     */
    public Timestamp strFormatTimetamp(String str){
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        format.setLenient(false);
        Timestamp ts = null;
        try {
            ts= new Timestamp(format.parse(str).getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ts;
    }


    /**
     * 将时间串转成Timestamp类型
     * @param str yyyy-MM-dd
     */
    public Date strFormatDate(String str) throws ParseException {

        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        Date date=null;
        try {
            date = ft.parse(str);
        }catch (ParseException e){
            e.printStackTrace();
        }
        return date;
    }


    public static void main(String[] arg) throws ParseException {

        String str="2019-09-11";
        ToolUtils toolUtils = new ToolUtils();
        Date date = toolUtils.strFormatDate(str);

        System.out.println();
        /*System.out.println(Integer.toHexString(12000).toUpperCase());
        System.out.println(new ToolUtils().getCode(10000));
        System.out.println(new ToolUtils().str2HexStr(120000));*/

    }


}
