package com.zbkc.common.utils;

import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.model.ToEmail;
import com.zbkc.model.vo.ResponseVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * 邮件工具类
 * @author yangyan
 * @date 2021/7/22
 */
@Component
public class EmailUtil {

//    @Autowired
//    private JavaMailSender mailSender;
//
//    @Value("${spring.mail.username}")
//    private String from;

//    public ResponseVO commonEmail(ToEmail toEmail) {
//        //创建简单邮件消息
//        SimpleMailMessage message = new SimpleMailMessage();
//        //谁发的
//        message.setFrom(from);
//        //谁要接收
//        message.setTo(toEmail.getTos());
//        //邮件标题
//        message.setSubject(toEmail.getSubject());
//        //邮件内容
//        message.setText(toEmail.getContent());
//        try {
//            mailSender.send(message);
//            return new ResponseVO(ErrorCodeEnum.SUCCESS, null);
//        } catch (MailException e) {
//            e.printStackTrace();
//            return new ResponseVO(ErrorCodeEnum.EMAIL_SEND_FAILD, null);
//        }
//    }
//
//    public ResponseVO htmlEmail(ToEmail toEmail) throws MessagingException {
//        //创建一个MINE消息
//        MimeMessage message = mailSender.createMimeMessage();
//        MimeMessageHelper minehelper = new MimeMessageHelper(message, true);
//        //谁发
//        minehelper.setFrom(from);
//        //谁要接收
//        minehelper.setTo(toEmail.getTos());
//        //邮件主题
//        minehelper.setSubject(toEmail.getSubject());
//        //邮件内容   true 表示带有附件或html
//        minehelper.setText(toEmail.getContent(), true);
//        try {
//            mailSender.send(message);
//            return new ResponseVO(ErrorCodeEnum.SUCCESS,null);
//        } catch (MailException e) {
//            e.printStackTrace();
//            return new ResponseVO(ErrorCodeEnum.EMAIL_SEND_FAILD, null);
//        }
  //  }
}
