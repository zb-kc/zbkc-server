package com.zbkc.common.utils;

import com.zbkc.model.po.WyBasicInfo;
import com.zbkc.model.vo.PropertyStructureVo;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 结构树 工具类
 * @author ZB3436.xiongshibao
 * @date 2021/12/17
 */
@Component
public class StructureUtils {

    /**
     * 获取物业结构树
     * @param allList 所有物业列表
     * @return 集合
     */
    public List<PropertyStructureVo> getPropertyStructureTree(List<PropertyStructureVo> allList){
        Long minId = Long.MAX_VALUE;
        Map<Long, List<PropertyStructureVo>> map = new HashMap<>();
        for (PropertyStructureVo propertyStructureVo : allList) {
            Long pid = propertyStructureVo.getPid();

            if (pid < minId) {
                minId = pid;
            }

            List<PropertyStructureVo> list = new ArrayList<>();
            if (map.containsKey(pid)) {
                list = map.get(pid);
            }
            list.add(propertyStructureVo);

            map.put(pid, list);
        }
        List<PropertyStructureVo> pidList = map.get(minId);
        return propertyStructureList(pidList, map);
    }

    /**
     * 递归 将物业集合 梳理成结构树样式
     * @param dto dto
     * @param map map
     * @return 结构树集合
     */
    private static List<PropertyStructureVo> propertyStructureList(List<PropertyStructureVo> dto, Map<Long, List<PropertyStructureVo>> map) {

        if (null == dto || dto.isEmpty()) {
            return null;
        }

        for (PropertyStructureVo vo : dto) {
            List<PropertyStructureVo> list = map.get(vo.getId());
            vo.setChildren(list);
            propertyStructureList(vo.getChildren(), map);
        }
        return dto;
    }


    /**
     * 统计当前物业树空置面积
     * @param list 物业结构树
     * @return 物业面积
     */
    public static Double countTotalEmptyArea(List<PropertyStructureVo> list){
        Double total = 0D;
        if(null != list){
            for (PropertyStructureVo structureVo : list) {
                if(null == structureVo.getChildren()){
                    //判断是否空置
                    if(structureVo.getIsEmpty() && null != structureVo.getBuildArea()){
                        total += structureVo.getBuildArea();
                    }
                    continue;
                }
                total += countTotalEmptyArea(structureVo.getChildren());

            }
        }
        return total;
    }

    /**
     * 根据结构树 重新设置 物业名称和物业地址
     * @param list list
     * @param wyName 物业单元名称
     * @param address 物业地址
     * @return 修改后的集合
     */
    public static void resetWyNameAndAddress(List<PropertyStructureVo> list , String wyName , String address , List<WyBasicInfo> totalWyBasicList){
        if(null != list && null != totalWyBasicList) {
            for (PropertyStructureVo structureVo : list) {
                String currentWyName = structureVo.getName();
                String currentUnitName= structureVo.getUnitName();

                WyBasicInfo wyBasicInfo = new WyBasicInfo();
                wyBasicInfo.setId(structureVo.getId());
                if(currentWyName.equals(currentUnitName)){
                    wyBasicInfo.setName(wyName);
                    wyBasicInfo.setUnitName(wyName);
                }else{
                    wyBasicInfo.setName(wyName + currentUnitName);
                    wyBasicInfo.setUnitName(currentUnitName);
                }
                wyBasicInfo.setAddress(address);
                totalWyBasicList.add(wyBasicInfo);

                if(null != structureVo.getChildren()){
                    resetWyNameAndAddress(structureVo.getChildren() ,wyBasicInfo.getName(), address,totalWyBasicList );
                }
            }
        }
    }



}
