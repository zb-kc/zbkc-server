package com.zbkc.common.utils;

import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * set集合操作工具类
 * @author ZB3436.xiongshibao
 * @date 2021/12/16
 */
@Component
public class SetOptUtils {

    /**
     * 取两数交集.
     * <P>
     * Example:
     *
     * <pre>
     * src={1,2,3},dest={2,4}
     * intersect(dest,src)={2}
     * </pre>
     *
     * @param dest
     *            The destination set.
     * @param src
     *            The source set.
     * @return the same elements of src and dest
     */
    public static <T> Set<T> intersect(Set<T> dest, Set<T> src) {
        Set<T> set = new HashSet<T>(src.size());
        copy(set, src);
        set.retainAll(dest);
        return set;
    }

    /**
     * 取两数并集.
     * <P>
     * Example:
     *
     * <pre>
     * src={1,2,3},dest={2,4,5}
     * union(dest,src)={1,2,3,4,5}
     * </pre>
     *
     * @param dest
     *            The destination set.
     * @param src
     *            The source set.
     * @return the all elements of src and dest
     */
    public static <T> Set<T> union(Set<T> dest, Set<T> src) {
        Set<T> set = new HashSet<T>(src.size());
        copy(set, src);
        set.addAll(dest);
        return set;
    }

    /**
     * 取两数差集(减法).
     * <P>
     * Example:
     *
     * <pre>
     * src={1,2,3},dest={2,4,5},src-dest={1，3}
     * diff(dest,src)={1，3}
     * </pre>
     *
     * @param dest
     *            The destination set.
     * @param src
     *            The source set.
     * @return the elements in src but not exist dest
     */
    public static <T> Set<T> diff(Set<T> dest, Set<T> src) {
        Set<T> set = new HashSet<T>(src.size());
        copy(set, src);
        set.removeAll(dest);
        return set;
    }

    /**
     * 集合判空.
     *
     * @param c
     *            The source collection.
     * @return true/false
     */
    public static boolean isEmpty(Collection<?> c) {
        boolean rs = false;
        if (c == null || (c != null && c.isEmpty())) {
            rs = true;
        }
        return rs;
    }

    /**
     * 判断两集合是否有相同的元素.
     * @param dest The destination set.
     * @param src The source list.
     * @return true/false
     */
    public static <T> boolean isSameElements(Set<T> dest, Set<T> src) {
        if (isEmpty(dest) || isEmpty(src)) {
            return false;
        }

        Set<T> set = intersect(dest, src);
        if (set.size() > 0) {
            return true;
        }

        return false;
    }

    /**
     * Copies all of the elements from src set into dest.
     *
     * @param dest
     *            The destination set.
     * @param src
     *            The source list.
     */
    private static <T> void copy(Set<T> dest, Set<T> src) {
        dest.addAll(src);
    }

    public static void main(String[] args) {
            Set<Long> set = new HashSet<Long>();
            Set<Long> set2 = new HashSet<Long>();

            set.add(1L);
            set.add(2L);

            set2.add(3L);
            System.out.println(diff(set, set2));
            System.out.println(diff(set2, set));
    }
}
