package com.zbkc.common.utils;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 合同下载模板 获取甲方信息
 * @author ZB3436.xiongshibao
 * @date 2021/12/8
 */
@Component
public class DownloadContractPartAInfoUtils {

    /**
     * 类型
     * 1产业用房租赁合同3+2（有经济贡献率）;
     * 2产业用房租赁合同3+2（无经济贡献率）;
     * 3产业用房租赁合同3年及3年以下;
     * 4商业租赁合同；
     * 5住宅租赁合同；
     * 6南山软件租赁合同
     */

    public static final Map<Byte , OrgInfo> BUSINESS_TYPE_AND_PART_A_MAP = new HashMap<>();

    public static final Map<Byte , OrgInfo> BUSINESS_TYPE_AND_PART_B_MAP = new HashMap<>();

    {
        //产业用房
        OrgInfo industrialBuildingsType1 = new OrgInfo(
            "深圳市大沙河建设投资有限公司",
            "91440300576363402K",
            "深圳市南山区南头街道大新路198号马家龙创新大厦406、503、B2401",
            "0755-86227921",
                (byte) 1,
            "郭伟博",
                (byte)1,
            "130406198701010398",
            "深圳市南山区南头街道大新路198号马家龙创新大厦406、503、B2401 ",
            "0755-86520706",
            "深圳市大沙河建设投资有限公司",
            "招商银行深圳南山支行",
            "755950053910688"
            );

        //TODO 待补充 商业 住宅

        //南山软件园
        OrgInfo softwareParkType4 = new OrgInfo(
                "深圳市南山区政府公共物业管理中心",
                "124403054557483654",
                "南山区桃园东路2号区委大楼A614",
                "26667159",
                (byte) 0,
                "",
                (byte)0,
                "",
                "",
                "",
                "深圳市南山区财政局",
                "中国银行股份有限公司深圳市分行",
                "752366183125"
        );

        //类型 1产业用房租赁合同3+2（有经济贡献率）;2产业用房租赁合同3+2（无经济贡献率）;3产业用房租赁合同3年及3年以下; 4商业租赁合同；5住宅租赁合同；6南山软件租赁合同
        BUSINESS_TYPE_AND_PART_A_MAP.put((byte) 1, industrialBuildingsType1);
        BUSINESS_TYPE_AND_PART_A_MAP.put((byte) 2, industrialBuildingsType1);
        BUSINESS_TYPE_AND_PART_A_MAP.put((byte) 3, industrialBuildingsType1);
        BUSINESS_TYPE_AND_PART_A_MAP.put((byte) 6, softwareParkType4);

        //申请业务类型 1产业用房租赁 2商业用房租赁 3住宅用房租赁 4南山软件园租赁
        BUSINESS_TYPE_AND_PART_B_MAP.put((byte) 1, industrialBuildingsType1);
        BUSINESS_TYPE_AND_PART_B_MAP.put((byte) 4, softwareParkType4);

    }

    /**
     * 根据申请业务类型获取甲方信息
     * @param businessType 申请业务类型 1产业用房租赁合同3+2（有经济贡献率）;
     * 2产业用房租赁合同3+2（无经济贡献率）;
     * 3产业用房租赁合同3年及3年以下;
     * 4商业租赁合同；
     * 5住宅租赁合同；
     * 6南山软件租赁合同
     * @return 甲方信息
     */
    public static OrgInfo getOrgInfoByBusinessType(Byte businessType){
        if(null == businessType){
            return new OrgInfo();
        }

        //合同表的类型 1产业用房租赁合同3+2（有经济贡献率）;2产业用房租赁合同3+2（无经济贡献率）;3产业用房租赁合同3年及3年以下; 4商业租赁合同；5住宅租赁合同；6南山软件租赁合同
        if(BUSINESS_TYPE_AND_PART_A_MAP.containsKey(businessType)){
            return BUSINESS_TYPE_AND_PART_A_MAP.get(businessType);
        }

        //业务表的类型 1产业用房租赁 2商业用房租赁 3住宅用房租赁 4南山软件园租赁
        if(BUSINESS_TYPE_AND_PART_B_MAP.containsKey(businessType)){
            return BUSINESS_TYPE_AND_PART_B_MAP.get(businessType);
        }

        return new OrgInfo();
    }

    /**
     * 给模板数据 添加甲方信息
     * @param testMap map
     * @param businessType 合同类型 1产业用房租赁合同3+2（有经济贡献率）;
     * 2产业用房租赁合同3+2（无经济贡献率）;
     * 3产业用房租赁合同3年及3年以下;
     * 4商业租赁合同；
     * 5住宅租赁合同；
     * 6南山软件租赁合同
     */
    public static void addPartAInfo(Map<String, String> testMap, Byte businessType) {
        if(null == businessType){
            return;
        }

        DownloadContractPartAInfoUtils.OrgInfo partAInfo = getOrgInfoByBusinessType(businessType);

        testMap.put("openingBank",partAInfo.getOpeningBank());
        testMap.put("openingBankName",partAInfo.getOpeningBankName());
        testMap.put("openingAccount",partAInfo.getOpeningAccount());

        testMap.put("orgName",partAInfo.getOrgName());
        testMap.put("orgSoicalCode",partAInfo.getSocialUniformCreditCode());
        testMap.put("orgAddress",partAInfo.getAddress());
        testMap.put("orgPhone",partAInfo.getPhone());

        //1 委托代理人 2法定代表人
        String representativeType = "";
        switch (partAInfo.getRepresentativeType()){
            case (byte)1:
                representativeType = "委托代理人";
                break;
            case (byte)2:
                representativeType = "法定代表人";
                break;
            default:
                break;
        }
        testMap.put("representativeType",representativeType);

        //1居民身份证2护照3其他
        String certificateType = "";
        switch (partAInfo.getCertificateType()){
            case (byte)1:
                certificateType = "居民身份证";
                break;
            case (byte)2:
                certificateType = "护照";
                break;
            case (byte)3:
                certificateType = "其他";
                break;
            default:
                break;
        }

        testMap.put("certificateType",certificateType);
        testMap.put("certificateCode",partAInfo.getCertificateCode());
        testMap.put("representativeAddress",partAInfo.getRepresentativeAddress());
        testMap.put("representactivePhone",partAInfo.getRepresentativePhone());
    }



    public static class OrgInfo implements Serializable {

        /**
         * 组织名称（全称）
         */
        private String orgName;
        /**
         * 统一社会信用代码
         */
        private String socialUniformCreditCode;

        /**
         * 通讯地址
         */
        private String address;

        /**
         * 联系电话
         */
        private String phone;

        /**
         * 代表人类型
         * 1 委托代理人 2法定代表人
         */
        private byte representativeType;

        /**
         * 代理人姓名
         */
        private String representativeName;

        /**
         * 证件类型
         * 1居民身份证2护照3其他
         */
        private byte certificateType;

        /**
         * 证件号码
         */
        private String certificateCode;

        /**
         * 代表人通讯地址
         */
        private String representativeAddress;

        /**
         * 代表人联系电话
         */
        private String representativePhone;

        /**
         * 开户行
         */
        private String openingBank;
        /**
         * 户名
         */
        private String openingBankName;

        /**
         * 账号
         */
        private String openingAccount;


        public OrgInfo() {
        }

        public OrgInfo(
                String orgName,
                String socialUniformCreditCode,
                String address,
                String phone,
                byte representativeType,
                String representativeName,
                byte certificateType,
                String certificateCode,
                String representativeAddress,
                String representativePhone,
                String openingBank,
                String openingBankName,
                String openingAccount) {
            this.orgName = orgName;
            this.socialUniformCreditCode = socialUniformCreditCode;
            this.address = address;
            this.phone = phone;
            this.representativeType = representativeType;
            this.representativeName = representativeName;
            this.certificateType = certificateType;
            this.certificateCode = certificateCode;
            this.representativeAddress = representativeAddress;
            this.representativePhone = representativePhone;
            this.openingBank = openingBank;
            this.openingBankName = openingBankName;
            this.openingAccount = openingAccount;
        }


        public String getOrgName() {
            return orgName;
        }

        public void setOrgName(String orgName) {
            this.orgName = orgName;
        }

        public String getSocialUniformCreditCode() {
            return socialUniformCreditCode;
        }

        public void setSocialUniformCreditCode(String socialUniformCreditCode) {
            this.socialUniformCreditCode = socialUniformCreditCode;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public byte getRepresentativeType() {
            return representativeType;
        }

        public void setRepresentativeType(byte representativeType) {
            this.representativeType = representativeType;
        }

        public String getRepresentativeName() {
            return representativeName;
        }

        public void setRepresentativeName(String representativeName) {
            this.representativeName = representativeName;
        }

        public byte getCertificateType() {
            return certificateType;
        }

        public void setCertificateType(byte certificateType) {
            this.certificateType = certificateType;
        }

        public String getCertificateCode() {
            return certificateCode;
        }

        public void setCertificateCode(String certificateCode) {
            this.certificateCode = certificateCode;
        }

        public String getRepresentativeAddress() {
            return representativeAddress;
        }

        public void setRepresentativeAddress(String representativeAddress) {
            this.representativeAddress = representativeAddress;
        }

        public String getRepresentativePhone() {
            return representativePhone;
        }

        public void setRepresentativePhone(String representativePhone) {
            this.representativePhone = representativePhone;
        }

        public String getOpeningBank() {
            return openingBank;
        }

        public void setOpeningBank(String openingBank) {
            this.openingBank = openingBank;
        }

        public String getOpeningBankName() {
            return openingBankName;
        }

        public void setOpeningBankName(String openingBankName) {
            this.openingBankName = openingBankName;
        }

        public String getOpeningAccount() {
            return openingAccount;
        }

        public void setOpeningAccount(String openingAccount) {
            this.openingAccount = openingAccount;
        }
    }

}
