package com.zbkc.common.utils;


import com.zbkc.model.dto.export.ExportPreContactDTO;
import com.zbkc.service.YyContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author yangyan
 * @date 2021/10/18
 */
@Component
public class ScheduledJobMonth  {
    @Autowired
    private YyContractService yyContractService;

    /**
     * 每个月1号8点导出预到期合同报表
     */
    @Scheduled(cron = "0 0 8 1 * ?")
    public  void exportContractEveryMonth() throws Exception {
        ExportPreContactDTO dto = new ExportPreContactDTO();
        dto.setNeedShowKeys("id,wyName,leaseUse,rentalArea,monthlyRent,rentUnitPrice,limitDay,leaser,startRentDate,endRentDate,year,legalPerson,legalPhone,clientPerson,clientPhone,remark");

        yyContractService.preContractTwoMonth(dto,null);
    }

    /**
     * 每个月1号8点半导出产业用房复核报表
     */
    @Scheduled(cron = "0 5 11 19 * ?")
    public  void exportIndustEveryMonth() throws Exception {
        ExportPreContactDTO dto = new ExportPreContactDTO();
        dto.setNeedShowKeys("id,wyName,leaseUse,rentalArea,monthlyRent,rentUnitPrice,type,name,startRentDate,endRentDate,year,legalPerson,legalPhone,clientPerson,clientPhone,remark,limitDay,depositAmount");
        yyContractService.industReviewThreeMonth(dto,null);
    }
}
