package com.zbkc.common.enums;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/12/16
 */
public enum FinalSysAdminRoleEnum {

    /**
     * 超级管理员
     */
    SUPER_ADMIN(1L, "超级管理员")
    ;

    private Long roleId;
    private String roleName;

    FinalSysAdminRoleEnum(Long roleId, String roleName) {
        this.roleId = roleId;
        this.roleName = roleName;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * 校验是否是管理员
     * @param roleId 权限id
     * @return true 是 false 不是
     */
    public static boolean checkIsAdmin(Long roleId){

        FinalSysAdminRoleEnum[] allSysAdminRole = FinalSysAdminRoleEnum.values();
        for (FinalSysAdminRoleEnum finalSysAdminRoleEnum : allSysAdminRole) {
            if(finalSysAdminRoleEnum.getRoleId().equals(roleId)){
                return true;
            }
        }

        return false;
    }

}
