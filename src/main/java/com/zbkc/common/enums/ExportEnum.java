package com.zbkc.common.enums;


/**
 * @author caobiyang
 * @date 2021/10/29
 */
public enum ExportEnum {

    /**
     * 缴费状态  缴费状态 1.未缴费、2.部分缴费、3.已缴费、4.欠缴费、5.已补缴、6.已缴费(预缴费转)
     */
    NOT_PAYMENT(1, "未缴费"),
    PARTIAL_PAYMENT(2, "部分缴费"),
    ALREADY_PAYMENT(3, "已缴费"),
    OWE_PAYMENT(4, "欠缴费"),
    ALREADY_MAKE_UP_PAYMENT(5, "已补缴"),
    PREPARE_PAYMENT(6, "已缴费(预缴费转)"),

    /**
     * 发票状态
     */
    NOT_INVOICED(1, "未开票"),
    ALREADY_INVOICED(2, "已开票"),


    /**
     * 短信状态
     *
     * @param code
     * @param name
     */
    START_USING(1, "启用"),
    PROHIBIT_USING(2, "禁用"),

    /**
     * 使用状态
     */
    IN_USE(1, "在用"),
    STOP_USE(2, "停用"),

    /**
     * 使用方式
     */
    LEASE(1, "出租"),
    DEPLOYMENT(2, "调配"),

    /**
     * 房屋租赁用途
     */
    OFFICE(1, "办公"),
    COMPREHENSIVE_RESEARCH(2, "综合（研发）"),
    INDUSTRY_RESEARCH(3, "工业研发"),
    RESEARCH_AND_DEVELOPMENT(4, "研发");

    ExportEnum(Object code, String name) {
        this.code = code;
        this.name = name;
    }

    private Object code;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getCode() {
        return code;
    }

    public void setCode(Object code) {
        this.code = code;
    }
}
