package com.zbkc.common.enums;

/**
 * 移交用房附件信息
 * @author ZB3436.xiongshibao
 * @date 2021/11/8
 */
public enum YJHouseFileNameEnum {
    /**
     * 移交用房附件信息
     */
    File_1(1,	""	),
    File_2(2,	""	),
    File_3(3,	""	),
    File_4(4,	"设计方案报建函及附图"	),
    File_5(5,	"开发商授权五件套"	),
    File_6(6,	"土地出让合同及补充协议"	),
    File_7(7,	"设计方案蓝图"	),
    File_8(8,	"创新型产业用房监管协议"	),
    File_9(9,	"公配物业设计方案审核阶段移交申请表"	),

    ;

    private int id;
    private String fileName;

    YJHouseFileNameEnum(int id, String fileName) {
        this.id = id;
        this.fileName = fileName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
