package com.zbkc.common.enums;

/**
 * 物业资产申报附件信息
 * @author ZB3436.xiongshibao
 * @date 2021/11/8
 */
public enum PropertyApplyFileNameEnum {
    /**
     * 物业资产申报附件信息
     */
    File_1(1,	"物业资产上报表加盖公章扫描件"	),
    File_2(2,	"办公用房信息表电子版"	),
    File_3(3,	"办公用房信息表签字加盖公章扫描件"	),

    ;

    private int id;
    private String fileName;

    PropertyApplyFileNameEnum(int id, String fileName) {
        this.id = id;
        this.fileName = fileName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
