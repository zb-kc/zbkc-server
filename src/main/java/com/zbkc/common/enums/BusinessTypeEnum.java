package com.zbkc.common.enums;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/25
 */
public enum BusinessTypeEnum {
    /**
     * 流程状态 0刚授权 1企业申请 2企业撤回
     */
    PROCESS_NEW_RECORD ( 0, "刚授权") ,
    PROCESS_APPLY (1, "企业申请") ,
    PROCESS_APPLY_UNDO ( 2, "企业撤回") ,
    ADMIN_CANCEL_AUTHORIZATION ( 3, "管理员取消授权") ,
    PROCESS_COMPLETE ( 4, "已办结") ,
    /**
     * 申请来源：1现场申请（管理端合同新增） 2线上申请（门户）
     */
    SIGN_SOURCE_SIT_APPLY ( "1", "现场申请"),
    SIGN_SOURCE_ONLINE_APPLY ( "2" , "线上申请") ,

    //移交用房申请
    DELIVERY_HOUSE_APPLY(1 ,"DELIVERY_HOUSE_APPLY" ),
    //物业资产申报
    PROPERTY_APPLY(2,"PROPERTY_APPLY" ),
    //合同签署申请-运营类
    CONTRACT_SIGN_APPLY_AGENCY(3,"CONTRACT_SIGN_APPLY_AGENCY" ),
    //合同签署申请-非运营类
    CONTRACT_SIGN_APPLY_NORMAL(4,"CONTRACT_SIGN_APPLY_NORMAL" ),

    //待办业务
    TODO (1 , "todo"),
    //所有业务
    ALL (2 , "all"),

    /**
     * 新签
     * 续签
     */
    NEW_SIGN (1 , "新签"),
    RENEW_SIGN (2 , "续签"),

    /**
     * 审核状态 0未审核 1审核通过 2有问题
     */

    HANDOVER_DETAILS_STATUS_NEW (0 , "未审核"),
    HANDOVER_DETAILS_STATUS_OK (1 , "审核通过"),
    HANDOVER_DETAILS_STATUS_ERROR (2 , "有问题");

    BusinessTypeEnum(Object code, String name) {
        this.code = code;
        this.name = name;
    }

    private Object code;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getCode() {
        return code;
    }

    public void setCode(Object code) {
        this.code = code;
    }
}
