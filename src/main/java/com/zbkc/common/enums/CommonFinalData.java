package com.zbkc.common.enums;

/**
 * 通用静态数据
 * @author ZB3436.xiongshibao
 * @date 2021/12/16
 */
public final class CommonFinalData {

    /**
     * property_status
     * 以下 数字对应的是label_manage中的label_id
     * 在用 40
     * 空置 41
     * 拟使用 42
     */
    public static final Long PROPERTY_STATUS_1 = 40L;
    public static final Long PROPERTY_STATUS_2 = 41L;
    public static final Long PROPERTY_STATUS_3 = 42L;

    /**
     * 使用方式 1出租; 2调配
     */
    public static final Integer USE_REG_METHOD_1 = 1;
    public static final Integer USE_REG_METHOD_2 = 2;

    /**
     * 使用状态 1 在用; 2停用
     */
    public static final Integer USE_REG_STATUS_1 = 1;
    public static final Integer USE_REG_STATUS_2 = 2;

    /**
     * 是否免租 1是；2否
     */
    public static final Integer USE_REG_RENT_FREE_1 = 1;
    public static final Integer USE_REG_RENT_FREE_2 = 2;

    /**
     * 代办类型 1移交用房申请；2合同签署申请；3物业资产申报;
     */
    public static final Integer YW_BUSINESS_AGENCY_TYPE_1 = 1;
    public static final Integer YW_BUSINESS_AGENCY_TYPE_2 = 2;
    public static final Integer YW_BUSINESS_AGENCY_TYPE_3 = 3;





}


