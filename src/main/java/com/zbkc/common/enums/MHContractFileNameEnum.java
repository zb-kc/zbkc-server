package com.zbkc.common.enums;

/**
 * 门户合同签署申请 附件名称
 * @author ZB3436.xiongshibao
 * @date 2021/10/21
 */
public enum MHContractFileNameEnum {

    /**
     * 合同签署申请 附件名称
     */
    File_1(1,	"入驻通知书"	),
    File_2(2,	"营业执照复印件"	),
    File_3(3,	"法人身份证复印件"	),
    File_4(4,	"股东证明书"	),
    File_5(5,	"法人代表证明书"	),
    File_6(6,	"法人授权委托书"	),
    File_7(7,	"经办人身份证复印件"	),
    File_8(8,	"续签通知书"	),
    ;

    private int id;
    private String fileName;

    MHContractFileNameEnum(int id, String fileName) {
        this.id = id;
        this.fileName = fileName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
