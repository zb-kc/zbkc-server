package com.zbkc.mapper;

import com.zbkc.model.dto.WyProRightHomeDTO;
import com.zbkc.model.dto.homeList.WyProRightHomeList;
import com.zbkc.model.po.WyProRightReg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 产权登记表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-08-06
 */
@Repository
public interface WyProRightRegMapper extends BaseMapper<WyProRightReg> {

    /**
     * @param id 主键id
     * @return  Integer
     */
    @Update("UPDATE wy_pro_right_reg SET status = 1 WHERE id = #{id}")
    Integer statusOpen(@Param("id") Long id);

    /**
     * @param id 主键id
     * @return Integer
     */
    @Update("UPDATE wy_pro_right_reg SET status = 2 WHERE id = #{id}")
    Integer statusClose(@Param("id") Long id);

    /**
     * @param name 名称
     * @return List<WyProRightReg>
     */
    @Select("select * from wy_pro_right_reg where unit_name like concat('%', #{name},'%')")
    List<WyProRightReg> selectByName(@Param("name") String name);

    /**
     * @param p 页码
     * @param size 行数
     * @param name 名称
     * @return List<WyProRightReg>o
     */
    @Select("select * from wy_pro_right_reg where unit_name like concat('%', #{name},'%') order by cast(create_time as datetime) DESC  limit #{p},#{size}")
    List<WyProRightReg> pageByName(@Param("p")Integer p,@Param("size")Integer size,@Param("name") String name);


    /**
     * @param name 名称
     * @return  List<WyProRightReg>
     */
    @Select("select id from wy_pro_right_reg where unit_name like concat('%', #{name},'%')")
    List<WyProRightReg> selectByNameTotal(@Param("name") String name);

   int addProRight(WyProRightReg wyProRightReg);

    /**
     * @param dto 查询条件
     * @return List<WyBasicInfoHomeList>
     */
    List<WyProRightHomeList> queryWyProRightHomeList(WyProRightHomeDTO dto);

    List<WyProRightHomeList> queryWyProRightHomeListTotal(WyProRightHomeDTO dto);

    Integer updWyProRight(WyProRightReg dto);

    /**
     * @param id 名称
     * @return List<WyProRightReg>
     */
    @Select("select * from wy_pro_right_reg where id =#{id}")
    WyProRightReg selectById(@Param("id") String id);

    @Update("update wy_pro_right_reg set status =2 where id=#{id}")
    Integer delById(String id);

}
