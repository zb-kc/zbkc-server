package com.zbkc.mapper;

import com.zbkc.model.po.WyProRightReg;
import com.zbkc.model.po.WyUnitInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 物业单元信息表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-08-09
 */
@Repository
public interface WyUnitInfoMapper extends BaseMapper<WyUnitInfo> {


    /**
     * @param p 页码
     * @param size 行数
     * @param name 名称
     * @return  List<WyUnitInfo>
     */
    @Select("select * from wy_unit_info where name like concat('%', #{name},'%') limit #{p},#{size}")
    List<WyUnitInfo> pageByName(@Param("p")Integer p, @Param("size")Integer size, @Param("name") String name);


    @Select("select id from wy_unit_info where name like concat('%', #{name},'%')")
    List<WyUnitInfo> selectByNameTotal(@Param("name") String name);

}
