package com.zbkc.mapper;

import com.zbkc.model.po.SysOrg;
import com.zbkc.model.po.SysRole;
import com.zbkc.model.po.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.vo.UserVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-07-27
 */
@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 根据名字查询
     * @param userName 用户名
     * @return SysUser
     */
    List<SysUser> queryByUserName(@Param( "userName" )String userName);

    /**
     * @param id 主键id
     * @return Integer
     */
    @Update("UPDATE sys_user SET status = 1 WHERE id = #{id}")
    Integer statusOpen(@Param("id") Long id);

    /**
     * @param id 主键id
     * @return Integer
     */
    @Update("UPDATE sys_user SET status = 2 WHERE id = #{id}")
    Integer statusClose(@Param("id") Long id);

    /**
     * @param p 页码
     * @param size 行数
     * @param name 名称
     * @return List<UserVO>
     */
    List<UserVO> pageByName(@Param("p")Integer p, @Param("size")Integer size, @Param("name") String name);


    /**
     * @param name 名称
     * @return  List<SysUser>
     */
    List<SysUser> selectByNameTotal(@Param("name") String name);

    /**
     * @param userId 用户id
     * @return List<SysOrg>
     */
    @Select("select b.*  from sys_user_org a,sys_org b\n" +
            "where b.id =a.org_id \n" +
            "and a.user_id=#{userId} ")
    List<SysOrg> selectByUserId(@Param("userId") Long userId);

    /**
     * @param userId  用户id
     * @return SysRole
     */
    @Select("select a.* from sys_role a,sys_user_role b where b.role_id=a.id and b.user_id=#{userId}")
    SysRole selectSelfRole( @Param("userId") Long userId);

    /**
     * @param userId 用户id
     * @param roleId 角色id
     * @return Integer
     */
    @Update("update sys_user_role set role_id =#{roleId} where user_id=#{userId}")
    Integer updUserRole(@Param("userId") Long userId, @Param("roleId") Long roleId);

    /**
     * 查询所有
     * @param
     * @return SysUser
     */
    @Select("select * from sys_user")
    List<SysUser> getAll();

    @Insert("INSERT INTO `sys_user_role`(`id`, `user_id`, `role_id`, `status`, `create_time`, `creater`) VALUES (8, 8, 6, 1, '2021-08-11 16:30:00', 'sadmin');")
    void insertUserRole(SysRole sysRole);
}
