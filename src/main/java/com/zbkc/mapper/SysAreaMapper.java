package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.SysArea;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 地区表 Mapper 接口
 * </p>
 *
 * @author gmding
 * @since 2021-08-018
 */
@Repository
public interface SysAreaMapper extends BaseMapper<SysArea> {

    /**
     * @param
     * @return List<SysArea>
     */
    List<SysArea> getProvinceCode();

    /**
     * @param
     * @return List<SysArea>
     */
    List<SysArea> getCityCodeByProvinceCode(@Param( "provinceCode" ) Long provinceCode);

    /**
     * @param
     * @return List<SysArea>
     */
    List<SysArea> getAreaCodeByProvinceCode(@Param("cityCode")Long cityCode);


    /**
     * @param
     * @return List<SysArea>
     */
    List<SysArea> getStreetCodeCodeByProvinceCode(@Param("areaCode")Long areaCode);

    /**
     * @param
     * @return List<SysArea>
     */
    List<SysArea> getCommitteeCodeCodeByProvinceCode(@Param("streetCode")Long streetCode);

    /**
     * @param
     * @return List<SysArea>
     */
    SysArea getDetailsByCode(@Param("code")Long code);

    /**
     * 根据街道code 获取名称
     * @param committeeCode 街道code
     * @return map
     */
    Map<String , String> selectNameByCommitteeCode(@Param("committeeCode")String committeeCode);

}
