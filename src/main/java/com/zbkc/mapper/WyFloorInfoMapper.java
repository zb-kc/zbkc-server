package com.zbkc.mapper;

import com.zbkc.model.po.WyFloorInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.WyProRightReg;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 物业楼层信息表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-08-10
 */
@Repository
public interface WyFloorInfoMapper extends BaseMapper<WyFloorInfo> {

    /**
     * @param p 页码
     * @param size 行数
     * @param name 名称
     * @return List<WyProRightReg>
     */
    @Select("select * from wy_floor_info where name like concat('%', #{name},'%') limit #{p},#{size}")
    List<WyFloorInfo> pageByName(@Param("p")Integer p, @Param("size")Integer size, @Param("name") String name);


    /**
     * @param name 名称
     * @return  List<WyProRightReg>
     */
    @Select("select id from wy_floor_info where name like concat('%', #{name},'%')")
    List<WyFloorInfo> selectByNameTotal(@Param("name") String name);
}
