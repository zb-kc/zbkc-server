package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.WyFacilitiesInfo;
import com.zbkc.model.vo.UnitNameVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import javax.annotation.security.PermitAll;
import java.util.List;

/**
 *
 * @author gmding
 * @since 2021-08-18
 */
@Repository
public interface WyFacilitiesInfoMapper extends BaseMapper<WyFacilitiesInfo> {
    int addWyFacilitiesInfo(WyFacilitiesInfo wyFacilitiesInfo);

    List<WyFacilitiesInfo> queryBywyBasicInfoId(@Param( "wyBasicInfoId" )Long wyBasicInfoId);

    List<WyFacilitiesInfo> queryBywyBasicInfoIdByFloor(Long wyBasicInfoId,String floorName);

    int updWyFacilitiesInfo(WyFacilitiesInfo wyFacilitiesInfo);

    int deleteByWyBasicInfoId(Long wyBasicInfoId);



}
