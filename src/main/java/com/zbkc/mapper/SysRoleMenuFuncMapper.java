package com.zbkc.mapper;

import com.zbkc.model.dto.MenuDTO;
import com.zbkc.model.po.SysRoleMenuFunc;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.lettuce.core.dynamic.annotation.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 系统角色菜单功能表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-08-02
 */
@Repository
public interface SysRoleMenuFuncMapper extends BaseMapper<SysRoleMenuFunc> {

    /**
     * @param roleId
     * @return List<MenuDTO>
     */
    @Select("SELECT a.* FROM sys_menu a LEFT JOIN sys_role_menu_func b ON a.id=b.menu_id WHERE b.role_id=#{roleId} AND a.pid=#{pid}")
    List<MenuDTO> findMenuDTOByRoleid(@Param("roleId") Long roleId,@Param("pid") Long pid);

    @Select("select menu_id from sys_role_menu_func where role_id =#{roleId}")
    List<String> findSelfMenuIds(@Param("roleId") Long roleId);


    Integer deleteByRoleId(Long roleId);

    /**
     * 批量插入数据
     * @param list list
     * @return 影响条数
     */
    Integer insertBatch(@Param("list") List<SysRoleMenuFunc> list);
}
