package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.dto.*;
import com.zbkc.model.po.YyContract;
import com.zbkc.model.po.YyContractList;
import com.zbkc.model.vo.*;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 运营管理——合同管理 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-09-13
 */
@Repository
public interface YyContractMapper extends BaseMapper<YyContract> {

   Integer addContract(YyContract yyContract);

   Integer updContract(YyContract yyContract);

   List<YyContractList> queryContractList(YyContractIndexDTO dto);

   List<YyContractList> queryContractListTotal(YyContractIndexDTO dto);

   @Select("select * from  yy_contract where id=#{id}")
   YyContract seeRow(@Param("id") String id);

   List<YyDepositHome> queryDepositList(DepositDTO dto);

   List<YyDepositHome> queryDepositListTotal(DepositDTO dto);

   DepositDetail queryDepositDetail(String id,Long wyId);

   Integer editDeposit(EditDepositDTO dto);

   List<YjContractVo> selectAllYjContract(YjContractDTO dto);

   List<YjContractVo> selectAllYjContractTotal(YjContractDTO dto);

   List<YjContractReport> selectPreContract(YjContractDTO dto);

   List<YjContractReport> selectPreContractTotal(YjContractDTO dto);

   List<IndReviewReport>  selectIndustReview(YjContractDTO dto);

   List<IndReviewReport>  selectIndustReviewTotal(YjContractDTO dto);

   List<LeaseDetailVo> selectleaseDetail(LeaseDetailDTO dto);

   List<LeaseDetailVo> selectleaseDetailTotal(LeaseDetailDTO dto);

   List<EmptyDetailVo> selectEmptyDetail(EmptyDetailDTO dto);

   List<EmptyDetailVo> selectEmptyDetailTotal(EmptyDetailDTO dto);

   List<YjContractReport> preContractTwoMonth();

   List<IndReviewReport>  industReviewThreeMonth();

   @Select("select * from  yy_contract where id=#{id} and template_type=5")
   YyContract selectById(@Param("id") Long id);

   YyContract selectOneByBusinessId(@Param("businessId") Long businessId);

   /**
    * 业务办理中 合同查询
    * @param bussinessId 业务id
    * @return 合同信息
    */
   YwContractVO selectYwContractIdByBusinessId(String bussinessId);

   /**
    * 根据社会统一信用代码查询 分页查询
    * @param ywAgencyBusinessDTO 社会统一信用代码,p,size
    * @return list
    */
    List<YyContractVO> selectYyContractListByPage(YwAgencyBusinessDTO ywAgencyBusinessDTO);

   /**
    * 分页查询 总数
    * @param ywAgencyBusinessDTO 社会统一信用代码,p,size
    * @return int
    */
    int selectYyContractListByPageCount(YwAgencyBusinessDTO ywAgencyBusinessDTO);

   YyContractDetail getWord(String id);

}
