package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.YySplitRent;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 运营管理——合同管理_拆分租金单价表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-09-13
 */
@Repository
public interface YySplitRentMapper extends BaseMapper<YySplitRent> {


    List<YySplitRent> selectAllSplitRent(String yyContractId);

    Integer updById(String id);

    Integer updRentById(YySplitRent yySplitRent);

    Integer addSplitRent(YySplitRent yySplitRent);

    Integer deleteByYyContractId(@Param("yyContractId") Long id);

    Integer insertBatch(List<YySplitRent> insertSplitRentList);

    /**
     * 批量更新
     * @param updateSplitRentList list
     * @return 影响行数
     */
    int updateBatch(@Param("list") List<YySplitRent> updateSplitRentList);
}
