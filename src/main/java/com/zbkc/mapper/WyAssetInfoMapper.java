package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.dto.CywyDTO;
import com.zbkc.model.dto.WyListDTO;
import com.zbkc.model.po.WyAssetStreet;
import com.zbkc.model.po.YdqList;
import com.zbkc.model.po.ZfWyAssetInfo;
import com.zbkc.model.vo.*;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 *
 * @author gmding
 * @since 2021-09-11
 */
@Repository
public interface WyAssetInfoMapper extends BaseMapper<ZfWyAssetInfo> {

    /**
     * 房屋分类总数
     * */
    @Select("select count(id) from zf_wy_asset_info_v2 where find_in_set( belong_type,#{belong_type}) and wy_five=#{wy_five}")
    Integer wyAssetInfoTypeNum(@Param( "belong_type" )String belongType,@Param( "wy_five" )String wyFive);

    @Select("select count(id) from zf_wy_asset_info_v2 where find_in_set( belong_type,#{belong_type})")
    Integer wyAssetInfoTypeNum2(@Param( "belong_type" )String belongType);

    /**
     * 租赁社会物业
     * */
    @Select("select count(id) from zf_wy_asset_info_v2 where find_in_set(belong_type,#{belong_type}) ")
    Integer wyAssetInfoZlNum(@Param( "belong_type" )String belongType);


    /**
     * 用房面积
     * */
    @Select("select SUM(build_area) from zf_wy_asset_info_v2 where find_in_set(belong_type,#{belong_type}) and wy_five=#{wy_five}")
    Double wyAssetInfoAreaNum(@Param( "belong_type" )String belongType,@Param( "wy_five" )String wyFive);

    /**
     * 实用面积
     * */
    @Select("select SUM(act_area) from zf_wy_asset_info_v2 where find_in_set(belong_type,#{belong_type})  ")
    Double wyAssetInfoActArea(@Param( "belong_type" )String belongType);

    /**
     * 出租总金额
     * */
    @Select(" SELECT sum(czyzj) as area FROM zf_wy_asset_info_v2  WHERE zfwysfcz='是'")
    Double wyAssetInfoCZMoney();

    /**
     * 出租总面积
     * */
    @Select(" SELECT sum(build_area) as area FROM zf_wy_asset_info_v2  WHERE zfwysfcz='是' and czyzj !=''")
    Double wyAssetInfoCZArea();



    /**
     * 实用面积
     * */
    @Select("select SUM(act_area) from zf_wy_asset_info_v2 where find_in_set(belong_type,#{belong_type})  and wy_five=#{wy_five}")
    Double wyAssetInfoActArea1(@Param( "belong_type" )String belongType,@Param( "wy_five" )String wyFive);

    /**
     * 建筑面积
     * */
    @Select("select SUM(build_area) from zf_wy_asset_info_v2 where find_in_set(belong_type,#{belong_type})")
    Double wyAssetInfoBuildArea(@Param( "belong_type" )String belongType);

    /**
     * 国企建筑面积
     * */
    @Select("SELECT sum(build_area) as area FROM wy_asset_info")
    Double wyAssetInfoGQBuildArea();

    /**
     * 国企出租面积
     * */
    @Select("SELECT sum(build_area) as area FROM wy_use_rel WHERE is_stop=0")
    Double wyAssetInfoCZGQBuildArea();




    /**
     * 建筑面积
     * */
    @Select("select SUM(build_area) from zf_wy_asset_info_v2 where belong_type=#{belong_type} and wy_five=#{wy_five}")
    Double wyAssetInfoBuildArea1(@Param( "belong_type" )String belongType ,@Param( "wy_five" )String wyFive);
    /**
     * 产权
     * */
    @Select("select count(cert_has) from zf_wy_asset_info_v2 where belong_type=#{belong_type} and cert_has='是'")
    int wyAssetInfoCertHas(@Param( "belong_type" )String belongType );

    /**
     * 产权
     * */
    @Select("select count(cert_has) from zf_wy_asset_info_v2 where belong_type=#{belong_type} and cert_has='是' and wy_five=#{wy_five}")
    int wyAssetInfoCertHas1(@Param( "belong_type" )String belongType ,@Param( "wy_five" )String wyFive);

    /**
     * 房屋编码
     * */
    @Select("select count(id) from zf_wy_asset_info_v2 where house_number like '%无%' ")
    int wyAssetInfoNoCode();
    /**
     * 不符合条件房屋编码
     * */
    @Select("select count(id) from zf_wy_asset_info_v2 where  house_number='' or house_number=0 ")
    int wyAssetInfoIneligibleCode();

    /**
     * 总房屋编码
     * */
    @Select("select count(id) from zf_wy_asset_info_v2")
    int wyAssetInfoIneHouseCode();

    /**
     * 公摊面积
     * */
    @Select("select SUM(pub_area) from zf_wy_asset_info_v2 where find_in_set(belong_type,#{belong_type})")
    Double wyAssetInfoPubArea(@Param( "belong_type" )String belongType);
    /**
     * 公摊面积
     * */
    @Select("select SUM(pub_area) from zf_wy_asset_info_v2 where find_in_set(belong_type,#{belong_type}) and wy_five=#{wy_five}")
    Double wyAssetInfoPubArea1(@Param( "belong_type" )String belongType,@Param( "wy_five" )String wyFive);



    /**
     * 占地面积
     * */
    @Select("SELECT SUM(v.land_area)  FROM zf_wy_asset_info_v2 v2  LEFT JOIN zf_wy_asset_info v on v.house_number=v2.house_number where v2.belong_type=#{belong_type}")
    Double wyAssetInfoZdArea(@Param( "belong_type" )String belongType);
    /**
     * 占地面积
     * */
    @Select("SELECT SUM(v.land_area)  FROM zf_wy_asset_info_v2 v2  LEFT JOIN zf_wy_asset_info v on v.house_number=v2.house_number where v2.belong_type=#{belong_type} and v2.wy_five=#{wy_five}")
    Double wyAssetInfoZdArea1(@Param( "belong_type" )String belongType,@Param( "wy_five" )String wyFive);


    /**
     * 国企粗租企业数量
     * */
    @Select("SELECT COUNT(ID) FROM wy_use_rel WHERE is_stop=0 and approve_use_start_date>#{start_date} and approve_use_start_date<#{end_date}")
    int wyAssetInfoGQCZNum(@Param( "start_date" )String start_date,@Param( "end_date" )String end_date);


    /**
     * 总空置面积
     * */
    @Select("select SUM(build_area) from zf_wy_asset_info_v2 where emptystate='是' and find_in_set(belong_type,#{belong_type})")
    Double wyAssetInfoKzArea(@Param( "belong_type" )String belongType);

    /**
     * 五大类空置面积
     * */
    @Select("select SUM(build_area) from zf_wy_asset_info_v2 where emptystate='是' and belong_type=#{belong_type} and wy_five=#{wy_five}")
    Double wyAssetInfoKzArea1(@Param( "belong_type" )String belongType,@Param( "wy_five" )String wyFive);

    /**
     * 五大类空置数量
     * */
    @Select("select count(id) from zf_wy_asset_info_v2 where emptystate='是' and find_in_set(belong_type,#{belong_type}) and wy_five=#{wy_five}")
    int wyAssetInfoKzNum(@Param( "belong_type" )String belongType,@Param( "wy_five" )String wyFive);

    @Select("select count(*) from zf_cx_empty_wy_info2 where find_in_set(propertytag,#{belong_type})")
    int selectEmptyNumberInKJ(@Param( "belong_type" )String belongType);
    /**
     * 出租面积
     * */
    @Select("select SUM(build_area) from zf_wy_asset_info_v2 where zfwysfcz='是' and find_in_set(belong_type,#{belong_type})")
    Double wyAssetInfoCzArea(@Param( "belong_type" )String belongType);






    //@Select("SELECT COUNT(fc.`name`) FROM fc_money fc,qy_ywk_sszt_hc qy WHERE fc.name=qy.QYMC GROUP BY fc.name")
    //与企服数据保持一致
    @Select("select a from zf_cx_basic_info group by b")
    List<Integer> wyAssetInfoSupportNum();


    /**
     * 出租资金
     * */
    @Select("select SUM(czyzj) from zf_wy_asset_info_v2 where zfwysfcz='是' and belong_type=#{belong_type} and czyzj !=0 and wy_five=#{wy_five}")
    Double wyAssetInfoCzyzj1(@Param( "belong_type" )String belongType,@Param( "wy_five" )String wyFive);

    /**
     * 入账金额
     * */
    @Select("select SUM(ruzhang_amount) from zf_wy_asset_info_v2 where ruzhang='是'  and belong_type=#{belong_type} and wy_five=#{wy_five}")
    Double wyAssetInfoRz(@Param( "belong_type" )String belongType,@Param( "wy_five" )String wyFive);


    /**
     * 出租面积 出租资金不为空
     * */
    @Select("select SUM(build_area) from zf_wy_asset_info_v2 where zfwysfcz='是' and belong_type=#{belong_type} and czyzj !=0 and wy_five=#{wy_five}")
    Double wyAssetInfoCzyArea1(@Param( "belong_type" )String belongType,@Param( "wy_five" )String wyFive);


    /**
     * 出租面积
     * */
    @Select("select SUM(build_area) from zf_wy_asset_info_v2 where zfwysfcz='是' and belong_type=#{belong_type} and wy_five=#{wy_five}")
    Double wyAssetInfoCzArea1(@Param( "belong_type" )String belongType,@Param( "wy_five" )String wyFive);

    /**
     * 预到期企业
     * */

    //@Select("SELECT zf.id,qy.企业名称 as name,zf.credit_id as code,timestampdiff(day,now(),zf.rent_date_end) as syNum,zfwy.area as buildArea,zfwy.unit_price as price FROM zf_wy_contract zf  " +
    //        "    LEFT JOIN zf_wy_contract_bind zfwy on zfwy.contract_id =zf.id  " +
    //        "    LEFT JOIN qy_basic_info qy on qy.信用代码=zf.credit_id  " +
    //        "    where  timestampdiff(day,now(),zf.rent_date_end)>0  and qy.企业名称!='' " +
    //        "    and qy.企业名称 like concat('%', #{index},'%') group by zf.id ORDER BY timestampdiff(day,now(),zf.rent_date_end) ASC LIMIT #{p},#{size} ")
    //@Select("SELECT\n" +
    //        "\tzf.id,\n" +
    //        "\tqy.企业名称 AS NAME,\n" +
    //        "\tzf.credit_id AS CODE,\n" +
    //        "\ttimestampdiff( DAY, now(), zf.rent_date_end ) AS syNum,\n" +
    //        "\tzfwy.area AS buildArea,\n" +
    //        "\tzfwy.unit_price AS price, DATE_FORMAT(zf.rent_date_start,\"%Y年%m月%d日\") as rentDateStart ,DATE_FORMAT(zf.rent_date_end,\"%Y年%m月%d日\") as rentDateEnd \n" +
    //        "FROM\n" +
    //        "\tzf_wy_contract zf\n" +
    //        "\tLEFT JOIN zf_wy_contract_bind zfwy ON zfwy.contract_id = zf.id\n" +
    //        "\tLEFT JOIN qy_basic_info qy ON qy.信用代码 = zf.credit_id \n" +
    //        "WHERE\n" +
    //        "\ttimestampdiff( DAY, now(), zf.rent_date_end )>= 0 \n" +
    //        "\tAND timestampdiff( DAY, now(), zf.rent_date_end )<= 366 \n" +
    //        "\tAND qy.企业名称 != '' \n" +
    //        "\tAND qy.企业名称 LIKE concat( '%', \"\", '%' ) \n" +
    //        "GROUP BY\n" +
    //        "\tzf.id \n" +
    //        "ORDER BY\n" +
    //        "\ttimestampdiff( DAY, now(), zf.rent_date_end ) ASC\n" +
    //        "\tLIMIT #{p},#{size}")
    List<YdqList> wyAssetInfoYdqArea(@Param( "index" )String index,@Param( "p" )int p,@Param( "size" )int size);

    //@Select("SELECT zf.id,qy.企业名称 as name,zf.credit_id as code,timestampdiff(day,now(),zf.rent_date_end) as syNum,zfwy.area as buildArea,zfwy.unit_price as price FROM zf_wy_contract zf  " +
    //        "    LEFT JOIN zf_wy_contract_bind zfwy on zfwy.contract_id =zf.id " +
    //        "    LEFT JOIN qy_basic_info qy on qy.信用代码=zf.credit_id  " +
    //        "    where  timestampdiff(day,now(),zf.rent_date_end)>0  and qy.企业名称!='' " +
    //        "    and qy.企业名称 like concat('%', #{index},'%') group by zf.id ")
    //@Select("SELECT\n" +
    //        "\tzf.id,\n" +
    //        "\tqy.企业名称 AS NAME,\n" +
    //        "\tzf.credit_id AS CODE,\n" +
    //        "\ttimestampdiff( DAY, now(), zf.rent_date_end ) AS syNum,\n" +
    //        "\tzfwy.area AS buildArea,\n" +
    //        "\tzfwy.unit_price AS price \n" +
    //        "FROM\n" +
    //        "\tzf_wy_contract zf\n" +
    //        "\tLEFT JOIN zf_wy_contract_bind zfwy ON zfwy.contract_id = zf.id\n" +
    //        "\tLEFT JOIN qy_basic_info qy ON qy.信用代码 = zf.credit_id \n" +
    //        "WHERE\n" +
    //        "\ttimestampdiff( DAY, now(), zf.rent_date_end )>= 0 \n" +
    //        "\tAND timestampdiff( DAY, now(), zf.rent_date_end )<= 366 \n" +
    //        "\tAND qy.企业名称 != '' \n" +
    //        "\tAND qy.企业名称 LIKE concat( '%', \"\", '%' ) \n" +
    //        "GROUP BY\n" +
    //        "\tzf.id ")
    List<YdqList> wyAssetInfoYdqAreaTotal(@Param( "index" )String index);





    /**
     * 物业列表
     * */
    //@Select("SELECT id,name,build_area as buildArea,address,man_unit as manUnit  FROM zf_wy_asset_info_v2 where  name like concat('%', #{index},'%') and belong_type=#{belong_type} and wy_five like concat('%', #{wy_five},'%') LIMIT #{p},#{size}")
    //@Select("SELECT\n" +
    //        "\ta.id,\n" +
    //        "\ta.NAME,\n" +
    //        "\ta.build_area AS buildArea,\n" +
    //        "\ta.address,\n" +
    //        "\ta.man_unit AS manUnit ,\n" +
    //        "\tb.coordinate as gps\n" +
    //        "FROM\n" +
    //        "\tzf_wy_asset_info_v2 a\n" +
    //        "\tleft join zf_wy_unit_info b on b.name = a.name\n" +
    //        "WHERE\n" +
    //        "\ta.NAME LIKE concat('%',#{index},'%') \n" +
    //        "\tand a.belong_type=#{belong_type} \n" +
    //        "\tand a.wy_five like concat('%', #{wy_five},'%')\n" +
    //        "\tand a.man_unit like concat('%', #{man_unit},'%')\n" +
    //        "\tand a.wy_kind like concat('%', #{wy_kind},'%')\n" +
    //        "\tand a.street_name like concat('%', #{street_name},'%')\n" +
    //        "\tLIMIT #{p},#{size}")
    List<YdqList> wyAssetInfoCywyList1(@Param( "belong_type" )String belongType,
                                       @Param( "wy_five" )String wyFive,
                                       @Param( "index" )String index,
                                       @Param( "p" )int p,
                                       @Param( "size" )int size,
                                       @Param( "man_unit" )String man_unit,
                                       @Param( "wy_kind" )String wy_kind,
                                       @Param( "street_name" )String street_name);

    //@Select("SELECT\n" +
    //        "\tid,\n" +
    //        "NAME \n" +
    //        "FROM\n" +
    //        "\tzf_wy_asset_info_v2 a\n" +
    //        "WHERE\n" +
    //        "\tNAME LIKE concat('%',#{index},'%') \n" +
    //        "\tand belong_type=#{belong_type} \n" +
    //        "\tand a.man_unit like concat('%', #{man_unit},'%')\n" +
    //        "\tand a.wy_kind like concat('%', #{wy_kind},'%')\n" +
    //        "\tand a.street_name like concat('%', #{street_name},'%')\n" +
    //        "\tand wy_five like concat('%', #{wy_five},'%')")
    List<YdqList> wyAssetInfoCywyListTotal1(@Param( "belong_type" )String belongType,@Param( "wy_five" )String wyFive,@Param( "index" )String index,
                                            @Param( "man_unit" )String man_unit,
                                            @Param( "wy_kind" )String wy_kind,
                                            @Param( "street_name" )String street_name);

    @Select( "select COUNT(v2.id) as num,sum(v2.build_area) as area,v2.street_name as street " +
            "    from zf_wy_asset_info_v2 v2 where find_in_set( v2.belong_type, #{belong_type} )   GROUP BY v2.street_name" )
    List<WyAssetStreet> getStreetList(@Param("belong_type" )String belongType );

    @Select( "select COUNT(v2.id) as num,sum(v2.build_area) as area,v2.street_name as street " +
            "    from zf_wy_asset_info_v2 v2 where v2.belong_type=#{belong_type}   and v2.wy_five=#{wyFive} GROUP By v2.street_name" )
    List<WyAssetStreet> getStreetList1(@Param("belong_type" )String belongType ,@Param( "wyFive" )String wyFive);

    @Select( "select v2.build_area as area,v2.name as street,d.addr as address  " +
            "    from zf_wy_asset_info_v2 v2 LEFT JOIN zf_cy_address d on d.name = v2.name where v2.belong_type=#{belong_type} and v2.wy_five=#{wyFive} " )
    List<WyAssetStreet> cyStreet(@Param("belong_type" )String belongType ,@Param( "wyFive" )String wyFive);


    @Select( "select sum(v2.build_area) " +
            "    from zf_wy_asset_info_v2 v2 LEFT JOIN zf_cy_address d on d.name = v2.name where v2.belong_type=#{belong_type} and v2.wy_five=#{wyFive} " )
    Double cyStreetTatol(@Param("belong_type" )String belongType ,@Param( "wyFive" )String wyFive);

    /**
     * 企业标签统计
     * */
   //#@Select( "SELECT COUNT(企业标签) FROM qy_basic_info WHERE 企业标签 like concat('%', #{qy_type},'%')")
   // @Select("SELECT\n" +
   //         "\tcount(*) as number\n" +
   //         "FROM\n" +
   //         "\t(\n" +
   //         "\tSELECT\n" +
   //         "\t\twy.use_unit \n" +
   //         "\tFROM\n" +
   //         "\t\tqy_basic_info qy,\n" +
   //         "\t\tzf_wy_contract wy \n" +
   //         "\tWHERE\n" +
   //         "\t\twy.use_unit = qy.企业名称 \n" +
   //         "\t\tAND qy.企业标签 LIKE concat( '%', #{qy_type}, '%' ) and qy.企业标签 not like concat('%非', #{qy_type},'%') \n" +
   //         "\tGROUP BY\n" +
   //         "\t\tqy.企业名称 \n" +
   //         "\t) a")
    int zdqyBasicInfo(@Param("qy_type" )String qyType);

    /**
     * 企业标签_企业列表
     * */
    @Select( "SELECT COUNT(wy.id) as num,qy.企业名称 as name ,qy.信用代码 as code FROM qy_basic_info qy,new_company_use_wy_info wy  " +
            "where wy.companyname=qy.企业名称 and qy.企业标签 like concat('%', #{type},'%') " +
            "and qy.企业标签 not like concat('%非', #{type},'%') " +
            "and qy.企业名称 like concat('%', #{index},'%') " +
            "and qy.isuse = \"在用\" " +
            "GROUP BY qy.企业名称,qy.信用代码  LIMIT #{p},#{size} ")
    List<ZdqyListVo> zdqyBasicInfoList(@Param("type" )String qyType,@Param("index" )String index,@Param( "p" )int p,@Param( "size" )int size );

    @Select( "SELECT COUNT(wy.use_unit) as num,qy.企业名称 as name ,qy.信用代码 as code FROM qy_basic_info qy,zf_wy_contract wy  " +
            "where wy.use_unit=qy.企业名称 and qy.`挂点领导` is not null  " +
            "GROUP BY qy.企业名称,qy.信用代码  LIMIT #{p},#{size} ")
    List<ZdqyListVo> zdqyBasicInfoList2(@Param("type" )String qyType,@Param("index" )String index,@Param( "p" )int p,@Param( "size" )int size );

    /**
     * 企业标签_企业列表
     * */
    @Select( "SELECT COUNT(wy.id) as num,qy.企业名称 as name ,qy.信用代码 as code FROM qy_basic_info qy,new_company_use_wy_info wy  " +
            "where wy.companyname=qy.企业名称 " +
            "and qy.企业标签 like concat('%', #{type},'%') " +
            "and qy.企业标签 not like concat('%非', #{type},'%') " +
            "and qy.企业名称 like concat('%', #{index},'%') " +
            "and qy.isuse = \"在用\" " +
            "GROUP BY qy.企业名称,qy.信用代码  ")
    List<ZdqyListVo> zdqyBasicInfoListTotal(@Param("type" )String qyType ,@Param("index" )String index);

    @Select( "SELECT COUNT(wy.use_unit) as num,qy.企业名称 as name ,qy.信用代码 as code FROM qy_basic_info qy,zf_wy_contract wy  " +
            "where wy.use_unit=qy.企业名称 and qy.`挂点领导` is not null  " +
            "GROUP BY qy.企业名称,qy.信用代码  ")
    List<ZdqyListVo> zdqyBasicInfoListTotal2(@Param("type" )String qyType ,@Param("index" )String index);

    @Select( "SELECT qy.地址 as address,qy.法定代表人姓名 as name,qy.注册资本 as zcMoney,qy.申报实收资本 as sbMoney,qy.成立日期 as time,qy.企业标签 as label,qy.挂点领导 as gdname FROM qy_basic_info qy where qy.信用代码=#{code}")
    List<QyDetailsVo> qyBasicInfo(@Param( "code" )String code);

    @Select( "SELECT COUNT(ID) FROM rs_ent_insured_person WHERE UNIT_CREDIT_NO=#{code} ")
    int qyCbNum(@Param( "code" )String code);

    /**
     * 参保列表
     * */
    @Select( "SELECT name,'身份证' as cardType,ID_NO as cardNo,UPDATETIME as updateTime FROM rs_ent_insured_person  WHERE UNIT_CREDIT_NO=#{code} ")
    List<CBpersonVo> cbList(@Param( "code" )String code);


    @Select( "SELECT zf.员工规模 FROM zf_rczl_info  zf WHERE zf.企业信用代码 =#{code} GROUP BY 员工规模")
    String scale(@Param( "code" )String code);
    /**
     * 扶持资金
     * */
    @Select( "SELECT year,money,project_name as projectName,project_type as projectType,yw_type as ywType,zg_dep as zgDep,zg_pro as zgPro FROM fc_money WHERE CODE=#{code} order by year desc")
    List<FCMoney>fcList(@Param( "code" )String code);

    @Select( "SELECT sum(money) FROM fc_money WHERE CODE=#{code}")
    Double fcListNum(@Param( "code" )String code);

    @Select( "SELECT year,money,project_name as projectName,project_type as projectType,yw_type as ywType,zg_dep as zgDep,zg_pro as zgPro FROM fc_money WHERE name=#{name} order by year desc")
    List<FCMoney> fcListByCompanyName(@Param( "name" )String name);

    @Select( "SELECT sum(money) FROM fc_money WHERE name=#{name}")
    Double fcListNumByCompanyName(@Param( "name" )String name);
    /**
     * 企业领航人才列表
     * */
    @Select( "SELECT qy.SQRXM as name,qy.rdjb as rcType,qy.ZXCOL_ID as  id,qy.ZSJZRQ as endTime,qy.ZSQSRQ as startTime FROM qy_lhrcinfo qy,qy_basic_info ba " +
            "WHERE qy.DWMC=ba.企业名称 and ba.信用代码=#{code}")
    List<QylhrcinfoVo> qyrcList(@Param( "code" )String code);

    //参保
    @Select( "SELECT number FROM company_cb_number WHERE companyname=#{companyname}")
    Integer selectCompanyCbNumber(@Param("companyname") String companyname);

    /**
     * 租凭信息列表
     * */
    @Select( "SELECT v.name as wyName,zb.area as area,zb.rent as mPrice ,zb.unit_price as price,zf.rent_date_start as startTime,zf.rent_date_end as endTime," +
            "timestampdiff(day,now(),zf.rent_date_end) as day " +
            "FROM zf_wy_contract zf,zf_wy_contract_bind zb,zf_wy_asset_info v  " +
            "WHERE  zf.id=zb.contract_id and v.id=zb.rel_id and zf.credit_id=#{code}  " )
    List<ZPDetailsVo> zpList(@Param( "code" )String code);

    @Select( "SELECT zf.企业介绍 FROM zf_rczl_info  zf WHERE zf.企业信用代码 =#{code} ")
    List<String> introduction(@Param( "code" )String code);
    @Select( "SELECT zf.企业名称 FROM qy_basic_info  zf WHERE zf.信用代码 =#{code} ")
    List<String> comName(@Param( "code" )String code);


    /**
     * 单位物业资产前五统计
     *
     */
    @Select( "select count(id) as num,man_unit as unitName from zf_wy_asset_info_v2 where  man_unit!='' GROUP BY man_unit ORDER BY count(id) DESC LIMIT 0,5")
    List<ZcVo>zcList();

    /**
     * 单位物业资产统计
     *
     */
    @Select( "select count(id) from zf_wy_asset_info_v2 where  man_unit!='' ")
    int zcTotal();

    /**
     * 租期限5~10年
     */
    @Select( "SELECT count(id) FROM zf_wy_contract WHERE timestampdiff(YEAR,rent_date_start,rent_date_end)>=5 and timestampdiff(YEAR,rent_date_start,rent_date_end)<10 and update_state = \"normal\" and practical_use in('CY_BG','SY_CF')")
    int zq5Year();

    @Select( "SELECT count(id) FROM zf_wy_contract WHERE timestampdiff(YEAR,rent_date_start,rent_date_end)>=10 and timestampdiff(YEAR,rent_date_start,rent_date_end)<15 and update_state = \"normal\" and practical_use in('CY_BG','SY_CF') ")
    int zq10Year();
    @Select( "SELECT count(id) FROM zf_wy_contract WHERE timestampdiff(YEAR,rent_date_start,rent_date_end)>=15 and update_state = \"normal\" and practical_use in('CY_BG','SY_CF')")
    int zq15Year();

    @Select( "SELECT COUNT(DISTINCT use_unit) FROM zf_wy_contract WHERE timestampdiff(MONTH,now(),rent_date_end)>1 and timestampdiff(MONTH,now(),rent_date_end) <=3 and update_state = \"normal\" and practical_use in('CY_BG','SY_CF')")
    int zq1Month();

    @Select( "SELECT COUNT(DISTINCT use_unit) FROM zf_wy_contract WHERE timestampdiff(MONTH,now(),rent_date_end)>3 and timestampdiff(MONTH,now(),rent_date_end)<=6 and update_state = \"normal\" and practical_use in('CY_BG','SY_CF')")
    int zq3Month();
    @Select( "SELECT COUNT(DISTINCT use_unit) FROM zf_wy_contract WHERE timestampdiff(MONTH,now(),rent_date_end) >6 and  timestampdiff(MONTH,now(),rent_date_end)<=12 and update_state = \"normal\" and practical_use in('CY_BG','SY_CF')")
    int zq6Month();

    //@Select( "select COUNT(id) from zf_wy_asset_info_v2 where  belong_type='政府物业' and ruzhang='是' ")
    //@Select( "select count(a.id) as number from wy_entry a  join wy_entry_bind b on b.entry_id = a.id join zf_wy_relation_tree c on c.rel_id = b.rel_id\n ")
    @Select("select count(*) from wy_entry_info")
    int rzToatl();

    //@Select( "select count(id) as num,sum(build_area) as area,man_unit as unitName from zf_wy_asset_info_v2 where  belong_type='政府物业' and  wy_five =#{index}  GROUP BY man_unit  ")
    List<CyWyVo> cyWy(@Param( "index" )String index , @Param("p") Integer p , @Param("size") Integer size , @Param("sort") Integer sort);


    @Select( "select SUM(build_area) as area,COUNT(ID) as num,SUM(czyzj) as price from zf_wy_asset_info_v2 where  belong_type='租赁社会物业'  ")
    ZlBasicInfoVo basicInfo();

    /**
     * 租凭物业使用单位统计情况列表
     * */
    @Select( "select count(id) as num,sum(build_area) as area,man_unit as unitName from zf_wy_asset_info_v2 where  belong_type='租赁社会物业'  GROUP BY man_unit  ")
    List<CyWyVo>zlList();



    /**
     * 租凭物业种类占比前五统计
     *
     */
    @Select( "select count(id) as num,act_usage as unitName from zf_wy_asset_info_v2 where   belong_type='租赁社会物业' GROUP BY act_usage ORDER BY COUNT(ID) DESC LIMIT 0,5 ")
    List<ZcVo>zlwyList();

    @Select( "select count(id) as num,act_usage as unitName from zf_wy_asset_info_v2 where   belong_type='租赁社会物业' GROUP BY act_usage ORDER BY COUNT(ID) DESC ")
    List<ZcVo>zlwyListTotal();

    /**
     * 各街道租凭物业面积
     */

    @Select( "SELECT street_name as streetName,sum(build_area) as area   FROM zf_wy_asset_info_v2 where  belong_type='租赁社会物业' GROUP BY street_name ")
    List<ZlPriceVo>zlPriceList();

    /**
     * 各单位租凭物业面积
     */

    @Select( "SELECT man_unit as streetName,sum(build_area) as area   FROM zf_wy_asset_info_v2 where  belong_type='租赁社会物业' and wy_kind like concat('%', #{index},'%') GROUP BY man_unit  ")
    List<ZlPriceVo>zlUnitList(@Param( "index" )String index);


    /**
     * 各单位租凭物业面积
     */

    @Select( "SELECT v2.name as name,i.land_area as landArea,i.avg_price as avgPrice,v2.address," +
            "v2.street_name as streetName,v2.community_name as communityName,v2.man_unit as manUnit,v2.wygsmc,v2.wygstel,"+
            "v2.zlshwysfqdht,v2.zlshwyqsrq,v2.zlshwydqrq,"+
            "i.building_count as buildingCount,IFNULL( i.build_area, 0 ) as buildArea," +
            "SUM( IFNULL( CASE WHEN a1.STATUS = '3' AND a1.has_nodes = '0' THEN a1.build_area ELSE 0 END, 0 ) ) vacantArea," +
            "COUNT( CASE WHEN a1.STATUS = '3' AND a1.has_nodes = '0' THEN 1 ELSE NULL END ) vacantCount," +
            "SUM( IFNULL( CASE WHEN a1.has_nodes = '0' and (b.use_type = '2' or a1.STATUS = '3') THEN a1.build_area ELSE 0 END, 0 ) ) rentableArea," +
            "SUM( CASE WHEN b.id IS NOT NULL THEN IFNULL( a1.build_area, 0 ) ELSE 0 END ) rentedArea," +
            "COUNT( DISTINCT b.use_unit ) qyCount," +
            "SUM(CASE WHEN TIMESTAMPDIFF( MONTH, c.rent_date_end, NOW( ) ) < 3 AND TIMESTAMPDIFF( MONTH, c.rent_date_end, NOW( ) ) > 0 THEN 1 ELSE 0 END ) beExpire," +
            "v2.cert_has as certHas,v2.ruzhang_amount as rzMoney " +
            " FROM " +
            " zf_wy_asset_info_v2 v2 " +
            " LEFT JOIN zf_wy_asset_info i on i.NAME=v2.NAME " +
            "   " +
            "  left JOIN ( " +
            "  SELECT " +
            "   a.id, " +
            "   a.build_area, " +
            "   a.id assetsId, " +
            "   a.STATUS, " +
            "   t.has_nodes  " +
            "  FROM " +
            "   zf_wy_asset_info a " +
            "   JOIN zf_wy_relation_tree t ON t.rel_id = a.id  " +
            "   AND t.del_flag = '0'  " +
            "  WHERE " +
            "   CONCAT( ',', a.type, ',' ) LIKE CONCAT( '%,', 'CY', ',%' ) UNION ALL " +
            "  SELECT " +
            "   u.id, " +
            "   u.build_area, " +
            "   belong_asset_id assetsId, " +
            "   u.STATUS, " +
            "   t.has_nodes  " +
            "  FROM " +
            "   zf_wy_unit_info u " +
            "   JOIN zf_wy_relation_tree t ON t.rel_id = u.id  " +
            "   AND t.del_flag = '0'  " +
            "  WHERE " +
            "   CONCAT( ',', u.type, ',' ) LIKE CONCAT( ',%', 'CY', ',%' )  " +
            "  ) a1 ON a1.assetsId = i.id " +
            "  LEFT JOIN zf_wy_use_rel b ON b.rel_id = a1.id  " +
            "  AND b.use_type = '4'  " +
            "  AND b.is_stop = '0'  " +
            "  AND b.del_flag = '0' " +
            "  LEFT JOIN zf_wy_contract c ON c.credit_id = b.credit_id  " +
            "  AND c.`status`='effective' " +
            " WHERE " +
            "  CONCAT( ',', i.type, ',' ) LIKE CONCAT( ',%', 'CY', ',%' )  " +
            "  AND i.del_flag = '0' " +
            //"  and v2.belong_type='政府物业' and  v2.wy_five ='产业用房' " +
            "and v2.id=#{id} GROUP BY i.id,i.NAME, " +
            " i.build_area ")
    CyDetailsVo cyDetails(@Param( "id" )String id);




    @Select( "SELECT a.buidingid as buildId,a.name,a.build_area as doorArea,c.name as buildName,c.build_area as buildTotalArea,v1.avg_price as avgPrice,zu.use_unit as qyCount  from zf_wy_industrialdoor a " +
            "    LEFT JOIN zf_wy_asset_info_v2 v2 on v2.id =#{id} " +
            "    LEFT JOIN zf_wy_asset_info v1 on v1.name=v2.name " +
            "    LEFT JOIN zf_wy_industrialfloor b on a.floorid=b.id " +
            "    LEFT JOIN zf_wy_industrialbuiding c on a.buidingid=c.id " +
            "    LEFT JOIN zf_wy_use_rel zu on zu.rel_id=v1.id " +
            "            where " +
            "    v1.id=a.parkid limit #{p},#{size}" )
    List<CyDetailsHouseInfoVo>cyDetailsHouseInfoList(@Param( "id" )String id,@Param( "p" )int p,@Param( "size" )int size);

    @Select( "SELECT count(a.buidingid) from zf_wy_industrialdoor a " +
            "    LEFT JOIN zf_wy_asset_info_v2 v2 on v2.id =#{id} " +
            "    LEFT JOIN zf_wy_asset_info v1 on v1.name=v2.name " +
            "    LEFT JOIN zf_wy_industrialfloor b on a.floorid=b.id " +
            "    LEFT JOIN zf_wy_industrialbuiding c on a.buidingid=c.id " +
            "    LEFT JOIN zf_wy_use_rel zu on zu.rel_id=v1.id " +
            "            where " +
            "    v1.id=a.parkid " )
    int cyDetailsHouseInfoListTotal(@Param( "id" )String id);



    //@Select( "SELECT u.id,u.name, u.build_area as area ,u.pre_enterprise as  preEnterprise,u.status " +
    //        "    FROM  zf_wy_unit_info u " +
    //        "    LEFT JOIN  zf_wy_relation_tree t ON  t.rel_id = u.id " +
    //        "    LEFT JOIN zf_wy_asset_info_v2 v2 on v2.id=#{id} " +
    //        "    LEFT JOIN zf_wy_asset_info v1 on v1.name=v2.name " +
    //        "    WHERE t.has_nodes = '0' and u.`status` = '2' " +
    //        "    and u.belong_asset_id = v1.id and u.name like concat('%', #{index},'%') limit #{p},#{size}" )

    //@Select("SELECT id,name,buildarea as area,preenterprise as preEnterprise,topname " +
    //        "FROM zf_cx_empty_wy_info " +
    //        "WHERE topid like concat('%', #{id},'%') " +
    //        "order by sort asc limit #{p},#{size}")
    List<KzDetailsVo> kzDetailsList(@Param( "id" )String id,@Param( "index" )String index,
                                    @Param( "p" )int p,@Param( "size" )int size,
                                    @Param( "param1" )Integer param1,@Param( "param2" )Integer param2);

    //@Select( "SELECT count(u.id) " +
    //        "    FROM  zf_wy_unit_info u " +
    //        "    LEFT JOIN  zf_wy_relation_tree t ON  t.rel_id = u.id " +
    //        "    LEFT JOIN zf_wy_asset_info_v2 v2 on v2.id=#{id} " +
    //        "    LEFT JOIN zf_wy_asset_info v1 on v1.name=v2.name " +
    //        "    WHERE t.has_nodes = '0' and u.`status` = '2' " +
    //        "    and u.belong_asset_id = v1.id and u.name like concat('%', #{index},'%')" )
    //@Select("SELECT count(id) FROM zf_cx_empty_wy_info WHERE topid like concat('%', #{id},'%')")
    int kzDetailsListTotal(@Param( "id" )String id,@Param( "index" )String index,
                           @Param( "param1" )Integer param1,@Param( "param2" )Integer param2);

    //@Select("SELECT id,name,buildarea as area,preenterprise as preEnterprise,topname FROM zf_cx_empty_wy_info2 WHERE topid like concat('%', #{id},'%') order by sort asc limit #{p},#{size}")
    List<KzDetailsVo> kzDetailsList2(@Param( "id" )String id,@Param( "index" )String index,@Param( "p" )int p,@Param( "size" )int size,
                                     @Param( "param1" )Integer param1,@Param( "param2" )Integer param2,@Param("propertytag")Integer propertytag);

    //@Select("SELECT count(id) FROM zf_cx_empty_wy_info2 WHERE topid like concat('%', #{id},'%')")
    int kzDetailsListTotal2(@Param( "id" )String id,@Param( "index" )String index,
                            @Param( "param1" )Integer param1,@Param( "param2" )Integer param2,@Param("propertytag")Integer propertytag);
    /**
     * 入驻列表
     * */
    @Select("SELECT  hc.JJHY as name,COUNT(hc.JJHY) as num FROM zf_wy_contract zc " +
            "    LEFT JOIN  zf_wy_asset_info  v1 on v1.id =#{id} " +
            "    LEFT JOIN zf_wy_contract_bind zb on zb.rel_id=v1.id " +
            "    LEFT JOIN qy_ywk_sszt_hc hc on hc.TYSHXYDM= zc.credit_id" +
            "            WHERE hc.JJHY!='' " +
            "    GROUP BY hc.JJHY")
    List<RzListVo>rzList(@Param( "id" )String id);

    /**
     * 入驻企业行业前五
     * */
//    @Select("SELECT  hc.JJHY as name,COUNT(hc.JJHY) as num FROM zf_wy_contract zc " +
//            "    LEFT JOIN  zf_wy_asset_info  v1 on v1.id =#{id} " +
//            "    LEFT JOIN zf_wy_contract_bind zb on zb.rel_id=v1.id " +
//            "    LEFT JOIN qy_ywk_sszt_hc hc on hc.TYSHXYDM= zc.credit_id" +
//            "            WHERE hc.JJHY!=''  " +
//            "    GROUP BY hc.JJHY  limit 0,5")
//    List<RzListVo>rz5List(@Param( "id" )String id);
    @Select(
            " SELECT  zs.JJHY as name,COUNT(zs.id) as num FROM " +
            "          zf_wy_unit_info a1 " +
            "             LEFT JOIN zf_wy_use_rel b ON b.rel_id = a1.id " +
            "            LEFT JOIN qy_ywk_sszt_hc zs on zs.QYMC=b.use_unit " +
            "              AND b.use_type = '4'  " +
            "             AND b.is_stop = '0'  " +
            "               AND b.del_flag = '0'  " +
            "            WHERE  a1.belong_asset_id = #{id} and b.rel_id!='' and zs.JJHY!='' " +
            "              GROUP BY zs.JJHY   " +
            "             ORDER BY COUNT(zs.id) DESC " +
            "            limit 0,5")
    List<RzListVo> rz5List(@Param( "id" )String id);



    /**
     * 入驻企业行业前五
     * */
    //@Select(     " SELECT  zs.JJHY as name,COUNT(zs.id) as num FROM " +
    //        "          zf_wy_unit_info a1 " +
    //        "             LEFT JOIN zf_wy_use_rel b ON b.rel_id = a1.id " +
    //        "            LEFT JOIN qy_ywk_sszt_hc zs on zs.QYMC=b.use_unit " +
    //        "              AND b.use_type = '4'  " +
    //        "             AND b.is_stop = '0'  " +
    //        "               AND b.del_flag = '0'  " +
    //        "            WHERE  a1.belong_asset_id = #{id} and b.rel_id!='' and zs.JJHY!='' " +
    //        "              GROUP BY zs.JJHY   " +
    //        "             ORDER BY COUNT(zs.id) DESC "  )
    @Select("SELECT zs.z AS NAME, COUNT( zs.a) AS num FROM zf_cx_basic_info zs WHERE f = #{id} GROUP BY z")
    List<RzListVo> rz5ListOtherTotal(@Param( "id" )String id);



    /**
     * 园区——入驻企业 预到期
     * */
   // @Select("SELECT  COUNT(zc.ID) FROM zf_wy_contract zc " +
//            "  LEFT JOIN  zf_wy_asset_info_v2  v2 on v2.id=#{id}  "+
//            "    LEFT JOIN  zf_wy_asset_info  v1 on v1.name =v2.name " +
//            "    LEFT JOIN zf_wy_contract_bind zb on zb.rel_id=v1.id " +
//            "    LEFT JOIN qy_ywk_sszt_hc hc on hc.TYSHXYDM= zc.credit_id" +
//            "            WHERE timestampdiff(MONTH,NOW(),zc.rent_date_end)<=1 and timestampdiff(MONTH,NOW(),zc.rent_date_end)>0" +
//            "    GROUP BY hc.QYMC")
    @Select( " SELECT   b.use_unit FROM    zf_wy_unit_info a1" +
            "    LEFT JOIN zf_wy_use_rel b ON b.rel_id = a1.id"+
            "   AND b.use_type = '4' "+
            "  AND b.is_stop = '0' "+
            "  AND b.del_flag = '0' "+
            " WHERE a1.belong_asset_id = #{id} and b.rel_id!=''"+
            "   GROUP BY b.use_unit")
    List<String>  ydq1Month(@Param( "id" )String id);

    @Select( " SELECT  COUNT(distinct zc.ID) FROM zf_wy_contract zc " +
            "    WHERE use_unit=#{name} and timestampdiff(MONTH,NOW(),zc.rent_date_end)> #{start} and timestampdiff(MONTH,NOW(),zc.rent_date_end)<=#{end} and zc.practical_use in('CY_BG','SY_CF') "
            )
    int  ydq1MonthNum(@Param( "name" )String  name,@Param( "start" )int  start,@Param( "end" )int  end);

    @Select( " SELECT  use_time FROM zf_wy_invest_time  WHERE name=#{name}")
    String  userTime(@Param( "name" )String  name);

    @Select( "SELECT b.use_unit as name, group_concat(a1.name) as roomNumber,sum(b.build_area) as buildArea,sum(re.cz) as cz,sum(re.total) as total,sum(re.bknum) as bknum,sum(re.yzj) as yzj  FROM zf_wy_unit_info a1 " +
            "  LEFT JOIN zf_wy_use_rel b ON b.rel_id = a1.id " +
            "  AND b.use_type = '4' " +
            "  AND b.is_stop = '0' " +
            " AND b.del_flag = '0' "+
            " LEFT JOIN zf_reg_tax  re on  re.name = b.use_unit  and re.year='2020'  "+
            "  WHERE a1.belong_asset_id = #{id}  and b.rel_id!='' and b.use_unit like concat('%', #{index},'%')  GROUP BY b.use_unit limit #{p},#{size} ")
    List<RzQyDetailsListVo>rzQyDetailsListVo(@Param( "id" )String id,@Param("index")String index,@Param( "p" )int p,@Param( "size" )int size);


    @Select( "SELECT * FROM zf_fcxy WHERE code=#{code} and year='2020' ORDER BY year ASC  " )
    FCXYVo rzQyFCRCFList(@Param( "code" )String code);




    //@Select( "SELECT  b.use_unit as name,b.credit_id as code,b.build_area as area,b.id FROM zf_wy_unit_info a1 " +
    //        "  LEFT JOIN zf_wy_use_rel b ON b.rel_id = a1.id " +
    //        "  AND b.use_type = '4' " +
    //        "  AND b.is_stop = '0' " +
    //        " AND b.del_flag = '0' WHERE  b.rel_id!='' and b.use_unit like concat('%', #{index},'%')  GROUP BY b.use_unit,b.credit_id,b.build_area,b.id limit #{p},#{size} ")
    @Select("select b as name , c as code,e as area,a as id  from zf_cx_basic_info where b LIKE concat( '%', #{index}, '%' ) group by b limit #{p},#{size} ")
    List<WyAssetsListVo>wyAssetsList(@Param("index")String index,@Param( "p" )int p,@Param( "size" )int size);




    List<WyListVo> wyList(WyListDTO dto);

    List<WyListVo> wyListTotal(WyListDTO dto);

    //@Select( "SELECT  a.*,b.code as code FROM zf_wy_analysis a left join new_company_info b on b.companyname = a.name  " +
    //        "  where  a.wyname like concat('%', #{wyname},'%') and a.year like concat('%', #{year},'%') " +
    //        "    and a.tztype like concat('%', #{tztype},'%')"+
    //        "    and a.tzresion like concat('%', #{tzresion},'%')"+
    //        "    and a.name like concat('%', #{name},'%')"+
    //        "   limit #{p},#{size} ")

    List<WyAnalysisVo> wyAnalysis(@Param("wyname")String wyname,
                                  @Param("tztype")String tztype,
                                  @Param("tzresion")String tzresion,
                                  @Param("name")String name,
                                  @Param("year")String year,
                                  @Param("p") int p,
                                  @Param("size") int size);

    //@Select( "SELECT  * FROM zf_wy_analysis " +
    //        "  where  wyname like concat('%', #{wyname},'%') and year like concat('%', #{year},'%') " +
    //        "    and tztype like concat('%', #{tztype},'%')"+
    //        "    and tzresion like concat('%', #{tzresion},'%')"+
    //        "    and name like concat('%', #{name},'%')"+
    //        "   ")
    Integer wyAnalysisTotal(@Param("wyname")String wyname,
                                       @Param("tztype")String tztype,
                                       @Param("tzresion")String tzresion,
                                       @Param("name")String name,
                                       @Param("year")String year);


    //@Select( "SELECT  b.use_unit as name,b.credit_id as code,b.build_area as area,b.id FROM zf_wy_unit_info a1 " +
    //        "  LEFT JOIN zf_wy_use_rel b ON b.rel_id = a1.id " +
    //        "  AND b.use_type = '4' " +
    //        "  AND b.is_stop = '0' " +
    //        " AND b.del_flag = '0' WHERE  b.rel_id!='' and b.use_unit like concat('%', #{index},'%')  GROUP BY b.use_unit,b.credit_id,b.build_area,b.id  ")
    @Select("select b as name , c as code,e as area,a as id  from zf_cx_basic_info where b LIKE concat( '%', #{index}, '%' )  group by b")
    List<WyAssetsListVo>wyAssetsListTotal(@Param("index")String index);

    @Select( "SELECT street_name FROM zf_wy_asset_info_v2 where street_name !='' GROUP BY street_name ")
    List<String>streetNameList();

    @Select( "SELECT community_name FROM zf_wy_asset_info_v2 where community_name !='' GROUP BY community_name ")
    List<String>communityNameList();

    @Select( "SELECT wy_five FROM zf_wy_asset_info_v2 where wy_five !='' GROUP BY wy_five ")
    List<String>wyTypeList();


    @Select( "SELECT id,name,build_area as area,wy_five as type FROM zf_wy_asset_info_v2 WHERE name like concat('%', #{index},'%') limit #{p},#{size}  ")
    List<WyAssetsListVo>wyAssetsInfoList(@Param("index")String index,@Param( "p" )int p,@Param( "size" )int size);

    @Select( "SELECT id,name,build_area as area,wy_five as type FROM zf_wy_asset_info_v2 WHERE name like concat('%', #{index},'%')   ")
    List<WyAssetsListVo>wyAssetsInfoListTotal(@Param("index")String index);





    @Select( "SELECT b.use_unit as name FROM zf_wy_unit_info a1 " +
            "  LEFT JOIN zf_wy_use_rel b ON b.rel_id = a1.id " +
            "  AND b.use_type = '4' " +
            "  AND b.is_stop = '0' " +
            " AND b.del_flag = '0' WHERE a1.belong_asset_id = #{id} and b.rel_id!='' and b.use_unit like concat('%', #{index},'%')  GROUP BY b.use_unit")
    List<RzQyDetailsListVo>rzQyDetailsListTotalVo(@Param( "id" )String id,@Param("index")String index);

    @Select( "select url from sys_img where zf_wy_asset_info_v2_id=#{id}" )
    List<String>imgUrl(@Param( "id" )String id);

    @Select( "select house_number as houseNumber,jg_time as jgTime,pizhun_no as pizhunNo,emptystate,yj_time as yjTime,man_unit as manUnit,wy_kind as wyKind,yj_unit as yjUnit,use_tel as useTel,street_name as streetName,build_area as buildArea,use_unit as  useUnit,address,act_area as actArea,belong_type as belongType,act_usage as actUsage,source,usageguiding,wy_scale as wyScale,syqssj,cert_has as certHas,cert_no as certNo,owner,cert_date as certDate,ruzhang,sfyrzpzfj,ruzhang_amount as ruzhangAmount,ruzhang_date as ruzhangDate,ruzhang_unit as ruzhangUnit,zfwysfcz,czczf,sfyczhtfj,czzlfs,czyzj,czdjbz,czqssj,czjssj,mzqx,czzljx," +
            "  zlshwysfqdht,zlshwyqsrq,zlshwydqrq,zlshwyyzj,zlshwyzldj,community_name as communityName,wygstel,wygsmc " +
            "  from zf_wy_asset_info_v2 " +
            "    WHERE id=#{id}" )
    WylistDetailsVo wylistDetails(@Param( "id" )String id);

    @Select( "SELECT tax2021 FROM zf_cx_basic_info WHERE b=#{name} limit 0,1" )
    Double nsTotal(@Param( "name" )String name);


    @Select( "SELECT * FROM zf_fcxy WHERE code=#{code} ORDER BY year ASC " )
    List<FCXYVo> fcxyList(@Param( "code" )String code);


    @Select( "SELECT COUNT(wymc) as num  from kj_fwbmmx where fwbmqk=#{code}  " )
    int houseCode(@Param( "code" )String code);


    @Select( " SELECT *  from kj_fwbmmx where fwbmqk like concat('%', #{type},'%') and wymc like concat('%', #{name},'%') and xxdz like concat('%', #{address},'%') limit #{p},#{size} " )
    List<HouseCodeListVo>houseCodeList(@Param( "type" )String type,@Param( "name" )String name,@Param( "address" )String address,@Param( "p" )int p,@Param( "size" )int size);

    @Select( " SELECT count(wymc)  from kj_fwbmmx where fwbmqk like concat('%', #{type},'%') and wymc like concat('%', #{name},'%') and xxdz like concat('%', #{address},'%')  " )
    int houseCodeListTotal(@Param( "type" )String type,@Param( "name" )String name,@Param( "address" )String address);

    @Select( " SELECT v1.id FROM zf_wy_asset_info_v2 v2 LEFT JOIN zf_wy_asset_info v1 on v1.name=v2.name  " +
            "   WHERE v2.id=#{id}  " )
    String V1Id(@Param( "id" )String id);

    @Select( " SELECT COUNT(id) FROM zf_wy_use_rel WHERE use_unit=#{name}" )
    int comNum(@Param( "name" )String name);
    @Select( " SELECT COUNT(ZXCOL_ID) FROM qy_lhrcinfo WHERE DWMC=#{name} " )
    int qyLhrcinfoNum(@Param( "name" )String name);

    @Select( " SELECT COUNT(org_name) FROM wy_sync_proposal WHERE org_name=#{name} " )
    int syncProposalNum(@Param( "name" )String name);


    @Select( " SELECT COUNT(APPLYUNITNAME) FROM pilottalents WHERE APPLYUNITNAME=#{name} " )
    int rcfcNym(@Param( "name" )String name);

    @Select( " SELECT COUNT(name) FROM support_room WHERE name=#{name} " )
    int rcfcNym1(@Param( "name" )String name);

    @Select( " SELECT user_name as userName,room_name as roomName,build_area as buildArea,contract_money as contractMoney ,contract_start_time as contractStartTime, " +
            "    contract_end_time as contractEndTime "+
            "    FROM support_room WHERE code=#{code} " )
    List<FCRCFListVo> FCRCFList(@Param( "code" )String code);

    @Select( " SELECT user_name as userName,room_name as roomName,build_area as buildArea,contract_money as contractMoney ,contract_start_time as contractStartTime, " +
            "    contract_end_time as contractEndTime "+
            "    FROM support_room WHERE name=#{companyname} and length(user_name) > 0 " )
    List<FCRCFListVo> FCRCFListByCompanyName(@Param( "companyname" )String companyname);



    @Select( " SELECT TYSHXYDM FROM qy_ywk_sszt_hc WHERE QYMC=#{name} " )
    String code(@Param( "name" )String name);

    @Select( " SELECT " +
            "  b.use_unit as name ,zs.code,zs.v1,zs.v2,zs.v3,zs.v4,zs.v5,zs.v6,zs.v7  " +
            ",zs.v8,zs.v9,zs.v10,zs.v11,zs.v12,zs.v13,zs.v14 FROM   " +
             "  zf_wy_unit_info a1 " +
            "  LEFT JOIN zf_wy_use_rel b ON b.rel_id = a1.id  " +
            "  LEFT JOIN zf_zscq_basic zs on zs.name=b.use_unit  " +
            " AND b.use_type = '4'  " +
            " AND b.is_stop = '0'  " +
            " AND b.del_flag = '0' " +
            "  WHERE " +
            " a1.belong_asset_id = #{id} and b.rel_id!='' and zs.code !='' " +
            "  GROUP BY b.use_unit,zs.v1,zs.v2,zs.v3,zs.v4,zs.v5,zs.v6,zs.v7 " +
            ",zs.v8,zs.v9,zs.v10,zs.v11,zs.v12,zs.v13,zs.v14,zs.code " )
    List<YQMoreAnalysisVo> YQMoreAnalysisList(@Param( "id" )String id);


    @Select( "SELECT sum(zb.total) as num,zs.JJHY as name FROM " +
            "zf_wy_unit_info a1 " +
            "   LEFT JOIN zf_wy_use_rel b ON b.rel_id = a1.id " +
            " LEFT JOIN qy_ywk_sszt_hc zs on zs.QYMC=b.use_unit  LEFT JOIN zf_zscq_basic zb on zb.name= zs.QYMC " +
            "   AND b.use_type = '4' " +
            "  AND b.is_stop = '0'  " +
            "   AND b.del_flag = '0'  " +
            " WHERE  a1.belong_asset_id = #{id} and b.rel_id!='' and zs.JJHY!='' " +
            "   GROUP BY zs.JJHY  " +
            "  ORDER BY sum(zb.total)  DESC " +
            "limit 0,10 " )

    List<Top10ListVo> Top10List(@Param( "id" )String id);


    @Select( "SELECT year,cz,total,area,guideprice,price,yzj,number FROM  zf_reg_tax WHERE code=#{code} ORDER BY year ASC " )
    List<RegTaxVo> regTaxList(@Param( "code" )String code);


    @Select( " SELECT zr.year,COUNT(zr.code) as num,sum(zr.cz) as cz,sum(zr.total) as total,sum(zr.area) as area,sum(zr.guideprice) as guideprice,sum(zr.price) as price ,sum(zr.yzj) as yzj, sum(zr.number) as number " +
            "  FROM zf_wy_unit_info a1  LEFT JOIN zf_wy_use_rel b ON b.rel_id = a1.id " +
            "  LEFT JOIN  zf_reg_tax zr on zr.code=b.credit_id AND b.use_type = '4' " +
            "  AND b.is_stop = '0'  " +
            " AND b.del_flag = '0'  WHERE  a1.belong_asset_id = #{id} and b.rel_id!='' and zr.year!='' " +
            "  GROUP BY zr.year" )
    List<RegTaxVo> regTax(@Param( "id" )String id);

    @Select( " SELECT zr.year,COUNT(zr.code) as num,sum(zr.cz) as cz,sum(zr.total) as total,sum(zr.area) as area,sum(zr.guideprice) as guideprice,sum(zr.price) as price ,sum(zr.yzj) as yzj, sum(zr.number) as number " +
            "  FROM zf_wy_unit_info a1  LEFT JOIN zf_wy_use_rel b ON b.rel_id = a1.id " +
            "  LEFT JOIN  zf_reg_tax zr on zr.code=b.credit_id AND b.use_type = '4' " +
            "  AND b.is_stop = '0'  " +
            " AND b.del_flag = '0'  WHERE  b.rel_id!='' and zr.year!='' " +
            "  GROUP BY zr.year" )
    List<RegTaxVo> cyReTax();

    @Select( " SELECT v2.name,v1.id from zf_wy_asset_info_v2 v2 " +
            "   LEFT JOIN zf_wy_asset_info v1 on v1.name=v2.name WHERE v2.belong_type='政府物业' and v2.wy_five='产业用房' " )
    List<ZlRegTaxVo> zlRegTaxList();


    List<EnterpriseSidewalkVo> enterpriseSidewalk(@Param( "type" )int type);


    List<Map> enterpriseList(@Param( "sort" )Integer sort,
                             @Param( "name" )String name,
                             @Param( "parkName" )String parkName,
                             @Param( "p" )Integer p,
                             @Param( "size" )Integer size,
                             @Param( "param1" )Integer param1,
                             @Param( "param2" )Integer param2,
                             @Param( "belongType" )String belongType,
                             @Param( "companyindustry" )String companyindustry);

    int enterpriseListTotal(@Param( "sort" )Integer sort, @Param( "name" )String name, @Param( "parkName" )String parkName,@Param( "param1" )Integer param1,
                            @Param( "param2" )Integer param2,
                            @Param( "belongType" )String belongType,
                            @Param( "companyindustry" )String companyindustry);

    List<Map> enterpriseListTotalBuildArea(@Param( "sort" )Integer sort, @Param( "name" )String name, @Param( "parkName" )String parkName,@Param( "param1" )Integer param1,
                                 @Param( "param2" )Integer param2,
                                 @Param( "belongType" )String belongType);


    /**
     * 查询空置列表
     * @param id
     * @param p
     * @param size
     * @return
     */
    @Select( "select * from zf_cx_empty_wy_info where topid = #{id} and `usestatus` = 0 limit #{p},#{size}" )
    List<KzDetailsVo> emptyList(@Param( "id" )String id,@Param( "p" )int p,@Param( "size" )int size);

    @Select( "select count(id) from zf_cx_empty_wy_info where topid = #{id} and `usestatus` = 0 " )
    int emptyListTotal(@Param( "id" )String id);

    //顶级园区
    List<Map> enterpriseTopWyName();

    List<Map> enterpriseTalent(@Param( "name" )String name, @Param( "p" )Integer p, @Param( "size" )Integer size);

    Integer enterpriseTalentTotal(Integer sort, String name, String parkName);


    @Select("select name from zf_wy_asset_info_v2 where id =  #{id}")
    String selectCurrentWyNameByV1Id(@Param( "id" )String id);

    @Select("SELECT count(id) as number FROM zf_cx_empty_wy_info WHERE topname =( SELECT NAME FROM zf_wy_asset_info_v2 WHERE id = #{id})")
    Integer getEmptyInfo(@Param( "id" )String id);


    @Select( "SELECT count(DISTINCT b.use_unit) as number FROM zf_wy_unit_info a1 " +
            "  LEFT JOIN zf_wy_use_rel b ON b.rel_id = a1.id " +
            "  AND b.use_type = '4' " +
            "  AND b.is_stop = '0' " +
            " AND b.del_flag = '0' WHERE a1.belong_asset_id = #{id} and b.rel_id!='' and b.use_unit like concat('%', #{index},'%')")
    Integer rzQyDetailsListTotalNumber(@Param( "id" )String id,@Param("index")String index);

    @Select( "select * from zf_cx_rent_detail_info where wytopname = #{name}" )
    List<Map> selectNewRzList(@Param( "name" )String name);


    //@Select( " SELECT * FROM zf_wy_contract zc " +
    //        "    WHERE use_unit=#{name} and timestampdiff(MONTH,NOW(),zc.rent_date_end)> #{start} and timestampdiff(MONTH,NOW(),zc.rent_date_end)<=#{end}"
    //)
    //@Select("SELECT a.use_unit, a.usable_area, a.price, a.deposit, a.unit_price, a.practical_use, a.rent_date_start, a.rent_date_end , GROUP_CONCAT(c.name) as wyname,d.label as use FROM zf_wy_contract a left join zf_wy_contract_bind b on a.id = b.contract_id left join zf_wy_relation_tree c on b.rel_id = c.rel_id left join sys_dict_value d on d.value = a.practical_use WHERE a.use_unit=#{name} and timestampdiff(MONTH,NOW(),a.rent_date_end)> #{start} and timestampdiff(MONTH,NOW(),a.rent_date_end)<=#{end} GROUP BY a.use_unit")
    //@Select("SELECT\n" +
    //        "\ta.use_unit,\n" +
    //        "\ta.usable_area,\n" +
    //        "\ta.price,\n" +
    //        "\ta.deposit,\n" +
    //        "\ta.unit_price,\n" +
    //        "\tDATE_FORMAT(a.rent_date_start,'%Y年%m月%d日') as rent_date_start,\n" +
    //        "\tDATE_FORMAT(a.rent_date_end,'%Y年%m月%d日') as rent_date_end,\n" +
    //        "\td.label,\n" +
    //        "\tGROUP_CONCAT(distinct c.NAME ) AS wyname, \n" +
    //        "\te.TYSHXYDM as code \n" +
    //        "FROM\n" +
    //        "\tzf_wy_contract a\n" +
    //        "\tLEFT JOIN zf_wy_contract_bind b ON a.id = b.contract_id\n" +
    //        "\tLEFT JOIN zf_wy_relation_tree c ON b.rel_id = c.rel_id\n" +
    //        "\tLEFT JOIN sys_dict_value d ON d.\n" +
    //        "VALUE\n" +
    //        "\t= a.practical_use \n" +
    //        " left join qy_ywk_sszt_hc e on e.QYMC = a.use_unit \n" +
    //        "WHERE\n" +
    //        "c.name like concat('%' , #{wyname} , '%')"+
    //        "\tand a.use_unit = #{name} \n" +
    //        "\tand a.practical_use in('CY_BG','SY_CF') \n" +
    //        "\tand timestampdiff(MONTH,NOW(),a.rent_date_end)> #{start} \n" +
    //        "\tand timestampdiff(MONTH,NOW(),a.rent_date_end)<=#{end} \n" +
    //        "\tGROUP BY a.use_unit")
    List<Map> ydq1MonthMap(@Param( "name" )String  name,@Param( "start" )int  start,@Param( "end" )int  end , @Param("wyname")String wyname);

    Integer ydq1MonthMapNumber(@Param( "name" )String  name,@Param( "start" )int  start,@Param( "end" )int  end , @Param("wyname")String wyname);

    @Select("select b as companyname,z as industry from zf_cx_basic_info where f = #{name} and z =  #{industry}")
    List<Map> selectWyIndustry(@Param( "name" )String name,@Param( "industry" ) String industry);

    List<Map> enterpriseList2(@Param( "sort" )Integer sort, @Param( "name" )String name, @Param( "parkName" )String parkName, @Param( "p" )Integer p, @Param( "size" )Integer size);

    List<Map> enterpriseList3(@Param( "sort" )Integer sort,
                              @Param( "name" )String name,
                              @Param( "parkName" )String parkName,
                              @Param( "p" )Integer p,
                              @Param( "size" )Integer size,
                              @Param( "manUnit" )String manUnit,
                              @Param( "companyindustry" )String companyindustry);

    int enterpriseList3Total(@Param( "sort" )Integer sort, @Param( "name" )String name, @Param( "parkName" )String parkName,
                             @Param( "manUnit" )String manUnit);

    int enterpriseListTotal2(@Param( "sort" )Integer sort, @Param( "name" )String name, @Param( "parkName" )String parkName);

    //采用企服的入驻企业数
    @Select("select count(*) as number from zf_cx_basic_info where f = #{name}")
    int enterCompanyNumber(@Param("name") String id );

    //@Select("SELECT\n" +
    //        "\tc.NAME as name,\n" +
    //        "\te.label as entry_type,\n" +
    //        "\ta.entry_price,\n" +
    //        "\tDATE_FORMAT(a.entry_time,\"%Y年%m月%d日\") as entry_time\n" +
    //        "FROM\n" +
    //        "\twy_entry a\n" +
    //        "\tLEFT JOIN wy_entry_bind b ON a.id = b.entry_id\n" +
    //        "\tJOIN zf_wy_relation_tree c ON c.rel_id = b.rel_id\n" +
    //        "\tleft join sys_dict_value e on e.dict_type_id = \"e335b4afc8204b1fb8a5b21b502454f1\" and a.entry_type = e.`value`\n" +
    //        " limit #{p},#{size} ")
    //@Select("SELECT name,\n" +
    //        "\tDATE_FORMAT(ruzhang_date,\"%Y年%m月%d日\")  as entry_time, \n" +
    //        "\truzhang_amount as entry_price,\n" +
    //        "\truzhang_unit \n" +
    //        "FROM\n" +
    //        "\tzf_wy_asset_info_v2 \n" +
    //        "WHERE\n" +
    //        "\truzhang = \"是\" \n" +
    //        "\tAND belong_type = '政府物业'\n" +
    //        "\tlimit #{p},#{size}")

    @Select("select wyname as name , DATE_FORMAT(entrydate,\"%Y年%m月%d日\")  as entry_time, entrymoney as entry_price,entrytype as entry_type, '' as ruzhang_unit from wy_entry_info limit #{p},#{size}")
    List<Map> newwyentrylist(@Param("p") Integer p, @Param("size")Integer size);

    //@Select("SELECT count(a.id) as number \n" +
    //        "\tFROM\n" +
    //        "\twy_entry a\n" +
    //        "\tLEFT JOIN wy_entry_bind b ON a.id = b.entry_id\n" +
    //        "\tJOIN zf_wy_relation_tree c ON c.rel_id = b.rel_id\n" +
    //        "\tleft join sys_dict_value e on e.dict_type_id = \"e335b4afc8204b1fb8a5b21b502454f1\" and a.entry_type = e.`value`\n")

    //@Select("select count(*) from zf_wy_asset_info_v2 where ruzhang = \"是\" and belong_type = '政府物业' ")
    @Select("select count(*) from wy_entry_info")
    int newwyentrylistTotal();

    //@Select("\tSELECT c.tota_area ,\n" +
    //        "\ta.equity_cert_no,\n" +
    //        "\ta.obligee,\n" +
    //        "\tc.NAME as name ,\n" +
    //        "\ta.rel_price,\n" +
    //        "\te.label as right_type,\n" +
    //        "\tf.label as right_nature,\n" +
    //        "\tDATE_FORMAT(a.cert_rel_date,\"%Y年%m月%d日\") as cert_rel_date\n" +
    //        "FROM\n" +
    //        "\twy_equity_rel a\n" +
    //        "\tLEFT JOIN wy_equity_bind b ON a.id = b.equity_id\n" +
    //        "\tJOIN zf_wy_relation_tree c ON c.rel_id = b.rel_id\n" +
    //        "\tleft join sys_dict_value e on e.dict_type_id = \"f06f73c18efb40679c6b0289efda033d\" and a.right_type = e.`value`\n" +
    //        "\tleft join sys_dict_value f on f.dict_type_id = \"2b500a70cb6a498d86873778c5129439\" and a.right_nature = f.`value`\n" +
    //        " limit #{p},#{size} ")

    //@Select("SELECT\n" +
    //        "\tbuild_area as tota_area , \n" +
    //        "\tcert_no as equity_unit_no,\n" +
    //        "\t`owner` as obligee,\n" +
    //        "\tname, \n" +
    //        "\tDATE_FORMAT(cert_date,\"%Y年%m月%d日\") as cert_rel_date\n" +
    //        "FROM\n" +
    //        "\tzf_wy_asset_info_v2 \n" +
    //        "WHERE\n" +
    //        "\tbelong_type = '政府物业' \n" +
    //        "\tAND cert_has = '是'\n" +
    //        "\tlimit #{p},#{size}")
    @Select("select jzmj as tota_area , cqzsh as equity_unit_no , qlr as  obligee , wymc as name , DATE_FORMAT(kssj,\"%Y年%m月%d日\") as cert_rel_date  from wy_cqdj limit #{p},#{size}")
    List<Map> newwyequitylist(@Param("p") Integer p, @Param("size")Integer size);

    //@Select("\tSELECT\n" +
    //        "\tcount(a.id) as number\n" +
    //        "FROM\n" +
    //        "\twy_equity_rel a\n" +
    //        "\tLEFT JOIN wy_equity_bind b ON a.id = b.equity_id\n" +
    //        "\tJOIN zf_wy_relation_tree c ON c.rel_id = b.rel_id\n" +
    //        "\tleft join sys_dict_value e on e.dict_type_id = \"f06f73c18efb40679c6b0289efda033d\" and a.right_type = e.`value`\n" +
    //        "\tleft join sys_dict_value f on f.dict_type_id = \"2b500a70cb6a498d86873778c5129439\" and a.right_nature = f.`value`\n")
    //@Select("select COUNT(id) from zf_wy_asset_info_v2 where  belong_type='政府物业' and cert_has='是'")
    @Select("select count(*) from wy_cqdj")
    int newwyequitylistTotal();

    //获取所有入账的物业pids
    @Select("select b.parent_ids from wy_entry_bind a join zf_wy_relation_tree b on a.rel_id = b.rel_id")
    List<String> wyentryname();

    @Select("select id from zf_wy_relation_tree where parent_id = \"bf680f0de90143de8db075c31139bbd8\"")
    List<String> selectAllTopWyId();

    @Select("select b.parent_ids from wy_equity_bind a join zf_wy_relation_tree b on a.rel_id = b.rel_id")
    List<String> wyequityname();

    //@Select("SELECT\n" +
    //        "\tcount(a.id) as totalnumber, \n" +
    //        "\tsum(c.tota_area) as totalbuildarea,\n" +
    //        "\tsum(a.entry_price) as totalprice\n" +
    //        "FROM\n" +
    //        "\twy_entry a\n" +
    //        "\tLEFT JOIN wy_entry_bind b ON a.id = b.entry_id\n" +
    //        "\tJOIN zf_wy_relation_tree c ON c.rel_id = b.rel_id\n")
    @Select("select count(id) as totalnumber, \n" +
            "\tsum(buildarea) as totalbuildarea,\n" +
            "\tsum(entrymoney) as totalprice\n" +
            "\tfrom \n" +
            "\twy_entry_info")
    Map selectTotalWyEntryInfo();

    //@Select("SELECT\n" +
    //        "\t\tcount(a.id) as totalnumber,\n" +
    //        "\t\tsum(c.tota_area) as totalbuildarea,\n" +
    //        "\t\tsum(a.rel_price) as totalrelprice\n" +
    //        "FROM\n" +
    //        "\twy_equity_rel a\n" +
    //        "\tLEFT JOIN wy_equity_bind b ON a.id = b.equity_id\n" +
    //        "\tJOIN zf_wy_relation_tree c ON c.rel_id = b.rel_id")

    @Select("select count(id) as totalnumber,\n" +
        "sum(jzmj) as totalbuildarea,\n" +
        "sum(0) as totalrelprice from wy_cqdj")
    Map selectToatlWyEquityInfo();


    @Select("SELECT\n" +
            "\ta.use_unit ,\n" +
            "\ta.unit_price,\n" +
            "\ta.deposit,\n" +
            "\ta.price,\n" +
            "\ta.usable_area,\n" +
            "\tDATE_FORMAT(a.rent_date_start,'%Y年%m月%d日') as rent_date_start,\n" +
            "\tDATE_FORMAT(a.rent_date_end,'%Y年%m月%d日') as rent_date_end,\n" +
            "\ta.rent_limit,\n" +
            "\tGROUP_CONCAT(distinct c.name) as wyname,\n" +
            "\td.label,\n" +
            "\ta.cert_no as code\n" +
            "FROM\n" +
            "\tzf_wy_contract a \n" +
            "\tleft join zf_wy_contract_bind b on a.id = b.contract_id\n" +
            "\t join zf_wy_relation_tree c on c.rel_id = b.rel_id\n" +
            "\tleft join sys_dict_value d on d.`value` = a.practical_use\n" +
            "WHERE\n" +
            "\ttimestampdiff( MONTH, NOW(), a.rent_date_end )> #{param1} \n" +
            "\tAND timestampdiff( MONTH, NOW(), a.rent_date_end )<= #{param2}\n" +
            "\tand a.update_state = \"normal\"\n" +
            "\tand a.practical_use in('CY_BG','SY_CF') \n" +
            "\tGROUP BY a.use_unit\n" +
            "\tlimit #{start},#{end}")
    List<Map> selectAllWyContractInfo(@Param( "start" )int  start,@Param( "end" )int  end , @Param( "param1" )int  param1,@Param( "param2" )int  param2);

    @Select("SELECT\n" +
            "\tcount( a.id ) as number\n" +
            "FROM\n" +
            "\t(\n" +
            "\tSELECT\n" +
            "\t\ta.id \n" +
            "\tFROM\n" +
            "\t\tzf_wy_contract a\n" +
            "\t\tLEFT JOIN zf_wy_contract_bind b ON a.id = b.contract_id\n" +
            "\t\t JOIN zf_wy_relation_tree c ON c.rel_id = b.rel_id\n" +
            "\t\tLEFT JOIN sys_dict_value d ON d.`value` = a.practical_use \n" +
            "\tWHERE\n" +
            "\t\ttimestampdiff( MONTH, NOW(), a.rent_date_end ) >  #{param1} \n" +
            "\t\tAND timestampdiff( MONTH, NOW(), a.rent_date_end ) <= #{param2} \n" +
            "\t\tAND a.update_state = \"normal\" \n" +
            "\tand a.practical_use in('CY_BG','SY_CF') \n" +
            "\tGROUP BY\n" +
            "\t\ta.use_unit \n" +
            "\t) a\n")
    int selectAllWyContractInfoTotal(@Param( "param1" )int  param1,@Param( "param2" )int  param2);

    //@Select("SELECT\n" +
    //        "\ta.use_unit ,\n" +
    //        "\ta.unit_price,\n" +
    //        "\ta.deposit,\n" +
    //        "\ta.price,\n" +
    //        "\ta.usable_area,\n" +
    //        "\tDATE_FORMAT(a.rent_date_start,'%Y年%m月%d日') as rent_date_start,\n" +
    //        "\tDATE_FORMAT(a.rent_date_end,'%Y年%m月%d日') as rent_date_end,\n" +
    //        "\ta.rent_limit,\n" +
    //        "\tGROUP_CONCAT(c.name) as wyname,\n" +
    //        "\td.label,\n" +
    //        "\ta.cert_no as code\n" +
    //        "FROM\n" +
    //        "\tzf_wy_contract a \n" +
    //        "\tleft join zf_wy_contract_bind b on a.id = b.contract_id\n" +
    //        "\tleft join zf_wy_relation_tree c on c.rel_id = b.rel_id\n" +
    //        "\tleft join sys_dict_value d on d.`value` = a.practical_use\n" +
    //        "WHERE\n" +
    //        "\ttimestampdiff( MONTH, NOW(), a.rent_date_end )> #{param1} \n" +
    //        "\tAND timestampdiff( MONTH, NOW(), a.rent_date_end )<= #{param2}\n" +
    //        "\tand a.update_state = \"normal\"\n" +
    //        "\tand a.practical_use not in('CY_BG','SY_CF') \n" +
    //        "\tGROUP BY a.use_unit\n" +
    //        "\tlimit #{start},#{end}")

    //@Select("SELECT\n" +
    //        "\t* \n" +
    //        "FROM\n" +
    //        "\tnot_cy_contract_delay_time a \n" +
    //        "WHERE\n" +
    //        "\ttimestampdiff( MONTH, NOW(), a.rent_date_end )> #{param1} \n" +
    //        "\tAND timestampdiff( MONTH, NOW(), a.rent_date_end )<= #{param2} \n" +
    //        "GROUP BY\n" +
    //        "\ta.use_unit\n" +
    //        "limit #{start},#{end}")
    List<Map> selectNotCyAllWyContractInfo(@Param( "start" )int  start,@Param( "end" )int  end , @Param( "param1" )int  param1,@Param( "param2" )int  param2);

    //@Select("SELECT\n" +
    //        "\tcount( a.id ) as number\n" +
    //        "FROM\n" +
    //        "\t(\n" +
    //        "\tSELECT\n" +
    //        "\t\ta.id \n" +
    //        "\tFROM\n" +
    //        "\t\tzf_wy_contract a\n" +
    //        "\t\tLEFT JOIN zf_wy_contract_bind b ON a.id = b.contract_id\n" +
    //        "\t\tLEFT JOIN zf_wy_relation_tree c ON c.rel_id = b.rel_id\n" +
    //        "\t\tLEFT JOIN sys_dict_value d ON d.`value` = a.practical_use \n" +
    //        "\tWHERE\n" +
    //        "\t\ttimestampdiff( MONTH, NOW(), a.rent_date_end ) >  #{param1} \n" +
    //        "\t\tAND timestampdiff( MONTH, NOW(), a.rent_date_end ) <= #{param2} \n" +
    //        "\t\tAND a.update_state = \"normal\" \n" +
    //        "\tand a.practical_use not in('CY_BG','SY_CF') \n" +
    //        "\tGROUP BY\n" +
    //        "\t\ta.use_unit \n" +
    //        "\t) a\n")
    @Select("SELECT\n" +
            "\tcount(*) \n" +
            "FROM\n" +
            "\tnot_cy_contract_delay_time a \n" )
    int selectNotCyAllWyContractInfoTotal(@Param( "param1" )int  param1,@Param( "param2" )int  param2);

    @Select("SELECT\n" +
            "\ta.use_unit ,\n" +
            "\ta.unit_price,\n" +
            "\ta.deposit,\n" +
            "\ta.price,\n" +
            "\ta.usable_area,\n" +
            "\tDATE_FORMAT(a.rent_date_start,'%Y年%m月%d日') as rent_date_start,\n" +
            "\tDATE_FORMAT(a.rent_date_end,'%Y年%m月%d日') as rent_date_end,\n" +
            "\ta.rent_limit,\n" +
            "\tGROUP_CONCAT(distinct c.name) as wyname,\n" +
            "\td.label,\n" +
            "\ta.cert_no as code\n" +
            "FROM\n" +
            "\tzf_wy_contract a \n" +
            "\tleft join zf_wy_contract_bind b on a.id = b.contract_id\n" +
            "\tleft join zf_wy_relation_tree c on c.rel_id = b.rel_id\n" +
            "\tleft join sys_dict_value d on d.`value` = a.practical_use\n" +
            "WHERE\n" +
            "\ttimestampdiff( YEAR,a.rent_date_start, a.rent_date_end ) >= #{param1} \n" +
            "\tand timestampdiff( YEAR,a.rent_date_start, a.rent_date_end ) < #{param2} \n" +
            "\tand a.update_state = \"normal\"\n" +
            "\t\tAND find_in_set(c.create_by , #{belongType}) \n" +
            "\tand a.practical_use in('CY_BG','SY_CF') \n" +
            "\tGROUP BY a.use_unit\n" +
            "\tlimit #{start},#{end}")
    List<Map> selectLongWyContractInfo(@Param( "start" )int  start,@Param( "end" )int  end,@Param( "param1" )int  param1,@Param( "param2" )int  param2,@Param( "belongType" )String  belongType);

    @Select("SELECT\n" +
            "\tcount( a.id ) as number\n" +
            "FROM\n" +
            "\t(\n" +
            "\tSELECT\n" +
            "\t\ta.id \n" +
            "\tFROM\n" +
            "\t\tzf_wy_contract a\n" +
            "\t\tLEFT JOIN zf_wy_contract_bind b ON a.id = b.contract_id\n" +
            "\t\tLEFT JOIN zf_wy_relation_tree c ON c.rel_id = b.rel_id\n" +
            "\t\tLEFT JOIN sys_dict_value d ON d.`value` = a.practical_use \n" +
            "\tWHERE\n" +
            "\t\ttimestampdiff( YEAR, a.rent_date_start, a.rent_date_end ) >= #{param1} \n" +
            "\t\tand timestampdiff( YEAR, a.rent_date_start, a.rent_date_end ) < #{param2} \n" +
            "\t\tAND a.update_state = \"normal\" \n" +
            "\t\tAND find_in_set(c.create_by , #{belongType}) \n" +
            "\tand a.practical_use in('CY_BG','SY_CF') \n" +
            "\tGROUP BY\n" +
            "\ta.use_unit \n" +
            "\t) a")
    int selectLongWyContractInfoTotal(@Param( "param1" )int  param1,@Param( "param2" )int  param2,@Param( "belongType" )String  belongType);

    //所有租期长的合同 非CY
    //@Select("SELECT\n" +
    //        "\ta.use_unit ,\n" +
    //        "\ta.unit_price,\n" +
    //        "\ta.deposit,\n" +
    //        "\ta.price,\n" +
    //        "\ta.usable_area,\n" +
    //        "\tDATE_FORMAT(a.rent_date_start,'%Y年%m月%d日') as rent_date_start,\n" +
    //        "\tDATE_FORMAT(a.rent_date_end,'%Y年%m月%d日') as rent_date_end,\n" +
    //        "\ta.rent_limit,\n" +
    //        "\tGROUP_CONCAT(c.name) as wyname,\n" +
    //        "\td.label,\n" +
    //        "\ta.cert_no as code\n" +
    //        "FROM\n" +
    //        "\tzf_wy_contract a \n" +
    //        "\tleft join zf_wy_contract_bind b on a.id = b.contract_id\n" +
    //        "\tleft join zf_wy_relation_tree c on c.rel_id = b.rel_id\n" +
    //        "\tleft join sys_dict_value d on d.`value` = a.practical_use\n" +
    //        "WHERE\n" +
    //        "\ttimestampdiff( YEAR,a.rent_date_start, a.rent_date_end ) >= #{param1} \n" +
    //        "\tand timestampdiff( YEAR,a.rent_date_start, a.rent_date_end ) < #{param2} \n" +
    //        //"\tand a.update_state = \"normal\"\n" +
    //        "\tand a.practical_use not in('CY_BG','SY_CF') \n" +
    //        "\tGROUP BY a.use_unit\n" +
    //        "\tlimit #{start},#{end}")
    @Select("SELECT\n" +
            "\tb.id AS wyid,\n" +
            "\ta.* \n" +
            "FROM\n" +
            "\tnot_cy_contract_long_time a\n" +
            "\tLEFT JOIN zf_wy_asset_info_v2 b ON a.wyname = b.`name` limit #{start},#{end}")
    List<Map> selectNotCyLongWyContractInfo(@Param( "start" )int  start,@Param( "end" )int  end,@Param( "param1" )int  param1,@Param( "param2" )int  param2);


    //@Select("SELECT\n" +
    //        "\tcount( a.id ) as number\n" +
    //        "FROM\n" +
    //        "\t(\n" +
    //        "\tSELECT\n" +
    //        "\t\ta.id \n" +
    //        "\tFROM\n" +
    //        "\t\tzf_wy_contract a\n" +
    //        "\t\tLEFT JOIN zf_wy_contract_bind b ON a.id = b.contract_id\n" +
    //        "\t\tLEFT JOIN zf_wy_relation_tree c ON c.rel_id = b.rel_id\n" +
    //        "\t\tLEFT JOIN sys_dict_value d ON d.`value` = a.practical_use \n" +
    //        "\tWHERE\n" +
    //        "\t\ttimestampdiff( YEAR, a.rent_date_start, a.rent_date_end ) >= #{param1} \n" +
    //        "\t\tand timestampdiff( YEAR, a.rent_date_start, a.rent_date_end ) < #{param2} \n" +
    //        "\t\tAND a.update_state = \"normal\" \n" +
    //        "\tand a.practical_use not in('CY_BG','SY_CF') \n" +
    //        "\tand c.name not in('百旺信高科技工业园','汇通大厦','南山云科技大厦','创新大厦') \n" +
    //        "\tGROUP BY\n" +
    //        "\ta.use_unit \n" +
    //        "\t) a")
    @Select("SELECT\n" +
            "\tcount(a.id) \n" +
            "FROM\n" +
            "\tnot_cy_contract_long_time a \n")
    Integer selectNotCyLongWyContractInfoTotal(@Param( "param1" )int  param1,@Param( "param2" )int  param2);


    //@Select("select name as text , housecode as gbcode from wy_basic_info_simple where pid = (select id from wy_basic_info_simple where name = #{wyname} )")

    @Select("SELECT\n" +
            "\ta.name as text,\n" +
            "\tb.house_number as gbcode\n" +
            "FROM\n" +
            "\tzf_wy_relation_tree  a\n" +
            "\tleft join zf_wy_unit_info b on b.id = a.rel_id\n" +
            "WHERE\n" +
            "\ta.parent_id = (\n" +
            "\tSELECT\n" +
            "\t\tid \n" +
            "\tFROM\n" +
            "\t\tzf_wy_relation_tree \n" +
            "\tWHERE\n" +
            "\t\tNAME = #{wyname} \n" +
            "\tAND type = 2 and has_nodes = 1)")
    List<Map> selectChildrenByTopWyName(@Param( "wyname" )String wyname);

    //@Select("select name,gps from wy_basic_info_simple where FIND_IN_SET(wy_five,#{assettype})  and pid = \"0\"")
    @Select("SELECT\n" +
            "\tv2.build_area AS area,\n" +
            "\tv2.NAME AS street,\n" +
            "\td.addr AS address \n" +
            "FROM\n" +
            "\tzf_wy_asset_info_v2 v2\n" +
            "\tLEFT JOIN zf_cy_address d ON d.NAME = v2.NAME \n" +
            "WHERE\n" +
            "\tv2.belong_type = \"政府物业\" \n" +
            "\tAND FIND_IN_SET(v2.wy_five,#{assettype})  ;")
    List<Map> selectWySimpleInfoByAssetType(@Param("assettype") String assettype);

    //@Select("SELECT\n" +
    //        "\tid,\n" +
    //        "\tNAME as name,\n" +
    //        "\tbelong_type,\n" +
    //        "\tstreet_name,\n" +
    //        "\tbuild_area,\n" +
    //        "\twy_five \n" +
    //        "FROM\n" +
    //        "\tzf_wy_asset_info_v2\n" +
    //        "\twhere name like concat('%', #{name},'%')\n" +
    //        "\tand find_in_set( belong_type , #{belong_type} )\n" +
    //        "\tand find_in_set( street_name , #{streetName} )\n" +
    //        "\tand wy_five like concat('%', #{wy_five},'%') order by build_area #{sort} limit #{p},#{size} ")
    List<Map> selectWyInfo(@Param( "name" )String  name,
                           @Param( "belong_type" )String  belong_type,
                           @Param( "wy_five" )String  wy_five,
                           @Param( "p" )Integer
                                   param2,@Param( "size" )int  param1,
                           @Param( "streetName" )String  streetName,
                           @Param( "sort" )int sort,
                           @Param( "wy_find" )String wy_find,
                           @Param( "param1" )Integer param3,
                           @Param( "param2" )Integer param4,
                           @Param( "manUnit" )String manUnit);

    @Select("SELECT\n" +
            "\tcount(id) as number\n" +
            "\tFROM\n" +
            "\tzf_wy_asset_info_v2\n" +
            "\twhere name like concat('%', #{name},'%')\n" +
            "\tand find_in_set( belong_type , #{belong_type} )\n" +
            "\tand street_name like concat('%', #{streetName} ,'%') \n" +
            "\tand wy_kind like concat('%', #{wy_find} ,'%') \n" +
            "\tand wy_five like concat('%', #{wy_five},'%')")
    int selectWyInfoTotal(@Param( "name" )String  name,@Param( "belong_type" )String  belong_type,@Param( "wy_five" )String  wy_five,@Param( "streetName" )String  streetName,@Param( "wy_find" )String  wy_find);

    //@Select("SELECT\n" +
    //        "\tname,build_area \n" +
    //        "\tFROM\n" +
    //        "\tzf_wy_asset_info_v2\n" +
    //        "\twhere name like concat('%', #{name},'%')\n" +
    //        "\tand find_in_set(belong_type, #{belong_type})\n" +
    //        "\tand street_name like concat('%', #{streetName} ,'%') \n" +
    //        "\tand wy_five like concat('%', #{wy_five},'%')")
    List<Map> selectWyInfoTotalBuildArea(@Param("dto") CywyDTO dto);

    @Select("SELECT\n" +
            "\tid,\n" +
            "\tNAME as name,\n" +
            "\tbelong_type,\n" +
            "\tstreet_name,\n" +
            "\tbuild_area,\n" +
            "\twy_five \n" +
            "FROM\n" +
            "\tzf_wy_asset_info_v2\n" +
            "\twhere name like concat('%', #{name},'%')\n" +
            "\tand belong_type like  concat('%', #{belong_type},'%')\n" +
            "\tand wy_five like concat('%', #{wy_five},'%') AND ruzhang != \"是\" limit #{p},#{size}")
    List<Map> selectNotEntryWyInfo(@Param( "name" )String  name,@Param( "belong_type" )String  belong_type,@Param( "wy_five" )String  wy_five,@Param( "p" )int  param2,@Param( "size" )int  param1);

    @Select("SELECT\n" +
            "\tcount(id) as number\n" +
            "\tFROM\n" +
            "\tzf_wy_asset_info_v2\n" +
            "\twhere name like concat('%', #{name},'%')\n" +
            "\tand belong_type like  concat('%', #{belong_type},'%')\n" +
            "\tand wy_five like concat('%', #{wy_five},'%') AND ruzhang != \"是\"")
    int selectNotEntryWyInfoTotal(@Param( "name" )String  name,@Param( "belong_type" )String  belong_type,@Param( "wy_five" )String  wy_five);

    @Select("SELECT\n" +
            "\tsum(build_area) as build_area\n" +
            "\tFROM\n" +
            "\tzf_wy_asset_info_v2\n" +
            "\twhere name like concat('%', #{name},'%')\n" +
            "\tand belong_type like  concat('%', #{belong_type},'%')\n" +
            "\tand wy_five like concat('%', #{wy_five},'%') AND ruzhang != \"是\"")
    Double selectNotEntryWyInfoTotalBuildArea(@Param( "name" )String  name,@Param( "belong_type" )String  belong_type,@Param( "wy_five" )String  wy_five);

    @Select("SELECT\n" +
            "\tid,\n" +
            "\tNAME as name,\n" +
            "\tbelong_type,\n" +
            "\tstreet_name,\n" +
            "\tbuild_area,\n" +
            "\twy_five \n" +
            "FROM\n" +
            "\tzf_wy_asset_info_v2\n" +
            "\twhere name like concat('%', #{name},'%')\n" +
            "\tand belong_type like  concat('%', #{belong_type},'%')\n" +
            "\tand wy_five like concat('%', #{wy_five},'%') AND cert_has != \"是\" limit #{p},#{size}")
    List<Map> selectNoCertHasWyInfo(@Param( "name" )String  name,@Param( "belong_type" )String  belong_type,@Param( "wy_five" )String  wy_five,@Param( "p" )int  param2,@Param( "size" )int  param1);

    @Select("SELECT\n" +
            "\tcount(id) as number\n" +
            "\tFROM\n" +
            "\tzf_wy_asset_info_v2\n" +
            "\twhere name like concat('%', #{name},'%')\n" +
            "\tand belong_type like  concat('%', #{belong_type},'%')\n" +
            "\tand wy_five like concat('%', #{wy_five},'%') AND cert_has != \"是\"")
    int selectNoCertHasWyInfoTotal(@Param( "name" )String  name,@Param( "belong_type" )String  belong_type,@Param( "wy_five" )String  wy_five);

    @Select("SELECT\n" +
            "\tsum(build_area) as build_area\n" +
            "\tFROM\n" +
            "\tzf_wy_asset_info_v2\n" +
            "\twhere name like concat('%', #{name},'%')\n" +
            "\tand belong_type like  concat('%', #{belong_type},'%')\n" +
            "\tand wy_five like concat('%', #{wy_five},'%') AND cert_has != \"是\"")
    Double selectNoCertHasWyInfoTotalBuildArea(@Param( "name" )String  name,@Param( "belong_type" )String  belong_type,@Param( "wy_five" )String  wy_five);

    //楼栋入驻
    @Select("SELECT\n" +
            "\t* \n" +
            "FROM\n" +
            "\t(\n" +
            "\tSELECT\n" +
            "\t\ta.use_unit,\n" +
            "\t\t GROUP_CONCAT(b.NAME) as wyname,\n" +
            "\t\tb.tota_area,\n" +
            "\t\ta.credit_id as code,\n" +
            "\t\tc.z \n" +
            "\tFROM\n" +
            "\t\tzf_wy_use_rel a\n" +
            "\t\tLEFT JOIN zf_wy_relation_tree b ON a.rel_id = b.rel_id\n" +
            "\t\tLEFT JOIN zf_cx_basic_info c ON c.f = a.use_unit \n" +
            "\tWHERE\n" +
            "\t\ta.del_flag = 0 \n" +
            "\t\tAND a.is_stop = 0 \n" +
            "\t\tAND b.NAME LIKE concat('%', #{name},'%') \n" +
            "\t\tGROUP BY a.use_unit\n" +
            "\t) a \n" +
            "limit #{p},#{size}")
    List<Map> selectWyEnterCompany(@Param( "name" )String  name,@Param( "p" )int  param2,@Param( "size" )int  param1);

    //楼栋入驻数
    @Select("SELECT\n" +
            "\tcount(*) as number \n" +
            "FROM\n" +
            "\t(\n" +
            "\tSELECT\n" +
            "\t\ta.use_unit,\n" +
            "\t\t GROUP_CONCAT(b.NAME) as wyname,\n" +
            "\t\tb.tota_area,\n" +
            "\t\ta.credit_id as code,\n" +
            "\t\tc.z \n" +
            "\tFROM\n" +
            "\t\tzf_wy_use_rel a\n" +
            "\t\tLEFT JOIN zf_wy_relation_tree b ON a.rel_id = b.rel_id\n" +
            "\t\tLEFT JOIN zf_cx_basic_info c ON c.f = a.use_unit \n" +
            "\tWHERE\n" +
            "\t\ta.del_flag = 0 \n" +
            "\t\tAND a.is_stop = 0 \n" +
            "\t\tAND b.NAME LIKE concat('%', #{name},'%') \n" +
            "\t\tGROUP BY a.use_unit\n" +
            "\t) a \n")
    int selectWyEnterCompanyTotal(@Param( "name" )String  name);

    //楼栋空置
    @Select("select * from zf_cx_empty_wy_info where name like concat('%', #{name},'%') limit #{p},#{size} ")
    List<Map> selectWyEmptyByLD(@Param( "name" )String  name, @Param( "p" )int  param2, @Param( "size" )int  param1);

    //楼栋空置数
    @Select("select count(*) from zf_cx_empty_wy_info where name like concat('%', #{name},'%') ")
    int selectWyEmptyByLDTotal(@Param( "name" )String  name);

    //查询空置面积
    @Select( "select sum(buildarea) as emptyarea from zf_cx_empty_wy_info where topname = #{topwyname}" )
    Double selectTotalEmptyByTopWyName(@Param( "topwyname" )String  name);

    @Select("select count(id) from zf_wy_asset_info_v2 where ruzhang != \"是\" and belong_type = '政府物业'")
    int selectWyEntryTotal();

    @Select("select count(*) as number from zf_wy_asset_info_v2 where emptystate =\"是\" and wy_five !=\"产业用房\" and FIND_IN_SET(belong_type,#{type})")
    int selectNotCyEmptyWyInWyAssetInfoV2Total(@Param( "type" )String type);

    @Select("select id,name,belong_type,street_name,build_area,man_unit as manunit from zf_wy_asset_info_v2 where emptystate =\"是\" and wy_five !=\"产业用房\" and FIND_IN_SET(belong_type,#{type}) limit #{p},#{size} ")
    List<Map> selectNotCyEmptyWyInWyAssetInfoV2(@Param( "type" )String type, @Param( "p" )int  param2, @Param( "size" )int  param1);

    @Select("select man_unit from zf_wy_asset_info_v2 where #{wyname} like CONCAT(\"%\",name,\"%\") limit 0,1\n")
    String selectManUnitByWyName(@Param( "wyname" )String wyname);

    @Select("select GROUP_CONCAT(c,d) as wyname from zf_cx_lease_info where b = \"深信服科技股份有限公司\" GROUP BY d\n")
    String selectWyNameByCompanyName(@Param( "name" )String name);

    List<Map> selectWyInfoByCompanyName(@Param( "companyName" )String companyName,@Param( "p" )Integer p , @Param( "size" )Integer size);

    List<Map> selectWyInfoByManUnit(@Param("dto") CywyDTO dto, @Param( "p" )Integer p , @Param( "size" )Integer size);

    Double selectWyInfoByManUnitTotalBuildArea(@Param("dto") CywyDTO dto);

    List<ZPDetailsVo> selectNewCompanyUseWyInfoByCompanyName(@Param("dto") CywyDTO dto);

    //根据公司名称获取退租原因
    List<Map> selectRefundReasonList(@Param("dto") CywyDTO dto);

    @Select("select * from new_company_info where code = #{code} limit 0,1")
    Map selectNewCompanyInfo(@Param("code") String code);

    //根据物业显示tab控制
    String selectWyShowTab(@Param("wyname") String wyname);

    //顶级物业扶持绩效
    Map selectTopWyAnalysis(@Param("wyname") String wyname);

    List<Map> selectTopWyAnalysis2(@Param("wyname") String wyname);

    //查询国企信息
    Map selectWyInfoByGuoqi(@Param("dto")CywyDTO dto);

    List<Map> selectTypeNumberByBelongType(@Param("wyFive")String wyFive);

    List<Map> selectWyFiveByBelongType(@Param("belongsType")String belongsType , @Param("manUnit") String manUnit);

    //查询国企物业的建筑面积
    Double selectGuoqiBuildAreaByUseStatus(@Param("dto")CywyDTO dto);

    //分组查询 国企物业使用状态
    List<Map> selectGuoqiBuildAreaGroupByUseStatus(@Param("dto")CywyDTO dto);

    //查询国企物业的入驻企业信息
    List<Map> selectGuoqiWyInfoByUseStatus(@Param("dto")CywyDTO dto);

    //查询国企物业的入驻企业数量
    Integer selectGuoqiWyInfoByUseStatusTotal(@Param("dto")CywyDTO dto);

    @Select("select address from zf_wy_asset_info_v2 where name = #{wyname}")
    String selectGuoqiAddressByWyName(@Param("wyname")String wyname);


    List<Map> enterpriseListByBelongType(@Param("sort") Integer sort,
                                         @Param("name") String name,
                                         @Param("parkName") String parkName,
                                         @Param("p") Integer p,
                                         @Param("size") Integer size,
                                         @Param("param1") Integer param1,
                                         @Param("param2") Integer param2,
                                         @Param("belongType") String belongType,
                                         @Param("companyindustry") String companyindustry,
                                         @Param("manUnit") String manUnit,
                                         @Param("wyFive")String wyFive);

    int enterpriseListByBelongTypeTotal(@Param("sort") Integer sort, @Param("name") String name, @Param("parkName") String parkName, @Param("param1") Integer param1,
                                        @Param("param2") Integer param2,
                                        @Param("belongType") String belongType,
                                        @Param("companyindustry") String companyindustry,
                                        @Param("manUnit") String manUnit,
                                        @Param("wyFive")String wyFive);

    List<Map> enterpriseListByBelongTypeTotalBuildArea(@Param("sort") Integer sort,
                                                       @Param("name") String name,
                                                       @Param("parkName") String parkName,
                                                       @Param("param1") Integer param1,
                                                       @Param("param2") Integer param2,
                                                       @Param("belongType") String belongType,
                                                       @Param("companyindustry") String companyindustry,
                                                       @Param("manUnit") String manUnit,
                                                       @Param("wyFive")String wyFive);

    //查询经济贡献承诺
    Map selectEconomicInfo(@Param("companyname") String name);

    //计算面积
    Double selectTotalBuildArea(@Param("belongType") String belongType,@Param("manUnit") String manUnit,@Param("wyFive") String wyFive );

    //2021纳入统计的公司  产值
    @Select("SELECT count( g) AS cz2021 FROM zf_cx_basic_info WHERE g IS NOT NULL AND g != \"不在库\" AND g != 0 and f like concat(\"%\" , #{wyname} ,\"%\")")
    Integer countCz2021Number(@Param("wyname")String wyname);

    //2021纳入统计的公司  纳税
    @Select("SELECT count( tax2021) AS tax2021 FROM zf_cx_basic_info WHERE tax2021 IS NOT NULL AND tax2021 != \"不在库\" AND tax2021 != 0 and f like concat(\"%\" , #{wyname} ,\"%\")")
    Integer countTax2021Number(@Param("wyname")String wyname);

    //2021纳入统计的公司  扶持资金
    @Select(" SELECT count( fuchi2021) AS fuchi2021 FROM zf_cx_basic_info WHERE fuchi2021 IS NOT NULL AND fuchi2021 != \"不在库\" AND fuchi2021 != 0 and f like concat(\"%\" , #{wyname} ,\"%\")\n")
    Integer countFuchi2021Number(@Param("wyname")String wyname);

    //2021纳入统计的公司  财力贡献
    @Select("SELECT count( contri2021) AS contri2021 FROM zf_cx_basic_info WHERE contri2021 IS NOT NULL AND contri2021 != \"不在库\" AND contri2021 != 0 and f like concat(\"%\" , #{wyname} ,\"%\")")
    Integer countContri2021Number(@Param("wyname")String wyname);

    //2021纳入统计的公司  实际租金
    @Select("SELECT count( sjzj) AS sjzj FROM zf_fcxy WHERE YEAR = 2021 AND sjzj IS NOT NULL AND sjzj != \"不在库\" and sjzj != 0 and topname like concat(\"%\" , #{wyname} ,\"%\")\n")
    Integer countSjzj2021Number(@Param("wyname")String wyname);

    //2021纳入统计的公司  节约租金
    @Select("SELECT count( jmzj) AS jyzj FROM zf_fcxy WHERE YEAR = 2021 AND jmzj IS NOT NULL AND jmzj != \"不在库\" and jmzj != 0 and topname like concat(\"%\" , #{wyname} ,\"%\") \n")
    Integer countJyzj2021Number(@Param("wyname")String wyname);

    //2021纳入统计的公司  使用绩效
    @Select("SELECT count( syjx) AS syjx FROM zf_fcxy WHERE YEAR = 2021 AND syjx IS NOT NULL AND syjx != \"不在库\" and syjx != 0 and topname like concat(\"%\" , #{wyname} ,\"%\")")
    Integer countSyjx2021Number(@Param("wyname")String wyname);

    //2021纳入统计的公司  平均面积
    @Select("SELECT count( avgarea) AS avgarea FROM zf_fcxy WHERE YEAR = 2021 AND avgarea IS NOT NULL AND avgarea != \"不在库\" and avgarea != 0 and topname like concat(\"%\" , #{wyname} ,\"%\")\n")
    Integer countAvgarea2021Number(@Param("wyname")String wyname);

    //2021纳入统计的公司  公司人数
    @Select("SELECT count( num) AS num FROM zf_fcxy WHERE YEAR = 2021 AND num IS NOT NULL AND num != \"不在库\" and num != 0 and topname like concat(\"%\" , #{wyname} ,\"%\")\n")
    Integer countNum2021Number(@Param("wyname")String wyname);

    //转租分租情况列表
    @Select("select * from new_company_sublet_lease_situation where company=#{companyName} limit #{p},#{size}")
    List<Map> selectNewCompanySubletLeaseList(@Param( "companyName" )String companyName  , @Param("p") int p , @Param("size")int size);

    //转租分租情况总数
    @Select("select count(*) as number from new_company_sublet_lease_situation where company =#{companyName}")
    Integer countNewCompanySubletLeaseNumber(@Param("companyName")String companyName);

    //是否有转租分租情况
    @Select("select count(*) as number from new_company_sublet_lease_situation where company = #{companyName} and situation != \"无转租分租情况\"")
    Integer hasNewCompanySubletLeaseNumber(@Param("companyName")String companyName);

    //合同信息
    //@Select("select id,topwyname , wyname , buildarea , companyname , unitprice , monthrent , updateunitprice,updatemonthrent,rentlimit , msg , dicount , DATE_FORMAT(checkdate,\"%Y-%m-%d\") as checkdate ,DATE_FORMAT(rentstartdate,\"%Y-%m-%d\") as rentstartdate , DATE_FORMAT(rentenddate,\"%Y-%m-%d\") as rentenddate from new_company_use_wy_info where companyname = #{companyName}")
    @Select("select * from new_company_contract where `承租方` = #{companyName}")
    List<Map> selectCompanyContractList(@Param( "companyName" )String companyName);

    //贡献承诺
    @Select("select * from new_company_ns_cz_situation where company = #{companyName} order by year asc")
    List<Map> selectCompanyCzNsList(@Param( "companyName" )String companyName);

    //获奖信息个数
    @Select("select count(*) as number from new_company_reward where company =#{companyName}")
    Integer countNewCompanyRewardNumber(@Param("companyName")String companyName);

    //获奖信息 列表
    @Select("select * from new_company_reward where company =#{companyName} limit #{p},#{size}")
    List<Map> newCompanyRewardList(@Param("companyName")String companyName , @Param("p") int p , @Param("size")int size);


    //证书个数
    @Select("select count(*) as number from new_company_certificate where company =#{companyName}")
    Integer countNewCompanyCertificateNumber(@Param("companyName")String companyName);

    //证书 列表
    @Select("select * from new_company_certificate where company =#{companyName} limit #{p},#{size}")
    List<Map> newCompanyCertificateList(@Param("companyName")String companyName , @Param("p") int p , @Param("size")int size);


    //专利个数
    @Select("select count(*) as number from new_company_patent where company =#{companyName}")
    Integer countNewCompanyPatentNumber(@Param("companyName")String companyName);

    //专利 列表
    @Select("select * from new_company_patent where company =#{companyName} limit #{p},#{size}")
    List<Map> newCompanyPatentList(@Param("companyName")String companyName , @Param("p") int p , @Param("size")int size);

    //软件著作权个数
    @Select("select count(*) as number from new_company_software_copyright where company =#{companyName}")
    Integer countCompanySoftwareCopyrightNumber(@Param("companyName")String companyName);

    //软件著作权 列表
    @Select("select * from new_company_software_copyright where company =#{companyName} limit #{p},#{size}")
    List<Map> newCompanySoftwareCopyrightList(@Param("companyName")String companyName , @Param("p") int p , @Param("size")int size);

    //新 公司详情
    @Select("select * from new_company_detail_info where name =#{companyName}")
    Map selectNewCompanyDetailInfo(@Param("companyName") String companyName);
}

