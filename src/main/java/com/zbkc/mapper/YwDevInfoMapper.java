package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.YwDevInfoPO;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.stereotype.Repository;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/9/27
 */
@Repository
public interface YwDevInfoMapper extends BaseMapper<YwDevInfoPO> {

    /**
     * 根据代办业务表id 查询开发商基本信息表
     * @param id
     * @return
     */
    YwDevInfoPO selectOneByBusinessId(@Param("businessId") long id);
}
