package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.LabelManage;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LabelManageMapper  extends BaseMapper<LabelManage> {

    /**
     * @param labelManage 实体类
     * @return int
     */
    int addLabelManage(LabelManage labelManage);

    void deleteByLabelId(@Param( "labelId" )Long labelId);

    /**
     * 批量插入 标签
     * @param insertLabelManageList list
     * @return 影响行数
     */
    int insertBatch(@Param("list") List<LabelManage> insertLabelManageList);
}
