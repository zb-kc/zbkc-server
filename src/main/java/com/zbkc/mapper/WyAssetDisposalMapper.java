package com.zbkc.mapper;

import com.zbkc.model.dto.WyAssetDisposalHomeDTO;
import com.zbkc.model.po.WyAssetDisposal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.dto.homeList.WyAssetDisposalHomeList;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 资产处置信息表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-09-07
 */
@Repository
public interface WyAssetDisposalMapper extends BaseMapper<WyAssetDisposal> {

    Integer addDisposal(WyAssetDisposal wyAssetDisposal);

    List<WyAssetDisposalHomeList> queryWyAssetDisposalHomeList(WyAssetDisposalHomeDTO dto);

    List<WyAssetDisposalHomeList> queryWyAssetDisposalHomeListTotal(WyAssetDisposalHomeDTO dto);

    @Select("select * from wy_asset_disposal where id=#{id}")
    WyAssetDisposal seeRow(String id);

    Integer updDisposal(WyAssetDisposal wyAssetDisposal);


    @Update("update wy_asset_disposal set status =2 where id=#{id}")
    Integer delById(String id);


}
