package com.zbkc.mapper;

import com.zbkc.model.po.SysAttachment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 系统文件_自定义附件字段 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-11-10
 */
@Repository
public interface SysAttachmentMapper extends BaseMapper<SysAttachment> {

    Integer addSysAttachMent(SysAttachment sysAttachment);

    @Select("SELECT name from sys_attachment where sys_file_path_id=#{id}")
    String getNameById(@Param("id") Long id);

    /**
     * 批量插入
     * @param insertSysAttachmentList list
     * @return 影响行数
     */
    int insertBatch(@Param("list") List<SysAttachment> insertSysAttachmentList);
}
