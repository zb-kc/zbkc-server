package com.zbkc.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.SysRoleDataPermission;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * 系统设置——角色管理_数据权限关联表Mapper
 * @author ZB3436.xiongshibao
 */
@Repository
public interface SysRoleDataPermissionMapper extends BaseMapper<SysRoleDataPermission> {

    /**
     * 根据用户id 查询对应的物业权限信息
     * @param userId 用户id
     * @param isUseReg 是否是使用登记
     * @return list<物业id>
     */
    List<Long> selectDataPermissionByUserId(String userId, Boolean isUseReg);

    /**
     * 批量插入
     * @param list list
     * @return 影响条数
     */
    Integer insertBatch(@Param("list") List<SysRoleDataPermission> list);

    Integer insertSelf(SysRoleDataPermission sysRoleDataPermission);

    Integer deleteByWyBasicInfoIdAfter(Long wyId);

    /**
     * 批量删除物业权限
     * @param roleId 权限id
     * @param oldDiff 需要删除的物业结合
     * @return 影响行数
     */
    Integer deleteBatchByWyIds(@Param("roleId") Long roleId,@Param("list") Set<Long> oldDiff);
}