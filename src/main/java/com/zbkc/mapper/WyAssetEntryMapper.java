package com.zbkc.mapper;

import com.zbkc.model.dto.WyAssetEntryHomeDTO;
import com.zbkc.model.po.WyAssetEntry;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.dto.homeList.WyAssetEntryHomeList;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p>
 * 资产入账信息表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-09-03
 */
@Repository
public interface WyAssetEntryMapper extends BaseMapper<WyAssetEntry> {

    /**
     * @param wyAssetEntry  主表对象
     * @return int
     */
    int addEntry( WyAssetEntry wyAssetEntry);

    List<WyAssetEntryHomeList> queryWyAssetEntryHomeList(WyAssetEntryHomeDTO dto);

    List<WyAssetEntryHomeList> queryWyAssetEntryHomeListTotal(WyAssetEntryHomeDTO dto);

    @Select("select * from wy_asset_entry where id=#{id}")
    WyAssetEntry selectByID(String id);

    int updEntry(WyAssetEntry wyAssetEntry);

    @Update("update wy_asset_entry set status =2 where id=#{id}")
    Integer delById(String id);

}
