package com.zbkc.mapper;


import com.zbkc.model.dto.YyCommissioneOperationHomeDTO;
import com.zbkc.model.po.YyCommissioneOperation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.dto.homeList.YyCommissioneOperationHomeList;
import com.zbkc.model.vo.YyCommissioneOperationVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p>
 * 运营管理——委托运营 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-09-10
 */
@Repository
public interface YyCommissioneOperationMapper extends BaseMapper<YyCommissioneOperation> {

    int addCommissioneOperation( YyCommissioneOperation yyCommissioneOperation);

    List<YyCommissioneOperationHomeList> queryCommissioneOperationHomeList(YyCommissioneOperationHomeDTO dto);

    List<YyCommissioneOperationHomeList> queryCommissioneOperationHomeListTotal(YyCommissioneOperationHomeDTO dto);

    YyCommissioneOperationVo seeRow(Long id);


    int updCommissioneOperation(YyCommissioneOperation yyCommissioneOperation);
}
