package com.zbkc.mapper;

import com.zbkc.model.po.SysUser;
import com.zbkc.model.po.SysUserOrg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.vo.SysUserVO;
import io.lettuce.core.dynamic.annotation.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 用户组织关联表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-08-11
 */
@Repository
public interface SysUserOrgMapper extends BaseMapper<SysUserOrg> {
    /**
     * @param userId 用户id
     * @return List<String>
     */
    @Select("select org_id from sys_user_org where user_id =#{userId}")
    List<Long> findSelfOrgIds(@Param("userId") Long userId);

    /**
     * @param orgId 组织id
     * @return  List<SysUserVO>
     */
    @Select("select b.id,b.user_name,b.nick_name from sys_user_org a,sys_user b where a.user_id=b.id and org_id =#{orgId} ORDER BY user_id ASC ")
    List<SysUserVO> findSelfUser(@Param("orgId") Long orgId);

    @Select("select role_id from sys_user_role where user_id in(select user_id from sys_user_org where org_id=#{orgId})")
    List<Long> findRolesByOrg(@Param("orgId") Long orgId);

    @Select("select org_id from sys_user_org where user_id=#{userId}")
    List<Long> findOrgIdsByUser(@Param("userId") Long userId);
    }
