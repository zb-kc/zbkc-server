package com.zbkc.mapper;

import cn.hutool.core.util.StrUtil;
import com.zbkc.model.dto.*;
import com.zbkc.model.po.WySysMsg;
import com.zbkc.model.po.YyBillManagement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.vo.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 运营管理——账单管理 Mapper 接口
 * </p>
 *
 * @author caobiyang
 * @since 2021-09-14
 */
@Repository
public interface YyBillManagementMapper extends BaseMapper<YyBillManagement> {

    Integer saveBill(YyBillManagementDTO dto);

    AllWordVo   seeRow(@Param("id") String id,@Param("wyId") Long wyId);

    Integer wordSave(CollecMoneyDTO dto);

    Integer moneySave(PendingRecept pendingRecept);

    CollecVo collecInit(CollecDTO dto);

    List<PendingRecept>  beReceived(@Param("contractId") Long contractId,@Param("wyId") Long wyId);

    List<String> getAllSmsName();
    
    List<YyBillManagementHomeList> homeList(BillManagementDTO dto);

    List<YyBillManagementHomeList> homeListTotal(BillManagementDTO dto);

    List<CallRecordVO> selectAllRecord(SmsReCordDTO dto);

    List<PendingRecept> selectAllPendingRecept();

    List<FeeRecord> queryAllFeeRecord(@Param("yyContractId") Long yyContractId,@Param("wyId") Long wyId);

    List<String>  selectWyNameByType(@Param("wyType") String wyType);

    List<YyBillManagement> getBillCount(@Param("bYear") Integer bYear  ,@Param("bMonth") Integer bMonth);

    CallRecordVO  sendSmsIndex(@Param("contractId") Long contractId);

    @Select("SELECT word from wy_sys_msg WHERE name = #{name}")
    String getWordByName(@Param("name") String name);

    List<ManyReceivedVO> waitReceived(@Param("contractId") Long contractId);

    Integer saveManyCollection(SaveCollectionDTO dto);

}


