package com.zbkc.mapper;

import com.zbkc.model.dto.WySysMsgHomeDTO;
import com.zbkc.model.po.WySysMsg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.WySysMsgHomeList;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 短信模板管理表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-09-11
 */
@Repository
public interface WySysMsgMapper extends BaseMapper<WySysMsg> {

    int addMsg(WySysMsg wySysMsg);

    List<WySysMsgHomeList> queryWySysMsgHomeList(WySysMsgHomeDTO dto);

    List<WySysMsgHomeList> queryWySysMsgHomeListTotal(WySysMsgHomeDTO dto);

    @Select("select id,name,type,status,word,remark,create_time,update_time,user_id from wy_sys_msg where id=#{id}")
    WySysMsg seeRow(String id);

    int updMsg(WySysMsg WySysMsg);

    List<WySysMsg> smsCall();

    @Select( "SELECT DISTINCT name from wy_sys_msg where status=1")
    List<String> getAllRemindMsgName();

    @Select( "SELECT * from wy_sys_msg ")
    List<WySysMsg> getAllMsgName();

}
