package com.zbkc.mapper;

import com.zbkc.model.po.SysParamConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 系统参数配置表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-08-05
 */
@Repository
public interface SysParamConfigMapper extends BaseMapper<SysParamConfig> {

    /**
     * @param name 名称
     * @return List<SysParamConfig>
     */
    List<SysParamConfig> selectByInput(@Param("name") String name);

    /**
     * @param id 主键id
     * @return Integer
     */
    @Update("UPDATE sys_param_config SET status = 1 WHERE id = #{id}")
    Integer statusOpen(@Param("id") Long id);


    /**
     * @param name 名称
     * @return List<SysParamConfig>
     */
    @Select("select * from sys_param_config where name=#{name}")
    List<SysParamConfig> selectByName(@Param("name") String name);

    /**
     * @param id 主键id
     * @return Integer
     */
    @Update("UPDATE sys_param_config SET status = 2 WHERE id = #{id}")
    Integer statusClose(@Param("id") Long id);

}
