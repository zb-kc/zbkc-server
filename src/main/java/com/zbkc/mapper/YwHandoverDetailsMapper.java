package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.YwAgencyBusinessPO;
import com.zbkc.model.po.YwHandoverDetailsPO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/12
 */
@Repository
public interface YwHandoverDetailsMapper extends BaseMapper<YwHandoverDetailsPO> {

    int deleteByPrimaryKey(Long id);

    int insertSelective(YwHandoverDetailsPO record);

    YwHandoverDetailsPO selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(YwHandoverDetailsPO record);

    int updateByPrimaryKey(YwHandoverDetailsPO record);

    int insertBatch(@Param("list") List<YwHandoverDetailsPO> list);
}
