package com.zbkc.mapper;


import com.zbkc.model.po.SysImg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.SysImg1;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 系统照片路径表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-09-03
 */
@Repository
public interface SysImgMapper extends BaseMapper<SysImg> {
    /**
     * @param sysImg 实体类
     * @return int
     */
    int addSysImg(SysImg sysImg);

    int updSysImg(SysImg sysImg);

    List<SysImg1> getBydbFileId( Long primaryTableId);

}
