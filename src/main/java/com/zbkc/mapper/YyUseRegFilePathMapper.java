package com.zbkc.mapper;

import com.zbkc.model.po.YyUseRegFilePath;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.YyUseRegFilePath1;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 运营管理——使用登记/委托运营——自定义附件表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-09-09
 */
@Repository
public interface YyUseRegFilePathMapper extends BaseMapper<YyUseRegFilePath> {

    int addYyUseRegFilePath(YyUseRegFilePath yyUseRegFilePath);

    @Select("select * from yy_use_reg_file_path where yy_id=#{yyId}")
    List<YyUseRegFilePath1> getByYyId(Long yyId);



}
