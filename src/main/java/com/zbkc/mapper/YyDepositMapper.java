package com.zbkc.mapper;

import com.zbkc.model.dto.PageDTO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.vo.YyDepositHome;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-09-16
 */
@Repository
public interface YyDepositMapper extends BaseMapper {

}
