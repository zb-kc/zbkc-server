package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.dto.SocicalWyDTO;
import com.zbkc.model.dto.WyBasicInfoHomeDTO;
import com.zbkc.model.dto.WyBasicInfoInDataPermissionDTO;
import com.zbkc.model.dto.homeList.WyBasicInfoHomeList;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.*;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * <p>
 * 物业基本(主体)信息表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-08-09
 */
@Repository
public interface WyBasicInfoMapper extends BaseMapper<WyBasicInfo> {
    /**
     * @param p 页码
     * @param size 行数
     * @param name 名称
     * @return List<WyBasicInfo>
     */
    @Select("select * from wy_basic_info where name like concat('%', #{name},'%') limit #{p},#{size}")
    List<WyBasicInfo> pageByName(@Param("p")Integer p, @Param("size")Integer size, @Param("name") String name);


    @Select("select count(*) from wy_basic_info where name=#{name} and pid=#{pid} and is_del=1")
    Integer selectTotalByNameAndPid(String name,Long pid);

    @Select("select name from wy_basic_info where id=#{id} and pid=#{pid} and is_del=1")
    String selectNameByIdAndPid(Long id,Long pid);

    /**
     * @param name 名称
     * @return List<WyBasicInfo>
     */
    @Select("select * from wy_basic_info where name = #{name} and (type=1 or type=5 or type=2 or type=6) and is_del=1")
    List<WyBasicInfo> selectByName(@Param("name") String name);

    /**
     * @param info 添加实体类对象
     * @return int
     */
    int addBasic(WyBasicInfo info);

    /**
     * @param dto 查询条件
     * @return List<WyBasicInfoHomeList>
     */
    List<WyBasicInfoHomeList> queryWyBasicInfoHomeList(WyBasicInfoHomeDTO dto);

    /**
     * @param dto 查询条件
     * @return List<WyBasicInfoHomeList>
     */
    List<WyBasicInfoHomeList> queryWyBasicInfoHomeListTotal(WyBasicInfoHomeDTO dto);

    /**
     * @param id 根据id查询
     * @return SysDataType
     */
    List<SysDataType> getSysDataTypeByID(@Param("id")Long id);

    /** @param id 根据id查询
     * @return SysDataType
     */
    List<SysDataType> getSysDataStatusByID(@Param("id")Long id);


    /**
     * @param id 根据id查询
     * @return SysDataScale
     */
    List<SysDataType> getSysDataScaleByID(@Param("id")Long id);

    /**
     * @param id 根据id查询
     * @return SysDataTag
     */
    List<SysDataType> getSysDataTagByID(@Param("id")Long id);

    /**
     * @param id 根据label_id查询
     * @return SysDataType
     */
    List<SysDataType> getSysDataTypesByLabelId(@Param("id")Long id);

    /**
     * @param id 根据label_id查询
     * @return SysDataType
     */
    SysDataType getSysDataTypeByLabelId(@Param("id")Long id);

    /**
     *
     * @param id 根据db_file_id查询
     * @return   List<SysFilePath1>
     */
    List<SysFilePath1> getSysFileByID(@Param("id") long id);


    /**
     * @param id 根据id查询详细信息
     * @return List<WyBasicInfoDetailsList>
     */
    WyBasicInfo getDetailsList(@Param("id")Long id);

    String  getNameById(Long id);


    /**
     * @param id 根据id查询
     * @return List<FloorList>
     */
    List<FloorList> getFloorListByPid(@Param("id")Long id);

    /**
     * @param pid 根据pid查询 物业结构
     * @return List<PropertyStructureVo>
     */
    List<PropertyStructureVo> getPropertyStructureById(@Param("pid")Long pid);

    List<WyFacilitiesInfo> selectWyFacilitiesByWyId(Long id);

    List<String> getSelfPropertyType(@Param("id")Long id);
    /**
     * @param id 根据wy id查询 物业结构
     * @param userId
     * @return List<PropertyStructureVo>
     */
    List<PropertyStructureVo> getPropertyStructureByUserId(@Param("id") Long id, @Param("userId") Long userId);

    /**
     * @param id 主键id
     * @return Integer
     */
    Integer logicDel(@Param("id") Long id);

    /**
     * @return  List<UnitNameVo>
     * @param userId
     */
    List<UnitNameVo> queryAllName(Long userId);

    Integer updateBasic(WyBasicInfo info);

    List<RentalSocialWyVo> getRentalSocialWyList(SocicalWyDTO dto);

    List<RentalSocialWyVo> getRentalSocialWyListTotal(SocicalWyDTO dto);

    RentalSosialVo seeRow(Long id);

    @Select("select * from wy_basic_info where name = #{name} and is_del=1")
    WyBasicInfo queryByName(@Param("name") String name);

    Integer DelShWy(@Param("id") Long  id);

    /**
     * 资产管理-选择物业资产
     * 查询所有物业信息
     * @return list
     */
    List<WyBaseInfoVO> selectAllWyInfo(String contractId);

    /**
     * 根据合同id 查询物业信息
     * @param ywContractId 合同id
     * @return List<WyBasicInfo>
     */
    List<WyBasicInfo> selectWyInfoByContractId(@Param("ywContractId")long ywContractId);

    /**
     * 根据物业ids查询物业名称
     * @param  wyIds 物业id
     * @return String
     */
    String selectWyNameByIds(@Param("wyIds") String wyIds);


    List<WyBasicInfo> selectByWyId(@Param("wyIds") Long[] wyIds);


    /**
     * 根据物业编码查询物业信息
     * @param houseCodes 物业编码
     * @return  list
     */
    List<WyBasicInfo> selectListByIds(@Param("ids") List<String> houseCodes);

    /**
     * 数据权限用-查询物业信息
     * @param wyBasicInfoInDataPermissionDTO dto
     * @return list
     */
    List<WyBasicInfoInDataPermissionPO> selectListInDataPermission(WyBasicInfoInDataPermissionDTO wyBasicInfoInDataPermissionDTO);

    /**
     * 根据物业idlist 查询物业信息
     * @param wyBasicIdList 物业id list
     * @return list
     */
    List<WyBaseInfoVO> selectByWyIdList(@Param("wyBasicIdList") List<Long> wyBasicIdList);

    /**
     * 根据用户id选择
     * @param roleId 角色id
     * @return list
     */
    List<WyBasicInfoInDataPermissionPO> selectListInDataPermissionByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据businessId 查询物业信息
     * @param businessId 合同id
     * @return List<WyBasicInfo>
     */
    List<WyBasicInfo> selectWyInfoByBusinessId(@Param("businessId")long businessId);

  List<WyBasicInfo> batch(Long[] ids);
   /**
     * 批量更新物业信息
     * @param updateWyBasicInfoList list
     * @return  影响行数
     */
    Integer updateBatch(@Param("list") List<WyBasicInfo> updateWyBasicInfoList);

   @Update(" update wy_basic_info set zl_monthly_rent=#{monthlyRent} where id=#{id}")
    Integer updWord(@Param("id") Long wyId, @Param("monthlyRent") BigDecimal monthlyRent );

    /**
     * 根据物业idlist 查询物业的property_tag
     * @param wyIdList 物业idlist
     * @return List
     */
    List<WyBasicInfo> getSysDataTagByIdList(@Param("wyIdList") List<Long> wyIdList , @Param("queryType") String queryType);


    WyBasicInfoDetailsList getDetail(Long id);

    /**
     * 根据父id 查询所有子节点
     * @param id pid
     * @return 所有子节点 包含自己
     */
    List<Long> selectAllSonPid(@Param("pid") Long id);

    @Select("select count(name) from wy_basic_info where pid=#{pid} and ( name=#{name} or unit_name=#{unitName})and is_del=1")
    Integer selectByNameAndPid(Long pid,String name,String unitName);


    List<SysDataType> selectWyTypes(Long id);

    @Select("SELECT * FROM wy_basic_info where property_code is null")
    List<WyBasicInfo> queryNullProperty();


    List<EmptyList> queryEmptyList(String id,String wyName,String preCompany,Integer p,Integer size);

    List<UsedList> queryUsedList(String id,String wyName,String objName,Integer p,Integer size);

    List<ProRightList> queryProrightList(String id,String wyName,String certificateNo,Integer p,Integer size);

    List<EntryList> queryEntryList(String id,String wyName,String entryType,Integer p,Integer size);

    List<BuildingList> queryBuildingList(String id,String wyName,Integer p,Integer size);

    List<FloorList> queryFloorList(String id,String wyName,Integer p,Integer size);

    List<UnitList> queryUnitList(String id,String wyName, String company,String status,Integer p,Integer size);

    @Update(" update wy_basic_info set property_status_id=#{propertyStatusId},update_time=#{updateTime} where id=#{id}")
    Integer updWyStatus(@Param("propertyStatusId") Long propertyStatusId, @Param("id") Long id, @Param("updateTime")Timestamp updateTime);

    /**
     * 查询所有子集 关联物业使用表
     * 根据集合A 内关联查询 使用登记表 得到物业id集合B
     * @param pid 顶级父节点
     * @return wyId
     */
    List<Long> selectAllSonPidWithYyUseReg(@Param("pid") long pid);

    /**
     * 根据集合A 内关联查询 合同表关联物业基础信息表 左关联合同信息表 where 合同时间未到期 得到物业id集合C
     * @param pid 顶级父节点
     * @return wyId
     */
    List<Long> selectAllSonPidWithYyContractWyRelation(@Param("pid") long pid);

    List<AllEmptyWyVO> getEmptyWyInfo(@Param("userId") Long userId);

}
