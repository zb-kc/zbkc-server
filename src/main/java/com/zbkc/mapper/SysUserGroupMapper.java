package com.zbkc.mapper;


import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.dto.GroupDTO;
import com.zbkc.model.po.SysUserGroup;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import java.io.Serializable;
import java.util.List;


/**
 * <p>
 * 系统 组织管理
 * </p>
 *
 * @author gmding
 * @since 2021-07-28
 */
@TableName("sys_user_group")
public interface SysUserGroupMapper extends BaseMapper<SysUserGroup> {

    /**
     * @param groupDTO
     * @return List<SysUserGroup>
     */
    @Select("select * from sys_user_group where name like concat('%', #{groupName},'%') and level=#{level} order by sort ASC")
    List<SysUserGroup> groupList(GroupDTO groupDTO);

    /**
     * @param parentid
     * @return   List<SysUserGroup>
     */
    @Select("select * from sys_user_group where parentid =#{parentid}  order by sort ASC")
    List<SysUserGroup> groupByPidList(@Param( "parentid" )long parentid);


    /**
     * @param id
     * @return Integer
     */
    @Update("UPDATE sys_user_group SET is_del = 1 WHERE id = #{id}")
    Integer delByInfo(@Param("id") Serializable id);

    @Update("UPDATE sys_user_group SET status = 1 WHERE id = #{id}")
    Integer statusOpen(Long id);

    @Update("UPDATE sys_user_group SET status = 2 WHERE id = #{id}")
    Integer statusClose(Long id);
}
