package com.zbkc.mapper;

import com.zbkc.model.po.SysMenu;
import com.zbkc.model.po.SysOrg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.vo.SysUserVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 组织管理 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-08-11
 */
@Repository
public interface SysOrgMapper extends BaseMapper<SysOrg> {

    /**
     * @param parentid 父级id
     * @return   List<SysOrg>
     */
    @Select("select * from sys_org where parentid =#{parentid}  order by sort ASC")
    List<SysOrg> orgsByPidList(@Param( "parentid" )long parentid);

    /**
     * @param id 主键id
     * @return Integer
     */
    @Update("UPDATE sys_org SET status = 1 WHERE id = #{id}")
    Integer statusOpen(@Param("id") Long id);

    /**
     * @param id 主键id
     * @return Integer
     */
    @Update("UPDATE sys_org SET status = 2 WHERE id = #{id}")
    Integer statusClose(@Param("id") Long id);

    /**
     * @return  List<SysUserVO>
     */
    @Select("select id,user_name from sys_user order by cast(create_time as datetime) DESC")
    List<SysUserVO> userList();

    @Select("select * from sys_org where parentid='0'")
    List<SysOrg>  parentOrg();


    @Select("SELECT b.* FROM sys_org a\n" +
            "left join sys_org b on a.id=b.parentid\n" +
            "where a.id=#{id}")
    List<SysOrg>  sonOrg(Long id);

}
