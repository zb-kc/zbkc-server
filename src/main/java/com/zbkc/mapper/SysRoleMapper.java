package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.SysRole;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 系统角色表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-07-29
 */
@Repository
public interface SysRoleMapper extends BaseMapper<SysRole> {

   /**
    * @param name 名称
    * @return List<SysDataValue>
    */
   @Select("select * from sys_role where name=#{name}")
   List<SysRole> selectByName(@Param("name") String name);

   /**
    * @param p 页码
    * @param size  行数
    * @param index 索引
    * @return List<SysRole>
    */
   List<SysRole> pageByIndex(@Param("p")Integer p, @Param("size")Integer size, @Param("index") String index);

   /**
    * @param id 主键id
    * @return Integer
    */
   @Update("UPDATE sys_role SET status = 1 WHERE id = #{id}")
   Integer statusOpen(@Param("id") Long id);

   /**
    * @param id 主键id
    * @return Integer
    */
   @Update("UPDATE sys_role SET status = 2 WHERE id = #{id}")
   Integer statusClose(@Param("id") Long id);

   /**
    * @param index 索引
    * @return  int
    */
   int selectByNameTotal(@Param("index") String index);
}
