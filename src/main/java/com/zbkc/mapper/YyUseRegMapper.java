package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.dto.YyUseRegHomeDTO;
import com.zbkc.model.dto.homeList.YyUseRegHomeList;
import com.zbkc.model.po.YyUseReg;
import com.zbkc.model.vo.YyUseRegVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 运营管理——使用登记 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-09-09
 */
@Repository
public interface YyUseRegMapper extends BaseMapper<YyUseReg> {
    int addYyUseReg( YyUseReg yyUseReg);

    List<YyUseRegHomeList> queryYyUseRegHomeList(YyUseRegHomeDTO dto);

    List<YyUseRegHomeList> queryYyUseRegHomeListTotal(YyUseRegHomeDTO dto);


    YyUseRegVo seeRow(String id);

    int updUseReg(YyUseReg yyUseReg);

    int insertBatch(@Param("list") List<YyUseReg> list);
    @Update(" update yy_use_reg set status=#{status} where id=#{id}")
    int updStatus(@Param("id") Long id,@Param("status") Integer status);
}
