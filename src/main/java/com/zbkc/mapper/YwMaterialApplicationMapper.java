package com.zbkc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.dto.YwMaterialApplicationDTO;
import com.zbkc.model.po.YwAgencyBusinessPO;
import com.zbkc.model.po.YwMaterialApplicationPO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/14
 */
@Repository
public interface YwMaterialApplicationMapper extends BaseMapper<YwMaterialApplicationPO> {

    /**
     * 分页查询
     * @param ywMaterialApplicationDTO 业务办理——物业资产申报-申请材料DTO
     * @return list
     */
    List<YwMaterialApplicationPO> selectYwMaterialApplicationByPage(YwMaterialApplicationDTO ywMaterialApplicationDTO);

    /**
     * 分页查询的总数
     * @param ywMaterialApplicationDTO 业务办理——物业资产申报-申请材料DTO
     * @return 总数
     */
    int selectYwMaterialApplicationByPageCount(YwMaterialApplicationDTO ywMaterialApplicationDTO);
}