package com.zbkc.mapper;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zbkc.model.po.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 系统菜单表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-07-30
 */
@TableName("sys_menu")
@Repository
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * @param parentid
     * @return   List<SysMenu>
     */
    @Select("select * from sys_menu where pid =#{parentid}  order by sort ASC")
    List<SysMenu> menuByPidList(@Param( "parentid" )long parentid);



    /**
     * @param id
     * @return Integer
     */
    @Update("UPDATE sys_menu SET is_del = 1 WHERE id = #{id}")
    Integer delByInfo(@Param("id") long id);

    /**
     * @param name 名称
     * @return List<SysMenu>
     */
    @Select("select * from sys_menu where name=#{name}")
    List<SysMenu> selectByName(@Param("name") String name);

    /**
     * @param id 主键id
     * @return Integer
     */
    @Update("UPDATE sys_menu SET status = 1 WHERE id = #{id}")
    Integer statusOpen(@Param("id") Long id);

    /**
     * @param id 主键id
     * @return Integer
     */
    @Update("UPDATE sys_menu SET status = 2 WHERE id = #{id}")
    Integer statusClose(@Param("id") Long id);

    /**
     * @return   List<SysMenu>
     */
    @Select("select * from sys_menu where pid =0")
    List<SysMenu> sortEnd();

    /**
     * @return   List<SysMenu>
     */
    @Select("select * from sys_menu where pid =1")
    List<SysMenu> sortSon();


    List<SysMenu> getUserMenu(@Param( "user_id" )Long userId,@Param( "pid" )Long pid);


    /**
     * 根据父级菜单获取子集菜单及按钮
     * @param userId 用户id
     * @param pid 父级菜单
     * @return list
     */
    List<SysMenu> selectMenuByParentId(@Param( "user_id" )Long userId,@Param( "pidList" )List<Long> pid);

    /**
     * 根据pid 查询所有sysMenu
     * @param pid pid
     * @return list
     */
    List<SysMenu> selectAllMenuByPid(@Param("pid") Long pid);
}
