package com.zbkc.mapper;

import com.zbkc.model.dto.SmsReCordDTO;
import com.zbkc.model.po.YjSmsRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.vo.CallRecordVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 预警提醒——合同到期提醒_短信记录表 Mapper 接口
 * </p>
 *
 * @author yangyan
 * @since 2021-09-15
 */
@Repository
public interface YjSmsRecordMapper extends BaseMapper<YjSmsRecord> {


    Integer insertSmsRecord(YjSmsRecord yjSmsRecord);

    List<CallRecordVO> selectAllWarnRecord(SmsReCordDTO dto );

    String selectWord(String  name);

}
