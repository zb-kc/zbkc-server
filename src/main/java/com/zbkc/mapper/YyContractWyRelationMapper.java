package com.zbkc.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.YyContractWyRelationPO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021-10-26
 */
@Repository
public interface YyContractWyRelationMapper extends BaseMapper<YyContractWyRelationPO> {

    /**
     * 根据idlist删除数据
     *
     * @param idList list
     * @return 影响条数
     */
    int deleteByIds(@Param("idList") List<Long> idList);

    /**
     * 批量插入
     *
     * @param insertList list
     * @return 影响条数
     */
    int insertBatch(@Param("list") List<YyContractWyRelationPO> insertList);

    @Delete(" delete from yy_contract_wy_relation  where yy_contract_id=#{contractId}")
    int deleteByContractId(Long contractId);

    @Select("SELECT wy_basic_info_id from yy_contract_wy_relation where yy_contract_id =#{contractId}")
    List<Long> findWyId(Long contractId);

    @Select("SELECT  DISTINCT yw_business_id from yy_contract_wy_relation where yy_contract_id=#{contractId}")
    Long getBusinessId(Long contractId );
}