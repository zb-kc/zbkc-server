package com.zbkc.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkc.model.po.SysUser;
import com.zbkc.model.po.SysUserRole;
import org.springframework.stereotype.Repository;

/**
 * @author yangyan
 * @date 2021/11/12
 */
@Repository
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {


    Long selectRoleByUserId(Long userId);


}