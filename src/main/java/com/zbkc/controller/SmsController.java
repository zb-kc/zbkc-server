package com.zbkc.controller;

import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.model.vo.ResponseVO;
//import com.zbkc.service.AliyunSmsSenderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


/**
 * 短信服务返回结果集
 * @author yangyan
 * @date 2021/7/21
 */
@Api(tags = "短信服务")
@RestController
@CrossOrigin("*")
@RequestMapping("/sms")
public class SmsController {

//    @Autowired
//    private AliyunSmsSenderService aliyunSmsSenderService;


//    @ApiOperation(value = "发送短信")
//    @GetMapping("/send")
//    public ResponseVO sendSms(@RequestParam("phoneNum") String phoneNum) {
//        aliyunSmsSenderService.sendSms(phoneNum);
//        return new ResponseVO(ErrorCodeEnum.SUCCESS,null);
//    }

//    public  ResponseVO getSmsRecord(@RequestParam("")  String){
//
//    }
}
