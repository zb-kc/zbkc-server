package com.zbkc.controller.warning;


import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.*;
import com.zbkc.model.dto.export.ExportYjContractDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.YjSmsRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 预警提醒——合同到期提醒_短信记录表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-09-15
 */
@Api(tags = "预警提醒-合同到期")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/yjSmsRecord")
public class YjSmsRecordController {

    @Autowired
    private YjSmsRecordService yjSmsRecordService;

    @ApiOperation(value = "合同到期提醒列表")
    @PostMapping(value = "/list")
    public ResponseVO allContract(@RequestBody YjContractDTO data) throws BusinessException {
        log.info("合同到期列表:{}",new Gson().toJson(data));

        if(null == data.getUserId()){
            return new ResponseVO(ErrorCodeEnum.USERID_NOT_FOUND, ErrorCodeEnum.USERID_NOT_FOUND.getErrMsg());
        }

        return yjSmsRecordService.allContarct(data);
    }


    @ApiOperation(value = "获取所有提醒模板短信名称")
    @GetMapping(value = "/Msg/AllName")
    public ResponseVO getAllRemindMsgName() throws BusinessException {
        return yjSmsRecordService.getAllRemindMsgName();
    }

    @ApiOperation(value = "根据名称查找内容")
    @GetMapping(value = "/find/{name}")
    public ResponseVO findWordByName(@PathVariable("name") String name) throws BusinessException {
        log.info("请求的名称:{}",new Gson().toJson(name));
        return yjSmsRecordService.selectWord(name);
    }

    @ApiOperation(value = "发送短信")
    @PostMapping(value = "/sendMessage")
    public ResponseVO sendMessage(@RequestBody SmsDTO data) throws BusinessException {
        log.info("发送短信:{}",new Gson().toJson(data));
        return yjSmsRecordService.sendMessage(data);

    }


    @ApiOperation(value = "预警短信记录")
    @PostMapping(value = "/warnRecord")
    public ResponseVO selectAllWallRecord(@RequestBody SmsReCordDTO data) throws BusinessException {
        log.info("短信记录:{}",new Gson().toJson(data));
        return yjSmsRecordService.selectAllRecord(data);
    }

    @ApiOperation(value = "导出合同到期提醒列表")
    @RequestMapping(value = "/list/export", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = "text/html;charset=UTF-8")
    public ResponseVO export(@RequestBody ExportYjContractDTO data, HttpServletResponse response) throws BusinessException {
        log.info("导出合同到期提醒:{}",new Gson().toJson(data));
        return yjSmsRecordService.export(data,response);

    }


}

