package com.zbkc.controller.warning;


import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.*;
import com.zbkc.model.dto.export.ExportDepositDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.YyContractService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-09-16
 */
@Api(tags = "运营管理-押金管理")
@RestController
@CrossOrigin("*")
@Slf4j
@RequestMapping("/yyDeposit")
public class YyDepositController {

    @Autowired
    private YyContractService yyContractService;

    @ApiOperation(value = "首页")
    @PostMapping("/homeList")
    public ResponseVO homeList(@RequestBody DepositDTO dto){
        log.info("首页参数:{}",dto);

        if(null == dto.getUserId()){
            return new ResponseVO(ErrorCodeEnum.USERID_NOT_FOUND, ErrorCodeEnum.USERID_NOT_FOUND.getErrMsg());
        }

        return yyContractService.queryDepositList(dto);
    }

    @ApiOperation(value = "详情")
    @GetMapping("/seeRow")
    public ResponseVO seeRow(@RequestParam String id,@RequestParam Long wyId){
        log.info("首页参数:{}",id);
        return yyContractService.seeDepositRow(id,wyId);
    }

    @ApiOperation(value = "编辑押金")
    @PostMapping("/edit")
    public ResponseVO editDeposit(@RequestBody EditDepositDTO dto){
        log.info("编辑参数:{}",dto);
        return yyContractService.editDeposit(dto);
    }

    @ApiOperation(value = "根据物业类型获取物业名称")
    @GetMapping(value = "/type/name")
    public ResponseVO selectWyNameByType(@RequestParam String wyType) throws BusinessException {
        return yyContractService.selectWyNameByType(wyType);
    }

    @ApiOperation(value = "导出押金列表")
    @RequestMapping(value = "/deposit/export", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = "text/html;charset=UTF-8")
    public ResponseVO export(@RequestBody ExportDepositDTO data, HttpServletResponse response) throws BusinessException {
        log.info("导出押金列表信息:{}", new Gson().toJson(data));
        return yyContractService.exportDeposit(data, response);
    }
}

