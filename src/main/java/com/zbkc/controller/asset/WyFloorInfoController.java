package com.zbkc.controller.asset;


import com.google.gson.Gson;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.PageDTO;
import com.zbkc.model.po.WyFloorInfo;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.WyFloorInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 物业楼层信息表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-08-10
 */
@Api(tags = "资产管理-楼层管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/wyFloorInfo")
public class WyFloorInfoController {
    @Autowired
    private WyFloorInfoService wyFloorInfoService;

    @ApiOperation(value = "分页查询")
    @RequestMapping(value = "/page", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO findPage(@RequestBody PageDTO dto) throws BusinessException {
        log.info("楼层管理:{}",new Gson().toJson(dto));
        return wyFloorInfoService.pageByName(dto.getP(),dto.getSize(),dto.getIndex());
    }

    @ApiOperation(value = "新增主体")
    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addRole(@RequestBody WyFloorInfo data) throws BusinessException {
        log.info("楼层管理:{}",new Gson().toJson(data));
        return wyFloorInfoService.insert(data);
    }

    @ApiOperation(value = "修改主体")
    @RequestMapping(value = "/upd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updRole(@RequestBody WyFloorInfo data) throws BusinessException {
        log.info("楼层管理:{}",new Gson().toJson(data));
        return wyFloorInfoService.updateById(data);
    }

}

