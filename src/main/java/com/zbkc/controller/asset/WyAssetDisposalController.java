package com.zbkc.controller.asset;


import com.google.gson.Gson;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.WyAssetDisposalDTO;
import com.zbkc.model.dto.WyAssetDisposalHomeDTO;
import com.zbkc.model.dto.WyAssetEntryDTO;
import com.zbkc.model.dto.WyAssetEntryHomeDTO;
import com.zbkc.model.dto.export.ExportWyAssetDisposalDTO;
import com.zbkc.model.dto.export.ExportWyAssetEntryDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.WyAssetDisposalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 资产处置信息表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-09-07
 */
@Api(tags = "资产管理-资产处置")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/wyAssetDisposal")
public class    WyAssetDisposalController {
    @Autowired
    private WyAssetDisposalService wyAssetDisposalService;

    @ApiOperation(value = "新增资产处理")
    @PostMapping(value = "/addDisposal")
    public ResponseVO addDisposal(@RequestBody WyAssetDisposalDTO data) throws Exception {
        log.info("新增处理:{}",new Gson().toJson(data));
        return wyAssetDisposalService.addDisposal(data);
    }

    @ApiOperation(value = "首页查询资产处理列表")
    @PostMapping("/list")
    public ResponseVO getList(@RequestBody WyAssetDisposalHomeDTO data) throws BusinessException {
        log.info("查看处理:{}",new Gson().toJson(data));

        return wyAssetDisposalService.getList( data );
    }

    @ApiOperation(value = "查看当前行的数据")
    @GetMapping("/row/{id}")
    public ResponseVO seeRow(@PathVariable("id") String id) throws BusinessException {
        log.info("资产处置:{}",new Gson().toJson(id));
        return wyAssetDisposalService.seeRow(id);
    }

    @ApiOperation(value = "修改处置")
    @RequestMapping(value = "/upd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updDisposal(@RequestBody WyAssetDisposalDTO data) throws Exception {
        log.info("修改处置:{}",new Gson().toJson(data));
        return wyAssetDisposalService.updDisposal(data);
    }

    @ApiOperation(value = "删除处置")
    @GetMapping("/del/{id}")
    public ResponseVO delAssetDispoal(@PathVariable("id") String id) throws BusinessException {
        log.info("删除产权:{}",new Gson().toJson(id));
        return wyAssetDisposalService.delAssetDispoal(id);
    }

    @ApiOperation(value = "导出资产处置列表")
    @RequestMapping(value = "/list/export", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = "text/html;charset=UTF-8")
    public ResponseVO export(@RequestBody ExportWyAssetDisposalDTO data, HttpServletResponse response) throws BusinessException {
        log.info("导出产权登记:{}",new Gson().toJson(data));
        return wyAssetDisposalService.export(data,response);

    }


}

