package com.zbkc.controller.asset;


import com.google.gson.Gson;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.PageDTO;
import com.zbkc.model.po.WyUnitInfo;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.WyUnitInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 * 物业单元信息表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-08-09
 */
@Api(tags = "资产管理-单元管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/wyUnitInfo")
public class WyUnitInfoController {
    @Autowired
    private WyUnitInfoService unitInfoService;

    @ApiOperation(value = "分页查询")
    @RequestMapping(value = "/page", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO findPage(@RequestBody PageDTO dto) throws BusinessException {
        log.info("单元管理:{}",new Gson().toJson(dto));
        return unitInfoService.pageByInput(dto.getP(),dto.getSize(),dto.getIndex());
    }

    @ApiOperation(value = "新增单元信息")
    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addRole(@RequestBody WyUnitInfo data) throws BusinessException {
        log.info("单元管理:{}",new Gson().toJson(data));
        return unitInfoService.insert(data);
    }

    @ApiOperation(value = "修改单元信息")
    @RequestMapping(value = "/upd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updRole(@RequestBody WyUnitInfo data) throws BusinessException {
        log.info("单元管理:{}",new Gson().toJson(data));
        return unitInfoService.updateById(data);
    }

    }




