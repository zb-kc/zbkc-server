package com.zbkc.controller.asset;


import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.WyProRightHomeDTO;
import com.zbkc.model.dto.export.ExporProRightRegDTO;
import com.zbkc.model.dto.export.ExportUseRegDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.dto.WyProRightRegDTO;
import com.zbkc.service.WyProRightRegService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;


/**
 * <p>
 * 产权登记表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-08-06
 */
@Api(tags = "资产管理-产权登记")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/wyProRightReg")
public class WyProRightRegController {
    @Autowired
    private WyProRightRegService wyProRightRegService;

    @ApiOperation(value = "新增产权登记")
    @PostMapping(value = "/addProRight")
    public ResponseVO addRole(@RequestBody WyProRightRegDTO data) throws BusinessException {
        log.info("新增资产:{}",new Gson().toJson(data));
        return wyProRightRegService.addProRight(data);
    }

    @ApiOperation(value = "所有房产名称")
    @PostMapping("/allProRight")
    public ResponseVO  allName(@RequestParam("userId")Long userId) throws BusinessException{

        if(null == userId){
            return new ResponseVO(ErrorCodeEnum.USERID_NOT_FOUND, ErrorCodeEnum.USERID_NOT_FOUND.getErrMsg());
        }

        return wyProRightRegService.selectAllName(userId);
    }

    @ApiOperation(value = "首页查询产权登记列表")
    @PostMapping("/list")
    public ResponseVO getList(@RequestBody WyProRightHomeDTO data) throws BusinessException {
        log.info("查看产权:{}",new Gson().toJson(data));

      return wyProRightRegService.getList( data );

    }

    @ApiOperation(value = "查看当前行的数据")
    @GetMapping("/row/{id}")
    public ResponseVO seeRow(@PathVariable("id") String id) throws BusinessException {
        log.info("修改产权:{}",new Gson().toJson(id));
        return wyProRightRegService.seeRow(id);
    }

    @ApiOperation(value = "修改产权")
    @RequestMapping(value = "/upd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updProright(@RequestBody WyProRightRegDTO data) throws BusinessException {
        log.info("修改产权:{}",new Gson().toJson(data));
        return wyProRightRegService.updProright(data);
    }

    @ApiOperation(value = "删除产权")
    @GetMapping("/del/{id}")
    public ResponseVO updProright(@PathVariable("id") String id) throws BusinessException {
        log.info("删除产权:{}",new Gson().toJson(id));
        return wyProRightRegService.delProright(id);
    }

    @ApiOperation(value = "导出产权登记列表")
    @RequestMapping(value = "/list/export", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = "text/html;charset=UTF-8")
    public ResponseVO export(@RequestBody ExporProRightRegDTO data, HttpServletResponse response) throws BusinessException {
        log.info("导出产权登记:{}",new Gson().toJson(data));
        return wyProRightRegService.export(data,response);

    }

}

