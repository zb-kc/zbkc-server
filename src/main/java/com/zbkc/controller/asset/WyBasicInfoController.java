package com.zbkc.controller.asset;


import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.ToolUtils;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.RentalSocialWyInfoDTO;
import com.zbkc.model.dto.SocicalWyDTO;
import com.zbkc.model.dto.WyBasicInfoDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.dto.WyBasicInfoHomeDTO;
import com.zbkc.model.vo.WyBaseInfoVO;
import com.zbkc.model.vo.WyBasicInfoVo;
import com.zbkc.service.WyBasicInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 物业基本(主体)信息表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-08-09
 */
@Api(tags = "资产登记")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/wyBasicInfo")
public class WyBasicInfoController {
    @Autowired
    private WyBasicInfoService wyBasicInfoService;
    @Resource
    private ToolUtils toolUtils;

    @ApiOperation(value = "新增 大厦,栋，层，室")
    @PostMapping("/addBasic")
    public ResponseVO addBasic(@RequestBody WyBasicInfoVo data) throws Exception {
        log.info("新增大厦:{}",new Gson().toJson(data));

        return wyBasicInfoService.addBasic( data );
    }


    @ApiOperation(value = "首页查询大厦信息列表")
    @PostMapping("/list")
    public ResponseVO getList(@RequestBody WyBasicInfoHomeDTO data) throws BusinessException {
        log.info("查看大厦:{}",new Gson().toJson(data));

        return wyBasicInfoService.getList( data );
    }

    @ApiOperation(value = "修改 大厦,栋，层，室")
    @PostMapping(value = "/update")
    public ResponseVO update(@RequestBody WyBasicInfoDTO dto) throws Exception {
        log.info("修改111:{}",new Gson().toJson(dto));
        return wyBasicInfoService.updateInfo(dto);
    }


    @ApiOperation(value = "查看 大厦,栋，层，室 详细信息")
    @GetMapping(value = "/details/{id}")
    public ResponseVO details(@PathVariable("id")Long id) throws BusinessException {
        return wyBasicInfoService.getDetails(id);
    }

    @ApiOperation(value = "批量查询")
    @GetMapping(value = "/batch")
    public ResponseVO batch(Long[] ids) throws BusinessException {
        return wyBasicInfoService.batch(ids);
    }

    @ApiOperation(value = "物业结构查询")
    @GetMapping(value = "/property/structure")
    public ResponseVO propertyStructure(@RequestParam("id")Long id ,@RequestParam("userId") Long userId ) throws BusinessException {
        if(null == userId){
            return new ResponseVO(ErrorCodeEnum.USERID_NOT_FOUND, ErrorCodeEnum.USERID_NOT_FOUND.getErrMsg());
        }

        return wyBasicInfoService.propertyStructure(id, userId);
    }

    @ApiOperation(value = "获取物业编码")
    @GetMapping(value = "/property/code")
    public ResponseVO propertyCode() throws BusinessException {
        return new ResponseVO( ErrorCodeEnum.SUCCESS,toolUtils.getPropertyCode() );
    }

    @ApiOperation(value = "名称去重")
    @GetMapping(value = "/property/nameCheck")
    public ResponseVO nameCheck(@RequestParam("pid")Long pid,String name,String unitName) throws BusinessException {
        return wyBasicInfoService.nameCheck(pid,name,unitName);
    }


    @ApiOperation(value = "物业名称查询")
    @GetMapping(value = "/property/name/{index}")
    public ResponseVO getPropertyName(@PathVariable("index")String index) throws BusinessException {
        return wyBasicInfoService.getPropertyName(index);
    }

    @ApiOperation(value = "逻辑删除")
    @GetMapping(value = "/del/{id}")
    public ResponseVO logicDel(@PathVariable("id")Long id) throws BusinessException {
        return wyBasicInfoService.logicDel(id);
    }

    @ApiOperation(value = "租赁社会物业列表")
    @PostMapping(value = "/property/list")
    public ResponseVO getRentalSocialWyList(@RequestBody SocicalWyDTO dto) throws BusinessException {
        return wyBasicInfoService.getRentalSocialWyList(dto);
    }

    @ApiOperation(value = "租赁社会物业查看当前行")
    @GetMapping(value = "/property/social/{id}")
    public ResponseVO seeRow(@PathVariable("id") Long id) throws BusinessException {
        return wyBasicInfoService.seeRow(id);
    }

    @ApiOperation(value = "新增租赁社会物业")
    @PostMapping(value = "/property/add")
    public ResponseVO addRentalSocialWy(@RequestBody RentalSocialWyInfoDTO dto) throws Exception {
        log.info("新增租赁物业:{}",new Gson().toJson(dto));
        return wyBasicInfoService.addRentalSocialWy(dto);
    }

    @ApiOperation(value = "修改租赁社会物业")
    @PostMapping(value = "/property/upd")
    public ResponseVO updRentalSocialWy(@RequestBody RentalSocialWyInfoDTO dto) throws Exception {
        log.info("修改租赁物业:{}",new Gson().toJson(dto));
        return wyBasicInfoService.updRentalSocialWy(dto);
    }

    @ApiOperation(value = "删除租赁社会物业")
    @PostMapping(value = "/property/del")
    public ResponseVO updRentalSocialWy(@RequestParam Long[] id) throws BusinessException {
        return wyBasicInfoService.delRentalSocialWy(id);
    }

    /**
     *
     * @param userId
     * @param isUseReg 是否是使用登记
     * @return
     * @throws BusinessException
     */
    @ApiOperation(value = "资产管理-选择物业资产-物业树")
    @PostMapping(value = "/property/structure/all")
    public ResponseVO getAllWyInfo(String userId , boolean isUseReg) throws BusinessException {
        log.info("资产管理-选择物业资产-物业树");
        List<WyBaseInfoVO> wyBaseInfoVOS = this.wyBasicInfoService.selectAllWyBaseInfo(null, userId, isUseReg);
        if(null == wyBaseInfoVOS || wyBaseInfoVOS.isEmpty()){
            return new ResponseVO(ErrorCodeEnum.SUCCESS , wyBaseInfoVOS);
        }

        Map<Long , List<WyBaseInfoVO>> map = new HashMap<>();
        for (WyBaseInfoVO wyBaseInfoVO : wyBaseInfoVOS) {
            Long pid = wyBaseInfoVO.getPid();
            List<WyBaseInfoVO> list = new ArrayList<>();
            if(map.containsKey(pid)){
                list = map.get(pid);
            }
            list.add(wyBaseInfoVO);

            map.put(pid , list);
        }

        List<WyBaseInfoVO> totalChildrenList = propertyStructureList(map.get(0L), map);

        List<WyBaseInfoVO> returnList = new ArrayList<>();

        WyBaseInfoVO root = new WyBaseInfoVO();
        root.setWyName("所有物业资产");
        root.setChildren(totalChildrenList);
        returnList.add(root);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , returnList);
    }

    @ApiOperation(value = "资产管理-查看物业资产")
    @GetMapping(value = "/property/show")
    public ResponseVO showWyInfoByContractId(@RequestParam("contractId") String contractId) throws BusinessException {
        log.info("资产管理-查看物业资产:{}",new Gson().toJson(contractId));
        List<WyBaseInfoVO> wyBaseInfoVOS = this.wyBasicInfoService.selectAllWyBaseInfo(contractId, null, null);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , wyBaseInfoVOS);
    }

    /**
     * 结构化树 
     * @param dto
     * @param map
     * @return
     */
    private List<WyBaseInfoVO> propertyStructureList(List<WyBaseInfoVO> dto , Map<Long,List<WyBaseInfoVO>> map){

        if(null == dto || dto.isEmpty()){
            return null;
        }

        for (WyBaseInfoVO wyBaseInfoVO : dto) {
            List<WyBaseInfoVO> list = map.get(wyBaseInfoVO.getId());
            wyBaseInfoVO.setChildren(list);
            propertyStructureList(wyBaseInfoVO.getChildren(), map);
        }
        return dto;
    }


    @ApiOperation(value = "资产登记-空置列表")
    @GetMapping(value = "/emptys")
    public ResponseVO queryEmptyList(@RequestParam("id") String id,String wyName,String preCompany,Integer p,Integer size) throws BusinessException {
        log.info("资产登记-空置列表:{}",new Gson().toJson(id+":"+wyName+":"+preCompany+":"+p+":"+size));
        return new ResponseVO(ErrorCodeEnum.SUCCESS , wyBasicInfoService.queryEmptyList(id,wyName,preCompany,p,size));
    }

    @ApiOperation(value = "资产登记-使用列表")
    @GetMapping(value = "/useds")
    public ResponseVO queryUsedList(@RequestParam("id") String id,String wyName,String objName,Integer p,Integer size) throws BusinessException {
        log.info("资产登记-使用列表:{}",new Gson().toJson(id+":"+wyName+":"+objName+":"+p+":"+size));

        /**
         * 根据物业id 查询其下所有子节点（包含自身） 得到物业id集合A
         * 根据集合A 内关联查询 使用登记表 得到物业id集合B
         * 根据集合A 内关联查询 合同表关联物业基础信息表  得到物业id集合C
         * 取集合B与集合C的并集
         */

        return new ResponseVO(ErrorCodeEnum.SUCCESS , wyBasicInfoService.queryUsedList(id ,wyName,objName,p,size));
    }

    @ApiOperation(value = "资产登记-产权列表")
    @GetMapping(value = "/prorights")
    public ResponseVO queryProRightList(@RequestParam("id") String id,String wyName,String certificateNo,Integer p,Integer size) throws BusinessException {
        log.info("资产登记-产权列表:{}",new Gson().toJson(id+":"+wyName+":"+certificateNo+":"+p+":"+size));

        return new ResponseVO(ErrorCodeEnum.SUCCESS , wyBasicInfoService.queryProrightList(id,wyName,certificateNo,p,size));
    }

    @ApiOperation(value = "资产登记-入账列表")
    @GetMapping(value = "/entrys")
    public ResponseVO queryEntryList(@RequestParam("id") String id,String wyName,String entryType,Integer p,Integer size) throws BusinessException {
        log.info("资产登记-入账列表:{}",new Gson().toJson(id+":"+wyName+":"+entryType+":"+p+":"+size));
        return new ResponseVO(ErrorCodeEnum.SUCCESS , wyBasicInfoService.queryEntryList(id,wyName,entryType,p,size));
    }

    @ApiOperation(value = "资产登记-楼栋列表")
    @GetMapping(value = "/building")
    public ResponseVO queryBuildingList(@RequestParam("id") String id,String wyName,Integer p,Integer size) throws BusinessException {
        log.info("资产登记-楼栋列表:{}",new Gson().toJson(id+":"+wyName+":"+p+":"+size));
        return new ResponseVO(ErrorCodeEnum.SUCCESS , wyBasicInfoService.queryBuildingList(id, wyName,p,size));
    }
    @ApiOperation(value = "资产登记-楼层列表")
    @GetMapping(value = "/floor")
    public ResponseVO queryFloorList(@RequestParam("id") String id,String wyName,Integer p,Integer size) throws BusinessException {
        log.info("资产登记-楼层列表:{}",new Gson().toJson(id+":"+wyName+":"+p+":"+size));
        return new ResponseVO(ErrorCodeEnum.SUCCESS , wyBasicInfoService.queryFloorList(id, wyName,p,size));
    }
    @ApiOperation(value = "资产登记-单元列表")
    @GetMapping(value = "/unit")
    public ResponseVO queryUnitList(@RequestParam("id") String id,String wyName,String company,String status,Integer p,Integer size) throws BusinessException {
        log.info("资产登记-单元列表:{}",new Gson().toJson(id+":"+wyName+":"+status+":"+p+":"+size));
        return new ResponseVO(ErrorCodeEnum.SUCCESS , wyBasicInfoService.queryUnitList(id, wyName, company, status,p,size));
    }

    @ApiOperation(value = "获取所有空置物业")
    @GetMapping(value = "/wyInfo/empty")
    public ResponseVO getEmptyWyInfo(@RequestParam("userId") Long userId) throws BusinessException {
        log.info("所有空置物业-用户id:{}",new Gson().toJson(userId));
        return wyBasicInfoService.getEmptyWyInfo(userId);
    }
}

