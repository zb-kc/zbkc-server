package com.zbkc.controller.asset;


import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.WyAssetEntryDTO;
import com.zbkc.model.dto.WyAssetEntryHomeDTO;
import com.zbkc.model.dto.export.ExporProRightRegDTO;
import com.zbkc.model.dto.export.ExportWyAssetEntryDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.WyAssetEntryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 资产入账信息表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-09-03
 */
@Api(tags = "资产管理-资产入账")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/wyAssetEntry")
public class WyAssetEntryController {
    @Autowired
    private WyAssetEntryService wyAssetEntryService;

    @ApiOperation(value = "资产名称")
    @PostMapping("/allProRight")
    public ResponseVO allName(@RequestParam("userId") Long userId) throws BusinessException {

        if(null == userId){
            return new ResponseVO(ErrorCodeEnum.USERID_NOT_FOUND, ErrorCodeEnum.USERID_NOT_FOUND.getErrMsg());
        }

        ResponseVO responseVO = wyAssetEntryService.selectAllName(userId);
        return responseVO;
    }

    @ApiOperation(value = "新增产权入账")
    @PostMapping(value = "/addEntry")
    public ResponseVO addEntry(@RequestBody WyAssetEntryDTO data) throws Exception {
        log.info("新增入账:{}",new Gson().toJson(data));
        return wyAssetEntryService.addEntry(data);
    }

    @ApiOperation(value = "首页查询资产入账列表")
    @PostMapping("/list")
    public ResponseVO getList(@RequestBody WyAssetEntryHomeDTO data) throws BusinessException {
        log.info("查看入账:{}",new Gson().toJson(data));
       return wyAssetEntryService.getList( data );

    }

    @ApiOperation(value = "查看当前行的数据")
    @GetMapping("/row/{id}")
    public ResponseVO seeRow(@PathVariable("id") String id) throws BusinessException {
        log.info("资产入账:{}",new Gson().toJson(id));
        return wyAssetEntryService.seeRow(id);
    }

    @ApiOperation(value = "修改入账")
    @RequestMapping(value = "/upd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updEntry(@RequestBody WyAssetEntryDTO data) throws Exception {
        log.info("修改入账:{}",new Gson().toJson(data));
        return wyAssetEntryService.updEntry(data);
    }

    @ApiOperation(value = "删除入账")
    @GetMapping("/del/{id}")
    public ResponseVO delAssetEntry(@PathVariable("id") String id) throws BusinessException {
        log.info("删除入账:{}",new Gson().toJson(id));
        return wyAssetEntryService.delAssetEntry(id);
    }

    @ApiOperation(value = "导出资产入账列表")
    @RequestMapping(value = "/list/export", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = "text/html;charset=UTF-8")
    public ResponseVO export(@RequestBody ExportWyAssetEntryDTO data, HttpServletResponse response) throws BusinessException {
        log.info("导出产权登记:{}",new Gson().toJson(data));
        return wyAssetEntryService.export(data,response);

    }

}

