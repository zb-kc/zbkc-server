package com.zbkc.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.po.SysUser;
import com.zbkc.model.po.SysUserPO;
import com.zbkc.model.vo.AuthInfoVO;
import com.zbkc.model.dto.LoginDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.LoginService;
import com.zbkc.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  用户信息控制层
 *  @author gmding
 *  @date 2021-7-16
 * */

@Api(tags = "用户信息")
@RestController
@RequestMapping("/userInfo")
@Slf4j
@CrossOrigin("*")
public class UserInfoController {
    @Resource
    private LoginService loginService;
    @Resource
    private SysUserService sysUserService;
    /**
     * @api {POST} /login
     * @apiDescription  用户登录
     * @apiName index
     * @apiParam {String} userName 用户的姓名
     * @apiParam {String} pwd 密码
     * @apiParamExample {json} Request-Example:
     * {
     *     "userName":"",
     *     "pwd":""
     * }
     * @apiGroup 用户信息
     * @apiSampleRequest userInfo/login
     *  @apiSuccess {string} code 错误码10000.Successful、10005.非法请求、10007.参数错误、2001.验证码过期、2002.验证码错误，2003.尚未生成验证码
     *  @apiSuccess {string} errMsg 返回详情
     *  @apiSuccess {array[]} data 结果集

     */

    @ApiOperation(value = "登录")
    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO userLogin(@RequestBody LoginDTO data) throws BusinessException {
        log.info("josn:{}",new Gson().toJson(data));
        return loginService.userLogin(data);
    }

    @ApiOperation(value = "token过期,刷新token")
    @RequestMapping(value = "/refreshToken", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO getRefreshToken(@RequestBody AuthInfoVO data) throws BusinessException {
        return loginService.getRefreshToken(data);
    }

    @ApiOperation(value = "匿名获取token")
    @GetMapping("/getAccessToken")
    public ResponseVO getAccessToken() throws BusinessException {
        return loginService.getAccessToken();
    }

    @ApiOperation(value = "根据id查询")
    @GetMapping("/find/{id}")
    public ResponseVO getById(@PathVariable("id") Serializable id){
        return sysUserService.selectById(id);
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public ResponseVO selectPage(@RequestParam Integer current,Integer size)
    {
        System.out.println("页码:"+current+","+"行数："+size);
        Page<SysUser> page = new Page<>(current,size);
       ResponseVO responseVO = sysUserService.selectPage(page,null);
        return responseVO;
    }
}
