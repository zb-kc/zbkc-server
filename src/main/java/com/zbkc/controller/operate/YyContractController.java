package com.zbkc.controller.operate;


import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.YyContractWyRelationMapper;
import com.zbkc.model.dto.DownLoadDTO;
import com.zbkc.model.dto.YyContractDTO;
import com.zbkc.model.dto.YyContractIndexDTO;
import com.zbkc.model.dto.export.ExportContractDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.WyBasicInfoService;
import com.zbkc.service.YwAgencyBusinessService;
import com.zbkc.service.YyContractService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;


/**
 * <p>
 * 运营管理——合同管理 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-09-13
 */
@Api(tags = "运营管理-合同管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/yyContract")
public class YyContractController {

    @Autowired
    private YyContractService yyContractService;
    @Autowired
    private WyBasicInfoService wyBasicInfoService;
    @Autowired
    private YyContractWyRelationMapper yyContractWyRelationMapper;
    @Autowired
    private YwAgencyBusinessService ywAgencyBusinessService;

    @ApiOperation(value = "新增与修改合同")
    @PostMapping(value = "/addContract")
    public ResponseVO addContract(@RequestBody YyContractDTO data) throws BusinessException {
        log.info("新增与修改合同:{}",new Gson().toJson(data));
        return yyContractService.addAndUpdContract(data);
    }


    @ApiOperation(value = "查看合同管理首页")
    @PostMapping("/list")
    public  ResponseVO  getList(@RequestBody YyContractIndexDTO data)throws  BusinessException{
        log.info("查看合同首页:{}",new Gson().toJson(data));
        if(null == data.getUserId()){
            return new ResponseVO(ErrorCodeEnum.USERID_NOT_FOUND, ErrorCodeEnum.USERID_NOT_FOUND.getErrMsg());
        }
        return  yyContractService.queryContractList(data);
    }

    @ApiOperation(value = "查看当前行的数据")
    @GetMapping("/row")
    public ResponseVO seeRow(@RequestParam String id ) throws BusinessException {
        log.info("查看单个合同:{}",new Gson().toJson(id));
        return yyContractService.seeRow(id);
    }

    @ApiOperation(value = "导出合同")
    @PostMapping("/export")
    public ResponseVO exportContractHome(@RequestBody ExportContractDTO dto, HttpServletResponse response) throws BusinessException {
        log.info("导出合同:{}",new Gson().toJson(dto));
        return yyContractService.exportContractHome(dto,response);

    }

    @ApiOperation(value = "根据选择的物业id查询")
    @PostMapping(value = "/wyIds")
    public ResponseVO selectByWyIds(@RequestParam Long[] wyIds) throws BusinessException {
        log.info("物业id：{}",new Gson().toJson(wyIds));
        return wyBasicInfoService.selectByWyId(wyIds);
    }


    @ApiOperation("合同管理-下载租赁交款通知单")
    @PostMapping("/downLoadPaymentNotice")
    public ResponseVO downLoadPaymentNotice(HttpServletResponse response ,@RequestBody DownLoadDTO dto) throws BusinessException {
        log.info("合同管理-下载租赁缴款通知书:{}",new Gson().toJson(dto));
        return yyContractService.downLoadPaymentNotice(response,dto);
    }

    @ApiOperation("合同管理-下载合同模板")
    @PostMapping("/downLoadContactTemplate")
    public ResponseVO downLoadContactTemplate(HttpServletResponse response ,@RequestBody DownLoadDTO dto) throws BusinessException {
        log.info("合同管理-下载合同模板:{}",new Gson().toJson(dto));
        return yyContractService.downLoadContactTemplate(response,dto);
    }


}

