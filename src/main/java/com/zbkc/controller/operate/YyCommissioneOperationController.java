package com.zbkc.controller.operate;


import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.export.ExportCommissioneOperationDTO;
import com.zbkc.model.dto.YyCommissioneOperationDTO;
import com.zbkc.model.dto.YyCommissioneOperationHomeDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.YyCommissioneOperationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;


/**
 * <p>
 * 运营管理——委托运营 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-09-10
 */
@Api(tags = "运营管理-委托运营")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/yyCommissioneOperation")
public class YyCommissioneOperationController {

    @Autowired
    private YyCommissioneOperationService yyCommissioneOperationService;

    @ApiOperation(value = "新增委托")
    @PostMapping(value = "/addOperation")
    public ResponseVO addCommissioneOperation(@RequestBody YyCommissioneOperationDTO data) throws BusinessException {
        log.info("新增委托:{}",new Gson().toJson(data));
      return yyCommissioneOperationService.addCommissioneOperation(data);

    }


    @ApiOperation(value = "首页查询委托运营列表")
    @PostMapping("/list")
    public ResponseVO getList(@RequestBody YyCommissioneOperationHomeDTO data) throws BusinessException {
        log.info("查看委托运营:{}",new Gson().toJson(data));

        if(null == data.getUserId()){
            return new ResponseVO(ErrorCodeEnum.USERID_NOT_FOUND, ErrorCodeEnum.USERID_NOT_FOUND.getErrMsg());
        }

        return yyCommissioneOperationService.getList( data );

    }

    @ApiOperation(value = "查看当前行的数据")
    @GetMapping("/row/{id}")
    public ResponseVO seeRow(@PathVariable("id") Long id) throws BusinessException {
        log.info("委托运营:{}",new Gson().toJson(id));
        return yyCommissioneOperationService.seeRow(id);

    }

    @ApiOperation(value = "修改委托运营")
    @RequestMapping(value = "/updOperation", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updCommissioneOperation(@RequestBody YyCommissioneOperationDTO data) throws BusinessException {
        log.info("修改委托运营:{}",new Gson().toJson(data));
        return yyCommissioneOperationService.updCommissioneOperation(data);

    }

    @ApiOperation(value = "导出委托运营表")
    @RequestMapping(value = "/export", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = "text/html;charset=UTF-8")
    public ResponseVO updCommissioneOperation(@RequestBody ExportCommissioneOperationDTO data, HttpServletResponse response) throws BusinessException {
        log.info("导出委托运营表:{}",new Gson().toJson(data));
        return yyCommissioneOperationService.exportCommissioneOperation(data,response);

    }
}

