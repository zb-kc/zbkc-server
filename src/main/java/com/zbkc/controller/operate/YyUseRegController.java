package com.zbkc.controller.operate;


import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.export.ExportUseRegDTO;
import com.zbkc.model.dto.YyUseRegDTO;
import com.zbkc.model.dto.YyUseRegHomeDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.YyUseRegService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;


/**
 * <p>
 * 运营管理——使用登记 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-09-09
 */
@Api(tags = "运营管理-使用登记")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/yyUseReg")
public class YyUseRegController {
    @Autowired
    private YyUseRegService yyUseRegService;

    @ApiOperation(value = "新增使用登记")
    @PostMapping(value = "/addUseReg")
    public ResponseVO addUseReg(@RequestBody YyUseRegDTO data) throws BusinessException {
        log.info("新增使用登记:{}",new Gson().toJson(data));
       return yyUseRegService.addUseReg(data);
    }

    @ApiOperation(value = "首页查询使用登记列表")
    @PostMapping("/list")
    public ResponseVO getList(@RequestBody YyUseRegHomeDTO data) throws BusinessException {
        log.info("查看使用登记:{}",new Gson().toJson(data));

        if(null == data.getUserId()){
            return new ResponseVO(ErrorCodeEnum.USERID_NOT_FOUND, ErrorCodeEnum.USERID_NOT_FOUND.getErrMsg());
        }

        return yyUseRegService.getList( data );
    }

    @ApiOperation(value = "查看当前行的数据")
    @GetMapping("/row/{id}")
    public ResponseVO seeRow(@PathVariable("id") String id) throws BusinessException {
        log.info("使用登记:{}",new Gson().toJson(id));
        return yyUseRegService.seeRow(id);

    }

    @ApiOperation(value = "修改使用登记")
    @RequestMapping(value = "/upd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updUseReg(@RequestBody YyUseRegDTO data) throws BusinessException {
        log.info("修改使用登记:{}",new Gson().toJson(data));
        return yyUseRegService.updUseReg(data);

    }

    @ApiOperation(value = "导出使用登记列表")
    @RequestMapping(value = "/list/export", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = "text/html;charset=UTF-8")
    public ResponseVO export(@RequestBody ExportUseRegDTO data, HttpServletResponse response) throws BusinessException {
        log.info("导出使用登记:{}",new Gson().toJson(data));
        return yyUseRegService.export(data,response);

    }

    @ApiOperation(value = "使用管理—停用")
    @GetMapping(value = "/use/stop")
    public ResponseVO usingStop(@RequestParam Long id ,@RequestParam Integer status) throws BusinessException {
        log.info("停用-id：{}，",new Gson().toJson(id));
        return yyUseRegService.updStatus(id,status);

    }

}

