package com.zbkc.controller.operate;

import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.YwAgencyBusinessDTO;
import com.zbkc.model.po.YwAgencyBusinessPO;
import com.zbkc.model.vo.PageVO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.vo.YwAgencyBusinessVO;
import com.zbkc.model.vo.YyContractVO;
import com.zbkc.service.ActivitiBaseService;
import com.zbkc.service.YwAgencyBusinessService;
import com.zbkc.service.YyContractService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * 企业授权管理
 * @author ZB3436.xiongshibao
 * @date 2021/10/26
 */
@Api(tags = "运营管理-企业授权管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/yyCompanyAuthorization")
public class YyCompanyAuthorizationController {

    @Autowired
    private YwAgencyBusinessService ywAgencyBusinessService;

    @Autowired
    private ActivitiBaseService activitiBaseService;

    @Autowired
    private YyContractService yyContractService;


    @ApiOperation("运营管理-企业授权管理-分页查询")
    @PostMapping("/contactSignList")
    public ResponseVO contactSignList(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException {

        log.info("运营管理-企业授权管理-分页查询:{}",new Gson().toJson(ywAgencyBusinessDTO));

        if(ywAgencyBusinessDTO.getP()<=0){
            return new ResponseVO(ErrorCodeEnum.PAGE_NUMBER_ERROR , ErrorCodeEnum.PAGE_NUMBER_ERROR.getErrMsg());
        }

        if(null == ywAgencyBusinessDTO.getUserId() || ywAgencyBusinessDTO.getUserId() < 1){
            return new ResponseVO(ErrorCodeEnum.USERID_NOT_FOUND , ErrorCodeEnum.USERID_NOT_FOUND.getErrMsg());
        }

        PageVO<YwAgencyBusinessVO> ywAgencyBusinessVOPage = this.ywAgencyBusinessService.pageYwAgencyBusinessVOList(ywAgencyBusinessDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , ywAgencyBusinessVOPage);
    }

    @ApiOperation("运营管理-企业授权管理-新增授权")
    @PostMapping("/addRecord")
    public ResponseVO addRecord(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{

        log.info("运营管理-企业授权管理-新增授权:{}",new Gson().toJson(ywAgencyBusinessDTO));

        ResponseVO checkVo = this.ywAgencyBusinessService.checkRecord(ywAgencyBusinessDTO);
        if(null != checkVo){
            return checkVo;
        }

        int record = this.ywAgencyBusinessService.addYwAgencyBusiness(ywAgencyBusinessDTO);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @ApiOperation("运营管理-企业授权管理-取消授权")
    @PostMapping("/cancelAuthorization")
    public ResponseVO cancelAuthorization(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{

        log.info("运营管理-企业授权管理-取消授权:{}",new Gson().toJson(ywAgencyBusinessDTO));

        //根据代办业务id查询是否存在工作流实例 存在则不能取消
        String processStatus = this.activitiBaseService.getProcessStatus(String.valueOf(ywAgencyBusinessDTO.getId()));
        if(null != processStatus){
            return new ResponseVO(ErrorCodeEnum.CANEL_ERROR_PROGRESSING , ErrorCodeEnum.CANEL_ERROR_PROGRESSING.getErrMsg());
        }

        YwAgencyBusinessPO ywAgencyBusinessPO = new YwAgencyBusinessPO();
        BeanUtils.copyProperties(ywAgencyBusinessDTO , ywAgencyBusinessPO);
        int record = this.ywAgencyBusinessService.cancelAuthorization(ywAgencyBusinessPO);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @ApiOperation("运营管理-企业授权管理-选择合同-列表")
    @PostMapping("/chooseContract/List")
    public ResponseVO chooseContractList(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{

        log.info("运营管理-企业授权管理-选择合同-列表:{}",new Gson().toJson(ywAgencyBusinessDTO));
        PageVO<YyContractVO> yyContractVOList = this.yyContractService.selectYyContractListByPage(ywAgencyBusinessDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , yyContractVOList);
    }

    @ApiOperation("运营管理-企业授权管理-选择合同-保存")
    @GetMapping("/chooseContract/save")
    public ResponseVO chooseContractSave(@RequestParam("socialUniformCreditCode") String socialUniformCreditCode , @RequestParam("contractIds") String contractIds) throws BusinessException{

        log.info("运营管理-企业授权管理-选择合同-保存:{}",new Gson().toJson(contractIds));
        int record = this.ywAgencyBusinessService.chooseContractSave(socialUniformCreditCode, contractIds);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @ApiOperation("运营管理-企业授权管理-选择主合同-列表")
    @PostMapping("/chooseMainContract/List")
    public ResponseVO chooseMainContractList(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{

        log.info("运营管理-企业授权管理-选择主合同-列表:{}",new Gson().toJson(ywAgencyBusinessDTO));
        PageVO<YyContractVO> yyContractVOList = this.yyContractService.selectYyContractListByPage(ywAgencyBusinessDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , yyContractVOList);
    }

    @ApiOperation("运营管理-企业授权管理-选择主合同")
    @PostMapping("/chooseMainContract")
    public ResponseVO chooseMainContract(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{

        log.info("运营管理-企业授权管理-选择主合同:{}",new Gson().toJson(ywAgencyBusinessDTO));
        int record = this.ywAgencyBusinessService.chooseMainContract(ywAgencyBusinessDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @ApiOperation("运营管理-企业授权管理-取消主合同")
    @PostMapping("/cancelMainContract")
    public ResponseVO cancelMainContract(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{

        log.info("运营管理-企业授权管理-取消主合同:{}",new Gson().toJson(ywAgencyBusinessDTO));
        ywAgencyBusinessDTO.setMainContractId("0");
        int record = this.ywAgencyBusinessService.chooseMainContract(ywAgencyBusinessDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }


    @ApiOperation("运营管理-企业授权管理-更新授权")
    @PostMapping("/updateRecord")
    public ResponseVO updateRecord(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{

        log.info("运营管理-企业授权管理-更新授权:{}",new Gson().toJson(ywAgencyBusinessDTO));
        int record = this.ywAgencyBusinessService.updateYwAgencyBusiness(ywAgencyBusinessDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @ApiOperation("运营管理-企业授权管理-导出")
    @PostMapping("/export")
    public void export(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO , HttpServletResponse response) throws BusinessException{

        log.info("运营管理-企业授权管理-导出{}",new Gson().toJson(ywAgencyBusinessDTO));

        this.ywAgencyBusinessService.exportCompanyAuthorization(ywAgencyBusinessDTO , response);
    }
}
