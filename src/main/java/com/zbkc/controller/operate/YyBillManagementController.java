package com.zbkc.controller.operate;


import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.*;
import com.zbkc.model.dto.export.ExportBillManagementDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.YyBillManagementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 运营管理——账单管理 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-09-18
 */
@Api(tags = "运营管理-账单管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/yyBillManagement")
public class YyBillManagementController {
    @Autowired
    private YyBillManagementService yyBillManagementService;


    @ApiOperation(value = "首页")
    @PostMapping("/homeList")
    public ResponseVO homeList(@RequestBody BillManagementDTO dto){
        log.info("首页参数:{}",dto);

        if(null == dto.getUserId()){
            return new ResponseVO(ErrorCodeEnum.USERID_NOT_FOUND, ErrorCodeEnum.USERID_NOT_FOUND.getErrMsg());
        }

        return yyBillManagementService.homeList(dto);
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public ResponseVO saveBill(@RequestBody YyBillManagementDTO dto){
        log.info("账单管理:{}",dto);
        return yyBillManagementService.saveBill(dto);
    }

    @ApiOperation(value = "查看账单")
    @GetMapping("/seeRow")
    public ResponseVO seeRow(@RequestParam String id,@RequestParam Long wyId){
        log.info("账单管理-账单id:{}，物业id:{}",id,wyId);
        return yyBillManagementService.seeRow(id,wyId);
    }


    @ApiOperation(value = "账单收款-页面")
    @PostMapping("/collecInit")
    public ResponseVO collecInit(@RequestBody CollecDTO dto){
        return yyBillManagementService.collecInit(dto);
    }


    @ApiOperation(value = "确认收款")
    @PostMapping("/collecMoney/save")
    public ResponseVO collecMoney(@RequestBody List<CollecMoneyDTO> dto){
        log.info("确认收款:{}",new Gson().toJson(dto));
        return yyBillManagementService.collecMoneySave(dto);
    }


    @ApiOperation(value = "短信催缴")
    @PostMapping("/callSms")
    public ResponseVO callSms(@RequestBody SmsDTO dto){
        return  yyBillManagementService.callSms(dto);
    }

    @ApiOperation(value = "催缴记录")
    @PostMapping(value = "/callRecord")
    public ResponseVO selectAllCallRecord(@RequestBody SmsReCordDTO data) throws BusinessException {
        log.info("催缴记录:{}",new Gson().toJson(data));
        return yyBillManagementService.selectAllCallRecord(data);
    }


    @ApiOperation(value = "根据物业类型获取物业名称")
    @GetMapping(value = "/type/name")
    public ResponseVO selectWyNameByType(@RequestParam String wyType) throws BusinessException {
        return yyBillManagementService.selectWyNameByType(wyType);
    }

    @ApiOperation(value = "导出账单列表")
    @RequestMapping(value = "/list/export", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = "text/html;charset=UTF-8")
    public ResponseVO export(@RequestBody ExportBillManagementDTO data, HttpServletResponse response) throws BusinessException {
        log.info("导出账单:{}",new Gson().toJson(data));
        return yyBillManagementService.exportBillMAnagement(data,response);
    }

//    @ApiOperation(value = "短信发送页面")
//    @GetMapping(value = "/sms/index")
//    public ResponseVO sendSmsIndex(@RequestParam Long contractId) throws BusinessException {
//        return yyBillManagementService.sendSmsIndex(contractId);
//    }

    @ApiOperation(value = "根据账期获取每月缴费情况")
    @GetMapping(value = "/pay")
    public ResponseVO getBillCount(@RequestParam String billTime) throws BusinessException {
        return yyBillManagementService.getBillCount(billTime);
    }

    @ApiOperation(value = "获取所有短信模板名称")
    @GetMapping(value = "/template/name")
    public ResponseVO getAllSmsName() throws BusinessException {
        return new ResponseVO(ErrorCodeEnum.SUCCESS,yyBillManagementService.getAllSmsName());
    }

    @ApiOperation(value = "根据短信名称获取短信内容")
    @GetMapping(value = "/word")
    public ResponseVO getWordByName(@RequestParam String name) throws BusinessException {
        return yyBillManagementService.getWordByName(name);
    }

    @ApiOperation(value = "多户收款-页面")
    @GetMapping(value = "/collection/many")
    public ResponseVO manyCollection(@RequestParam Long contractId,@RequestParam Integer billStatus) throws BusinessException {
        return yyBillManagementService.manyCollection(contractId,billStatus);
    }


    @ApiOperation(value = "多户收款-收款保存")
    @PostMapping(value = "/many/save")
    public ResponseVO saveManyCollection(@RequestBody List<SaveCollectionDTO> data) throws BusinessException {
        log.info("多户收款-收款保存:{}",new Gson().toJson(data));
        return yyBillManagementService.saveManyCollection(data);
    }





}

