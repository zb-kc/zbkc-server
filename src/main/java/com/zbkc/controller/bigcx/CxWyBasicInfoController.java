package com.zbkc.controller.bigcx;

import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.EnterpriseDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.WyAssetInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 系统用户表
 * </p>
 *
 * @author yangyan
 * @since 2021-07-27
 */
@Api(tags = "创新型产业用房")
@RestController
@Slf4j
@RequestMapping("/cx/basicInfo")
public class CxWyBasicInfoController {

    @Autowired
    private WyAssetInfoService wyAssetInfoService;


    @ApiOperation(value = "2020年入驻企业排行")
    @GetMapping("/enterprise/sidewalk/{type}")
    public ResponseVO enterpriseSidewalk(@PathVariable(value = "type")int type) throws BusinessException {

        return wyAssetInfoService.enterpriseSidewalk(type);
    }


    @ApiOperation(value = "入驻企业列表")
    @PostMapping("/enterprise/list")
    public ResponseVO enterpriseList(@RequestBody  EnterpriseDTO dto) throws BusinessException {

        return wyAssetInfoService.enterpriseList(dto);
    }

    @ApiOperation(value = "入驻企业列表2")
    @PostMapping("/enterprise/gqlist")
    public ResponseVO enterpriseGqList(@RequestBody  EnterpriseDTO dto) throws BusinessException {

        return wyAssetInfoService.enterpriseGqList(dto);
    }

    @ApiOperation(value = "人才列表")
    @PostMapping("/enterprise/talent")
    public ResponseVO enterpriseTalent(@RequestBody  EnterpriseDTO dto) throws BusinessException {

        return this.wyAssetInfoService.enterpriseTalent(dto);
    }


}
