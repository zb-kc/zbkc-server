package com.zbkc.controller.sys;

import com.google.gson.Gson;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.PageDTO;
import com.zbkc.model.po.SysDataType;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.SysDataTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 数据字典类型表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-08-03
 */
@Api(tags = "系统管理-数据字典")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sysDataType")
public class SysDataTypeController {

    @Autowired
    private SysDataTypeService sysDataTypeService;

    @ApiOperation(value = "分页查询")
    @RequestMapping(value = "/page", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO findPage(@RequestBody PageDTO dto) throws BusinessException {
        log.info("数据字典:{}",new Gson().toJson(dto));
        return sysDataTypeService.pageByName(dto.getP(),dto.getSize(),dto.getIndex());
    }

    @ApiOperation(value = "根据code查询所有物业")
    @GetMapping("/code/{code}")
    public ResponseVO selectAllByCode(@PathVariable("code")String code) throws BusinessException {
        log.info("数据字典:{}",new Gson().toJson(code));
        return sysDataTypeService.selectAllByCode(code);
    }

    @ApiOperation(value = "新增类型")
    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addRole(@RequestBody SysDataType sysDataType) throws BusinessException {
        log.info("数据字典:{}",new Gson().toJson(sysDataType));
        return sysDataTypeService.insert(sysDataType);
    }

    @ApiOperation(value = "修改类型")
    @RequestMapping(value = "/upd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updRole(@RequestBody SysDataType data) throws BusinessException {
        log.info("数据字典:{}",new Gson().toJson(data));
        return sysDataTypeService.updateById(data);
    }

    @ApiOperation(value = "状态变化")
    @GetMapping("/status")
    public ResponseVO status(@RequestParam("id") Long id ,@RequestParam("status") Boolean status) throws BusinessException {
        log.info("数据字典:{}",new Gson().toJson(id));
        log.info("数据字典:{}",new Gson().toJson(status));
        return sysDataTypeService.status(id,status);
    }

}

