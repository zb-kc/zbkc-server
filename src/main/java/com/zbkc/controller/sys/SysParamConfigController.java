package com.zbkc.controller.sys;


import com.google.gson.Gson;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.po.SysParamConfig;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.SysParamConfigService;
import io.lettuce.core.dynamic.annotation.Param;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;


/**
 * <p>
 * 系统参数配置表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-08-05
 */
@Api(tags = "系统管理-系统参数配置")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sysParamConfig")
public class SysParamConfigController {
    @Autowired
    private SysParamConfigService sysParamConfigService;

    @ApiOperation(value = "系统参数列表")
    @GetMapping("/list")
    public ResponseVO findAll() throws BusinessException {
        return sysParamConfigService.selectList(null);
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    public ResponseVO findPage(@RequestParam("page")Integer page, @RequestParam("size")Integer size) throws BusinessException {
        log.info("系统参数配置:{}",new Gson().toJson(page));
        log.info("系统参数配置:{}",new Gson().toJson(size));
        return sysParamConfigService.page(page,size);
    }

    @ApiOperation(value = "条件查询")
    @GetMapping("/input")
    public ResponseVO findByInput(@Param("name") String name) throws BusinessException {
        log.info("系统参数配置:{}",new Gson().toJson(name));
       return sysParamConfigService.selectByInput(name);
    }

    @ApiOperation(value = "新增系统参数配置")
    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addRole(@RequestBody SysParamConfig sysParamConfig) throws BusinessException {
        log.info("数据字典值:{}",new Gson().toJson(sysParamConfig));
        return sysParamConfigService.insert(sysParamConfig);
    }

    @ApiOperation(value = "修改系统参数配置")
    @RequestMapping(value = "/upd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updRole(@RequestBody SysParamConfig data) throws BusinessException {
        log.info("数据字典值:{}",new Gson().toJson(data));
        return sysParamConfigService.updateById(data);
    }

    @ApiOperation(value = "状态变化")
    @GetMapping("/status")
    public ResponseVO status(@RequestParam("id") Long id ,@RequestParam("status") Boolean status) throws BusinessException {
        log.info("数据字典:{}",new Gson().toJson(id));
        log.info("数据字典:{}",new Gson().toJson(status));
        return sysParamConfigService.status(id,status);
    }

}

