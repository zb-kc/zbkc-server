package com.zbkc.controller.sys;


import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.GrantOrgDTO;
import com.zbkc.model.dto.GrantUserDTO;
import com.zbkc.model.dto.MenuDTO;
import com.zbkc.model.dto.OrgDTO;
import com.zbkc.model.po.SysMenu;
import com.zbkc.model.po.SysOrg;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.SysOrgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 * 组织管理 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-08-11
 */
@Api(tags = "系统管理-组织管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sysOrg")
public class SysOrgController {
    @Autowired
    private SysOrgService sysOrgService;

    @ApiOperation(value = "组织管理列表")
    @RequestMapping(value = "/list", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO findAll(@RequestBody OrgDTO orgDTO) throws BusinessException {
        log.info("组织管理:{}",new Gson().toJson(orgDTO));
        return sysOrgService.orgList(orgDTO);
    }
    @ApiOperation(value = "运营管理单位")
    @GetMapping("/parentOrg")
    public ResponseVO parentOrg() {
        return sysOrgService.parentOrg();
    }

    @ApiOperation(value = "运营单位项目组")
    @GetMapping("/sonOrg/{id}")
    public ResponseVO sonOrg(@PathVariable("id")Long id) {
        return sysOrgService.sonOrg(id);
    }

    @ApiOperation(value = "是否禁用")
    @GetMapping("/status")
    public ResponseVO status(@RequestParam("id") Long id ,@RequestParam("status") Boolean status) throws BusinessException {
        log.info("组织管理:{}",new Gson().toJson(id));
        log.info("组织管理:{}",new Gson().toJson(status));
        return sysOrgService.status(id,status);
    }

    @ApiOperation(value = "修改组织")
    @RequestMapping(value = "/upd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updGroup(@RequestBody SysOrg data) throws BusinessException {
        log.info("组织管理:{}",new Gson().toJson(data));
        return sysOrgService.updateById(data);
    }

    @ApiOperation(value = "组织赋权用户")
    @RequestMapping(value = "/grant", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO grantMenus(@RequestBody GrantUserDTO dto) throws BusinessException {
        log.info("组织管理:{}",new Gson().toJson(dto));
        return  sysOrgService.grantUser(dto);

    }

    @ApiOperation(value = "所有用户列表")
    @RequestMapping(value = "/user", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO findAllUser() throws BusinessException {
        return sysOrgService.userList();
    }

    @ApiOperation(value = "组织拥有的用户列表")
    @RequestMapping(value = "/selfOrg/{orgId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO findSelfOrg(@PathVariable("orgId") Long orgId) throws BusinessException {
        log.info("组织管理:{}",new Gson().toJson(orgId));
        return sysOrgService.findSelfUser(orgId);
    }

    @ApiOperation(value = "新增组织")
    @PostMapping("/add")
    public ResponseVO addOrg(@RequestBody SysOrg sysOrg) throws BusinessException{
        log.info("组织管理:{}",new Gson().toJson(sysOrg));
        return sysOrgService.insert(sysOrg);
    }

    @ApiOperation(value = "所有组织-不分页")
    @RequestMapping(value = "/listNoPage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO findAllNoPage() throws BusinessException {
        log.info("所有组织-不分页");
        List<SysOrg> allListNoPage = this.sysOrgService.selectAllListNoPage();
        return new ResponseVO(ErrorCodeEnum.SUCCESS , allListNoPage);
    }
}

