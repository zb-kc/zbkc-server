package com.zbkc.controller.sys;


import com.google.gson.Gson;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.GrantOrgDTO;
import com.zbkc.model.dto.PageDTO;
import com.zbkc.model.po.SysUser;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
 * <p>
 * 系统用户表
 * </p>
 *
 * @author yangyan
 * @since 2021-07-27
 */
@Api(tags = "系统管理-用户管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sysUser")
public class SysUserController {
    @Resource
    private SysUserService sysUserService;


    @ApiOperation(value = "新增用户")
    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addRole(@RequestBody SysUser sysUser) throws BusinessException {
        log.info("用户管理:{}",new Gson().toJson(sysUser));
         return this.sysUserService.insertRecord(sysUser);

    }

    @ApiOperation(value = "修改用户")
    @RequestMapping(value = "/upd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updRole(@RequestBody SysUser data) throws BusinessException {
        log.info("用户管理:{}",new Gson().toJson(data));
        return sysUserService.updateRecord(data);
    }

    @ApiOperation(value = "是否禁用")
    @GetMapping("/status")
    public ResponseVO status(@RequestParam("id") Long id ,@RequestParam("status") Boolean status) throws BusinessException {
        log.info("用户管理:{}",new Gson().toJson(id));
        log.info("用户管理:{}",new Gson().toJson(status));
        return sysUserService.status(id,status);
    }

    @ApiOperation(value = "分页查询")
    @RequestMapping(value = "/page", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO findPage(@RequestBody PageDTO dto) throws BusinessException {
        log.info("用户管理:{}",new Gson().toJson(dto));
        return sysUserService.pageByName(dto.getP(),dto.getSize(),dto.getIndex());
    }

    @ApiOperation(value = "用户拥有的组织列表")
    @RequestMapping(value = "/selfOrg/{userId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO findSelfOrg(@PathVariable("userId") Long userId) throws BusinessException {
        log.info("用户管理:{}",new Gson().toJson(userId));
        return sysUserService.findSelfOrgIds(userId);
    }

    @ApiOperation(value = "用户权限——菜单列表")
    @GetMapping("/selfMenu/{userId}")
    public ResponseVO findSelfMenu(@PathVariable("userId") Long userId) throws BusinessException {
        log.info("用户管理:{}",userId);
        return sysUserService.findSelfMenu(userId);
    }


    @ApiOperation(value = "用户修改角色")
    @GetMapping("/updRole")
    public ResponseVO updRole(@RequestParam("userId") Long userId, @RequestParam("roleId") Long roleId) throws BusinessException {
        log.info("用户管理:{}",new Gson().toJson(userId));
        log.info("用户管理:{}",new Gson().toJson(roleId));
        return sysUserService.updUserRole(userId,roleId);

    }

    @ApiOperation(value = "用户赋权组织")
    @RequestMapping(value = "/grant", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO grantMenus(@RequestBody GrantOrgDTO dto) throws BusinessException {
        log.info("用户管理:{}",new Gson().toJson(dto));
        return  sysUserService.grantOrgs(dto);
    }

}

