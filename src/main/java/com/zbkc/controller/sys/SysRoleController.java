package com.zbkc.controller.sys;

import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.PageDTO;
import com.zbkc.model.dto.SysRoleDataPermissionDTO;
import com.zbkc.model.dto.WyBasicInfoInDataPermissionDTO;
import com.zbkc.model.po.SysRole;
import com.zbkc.model.po.WyBasicInfo;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.model.vo.WyBasicInfoInDataPermissionVO;
import com.zbkc.service.SysRoleDataPermissionService;
import com.zbkc.service.SysRoleService;
import com.zbkc.service.WyBasicInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 系统角色表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-07-29
 */
@Api(tags = "系统管理-角色管理")
@RestController
@CrossOrigin("*")
@RequestMapping("/sysRole")
@Slf4j
public class SysRoleController {
    @Resource
    private SysRoleService sysRoleService;

    @Autowired
    private WyBasicInfoService wyBasicInfoService;

    @Autowired
    private SysRoleDataPermissionService sysRoleDataPermissionService;

    @ApiOperation(value = "系统角色列表")
    @GetMapping("/list")
    public ResponseVO findAll() throws BusinessException {
        return sysRoleService.selectList(null);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public ResponseVO findPage(@RequestBody PageDTO dto) throws BusinessException {
        log.info("角色管理:{}",new Gson().toJson(dto));
        return sysRoleService.pageByIndex(dto.getP(),dto.getSize(),dto.getIndex());
    }

    @ApiOperation(value = "新增角色")
    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
        public ResponseVO addRole(@RequestBody SysRole sysRole) throws BusinessException {
        log.info("角色管理:{}",new Gson().toJson(sysRole));
        return sysRoleService.insert(sysRole);
    }

    @ApiOperation(value = "修改角色")
    @RequestMapping(value = "/upd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updRole(@RequestBody SysRole data) throws BusinessException {
        log.info("角色管理:{}",new Gson().toJson(data));
        return sysRoleService.updateById(data);
    }

    @ApiOperation(value = "是否禁用")
    @GetMapping("/status")
    public ResponseVO status(@RequestParam("id") Long id ,@RequestParam("status") Boolean status) throws BusinessException {
        log.info("角色管理:{}",new Gson().toJson(id));
        log.info("角色管理:{}",new Gson().toJson(status));
        return sysRoleService.status(id,status);
    }

    @ApiOperation(value = "数据权限-物业列表")
    @PostMapping("/dataPermissionsList")
    public ResponseVO dataPermissionsList(@RequestBody WyBasicInfoInDataPermissionDTO wyBasicInfoInDataPermissionDTO) throws BusinessException {
        log.info("数据权限-物业列表");

        WyBasicInfoInDataPermissionVO wyBasicInfos = this.wyBasicInfoService.selectListInDataPermission(wyBasicInfoInDataPermissionDTO);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , wyBasicInfos);
    }

    @ApiOperation(value = "数据权限-保存信息")
    @PostMapping("/saveDataPermission")
    public ResponseVO saveDataPermission(@RequestBody SysRoleDataPermissionDTO sysRoleDataPermissionDTO) throws BusinessException {
        log.info("数据权限-保存信息:{}",new Gson().toJson(sysRoleDataPermissionDTO));

        if(null == sysRoleDataPermissionDTO.getRoleId() || null == sysRoleDataPermissionDTO.getWyBasicInfoList()){
            return new ResponseVO(ErrorCodeEnum.NULLERROR , ErrorCodeEnum.NULLERROR.getErrMsg());
        }

        Integer record = this.sysRoleDataPermissionService.saveDataPermission(sysRoleDataPermissionDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

}

