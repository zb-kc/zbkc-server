package com.zbkc.controller.sys;


import com.google.gson.Gson;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.GrantMenuDTO;
import com.zbkc.model.dto.MenuDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.SysMenuService;
import com.zbkc.service.SysRoleMenuFuncService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 * 系统角色菜单功能表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-08-02
 */
@Api(tags = "系统管理-权限管理")
@RestController
@CrossOrigin("*")
@Slf4j
@RequestMapping("/sysRoleMenuFunc")
public class SysRoleMenuFuncController {
    @Autowired
    private SysRoleMenuFuncService sysRoleMenuFuncService;
    @Autowired
    private SysMenuService sysMenuService;


    @ApiOperation(value = "所有权限菜单列表")
    @RequestMapping(value = "/list", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO findAll(@RequestBody MenuDTO menuDTO) throws BusinessException {
        return sysMenuService.menusList(menuDTO);
    }

    @ApiOperation(value = "角色拥有的权限菜单列表")
    @RequestMapping(value = "/self/{roleId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO findSelf(@PathVariable("roleId") Long roleId) throws BusinessException {
        log.info("角色管理:{}",new Gson().toJson(roleId));
        return sysRoleMenuFuncService.findSelfMenuIds(roleId);

    }

    @ApiOperation(value = "角色赋权菜单")
    @RequestMapping(value = "/grant", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO grantMenus(@RequestBody GrantMenuDTO grantMenuDTO) throws BusinessException {
        log.info("角色管理:{}",new Gson().toJson(grantMenuDTO));
        return  sysRoleMenuFuncService.grantMenus(grantMenuDTO);
    }

}

