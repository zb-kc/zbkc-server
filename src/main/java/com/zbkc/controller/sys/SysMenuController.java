package com.zbkc.controller.sys;


import com.google.gson.Gson;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.SysMenuMapper;
import com.zbkc.model.dto.MenuDTO;
import com.zbkc.model.po.SysMenu;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.SysMenuService;
import io.lettuce.core.dynamic.annotation.Param;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 系统菜单表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-07-30
 */
@Api(tags = "系统管理-菜单管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/sysMenu")
public class SysMenuController {
    @Autowired
    private SysMenuService sysMenuService;

    @ApiOperation(value = "菜单管理列表")
    @RequestMapping(value = "/list", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO findAll(@RequestBody MenuDTO menuDTO) throws BusinessException {
        log.info("菜单管理:{}",new Gson().toJson(menuDTO));
        return sysMenuService.menusList(menuDTO);
    }

    @ApiOperation(value = "删除菜单")
    @GetMapping("/del/{id}")
    public ResponseVO delMenu(@PathVariable("id") Long id) throws BusinessException {
        log.info("菜单管理:{}",new Gson().toJson(id));
        return sysMenuService.del(id);
    }

    @ApiOperation(value = "修改菜单")
    @RequestMapping(value = "/upd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updMenu(@RequestBody SysMenu data) throws BusinessException {
        log.info("菜单管理:{}",new Gson().toJson(data));
        return sysMenuService.updateById(data);
    }

    @ApiOperation(value = "是否禁用")
    @GetMapping("/status")
    public ResponseVO status(@RequestParam("id") Long id ,@RequestParam("status") Boolean status) throws BusinessException {
        log.info("菜单管理:{}",new Gson().toJson(id));
        log.info("菜单管理:{}",new Gson().toJson(status));
        return sysMenuService.status(id,status);
    }

    @ApiOperation(value = "新增菜单")
    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO addMenu(@RequestBody SysMenu sysMenu) throws BusinessException {
        log.info("菜单管理:{}",new Gson().toJson(sysMenu));
        return sysMenuService.insert(sysMenu);
    }


}

