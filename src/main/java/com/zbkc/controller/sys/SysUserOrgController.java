package com.zbkc.controller.sys;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 *  用户信息控制层
 *  @author gmding
 *  @date 2021-7-28
 * */

@Api(tags = "系统管理-组织管理（暂不用）")
@RestController
@CrossOrigin("*")
@RequestMapping("/sysUserOrg")
@Slf4j
public class SysUserOrgController {
}
