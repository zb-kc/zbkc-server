package com.zbkc.controller.report;

import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.*;
import com.zbkc.model.dto.export.ExportEmptyDTO;
import com.zbkc.model.dto.export.ExportLeaseDTO;
import com.zbkc.model.dto.export.ExportPreContactDTO;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.YyContractService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 运营管理——常用报表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-09-09
 */
@Api(tags = "运营管理-常用报表")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/yyPreContract")
public class YyCommonReportController {

    @Autowired
    private YyContractService yyContractService;

    @ApiOperation(value = "合同预到期列表")
    @PostMapping(value = "/contractList")
    public ResponseVO allPreContract(@RequestBody YjContractDTO data) throws BusinessException {
        log.info("合同预到期列表:{}",new Gson().toJson(data));

        if(null == data.getUserId()){
            return new ResponseVO(ErrorCodeEnum.USERID_NOT_FOUND, ErrorCodeEnum.USERID_NOT_FOUND.getErrMsg());
        }

        return yyContractService.selectPreContract(data);
    }

    @ApiOperation(value = "预到期合同报表导出")
    @PostMapping("/exportPreContract")
    public ResponseVO exportContract(@RequestBody ExportPreContactDTO dto, HttpServletResponse response)  {
        log.info("预到期合同导出:{}",new Gson().toJson(dto));
        return yyContractService.exportPreContract(dto,response);
    }

    @ApiOperation(value = "产业用房复核列表")
    @PostMapping(value = "/industRevieew")
    public ResponseVO industReview(@RequestBody YjContractDTO data) throws BusinessException {
        log.info("产业用房复核:{}",new Gson().toJson(data));
        return yyContractService.selectIndustReview(data);
    }

    @ApiOperation(value = "产业用房报表导出")
    @PostMapping("/exportIndust")
    public ResponseVO exportIndust(@RequestBody ExportPreContactDTO dto, HttpServletResponse response) {
        log.info("产业用房报表导出:{}",new Gson().toJson(dto));
        return yyContractService.exportIndust(dto,response);
    }

    @ApiOperation(value = "租赁明细列表")
    @PostMapping(value = "/leaseDetail")
    public ResponseVO leaseDetail(@RequestBody LeaseDetailDTO data) throws BusinessException {
        log.info("租赁申请:{}",new Gson().toJson(data));
        return yyContractService.leaseDetail(data);
    }

    @ApiOperation(value = "租赁明细报表导出")
    @PostMapping(value = "/exportLease")
    public ResponseVO exportLease(@RequestBody ExportLeaseDTO data, HttpServletResponse response)  {
        log.info("租赁明细报表导出:{}",new Gson().toJson(data));
        return yyContractService.exportLease(data,response);
    }


    @ApiOperation(value = "空置明细列表")
    @PostMapping(value = "/emptyDetail")
    public ResponseVO emptyDetail(@RequestBody EmptyDetailDTO data) throws BusinessException {
        log.info("空置明细:{}",new Gson().toJson(data));

        if(null == data.getUserId()){
            return new ResponseVO(ErrorCodeEnum.USERID_NOT_FOUND, ErrorCodeEnum.USERID_NOT_FOUND.getErrMsg());
        }

        return yyContractService.emptyDetail(data);

    }

    @ApiOperation(value = "空置明细报表导出")
    @PostMapping(value = "/exportEmpty")
    public ResponseVO exportEmpty(@RequestBody ExportEmptyDTO data, HttpServletResponse response)  {
        log.info("空置明细报表导出:{}",new Gson().toJson(data));
        return yyContractService.exportEmpty(data,response);
    }

}
