package com.zbkc.controller.bigkj;

import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.*;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.WyAssetInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

/**
 * <p>
 * 系统用户表
 * </p>
 *
 * @author yangyan
 * @since 2021-07-27
 */
@Api(tags = "空间大屏")
@RestController
@Slf4j
@RequestMapping("/big/wyAssetInfo")
public class BigWyAssetInfoController {

    @Autowired
    private WyAssetInfoService wyAssetInfoService;

    @ApiOperation(value = "首页")
    @GetMapping("/home/info")
    public ResponseVO homeInfo(@RequestParam String type) throws BusinessException {

        return wyAssetInfoService.getHomeInfo(type);
    }


    @ApiOperation(value = "产业用房预到期企业")
    @PostMapping("/cy/ydqlist")
    public ResponseVO ydqList(@RequestBody PageDTO dto) throws BusinessException {

        return wyAssetInfoService.ydqList(dto.getIndex(),dto.getP(),dto.getSize());
    }

    @ApiOperation(value = "产业用房——物业列表")
    @PostMapping("/cy/cywylist")
    public ResponseVO cywyList(@RequestBody CywyDTO dto) throws BusinessException {

        return wyAssetInfoService.cywyList(dto);
    }

    @ApiOperation(value = "首页_五类—物业列表_详情")
    @GetMapping("/wylist/details/{id}")
    public ResponseVO wylistDetails(@PathVariable(value = "id",required=false)String id) throws BusinessException {

        return wyAssetInfoService.wylistDetails(id);
    }

    @ApiOperation(value = "五大类基本信息统计")
    @PostMapping("/cy/wyFivelist/{index}")
    public ResponseVO wyFivelist(@PathVariable(value = "index",required=false)String index) throws BusinessException {

        return wyAssetInfoService.wyFivelist(index);
    }

    @ApiOperation(value = "产业用房统计分析")
    @GetMapping("/cy/analysis")
    public ResponseVO analysis() throws BusinessException {

        return wyAssetInfoService.analysis();
    }

    @ApiOperation(value = "产业用房——重点企业")
    @GetMapping("/cy/zdqy")
    public ResponseVO zdqy() throws BusinessException {

        return wyAssetInfoService.zdqy();
    }

    @ApiOperation(value = "产业用房——重点企业_类别列表")
    @PostMapping("/cy/zdqy/list")
    public ResponseVO zdqyList(@RequestBody CywyDTO dto) throws BusinessException {

        return wyAssetInfoService.zdqyList(dto);
    }

    @ApiOperation(value = "产业用房——预到期详情查看")
    @GetMapping("/qy/details/{code}")
    public ResponseVO details(@PathVariable(value = "code",required=false)String code) throws BusinessException {

        return wyAssetInfoService.details(code);
    }

    @ApiOperation(value = "产业用房——园区_扶持效益分析")
    @GetMapping("/qy/regTax/{id}")
    public ResponseVO regTax(@PathVariable(value = "id",required=false)String id) throws BusinessException {

        return wyAssetInfoService.regTax(id);
    }

    @ApiOperation(value = "首页——产业用房_扶持效益分析")
    @GetMapping("/cy/regTax")
    public ResponseVO cyRegTax() throws BusinessException {

        return wyAssetInfoService.cyReTax();
    }

    @ApiOperation(value = "政府物业——更多分析")
    @GetMapping("/zf/analysis")
    public ResponseVO zfAnalysis() throws BusinessException {

        return wyAssetInfoService.zfAnalysis();
    }

    @ApiOperation(value = "产业用房——园区详情")
    @GetMapping("/cy/details/{id}")
    public ResponseVO cyDetails(@PathVariable(value = "id",required=false)String id) throws BusinessException {

        return wyAssetInfoService.cyDetails(id);
    }

    @ApiOperation(value = "产业用房——园区详情_房屋信息")
    @PostMapping("/cy/details/house/info")
    public ResponseVO cyDetailsHouseInfo(@RequestBody CywyDTO dto) throws BusinessException {
        return wyAssetInfoService.cyDetailsHouseInfo(dto);
    }
    @ApiOperation(value = "产业用房——园区详情_空置信息")
    @PostMapping("/cy/details/kzDetailsList")
    public ResponseVO KzDetailsList(@RequestBody CywyDTO dto) throws BusinessException {
        return wyAssetInfoService.KzDetailsList(dto);
    }

    @ApiOperation(value = "产业用房——园区详情_空置信息2")
    @PostMapping("/cy/details/kzDetailsList2")
    public ResponseVO KzDetailsList2(@RequestBody CywyDTO dto) throws BusinessException {
        return wyAssetInfoService.KzDetailsList2(dto);
    }

    @ApiOperation(value = "产业用房——园区详情_更多分析_总统分析")
    @GetMapping("/cy/details/more/analysis/{id}")
    public ResponseVO moreAnalysis(@PathVariable(value = "id",required=false)String id) throws BusinessException {
        return wyAssetInfoService.MoreAnalysis(id);
    }

    @ApiOperation(value = "产业用房——园区详情_入驻企业分析")
    @GetMapping("/cy/details/rzDetailsList/{id}")
    public ResponseVO RzDetailsList(@PathVariable(value = "id",required=false)String id) throws BusinessException {
        return wyAssetInfoService.RzDetailsList(id);
    }

    @ApiOperation(value = "产业用房——园区详情_入驻企业列表")
    @PostMapping("/cy/details/rzqyList")
    public ResponseVO RzQyDetailsList(@RequestBody CywyDTO dto) throws BusinessException {
        return wyAssetInfoService.RzQyDetailsList(dto);
    }


    @ApiOperation(value = "五类——物业统计")
    @GetMapping("/cy/wy/{index}")
    public ResponseVO cyWy(@PathVariable(value = "index",required=false)String index , @Param("p") Integer p , @Param("size") Integer size , @Param("sort") Integer sort) throws BusinessException {

        return wyAssetInfoService.cyWy(index , p , size , sort);
    }


    @ApiOperation(value = "租赁社会物业——基本信息")
    @GetMapping("/zl/basic/info")
    public ResponseVO basicInfo() throws BusinessException {

        return wyAssetInfoService.basicInfo();
    }


    @ApiOperation(value = "租赁社会物业——物业列表")
    @PostMapping("/zl/wy/list")
    public ResponseVO zlAnalysis(@RequestBody CywyDTO dto) throws BusinessException {

        return wyAssetInfoService.zlAnalysis(dto);
    }


    @ApiOperation(value = "租赁社会物业——更多分析")
    @GetMapping("/zl/more/analysis")
    public ResponseVO moreAnalysis() throws BusinessException {

        return wyAssetInfoService.moreAnalysis();
    }

    @ApiOperation(value = "租赁社会物业——更多分析_单位面积统计")
    @GetMapping("/zl/more/unit/{index}")
    public ResponseVO moreUnitAnalysis(@PathVariable(value = "index",required=false)String index) throws BusinessException {

        return wyAssetInfoService.moreUnit(index);
    }


    @ApiOperation(value = "地图——街道坐标")
    @GetMapping("/map/street/{index}")
    public ResponseVO mapStreet(@PathVariable(value = "index",required=false)String index) throws BusinessException {
        return wyAssetInfoService.mapStreet(index);
    }
    @ApiOperation(value = "地图——产业用房_物业坐标")
    @GetMapping("/map/cyStreet")
    public ResponseVO cyStreet() throws BusinessException {
        return wyAssetInfoService.cyStreet();
    }

    @ApiOperation(value = "首页—核查任务")
    @GetMapping("/house/code")
    public ResponseVO houseCode() throws BusinessException {
        return wyAssetInfoService.houseCode();
    }

    @ApiOperation(value = "首页—核查任务列表")
    @PostMapping("/house/codeList")
    public ResponseVO houseCodeList(@RequestBody HouseCodeDTO dto) throws BusinessException {
        return wyAssetInfoService.houseCodeList(dto);
    }


    @ApiOperation(value = "首页—物业资产")
    @PostMapping("/home/wy/assetsList")
    public ResponseVO wyAssetsList(@RequestBody WyAssetsListDTO dto) throws BusinessException {
        return wyAssetInfoService.wyAssetsList(dto);
    }


    @ApiOperation(value = "首页—智能选址_街道名称列表")
    @GetMapping("/home/wy/streetName")
    public ResponseVO streetNameList() throws BusinessException {
        return wyAssetInfoService.streetNameList();
    }

    @ApiOperation(value = "首页—智能选址_社区名称列表")
    @GetMapping("/home/wy/communityName")
    public ResponseVO communityNameList() throws BusinessException {
        return wyAssetInfoService.communityNameList();
    }

    @ApiOperation(value = "首页—智能选址_物业种类列表")
    @GetMapping("/home/wy/wyTypeList")
    public ResponseVO wyTypeList() throws BusinessException {
        return wyAssetInfoService.wyTypeList();
    }


    @ApiOperation(value = "首页—智能选址_物业查询列表")
    @PostMapping("/home/wy/list")
    public ResponseVO wyList(@RequestBody WyListDTO dto) throws BusinessException {
        return wyAssetInfoService.wyList(dto);
    }


    @ApiOperation(value = "首页—产业用房_用房专业分析")
    @PostMapping("/home/wy/analysis")
    public ResponseVO wyAnalysis(@RequestBody wyAnalysisDTO dto) throws BusinessException {
        return wyAssetInfoService.wyAnalysis(dto);
    }

    @ApiOperation(value = "空间资源大屏_新入住企业")
    @PostMapping("/newrzqy")
    public ResponseVO newrzqy(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.newrzqy(dto);
    }

    @ApiOperation(value = "空间资源大屏_新入住企业分析")
    @PostMapping("/newrzqyfx")
    public ResponseVO newrzqyfx(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.newrzqyfx(dto);
    }

    @ApiOperation(value = "空间资源大屏_预到期合同列表")
    @PostMapping("/newdqht")
    public ResponseVO newdqht(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.newdqht(dto);
    }

    @ApiOperation(value = "空间资源大屏_园区行业")
    @PostMapping("/newyqhy")
    public ResponseVO newyqhy(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.newyqhy(dto);
    }

    @ApiOperation(value = "空间资源大屏_资产入账")
    @PostMapping("/newwyentry")
    public ResponseVO newwyentry(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.newwyentry(dto);
    }

    @ApiOperation(value = "空间资源大屏_产权登记")
    @PostMapping("/newwyequity")
    public ResponseVO newwyequity(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.newwyequity(dto);
    }

    @ApiOperation(value = "空间资源大屏_所有租期时间长的合同-产业用房")
    @PostMapping("/allLongContract")
    public ResponseVO allLongContract(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.allLongContract(dto);
    }

    @ApiOperation(value = "空间资源大屏_所有租期时间长的合同-非产业用房")
    @PostMapping("/allNotCyLongContract")
    public ResponseVO allNotCyLongContract(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.allNotCyLongContract(dto);
    }

    @ApiOperation(value = "空间资源大屏_所有预到期的合同-产业用房")
    @PostMapping("/alldelayContract")
    public ResponseVO alldelayContract(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.alldelayContract(dto);
    }

    @ApiOperation(value = "空间资源大屏_所有预到期的合同-非产业用房")
    @PostMapping("/allNotCyDelayContract")
    public ResponseVO allNotCyDelayContract(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.allNotCyDelayContract(dto);
    }

    @ApiOperation(value = "空间资源大屏_根据顶级园区名取子集")
    @PostMapping("/selectChildrenByTopWyName")
    public ResponseVO selectChildrenByTopWyName(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.selectChildrenByTopWyName(dto);
    }

    @ApiOperation(value = "空间资源大屏_根据资产类型取所有物业")
    @PostMapping("/selectWySimpleNameByAssetType")
    public ResponseVO selectWySimpleNameByAssetType(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.selectWySimpleNameByAssetType(dto);
    }

    @ApiOperation(value = "空间资源大屏_查询总资产信息")
    @PostMapping("/selectAllWyInfo")
    public ResponseVO selectAllWyInfo(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.selectAllWyInfo(dto);
    }

    @ApiOperation(value = "空间资源大屏_未入账列表")
    @PostMapping("/notEntryWyInfo")
    public ResponseVO notEntryWyInfo(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.notEntryWyInfo(dto);
    }

    @ApiOperation(value = "空间资源大屏_未产权登记列表")
    @PostMapping("/notCertHasWyInfo")
    public ResponseVO notCertHasWyInfo(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.notCertHasWyInfo(dto);
    }

    @ApiOperation(value = "空间资源大屏_根据顶级物业查看楼栋")
    @PostMapping("/showLDInfo")
    public ResponseVO showLDInfo(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.showLDInfo(dto);
    }

    @ApiOperation(value = "空间资源大屏_根据楼栋名查看入驻空置信息")
    @PostMapping("/showLDEntryAndEmptyInfo")
    public ResponseVO showLDEntryAndEmptyInfo(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.showLDEntryAndEmptyInfo(dto);
    }

    @ApiOperation(value = "空间资源大屏_查看楼栋入驻企业-分页查询")
    @PostMapping("/showLDEneterInfo")
    public ResponseVO showLDEneterInfo(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.showLDEnterInfo(dto);
    }

    @ApiOperation(value = "空间资源大屏_查看楼栋空置-分页查询")
    @PostMapping("/showLDEmptyInfo")
    public ResponseVO showLDEmptyInfo(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.showLDEmptyInfo(dto);
    }

    @ApiOperation(value = "空间资源大屏——空置信息非产业用房")
    @PostMapping("/emptyNotCyList")
    public ResponseVO emptyNotCyList(@RequestBody CywyDTO dto) throws BusinessException {
        return wyAssetInfoService.emptyNotCyList(dto);
    }

    @ApiOperation(value = "空间资源大屏_根据公司名称查看入驻信息")
    @PostMapping("/showWyInfoByCompanyName")
    public ResponseVO showWyInfoByCompanyName(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.showWyInfoByCompanyName(dto);
    }


    @ApiOperation(value = "空间资源大屏_根据管理单位查询物业信息")
    @PostMapping("/selectWyInfoByManUnit")
    public ResponseVO selectWyInfoByManUnit(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.selectWyInfoByManUnit(dto);
    }

    @ApiOperation(value = "空间资源大屏_国企物业信息")
    @PostMapping("/selectWyInfoGuoqi")
    public ResponseVO selectWyInfoGuoqi(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.selectWyInfoGuoqi(dto);
    }

    @ApiOperation(value = "空间资源大屏_根据园区名查询园区相关信息（仅限国企物业）")
    @PostMapping("/selectWyDetailInfoGuoqi")
    public ResponseVO selectWyDetailInfoGuoqi(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.selectWyDetailInfoGuoqi(dto);
    }

    @ApiOperation(value = "空间资源大屏_根据园区名查询园区使用状态信息（仅限国企物业）")
    @PostMapping("/selectWyInfoGuoqiByUseStatus")
    public ResponseVO selectWyInfoGuoqiByUseStatus(@RequestBody CywyDTO dto) throws BusinessException, ParseException {
        return wyAssetInfoService.selectWyInfoGuoqiByUseStatus(dto);
    }

    @ApiOperation(value = "入驻企业列表")
    @PostMapping("/enterpriseListByBelongType")
    public ResponseVO enterpriseByBelongType(@RequestBody  EnterpriseDTO dto) throws BusinessException {

        return wyAssetInfoService.enterpriseByBelongType(dto);
    }

    @ApiOperation(value = "企业转租分租情况列表")
    @PostMapping("/company/subletLease")
    public ResponseVO subletLeaseList(@RequestBody PageDTO dto) throws BusinessException {
        return wyAssetInfoService.subletLeaseList(dto.getIndex(),dto.getP(),dto.getSize());
    }

    @ApiOperation(value = "企业获奖信息列表")
    @PostMapping("/company/reward")
    public ResponseVO rewardList(@RequestBody PageDTO dto) throws BusinessException {
        return wyAssetInfoService.rewardList(dto.getIndex(),dto.getP(),dto.getSize());
    }

    @ApiOperation(value = "企业证书列表")
    @PostMapping("/company/certificate")
    public ResponseVO certificateList(@RequestBody PageDTO dto) throws BusinessException {
        return wyAssetInfoService.certificateList(dto.getIndex(),dto.getP(),dto.getSize());
    }

    @ApiOperation(value = "企业专利列表")
    @PostMapping("/company/patent")
    public ResponseVO patentList(@RequestBody PageDTO dto) throws BusinessException {
        return wyAssetInfoService.patentList(dto.getIndex(),dto.getP(),dto.getSize());
    }

    @ApiOperation(value = "企业软件著作权列表")
    @PostMapping("/company/softwarecopyright")
    public ResponseVO softWareCopyRightList(@RequestBody PageDTO dto) throws BusinessException {
        return wyAssetInfoService.softWareCopyRightList(dto.getIndex(),dto.getP(),dto.getSize());
    }
}
