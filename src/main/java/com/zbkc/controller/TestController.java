package com.zbkc.controller;

import com.zbkc.common.utils.DateUtil;
import com.zbkc.common.utils.RedisUtil;
import com.zbkc.common.utils.ToolUtils;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.vo.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = "测试")
@Controller
@RequestMapping("/test")
@Slf4j
public class TestController {

    @Resource
    private RedisUtil redisUtil;
    @Resource
    private ToolUtils toolUtils;

    @ApiOperation(value = "测试")
    @RequestMapping(value = "/text", method = RequestMethod.GET)
    @ResponseBody
    public ResponseVO text()throws BusinessException {

        String code="NS"+new DateUtil().getYear()+new DateUtil().getMonth();
        String num= (String) redisUtil.get( code );

        log.info( "编码:{}",num);
        log.info( "编码:{}",toolUtils.getPropertyCode());
        return new ResponseVO();
    }



}
