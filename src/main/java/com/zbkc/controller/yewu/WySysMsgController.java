package com.zbkc.controller.yewu;


import com.google.gson.Gson;
import com.zbkc.configure.BusinessException;
import com.zbkc.model.dto.export.ExportSmsTemplateDTO;
import com.zbkc.model.dto.WySysMsgHomeDTO;
import com.zbkc.model.po.WySysMsg;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.WySysMsgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 短信模板管理表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-09-11
 */
@Api(tags = "业务办理-短信管理")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/wySysMsg")
public class WySysMsgController {
    @Autowired
    private WySysMsgService wySysMsgService;

    @ApiOperation(value = "新增短信模板")
    @PostMapping(value = "/addMsg")
    public ResponseVO addMsg(@RequestBody WySysMsg data) throws BusinessException {
        log.info("新增短信模板:{}",new Gson().toJson(data));
               return wySysMsgService.addMsg(data);

    }

    @ApiOperation(value = "首页查询短信模板列表")
    @PostMapping("/list")
    public ResponseVO getList(@RequestBody WySysMsgHomeDTO data) throws BusinessException {
        log.info("查看短信模板:{}",new Gson().toJson(data));
        return wySysMsgService.getList( data );

    }

    @ApiOperation(value = "查看当前行的数据")
    @GetMapping("/row/{id}")
    public ResponseVO seeRow(@PathVariable("id") String id) throws BusinessException {
        log.info("短信模板:{}",new Gson().toJson(id));
        return wySysMsgService.seeRow(id);


    }

    @ApiOperation(value = "修改短信模板")
    @RequestMapping(value = "/upd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseVO updMsg(@RequestBody WySysMsg data) throws BusinessException {
        log.info("修改短信模板:{}", new Gson().toJson(data));
        return wySysMsgService.updMsg(data);
    }

    @ApiOperation(value = "导出短信模板列表")
    @RequestMapping(value = "/template/export", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,produces = "text/html;charset=UTF-8")
    public ResponseVO export(@RequestBody ExportSmsTemplateDTO data, HttpServletResponse response) throws BusinessException {
        log.info("导出短信模板:{}",new Gson().toJson(data));
        return wySysMsgService.export(data,response);
    }

}

