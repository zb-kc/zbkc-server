package com.zbkc.controller.yewu;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.google.gson.Gson;
import com.zbkc.common.enums.ErrorCodeEnum;
import com.zbkc.common.utils.CreateFileUtil;
import com.zbkc.configure.BusinessException;
import com.zbkc.mapper.YyContractMapper;
import com.zbkc.model.dto.*;
import com.zbkc.model.po.*;
import com.zbkc.model.vo.*;
import com.zbkc.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.*;

/**
 *
 * 业务办理-控制器
 *
 * @author ZB3436.xiongshibao
 * @since 2021-9-16
 */
@Api(tags = "业务办理-所有业务")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/ywBusiness")
public class YwBusinessController {

    @Autowired
    private ActivitiBaseService activitiBaseService;

    @Autowired
    private YwAgencyBusinessService ywAgencyBusinessService;

    @Autowired
    private YwHandoverDetailsSerivce ywHandoverDetailsSerivce;

    @Autowired
    private YwDevInfoService ywDevInfoService;

    @Autowired
    private YyContractService yyContractService;

    @Autowired
    private YwMaterialApplicationService ywMaterialApplicationService;

    @javax.annotation.Resource
    private CreateFileUtil createFileUtil;

    /**
     * 增加移交用房申请信息-关联工作流
     */
    @ApiOperation("业务办理-移交用房申请/物业资产申报-新增")
    @PostMapping("/yw/agencyApply/add")
    public ResponseVO addAgencyApply(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{
        log.info("业务办理-开发商基本信息-新增:{}",new Gson().toJson(ywAgencyBusinessDTO));

        YwAgencyBusinessPO ywAgencyBusinessPO = new YwAgencyBusinessPO();
        BeanUtils.copyProperties(ywAgencyBusinessDTO, ywAgencyBusinessPO);

        ywAgencyBusinessPO.setId(IdWorker.getId());
        ywAgencyBusinessPO.setTableId(ywAgencyBusinessPO.getId().toString());
        ywAgencyBusinessPO.setApplicationTime(new Timestamp(System.currentTimeMillis()));
        ywAgencyBusinessPO.setCreateTime(new Date());
        //设置受理号
        this.ywAgencyBusinessService.generateAcceptCode(ywAgencyBusinessPO);
        //生成附件列表
        this.ywHandoverDetailsSerivce.generateHandoverDetailsList(ywAgencyBusinessPO);

        this.activitiBaseService.addAgencyBusinessRecord(ywAgencyBusinessPO);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , ywAgencyBusinessPO);
    }

    @ApiOperation("业务办理-开发商基本信息-新增")
    @PostMapping("/yw/devInfo/add")
    public ResponseVO addDevInfo(@RequestBody YwDevInfoDTO ywDevInfoDTO) throws BusinessException{
        log.info("业务办理-开发商基本信息-新增:{}",new Gson().toJson(ywDevInfoDTO));
        YwDevInfoPO ywDevInfoPO = new YwDevInfoPO();
        BeanUtils.copyProperties(ywDevInfoDTO, ywDevInfoPO);

        //新增申请记录
        int id = this.ywDevInfoService.addYwDevInfo(ywDevInfoPO);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , id);
    }

    @ApiOperation("业务办理-所有列表")
    @PostMapping("/yw/list")
    public ResponseVO allList(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{

        log.info("业务办理-所有列表:{}",new Gson().toJson(ywAgencyBusinessDTO));

        if(ywAgencyBusinessDTO.getUserId() < 1){
            return new ResponseVO(ErrorCodeEnum.USERID_NOT_FOUND , ErrorCodeEnum.USERID_NOT_FOUND.getErrMsg());
        }

        PageVO<YwAgencyBusinessPO> data = this.ywAgencyBusinessService.pageYwAgencyBusinessList(ywAgencyBusinessDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , data);
    }

    @ApiOperation("业务办理-导出")
    @PostMapping(value = "/yw/export" )
    public void export(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO , HttpServletResponse response) throws BusinessException{

        log.info("业务办理-导出:{}",new Gson().toJson(ywAgencyBusinessDTO));
        this.ywAgencyBusinessService.export(ywAgencyBusinessDTO, response);
    }

    @ApiOperation("业务办理-流转信息")
    @GetMapping("/yw/processInfo")
    public ResponseVO processInfo(@RequestParam("businessId") String businessId) throws BusinessException{
        log.info("业务办理-流转信息-—待办业务表id:{}",new Gson().toJson(businessId));
        List<YwProcessInfoVO> processInfoVOList = this.activitiBaseService.findProcessAllStatusByBusinessKey(businessId);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , processInfoVOList);
    }

    @ApiOperation("业务办理-移交用房申请-修改")
    @PostMapping("/yw/agencyApply/update")
    public ResponseVO agencyApplyUpdate(@RequestBody YwAgencyBusinessDTO ywAgencyBusinessDTO) throws BusinessException{
        log.info("业务办理-修改:{}",new Gson().toJson(ywAgencyBusinessDTO));
        int id = this.ywAgencyBusinessService.updateYwAgencyBusiness(ywAgencyBusinessDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , id);
    }

    @ApiOperation("业务办理-下载-移交用房呈批表")
    @GetMapping("/yw/agencyApply/table")
    public void agencyApplyTable(HttpServletResponse response , @RequestParam("businessId") String businessId) throws BusinessException{
        log.info("业务办理-移交用房呈批表-—待办业务表id:{}",new Gson().toJson(businessId));

        this.ywAgencyBusinessService.downloadAgencyApplyTable(response , businessId);
    }

    @ApiOperation("业务办理-下载-物业租赁合同呈批表")
    @GetMapping("/yw/signApplication/table")
    public ResponseVO signApplicationTable(HttpServletResponse response , @RequestParam("businessId") String businessId) throws BusinessException{

        log.info("业务办理-物业租赁合同呈批表-—待办业务表id:{}",new Gson().toJson(businessId));

        YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessService.selectOne(businessId);
        if(null == ywAgencyBusinessPO){
            return new ResponseVO(ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND , ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND.getErrMsg());
        }

        this.ywAgencyBusinessService.downloadSignApplicationTable(response , businessId , ywAgencyBusinessPO);
        return null;
    }

    @ApiOperation("业务办理-终止流程")
    @PostMapping("/yw/endTask")
    public ResponseVO endTask(@RequestBody YwProcessInfoVO ywProcessInfoVO) throws BusinessException {
        log.info("业务办理-终止流程:{}",new Gson().toJson(ywProcessInfoVO));

        ResponseVO check = this.ywAgencyBusinessService.checkApplyStatus(ywProcessInfoVO.getBusinessId());
        if(null != check){
            return check;
        }

        this.activitiBaseService.terminateProcessInstance(ywProcessInfoVO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , ywProcessInfoVO);
    }

    @ApiOperation("业务办理-材料补正")
    @PostMapping("/yw/supplementData")
    public ResponseVO supplementData(@RequestBody Map<String , String> opinionMap) throws BusinessException{

        log.info("业务办理-材料补正——业务办理——附件材料审核状态表ids及对应意见:{}",new Gson().toJson(opinionMap));

        ResponseVO responseVO = this.ywAgencyBusinessService.checkApplyStatus(opinionMap.get("businessId"));
        if(null != responseVO){
            return responseVO;
        }

        return this.ywAgencyBusinessService.supplementData(opinionMap);
    }

    @ApiOperation("业务办理-获取流程进度图")
    @GetMapping("/yw/getProcessImg")
    public ResponseVO getProcessImg(@RequestParam String businessId) throws BusinessException {
        log.info("业务办理-获取流程进度图-—待办业务表id:{}",new Gson().toJson(businessId));
        String processImg = this.activitiBaseService.getProcessImg(businessId);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , processImg);
    }

    @ApiOperation("业务办理-审核通过")
    @PostMapping("/yw/completeTask")
    public ResponseVO completeTask(@RequestBody YwContractDTO ywContractDTO) throws BusinessException{
        log.info("业务办理-审核通过——待办业务表id及审核意见:{}",new Gson().toJson(ywContractDTO));

        ResponseVO check = this.ywAgencyBusinessService.checkApplyStatus(ywContractDTO.getBusinessId());
        if(null != check){
            return check;
        }

        int record = this.activitiBaseService.completeTask(ywContractDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @ApiOperation("业务办理-驳回")
    @GetMapping("/yw/turndownTask")
    public ResponseVO turndownTask(@RequestParam String businessId , String opinion) throws BusinessException {
        log.info("业务办理-驳回——待办业务表id及驳回意见:{}",new Gson().toJson(businessId +","+opinion));

        ResponseVO check = this.ywAgencyBusinessService.checkApplyStatus(businessId);
        if(null != check){
            return check;
        }

        ResponseVO responseVO = this.activitiBaseService.turnDownTaskInstance(businessId, opinion);
        if(null != responseVO){
            return responseVO;
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS , null);
    }

    @ApiOperation("业务办理-办理")
    @GetMapping("/yw/dealWith")
    public ResponseVO dealWith(@RequestParam String businessId) throws BusinessException {

        log.info("业务办理-办理-业务办理——待办业务表id:{}",new Gson().toJson(businessId));

        ResponseVO responseVO = this.ywAgencyBusinessService.checkApplyStatus(businessId);
        if(null != responseVO){
            return responseVO;
        }

        return this.ywAgencyBusinessService.dealWith(businessId);
    }

    @ApiOperation("业务办理-材料通过")
    @GetMapping("/yw/dataPass")
    public ResponseVO dataPass(@RequestParam String handoverDetailsId) throws BusinessException {
        log.info("业务办理-材料通过-业务办理——附件材料审核状态表id:{}",new Gson().toJson(handoverDetailsId));

        ResponseVO check = this.ywAgencyBusinessService.checkApplyStatus(handoverDetailsId);
        if(null != check){
            return check;
        }

        int rowId = this.ywHandoverDetailsSerivce.dataPass(handoverDetailsId);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , rowId);
    }

    @ApiOperation("业务办理-物业资产申报-申请材料列表")
    @PostMapping("/yw/applyData/list")
    public ResponseVO applyDataList(@RequestBody YwMaterialApplicationDTO ywMaterialApplicationDTO) throws BusinessException {
        log.info("业务办理-物业资产申报-申请材料列表:{}",new Gson().toJson(ywMaterialApplicationDTO));
        PageVO<YwMaterialApplicationPO> ywMaterialApplicationPOPageVO = this.ywMaterialApplicationService.pageYwAgencyBusinessList(ywMaterialApplicationDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , ywMaterialApplicationPOPageVO);
    }

    @ApiOperation("业务办理-物业资产申报-申请材料列表-新增")
    @PostMapping("/yw/applyData/insert")
    public ResponseVO applyDataInsert(@RequestBody YwMaterialApplicationDTO ywMaterialApplicationDTO) throws BusinessException {
        log.info("业务办理-物业资产申报-申请材料列表-新增:{}",new Gson().toJson(ywMaterialApplicationDTO));
        int i = this.ywMaterialApplicationService.insertMaterialApplication(ywMaterialApplicationDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , i);
    }

    @ApiOperation("业务办理-附件打包下载")
    @GetMapping("/yw/downloadBatchByFile")
    public void downloadBatchByFile(@RequestParam String downloadFileIds,HttpServletResponse response) throws BusinessException {
        log.info("业务办理-附件打包下载-业务办理——附件材料审核状态表ids:{}",new Gson().toJson(downloadFileIds));
        this.ywAgencyBusinessService.downloadBatchByFile(response , downloadFileIds);
    }

    @ApiOperation("业务办理-文件上传(申请流程中)")
    @PostMapping("/yw/fileUpload")
    public ResponseVO applyDataInsert(@RequestBody MultipartFile file ,@RequestParam String ywHandlerDetailsId) throws BusinessException {
        log.info("业务办理-文件上传(申请流程中)-业务办理——附件材料审核状态表id:{}",new Gson().toJson(ywHandlerDetailsId));

        ResponseVO check = this.ywAgencyBusinessService.checkApplyStatus(ywHandlerDetailsId);
        if(null != check){
            return check;
        }

        //保存文件
        FileVo vo = this.createFileUtil.saveFile(file);

        //保存文件路径
        SysFilePath1 sysFilePath1 = this.ywAgencyBusinessService.saveSysFilePath(vo , ywHandlerDetailsId);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , sysFilePath1);
    }

    @ApiOperation("业务办理-文件下载-单个文件")
    @GetMapping("/yw/downloadFileOne")
    public void downloadFileOne(HttpServletResponse response, @RequestParam String ywHandlerDetailsId) throws BusinessException {
        log.info("业务办理-文件下载-单个文件-业务办理——附件材料审核状态表id:{}",new Gson().toJson(ywHandlerDetailsId));

        this.ywHandoverDetailsSerivce.downloadFileOne(response , ywHandlerDetailsId);
    }

    @ApiOperation("业务办理-终止流程-测试用")
    @GetMapping("/yw/deleteProcess")
    public ResponseVO deleteProcess(String businessId , String reason) throws BusinessException{
        log.info("业务办理-终止流程-测试用-待办业务表id:{}",new Gson().toJson(businessId));
        this.activitiBaseService.deleteProcess(businessId , reason);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , businessId);
    }

    @ApiOperation("业务办理-下载租赁交款通知单-待修改")
    @GetMapping("/yw/downLoadPaymentNotice")
    public ResponseVO downLoadPaymentNotice(HttpServletResponse response , @RequestParam("businessId") String businessId) throws BusinessException {

        log.info("业务办理-下载租赁交款通知-—待办业务表id:{}",new Gson().toJson(businessId));

        YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessService.selectOne(businessId);

        if(null == ywAgencyBusinessPO){
            return new ResponseVO(ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND , ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND.getErrMsg());
        }

        this.ywAgencyBusinessService.downLoadPaymentNotice(response , businessId);

        return null;
    }

    @ApiOperation("业务办理-下载房屋租赁合同-待修改")
    @GetMapping("/yw/downLoadContactTemplate")
    public ResponseVO downLoadContactTemplate(HttpServletResponse response , @RequestParam("businessId") String businessId) throws BusinessException {

        log.info("业务办理-下载房屋租赁合同-—待办业务表id:{}",new Gson().toJson(businessId));

        YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessService.selectOne(businessId);

        if(null == ywAgencyBusinessPO){
            return new ResponseVO(ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND , ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND.getErrMsg());
        }

        this.ywAgencyBusinessService.downLoadContactTemplate(response , businessId);

        return null;
    }

    @ApiOperation("业务办理-按钮-查看原合同")
    @PostMapping("/yw/showOldContract")
    public ResponseVO showOldContract(String businessId) throws BusinessException{
        log.info("业务办理-按钮-查看原合同-待办业务表id:{}",new Gson().toJson(businessId));

        ResponseVO check = this.ywAgencyBusinessService.checkApplyStatus(businessId);
        if(null != check){
            return check;
        }

        YwContractVO ywContractVO = this.ywAgencyBusinessService.showOldContract(businessId);
        if(null == ywContractVO){
            return new ResponseVO(ErrorCodeEnum.CONTRACT_NOT_FOUND , ErrorCodeEnum.CONTRACT_NOT_FOUND.getErrMsg());
        }
        return new ResponseVO(ErrorCodeEnum.SUCCESS , ywContractVO);
    }

    @ApiOperation("业务办理-查看-物业租赁合同呈批表")
    @GetMapping("/yw/signApplication/table/show")
    public ResponseVO showSignApplicationTable(String businessId) throws BusinessException{
        log.info("业务办理-查看-物业租赁合同呈批表—待办业务表id:{}",new Gson().toJson(businessId));

        YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessService.selectOne(businessId);

        if(null == ywAgencyBusinessPO){
            return new ResponseVO(ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND , ErrorCodeEnum.AGENCY_APPLICATION_NOT_FOUND.getErrMsg());
        }

        Map<String, String> testMap = this.ywAgencyBusinessService.getWordInfoMap(businessId, ywAgencyBusinessPO);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , testMap);
    }

    @ApiOperation("业务办理-查看-移交用房呈批表")
    @GetMapping("/yw/agencyApply/table/show")
    public ResponseVO showAgencyApply(String businessId) throws BusinessException{
        log.info("业务办理-按钮-移交用房呈批表—待办业务表id:{}",new Gson().toJson(businessId));

        YwAgencyBusinessPO ywAgencyBusinessPO = this.ywAgencyBusinessService.selectOne(businessId);

        Map<String, Object> testMap = new HashMap<>();
        testMap.put("proName", ywAgencyBusinessPO.getProName());
        testMap.put("applicantUnit", ywAgencyBusinessPO.getApplicantUnit());
        testMap.put("stageName", ywAgencyBusinessPO.getStageName());

        List<String[]> testList = new ArrayList<>();
        this.ywAgencyBusinessService.getProcessInfoList(businessId, testList);

        testMap.put("processList" , testList );

        return new ResponseVO(ErrorCodeEnum.SUCCESS , testMap);
    }

    @ApiOperation("业务办理-删除-合同附件")
    @GetMapping("/yw/deleteContractFile")
    public ResponseVO deleteContractFile(@RequestParam("fileId") String fileId) throws BusinessException{
        log.info("业务办理-删除-合同附件—文件id:{}",new Gson().toJson(fileId));

        int record = this.yyContractService.deleteContractFile(fileId);

        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @ApiOperation("业务办理-合同签署申请-选择物业-保存信息")
    @GetMapping("/yw/chooseWy")
    public ResponseVO chooseWy(@RequestParam("businessId") String businessId,@RequestParam("wyIds") String wyIds) throws BusinessException{
        log.info("业务办理-合同签署申请-选择物业—物业ids:{}",new Gson().toJson(businessId + ","+wyIds));

        if(StringUtils.isEmpty(businessId)){
            return new ResponseVO(ErrorCodeEnum.NULLERROR , ErrorCodeEnum.NULLERROR.getErrMsg());
        }

        int record = this.yyContractService.chooseWy(businessId, wyIds);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @Autowired
    private YyContractMapper yyContractMapper;

    @ApiOperation("业务办理-【测试用】合同信息-新增")
    @PostMapping("/yw/addContract")
    public ResponseVO addContract(@RequestBody YyContract yyContract) throws BusinessException{
        log.info("业务办理-【测试用】合同信息-新增:{}",new Gson().toJson(yyContract));

        int insert = this.yyContractMapper.insert(yyContract);

        return new ResponseVO(ErrorCodeEnum.SUCCESS ,insert);
    }

    @ApiOperation("业务办理-审核中-保存合同信息")
    @PostMapping("/yw/saveContract")
    public ResponseVO saveContract(@RequestBody YwContractDTO ywContractDTO) throws BusinessException{
        log.info("业务办理-审核中-保存合同信息:{}",new Gson().toJson(ywContractDTO));
        int record = this.ywAgencyBusinessService.updateYyContractInProgress(ywContractDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , record);
    }

    @ApiOperation("业务办理-清除所有工作流")
    @GetMapping("/yw/deleteAllProgress")
    public ResponseVO deleteAllProgress() throws BusinessException {
        log.info("业务办理-清除所有工作流");
        this.activitiBaseService.deleteAllProgress();
        return new ResponseVO(ErrorCodeEnum.SUCCESS , null);
    }

    @ApiOperation("业务办理-附件材料审核状态列表")
    @PostMapping("/yw/getHandoverDetails")
    public ResponseVO selectYwHandoverDetailsByBusinessId(@RequestBody YwContractDTO ywContractDTO) throws BusinessException {
        log.info("业务办理-附件材料审核状态列表:{}",new Gson().toJson(ywContractDTO));

        if (StringUtils.isEmpty(ywContractDTO.getBusinessId())) {
            return new ResponseVO(ErrorCodeEnum.BUSINESS_ID_ERROR , ErrorCodeEnum.BUSINESS_ID_ERROR.getErrMsg());
        }

        List<YwHandoverDetailsDTO> ywHandoverDetailsDTOList = this.ywAgencyBusinessService.selectYwHandoverDetailsByBusinessId(ywContractDTO);
        return new ResponseVO(ErrorCodeEnum.SUCCESS , ywHandoverDetailsDTOList);
    }

    @ApiOperation("业务办理-材料补正-保存信息")
    @PostMapping("/yw/saveSupplementData")
    public ResponseVO saveSupplementData(@RequestBody Map<String , String> opinionMap) throws BusinessException {

        log.info("业务办理-材料补正-保存信息:{}",new Gson().toJson(opinionMap));

        if(opinionMap.isEmpty()){
            return new ResponseVO(ErrorCodeEnum.NULLERROR , ErrorCodeEnum.NULLERROR.getErrMsg());
        }

        return this.ywAgencyBusinessService.saveSupplementData(opinionMap);
    }

    @ApiOperation("业务办理-办理-前置处理")
    @GetMapping("/yw/dealWithBefore")
    public ResponseVO dealWithBefore(@RequestParam String businessId) throws BusinessException {

        log.info("业务办理-办理-前置处理——待办业务表id:{}",new Gson().toJson(businessId));

        ResponseVO responseVO = this.ywAgencyBusinessService.checkApplyStatus(businessId);
        if(null != responseVO){
            return responseVO;
        }

        return this.ywAgencyBusinessService.dealWithBefore(businessId);
    }

}
