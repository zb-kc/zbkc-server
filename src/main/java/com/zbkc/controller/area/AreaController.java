package com.zbkc.controller.area;


import com.zbkc.configure.BusinessException;
import com.zbkc.model.vo.ResponseVO;
import com.zbkc.service.AreaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 产权登记表 前端控制器
 * </p>
 *
 * @author yangyan
 * @since 2021-08-06
 */
@Api(tags = "地区位置")
@RestController
@Slf4j
@CrossOrigin("*")
@RequestMapping("/area")
public class AreaController {

    @Autowired
    private AreaService areaService;

    @ApiOperation(value = "获取所有省编码")
    @GetMapping(value = "/provinceCode")
    public ResponseVO getProvinceCode() throws BusinessException {
        return areaService.getProvinceCode();
    }

    @ApiOperation(value = "根据省编码获取所有市编码")
    @GetMapping(value = "/cityCode/{provinceCode}")
    public ResponseVO getCityCodeByProvinceCode(@PathVariable("provinceCode") Long provinceCode) throws BusinessException {
        return areaService.getCityCodeByProvinceCode(provinceCode);
    }

    @ApiOperation(value = "根据市编码获取所有区编码")
    @GetMapping(value = "/areaCode/{cityCode}")
    public ResponseVO getAreaCodeByProvinceCode(@PathVariable("cityCode") Long cityCode) throws BusinessException {
        return areaService.getAreaCodeByProvinceCode(cityCode);
    }
    @ApiOperation(value = "根据区编码获取所有街道编码")
    @GetMapping(value = "/streetCode/{areaCode}")
    public ResponseVO getStreetCodeCodeByProvinceCode(@PathVariable("areaCode") Long areaCode) throws BusinessException {
        return areaService.getStreetCodeCodeByProvinceCode(areaCode);
    }
    @ApiOperation(value = "根据街道编码获取所有社区编码")
    @GetMapping(value = "/committeeCode/{streetCode}")
    public ResponseVO getCommitteeCodeCodeByProvinceCode(@PathVariable("streetCode") Long streetCode) throws BusinessException {
        return areaService.getCommitteeCodeCodeByProvinceCode(streetCode);
    }


    @ApiOperation(value = "根据编码获取详细信息")
    @GetMapping(value = "/details/{code}")
    public ResponseVO getDetailsByCode(@PathVariable("code") Long code) throws BusinessException {
        return areaService.getDetailsByCode(code);
    }


}
