package com.zbkc.model.vo;

import lombok.Data;
import java.math.BigDecimal;


/**
 * @author yangyan
 * @date 2021/09/14
 */
@Data
public class PaidRecord {

    /**
     * 缴费时间  收款日期
     */
    private String paidTime;

    /**
     * 收款操作人
     */
    private String collecPerson;

    /**
     * 收费项目
     */
    private String paidProject;

    /**
     * 计费周琦 起租时间
     */
    private String startRentDate;

    /**
     * 计费周琦 止租时间
     */
    private String endRentDate;

    /**
     * 实收金额(元)
     */
    private BigDecimal actualCollection;

    /**
     * 收款方式 1：微信支付  2：支付宝支付 3：其它
     */
    private Integer paidMethod;

    /**
     * 备注
     */
    private String remark;

}
