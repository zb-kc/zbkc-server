package com.zbkc.model.vo;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysFilePath1;
import com.zbkc.model.po.YyCommissioneOperation;
import com.zbkc.model.po.YyUseRegFilePath1;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * @author yangyan
 * @date
 */
@Data
public class YyCommissioneOperationVo {

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 物业名称
     */
    private String name;

    /**
     * 运营面积（建筑面积）
     */
    private Double buildArea;

//    /**
//     * 传输对象
//     */
//    private YyCommissioneOperation yyCommissioneOperation;

    /**
     * 合同附件
     */
    private List<SysFilePath1> contractFileList;

    /**
     * 使用批准附件
     */
    private List<SysFilePath1> useApprovalFileList;

    /**
     * 自定义文件
     */
    private Map<String, List<SysFilePath1>> customFileMap;

    /**
     * 房地产名称   物业资产id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 使用起始 开始时间
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Timestamp startTime;

    /**
     * 使用起始 结束时间
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Timestamp endTime;

    /**
     * 运营单位名称
     */
    private String operationUnit;

    /**
     * 统一社会信用代码
     */
    private String creditCode;

    /**
     * 法定代表人姓名
     */
    private String legalName;

    /**
     * 法定代表人联系方式
     */
    private String legalPhone;

    /**
     * 法定代表人证件类型
     */
    private String legalType;

    /**
     * 法定代表人证件号码
     */
    private String legalCode;

    /**
     * 代理人联系方式
     */
    private String agentPhone;

    /**
     * 代理人姓名
     */
    private String agentName;

    /**
     * 代理人类型
     */
    private String agentType;

    /**
     * 代理人证件号码
     */
    private String agentCode;

    /**
     * 运营费用
     */
    private Double operationMoney;

    /**
     * 备注
     */
    private String remark;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 更新时间
     */
    private Timestamp updateTime;

    /**
     * 自定义文件id 关联yy_use_reg_file表
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long custFileId;

    /**
     * 合同附件  sys_file_path字段db_file_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long contractFileId;

    /**
     * 使用批准文件  sys_file_path字段db_file_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long useApprovalFileId;

    /**
     * 自定义附件  sys_file_path字段db_file_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long customFileId;



}
