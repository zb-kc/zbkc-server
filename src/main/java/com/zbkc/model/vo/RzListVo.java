package com.zbkc.model.vo;

import lombok.Data;

@Data
public class RzListVo {
    private String name;
    private int num;
}
