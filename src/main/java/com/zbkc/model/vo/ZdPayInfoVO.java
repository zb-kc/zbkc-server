package com.zbkc.model.vo;

import lombok.Data;

/**
 * @author caobiyang
 * @date 2020/10/20
 */
@Data
public class ZdPayInfoVO {
    /**
     * 已缴账单数
     */
    private Integer yjBillCount;

    /**
     * 未缴账单数
     */
    private Integer wjBillCount;

    /**
     * 欠缴帐单数
     */
    private Integer qjBillCount;
}
