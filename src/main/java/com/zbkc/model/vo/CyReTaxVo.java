package com.zbkc.model.vo;

import lombok.Data;

import java.util.List;

@Data
public class CyReTaxVo {

    /**
     * 近三年企业趋势信息
     * */
    List<RegTaxVo> regTaxList;

    /**
     * 近三年园区租赁信息
     * */
    List<ZlRegTaxVo> zlRegTaxVoList;

}
