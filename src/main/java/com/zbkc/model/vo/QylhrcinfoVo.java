package com.zbkc.model.vo;

import lombok.Data;

@Data
public class QylhrcinfoVo {
    /**
     * 姓名
     * */
    private  String name;
    /**
     * 人才类型
     * */
    private  String rcType;
    /**
     * id
     * */
    private  String id;

    /**
     * 结束时间
     * */
    private  String endTime;
    /**
     * 开始时间
     * */
    private  String startTime;






}
