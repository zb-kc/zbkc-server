package com.zbkc.model.vo;

import lombok.Data;

import java.util.List;

@Data
public class MoreAnalysisVo {
    /**
     * 租凭物业种类占比前五统计
     * */
    public List<ZcVo> zbList;
    /**
     *各街道租凭物业面积
     * */
    public List<ZlPriceVo>zlPriceList;

}
