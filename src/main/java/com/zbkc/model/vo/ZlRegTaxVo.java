package com.zbkc.model.vo;

import lombok.Data;

import java.util.List;

@Data
public class ZlRegTaxVo {
    /**
     * 园区名称
     * */
    public String name;
    /**
     * 园区 id
     * */
    public String id;

    /**
     * 租赁信息
     * */
    List<RegTaxVo> regTaxList;


}
