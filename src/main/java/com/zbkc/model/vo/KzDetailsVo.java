package com.zbkc.model.vo;

import lombok.Data;

@Data
public class KzDetailsVo {
    /**
     *名称
     * */
    private String name;
    /**
     *id
     * */
    private String id;
    /**
     * 建筑面积
     * */
    private Double area;
    /**
     * 上家租户企业
     * */
    private String preEnterprise;
    /**
     * 单元状态（1：预备在建，2：空置，3：运营中，4：处置中，5：已处置）
     * */
    private String status;

    //顶级物业
    private String topname;

}
