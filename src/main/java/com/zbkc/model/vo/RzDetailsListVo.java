package com.zbkc.model.vo;

import lombok.Data;

import java.util.List;

@Data
public class RzDetailsListVo {
    /**
     * 入驻列表
     * */
    List<RzListVo>rzList;
    /**
     * 预到期
     * */
    private  int ydq1Month;
    /**
     * 预到期
     * */
    private  int ydq3Month;
    /**
     * 预到期
     * */
    private  int ydq6Month;

}
