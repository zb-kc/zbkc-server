package com.zbkc.model.vo;

import lombok.Data;

@Data
public class FCMoney {
    /**
     * 年
     * */
    private String year;
    /**
     * 扶持资金
     * */
    private String money;

    /**
     * 项目名称
     * */
    private String projectName;
    /**
     * 资金类型
     * */
    private String projectType;
    /**
     * 业务类别
     * */
    private String ywType;

    /**
     * 主管部门
     * */
    private String zgDep;
    /**
     * 主管科室
     * */
    private String zgPro;



}
