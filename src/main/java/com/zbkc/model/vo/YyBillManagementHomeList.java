package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author yangyan
 * @date 2021/09/16
 */
@Data
public class YyBillManagementHomeList {
    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Integer id;

    /**
     * 承租方
     */
    private String leaser;

    /**
     * 账单日期  yyyy.mm
     */
    private String billTime;

    /**
     * 租赁物业地址
     */
    private String address;

    /**
     * 月租金(元)
     */
    private BigDecimal monthlyRent;

    /**
     * 应收金额(元)
     */
    private BigDecimal amountReceivable;

    /**
     * 实收金额(元)
     */
    private BigDecimal actualCollection;

    /**
     * 缴费状态  缴费状态 1.未缴费、2.部分缴费、3.已缴费、4.欠缴费、5.已补缴、6.已缴费(预缴费转)
     */
    private Integer paymentStatus;

    /**
     * 开票状态 1:未开票;2已开票 默认1
     */
    private Integer invoiceStatus;

    /**
     * 用于导出缴费状态字符串   未缴费、部分缴费、已缴费、欠缴费、已补缴、已缴费(预缴费转)
     */
    private String paymentStatusStr;

    /**
     * 用于导出开票状态字符串  未开票、已开票、默认（未开票）
     */
    private String invoiceStatusStr;

    /**
     * 物业id
     */
    private String wyId;

    /**
     * 物业名字
     */
    private String wyName;

    /**
     *账单状态 1:未缴费;2已缴费 ,3欠缴费  默认1
     */
    private Integer billStatus;

    /**
     * 合同id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long contractId;
}
