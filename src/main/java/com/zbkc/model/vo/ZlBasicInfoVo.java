package com.zbkc.model.vo;

import lombok.Data;

import java.util.List;

@Data
public class ZlBasicInfoVo {
    /**
     *
     * 总面积
     * */
    private Double area;
    /**
     * 租赁总数
     * */
    private int num;
    /**
     * 月租金
     * */
    private Double price;
    /**
     * 平均租金
     * */
    private Double avgPrice;

    /**
     * 租凭物业使用单位统计情况列表
     * */
    private List<CyWyVo> zlList;



}
