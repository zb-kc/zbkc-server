package com.zbkc.model.vo;


import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysFilePath;
import com.zbkc.model.po.SysImg;
import com.zbkc.model.po.WyFacilitiesInfo;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * 查看大厦 条件实体类
 * @author gmding
 * @date 2021/08/20
 */
@Data
public class WyBasicInfoVo {
    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 父级id 默认 0
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long pid;

    /**
     * 资产类型:  产业用房；孵化园区；办公用房；公共服务设施；商业；住宅；租赁社会物业
     */
    private String assetType;

    /**
     * 物业种类:  产业用房；商业；公共配套；办公用房；公共配套；基本办公用房；服务用房；设备用房；附属用房；教育；社区服务；市政共用；文体；医疗卫生
     *
     */
    private String propertyType;

    /**
     * 物业种类型 类型 1园区 2栋 3层 4室 5零散 6楼栋(主体)
     */
    private Integer type;

    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 物业权属 id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyTagId;


    /**
     * 归属所有人
     */
    @Length(max = 20,min = 1,message = "物业名称最长20个字符")
    private String belongPerson;

    /**
     * 物业名称
     *
     * */
    @Length(max = 100,min = 1,message = "物业名称最长100个字符")
    private String name;

    /**
     * 楼栋，楼层，单元、室名称
     */
    @Length(max = 100 ,message = "单元、室名称限制长度100个字符")
    private String unitName;

    /**
     * 物业简称
     *
     * */
    @Length(max = 100,message = "物业简称最长100个字符")
    private String shortName;

    /**
     * 别名/曾用名
     *
     * */
    @Length(max = 100,message = "别名/曾用名最长100个字符")
    private String aliasName;

    /**
     * 物业状态 id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyStatusId;

    /**
     * 物业规模 id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyScaleId;

    /**
     * 物业种类 id
     *
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] propertyTypeId;

    /**
     * 主要用途 id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long mainPurposId;

    /**
     * 物业标签id（政府）
     *
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] wyTagId;

    /**
     * 物业规划（政府）
     */

    @Length(max = 50 , message = "物业规划限制长50个字符")
    private String propertyPlan;

    /**
     * 物业编号
     */
    @Length(max = 50 , message = "物业编号限制长50个字符")
    private String propertyCode;


    /**
     * 物业简介
     * */
    private String propertyIntroduction;

    /**
     * 社区编码
     **/
    @JsonSerialize(using= ToStringSerializer.class)
    private Long areaCode;

    /**
     * 地址
     */
    @Length(max = 100 , message = "地址限制长100个字符")
    private String address;

    /**
     * 详细地址
     */
    @Length(max = 200 , message = "详细地址限制长200个字符")
    private String detailAddr;


    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 维度
     */
    private BigDecimal latitude;

    /**
     * 建筑面积
     * */
    private Double buildArea;

    /**
     * 用地面积
     * */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Double landArea;

    /**
     * 实用面积
     * */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Double practicalArea;

    /**
     * 公摊面积
     * */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Double shareArea;

    /**
     * 物业来源 id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] propertySourceId;

    /**
     * 物业图片
     * */
    private List<SysImg> propertyImg;

    /**
     * 运营方式  1，自营  2.委托运营  3.其他管理单位
     * */
    private Integer operatesType;

    /**
     * 委托运营管理单位   1.深圳市大沙河建设投资有限公司、2.深圳市深汇通泰丰物业发展有限公司
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long operatesUnitId;

    /**
     * 运营单位项目组  （ 缺失字段）
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long opreatesGroupId;

    /**
     * 其他管理单位
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long otherUnitId;

    /**
     * 设计单位
     */
    @Length(max = 50 , message = "设计单位限制长50个字符")
    private String designUnit;

    /**
     * 施工单位
     */
    @Length(max = 50 , message = "施工单位限制长50个字符")
    private String buildUnit;

    /**
     * 建造说明
     */
    @Length(max = 500 , message = "建造说明限制长500个字符")
    private String buildDescription;

    /**
     * 建造备注
     */
    @Length(max = 500 , message = "建造备注限制长500个字符")
    private String buildRemark;


    /**
     * 验收报告
     * */
    private List<SysFilePath> acceptanceReportFile;

    /**
     * 施工许可证
     */
    private List<SysFilePath> constructPermitFile;

    /**
     *
     * 建造自定义附件
     * */
    private Map<String,List<SysFilePath>> buildCustFile;

    /**
     * 用地开始时间
     * */
    private String landStartTime;

    /**
     * 用地结束时间
     * */
    private String landEndTime;

    /**
     * 建设单位
     */
    @Length(max = 50 , message = "建设单位限制长50个字符")
    private String constructionUnit;

    /**
     * 用地说明
     */
    @Length(max = 500 , message = "用地说明限制长500个字符")
    private String landDescription;

    /**
     * 用地备注
     */
    @Length(max = 500 , message = "用地说明限制长500个字符")
    private String landRemark;

    /**
     * 土地出让合同
     * */
    private List<SysFilePath> landTransferContractFile;
    /**
     *
     * 红线图或宗线图
     * */
    private List<SysFilePath> redLineFile;

    /**
     * 用地许可证
     * */
    private List<SysFilePath> landPermitFile;

    /**
     *
     * 用地自定义附件
     * */
    private Map<String,List<SysFilePath>> siteCustFile;

    /**
     * 物业公司名称
     */
    @Length(max = 50 , message = "物业公司名称限制长50个字符")
    private String companyName;

    /**
     * 联系人
     */
    @Length(max = 50 , message = "物业公司名称限制长50个字符")
    private String contacts;

    /**
     * 联系电话
     */
    @Length(max = 50 , message = "物业公司名称限制长50个字符")
    private String phone;

    /**
     * 物业管理费用
     */
    private double manageFee;

    /**
     * 物管信息备注
     */
    @Length(max = 50 , message = "物业公司名称限制长50个字符")
    private String remark;

    /**
     * 房屋编码
     */
    private String houseCode;

    /**
     * 楼栋数量
     *
     * */
    private int num;

    /**
     * 层高
     * */
    private Integer storeyHeight;
    /**
     * 承重
     * */
    private Double loadBearer;


    /**
     * 附属设施信息列表
     * */
    private List<WyFacilitiesInfo> wyFacilitiesInfoList;

    /**
     * 朝向 1.东2.南3.西4.北
     */
    private Integer towards;


    /**
     * 户型图  sys_img字段primary_table_id
     */
    private List<SysImg> houseImg;

    /**
     * 内三维
     */
    @Length(max = 100 , message = "内三维名称限制长100个字符")
    private String threeDimen;







}
