package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysFilePath1;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * @author yangyan
 * @date 2021/09/15
 */
@Data
public class YjContractVo {

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;


    /**
     * 物业名称
     */
    private String wyName;

    /**
     * 房屋租赁用途   1,办公 2，综合（研发）3，工业研发 4，研发
     */
    private Integer leaseUse;

    /**
     * 租赁面积
     */
    private BigDecimal rentalArea;
    /**
     * 月租金
     */
    private BigDecimal monthlyRent;
    /**
     * 租金单价
     */
    private BigDecimal rentUnitPrice;

    /**
     * 天数
     */
    private Integer limitDay;

    /**
     * 承租方
     */
    private String leaser;
    /**
     * 起租时间
     */
    private String  startRentDate;
    /**
     * 止租时间
     */
    private String endRentDate;

    /**
     * 合同年限
     */
    private Integer year;
    /**
     * 法人
     */
    private String legalPerson;
    /**
     * 法人联系方式
     */
    private String legalPhone;
    /**
     * 代理人
     */
    private String clientPerson;
    /**
     * 代理人联系方式
     */
    private String clientPhone;
    /**
     * 备注
     */
    private String remark;

    /**
     * 租赁用途中文 便于导出: 办公 、综合（研发）、工业研发、 研发
     */
    private String leaseUseStr;

    /**
     * 合同 sys_file_path字段db_file_id sys_file_path字段db_file_id
     */
    private Long contractFilePathId;

    /**
     * 合同
     */
    private List<SysFilePath1> contractFileList;

    /**
     * 资产类型(物业类型):  产业用房；孵化园区；办公用房；公共服务设施；商业；住宅；租赁社会物业
     */
    private String assetType;

    /**
     * 物业id
     */
    private Long wyId;


}
