package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.YyBillManagement;
import lombok.Data;

/**
 * @author yangyan
 * @date 2021/09/14
 */
@Data
public class ContractAndBillVo {

    private YyBillManagement info;
    private String leaser;
    private String leaserAddress;
    private String leaserPhone;
    private String collecPhone;
    private Integer templateType;
    private String startRentDate;
    private String endRentDate;
    private String freeRent;
    private String wyName;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long contractFilePathId;
}
