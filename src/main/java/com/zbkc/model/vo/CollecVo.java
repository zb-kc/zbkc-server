package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author yangyan
 */
@Data
public class CollecVo {

//    /**
//     * 主键id
//     */
//    @JsonSerialize(using= ToStringSerializer.class)
//    private Long id;

    private List<PendingRecept> pendingRecept;

    /**
     * 承租方
     */
    private String leaser;

    /**
     * 合同id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long contractId;

    /**
     * 承租方电话号码
     */
    private String leaserPhone;

    /**
     * 租赁物业地址
     */
    private String address;

    /**
     * 收款日期
     */
    private String receivedDate;

    /**
     * 预收款余额
     */
    private BigDecimal advanceCollection;

    /**
     * 缴费方式 1：微信支付  2：支付宝支付 3：post机刷卡 4：转账 5：柜台刷卡 6：批扣 7:其它
     */
    private Integer paidMethod;

    /**
     * 备注
     */
    private String remark;

    /**
     * 物业id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyId;

}
