package com.zbkc.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.*;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author yangyan
 * @date 2021/09/03
 */
@Data
public class WyproRightVo {

    private WyProRightReg reg;

    /**
     *房地产名称
     */
    private String name;

    /**
     * 权力类型 数据字典值
     */
    private SysDataType rightType;

    /**
     * 土地用途 数据字典值
     */
    private List<SysDataType> landPurposeList;


    /**
     * 共有情况 数据字典值
     */
    private SysDataType commonSituation;



    /**
     * 产权证书 文件
     */
    private List<SysFilePath1> proRightFile;

    /**
     * 自定义证书 文件
     */
    private Map<String,List<SysFilePath1>> custRightFile;




}
