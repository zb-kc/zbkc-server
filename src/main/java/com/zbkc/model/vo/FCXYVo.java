package com.zbkc.model.vo;

import lombok.Data;

@Data
public class FCXYVo {
    /**
     * 企业名称
     * */
    private String name;
    /**
     * 企业代码
     * */
    private String code;
    /**
     * 年份
     * */
    private String year;
    /**
     * 人员数量
     * */
    private String num;
    /**
     * 人均面积
     * */
    private String avgarea;
    /**
     * 年实际缴纳租金
     * */
    private String sjzj;
    /**
     * 年租金减免
     * */
    private String jmzj;
    /**
     * 财力贡献
     * */
    private String clgx;
    /**
     * 使用绩效分析
     * */
    private String syjx;
    /**
     * 产值（单位：亿元）
     * */
    private String cz;
    /**
     * 单位面积产值(单位：元）
     * */
    private String avgcz;
    /**
     * 纳税（单位：元）
     * */
    private String ns;

    /**
     * 单位面积纳税(单位：元）
     * */
    private String avgns;

    /**
     * 面积
     * */
    private String area;

}
