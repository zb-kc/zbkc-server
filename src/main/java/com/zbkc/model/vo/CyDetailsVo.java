package com.zbkc.model.vo;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class CyDetailsVo {

    /**
     * 名称
     * */
    public String name;
    /**
     * 建筑面积
     * */
    public Double buildArea;
    /**
     * 用地面积
     * */
    public Double landArea;

    /**
     * 平均租金
     * */
    public Double avgPrice;
    /**
     * 楼栋数量
     * */
    public Double buildingCount;
    /**
     *可出租面积
     * */
    public Double rentableArea;
    /**
     *已出租面积
     * */
    public Double rentedArea;

    /**
     *空置户
     * */
    public int vacantCount;
    /**
     * 空置面积
     * */
    public Double vacantArea;
    /**
     * 入驻企业数
     * */
    public int qyCount;
    /**
     *预到期
     * */
    public int beExpire;
    /**
     *是否有产权证
     * */
    public String certHas;
    /**
     *入账金额
     * */
    public String rzMoney;
    /**
     * 地址
     * */
    public String address;

    /**
     *街道
     * */
    public String streetName;
    /**
     *社区
     * */
    public String communityName;
    /**
     *管理单位
     * */
    public String manUnit;
    /**
     *物业公司
     * */
    public String wygsmc;
    /**
     *物业公司电话
     * */
    public String wygstel;


    /**
     *是否签订合同
     * */
    public String zlshwysfqdht;
    /**
     *（租赁社会物业）起始日期
     * */
    public String zlshwyqsrq;
    /**
     *（租赁社会物业）到期日期
     * */
    public String zlshwydqrq;

    /**
     * 使用时间
     * */
    public String userTime;

    /**
     * 照片地址
     * */
    public List<String> url;

    //显示的页签控制
    private String[] showTab;

    //扶持效益分析
    private Map analysisMap;
 }
