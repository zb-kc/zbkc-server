package com.zbkc.model.vo;


import lombok.Data;

@Data
public class CBpersonVo {
    /**
     * 参保姓名
     * */
    private String name;
    /**
     * 证件类型
     * */
    private  String cardType;
    /**
     * 证件号码
     * */
    private String cardNo;
    /**
     * 更新日期
     * */
    private String updateTime;
}
