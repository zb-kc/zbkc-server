package com.zbkc.model.vo;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class DetailsVo {
    /**
     * 企业介绍
     * */
    private String introduction;
    /**
     * 公司名称
     * */
    private String comName;

    /**
     * 企业信息
     * */
    QyDetailsVo qyDetails;

    /**
     * 纳税总和
     * */
    private Double nsTotal;



    /**
     * 参保列表
     * */
    List<CBpersonVo> cbList;
    /**
     * 扶持资金列表
     *
     */
    List<FCMoney>fcList;

    /**
     * 领航人才
     * */
    List<QylhrcinfoVo> qyrcList;
    /**
     *租凭信息
     * */
    List<ZPDetailsVo> zpList;

    /**
     *扶持人才房列表
     * */
    List<FCRCFListVo>fcrcfList;

    /**
     *三年登记税收统计
     * */
    List<RegTaxVo> regTaxList;

    /**
     *企业扶持效益
     * */
    List<FCXYVo> fcxyList;

    /**
     * 退租原因list
     */
    List<Map> refundReasonList;

    //是否经济贡献承诺
    private String isEconomicCommit;

    //经济贡献承诺
    private String economicCommit;

    //产业监管情况
    private Map<String , Object> regulatorySituationMap;
}
