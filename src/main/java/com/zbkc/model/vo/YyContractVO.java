package com.zbkc.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/26
 */
@Data
@JsonIgnoreProperties(value = {"wyBaseInfoVOList"})
public class YyContractVO {

    @JsonSerialize(using= ToStringSerializer.class)
    private long id;

    /**
     * 出租物业地址 与 承租方通信地址 相同
     */
    private String leaseWyAddress;

    /**
     * 合同类型 1产业用房租赁合同3+2（有经济贡献率）;2产业用房租赁合同3+2（无经济贡献率）;3产业用房租赁合同3年及3年以下;
     */
    private String type;
    /**
     * 出租面积
     */
    private String rentalArea;
    /**
     * 签约类型 1新签 2续签
     */
    private String webSignType;
    /**
     * 月租金
     */
    private String monthlyRent;
    /**
     * 起租时间
     */
    private String startRentDate;
    /**
     * 止租时间
     */
    private String endRentDate;

    /**
     * 物业信息
     */

    private List<WyBaseInfoVO> wyBaseInfoVOList;

    /**
     * 是否是主合同 true 是 false否
     */
    private boolean mainContract;

}
