package com.zbkc.model.vo;

import lombok.Data;

import java.util.List;

@Data
public class YQMoreAnalysisVo {
    /**
     * 公司名称
     * */
    private String name;
    /**
     * 企业统一代码
     * */
    private String code;
    /**
     *
     * */
    private int v1;

    /**
     *
     * */
    private int v2;
    /**
     *
     * */
    private int v3;
    /**
     *
     * */
    private int v4;
    /**
     *
     * */
    private int v5;
    /**
     *
     * */
    private int v6;
    /**
     *
     * */
    private int v7;
    /**
     *
     * */
    private int v8;
    /**
     *
     * */
    private int v9;
    /**
     *
     * */
    private int v10;
    /**
     *
     * */
    private int v11;
    /**
     *
     * */
    private int v12;
    /**
     *
     * */
    private int v13;
    /**
     *
     * */
    private int v14;

    List<Top10ListVo>top10List;


}
