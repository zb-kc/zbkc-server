package com.zbkc.model.vo;

import lombok.Data;

@Data
public class RegTaxVo {
    /**
     *
     * 年份
     * */
    private String year;
    /**
     * 企业数量
     * */
    private String num;

    /**
     * 企业人数
     * */
    private String number;
    /**
     *
     * 产值金额 (亿元)
     * */
    private String cz;
    /**
     *
     * 纳税金额 (元)
     * */
    private String total;

    /**
     *
     * 面积
     * */
    private String area;

    /**
     *
     * 指导单价
     * */
    private String guideprice;
    /**
     *
     * 租金单价
     * */
    private String price;

    /**
     *
     * 月租金
     * */
    private String yzj;

}
