package com.zbkc.model.vo;

import lombok.Data;

@Data
public class HouseCodeListVo {

    /**
     * 物业名称
     * */
    private String wymc;
    /**
     * 街道
     * */
    private String jd;
    /**
     * 社区
     * */
    private String sq;
    /**
     * 详细地址
     * */
    private String xxdz;
    /**
     * 房屋编码补充状态
     * */
    private String fwbmqk;
    /**
     * 房屋编码匹配状态
     * */
    private String hsqksm;
    /**
     * 楼栋编码
     * */
    private String ldbm;
    /**
     * 房屋编码
     * */
    private String fwbm;







}
