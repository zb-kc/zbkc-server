package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @author yangyan
 * @date 2021/09/17
 *  * <p>
 *  *     预到期合同
 *  * </p>
 */
@Data
public class YjContractReport {
    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 租赁物业
     */
    private String wyName;

    /**
     * 资产类型(物业类型):  产业用房；孵化园区；办公用房；公共服务设施；商业；住宅；租赁社会物业
     */
    private String assetType;

    /**
     * 房屋租赁用途   1,办公 2，综合（研发）3，工业研发 4，研发
     */
    private Integer leaseUse;
    /**
     * 出租面积 租赁面积
     */
    private BigDecimal rentalArea;
    /**
     * 月租金
     */
    private BigDecimal monthlyRent;
    /**
     * 租金单价
     */
    private BigDecimal rentUnitPrice;

    /**
     *  剩余期限
     */
    private int limitDay;

    /**
     * 承租方
     */
    private String leaser;
    /**
     * 起租时间
     */
    private String startRentDate;

    /**
     * 止租时间
     */
    private String endRentDate;

    /**
     * 合同期限
     */

    private int year;

    /**
     * 法人代表
     */
    private String legalPerson;

    /**
     * 法人联系方式
     */
    private String legalPhone;
    /**
     * 委托人
     */
    private String clientPerson;

    /**
     * 委托人联系方式
     */
    private String clientPhone;

    /**
     * 备注
     */
    private String remark;

//    /**
//     * 类型 1产业用房租赁合同3+2（有经济贡献率）;2产业用房租赁合同3+2（无经济贡献率）;3产业用房租赁合同3年及3年以下;
//     */
//    private Integer type;

    /**
     *  房屋租赁用途中文（便于导出）  办公 、综合（研发）、工业研发 、研发
     */
    private String leaseUseStr;


}
