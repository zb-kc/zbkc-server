package com.zbkc.model.vo;

import lombok.Data;

@Data
public class WyListVo {


    /**
     * 物业名称
     * */
    private String name;
    /**
     * 物业地址
     * */
    private String address;
    /**
     * 物业种类
     * */
    private String wyType;
    /**
     * 建筑面积
     * */
    private String buildArea;
    /**
     * 平均租金
     * */
    private String avgPrice;

    /**
     * 街道名称
     * */
    private String streetName;
    /**
     * 社区名称
     * */
    private String communityName;
}
