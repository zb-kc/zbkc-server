package com.zbkc.model.vo;

import lombok.Data;

@Data
public class ZdqyListVo {
    /**
     * 租赁数
     * */
    private int num;
    /**
     * 企业名称
     * */
    private String name;
    /**
     * 信用代码
     * */
    private String code;
}
