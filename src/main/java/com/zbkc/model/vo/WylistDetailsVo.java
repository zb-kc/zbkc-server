package com.zbkc.model.vo;

import lombok.Data;

@Data
public class WylistDetailsVo {
    /**
     *房屋编码
     * */
    private String houseNumber;
    /**
     * 竣工时间
     * */
    private String jgTime;
    /**
     *使用批准文件号
     * */
    private String pizhunNo;
    /**
     * 是否空置
     * */
    private String emptystate;
    /**
     * 移交时间
     * */
    private String yjTime;
    /**
     * 管理单位
     * */
    private String manUnit;
    /**
     * 物业总类8+1
     * */
    private String wyKind;
    /**
     * 移交单位
     * */
    private String yjUnit;
    /**
     * 使用单位联系方式
     * */
    private String useTel;
    /**
     * 街道
     * */
    private String streetName;
    /**
     * 建筑面积
     * */
    private String buildArea;
    /**
     * 实际使用单位
     * */
    private String useUnit;

    /**
     * 详细地址
     * */
    private String address;
    /**
     * 实用面积
     * */
    private String actArea;
    /**
     * 物业归属类型
     * */
    private String belongType;
    /**
     * 实际用途
     * */
    private String actUsage;
    /**
     * 物业来源
     * */
    private String source;
    /**
     * 规定用途
     * */
    private String usageguiding;
    /**
     * 该物业规模
     * */
    private String wyScale;

    /**
     * 使用起止日期
     * */
    private String syqssj;

    /**
     * 是否已办理产权证
     * */
    private String certHas;
    /**
     * 产权证号
     * */
    private String certNo;
    /**
     * 产权人
     * */
    private String owner;
    /**
     * 产权证办理日期
     * */
    private String certDate;
    /**
     * 是否已入账
     * */
    private String ruzhang;
    /**
     * 入账凭证
     * */
    private String sfyrzpzfj;

    /**
     * 入账金额
     * */
    private String ruzhangAmount;
    /**
     *
     * 入账日期
     * */
    private String ruzhangDate;
    /**
     * 入账单位
     * */
    private String ruzhangUnit;
    /**
     * 是否出租
     * */
    private String zfwysfcz;
    /**
     * 出租承租方
     * */
    private String czczf;
    /**
     * 出租合同
     * */
    private String sfyczhtfj;
    /**
     * 租赁方式
     * */
    private String czzlfs;
    /**
     *
     * 租金（元）
     * */
    private String czyzj;
    /**
     * 租金定价标准
     * */
    private String czdjbz;
    /**
     *
     * 出租起始时间
     * */
    private String czqssj;
    /**
     *
     * 出租结束时间
     * */
    private String czjssj;

    /**
     * 免租期限
     * */
    private String mzqx;

    /**
     * 租赁期限（年）
     * */
    private String czzljx;


    /**
     *是否签订合同
     * */
    public String zlshwysfqdht;
    /**
     *（租赁社会物业）起始日期
     * */
    public String zlshwyqsrq;
    /**
     *（租赁社会物业）到期日期
     * */
    public String zlshwydqrq;

    /**
     *（租赁社会物业）月租金
     * */
    public String zlshwyyzj;

    /**
     *（租赁社会物业）租赁单价
     * */
    public String zlshwyzldj;



    /**
     *社区
     * */
    public String communityName;

    /**
     *物业公司
     * */
    public String wygsmc;
    /**
     *物业公司电话
     * */
    public String wygstel;


}
