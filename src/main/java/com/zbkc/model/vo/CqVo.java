package com.zbkc.model.vo;

import lombok.Data;

@Data
public class CqVo {
    /**
     * 产业用房
     * */
    private int cyNum;
    /**
     * 办公用房
     * */
    private int bgNum;
    /**
     * 公共服务设施
     * */
    private int ggNum;
    /**
     * 商业
     * */
    private int syNum;
    /**
     * 住宅
     * */
    private int zzNum;
}
