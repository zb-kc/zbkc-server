package com.zbkc.model.vo;

import lombok.Data;

@Data
public class CyWyVo {
    /**
     * 面积
     * */
    private Double area;
    /**
     * 数量
     * */
    private Double num;
    /**
     * 名称
     * */
    private String unitName;
}
