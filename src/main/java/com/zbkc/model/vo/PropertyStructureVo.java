package com.zbkc.model.vo;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysDataType;
import com.zbkc.model.po.WyFacilitiesInfo;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

/**
 * 物业结构实体类
 * @author gmding
 * @date 2021/08/23
 */
@Data
public class PropertyStructureVo {

    /**
     * 物业名称
     *
     * */
    private String name;

    /**
     * 单元，楼栋，室内，楼层名称
     *
     * */
    private String unitName;

    /**
     * 父级id 默认 0
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long pid;
    /**
     *物业种类型 类型 1园区 2楼栋 3楼层 4单元 5室 6零散 7楼栋(主体)
     * */
    private Integer type;
    /**
     * id 默认 0
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;


    /**
     * 物业种类id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyTypeId;

    /**
     * 物业标签 数据字典值
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyTagId;

    /**
     * 创建时间
     * */
    private Timestamp createTime;

    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 子节点
     * */
    private List<PropertyStructureVo> children;

    /**
     * 下一级规模
     */
    private String sonScale;

    /**
     * 关联物业地址
     */
    private String wyAddress;

    /**
     * 楼栋/层数量
     */
    private Integer num;

    /**
     * 物业编码
     */
    private  String wyCode;

    /**
     * 楼层编码
     */
    private  String houseCode;

    /**
     * 下级可选物业种类
     */
    private List<SysDataType> wyTypes;

    /**
     * 权属人
     */
    private String belongPerson;

    /**
     * 关联附属信息
     */
    private List<WyFacilitiesInfo> facilitiesInfos;

    /**
     * 是否空置 true 为空 false为不空
     */
    private Boolean isEmpty = false;

    /**
     * 建筑面积
     */
    private Double buildArea;

}

