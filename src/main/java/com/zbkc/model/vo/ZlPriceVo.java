package com.zbkc.model.vo;

import lombok.Data;

@Data
public class ZlPriceVo {
    /**
     * 面积
     * */
    private Double area;
    /**
     *街道名称
     * */
    private String streetName;


}
