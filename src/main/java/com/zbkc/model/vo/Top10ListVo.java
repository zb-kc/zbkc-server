package com.zbkc.model.vo;

import lombok.Data;

@Data
public class Top10ListVo {
    private String name;
    private int num;
}
