package com.zbkc.model.vo;

import lombok.Data;

@Data
public class ZcVo {
    /**
     * 数量
     * */
    private int num;
    /**
     * 名称
     * */
    private String unitName;

}
