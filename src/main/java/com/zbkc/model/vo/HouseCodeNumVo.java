package com.zbkc.model.vo;

import lombok.Data;

@Data
public class HouseCodeNumVo {
    /**
     * 未补充编码
     * */

    private int noCode;
    /**
     * 已补充编码
     * */
    private int haveCode;
    /**
     * 不符合编码条件
     * */
    private int incompatible;

}
