package com.zbkc.model.vo;

import lombok.Data;

import java.util.List;

@Data
public class CyDetailsAnalysisVo {

    /**
     * 入驻列表
     * */
    private List<YqRentListVo>rzList;
    /**
     * 出租列表
     * */
    private List<YqRentListVo>czList;


}
