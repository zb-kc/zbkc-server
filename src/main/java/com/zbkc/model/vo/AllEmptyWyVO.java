package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author caobiyang
 * @date 2021/12/22
 */
@Data
public class AllEmptyWyVO {
    /**
     * 物业id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 物业name
     */
    private String wyName;
}
