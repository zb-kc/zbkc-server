package com.zbkc.model.vo;

import lombok.Data;

@Data
public class WyAnalysisVo {

    /**
     * 企业名称
     * */
    private String name;
    /**
     *
     * 物业名称
     */
    private String wyname;
    /**
     *入驻时间
     */
    private String rztime;

    /**
     *合同开始时间
     */
    private String  starttime;

    /**
     *合同结束时间
     */
    private String endtime;
    /**
     *退房日期
     */
    private String tftime;
    /**
     *退租类型
     */
    private String tztype;
    /**
     *退租原因
     */
    private String tzresion;
    /**
     *详细原因
     */
    private String resion;
    /**
     *年份
     */
    private String year;

    //信用代码
    private String code;

}
