package com.zbkc.model.vo;

import lombok.Data;

@Data
public class FCRCFListVo {
    /**
     * 人才姓名
     *
     * */
    private String userName;

    /**
     * 房源名称
     *
     * */
    private String roomName;
    /**
     * 建筑面积
     *
     * */
    private String buildArea;
    /**
     * 合同金额
     *
     * */
    private String contractMoney;
    /**
     * 合同开始时间
     *
     * */
    private String contractStartTime;
    /**
     * 合同结束时间
     *
     * */
    private String contractEndTime;









}
