package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

/**
 * 多户收款返回信息实体类
 * @author caobiyang
 * @date 2021/11/24
 */
@Data
public class BillInfoVo {
    /**
     * 合同id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long contractId;

    /**
     * 承租方
     */
    private String leaser;

    /**
     * 承租方电话
     */
    private String leaserPhone;

    /**
     * 收款日期
     */
    private String receivedDate;

    /**
     * 待收金额表单
     */
    private List<ManyReceivedVO>  receivedList;
}
