package com.zbkc.model.vo;

import com.zbkc.model.po.SysImg1;
import lombok.Data;

import java.util.List;

/**
 * 社会租赁物业当前行返回类
 * @author caobiyang
 * @since  2021-10-14
 */
@Data
public class ShWyVO {
    /**
     * 租赁社会物业返回信息
     */
    private RentalSosialVo rentalSosialVo;
    /**
     * 物业图片
     * */
    private List<SysImg1> propertyImgPath;
    /**
     * 户型图
     * */
    private List<SysImg1> houseImgs;
}
