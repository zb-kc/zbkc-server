package com.zbkc.model.vo;

import lombok.Data;

@Data
public class KZVo {
    /**
     * 产业用房空置面积
     * */
    private Double cyArea;
    /**
     * 办公用房空置面积
     * */
    private Double bgArea;
    /**
     * 商业用房空置面积
     * */
    private Double syArea;
    /**
     * 公共服务空置面积
     * */
    private Double ggArea;
    /**
     * 住在空置面积
     * */
    private Double zzArea;

    /**
     * 产业用房空置率
     * */
    private Double cyRate;

    /**
     * 办公用房空置率
     * */
    private Double bgRate;
    /**
     * 商业用房空置率
     * */
    private Double syRate;
    /**
     * 公共服务空置率
     * */
    private Double ggRate;
    /**
     * 住在空置率
     * */
    private Double zzRate;




}
