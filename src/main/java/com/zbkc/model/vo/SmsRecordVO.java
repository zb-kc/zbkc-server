package com.zbkc.model.vo;

import lombok.Data;
import org.apache.poi.ss.formula.functions.T;

import java.sql.Timestamp;
import java.util.List;


/**
 * @date 2021/9/15
 * @author caobiyang
 */
@Data
public class SmsRecordVO {

    private Long id;

    /**
     * 物业名称
     */
    private String propertyName;

    /**
     * 承租方
     */
    private String leaser;

    /**
     * 当前年月
     */
    private String currentMonth;

    /**
     * 短信模板类型
     */
    private  Integer type;

    /**
     * 短信名称
     */
    private List<String> name;

    /**
     * 短信内容
     */
    private  String word;

    /**
     * 发送短信时间
     */
    private Timestamp sendTime;

    /**
     * 短信接收方电话
     */
    private String phone;
}
