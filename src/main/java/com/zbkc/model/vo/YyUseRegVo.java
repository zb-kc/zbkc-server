package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.*;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * @author yangyan
 * @date 2020/09/09
 */
@Data
public class YyUseRegVo {
    private static final long serialVersionUID=1L;
    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 房地产名称   物业资产id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /***
     * 物业种类
     */
    private String  propertyType;

    /**
     * 建筑面积
     */
    private Double buildArea;

    /**
     * 使用方式 1出租; 2调配
     */
    private Integer method;

    /**
     * 是否免租 1是；2否
     */
    private Integer rentFree;

    /**
     * 使用状态 1 在用; 2停用
     */
    private Integer status;

    /**
     * 使用对象名称
     */
    private String objName;

    /**
     * 使用对象类型 1.企业/2.个人/3.个体工商户/4.行政事业单位/5.其他
     */
    private Integer objType;

    /**
     * 统一社会信用代码
     */
    private String creditCode;

    /**
     * 联系人姓名
     */
    private String contactName;

    /**
     * 联系方式
     */
    private String contactPhone;

    /**
     * 使用开始
     */
    private String startTime;

    /**
     * 使用结束
     */
    private String endTime;

    /**
     * 实际使用用途
     */
    private String actualUse;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;


    /**
     * 合同附件id  sys_file_path字段db_file_id
     */
    private Long contractFileId;

    /**
     * 使用批准文件id  sys_file_path字段db_file_id
     */
    private Long useApprovalFileId;

    /**
     * 自定义附件id  sys_file_path字段db_file_id
     */
    private Long customFileId;

    /**
     * 合同附件
     */
    private List<SysFilePath1> contractFileList;

    /**
     * 使用批准附件
     */
    private List<SysFilePath1> useApprovalFileList;

    /**
     * 自定义文件
     */
    private Map<String,List<SysFilePath1>> customFileMap;
}
