package com.zbkc.model.vo;

import lombok.Data;

@Data
public class ZPDetailsVo {
    /**
     *
     *物业名称
     */
    private String wyName;
    /**
     * 面积
     * */
    private String area;
    /**
     * 单价
     * */
    private String price;
    /**
     * 月租金
     * */
    private String mPrice;
    /**
     * 开始时间
     * */
    private String startTime;
    /**
     * 结束时间
     * */
    private String endTime;

    /**
     * 结束时间
     * */
    private String day;
    /**
     * 附件
     * */
    private String url;



}
