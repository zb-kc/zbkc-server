package com.zbkc.model.vo;

import lombok.Data;

@Data
public class HouseCodeVo {
    /**
     * 已补充编码
     * */
    private int haveCode;
    /**
     *
     * 无编码
     * */
    private int noCode;
    /**
     *
     * 不符合条件编码
     * */
    private int ineligibleCode;

}
