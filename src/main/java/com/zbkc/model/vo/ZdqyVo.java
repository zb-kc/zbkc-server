package com.zbkc.model.vo;

import lombok.Data;

@Data
public class ZdqyVo {
    /**
     * 领导挂点企业
     * */
    public int ldgdNum;
    /**
     * 工业百强
     * */
    public int gybqNum;
    /**
     * 服务业百强
     * */
    public int fwybqNum;
    /**
     * 建筑业五十强
     * */
    public int jzyNum;
    /**
     * 纳税百强
     * */
    public int lsbqNum;
    /**
     * 上市公司
     * */
    public int shqyNum;


}
