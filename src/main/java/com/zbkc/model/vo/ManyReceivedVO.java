package com.zbkc.model.vo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author caobiyang
 * @Description 多户收款返回实体类
 * @date 2021-11-10
 * */
@Data
public class ManyReceivedVO {
    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 合同id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long contractId;

    /**
     * 账单日期  yyyy.mm
     */
    private String billTime;

    /**
     * 月租金(元)
     */
    private BigDecimal monthlyRent;

    /**
     * 应收金额
     */
    private BigDecimal amountReceivable;

    /**
     * 实收金额
     */
    private BigDecimal actualCollection;


    /**
     * 待缴滞纳金
     */
    private BigDecimal waitPayment;


    /**
     * 租赁物业
     */
    private String leaseWy;


}

