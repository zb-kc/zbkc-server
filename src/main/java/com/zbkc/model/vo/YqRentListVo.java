package com.zbkc.model.vo;

import lombok.Data;

@Data
public class YqRentListVo {
    /**
     * 数量
     *
     */
    private int num;
    /**
     * 年份
     * */
    private String year;
}
