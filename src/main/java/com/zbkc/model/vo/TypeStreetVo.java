package com.zbkc.model.vo;

import lombok.Data;

@Data
public class TypeStreetVo {
    /**
     * 类别
     * */
    private String type;
    /**
     * 街道名称
     * */
    private String streetName;
}
