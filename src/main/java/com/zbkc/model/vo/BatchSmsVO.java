package com.zbkc.model.vo;

import lombok.Data;

import java.util.List;


/**
 * @date 2021/9/15
 * @author caobiyang
 */
@Data
public class BatchSmsVO {

    /**
     * 租赁方物业个数
     */
    private Integer wySize;

    /**
     * 短信模板类型
     */
    private  Integer type;

    /**
     * 短信名称
     */
    private List<String> name;


    /**
     * 短信内容
     */
    private  String word;

}
