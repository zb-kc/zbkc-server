package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

/**
 * 选择物业资产页面-物业信息VO
 * @author ZB3436.xiongshibao
 * @date 2021/10/25
 */
@Data
public class WyBaseInfoVO {

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 父节点
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long pid;

    /**
     * 物业名称
     */
    private String wyName;

    /**
     * 物业主体 园区-楼栋等
     */
    private String wyType;

    /**
     * 物业规模
     */
    private String wySize;

    /**
     * 物业标签
     */
    private String wyLabel;

    /**
     * 地址
     */
    private String wyAddress;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 租赁面积
     */
    private String rentalArea;

    /**
     * 单价
     */
    private String rentPrice;

    /**
     * 月租金
     */
    private String monthlyRent;

    /**
     * 主要用途
     */
    private String mainPurposStr;

    /**
     * 建筑面积
     */
    private double buildArea;

    /**
     * 公摊面积
     */
    private double shareArea;

    /**
     * 子类
     */
    private List<WyBaseInfoVO> children;


    /**
     * 房屋编号
     */
    private String houseCode;

    /**
     * 社区编码
     */
    private String areaCode;

    /**
     * 下载合同模板用
     */
    private String address;

    /**
     * 物业类型 1园区 2楼栋 3楼层 4单元 5室 6零散 7楼栋(主体)
     */
    private Integer type;

}
