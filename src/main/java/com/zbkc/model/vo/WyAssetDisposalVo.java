package com.zbkc.model.vo;


import com.zbkc.model.po.*;
import lombok.Data;
import java.util.List;
import java.util.Map;

/**
 * @author yangyan
 * @date 2021/09/08
 */
@Data
public class WyAssetDisposalVo {
    /**
     * 传输对象
     */
    private WyAssetDisposal wyAssetDisposal;


    /**
     * 处置方式
     */
    private SysDataType solvingMethodId;

    /**
     * 自定义附件
     */
    private Map<String,List<SysFilePath1>> custRightFile;

}
