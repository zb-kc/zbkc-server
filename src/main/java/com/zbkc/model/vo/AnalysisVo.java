package com.zbkc.model.vo;

import lombok.Data;

import java.util.List;

@Data
public class AnalysisVo {

    /*
    * 政府及国企物业产业用房数统计
    * 政府
    * */
    private  int zfNum;
    /*
     * 政府及国企物业产业用房数统计
     * 政府
     * */
    private  int gqNum;

    /*
     * 国企
     * */
    private List<AnalysisListVo> gqList;
    /*
     * 政府
     * */
    private List<AnalysisListVo> zfList;







}
