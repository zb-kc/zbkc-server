package com.zbkc.model.vo;

import lombok.Data;

/**
 * 业务流程状态
 * @author ZB3436.xiongshibao
 */
@Data
public class YwProcessInfoVO {

    /**
     * 节点id
     */
    private String taskId;
    /**
     * 流程进度
     */
    private String process;

    /**
     * 经办人
     */
    private String agentName;

    /**
     * 操作时间
     */
    private String operatingTime;

    /**
     * 处理决定
     */
    private String decision;
    /**
     * 经办意见
     */
    private String opinion;

    /**
     * 业务id
     */
    private String businessId;

    /**
     * 代办类型 1移交用房申请；2物业资产申报; 3合同签署申请； 4 合同签署申请委非托运营类
     */
    private String applyType;

    /**
     * 合同签署类型-运行类-非运营类
     */
    private String contractSignType;

    /**
     * 审批结果-同意-驳回-终止流程
     */
    private String result;
    /**
     * 节点备注-审批意见
     */
    private String comment;

}
