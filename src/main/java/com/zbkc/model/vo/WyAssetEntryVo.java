package com.zbkc.model.vo;

import com.zbkc.model.po.*;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author yangyan
 * @date 2020/09/06
 *
 */
@Data
public class WyAssetEntryVo {
    /**
     * 传输对象
     */
    private WyAssetEntry wyAssetEntry;

    /**
     * 入账类型 数据字典值
     */
    private SysDataType entryType;

    /**
     * 南山区政府产权登记表
     */
    private List<SysFilePath1> propertyRegistrationFile;

    /**
     * 发改立项批复
     */
    private  List<SysFilePath1> projectApprovalFile;

    /**
     * 财政批复
     */
    private  List<SysFilePath1> financialApprovalFile;

    /**
     * 审计报告
     */
    private  List<SysFilePath1> auditReportFile;

    /**
     * 其他材料
     */
    private  List<SysFilePath1> otherMaterialsFile;

    /**
     * 自定义材料 文件
     */
    private Map<String,List<SysFilePath1>> custRightFile;
}
