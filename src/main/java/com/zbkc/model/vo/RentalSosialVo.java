package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysImg;
import com.zbkc.model.po.SysImg1;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 *  租赁社会物业返回类
 * @author caobiyang
 * @since  2021-10-11
 */
@Data
public class RentalSosialVo {

    /**
     * 物业id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     *租赁物业名称
     */
    private String name;

    /**
     * 别名/曾用名
     *
     * */
    private String aliasName;

    /**
     * 物业编号
     * */
    private String propertyCode;

    /**
     * 楼栋编码
     */
    private String houseCode;

    /**
     * 地址
     */
    private String address;

    /**
     * 社区编码
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long areaCode;

    /**
     * 详细地址
     */
    private String detailAddr;

    /**
     * 租赁用途
     */
    private String purpose;

    /**
     * 租赁面积
     */
    private BigDecimal zlRentalArea;

    /**
     * 租赁起始日期
     */
    private String  zlStartRentDate;

    /**
     * 租赁截止日期
     */
    private String  zlEndRentDate;

    /**
     * 月租金
     */
    private BigDecimal zlMonthlyRent;

    /**
     * 租金单价
     */
    private BigDecimal zlRentUnitPrice;

    /**
     * 出租方
     */
    private String lessor;

    /**
     * 出租联系方式
     */
    private String lessorPhone;

    /**
     * 物业图片id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyImgId;

    /**
     *户型图片id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long houseImgId;

    /**
     * 物业公司名称/租赁物业单位
     */
    private String companyName;

    /**
     * 层高
     * */
    private Integer storeyHeight;

    /**
     * 承重
     * */
    private Double loadBearer;

    /**
     * 朝向 1.东2.南3.西4.北
     */
    private Integer towards;

    /**
     * 内三维
     */
    private String threeDimen;

}
