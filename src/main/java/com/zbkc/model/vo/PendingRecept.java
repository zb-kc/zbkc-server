package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author yangyan
 * @date 2021/09/18
 * <p>
 *    待收金额
 * </p>
 */
@Data
public class PendingRecept {

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 合同id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long contractId;

    /**
     * 账单日期  yyyy.mm
     */
    private String billTime;

    /**
     * 月租金(元)
     */
    private BigDecimal monthlyRent;

    /**
     * 已缴纳租金
     */
    private BigDecimal paidRent;
    /**
     * 应收金额
     */
    private BigDecimal amountReceivable;

    /**
     * 实收金额
     */
    private BigDecimal actualCollection;

    /**
     * 待缴滞纳金
     */
    private BigDecimal waitPayment;





}
