package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author yangyan
 * @date 2021/09/16
 */
@Data
public class YyDepositHome {

    /**
     * id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 承租房
     */
    private String leaser;

    /**
     * 租赁物业地址
     */
    private String  wyAddress;

    /**
     * 实收押金
     */
    private BigDecimal depositAmount;

    /**
     * 缴费状态  缴费状态 1.未缴费、2.部分缴费、3.已缴费、4.欠缴费、5.已补缴、6.已缴费(预缴费转)
     */
    private Integer paymentStatus;

    /**
     * 缴费状态中文 便于导出  未缴费、部分缴费、已缴费、欠缴费、已补缴、已缴费(预缴费转)
     */
    private String paymentStatusStr;

    /**
     * 物业id
     */
    private Long wyId;

}
