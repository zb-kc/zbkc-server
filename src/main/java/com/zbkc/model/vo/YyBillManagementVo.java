package com.zbkc.model.vo;

import com.zbkc.model.po.SysFilePath1;
import com.zbkc.model.po.YyUseRegFilePath1;
import lombok.Data;

import java.util.List;

/**
 * @author yangyan
 * @date 2021/09/16
 */
@Data
public class YyBillManagementVo {
    /**
     * 所有字段
     */
    private AllWordVo allWordVo;

    /**
     * 缴费记录
     */
    private List<FeeRecord> feeRecord;

    /**
     * 合同附件
     */
    private List<SysFilePath1> filePaths;
}
