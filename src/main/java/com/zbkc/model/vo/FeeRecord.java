package com.zbkc.model.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author yangyan
 * @date 2021/09/27
 */
@Data
public class FeeRecord {

    /**
     * id
     */
    private Long id;
    /**
     * 开票日期 转化收款日期
     */
    private Timestamp invoiceDate;
    /**
     * 收款操作人
     */
    private String collecPerson;
    /**
     * 收费项目
     */
    private String project;
    /**
     * 计费周期开始时间
     */
    private Timestamp startTime;
    /**
     * 计费周期结束时间
     */
    private Timestamp endTime;

    /**
     * 实收金额
     */
    private BigDecimal actualCollection;

    /**
     * 收款方式
     */
    private Integer paidMethod;

    /**
     * 备注
     */
    private String  remark;
}
