package com.zbkc.model.vo;

import lombok.Data;

import java.util.List;

@Data
public class ZfAnalysisVo {
    /**
     * 空置情况统计分析
     * */
    public  KZVo kzList;
    /**
     * 入账统计
     * */
    public RZVo rzList;

    /**
     * 产权统计
     * */
    public CqVo cqList;

    /**
     * 房屋编码统计
     * */
    public  HouseCodeVo houseCodeList;

    /**
     * 单位物业资产前五统计
     * */
    public List<ZcVo> zcList;

    /**
     * 租期限5~10年
     * */

    public int zq5Year;

    /**
     * 租期限10~15年
     * */

    public int zq10Year;
    /**
     * 租期限15
     * */

    public int zq15Year;

    /**
     * 预到期 <=1
     * */

    public int zq1Month;

    /**
     * 预到期 >1 <=3
     * */

    public int zq3Month;
    /**
     * 预到期 >=3 <=6
     * */

    public int zq6Month;




}
