package com.zbkc.model.vo;

import lombok.Data;

@Data
public class CyDetailsHouseInfoVo {
    /**
     * 建筑id
     * */
    private String buidId;
    /**
     * 楼栋名称
     * */
    private String name;

    /**
     * 建筑名称
     * */
    private String buildName;
    /**
     * 楼栋面积
     * */
    private Double doorArea;
    /**
     * 建筑面积
     * */
    private Double buildTotalArea;


    /**
     *已出租面积
     * */
    public Double rentedArea;
    /**
     * 空置面积
     * */
    public Double vacantArea;
    /**
     * 平均租金
     * */
    public Double avgPrice;
    /**
     * 入驻企业数
     * */
    public int qyCount;

}
