package com.zbkc.model.vo;

import lombok.Data;

@Data
public class WyAssetsListVo {

    /**
     * 名称
     * */
    private String name;
    /*
     *企业编码
     * */
    private String  code;
    /**
     * 面积
     * */
    private String area;
    /**
     * id
     * */
    private String id;

    /**
     * 产业用房,公共服务设施,住宅,办公用房,商业
     * */
    private String type;

}
