package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 租赁社会物业返回封装
 * @author caobiyang
 * @date 2021/10/08
 */
@Data
public class RentalSocialWyVo {

    /**
     * 物业id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     *租赁物业名称
     */
    private String name;

    /**
     * 物业地址
     */
    private String address;

    /**
     * 物业详细地址
     */
    private String detailAddr;

    /**
     * 租赁面积
     */
    private Double zlRentalArea;

    /**
     * 租赁用途
     */
    private String purpose;

    /**
     * 月租金
     */
    private BigDecimal zlMonthlyRent;
}
