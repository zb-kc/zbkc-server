package com.zbkc.model.vo;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.YyBillManagement;
import com.zbkc.model.po.YyUseRegFilePath1;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author yangyan
 * @date 2020/09/14
 */
@Data
public class AllWordVo {

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

//    /**
//     * 合同管理id
//     */
//    @JsonSerialize(using= ToStringSerializer.class)
//    private Long yyContractId;

    /**
     * 账单日期  yyyy.mm
     */
    private String billTime;

    /**
     * 账单状态  1:未缴费;2已缴费 ,3欠缴费  默认1
     */
    private Integer billStatus;

    /**
     * 预收款余额
     */
    private BigDecimal advanceCollection;

    /**
     * 逾期天数
     */
    private Integer overDay;

    /**
     * 已缴滞纳金
     */
    private BigDecimal overduePayment;

    /**
     * 待缴滞纳金
     */
    private BigDecimal waitPayment;

    /**
     * 月租金(元)
     */
    private BigDecimal monthlyRent;

    /**
     * 已缴纳租金
     */
    private BigDecimal paidRent;

    /**
     * 首期租金
     */
    private BigDecimal firstPhaseRent;

    /**
     * 租金差价
     */
    private BigDecimal diffRent;

    /**
     * 应收金额(元)
     */
    private BigDecimal amountReceivable;

    /**
     * 实收金额(元)
     */
    private BigDecimal actualCollection;

    /**
     * 缴费方式 1：微信支付  2：支付宝支付 3：其它
     */
    private Integer paidMethod;

    /**
     * 缴费时间
     */
    private String paidTime;

    /**
     * 缴费状态  缴费状态 1.未缴费、2.部分缴费、3.已缴费、4.欠缴费、5.已补缴、6.已缴费(预缴费转)
     */
    private Integer paymentStatus;

    /**
     * 收款操作人
     */
    private String collecPerson;

    /**
     * 备注
     */
    private String remark;

    /**
     * 开票状态 1:未开票;2已开票 默认1
     */
    private Integer invoiceStatus;

    /**
     * 开票日期
     */
    private String invoiceDate;

    /**
     * 发票编码
     */
    private String invoiceCode;

    /**
     * 开票操作人
     */
    private String invoiceOperator;



    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 承租方
     */
    private String leaser;
    /**
     * 出租物业地址
     */
    private String wyAddress;

    /**
     * 物业电话
     */
    private String  wyPhone;
    /**
     * 物业名称
     */
    private String  wyName;

    /**
     * 承租方电话
     */
    private String leaserPhone;
    /**
     * 催收电话
     */
    private String collecPhone;
    /**
     * 合同id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long contractId;
//    /**
//     * 模板类型 1产业用房;2商业; 3住宅 ;4 南山软件园
//     */
//    private String templateType;
    /**
     * 起租时间
     */
    private String startRentDate;
    /**
     * 止租时间
     */
    private String endRentDate;
    /**
     * 免租期
     */
    private String freeRent;
    /**
     * 合同 sys_file_path字段db_file_id
     */
    private Long contractFilePathId;

    /**
     * 合同编码
     */
    private String contractCode;

    /**
     * 物业类型
     */

    private String wyType;

    /**
     * 物业id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyId;


}
