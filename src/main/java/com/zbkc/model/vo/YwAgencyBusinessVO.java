package com.zbkc.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author ZB3436.xiongshibao
 * @date 2021-10-26
 */
@Data
public class YwAgencyBusinessVO {

    @JsonSerialize(using= ToStringSerializer.class)
    private long id;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 网签类型 1新签 2续签
     */
    private Integer webSignType;

    /**
     * 申请单位
     */
    private String applicantUnit;

    /**
     * 统一社会信用代码
     */
    private String socialUniformCreditCode;

    /**
     * 物业名称
     */
    private String wyName;

    /**
     * 租赁范围
     */
    private String leaseArea;

    /**
     * 是否经济承诺贡献 1是 2否
     */
    private Integer isCommitment;

    /**
     * 申请业务类型 1产业用房租赁 2商业用房租赁 3住宅用房租赁 4南山软件园租赁
     */
    private Integer businessType;

    /**
     * 授权状态/流程状态 0刚授权 1企业申请 2企业撤回 3管理员取消授权
     */
    private Byte recordStatus = 0;

    /**
     * 营业执照地址
     */
    private String businessLicenseAddress;
    /**
     * 备注
     */
    private String remark;

    /**
     * 排序类型
     * 1 webSignType  升序 2 webSignType降序
     * 3 businessType 升序 4 businessType降序
     * 5 recordStatus 升序 6 recordStatus降序
     */
    private Long sort;


    /**
     * 属性关联的名称 用于导出excel的标题
     */
    @JsonIgnore
    public Map<String , String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
        keyMap.put("id" , "序号");
        keyMap.put("webSignTypeStr" , "网签类型");
        keyMap.put("applicantUnit" , "申请单位");
        keyMap.put("socialUniformCreditCode" , "统一社会信用代码");
        keyMap.put("wyName" , "物业名称");
        keyMap.put("leaseArea" , "租赁范围");
        keyMap.put("isCommitmentTypeStr" , "是否经济承诺贡献");
        keyMap.put("businessTypeStr" , "申请业务类型");
        keyMap.put("businessLicenseAddress" , "营业执照地址");
        keyMap.put("remark" , "备注");
    }

    private String webSignTypeStr;
    private String businessTypeStr;
    private String isCommitmentTypeStr;

    /**
     * 经济承诺贡献
     */
    private String economicCommit;


}
