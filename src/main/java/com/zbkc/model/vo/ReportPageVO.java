package com.zbkc.model.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 分页模糊查询后 返回包封装
 * @author yangyan
 * @date 2021/08/10
 */
@Data
public class ReportPageVO<T> {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    private List<T> list;
    private Integer total;
    private Integer p;
    private Integer size;
    /**
     * 总租赁面积
     */
    private BigDecimal totalRentalArea;

    /**
     * 租赁物业数
     */
    private Integer wyLeaseNum;

    /**
     * 总月租金
     */
    private BigDecimal totalMonthlyRent;

    /**
     * 有经济贡献率数
     */
    private Integer contributionNum;

    /**
     * 空置面积
     */
    private BigDecimal vacantArea;

    /**
     * 空置物业数
     */
    private Integer  wyVacantNum;
}
