package com.zbkc.model.vo;

import lombok.Data;

@Data
public class RZVo {

    /**
     * 产业用房金额
     * */
    private Double cyNum;
    /**
     * 办公用房金额
     * */
    private Double bgNum;
    /**
     * 公共服务设施金额
     * */
    private Double ggNum;
    /**
     * 商业金额
     * */
    private Double syNum;
    /**
     * 住宅金额
     * */
    private Double zzNum;



}
