package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author yangyan
 * @date 2021/09/18
 * <p>
 *     租赁申请返回对象
 * </p>
 */
@Data
public class LeaseDetailVo {
    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 物业名称
     */
    private String wyName;

    /**
     * 资产类型(物业类型):  产业用房；孵化园区；办公用房；公共服务设施；商业；住宅；租赁社会物业
     */
    private String assetType;

    /**
     * 物业地址
     */
    private String address;
    /**
     * 出租面积 租赁面积
     */
    private BigDecimal rentalArea;
    /**
     * 租金单价
     */
    private BigDecimal rentUnitPrice;

    /**
     * 承租方
     */
    private String leaser;

    /**
     * 起租时间
     */
    private String startRentDate;

    /**
     * 止租时间
     */
    private String endRentDate;

    /**
     * 租赁年限
     */
    private Integer year;

    /**
     * 备注
     */
    private String remark;

    /**
     * 月租金
     */
    private BigDecimal zlMonthlyRent;

}
