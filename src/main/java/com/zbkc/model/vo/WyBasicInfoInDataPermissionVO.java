package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysDataType;
import com.zbkc.model.po.WyBasicInfoInDataPermissionPO;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/11/10
 */
@Data
public class WyBasicInfoInDataPermissionVO {
    /**
     * 当前已选的
     */
    private List<WyBasicInfoInDataPermissionPO> chooseList;
    /**
     * 所有物业list集合
     */
    private List<WyBasicInfoInDataPermissionPO> allWyInfoList;

}
