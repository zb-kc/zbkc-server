package com.zbkc.model.vo;

import lombok.Data;
import org.springframework.stereotype.Repository;

@Data
@Repository
public class QyDetailsVo {
    /**
    * 企业法定代表人、挂点领导
    * */
    private  String name;

    /**
     * 挂点领导
     * */
    private  String gdname;


    /**
     * 电话
     * */
    private  String phone;
    /**
     * 注册资金
     * */
    private String zcMoney;
    /**
     * 申报实收资本
     * */
    private String sbMoney;
    /**
     *参保人数
     */
    public int cbNum;
    /**
     * 成立时间
     * */
    private String time;

    /**
     * 成立代码
     * */
    private String code;
    /**
     * 规模
     * */
    private String scale;

    /**
     * 扶持资金
     * */
    private Double fcMoney;

    /**
     * 人才总数
     * */
    private String rcNum;

    /**
     * 租凭信息
     * */
    private String zpNum;

    /**
     * 扶持人才房
     * */
    private String fcrcNum;
    /**
     * label企业标签
     * */
    private String label;

    /**
     * 地址
     * */
    private String address;




}
