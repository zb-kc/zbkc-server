package com.zbkc.model.vo;

import lombok.Data;

import java.util.List;

/**
 * 标签实体传参
 * @author gmding
 * @date 2021/08/23
 */
@Data
public class TagVo<T> {
    private List<T> list;
    private Long id;
}
