package com.zbkc.model.vo;

import com.zbkc.model.po.SysFilePath;
import com.zbkc.model.po.SysFilePath1;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author yangyan
 * @date 2021/09/16
 * <p>
 *
 * </p>
 */
@Data
public class DepositDetail {

    /**
     * 承租房
     */
    private String leaser;

    /**
     * 租赁物业地址
     */
    private String  wyAddress;

    /**
     * 承租方电话
     */
    private String leaserPhone;

    /**
     * 资产类型(物业类型):  产业用房；孵化园区；办公用房；公共服务设施；商业；住宅；租赁社会物业
     */
    private String assetType;

    /**
     * 催收电话
     */
    private String collecPhone;

    /**
     * 起租时间
     */
    private String startRentDate;

    /**
     * 止租时间
     */
    private String endRentDate;

    /**
     * 免租期
     */
    private String freeRent;

    /**
     * 合同 sys_file_path字段db_file_id
     */
    private Long contractFilePathId;

    /**
     * 合同编码
     */
    private String contractCode;

    /**
     * 实收押金
     */
    private BigDecimal depositAmount;

    /**
     * 押金状态
     */
    private Integer depositStatus;

    /**
     * 备注
     */
    private String remark;

    /**
     * 合同附件
     */
    private List<SysFilePath1> contractFile;


}
