package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import java.math.BigDecimal;

/**
 * @author yangyan
 * @date 2021/09/18
 * <p>
 *     空置明细返回对象
 * </p>
 */
@Data
public class EmptyDetailVo {

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 物业名称
     */
    private String wyName;

    /**
     * 物业种类
     */
    private String assetType;

    /**
     * 物业地址
     */
    private String address;

    /**
     * 出租面积 租赁面积
     */
    private BigDecimal rentalArea;

    /**
     * 备注
     */
    private String remark;


}
