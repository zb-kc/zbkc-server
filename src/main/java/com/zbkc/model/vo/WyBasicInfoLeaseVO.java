package com.zbkc.model.vo;

import lombok.Data;
import java.math.BigDecimal;
import java.util.List;


/**
 * @author yangyan
 * @date 2021/10/28
 */
@Data
public class WyBasicInfoLeaseVO {

    /**
     *承租方通信地址   与物业地址一致
     */

    private List<String> leaserAddress;

    /**
     * 房屋编码
     */
    private List<String> houseCode;

    /**
     * 出租面积 建筑面积累加
     */
    private Double rentArea;

    /**
     * 套内建筑面积
     */
    private Double setArea;

    /**
     * 公摊面积
     */
    private Double shareArea;


    /**
     * 月租金  出租面积*租金单价
     */
//    private BigDecimal monthlyRent;

}
