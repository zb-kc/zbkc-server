package com.zbkc.model.vo;

import lombok.Data;

@Data
public class EnterpriseSidewalkVo {
    private String b;
    private String l;
    private String k;
    private String x;
    private String y;
    private String e;
    private String contri2021;
    private String g;
    private String tax2021;


}
