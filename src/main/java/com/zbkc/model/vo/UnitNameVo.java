package com.zbkc.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 *
 * @author yangyan
 * @date 2021/08/30
 */
@Data
public class UnitNameVo {
    /**
     * 主体信息id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 房地产名称 与 主体名称相同
     */
    private String  unitName;
}
