package com.zbkc.model.vo;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;


/**
 * @date 2021/9/15
 * @author caobiyang
 */
@Data
public class CallRecordVO {


    /**
     * 物业名称
     */
    private String wyName;

    /**
     * 承租方
     */
    private String leaser;

    /**
     * 短信模板类型
     */
    private  Integer type;

    /**
     * 短信名称
     */
    private String smsName;

    /**
     * 短信名称集合
     */
    private List<String> smsNameList;

    /**
     * 短信内容
     */
    private  String word;

    /**
     * 发送短信时间
     */
    private String  sendTime;

    /**
     * 当前月
     */
    private String currentMonth;

    /**
     * 短信发送单位（物业管理单位）
     */
    private String manageUnit;
}
