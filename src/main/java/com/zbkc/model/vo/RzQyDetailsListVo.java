package com.zbkc.model.vo;

import lombok.Data;

@Data
public class RzQyDetailsListVo {
    /**
     * id
     * */
    private String id;
    /**
     * 名称
     * */
    private String name;
    /*
    *租赁物业数量
    * */
    private int wyNum;
    /**
     * 人才房数
     * */
    private int rcfNum;
    /**
     * 人才数
     * */
    private int rcNum;
    /**
     * 扶持次数
     * */
    private int fcNum;
    /**
     * 企业编码
     * */
    private String code;

    /**
     * 房号
     * */
    private String roomNumber;
    /**
     * 建筑面积
     * */
    private String buildArea;
    /**
     * 产值（亿元）
     * */
    private String cz;
    /**
     * 纳税总额
     * */
    private String total;
    /**
     * 总人数
     * */
    private String bknum;


    /**
     * 财力贡献
     * */
    private String clgx;

    /**
     * 纳税（单位：元）
     * */
    private String ns;




}

