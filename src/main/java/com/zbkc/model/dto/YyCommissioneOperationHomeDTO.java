package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * @author yangyan
 * @date 2020/09/11
 */
@Data
public class YyCommissioneOperationHomeDTO {
    /**
     * 页码
     * */
    private Integer p;
    /**
     * 大小
     * */
    private Integer size;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 项目名称
     */
    @Length(max = 100 , message = "项目名称限制长度100个字符")
    private String name;

    /**
     * 运营单位名称
     */
    @Length(max = 50 , message = "运营单位名称限制长度50个字符")
    private String operationUnit;

    /**
     * 使用起始 开始时间
     */
    private String startTime;

    /**
     * 使用起始 结束时间
     */
    private String endTime;

    /**
     *  列表按字段排序：1-委托运营面积升序 2-委托运营面积降序 3-运营开始日期升序 4-运营开始日期降序 5-运营截止日期升序 6-运营截止日期降序
     */
    private Integer sort;


}
