package com.zbkc.model.dto.homeList;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @author yangyan
 * @date 2021/09/10
 */
@Data
public class YyUseRegHomeList {

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 物业名称
     */
    private String name;


    /**
     * 建筑面积
     */
    private Double buildingArea;

    /**
     * 使用对象名称
     */
    private String objName;

    /**
     * 使用方式 1出租; 2调配
     */
    private Integer method;

    /**
     * 使用开始
     */
    private String startTime;

    /**
     * 使用结束
     */
    private String endTime;

    /**
     * 使用状态 1 在用; 2停用
     */
    private Integer status;

    /**
     * 使用方式中文 方便导出：出租、调配
     */
    private String methodStr;

    /**
     * 使用状态中文 方便导出：在用、调用
     */
    private String statusStr;

    /**
     * 使用起止日期 方便导出
     */
    private String startTimeAndEndTime;
}
