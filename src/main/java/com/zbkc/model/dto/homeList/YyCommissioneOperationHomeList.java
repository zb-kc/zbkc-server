package com.zbkc.model.dto.homeList;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @author yangyan
 * @date 2020/09/11
 */
@Data
public class YyCommissioneOperationHomeList {
    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 项目名称
     */
    private String name;

    /**
     *委托运营面积
     */
    private Double buildArea;

    /**
     * 运营单位名称
     */
    private String operationUnit;

    /**
     * 使用起始 开始时间
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Timestamp startTime;

    /**
     * 使用起始 结束时间
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Timestamp endTime;

    /**
     * 使用起止时间 便于导出
     */
    private String startTimeAndEndTime;
}
