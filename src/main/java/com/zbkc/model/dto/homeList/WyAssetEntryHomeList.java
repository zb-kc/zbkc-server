package com.zbkc.model.dto.homeList;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import java.math.BigDecimal;

/**
 * @author yangyan
 * @date 2021/09/06
 */
@Data
public class WyAssetEntryHomeList {

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 资产名称
     */
    private String name;
    /**
     * 入账类型 id
     */
    private String entryType;
    /**
     * 入账金额
     */
    private BigDecimal entryMoney;

    /**
     * 入账时间
     */
    private String entryTime;
}
