package com.zbkc.model.dto.homeList;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * 查询返回实体类
 * @author gmding
 * @date 2021/08/20
 */
@Data
public class WyBasicInfoHomeList {
    /**
     * id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 资产类类型
     */
    private String assetType;

    /**
     * 物业名称
     * */
    private String name;
    /**
     * 物业规模
     */
    private String propertyScaleName;

    /**
     * 物业种类
     * */
    private  String propertyTypeName;
    /**
     * 建筑面积
     * */
    private Double buildArea;

    /**
     *街道
     * */
    private String street;

    /**
     *社区
     * */
    private String committee;

    /**
     * 状态
     * */
    private String propertyStatusName;

    /**
     *物业权属
     * */
    private String propertyTagName;

    /**
     * 物业种类型 类型 1园区 2楼 3单元 4楼层 5室 6零散 7楼栋(主体)
     * */
    private Integer type;

    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 空置面积
     */
    private Double emptyArea;

}
