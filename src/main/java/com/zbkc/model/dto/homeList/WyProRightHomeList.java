package com.zbkc.model.dto.homeList;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @author yangyan
 * @date 2021/09/01
 */
@Data
public class WyProRightHomeList {

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 房地产名称
     */
    private String name;

    /**
     * 产权证书号
     */
    private String certificateNo;

    /**
     * 权利人
     */
    private String obligee;

    /**
     * 权利类型
     */
    private String rightTypeName;

    /**
     * 建筑面积
     */
    private Double buildingArea;

    /**
     * 使用期限 开始时间
     */
    private Timestamp useLimitStart;

    /**
     * 使用期限 结束时间
     */
    private Timestamp useLimitEnd;

    /**
     * 使用期限（方便导出）
     */
    private String termStr;
}
