package com.zbkc.model.dto.homeList;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @author yangyan
 * @date 2020/09/07
 */
@Data
public class WyAssetDisposalHomeList {

    /**
     * id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 资产名称
     */
    private String name;
    /**
     * 批准文件号
     */
    private String documentCode;
    /**
     * 批准单位
     */
    private String authority;
    /**
     * 处置单位
     */
    private String disposalUnit;

    /**
     * 处置时间
     */
    private String disposalTime;
    /**
     * 处置方式
     */
    private String solvingMethod;
}
