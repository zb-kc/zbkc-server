package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.vo.PendingRecept;
import lombok.Data;

import javax.validation.constraints.Null;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author yangyan
 * @date 2021/09/14
 */
@Data
public class CollecMoneyDTO {

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 缴费方式 1：微信支付  2：支付宝支付 3：post机刷卡 4：转账 5：柜台刷卡 6：批扣 7:其它
     */
    private Integer paidMethod;

    /**
     * 备注
     */
    private String remark;

    /**
     * 账单状态
     */
    private Integer billStatus;

    /**
     * 待缴滞纳金
     */
    private BigDecimal waitPayment;

    /**
     * 缴费状态
     */
    private Integer paymentStatus;

    /**
     * 结余转预收款(预收款余额)
     */
    private BigDecimal advanceCollection;

}
