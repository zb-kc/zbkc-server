package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/10/13
 */
@Data
public class YwDevInfoDTO {

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 申请单位
     */
    private String unit;
    /**
     * 联系电话
     */
    private String phone;
    /**
     * 信用代码
     */
    private String code;
    /**
     * 地址
     */
    private String address;
    /**
     * 详细地址
     */
    private String detailAddr;
    /**
     * 地区编码 sys_area  committee_code社区字段
     */
    private Long areaCode;
    /**
     * 经度
     */
    private String longitude;
    /**
     * 纬度
     */
    private String latitude;
    /**
     * 法定代表人
     */
    private String legalName;
    /**
     * 法定代表人电话
     */
    private String legalPhone;
    /**
     * 法定代表证件类型
     */
    private String legalType;
    /**
     * 法定证件号码
     */
    private String legalCode;
    /**
     * 委托代理人
     */
    private String agentName;
    /**
     * 委托代理人手机号码
     */
    private String agentPhone;
    /**
     * 委托代理人身份类型
     */
    private String agentType;
    /**
     * 委托代理人身份证号
     */
    private String agentCode;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 土地合同号
     */
    private String contractCode;
    /**
     * 合同签订日期
     */
    private Timestamp contractTime;
    /**
     * 补充协议号
     */
    private String agreementCode;
    /**
     * 协议签订日期
     */
    private Timestamp contractSignTime;
    /**
     * 用地规划许可证号
     */
    private String planPermitCode;
    /**
     * 区政府同意规划条件的涵
     */
    private String governmentPlan;
    /**
     * 签发日期
     */
    private Timestamp issueTime;
    /**
     * 项目实施监管协议
     */
    private String agreement;
    /**
     * 宗地号
     */
    private String parcelNo;
    /**
     * 建筑面积
     */
    private BigDecimal buildArea;
    /**
     * 用地面积
     */
    private BigDecimal landArea;
    /**
     * 土地位置(五级联动)
     */
    private String landLocation;
    /**
     * 土地位置 详细位置
     */
    private String landLocationDetail;

    /**
     * 附件材料审核状态
     */
    private List<YwHandoverDetailsDTO> ywHandoverDetailsDTOList;

    /**
     * 是否能修改 true能修改 false不能
     */
    private Boolean canModify;

}
