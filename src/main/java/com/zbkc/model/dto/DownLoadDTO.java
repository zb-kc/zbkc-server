package com.zbkc.model.dto;

import com.zbkc.model.po.YyContract;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class DownLoadDTO {
    /**
     * 合同对象信息
     */
    private YyContract info;

    /**
     *房屋编码
     */
    private String houseCode;

    /**
     * 月租金
     */
    private BigDecimal monthlyRent;

    /**
     * 租赁面积
     */
    private Double rentalArea;

    /**
     * 建筑面积
     */
    private Double buildArea;

    /**
     * 物业地址
     */
    private String detailAddr;

    /**
     * 公摊面积
     */
    private Double shareArea;

    /**
     * 套内面积
     */
    private Double insideArea;

    /**
     * 社区编码
     */
    private String areaCode;

    /**
     * 物业名称
     */
    private String name;
}
