package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * @author caobiyang
 * @date 2021/10/15
 */
@Data
public class DepositDTO {
    /**
     * 页码
     */
    private int p;
    /**
     * 行数
     */
    private int size;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;


    /**
     * 按照字段排序：1-实收押金升序 2-实收押金倒序
     */
    private Integer sort;

    /**
     * 承租方
     */
    @Length(max = 50 , message = "承租方限制长度50个字符")
    private String leaser;

    /**
     * 租赁物业地址
     */
    @Length(max = 100 , message = "租赁物业地址限制长度100个字符")
    private String  address;

    /**
     * 小实收押金
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal smallDepositAmount;
    /**
     * 大实收押金
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal bigDepositAmount;

    /**
     * 缴费状态  缴费状态 1.未缴费、2.部分缴费、3.已缴费、4.欠缴费、5.已补缴、6.已缴费(预缴费转)
     */
    private Integer paymentStatus;

}
