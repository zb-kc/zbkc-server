package com.zbkc.model.dto;

import lombok.Data;

@Data
public class WyListDTO {
    /**
     * 页码
     */
    private int p;
    /**
     * 行数
     */
    private int size;
    /**
     * 物业名称搜索
     */
    private String index;

    /**
     * 物业种类
     * */
    private String wyType;
    /**
     * 街道名称
     * */
    private String streetName;
    /**
     * 社区名称
     * */
    private String communityName;


    /**
     * 空置面积 起
     * */
    private String kzAreaStart;

    /**
     * 空置面积 始
     * */
    private String kzAreaEnd;

    /**
     * 平均租金 起
     * */
    private String avgPriceStart;

    /**
     * 平均租金 始
     * */
    private String avgPriceEnd;



}
