package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysFilePath;
import com.zbkc.model.po.SysImg;
import lombok.Data;

import java.util.List;
import java.util.Map;


/**
 * @author yangyan
 * @date 2020/09/07
 */
@Data
public class WyAssetDisposalDTO {
    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 资产名称id  基础信息表
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 批准文件号
     */
    private String documentCode;

    /**
     * 批准单位
     */
    private String authority;

    /**
     * 批准时间
     */
    private String approvalTime;

    /**
     * 处置单位
     */
    private String disposalUnit;

    /**
     * 处置时间
     */
    private String  disposalTime;

    /**
     * 处置方式 数据字典值
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long solvingMethodId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 自定义附件
     */
    private Map<String,List<SysFilePath>> custRightFile;



    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;
}
