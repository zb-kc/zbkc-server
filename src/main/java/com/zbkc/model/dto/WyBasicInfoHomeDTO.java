package com.zbkc.model.dto;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
/**
 * 查看大厦 条件实体类
 * @author gmding
 * @date 2021/08/20
 */
@Data
public class WyBasicInfoHomeDTO {
    /**
     * 页码
     * */
    private Integer p;
    /**
     * 大小
     * */
    private Integer size;

    /**
     * 物业名称
     * */
    private String name;


    /**
     * 资产类型
     * */
    private String assetType;


    /**
     * 物业种类（物业种类根据所在物业的不同）
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Integer propertyTypeId;

    /**
     * 物业标签
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyTagId;

    /**
     * 物业规模（仅限区分园区，楼栋主体，零散） 数据字典
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Integer propertyScaleId;
    /**
     * 街道
     * */
    private String streetName;
    /**
     * 社区
     * */
    private String committeeName;


    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;
    /**
     * 0 无排序 1 升序/ 2降序
     * */
    private int sort;




}
