package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysFilePath;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.util.List;
import java.util.Map;

/**
 *
 * @author yangyan
 * @date 2021/09/09
 */
@Data
public class YyUseRegDTO {

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 房地产名称   物业资产id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 使用方式 1出租; 2调配
     */
    private Integer method;
    /**
     * 是否免租 1是；2否
     */
    private Integer rentFree;

    /**
     * 使用状态 1 在用; 2停用
     */
    private Integer status;

    /**
     * 使用对象名称
     */
    @Length(max = 50 , message = "使用对象名称限制长度50个字符")
    private String objName;

    /**
     * 使用对象类型 1.企业/2.个人/3.个体工商户/4.行政事业单位/5.其他
     */
    private Integer objType;

    /**
     * 统一社会信用代码
     */
    @Length(max = 50 , message = "统一社会信用代码限制长度50个字符")
    private String creditCode;

    /**
         * 联系人姓名
     */
    @Length(max = 20 , message = "联系人姓名限制长度50个字符")
    private String contactName;

    /**
     * 联系方式
     */
    @Length(max = 50 , message = "联系方式限制长度50个字符")
    private String contactPhone;

    /**
     * 使用开始
     */
    private String startTime;

    /**
     * 使用结束
     */
    private String endTime;

    /**
     * 实际使用用途
     */
    @Length(max = 50 , message = "实际使用用途限制长度50个字符")
    private String actualUse;

    /**
     * 自定义文件
     */
    private Map<String,List<SysFilePath>> customFileMap;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 合同附件
     */
    private List<SysFilePath> contractFileList;

    /**
     * 使用批准附件
     */
    private List<SysFilePath> useApprovalFileList;
}
