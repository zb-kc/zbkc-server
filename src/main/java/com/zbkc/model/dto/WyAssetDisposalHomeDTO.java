package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author yangyan
 * @date 2020/09/07
 */
@Data
public class WyAssetDisposalHomeDTO {

    /**
     * 页码
     * */
    private Integer p;
    /**
     * 大小
     * */
    private Integer size;

    /**
     * 资产名称
     */
    private String name;
    /**
     * 批准文件号
     */
    private String documentCode;
    /**
     * 处置时间 开始时间
     */
    private String dispoalTimeStart;
    /**
     * 处置时间 结束时间
     */
    private String dispoalTimeEnd;
    /**
     * 处置方式
     */
    private Long solvingMethodId;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /***
     * 列表按照字段排序   1-处置时间升序  2-处置时间降序
     */
    private Integer sort;
}
