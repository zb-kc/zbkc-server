package com.zbkc.model.dto;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.*;
import lombok.Data;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 物业基本(主体)信息表 修改实体传参
 * </p>
 *
 * @author gmding
 * @since 2021-08-23
 */
@Data
public class WyBasicInfoDTO {
//    /**
//     * 列表id
//     * */
//    @JsonSerialize(using= ToStringSerializer.class)
//    private Long id;
    /**
     * 修改对象
     * */
    private WyBasicInfo info;
    /**
     *物业状态 数据字典值
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyStatus;
    /**
     * 物业规模 数据字典值
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyScale;
    /**
     * 物业权属 数据字典值
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyTag;
    /**
     * 物业种类 数据字典值
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] propertyType;

    /**
     * 物业来源 数据字典值
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] propertySource;

    /**
     * 主要用途 数据字典值
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long mainPurpos;

    /**
     * 物业标签id（政府）关联表label_manage 字段label_id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] wyTag;

    /**
     * 物业图片
     * */
    private List<SysImg> propertyImg;

    /**
     * 验收报告
     * */
    private List<SysFilePath> acceptanceReportFile;
    /**
     * 施工许可证
     */
    private List<SysFilePath> constructPermitFile;

    /**
     *
     * 建造自定义附件
     * */
    private Map<String,List<SysFilePath>> buildCustFile;

    /**
     * 土地出让合同
     * */
    private List<SysFilePath> landTransferContractFile;
    /**
     * 红线图或宗线图
     * */
    private List<SysFilePath> redLineFile;
    /**
     * 用地许可证
     * */
    private List<SysFilePath> landPermitFile;
    /**
     * 户型图
     * */
    private List<SysImg> houseImg;

    /**
     *
     * 用地自定义附件 sys_file_path字段db_file_id
     * */
    private Map<String,List<SysFilePath>> siteCustFile;
    /**
     * 附属设施信息
     * */
    private List<WyFacilitiesInfo>  wyFacilitiesInfo;

}
