package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

/**
 * @author yangyan
 * @date 2021/09/18
 * <p>
 *     账单首页请求参数
 * </p>
 */
@Data
public class BillManagementDTO {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     * 页码
     */
    private Integer p;
    /**
     * 行数
     */
    private Integer size;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 账期
     */
    private String billTime;

    /**
     * 按照字段排序 1-账期升序 2-账期降序 3-月租金升序 4-月租金降序 5-应收金额升序 6-应收金额降序 7-实收金额升序 8-实收金额降序
     *              9-发票状态排序（1未开在前）10-发票状态排序（2已开在前）
     */
    private Integer sort;


    /**
     * 小月租金(元)
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal smallMonthlyRent;

    /**
     * 大月租金(元)
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal bigMonthlyRent;


    /**
     * 小应收金额(元)
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal smallAmountReceivable;

    /**
     * 大应收金额(元)
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal bigAmountReceivable;

    /**
     * 缴费状态  缴费状态 1.未缴费、2.部分缴费、3.已缴费、4.欠缴费、5.已补缴、6.已缴费(预缴费转)
     */
    private Integer paymentStatus;

    /**
     * 开票状态 1:未开票;2已开票 默认1
     */
    private Integer invoiceStatus;

    /**
     * 租赁物业地址
     */
    @Length(max = 100 , message = "租赁物业地址限制长度100个字符")
    private String address;

    /**
     * 小实收金额(元)
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal smallActualCollection;

    /**
     * 大实收金额(元)
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal bigActualCollection;

    /**
     * 账单年份
     */
    private String  bYear;

    /**
     * 账单月份
     */
    private String bMonth;



}
