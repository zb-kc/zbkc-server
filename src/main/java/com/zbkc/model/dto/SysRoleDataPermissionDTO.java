package com.zbkc.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

/**
 *系统设置——角色管理_数据权限关联表 DTO
 * @author ZB3436.xiongshibao
 */
@Data
public class SysRoleDataPermissionDTO {

    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long roleId;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 物业idlist
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private List<Long> wyBasicInfoList;

    /**
     * 是否选择所有物业信息
     */
    private Boolean isAll;
}