package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * @author yangyan
 * @date 2021/10/26
 */
@Data
public class YyContractIndexDTO {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 页码
     */
    private Integer p;

    /**
     * 行数
     */
    private Integer size;

    /**
     * 承租方
     */
    @Length(max = 50 , message = "承租方限制长度50个字符")
    private String leaser;
    /**
     * 物业地址
     */
    @Length(max = 100 , message = "物业地址限制长度100个字符")
    private String address;
    /**
     * 租赁开始时间
     */
    private String startRentTime;
    /**
     * 租赁结束时间
     */
    private String entRentTime;

    /**
     * 合同类型 1产业用房;2商业; 3住宅 ;4 南山软件园;5 租赁社会
     */
    private Integer templateType;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 列表按照字段排序 1-月租金升序 2-月租金降序 3-租赁期限升序 4-租赁期限降序 5-租赁开始时间升序 6-租赁开始时间降序
     *  7-租赁结束时间升序 8-租赁结束时间降序
     */
    private Integer sort;
}
