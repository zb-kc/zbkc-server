package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;

/**
 *
 * @author yangyan
 * @date 2021/09/18
 * <p>
 *     空置明细
 * </p>
 */
@Data
public class EmptyDetailDTO {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     * 页码
     */
    private Integer p;
    /**
     * 行数
     */
    private Integer size;
    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 物业名称
     */
    @Length(max = 50 , message = "物业名称限制长度50个字符")
    private String wyName;

//    /**
//     * 物业种类
//     */
//    @JsonSerialize(using= ToStringSerializer.class)
//    private Long wyTypeId;

    /**
     * 资产类型(物业类型):  产业用房；孵化园区；办公用房；公共服务设施；商业；住宅；租赁社会物业
     */
    private String assetType;

    /**
     * 最低值面积
     */
    @Min(value = 0 , message = "")
    private Double smallArea;

    /**
     * 最高值面积
     */
    @Min(value = 0 , message = "")
    private Double bigArea;

    /**
     * 物业地址
     */
    @Length(max = 100 , message = "物业地址限制长度100个字符")
    private String address;

    /**
     * 按照字段排序
     * 1：租赁面积升序 2：租赁面积降序
     */
    private Integer sort;
}
