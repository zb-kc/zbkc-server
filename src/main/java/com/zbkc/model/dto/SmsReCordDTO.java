package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author caobiyang
 * @date 2021/09/16
 */
@Data
public class SmsReCordDTO {

    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 物业基础信息表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyId;
}
