package com.zbkc.model.dto;

import lombok.Data;

@Data
public class WyAssetsListDTO {
    /**
     * 页码
     */
    private int p;
    /**
     * 行数
     */
    private int size;
    /**
     * 地址
     */
    private String index;

    /**
     * 1 企业搜索 2物业搜索
     * */
    private int type;
}
