package com.zbkc.model.dto.export;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zbkc.model.dto.WyProRightHomeDTO;
import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author caobiyang
 * @date 2021/12/20
 * <p>
 *    导出产权登记请求类
 * </p>
 */
@Data
public class ExporProRightRegDTO {

    private WyProRightHomeDTO wyProRightHomeDTO;

    /**
     * 属性关联的名称 用于导出excel的标题
     */
    @JsonIgnore
    private Map<String , String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
        keyMap.put("name" , "物业名称");
        keyMap.put("certificateNo" , "产权证书号");
        keyMap.put("obligee" , "权利人");
        keyMap.put("rightTypeName" , "权力类型");
        keyMap.put("buildingArea","建筑面积");
        keyMap.put("termStr" , "使用期限");
    }

    /**
     * 导出excel表需要显示的字段名称 如，id,proName,stageName
     */
    private String needShowKeys;
}
