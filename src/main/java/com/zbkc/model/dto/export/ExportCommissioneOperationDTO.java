package com.zbkc.model.dto.export;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zbkc.model.dto.YyCommissioneOperationHomeDTO;
import lombok.Data;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author caobiyang
 * @date 2021/10/15
 */
@Data
public class ExportCommissioneOperationDTO {
    /**
     * 列表数据
     */
    private YyCommissioneOperationHomeDTO yyCommissioneOperationHomeDTO;

    /**
     * 属性关联的名称 用于导出excel的标题
     */
    @JsonIgnore
    private Map<String , String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
        keyMap.put("name" , "物业名称");
        keyMap.put("buildArea" , "委托运营面积");
        keyMap.put("operationUnit" , "运营管理单位");
        keyMap.put("startTimeAndEndTime" , "运营起止日期");
    }

    /**
     * 导出excel表需要显示的字段名称 如，id,proName,stageName
     */
    private String needShowKeys;
}
