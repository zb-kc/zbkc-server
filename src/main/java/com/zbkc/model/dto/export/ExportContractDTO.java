package com.zbkc.model.dto.export;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zbkc.model.dto.YyContractIndexDTO;
import lombok.Data;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author yangyan
 * @date 2021/10/27
 */
@Data
public class ExportContractDTO {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 模糊参数
     */
    private YyContractIndexDTO yyContractIndexDTO;
    /**
     * 属性关联的名称 用于导出excel的标题
     */
    @JsonIgnore
    private Map<String, String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
        keyMap.put("id", "序号");
        keyMap.put("leaser", "承租方");
        keyMap.put("contactPerson", "联系人");
        keyMap.put("contactPhone", "联系电话");
        keyMap.put("wyAddress", "租赁物业地址");
        keyMap.put("leaseUsing", "租赁用途");
        keyMap.put("rentPrice", "租金单价");
        keyMap.put("monthlyRent", "月租金");
        keyMap.put("leaseTerm", "租赁期限");
        keyMap.put("startRentDate", "起租时间");
        keyMap.put("endRentDate", "止租时间");
        keyMap.put("isFree", "是否免租");

    }

    /**
     * 导出excel表需要显示的字段名称 如，id,proName,stageName
     */
    private String needShowKeys;
}
