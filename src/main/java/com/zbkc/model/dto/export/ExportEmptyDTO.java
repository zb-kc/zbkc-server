package com.zbkc.model.dto.export;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zbkc.model.dto.EmptyDetailDTO;
import lombok.Data;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author yangyan
 * @date 2021/10/15
 */
@Data
public class ExportEmptyDTO {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 模糊参数
     */
    private EmptyDetailDTO emptyDetailDTO;
    /**
     * 属性关联的名称 用于导出excel的标题
     */
    @JsonIgnore
    private Map<String, String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
//        keyMap.put("id", "序号");
        keyMap.put("wyName", "物业名称");
        keyMap.put("assetType", "物业类型");
        keyMap.put("address", "物业地址");
        keyMap.put("rentalArea", "租赁面积");
        keyMap.put("remark", "备注");
    }

    /**
     * 导出excel表需要显示的字段名称 如，id,proName,stageName
     */
    private String needShowKeys;
}
