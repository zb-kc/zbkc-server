package com.zbkc.model.dto.export;

import com.zbkc.model.dto.YjContractDTO;
import lombok.Data;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author yangyan
 * @date 2021/10/14
 */
@Data
public class ExportPreContactDTO {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 模糊参数
     */
    private YjContractDTO yjContractDTO;
    /**
     * 属性关联的名称 用于导出excel的标题
     */
    private Map<String, String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
        keyMap.put("id", "序号");
        keyMap.put("wyName", "租赁物业");
        keyMap.put("assetType", "物业类型");
        keyMap.put("leaseUseStr", "租赁用途");
        keyMap.put("rentalArea", "租赁面积");
        keyMap.put("monthlyRent", "月租金");
        keyMap.put("rentUnitPrice", "租金单价");
        keyMap.put("limitDay", "剩余期限");
        keyMap.put("leaser", "承租方");
        keyMap.put("startRentDate", "起租时间");
        keyMap.put("endRentDate", "止租时间");
        keyMap.put("year", "合同期限");
        keyMap.put("legalPerson", "法人代表");
        keyMap.put("legalPhone", "法人联系方式");
        keyMap.put("clientPerson", "委托人");
        keyMap.put("clientPhone", "委托人联系方式");
        keyMap.put("remark", "备注");
        keyMap.put("type" , "类型");
        keyMap.put("name" , "企业名称");
        keyMap.put("depositAmount" , "押金金额");

    }

    /**
     * 导出excel表需要显示的字段名称 如，id,proName,stageName
     */
    private String needShowKeys;
}