package com.zbkc.model.dto.export;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zbkc.model.dto.YjContractDTO;
import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author caobiyang
 * @date 2021/10/25
 */
@Data
public class ExportYjContractDTO {
    /**
     * 查询列表条件
     */
    private YjContractDTO yjContractDTO;

    /**
     * 属性关联的名称 用于导出excel的标题
     */
    @JsonIgnore
    private Map<String , String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
        keyMap.put("wyName" , "租赁物业");
        keyMap.put("assetType", "物业类型");
        keyMap.put("leaseUseStr" , "租赁用途");
        keyMap.put("rentalArea" , "租赁面积");
        keyMap.put("monthlyRent" , "月租金");
        keyMap.put("rentUnitPrice" , "租金单价");
        keyMap.put("limitDay" , "剩余期限");
        keyMap.put("leaser" , "承租方");
        keyMap.put("startRentDate" , "起租日期");
        keyMap.put("endRentDate" , "止租日期");
        keyMap.put("year" , "合同期限");
        keyMap.put("legalPerson" , "法人");
        keyMap.put("legalPhone" , "法人联系方式");
        keyMap.put("remark" , "备注");

    }

    /**
     * 导出excel表需要显示的字段名称 如，id,proName,stageName
     */
    private String needShowKeys;
}
