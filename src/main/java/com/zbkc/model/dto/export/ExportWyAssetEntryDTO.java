package com.zbkc.model.dto.export;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zbkc.model.dto.WyAssetEntryHomeDTO;
import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author caobiyang
 * @date 2021/12/20
 * <p>
 *    导出资产入账请求类
 * </p>
 */
@Data
public class ExportWyAssetEntryDTO {
    private WyAssetEntryHomeDTO wyAssetEntryHomeDTO;

    /**
     * 属性关联的名称 用于导出excel的标题
     */
    @JsonIgnore
    private Map<String , String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
        keyMap.put("name" , "物业名称");
        keyMap.put("entryType" , "入账类型");
        keyMap.put("entryMoney" , "入账金额（元）");
        keyMap.put("entryTime" , "入账时间");

    }

    /**
     * 导出excel表需要显示的字段名称 如，id,proName,stageName
     */
    private String needShowKeys;
}
