package com.zbkc.model.dto.export;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zbkc.model.dto.WyAssetDisposalHomeDTO;
import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author caobiyang
 * @date 2021/12/20
 * <p>
 *    导出资产处置请求类
 * </p>
 */
@Data
public class ExportWyAssetDisposalDTO {
    private WyAssetDisposalHomeDTO wyAssetDisposalHomeDTO;

    /**
     * 属性关联的名称 用于导出excel的标题
     */
    @JsonIgnore
    private Map<String , String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
        keyMap.put("name" , "物业名称");
        keyMap.put("documentCode" , "批准文件号");
        keyMap.put("authority" , "批准单位");
        keyMap.put("disposalUnit" , "处置单位");
        keyMap.put("disposalTime" , "处置时间");
        keyMap.put("solvingMethod" , "处置方式");

    }

    /**
     * 导出excel表需要显示的字段名称 如，id,proName,stageName
     */
    private String needShowKeys;
}
