package com.zbkc.model.dto.export;

import com.zbkc.model.dto.LeaseDetailDTO;
import lombok.Data;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author yangyan
 * @date 2021/10/15
 */
@Data
public class ExportLeaseDTO {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 模糊参数
     */
    private LeaseDetailDTO leaseDetailDTO;
    /**
     * 属性关联的名称 用于导出excel的标题
     */
    private Map<String, String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
//        keyMap.put("id", "序号");
        keyMap.put("wyName", "租赁物业");
        keyMap.put("assetType", "物业类型");
        keyMap.put("address", "物业地址");
        keyMap.put("rentalArea", "租赁面积");
        keyMap.put("zlMonthlyRent", "租金标准（元/月）");
        keyMap.put("leaser", "承租方");
        keyMap.put("startRentDate", "起租时间");
        keyMap.put("endRentDate", "止租时间");
        keyMap.put("year", "租赁年限");
        keyMap.put("remark", "备注");
    }

    /**
     * 导出excel表需要显示的字段名称 如，id,proName,stageName
     */
    private String needShowKeys;
}
