package com.zbkc.model.dto.export;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zbkc.model.dto.DepositDTO;
import lombok.Data;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author caobiyang
 * @date 2021/10/18
 */
@Data
public class ExportDepositDTO {
    /**
     * 押金信息
     */
    private DepositDTO info;

    /**
     * 属性关联的名称 用于导出excel的标题
     */
    @JsonIgnore
    private Map<String , String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
        keyMap.put("leaser" , "承租方");
        keyMap.put("wyAddress" , "租赁物业地址");
        keyMap.put("depositAmount" , "实收押金");
        keyMap.put("paymentStatusStr" , "缴费状态");
    }

    /**
     * 导出excel表需要显示的字段名称 如，id,proName,stageName
     */
    private String needShowKeys;
}
