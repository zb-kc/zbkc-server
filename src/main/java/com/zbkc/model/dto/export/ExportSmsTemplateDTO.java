package com.zbkc.model.dto.export;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zbkc.model.dto.WySysMsgHomeDTO;
import lombok.Data;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * *@author caobiyang
 * @date 2021/10/22
 */
@Data
public class ExportSmsTemplateDTO {
    /**
     * 查询条件
     */
    private WySysMsgHomeDTO wySysMsgHomeDTO;

    /**
     * 属性关联的名称 用于导出excel的标题
     */
    @JsonIgnore
    private Map<String , String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
        keyMap.put("name" , "短信模板名称");
        keyMap.put("createTime" , "创建时间");
        keyMap.put("statusStr" , "状态");
    }

    /**
     * 导出excel表需要显示的字段名称 如，id,proName,stageName
     */
    private String needShowKeys;
}
