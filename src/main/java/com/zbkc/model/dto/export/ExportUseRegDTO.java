package com.zbkc.model.dto.export;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zbkc.model.dto.YyUseRegHomeDTO;
import lombok.Data;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Data
public class ExportUseRegDTO {


    private YyUseRegHomeDTO yyUseRegHomeDTO;

    /**
     * 属性关联的名称 用于导出excel的标题
     */
    @JsonIgnore
    private Map<String , String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
        keyMap.put("name" , "物业名称");
        keyMap.put("buildingArea" , "建筑面积");
        keyMap.put("objName" , "使用对象名称");
        keyMap.put("methodStr" , "使用方式");
        keyMap.put("startTimeAndEndTime","使用起止日期");
        keyMap.put("statusStr" , "使用状态");
    }

    /**
     * 导出excel表需要显示的字段名称 如，id,proName,stageName
     */
    private String needShowKeys;
}
