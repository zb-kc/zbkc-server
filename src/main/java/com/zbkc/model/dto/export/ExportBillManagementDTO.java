package com.zbkc.model.dto.export;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zbkc.model.dto.BillManagementDTO;
import lombok.Data;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author caobiyang
 * @date 2021/09/18
 * <p>
 *    导出账单请求类
 * </p>
 */
@Data
public class ExportBillManagementDTO {

    /**
     * 导出账单信息
     */
    private BillManagementDTO info;

    /**
     * 属性关联的名称 用于导出excel的标题
     */
    @JsonIgnore
    private Map<String , String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
        keyMap.put("leaser" , "承租方");
        keyMap.put("billTime" , "账单日期");
        keyMap.put("address" , "租赁物业地址");
        keyMap.put("monthlyRent" , "月租金");
        keyMap.put("amountReceivable" , "应收金额");
        keyMap.put("actualCollection" , "实收金额");
        keyMap.put("paymentStatusStr" , "缴费状态");
        keyMap.put("invoiceStatusStr" , "发票状态");
    }

    /**
     * 导出excel表需要显示的字段名称 如，id,proName,stageName
     */
    private String needShowKeys;
}
