package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.YyUseRegFilePath;
import lombok.Data;


import java.util.List;

/**
 * @author yangyan
 * @date 2021/10/09
 */
@Data
public class YyCommissioneOperationDTO1 {
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 房地产名称   物业资产id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 使用起始 开始时间
     */
    private String startTime;

    /**
     * 使用起始 结束时间
     */
    private String endTime;

    /**
     * 运营单位名称
     */
    private String operationUnit;

    /**
     * 统一社会信用代码
     */
    private String creditCode;

    /**
     * 法定代表人姓名
     */
    private String legalName;

    /**
     * 法定代表人联系方式
     */
    private String legalPhone;

    /**
     * 法定代表人证件类型
     */
    private String legalType;

    /**
     * 法定代表人证件号码
     */
    private String legalCode;

    /**
     * 代理人联系方式
     */
    private String agentPhone;

    /**
     * 代理人姓名
     */
    private String agentName;

    /**
     * 代理人类型
     */
    private String agentType;

    /**
     * 代理人证件号码
     */
    private String agentCode;

    /**
     * 运营费用
     */
    private Double operationMoney;

    /**
     * 备注
     */
    private String remark;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 自定义附件
     */
    private List<YyUseRegFilePath> custProRightFile;
}
