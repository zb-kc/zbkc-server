package com.zbkc.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * 业务办理-待办业务-所有业务的列表类
 * @author ZB3436.xiongshibao
 * @since 2021-9-16
 */
@Data
public class YwbusinessDTO {

    /**
     * 页码
     * */
    private Integer p;
    /**
     * 大小
     * */
    private Integer size;

    /**
     * 排序id
     */
    private Long id;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 受理号
     */
    private String acceptednumber;
    /**
     * 阶段名称
     */
    private String stageName;
    /**
     * 申请单位
     */
    private String applicantCompany;
    /**
     * 流程进度
     */
    private String processSchedule;
    /**
     * 申请时间
     */
    private String applicantTime;

    /**
     * 用户id
     */
    private long userId;

    /**
     * 查询的类型（loading待办 all所有）
     */
    private String businessType;

}
