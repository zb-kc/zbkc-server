package com.zbkc.model.dto;

import lombok.Data;

@Data
public class wyAnalysisDTO {
    /**
     * 页码
     */
    private int p;
    /**
     * 行数
     */
    private int size;
    /**
     * 退租园区
     */
    private String wyname;

    /**
     * 退租年份
     * */
    private String year;
    /**
     * 退租类型
     * */
    private String tztype;
    /**
     * 退租原因
     * */
    private String tzresion;


    /**
     * 企业名称
     * */
    private String name;
}
