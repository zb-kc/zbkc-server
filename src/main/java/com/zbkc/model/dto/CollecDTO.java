package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author yangyan
 * @date 2021/09/18
 *
 */
@Data
public class CollecDTO {

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 账单状态  1:未缴费;2已缴费 ,3欠缴费  默认1
     */
    private Integer billStatus;

    /**
     * 物业id
     */
    private Long wyId;

}
