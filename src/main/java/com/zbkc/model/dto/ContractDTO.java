package com.zbkc.model.dto;


import lombok.Data;

/**
 * @author yangyan
 * @date 2021/09/17
 * <p>
 *     账单首页请求参数
 * </p>
 */
@Data
public class ContractDTO {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     * 页码
     */
    private Integer p;
    /**
     * 行数
     */
    private Integer size;
    /**
     * 模板类型
     */
    private Integer templateType;

//    /**
//     * 月初时间
//     */
//    private String monthStart;
//    /**
//     * 月末时间
//     */
//    private String monthEnd;

    /**
     * 用户id
     */
    private Long userId;



}
