package com.zbkc.model.dto;

import lombok.Data;

@Data
public class HouseCodeDTO {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     * 页码
     */
    private Integer p;
    /**
     * 行数
     */
    private Integer size;
    /**
     * 地址
     */
    private String address;

    /**
     * 物业名称
     * */
    private String name;
    /**
     * 类型 不符合编码条件/已补充编码/未补充编码
     * */
    private String type;

}
