package com.zbkc.model.dto;

import lombok.Data;

import java.io.Serializable;


/**
 * @author gmding
 * @Description 授权返回实体类
 * @date 2021-7-16
 * */
@Data
public class LoginDTO implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String pwd;
}
