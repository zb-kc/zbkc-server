package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author yangyan
 * @date 2021/09/11
 */
@Data
public class WySysMsgHomeDTO {
    /**
     * 页码
     * */
    private Integer p;
    /**
     * 大小
     * */
    private Integer size;
    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 短信名称
     */
    private String name;

    /**
     * 短信状态 1:启用 2：禁用
     */
    private Integer status;

    /**
     * 列表根据字段排序：1-创建时间升序  2-创建时间降序
     */
    private Integer sort;


}
