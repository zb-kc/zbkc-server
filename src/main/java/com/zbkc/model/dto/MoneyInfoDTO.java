package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author caobiyang
 * @Description 滞纳金和实收修改请求类
 * @date 2021-11-09
 * */
@Data
public class MoneyInfoDTO {

    /**
     * 账单id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 待缴滞纳金
     */
    private BigDecimal waitPayment;

    /**
     * 实收修改（已缴纳租金）
     */
    private  BigDecimal paidRent;
}
