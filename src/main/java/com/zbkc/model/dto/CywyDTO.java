package com.zbkc.model.dto;

import lombok.Data;

@Data
public class CywyDTO {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     * 页码
     */
    private Integer p;
    /**
     * 行数
     */
    private Integer size;
    /**
     * 关键字
     */
    private String index;

    /**
     * 类型
     * */
    private String type;

    /**
     * 政府物业
     */
    private String belongsType;

    private Integer year;

    //物业名称
    private String name;

    //物业id
    private String id ;

    //行业
    private String industry;

    //资产五大类
    private String wyFive;

    //街道
    private String streetName;

    //排序 0 asc 1 desc
    private Integer sort;

    //物业种类八小类
    private String wy_kind;

    //公司名称
    private String companyName;

    private Integer startNumber;
    private Integer endNumber;

    //管理单位
    private String manUnit;

    //建筑面积开始 -结束
    private Integer buildAreaStart;
    private Integer buildAreaEnd;

    //1 政府 2 国企
    private Integer propertytag;

    //国企物业的使用状态 自用 空置 出租 调配
    private String usestatus;

    private String searchName;

    //顶级物业关联单位
    private String topWyManUnit;
}
