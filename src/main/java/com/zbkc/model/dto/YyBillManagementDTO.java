package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * @author yangyan
 * @date 2021/09/14
 */
@Data
public class YyBillManagementDTO {


    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 备注
     */
    @Length(max = 1000 , message = "备注限制长度1000个字符")
    private String remark;

    /**
     * 待缴滞纳金
     */
    @Length(max = 20 , message = "待缴滞纳金限制长度20个字符")
    private String waitPayment;

}
