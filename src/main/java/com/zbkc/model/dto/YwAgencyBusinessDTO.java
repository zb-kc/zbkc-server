package com.zbkc.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/9/27
 */
@Data
public class YwAgencyBusinessDTO {

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 项目名称
     */
    @Length(max = 50 , message = "项目名称限制长度50个字符")
    private String proName;
    /**
     * 受理号
     */
    @Length(max = 50 , message = "受理号限制长度50个字符")
    private String acceptCode;
    /**
     * 阶段名称
     */
    @Length(max = 50 , message = "阶段名称限制长度50个字符")
    private String stageName;

    /**
     * 申请单位
     */
    @Length(max = 255 , message = "申请单位限制长度255个字符")
    private String applicantUnit;

    /**
     * 流程进度
     */
    @Length(max = 20 , message = "流程进度限制长度20个字符")
    private String process;

    /**
     * 申请时间
     */
    private Timestamp applicationTime;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 当前页码
     */
    private int p;
    /**
     * 每页行数
     */
    private int size;
    /**
     * 查询-开始时间
     */
    private String queryStartTime;
    /**
     * 查询-结束时间
     */
    private String queryEndTime;

    /**
     * 代办类型 1移交用房申请；2物业资产申报; 3合同签署申请； 4 合同签署申请委非托运营类
     */
    private Integer agencyType;

    /**
     * 业务类型 待办业务-所有业务
     *
       申请业务类型 1产业用房租赁 2商业用房租赁 3住宅用房租赁 4南山软件园租赁
     */
    private String businessType;

    /**
     * 属性关联的名称 用于导出excel的标题
     */
    @JsonIgnore
    private Map<String , String> keyMap;
    {
        keyMap = new LinkedHashMap<>();
        keyMap.put("id" , "序号");
        keyMap.put("proName" , "项目名称");
        keyMap.put("acceptCode" , "受理号");
        keyMap.put("stageName" , "阶段名称");
        keyMap.put("applicantUnit" , "申请单位");
        keyMap.put("process" , "流程进度");
        keyMap.put("applicationTime" , "申请时间");
        keyMap.put("contactName" , "联系人");
        keyMap.put("contactPhone" , "联系电话");
        keyMap.put("businessType" , "办理类型");
        keyMap.put("type" , "业务类型");
        keyMap.put("agencyTypeStr" , "申请类型");
    }

    /**
     * 导出excel表需要显示的字段名称 如，id,proName,stageName
     */
    private String needShowKeys;

    /**
     * 社会统一信用代码
     */
    @Length(max = 100 , message = "社会统一信用代码限制长度100个字符")
    private String socialUniformCreditCode;

    /**
     * 选择的多个合同id
     */
    private String selectContracts;

    /**
     * 主合同id
     */
    private String mainContractId;

    /**
     * 业务名称
     */
    private String businessName;

    /**
     * 网签类型 1新签 2续签
     */
    private String webSignType;

    /**
     * 申请对象
     */
    private String applyObj;

    /**
     * 营业执照地址
     */
    private String businessLicenseAddress;

    /**
     * 物业名称
     */
    private String wyName;

    /**
     * 租赁范围
     */
    @Length(max = 255 , message = "租赁范围限制长度255个字符")
    private String leaseArea;

    /**
     * 是否经济承诺贡献 1是 2否
     */
    private String isCommitment;

    /**
     * 续签类型 1三年内续签 2三年以上续签
     */
    private Byte renewalType;

    /**
     * 备注
     */
    @Length(max = 1000 , message = "备注限制长度1000个字符")
    private String remark;

    /**
     * 流程状态 0刚授权 1企业申请 2企业撤回 3管理员取消授权
     */
    private Byte recordStatus;

    /**
     * 排序
     * 1 申请时间升序
     * 2 申请时间降序
     */
    private Integer sort;

    /**
     * 查询得到的总数
     */
    @JsonIgnore
    private Integer total;

    /**
     * 联系人
     */
    private String contactName;
    /**
     * 联系电话
     */
    private String contactPhone;

    /**
     * 经济承诺贡献
     */
    @Length(max = 1000 , message = "经济承诺贡献限制长度1000个字符")
    private String economicCommit;

    /**
     * 地址
     */
    private String wyAddress;

}
