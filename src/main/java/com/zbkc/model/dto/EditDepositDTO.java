package com.zbkc.model.dto;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author caobiyang
 * @date 2021/9/17
 */
@Data
public class EditDepositDTO {

    @JsonSerialize(using= ToStringSerializer.class)
    private  Long id;

    /**
     * 押金状态 1，未收押金，2，已收押金 , 3.已退还押金  默认1
     */
    private Integer depositStatus;

    /**
     * 备注
     */
    private String remark;
}
