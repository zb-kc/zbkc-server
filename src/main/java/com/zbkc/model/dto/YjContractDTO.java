package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author yangyan
 * @date 2021/10/04
 */
@Data
public class YjContractDTO {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     * 页码
     */
    private Integer p;
    /**
     * 行数
     */
    private Integer size;

    /**
     * 资产类型(物业类型):  产业用房；孵化园区；办公用房；公共服务设施；商业；住宅；租赁社会物业
     */
    private String assetType;

    /**
     * 物业名称
     */
    @Length(max = 20 , message = "物业名称限制长度20个字符")
    private String wyName;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 按照字段排序  1：租赁面积升序 2：租赁面积降序 3：月租金升序 4：月租金降序
     * 5：租金单价升序 6：租金单价降序 7：起租日期升序 8：起租日期降序 9：止租日期升序  10：止租日期降序
     * 11.剩余期限升序 12.剩余期限降序 13.合同年限升序 14.合同年限降序
     */
    private Integer sort;



}
