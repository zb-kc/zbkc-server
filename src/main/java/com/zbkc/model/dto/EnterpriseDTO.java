package com.zbkc.model.dto;

import lombok.Data;


@Data
public class EnterpriseDTO {
    private Integer sort;
    private String name;
    private Integer p;
    private Integer size;
    private String  parkName;
    private String belongsType;
    private String companyName;
    private Integer startNumber ;
    private Integer endNumber ;
    private String companyindustry;
    private String manUnit;
    private String wyFive;

}

