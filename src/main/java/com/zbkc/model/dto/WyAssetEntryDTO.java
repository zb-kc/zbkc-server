package com.zbkc.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysFilePath;
import com.zbkc.model.po.SysImg;
import lombok.Data;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 *
 * @author yangyan
 * @date 2021/09/03
 */
@Data
public class WyAssetEntryDTO {

    /**
     * 主键ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 物业资产id 基础信息表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 入账类型 数据字典值
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long entryType;

    /**
     * 入账金额
     */
    private BigDecimal entryMoney;

    /**
     * 入账时间
     */
    private String entryTime;

    /**
     * 南山区政府产权登记表 文件
     */
    private List<SysFilePath>  propertyRegistrationFile;

    /**
     * 发改立项批复 文件
     */
    private List<SysFilePath> projectApprovalFile;

    /**
     * 财政批复 文件
     */
    private List<SysFilePath> financialApprovalFile;


    /**
     * 审计报告 文件
     */
    private List<SysFilePath> auditReportFile;

    /**
     * 其他材料  文件
     */
    private List<SysFilePath> otherMaterialsFile;

    /**
     * 自定义材料 文件
     */
    private Map<String,List<SysFilePath>> custRightFile;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

}
