package com.zbkc.model.dto;



import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysFilePath;
import com.zbkc.model.po.SysFilePath1;
import com.zbkc.model.po.YyContract;
import com.zbkc.model.po.YySplitRent;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author yangyan
 * @date 2021/10/27
 */
@Data
public class YyContractDTO {

//  /**
//   * 主键id
//   */
//  @JsonSerialize(using= ToStringSerializer.class)
//  private Long id;


    /**
     * 传输对象
     */
   public YyContract info;

    /**
     * 租赁期限 起租时间
     */
    private String startRentDate;

    /**
     * 租赁期限 止租时间
     */
    private String endRentDate;

    /**
     * 首期租金交付日期
     */
    private String firstRentTime;

   /**
    * 拆分列表
    * */
   private List<YySplitRent>rentList;


   /**
    * 入驻通知书
    */
   private List<SysFilePath> residentNoticeFile;

   /**
    * 营业执照复印件
    */
   private List<SysFilePath> businessLicenseFile;

   /**
    * 法人身份证复印件
    */
   private List<SysFilePath> legalPersonFile;

   /**
    * 股东证明书
    */
   private List<SysFilePath> shareholderFile;

    /**
     * 法人代表证明书
     */
    private List<SysFilePath> legalRepresentativeFile;

    /**
     * 法人授权委托书
     */
    private List<SysFilePath> legalPersonPowerFile;

    /**
     * 经办人身份证复印件
     */
    private List<SysFilePath> handlingPersonCardFile;

   /**
    * 南山区产业用房建设和管理工作领导小组会议纪要
    */
   private List<SysFilePath> meetingMinutesFile;

   /**
    *房屋建筑面积总表，房屋建筑面积分户汇总表及房屋建筑面积分户位置图 sys_file_path字段db_file_id
    */
   private List<SysFilePath> buildingAreaFile;

   /**
    * 有经济贡献率的企业的经济贡献承诺书
    */
   private List<SysFilePath> commitmentFile;

   /**
    * 南山区政策性产业用房入驻通知书
    */
   private List<SysFilePath> settlementNoticeFile;

    /**
     * 会议纪要
     */
    private List<SysFilePath> meetingFile;

    /**
     * 租金价格评估报告
     */
    private List<SysFilePath> rentReportFile;

    /**
     * 身份证复印件
     */
    private List<SysFilePath> personFile;

    /**
     * 南山数字文化产业基地入驻通知书
     */
    private List<SysFilePath> digitalNoticeFile;

    /**
     * 合同
     */
    private List<SysFilePath> contractFile;

    /**
     * 自定义附件
     */
    private Map<String,List<SysFilePath>> custRightFile;

    /**
     * 续租通知书
     */
    private List<SysFilePath> renewalSignFile;



    private  List<String> wyIds;

}
