package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @author yangyan
 * @date 2021/10/13
 */
@Data
public class SmsDTO {
    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 物业基础信息表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyId;

    /**
     * 主表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long contractId;


    /**
     * 模板类型
     */
    private Integer msgType;

    /**
     * 发送时间
     */
    private Timestamp sendTime;

    /**
     * 发送状态  1:失败 ；2成功
     */
    private Boolean sendStatus;

}

