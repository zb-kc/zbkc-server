package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.*;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author yangyan
 * @date 2021/09/16
 */
@Data
public class YyContractDetail {

    /**
     * 主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 传输对象
     */
    private YyContract info;


    /**
     *租赁物业
     */
    private WyBasicInfo wyBasicInfo;

    /**
     * 出租面积
     *
     */
    private Double rentAreaTotal;

    /**
     * 套内建筑面积
     */
    private double inSetAreaTotal;

    /**
     * 公摊面积
     */
    private double shareAreaTotal;



    /**
     * 拆分列表
     * */
    private List<YySplitRent>rentList;


    /**
     * 入驻通知书
     */
    private List<SysFilePath1> residentNoticeFile;

    /**
     * 营业执照复印件
     */
    private List<SysFilePath1> businessLicenseFile;

    /**
     * 法人身份证复印件
     */
    private List<SysFilePath1> legalPersonFile;

    /**
     * 股东证明书
     */
    private List<SysFilePath1> shareholderFile;

    /**
     * 法人代表证明书
     */
    private List<SysFilePath1> legalRepresentativeFile;

    /**
     * 法人授权委托书
     */
    private List<SysFilePath1> legalPersonPowerFile;

    /**
     * 经办人身份证复印件
     */
    private List<SysFilePath1> handlingPersonCardFile;

    /**
     * 南山区产业用房建设和管理工作领导小组会议纪要
     */
    private List<SysFilePath1> meetingMinutesFile;

    /**
     *房屋建筑面积总表，房屋建筑面积分户汇总表及房屋建筑面积分户位置图
     */
    private List<SysFilePath1> buildingAreaFile;

    /**
     * 有经济贡献率的企业的经济贡献承诺书
     */
    private List<SysFilePath1> commitmentFile;

    /**
     * 南山区政策性产业用房入驻通知书
     */
    private List<SysFilePath1> settlementNoticeFile;

    /**
     * 会议纪要
     */
    private List<SysFilePath1> meetingFile;

    /**
     * 租金价格评估报告
     */
    private List<SysFilePath1> rentReportFile;

    /**
     * 身份证复印件
     */
    private List<SysFilePath1> personFile;

    /**
     * 南山数字文化产业基地入驻通知书
     */
    private List<SysFilePath1> digitalNoticeFile;

    /**
     * 合同
     */
    private List<SysFilePath1> contractFile;

    /**
     * 自定义文件
     */
    private Map<String,List<SysFilePath1>> customFileMap;

    /**
     * 续签通知书 sys_file_path字段db_file_id
     */
    private  List<SysFilePath1>  renewalSignFile;

    /**
     * 待办申请信息
     */
    private YwAgencyBusinessPO ywAgencyBusinessPO;

    /**
     * 甲方持有房屋证明文件id
     */
    private Long houseProof;

    /**
     * 房屋权属情况id
     */
    private Long houseOwnership;

//    /**
//     * 物业ids
//     */
//    private List<Long> wyIds;
}
