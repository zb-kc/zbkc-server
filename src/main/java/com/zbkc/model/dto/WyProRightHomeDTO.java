package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;


/**
 *
 * @author yangyan
 * @date 2021/09/01
 */
@Data
public class WyProRightHomeDTO {

    /**
     * 页码
     * */
    private Integer p;
    /**
     * 大小
     * */
    private Integer size;


    /**
     * 房地产名称
     */
    private String wyBasicInfoName;

    /**
     * 产权证书号
     */
    private String certificateNo;



    /**
     * 权利类型   数据字典值
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long rightType;

    /**
     * 土地用途 数据字典值
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long landPurpose;


    /**
     * 使用期限 开始时间
     */
    private String useLimitStart;

    /**
     * 使用期限 结束时间
     */
    private String useLimitEnd;

    /**
     * 建筑面积 最小值
     */
        private Double buildingAreaMin;

    /**
     * 建筑面积 最大值
     */
    private Double buildingAreaMax;

    /**
     * 街道
     */
    private String street;

    /**
     * 社区
     */
    private String community;
    /**
     * 详细地址
     */
    private String detailsAddress;


    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 按照字段排序：1-建筑面积升序 2-建筑面积降序 3-使用开始日期升序 4-使用开始日期降序（默认） 5-使用结束日期升序 6-使用结束时间降序
     */
    private Integer sort ;

}
