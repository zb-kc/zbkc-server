package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysFilePath;
import com.zbkc.model.po.SysFilePath1;
import com.zbkc.model.po.SysImg;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 *  租赁社会物业请求包
 * @author caobiyang
 * @since  2021-10-08
 */
@Data
public class RentalSocialWyInfoDTO {
    /**
     * 物业id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 资产类型:  产业用房；孵化园区；办公用房；公共服务设施；商业；住宅；租赁社会物业
     */
    private String assetType;

    /**
     * 物业种类:  产业用房；商业；公共配套；办公用房；公共配套；基本办公用房；服务用房；设备用房；附属用房；教育；社区服务；市政共用；文体；医疗卫生
     *
     */
    private String propertyType;

    /**
     *租赁物业名称
     */
    private String name;

    /**
     * 别名/曾用名
     *
     * */
    private String aliasName;

    /**
     * 物业编号
     * */
    private String propertyCode;

    /**
     * 楼栋编码
     */
    private String houseCode;

    /**
     * 地址
     */
    private String address;

    /**
     * 社区编码
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long areaCode;

    /**
     * 详细地址
     */
    private String detailAddr;

    /**
     * 租赁用途
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long leaseUseId ;

    /**
     * 租赁面积
     */
    private Double zlRentalArea;

    /**
     * 租赁起始日期
     */
    private String  zlStartRentDate;

    /**
     * 租赁截止日期
     */
    private String zlEndRentDate;

    /**
     * 月租金
     */
    private BigDecimal zlMonthlyRent;

    /**
     * 租金单价
     */
    private BigDecimal  zlRentUnitPrice;

    /**
     * 出租方
     */
    private String lessor;

    /**
     * 出租联系方式
     */
    private String lessorPhone;


    /**
     * 物业公司名称/租赁物业单位
     */
    private String companyName;

    /**
     * 层高
     * */
    private Integer storeyHeight;

    /**
     * 承重
     * */
    private Double loadBearer;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 维度
     */
    private BigDecimal latitude;


    /**
     * 朝向 1.东2.南3.西4.北
     */
    private Integer towards;

    /**
     * 户型图  sys_img字段primary_table_id
     */
    private List<SysImg> houseImgs;

    /**
     * 内三维
     */
    private String threeDimen;

    /**
     * 物业图片
     * */
    private List<SysImg> propertyImgPath;

    /**
     * 用户id
     */
    private Long userId;
}
