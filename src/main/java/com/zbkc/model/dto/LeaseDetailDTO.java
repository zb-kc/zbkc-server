package com.zbkc.model.dto;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

/**
 * @author yangyan
 * @date 2021/09/18
 */
@Data
public class LeaseDetailDTO {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;
    /**
     * 页码
     */
    private Integer p;
    /**
     * 行数
     */
    private Integer size;
    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 物业名称
     */
    @Length(max = 50 , message = "物业名称限制长度50个字符")
    private String wyName;
//    /**
//     * 物业种类
//     */
//    @JsonSerialize(using= ToStringSerializer.class)
//    private Long wyTypeId;
    /**
     * 资产类型(物业类型):  产业用房；孵化园区；办公用房；公共服务设施；商业；住宅；租赁社会物业
     */
    private String assetType;

    /**
     * 最低值面积
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal smallArea;

    /**
     * 最高值面积
     */
    @DecimalMin(value ="0", message = "")
    private BigDecimal bigArea;

//    /**
//     * 起租时间
//     */
//    private String startRentTime;
//
//    /**
//     * 止租时间
//     */
//    private String endRentTime;

    /**
     * 租赁期限
     */
    @Min(value = 0 , message = "")
    private Integer year;

    /**
     * 物业地址
     */
    @Length(max = 100 , message = "物业地址限制长度100个字符")
    private String address;

    /**
     * 按照字段排序  1：租赁面积升序 2：租赁面积降序
     * 3：租金单价升序 4：租金单价降序 5：起租日期升序 6：起租日期降序 7：止租日期升序  8：止租日期降序
     * 9.租赁期限升序 10.租赁期限降序 11.月租金（租金标准）升序 12.月租金（租金标准）降序
     */
    private Integer sort;

    /**
     * 起租开始时间
     */
    private String startStartTime;

    /**
     * 起租结束时间
     */
    private String startEndTime;

    /**
     * 止租开始时间
     */
    private String stopStartTime;

    /**
     * 止租结束时间
     */
    private String stopEndTime;


}
