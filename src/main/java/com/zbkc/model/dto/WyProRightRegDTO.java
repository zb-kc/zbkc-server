package com.zbkc.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysFilePath;
import com.zbkc.model.po.SysFilePath1;
import lombok.Data;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author yangyan
 * @date 2021/08/30
 */
@Data
public class WyProRightRegDTO {

    /**
     * 主键ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 物业单元编号
     */
    private String unitCode;

    /**
     * 资产名称id  基础信息表
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 产权证书号
     */
    private String certificateNo;

    /**
     * 不动产单元号
     */
    private String estateUnitNo;

    /**
     * 竣工日期
     */
    private String completionDate;

    /**
     * 权利人
     */
    private String obligee;

    /**
     * 权利类型  数据字典值
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long rightType;

    /**
     * 登记价
     */
    private BigDecimal registerPrice;

    /**
     * 产权证登记日期
     */
    private String regTime;

    /**
     * 土地位置
     */
    private String landLocation;

    /**
     * 社区编码
     *
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long areaCode;

    /**
     * 详细地址
     */
    private String detailAddr;

    /**
     * 建筑面积
     */
    private Double buildingArea;

    /**
     * 套内面积
     */
    private Double insideArea;

    /**
     * 土地用途 数据字典值
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long[] landPurpose;

    /**
     * 登记用途
     */
    private String registerPurpose;

    /**
     * 共有情况 数据字典值
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long commonSituation;

    /**
     * 使用期限 开始时间
     */
    private String useLimitStart;

    /**
     * 使用期限 结束时间
     */
    private String useLimitEnd;

    /**
     * 产权证书 文件
     */
    private List<SysFilePath> proRightFile;

    /**
     * 自定义证书 文件
     */
    private Map<String,List<SysFilePath>> custRightFile;

    /**
     * 备注
     */
    private String remark;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;


}
