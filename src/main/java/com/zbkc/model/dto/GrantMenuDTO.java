package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyan
 * @date 2021/8/3
 */
@Data
public class GrantMenuDTO implements Serializable {

    /**
     * 系统角色ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long roleId;

    /**
     * 菜单集合
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private List<Long> menuIds;
}
