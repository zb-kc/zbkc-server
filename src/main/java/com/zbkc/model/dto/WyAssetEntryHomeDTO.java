package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.sql.Timestamp;


/**
 * @author yangyan
 * @date 2020/09/06
 */
@Data
public class WyAssetEntryHomeDTO {

    /**
     * 页码
     * */
    private Integer p;
    /**
     * 大小
     * */
    private Integer size;

    /**
     * 资产名称
     */
    private String name;

    /**
     * 入账时间 开始时间
     */
    private String entryTimeStart;
    /**
     * 入账时间 结束时间
     */
    private String entryTimeEnd;


    /**
     * 入账类型 数据字典值
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long entryTypeId;
    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 列表按照字段排序：1-入账金额升序 2-入账金额降序 3-入账时间升序 4-入账时间降序
     */
    private Integer sort;
}
