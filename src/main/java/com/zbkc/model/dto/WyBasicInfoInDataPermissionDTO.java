package com.zbkc.model.dto;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * 物业信息 在数据权限页面的DTO
 * @author ZB3436.xiongshibao
 * @date 2021/11/10
 */
@Data
public class WyBasicInfoInDataPermissionDTO {

    /**
     * 物业类型/标签
     */
    private String assetType;
    /**
     * 地址
     */
    private String address;
    /**
     * 物业名称
     */
    private String name;
    /**
     * 物业标签  关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyTagId;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long roleId;

}
