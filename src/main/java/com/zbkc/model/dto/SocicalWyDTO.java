package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 *  租赁社会物业条件查询
 * @author caobiyang
 * @since  2021-10-12
 */
@Data
public class SocicalWyDTO {
    /**
     * 用户id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 物业名称
     */
    private String name;

    /**
     * 地址
     */
    private String address;

    /**
     * 详细地址
     */
    private String detailAddr;

    /**
     * 租赁用途
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long leaseUseId;

    /**
     * 页码
     * */
    private Integer p;
    /**
     * 大小
     * */
    private Integer size;

    /**
     *  列表按字段排序：1.建筑面积升序 2.建筑面积降序 3.月租金升序 4.月租金降序
     */
    private Integer sort;



}
