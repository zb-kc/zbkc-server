package com.zbkc.model.dto;


import lombok.Data;

import java.io.Serializable;

/**
 * @author gmding
 * @Description 组织请求参数
 * @date 2021-7-28
 * */
@Data
public class GroupDTO implements Serializable {


    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;


    /**
     * 组织名称
     */
    private String groupName;

    /**
     * 是否禁用 0 所有 状态(1:有效, 2:无效)
     *
     * */
    private Integer status;

    /**
     * 层级
     *
     * */
    private Integer level;



}
