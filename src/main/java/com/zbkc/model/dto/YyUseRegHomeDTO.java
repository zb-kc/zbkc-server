package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * @author yangyan
 * @date 2020/09/10
 */
@Data
public class YyUseRegHomeDTO {
    /**
     * 页码
     * */
    private Integer p;
    /**
     * 大小
     * */
    private Integer size;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 物业名称
     */
    @Length(max = 100 , message = "物业名称限制长度100个字符")
    private String name;

    /**
     * 使用对象名称
     */
    @Length(max = 50 , message = "使用对象名称限制长度50个字符")
    private String objName;

    /**
     * 使用状态 1 在用; 2停用
     */
    private Integer status;

    /**
     * 使用开始
     */
    private String startTime;

    /**
     * 使用结束
     */
    private String endTime;

    /**
     * 列表按照字段排序：1-建筑面积升序 2-建筑面积降序 3-使用开始日期升序  4-使用开始日期降序  5-使用截止日期升序 6-使用截止日期降序
     *                   7-使用方式排序（出租在前） 8-使用方式排序（调配在前）9-使用状态排序（在用在前面） 10-使用状态排序（停用在前面）
     */
    private Integer sort;
}
