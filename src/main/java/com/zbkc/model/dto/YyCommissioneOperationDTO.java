package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysFilePath;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.util.List;
import java.util.Map;

/**
 * @author yangyan
 * @date 2020/09/10
 */
@Data
public class YyCommissioneOperationDTO {

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 房地产名称   物业资产id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 使用起始 开始时间
     */
    private String startTime;

    /**
     * 使用起始 结束时间
     */
    private String endTime;

    /**
     * 运营单位名称
     */
    @Length(max = 50 , message = "运营单位名称限制长度50个字符")
    private String operationUnit;

    /**
     * 统一社会信用代码
     */
    @Length(max = 50 , message = "统一社会信用代码限制长度50个字符")
    private String creditCode;

    /**
     * 法定代表人姓名
     */
    @Length(max = 50 , message = "法定代表人姓名限制长度50个字符")
    private String legalName;

    /**
     * 法定代表人联系方式
     */
    @Length(max = 20 , message = "法定代表人联系方式限制长度20个字符")
    private String legalPhone;

    /**
     * 法定代表人证件类型
     */
    private String legalType;

    /**
     * 法定代表人证件号码
     */
    @Length(max = 50 , message = "法定代表人证件号码限制长度50个字符")
    private String legalCode;

    /**
     * 代理人联系方式
     */
    @Length(max = 20 , message = "代理人联系方式限制长度20个字符")
    private String agentPhone;

    /**
     * 代理人姓名
     */
    @Length(max = 50 , message = "代理人姓名限制长度50个字符")
    private String agentName;

    /**
     * 代理人类型
     */
    private String agentType;

    /**
     * 代理人证件号码
     */
    @Length(max = 50 , message = "代理人证件号码限制长度50个字符")
    private String agentCode;

    /**
     * 运营费用
     */
    private Double operationMoney;

    /**
     * 备注
     */
    @Length(max = 1000 , message = "备注限制长度1000个字符")
    private String remark;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;


    /**
     * 合同附件
     */
    private List<SysFilePath> contractFileList;

    /**
     * 使用批准附件
     */
    private List<SysFilePath> useApprovalFileList;

    /**
     * 自定义文件
     */
    private Map<String,List<SysFilePath>> customFileMap;
}
