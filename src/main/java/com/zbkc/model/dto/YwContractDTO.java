package com.zbkc.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.model.po.SysDataType;
import com.zbkc.model.po.SysFilePath1;
import com.zbkc.model.po.YySplitRent;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.util.List;

/**
 * 业务办理-合同DTO
 * @author ZB3436.xiongshibao
 * @date 2021/10/21
 */
@Data
public class YwContractDTO {

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 项目名称
     */
    @Length(max = 50 , message = "项目名称限制长度20个字符")
    private String proName;

    /**
     * 模板类型 1产业用房;2商业; 3住宅 ;4 南山软件园
     */
    private String templateType;

    /*====================企业信息开始======================*/

    /**
     * 类型 1产业用房租赁合同3+2（有经济贡献率）;2产业用房租赁合同3+2（无经济贡献率）;3产业用房租赁合同3年及3年以下;
     */
    private String type;

    /**
     * 承租类型 1承租对象为企业；2承租对象为其它
     */
    private String leaseType;

    /**
     * 承租方
     */
    private String leaser;

    /**
     * 承租方证件类型 承租方证件类型 1社会统一信用代码，2身份证，3护照
     */
    private String leaserDocumentType;

    /**
     * 承租方证件号码
     */
    private String leaserDocumentNumber;

    /**
     * 营业执照地址
     */
    private String leaseAddress;

    /**
     * 联系人
     */
    private String contactPerson;

    /**
     * 联系电话
     */
    private String contactPhone;

    /**
     * 法人申请 1是；2否
     */
    private String legalApplication;

    /**
     * 法人代表
     */
    private String legalPerson;
    /**
     *法人联系方式
     */
    private String legalPhone;
    /**
     *法人证件类型 1身份证，2护照
     */
    private String legalType;
    /**
     *法人号码
     */
    private String legalCode;

    /**
     * 委托人申请 1是，2否
     */
    private String clientApplication;
    /**
     * 委托人
     */
    private String clientPerson;
    /**
     * 委托人联系方式
     */
    private String clientPhone;
    /**
     * 委托人证件类型 1身份证，2护照
     */
    private String clientType;

    /**
     * 委托人证件号码
     */
    private String clientCode;

    /**
     * 备注
     */
    private String companyInfoRemark;

    /*====================企业信息结束======================*/
    /*====================租赁信息开始======================*/

    /**
     * 出租物业地址 与 承租方通信地址 相同
     */
    private String leaseWyAddress;

    /**
     * 是否经济贡献承诺
     */
    private String isEconomicCommitment;
    /**
     * 经济贡献承诺
     */
    private String economicCommitment;

    /**
     * 是否拆分租金单价 1是；2否
     */
    private Integer splitRent;

    /**
     * 拆分租金列表
     * */
    private List<YySplitRent> rentList;

    /**
     * 房屋编码（多个用顿号隔开）
     */
    private String houseCodes;

    /**
     * 甲方持有房屋证明文件类型
     */
    private String partAHouseCredentials;
    /**
     * 甲方持有房屋证明文件类型数据字典
     */
    public List<SysDataType> partAHouseCredentialsList;

    /**
     * 房屋所有证权或不动产权证书编号
     */
    private String partAHouseCredentialsNumber;

    /**
     * 出租面积
     */
    private String rentalArea;

    /**
     * 套内面积
     */
    private String setArea;
    /**
     * 公摊面积
     */
    private String pubArea;

    /**
     * 指导价
     */
    private String guidePrice;

    /**
     * 折扣 全价 1/30%/0.3/0.5/0.9
     */
    private String discount;

    /**
     * 租金单价
     */
    private String rentUnitPrice;
    /**
     * 月租金
     */
    private String monthlyRent;
    /**
     * 免租期
     */
    private String freeRent;
    /**
     * 起租时间
     */
    private String startRentDate;
    /**
     * 止租时间
     */
    private String endRentDate;
    /**
     * 首期租金交付日期
     */
    private String firstRentTime;

    /**
     * 首期租金交付金额
     */
    private String firstRentMoney;

    /**
     * 押金金额
     */
    private String depositAmount;

    /**
     * 房屋租赁用途
     */
    private String leaseUse;

    /**
     * 备注
     */
    @Length(max = 1000 , message = "备注限制长度1000个字符")
    private String remark;

    /*====================租赁信息结束======================*/

    /*====================拟定合同内容开始======================*/
    /**
     * 合同信息
     */
    private List<SysFilePath1> contractFiles;
    /*====================拟定合同内容结束======================*/

    /*====================附件材料审核状态结束======================*/
    /**
     * 附件材料审核状态
     */
    private List<YwHandoverDetailsDTO> ywHandoverDetailsDTOList;
    /*====================附件材料审核状态结束======================*/

    /*====================新增属性开始 2021年10月21日 ======================*/
    /**
     * 法定代表人通讯地址
     */
    private String legalPersonContactAddress;
    /**
     * 委托代理人通讯地址
     */
    private String clientPersonContactAddress;
    /**
     * 房屋权属情况
     */
    private String houseOwnership;
    /**
     * 数据字典-房屋权属情况
     */
    private List<SysDataType> houseOwnerShipList;
    /**
     * 水费
     * 电费
     * 燃气费
     * 物业管理费
     * 其他费
     */
    private String waterCharge;
    private String electricCharge;
    private String gasCharge;
    private String propertyManagementCharge;
    private String otherCharge;
    /**
     * 受理号
     */
    private String acceptCode;

    /**
     * 签约类型 1新签 2续签
     */
    private String webSignType;
    /*====================新增属性结束 2021年10月22日 ======================*/

    /**
     * 业务申请id
     */
    private String businessId ;
    /**
     * 审核意见
     */
    private String opinion;

    /**
     * 清除企业信息 在工作流第二个节点中 审核人员可以修改合同信息，但是不能修改企业的信息，因此清除企业相关信息
     */
    public void clearCompanyInfo() {
        this.type 	=	null;
        this.leaseType 	=	null;
        this.leaser 	=	null;
        this.leaserDocumentType 	=	null;
        this.leaserDocumentNumber 	=	null;
        this.leaseAddress 	=	null;
        this.contactPerson 	=	null;
        this.contactPhone 	=	null;
        this.legalApplication 	=	null;
        this.legalPerson 	=	null;
        this.legalPhone 	=	null;
        this.legalType 	=	null;
        this.legalCode 	=	null;
        this.clientApplication 	=	null;
        this.clientPerson 	=	null;
        this.clientPhone 	=	null;
        this.clientType 	=	null;
        this.clientCode 	=	null;
        this.companyInfoRemark 	=	null;
        this.legalPersonContactAddress 	=	null;
        this.clientPersonContactAddress 	=	null;
        this.webSignType 	=	null;

        if(null != ywHandoverDetailsDTOList){
            this.ywHandoverDetailsDTOList.clear();
        }

    }

    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

}
