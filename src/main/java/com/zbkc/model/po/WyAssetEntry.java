package com.zbkc.model.po;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.joda.time.DateTime;

/**
 * <p>
 * 资产入账信息表
 * </p>
 *
 * @author yangyan
 * @since 2021-09-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WyAssetEntry implements Serializable {

    private static final long serialVersionUID=1L;

    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 物业资产id 基础信息表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 入账类型 id 关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long entryTypeId;

    /**
     * 入账金额
     */
    private BigDecimal entryMoney;

    /**
     * 入账时间
     */
    private Date entryTime;


    /**
     * 南山区政府产权登记表id  sys_file_path字段db_file_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyRegistrationFileId;

    /**
     * 发改立项批复id sys_file_path字段db_file_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long projectApprovalFileId;

    /**
     * 财政批复id sys_file_path字段db_file_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long financialApprovalFileId;

    /**
     * 审计报告id sys_file_path字段db_file_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long auditReportFileId;

    /**
     * 其他材料id sys_file_path字段db_file_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long otherMaterialsFileId;


    /**
     * 自定义文件id sys_file_path字段db_file_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long custRightFileId;

    /**
     *
     * 状态(1:有效, 2:无效)
     */
    private Integer status;

    private Timestamp createTime;

    private Timestamp updateTime;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;


}
