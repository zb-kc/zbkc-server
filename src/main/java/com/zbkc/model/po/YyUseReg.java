package com.zbkc.model.po;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 运营管理——使用登记
 * </p>
 *
 * @author yangyan
 * @since 2021-09-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class YyUseReg implements Serializable {

    private static final long serialVersionUID=1L;
    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 房地产名称   物业资产id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;


    /**
     * 使用方式 1出租; 2调配
     */
    private Integer method;

    /**
     * 是否免租 1是；2否
     */
    private Integer rentFree;

    /**
     * 使用状态 1 在用; 2停用
     */
    private Integer status;

    /**
     * 使用对象名称
     */
    private String objName;

    /**
     * 使用对象类型 1.企业/2.个人/3.个体工商户/4.行政事业单位/5.其他
     */
    private Integer objType;

    /**
     * 统一社会信用代码
     */
    private String creditCode;

    /**
     * 联系人姓名
     */
    private String contactName;

    /**
     * 联系方式
     */
    private String contactPhone;

    /**
     * 使用开始
     */
    private Timestamp startTime;

    /**
     * 使用结束
     */
    private Timestamp endTime;

    /**
     * 实际使用用途
     */
    private String actualUse;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 更新时间
     */
    private Timestamp updateTime;

    /**
     * 文件id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long filePathId;

    /**
     * 合同附件  sys_file_path字段db_file_id
     */
    private Long contractFileId;

    /**
     * 使用批准文件  sys_file_path字段db_file_id
     */
    private Long useApprovalFileId;

    /**
     * 自定义附件  sys_file_path字段db_file_id
     */
    private Long customFileId;


}
