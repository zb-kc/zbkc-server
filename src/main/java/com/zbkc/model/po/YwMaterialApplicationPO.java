package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 业务办理——物业资产申报-申请材料
 * @author ZB3436.xiongshibao
 * @date 2021-10-14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("yw_material_application")
public class YwMaterialApplicationPO implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 基础信息表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 基础信息表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyUsedInfoId;

    /**
     * 物业资产名称
     */
    private String wyName;

    /**
     * 建筑面积
     */
    private String buildArea;

    /**
     * 实际使用单位
     */
    private String actualUseUnit;

    /**
     * 实际使用用途
     */
    private String theRealFunction;

    /**
     * 使用单位
     */
    private String useUnit;

    /**
     * 使用起止时间
     */
    private String useTimeStr;

    /**
     * 使用状态
     */
    private String status;

    /**
     * 是否核对
     */
    private String isCheck;

    /**
     *  业务办理——代办业务表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long ywAgencyBusinessId;

    /**
     * 附件材料id  关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long materialApplicationId;

}