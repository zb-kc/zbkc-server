package com.zbkc.model.po;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统菜单表
 * </p>
 *
 * @author yangyan
 * @since 2021-07-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysMenu implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 菜单ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 路径
     */
    private String url;

    /**
     * 父级ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long pid;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态(1:有效, 2:无效)
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 创建人
     */
    private String creater;

    /**
     * 修改时间
     */
    private Timestamp updateTime;

    /**
     * 前端路由名称
     */
    private String routerName;

    /**
     * 前端路由路径
     */
    private String routerPath;

    /**
     * 前端路由视图
     */
    private String routerViews;

    /**
     * 图标
     */
    private String icon;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 类型  默认0   0菜单 1按钮
     */
    private Integer type;

    @TableField(exist = false)
    private List<SysMenu> sysMenusList;
}
