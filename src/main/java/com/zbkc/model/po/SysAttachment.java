package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 系统文件_自定义附件字段
 * </p>
 *
 * @author yangyan
 * @since 2021-11-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysAttachment implements Serializable {

    private static final long serialVersionUID=1L;
    /**
     * 主键id
     */

    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 主表自定义附件id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long sysFilePathId;

    /**
     * 附件字段名称
     */
    private String name;


}
