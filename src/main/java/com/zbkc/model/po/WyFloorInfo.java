package com.zbkc.model.po;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 物业楼层信息表
 * </p>
 *
 * @author yangyan
 * @since 2021-08-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WyFloorInfo implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 楼栋编号
     */
    private String buildingCode;

    /**
     * 名称
     */
    private String name;

    /**
     * 简称
     */
    private String shortName;

    /**
     * 物业编号
     */
    private String propretyCode;

    /**
     * 楼层编码
     */
    private String floorCode;

    /**
     * 物业种类,来源于数据字典
     */
    private Integer propretyType;

    /**
     * 使用状态,来源于数据字典(1：未使用，2：使用中)
     */
    private Integer useStatus;

    /**
     * 建筑面积
     */
    private Double buildingArea;

    /**
     * 实用面积
     */
    private Double practicalArea;

    /**
     * 公摊面积
     */
    private Double shareArea;

    /**
     * 层高
     */
    private Double floorHeight;

    /**
     * 承重
     */
    private Double bearing;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 创建人
     */
    private String creater;

    /**
     * 修改时间
     */
    private Timestamp updateTime;


}
