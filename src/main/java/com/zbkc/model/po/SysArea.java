package com.zbkc.model.po;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 区域表
 * </p>
 *
 * @author gmding
 * @since 2021-08-18
 */
@Data
public class SysArea implements Serializable {
    private static final long serialVersionUID=1L;
    /**
     * 主键id 区划代码
     */

    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 地区名称
     */
    private String name;

    /**
     * 父级区划代码
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long pid;

    /**
     * 省/直辖市代码
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long provinceCode;

    /**
     * 市代码
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long cityCode;
    /**
     * 区/县代码
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long areaCode;
    /**
     * 街道/镇代码
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long streetCode;
    /**
     * 社区/乡村代码
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long committeeCode;
    /**
     * 城乡分类代码
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long committeeType;

    /**
     * 排序
     */
    private int sort;

    /**
     * 级别: 1-省/直辖市, 2-市, 3-区/县/地级市, 4-街道/镇, 5-社区/乡村
     */
    private int level;


}
