package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 *系统设置——角色管理_数据权限关联表
 * @author ZB3436.xiongshibao
 */
@Data
public class SysRoleDataPermission {

    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long roleId;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

}