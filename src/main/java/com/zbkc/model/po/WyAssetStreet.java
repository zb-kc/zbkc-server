package com.zbkc.model.po;

import lombok.Data;


/**
 * @author gmding
 */
@Data
public class WyAssetStreet {
    /**
     * 街道名称
     * */
    private String street;
    /**
     * 面积
     * */
    private String area;
    /**
     * 数量
     * */
    private int num;
    /**
     * 经度纬度
     * */
    private String address;
    /**
     * 面积占比
     * */
    private  Double percentage;

    /**
     * 街道编码
     */
    private String streetCode;


    public String getAddress() {

        switch (street){
            case "南头街道":
                address="113.9255311485475,22.535466038943976";
                break;
            case "南山街道":
                address="113.91869010939666,22.526822811545554";
                break;
            case "招商街道":
                //address="113.9129960936071,22.482348112250502";
                address="113.932037,22.497801";
                break;
            case "桃源街道":
                address="113.972794,22.555917";
                break;
            case "沙河街道":
                address="113.986429621895,22.53576499208788";
                break;
            case "粤海街道":
                address="113.93150649019,22.51801736868394";
                break;
            case "蛇口街道":
                address="113.92912326806174,22.48809059297315";
                break;
            case "西丽街道":
                address="113.9537703891831,22.577146655246608";
                break;
            case "大浪街道":
                address="114.03681699999999,22.62089";
                break;
            case "新安街道":
                address="113.97681699999999,22.62089";
                break;
            default:
                //address="";
                break;
        }
        return address;
    }


}
