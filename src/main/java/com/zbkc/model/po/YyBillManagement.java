package com.zbkc.model.po;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 运营管理——账单管理
 * </p>
 *
 * @author caobiyang
 * @since 2021-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class YyBillManagement implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 合同管理id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long yyContractId;

    /**
     * 账单日期  yyyy.mm
     */
    private String billTime;

    /**
     * 账单状态  1:未缴费;2已缴费 ,3欠缴费  默认1
     */
    private Integer billStatus;

    /**
     * 预收款余额
     */
    private BigDecimal advanceCollection;

    /**
     * 逾期天数
     */
    private Integer overDay;

    /**
     * 已缴滞纳金
     */
    private BigDecimal overduePayment;

    /**
     * 待缴滞纳金
     */
    private BigDecimal waitPayment;

    /**
     * 月租金(元)
     */
    private BigDecimal monthlyRent;


    /**
     * 已缴纳租金
     */
    private BigDecimal paidRent;

    /**
     * 首期租金
     */
    private BigDecimal firstPhaseRent;

    /**
     * 租金差价
     */
    private BigDecimal diffRent;

    /**
     * 应收金额(元)
     */
    private BigDecimal amountReceivable;

    /**
     * 实收金额(元)
     */
    private BigDecimal actualCollection;

    /**
     * 缴费方式 1：微信支付  2：支付宝支付 3：其它
     */
    private Integer paidMethod;

    /**
     * 缴费时间
     */
    private Timestamp paidTime;

    /**
     * 缴费状态  缴费状态 1.未缴费、2.部分缴费、3.已缴费、4.欠缴费、5.已补缴、6.已缴费(预缴费转)
     */
    private Integer paymentStatus;

    /**
     * 收款操作人
     */
    private String collecPerson;

    /**
     * 备注
     */
    private String remark;

    /**
     * 开票状态 1:未开票;2已开票 默认1
     */
    private Integer invoiceStatus;

    /**
     * 开票日期
     */
    private Timestamp invoiceDate;

    /**
     * 发票编码
     */
    private String invoiceCode;

    /**
     * 开票操作人
     */
    private String invoiceOperator;

    /**
     * 创建时间   
     */
    private Timestamp createTime;

    private Timestamp updateTime;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;




}
