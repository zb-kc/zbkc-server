package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.configure.BasePathPatterns;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 系统照片路径表
 * </p>
 *
 * @author yangyan
 * @since 2021-09-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysImg1 implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 照片
    路径
     */
    private String path;

    /**
     *文件名称
     * */
    private String name;
    /**
     * 关联主表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long primaryTableId;

    public String getPath() {
        return BasePathPatterns.urlPath+path;
    }

}
