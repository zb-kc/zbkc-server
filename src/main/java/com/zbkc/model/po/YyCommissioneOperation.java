package com.zbkc.model.po;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 运营管理——委托运营
 * </p>
 *
 * @author yangyan
 * @since 2021-09-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class YyCommissioneOperation implements Serializable {

    private static final long serialVersionUID=1L;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 房地产名称   物业资产id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 使用起始 开始时间
     */
    private Timestamp startTime;

    /**
     * 使用起始 结束时间
     */
    private Timestamp endTime;

    /**
     * 运营单位名称
     */
    private String operationUnit;

    /**
     * 统一社会信用代码
     */
    private String creditCode;

    /**
     * 法定代表人姓名
     */
    private String legalName;

    /**
     * 法定代表人联系方式
     */
    private String legalPhone;

    /**
     * 法定代表人证件类型
     */
    private String legalType;

    /**
     * 法定代表人证件号码
     */
    private String legalCode;

    /**
     * 代理人联系方式
     */
    private String agentPhone;

    /**
     * 代理人姓名
     */
    private String agentName;

    /**
     * 代理人类型
     */
    private String agentType;

    /**
     * 代理人证件号码
     */
    private String agentCode;

    /**
     * 运营费用
     */
    private Double operationMoney;

    /**
     * 备注
     */
    private String remark;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 更新时间
     */
    private Timestamp updateTime;

    /**
     * 自定义文件id 关联yy_use_reg_file表
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long custFileId;

    /**
     * 合同附件  sys_file_path字段db_file_id
     */
    private Long contractFileId;

    /**
     * 使用批准文件  sys_file_path字段db_file_id
     */
    private Long useApprovalFileId;

    /**
     * 自定义附件  sys_file_path字段db_file_id
     */
    private Long customFileId;


}
