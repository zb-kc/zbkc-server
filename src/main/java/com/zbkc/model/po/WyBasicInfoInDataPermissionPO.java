package com.zbkc.model.po;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

/**
 * @author ZB3436.xiongshibao
 * @date 2021/11/10
 */
@Data
public class WyBasicInfoInDataPermissionPO {

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 物业类型
     */
    private String assetType;
    /**
     * 地址
     */
    private String address;
    /**
     * 物业名称
     */
    private String name;
    /**
     * 物业权属/标签  关联表label_manage 字段label_id
     */
    private String propertyTagIdStr;

}
