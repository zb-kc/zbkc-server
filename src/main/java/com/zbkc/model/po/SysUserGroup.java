package com.zbkc.model.po;


import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;


/**
 * <p>
 * 系统用户组织表
 * </p>
 *
 * @author yangyan
 * @since 2021-07-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysUserGroup implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 用户ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;
    /**
     * 名称
     */
    private String name;

    /**
     * 备注 描述
     */
    private String remark;

    /**
     * 状态(1:有效, 2:无效)
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 创建人
     * */
    private String creater;

    /**
     * 修改时间
     * */
    private Timestamp updateTime;

    /**
     * 父级id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long parentid;

    /**
     * 邮箱
     * */
    private String email;

    /**
     * 排序
     * */
    private Integer sort;

    /**
     * 电话
     * */
    private String phone;

    /**
     * 目录层级 默认0(主目录)
     * */
    private Integer level;



    private List<SysUserGroup> groupList;

}
