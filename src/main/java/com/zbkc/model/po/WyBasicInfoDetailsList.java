package com.zbkc.model.po;



import lombok.Data;
import java.util.List;
import java.util.Map;


/**
 * 查看 大厦,栋，层，室 详细信息
 * @author gmding
 * @date 2021/08/20
 */
@Data
public class WyBasicInfoDetailsList {

    /**
     * 传输主体
     * */
    private WyBasicInfo info;

    /**
     * 空值面积总和
     */
    private  double emptyTotal;
    /**
     * 使用面积总和
     */
    private  double usedAreaTotal;
    /**
     * 产权面积总和
     */
    private double prorightAreaTotal;
    /**
     * 入账面积总和
     */
    private  double entryAreaTotal;
    /**
     * 使用对象
     *
     */
    private String objName;
//    /**
//     * 空置列表
//     */
//    private  List<EmptyList> emptyList;
//
//    /**
//     * 使用列表
//     */
//    private  List<UsedList> usedList;
//
//    /**
//     * 产权列表
//     */
//    private  List<ProRightList> proRightList;
//
//    /**
//     * 入账列表
//     */
//    private  List<EntryList> entryList;
//
//    /**
//     * 楼栋/楼层/单元列表
//     * */
//    private List<FloorList>floorList;

    /**
     * 楼栋数量
     */
    private  Integer num;
    /**
     * 物业状态
     * */
    private List<SysDataType> propertyStatus;
    /**
     *物业规模
     */
    private List<SysDataType> propertyScale;
    /**
     * 物业种类
     * */
    private List<SysDataType> propertyType;

    /**
     * 物业权属
     * */
    private List<SysDataType> propertyTag;
    /**
     * 物业来源
     * */
    private List<SysDataType> propertySource;
    /**
     * 主要用途
     */
    private List<SysDataType> mainPurpos;
    /**
     * 物业标签
     * */
    private List<SysDataType> wyTag;
    /**
     * 物业图片列表
     */
    private List<SysImg1>propertyImg;

    /**
     * 验收报告
     * */
    private List<SysFilePath1> acceptanceReportFile;
    /**
     * 施工许可证
     * */
    private List<SysFilePath1> constructPermitFile;

    /**
     *建造自定义附件
     */
    private  Map<String,List<SysFilePath1>> buildCustFile;
    /**
     * 土地出让合同
     * */
    private List<SysFilePath1> landTransferContractFile;

    /**
     *
     * 红线图或宗线图
     * */
    private  List<SysFilePath1> redLineFile;
    /**
     * 用地许可证
     * */
    private List<SysFilePath1> landPermitFile;

    /**
     *
     * 用地自定义附件
     * */
    private Map<String,List<SysFilePath1>>siteCustFile;

    /**
     * 户型图  sys_img字段primary_table_id
     */
    private List<SysImg1> houseImg;

    /**
     * 附属设施信息列表
     * */
    private List<WyFacilitiesInfo> wyFacilitiesInfoList;


    /**
     * 关联物业
     */
    private String  relationName;

    /**
     * 关联物业规模
     */
    private String  relationScale;

    /**
     * 下级可选物业种类
     */
    private List<SysDataType> wyTypes;

}

