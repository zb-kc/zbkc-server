package com.zbkc.model.po;

import lombok.Data;

/**
 * @author yangyan
 * @date 2021/12/15
 */
@Data
public class ProRightList {

    /**
     * 物业名称
     */
    private  String  wyName;
    /**
     * 产权证书号
     */
    private String  certificateNo;
    /**
     * 权利人
     */
    private  String obligee;
    /**
     * 建筑面积
     */
    private double buildArea;
    /**
     * 使用期限开始时间
     */
    private String useLimitStart;
    /**
     * 使用时间结束时间
     */
    private String useLimitEnd;
    /**
     * 产权登记日期
     */
    private String regTime;
}
