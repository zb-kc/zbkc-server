package com.zbkc.model.po;


import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 数据库表类
 *  @author gmding
 *  @date 2021-7-16
 * */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_user")
public class SysUserPO implements Serializable {
    /**
     * 序列号
     */
    private static final long serialVersionUID = 1L;

    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private long id;
    private String userName;
    private String nickName;
    private String pwd;
    private Integer sex;
    private Integer age;
    private Integer status;
    private Timestamp createTime;
    private String creater;
    private Timestamp updateTime;
}
