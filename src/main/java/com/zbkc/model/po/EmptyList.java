package com.zbkc.model.po;

import lombok.Data;

/**
 * @author yangyan
 * @date 2021/12/15
 */
@Data
public class EmptyList {

    /**
     * 物业名称
     */
    private String wyName;
    /**
     * 建筑面积
     */
    private double buildArea;
    /**
     * 单元状态
     */
    private String status;
    /**
     * 上一次入驻企业
     */
    private String preCompany;

    /**
     * 内三位
     */
    private String threeDimen;
}
