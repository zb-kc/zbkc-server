package com.zbkc.model.po;

import lombok.Data;

/**
 * @author yangyan
 * @date 2021/12/17
 */
@Data
public class BuildingList {
    /**
     * id
     */
    private String id;
    /**
     * 名称
     * */
    private String wyName;
    /**
     * 单元名称
     * */
    private String unitName;
    /**
     *建筑面积
     * */
    private Double buildArea;
    /**
     *已使用面积
     * */
    private Double usedArea;
    /**
     *空值面积
     * */
    private Double emptyArea;
}
