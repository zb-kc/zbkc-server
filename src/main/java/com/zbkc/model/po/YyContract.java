package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.DecimalMin;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 运营管理——合同管理
 * </p>
 *
 * @author yangyan
 * @since 2021-09-13
 */
@Data
public class YyContract implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 模板类型 1产业用房;2商业; 3住宅 ;4 南山软件园;5 租赁社会物业
     */
    private Integer templateType;

    /**
     * 合同模板类型 1产业用房租赁合同3+2（有经济贡献率）;2产业用房租赁合同3+2（无经济贡献率）;3产业用房租赁合同3年及3年以下;
     * 4商业租赁合同；5住宅租赁合同；南山软件租赁合同
     */
    private Integer type;

    /**
     * 承租对象 1承租对象为企业,2承租对象为工体商户,3承租对象为个人，4承租对象为其它
     */
    private Integer leaseType;

    /**
     * 签约类型 1，新签 2，续签 默认1
     */
    private Integer contractType;

    /**
     * 申请人类型 1，法人申请 2，委托代理人申请
     */
    private Integer applicationType;

    /**
     * 承租方
     */
    @Length(max = 50 , message = "承租方限制长度50个字符")
    private String leaser;

    /**
     * 承租方证件类型 1社会统一信用代码，2身份证，3护照
     */
    private Integer leaserType;

    /**
     * 承租方证件号码
     */
    @Length(max = 50 , message = "承租方证件号码限制长度50个字符")
    private String leaserCode;

    /**
     * 承租方联系方式
     */
    @Length(max = 20 , message = "承租方联系方式限制长度20个字符")
    private String leaserPhone;

    /**
     * 法定人代表
     */
    @Length(max = 100 , message = "法定人代表限制长度100个字符")
    private String legalPerson;

    /**
     * 法定人联系方式
     */
    @Length(max = 20 , message = "法定人联系方式限制长度20个字符")
    private String legalPhone;

    /**
     * 法定人代表证件类型  1身份证，2护照
     */
    private Integer legalType;

    /**
     * 法定人代表证件号码
     */
    @Length(max = 50 , message = "法定人代表证件号码限制长度50个字符")
    private String legalCode;

    /**
     * 法定代表人联系地址
     */
    @Length(max = 100 , message = "法定代表人联系地址限制长度255个字符")
    private String legalAddress;

    /**
     * 联系人
     */
    @Length(max = 100 , message = "联系人限制长度255个字符")
    private String contactPerson;

    /**
     * 联系电话
     */
    @Length(max =20 , message = "联系电话限制长度50个字符")
    private String contactPhone;

    /**
     * 委托代理人
     */
    @Length(max = 100 , message = "委托代理人限制长度100个字符")
    private String clientPerson;

    /**
     * 委托代理人联系方式
     */
    @Length(max = 20 , message = "委托代理人联系方式限制长度20个字符")
    private String clientPhone;

    /**
     * 委托代理人证件类型  1身份证，2护照
     */
    private Integer clientType;

    /**
     * 委托代理人证件号码
     */
    @Length(max = 50 , message = "委托代理人证件号码限制长度50个字符")
    private String clientCode;

    /**
     * 委托代理人联系地址
     */
    @Length(max = 100 , message = "委托代理人联系地址限制长度100个字符")
    private String clientAddress;

    /**
     * 企业信息备注
     */
    @Length(max = 1000 , message = "企业信息备注限制长度1000个字符")
    private String companyInfoRemark;

    /**
     * 基础信息表id  关联wy_basic_info的主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 是否经济承诺贡献 1是 2无
     */
    private Integer isEconomicCommit;

    /**
     * 经济承诺贡献
     */
    @Length(max = 1000 , message = "经济承诺贡献限制长度1000个字符")
    private String economicCommit;

    /**
     * 房屋权属情况id  关联表label_manage 字段label_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long houseOwnershipId;

    /**
     * 甲方持有房屋证明文件  关联表label_manage 字段label_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long houseProofId;

    /**
     * 房屋所有证权或不动产权证书编号
     **/
    private String certificationNumber;

    /**
     * 指导价
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal guidePrice;

    /**
     * 折扣 1:全价 2:30%  3:50% 4:70% 5:90%
     */
    private Integer discount;

    /**
     * 是否拆分租金单价 1是；2否
     */
    private Integer isSplitRent;

    /**
     * 租金单价
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal rentPrice;

    /**
     * 免租期限 0无，1十五日，2一个月，3两个月，4三个月，5四个月
     */
    private Integer freeRent;

    /**
     * 租赁期限 起租时间
     */
    private Date startRentDate;

    /**
     * 租赁期限 止租时间
     */
    private Date endRentDate;

    /**
     * 首期租金交付日期
     */
    private Date firstRentTime;

    /**
     * 首期租金交付金额
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal firstRentMoney;

    /**
     * 押金金额
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal depositAmount;

    /**
     * 房屋租赁用途
     * 1,办公 2，综合（研发）3，工业研发 4，研发
     */
    private Integer leaseUse;

    /**
     * 水费
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal waterFee;

    /**
     * 电费
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal electricFee;

    /**
     * 燃气费
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal gasFee;

    /**
     * 物业管理费
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal propertyManagementFee;

    /**
     * 其他费
     */
    @DecimalMin(value = "0" , message = "")
    private BigDecimal otherFee;

    /**
     * 备注
     */
    @Length(max = 1000 , message = "备注限制长度1000个字符")
    private String remark;

    /**
     * 入驻通知书 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long residentNoticeFilePathId;


    /**
     * 营业执照复印件 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long businessLicenseFilePathId;

    /**
     * 法人身份证复印件 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long legalPersonFilePathId;


    /**
     * 法人代表证明书 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long legalRepresentativeFilePathId;

    /**
     * 股东证明书 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long shareholderFilePathId;

    /**
     * 法人授权委托书 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long legalPersonPowerFilePathId;

    /**
     * 经办人身份证复印件 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long handlingPersonCardFilePathId;

    /**
     * 南山区产业用房建设和管理工作领导小组会议纪要 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long meetingMinutesFilePathId;

    /**
     * 房屋建筑面积总表，房屋建筑面积分户汇总表及房屋建筑面积分户位置图 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long buildingAreaFilePathId;

    /**
     * 有经济贡献率的企业的经济贡献承诺书 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long commitmentFilePathId;

    /**
     * 南山区政策性产业用房入驻通知书 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long settlementNoticeFilePathId;


    /**
     * 会议纪要 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long meetingFilePathId;

    /**
     * 租金价格评估报告 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long rentReportFilePathId;

    /**
     * 身份证复印件 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long personFilePathId;


    /**
     * 南山数字文化产业基地入驻通知书 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long digitalNoticeFilePathId;

    /**
     * 合同 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long contractFilePathId;

    /**
     * 自定义附件 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long custRightFileId;

    /**
     * 用户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    private Timestamp createTime;

    private Timestamp updateTime;

    /**
     * 附件材料审核状态
     */
    @TableField(exist = false)
    private List<YwHandoverDetailsPO> ywHandoverDetailsDTOList;
//
//    /**
//     * 合同附件信息
//     */
//    @TableField(exist = false)
//    private SysFilePath1 contactSysFilePath;
//
//    /**
//     * 待办业务申请记录
//     */
//    @TableField(exist = false)
//    private YwAgencyBusinessPO ywAgencyBusinessPO;

    /**
     * 物业编码
     */
    @TableField(exist = false)
    private String houseCode;

    /**
     * 月租金
     */
    @TableField(exist = false)
    private BigDecimal monthlyRent;

    /**
     * 租赁面积
     */
    @TableField(exist = false)
    private BigDecimal rentalArea;

    /**
     *  续签通知书 sys_file_path字段db_file_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long renewalSignFilePathId;

    private String tableId;

    /**
     * 企业营业执照地址
     */
    @Length(max = 255 , message = "营业执照地址限制长度255个字符")
    private String businessLicenseAddr;

    /*
    多个物业id
     */
    @TableField(exist = false)
    private String wyBasicInfoIdStr;

    /**
     * 月租金
     */
    private String zlMonthlyRent;
}
