package com.zbkc.model.po;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.hibernate.validator.constraints.Length;


/**
 * <p>
 * 物业基本(主体)信息表
 * </p>
 *
 * @author yangyan
 * @since 2021-08-09
 */
@Data
public class WyBasicInfo implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * unit_info表主键
     */
    private String tableId;

    /**
     * 父级id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long pid;

    /**
     * 资产类型:  产业用房；孵化园区；办公用房；公共服务设施；商业；住宅；租赁社会物业
     */
    @Length(max = 20, message = "资产类型限制长度20个字符")
    private String assetType;

    /**
     * 物业种类:  产业用房；商业；公共配套；办公用房；公共配套；基本办公用房；服务用房；设备用房；附属用房；教育；社区服务；市政共用；文体；医疗卫生
     *
     */
    @Length(max = 20, message = "物业种类限制长度20个字符")
    private String propertyType;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 物业种类型 类型 1园区 2楼栋 3楼层 4单元 5室 6零散 7楼栋(主体)
     */
    private Integer type;

    /**
     * 归属所有人
     */
    @Length(max = 20 , message = "归属所有人限制长度20个字符")
    private String belongPerson;

    /**
     * 物业名称
     */
    @Length(max = 100 , message = "物业名称限制长度100个字符")
    private String name;

    /**
     * 楼栋，楼层，单元、室名称
     */
    @Length(max = 100 , message = "单元、室名称限制长度100个字符")
    private String unitName;

    /**
     * 简称
     */
    @Length(max = 20 , message = "简称限制长度20个字符")
    private String shortName;

    /**
     * 别名/曾用名
     *
     * */
    @Length(max = 20 , message = "单元、室名称限制长度20个字符")
    private String aliasName;

    /**
     * 物业标签  关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyTagId;

    /**
     * 物业规模 关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyScaleId;

    /**
     * 物业状态id 关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyStatusId;

    /**
     * 物业种类 标签id 关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private  Long propertyTypeId;

    /***
     * 物业来源 id 关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private  Long propertySourceId;

    /**
     * 主要用途id 关联表label_manage 字段label_id  （多余字段）
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long mainPurposId;

    /**
     * 物业标签id（政府）关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyTagId;

    /**
     * 物业规划（政府）
     */

    @Length(max = 50 , message = "物业规划限制长50个字符")
    private String propertyPlan;

    /**
     * 物业编号
     */
    @Length(max = 50 , message = "物业编号限制长50个字符")
    private String propertyCode;

    /**
     * 物业简介
     * */
    private String propertyIntroduction;

    /**
     * 地址
     */
    @Length(max = 100 , message = "地址限制长100个字符")
    private String address;

    /**
     * 详细地址
     */
    @Length(max = 200 , message = "详细地址限制长200个字符")
    private String detailAddr;

    /**
     * 社区编码
     *
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long areaCode;

    /**
     * 用地面积
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Double landArea;

    /**
     * 建筑面积
     */
    private Double buildArea;

    /**
     * 实用面积
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Double practicalArea;

    /**
     * 公摊面积
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Double shareArea;

    /**
     * 物业图片
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long propertyImgId;

    /**
     * 运营方式  1，自营  2.委托运营  3.其他管理单位
     */
    private Integer operatesType;

    /**
     * 委托运营管理单位
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long operatesUnitId;

    /**
     * 运营单位项目组
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long opreatesGroupId;

    /**
     * 其他管理单位
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long otherUnitId;

    /**
     * 设计单位
     */
    @Length(max = 50 , message = "设计单位限制长50个字符")
    private String designUnit;

    /**
     * 施工单位
     */
    @Length(max = 50 , message = "施工单位限制长50个字符")
    private String buildUnit;

    /**
     * 建造说明
     */
    @Length(max = 500 , message = "建造说明限制长500个字符")
    private String buildDescription;

    /**
     * 建造备注
     */
    @Length(max = 500 , message = "建造备注限制长500个字符")
    private String buildRemark;

    /**
     * 验收报告 sys_file_path字段db_file_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long acceptanceReportFileId;

    /**
     * 施工许可证 sys_file_path字段db_file_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long constructPermitFileId;

    /**
     *
     * 建造自定义附件 sys_file_path字段db_file_id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long buildCustFileId;


    /**
     * 用地开始时间
     */
    private Date landStartTime;

    /**
     * 用地结束时间
     */
    private Date landEndTime;

    /**
     * 用地开始时间
     * */
    @TableField(exist = false)
    private String landStartTimeStr;

    /**
     * 用地结束时间
     * */
    @TableField(exist = false)
    private String landEndTimeStr;

    /**
     * 建设单位
     */
    @Length(max = 50 , message = "建设单位限制长50个字符")
    private String constructionUnit;

    /**
     * 用地说明
     */
    @Length(max = 500 , message = "用地说明限制长500个字符")
    private String landDescription;

    /**
     * 用地备注
     */
    @Length(max = 500 , message = "用地说明限制长500个字符")
    private String landRemark;

    /**
     * 土地出让合同 sys_file_path字段db_file_id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long landTransferContractFileId;

    /**
     *
     * 红线图或宗线图 sys_file_path字段db_file_id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long redLineFileId;

    /**
     * 用地许可证 sys_file_path字段db_file_id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long landPermitFileId;

    /**
     *
     * 用地自定义附件 sys_file_path字段db_file_id
     * */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long siteCustFileId;

    /**
     * 物业公司名称
     */
    @Length(max = 50 , message = "物业公司名称限制长50个字符")
    private String companyName;

    /**
     * 联系人
     */
    @Length(max = 50 , message = "物业公司名称限制长50个字符")
    private String contacts;

    /**
     * 联系电话
     */
    @Length(max = 50 , message = "物业公司名称限制长50个字符")
    private String phone;

    /**
     * 物业管理费
     */
    private BigDecimal manageFee;

    /**
     * 物管信息备注
     */
    @Length(max = 50 , message = "物业公司名称限制长50个字符")
    private String remark;

    /**
     * 物业-租赁用途id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long zlLeaseUseId;

    /**
     * 物业-租赁面积
     */
    private Double zlRentalArea;

    /**
     *物业_租赁起始时间
     */
    private Date zlStartRentDate;

    /**
     * 物业_租赁截止时间
     */
    private Date zlEndRentDate;

    /**
     * 物业_月租金
     */
    private BigDecimal zlMonthlyRent;

    /**
     * 物业_租赁单价
     */
    private BigDecimal zlRentUnitPrice;

    /**
     * 物业_出租方
     */
    @Length(max = 50 , message = "物业_出租方名称限制长50个字符")
    private String lessor;

    /**
     * 物业_出租方联系方式
     */
    @Length(max = 20 , message = "物业_出租方联系方式名称限制长20个字符")
    private String lessorPhone;

    /**
     * 房屋编码
     */
    @Length(max = 200 , message = "房屋编码名称限制长200个字符")
    private String houseCode;

    /**
     * 数量 楼栋/楼层
     *
     * */
    private int num;

    /**
     * 层高
     * */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer storeyHeight;

    /**
     * 承重
     * */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Double loadBearer;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 维度
     */
    private BigDecimal latitude;

    /**
     * 是否删除 1，未删除 2，已删除
     */
    private Integer isDel;

    /**
     * 创建时间
     */
    private Timestamp createTime;


    /**
     * 修改时间
     */
    private Timestamp updateTime;

    /**
     * 朝向 1.东2.南3.西4.北
     */
    private Integer towards;

    /**
     * 户型图  sys_img字段primary_table_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long houseImgId;

    /**
     * 内三维
     */
    @Length(max = 100 , message = "内三维名称限制长100个字符")
    private String threeDimen;

    /**
     * 套内建筑面积
     */
    @TableField(exist = false)
    private Double insideArea;
}
