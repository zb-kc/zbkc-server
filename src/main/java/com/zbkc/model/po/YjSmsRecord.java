package com.zbkc.model.po;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 预警提醒——合同到期提醒_短信记录表
 * </p>
 *
 * @author yangyan
 * @since 2021-09-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class YjSmsRecord implements Serializable {

    private static final long serialVersionUID=1L;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 物业表id 
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 合同主表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long contractId;


    /**
     * 短信模板类型
     */

    private Integer msgType;



    /**
     * 发送状态  1:失败 ；2成功
     */
    private Boolean sendStatus;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

    /**
     * 发送时间
     */
    private Timestamp sendTime;


}
