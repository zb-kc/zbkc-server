package com.zbkc.model.po;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 政府 物业资产信息表
 * </p>
 *
 * @author gmding
 * @since 2021-08-18
 */
@Data
public class ZfWyAssetInfo implements Serializable {
    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    private String id;

}
