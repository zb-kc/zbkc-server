package com.zbkc.model.po;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统参数配置表
 * </p>
 *
 * @author yangyan
 * @since 2021-08-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysParamConfig implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 参数配置类型(1:系统参数, 2:业务参数)
     */
    private Integer type;

    /**
     * 名称
     */
    private String name;

    /**
     * 参数编号
     */
    private String code;

    /**
     * 参数值
     */
    private String value;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态(1:有效, 2:无效)
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 创建人
     */
    private String creater;

    /**
     * 修改时间
     */
    private Timestamp updateTime;


}
