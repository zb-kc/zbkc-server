package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * <p>
 * 系统角色表
 * </p>
 *
 * @author xiongshibao
 * @since 2021-11-2
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysUserRole implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 系统角色ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * id
     * user_id
     * role_id
     * status
     * create_time
     * creater
     */
    /**
     * 角色名称
     */
    private Long userId;
    private Long roleId;
    /**
     * 状态(1:有效, 2:无效)
     */
    private Integer status;
    private Date createTime;

    /**
     * 创建人
     */
    private String creater;

}
