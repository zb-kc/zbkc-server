package com.zbkc.model.po;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 短信模板管理表
 * </p>
 *
 * @author yangyan
 * @since 2021-09-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WySysMsg implements Serializable {

    private static final long serialVersionUID=1L;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 短信名称
     */
    private String name;

    /**
     * 短信类别  1短信到期模板 2短信催缴模板
     */
    private Integer type;

    /**
     * 短信状态 1启用 2禁用
     */
    private Integer status;

    /**
     * 短信内容
     */
    private String word;

    /**
     * 备注
     */
    private String remark;

    private String createTime;

    private String updateTime;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;


}
