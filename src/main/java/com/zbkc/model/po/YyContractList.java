package com.zbkc.model.po;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;


/**
 * 查询返回实体类
 * @author caobiyang
 * @date 2021/09/14
 */
@Data
public class YyContractList {

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 承租方
     */
    private String leaser;


    /**
     * 合同类型 模板类型 1产业用房;2商业; 3住宅 ;4 南山软件园;5 租赁社会物业
     */
    private String templateType;

    /**
     * 合同模板类型 1产业用房租赁合同3+2（有经济贡献率）;2产业用房租赁合同3+2（无经济贡献率）;3产业用房租赁合同3年及3年以下; 4商业租赁合同；5住宅租赁合同；6南山软件租赁合同
     */
    private String type;

    /**
     * 租赁物业地址
     */
    private String wyAddress;

    /**
     * 租赁用途中文（方便导出）
     */
    private String leaseUsing;

    /**
     * 租赁用途
     */
    private Integer leaseUse;

    /**
     * 月租金
     */
    private BigDecimal monthlyRent;

    /**
     * 租赁期限
     */
    private Integer leaseTerm;

    /**
     * 起租时间
     */
    private String startRentDate;

    /**
     * 止租时间
     */
    private String endRentDate;

    /**
     * 联系人
     */
    private String contactPerson;

    /**
     * 联系电话
     */
    private String contactPhone;

    /**
     * 租赁单价
     */
    private BigDecimal rentPrice;

    /**
     * 免租期限
     */
    private Integer freeRent;

    /**
     * 是否免租
     */
    private String isFree;


}
