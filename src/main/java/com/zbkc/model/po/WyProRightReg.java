package com.zbkc.model.po;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 产权登记表
 * </p>
 *
 * @author yangyan
 * @since 2021-08-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WyProRightReg implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 物业单元编号
     */
    private String unitCode;

    /**
     * 资产名称id  基础信息表
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 产权证书号
     */
    private String certificateNo;

    /**
     * 不动产单元号
     */
    private String estateUnitNo;

    /**
     * 竣工日期
     */
    private Date completionDate;

    /**
     * 权利人
     */
    private String obligee;

    /**
     * 权利类型   关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long rightTypeId;

    /**
     * 登记价
     */
    private BigDecimal registerPrice;

    /**
     * 产权证登记日期
     */
    private Date regTime;

    /**
     * 土地位置
     */
    private String landLocation;

    /**
     * 社区编码
     *
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long areaCode;

    /**
     * 详细地址
     */
    private String detailAddr;

    /**
     * 建筑面积
     */
    private Double buildingArea;

    /**
     * 套内面积
     */
    private Double insideArea;

    /**
     * 土地用途 关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long landPurposeId;

    /**
     * 登记用途
     */
    private String registerPurpose;

    /**
     * 共有情况  关联表label_manage 字段label_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long commonSituationId;

    /**
     * 使用期限 开始时间
     */
    private Date useLimitStart;

    /**
     * 使用期限 结束时间
     */
    private Date useLimitEnd;

    /**
     * 产权证书 sys_file_path字段db_file_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long proRightFileId;

    /**
     * 自定义证书 sys_file_path字段db_file_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long custRightFileId;

    /**
     * 备注
     */
    private String remark;

    /**
     *
     * 状态(1:有效, 2:无效)
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Timestamp createTime;


    /**
     * 修改时间
     */
    private Timestamp updateTime;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;

}
