package com.zbkc.model.po;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @author yangyan
 * @date 2021/09/11
 */
@Data
public class WySysMsgHomeList {
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 短信名称
     */
    private String name;

    /**
     * 短信类别  1短信到期模板 2短信催缴模板
     */
    private Integer type;

    /**
     * 短信状态 1启用 2禁用
     */
    private Integer status;
    private String remark;
    private String word;

    private String createTime;

    /**
     * 用于导出状态字符串
     */
    private String statusStr;
}
