package com.zbkc.model.po;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 *
 * @author gmding
 * @since 2021-09-11
 */
@Data
public class WyAssetHomeInfo implements Serializable {
    private static final long serialVersionUID=1L;

    /**
     * 产业用房数量
     * */
    private int cyNum;
    /**
     * 办公用房数量
     * */
    private int bgNum;
    /**
     * 公共服务设施数量
     * */
    private int ggNum;
    /**
     * 商业数量
     * */
    private int syNum;
    /**
     * 住宅数量
     * */
    private int zzNum;

    /**
     * 租赁社会物业
     * */
    private int zlNum;
    /**
     * 产权办理
     * */
    private int certHasNum;

    /**
     * 产业用房面积
     * */
    private BigDecimal cyArea;
    /**
     * 办公用房面积
     * */
    private BigDecimal bgArea;
    /**
     * 公共服务设施面积
     * */
    private BigDecimal ggArea;
    /**
     * 商业面积
     * */
    private BigDecimal syArea;
    /**
     * 住宅面积
     * */
    private BigDecimal zzArea;
    /**
     * 实用面积
     * */
    private BigDecimal actArea;
    /**
     * 建筑面积
     * */
    private BigDecimal buildArea;

    /**
     * 公摊面积
     * */
    private BigDecimal pubArea;
    /**
     * 扶持企业数量
     * */
    private int supportNum;

    /**
     * 平均租金
     * */
    private BigDecimal averageRent;

    /**
     * 空置面积
     * */
    private BigDecimal kzArea;

    /**
     * 出租面积
     * */
    private BigDecimal czArea;

    /**
     * 占地面积
     * */
    private BigDecimal zdArea;

    /**
     * 资产入账
     * */
    private int rzNum;
    /**
     * 国企使用面积
     * */
    private Double gqUserArea;
    /**
     * 国企建筑面积
     * */
    private Double gqBuildArea;
    /**
     * 空置数量
     * */
    private int kzNum;

    /**
     * 街道列表
     * */
    private List<WyAssetStreet>streetList;

    /**
     * 资产未入账
     * */
    private int notRzNum;

    /**
     * 产权办理
     * */
    private int notCertHasNum;

    //出租租期长(不含产业用房)
    private int longContractNotCY;

    //合同预到期(不含产业用房)
    private int delayContractNotCY;

    //空置未使用(不含产业用房)
    private int emptyNumberNotCY;

    //总资产数
    private int totalWyNumber;

    //扶持企业数量
    private int fcCompanyNumber;
}
