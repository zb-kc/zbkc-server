package com.zbkc.model.po;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author yangyan
 * @date 2021/12/15
 */
@Data
public class EntryList {

    /**
     * 物业名称
     */
    private String wyName;
    /**
     * 入账类型
     */
    private String entryType;
    /**
     * 入账金额
     */
    private BigDecimal entryMoney;
    /**
     * 入账时间
     */
    private String entryTime;
    /**
     * 入账面积
     */
    private Double entryArea;
}
