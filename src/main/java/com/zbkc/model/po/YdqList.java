package com.zbkc.model.po;

import lombok.Data;

@Data
public class YdqList {

    private String id;
    /**
     * 物业名称
     * */
    private String name;
    /**
     * 企业编码
     * */
    private String code;
    /**
     *建筑面积
     * */
    private double buildArea;
    /**
     * 剩余时间 天
     * */
    private int syNum;

    /**
     * 单价
     * */
    private int price;
    /**
     * 详细地址
     * */
    private String address;
    /**
     * 管理单位
     * */
    private String manUnit;
    //起租
    private String rentDateStart;
    //始租
    private String rentDateEnd;

    //街道名称
    private String streetName;


}
