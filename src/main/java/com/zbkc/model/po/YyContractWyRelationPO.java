package com.zbkc.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @author ZB3436.xiongshibao
 * @date 2021-10-26
 */
@Data
@TableName("yy_contract_wy_relation")
public class YyContractWyRelationPO {
    @JsonSerialize(using= ToStringSerializer.class)
    @TableId(value = "id",type = IdType.ASSIGN_ID)
    private Long id;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long yyContractId;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    @JsonSerialize(using= ToStringSerializer.class)
    private Long ywBusinessId;

}