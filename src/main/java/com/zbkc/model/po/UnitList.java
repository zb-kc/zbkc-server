package com.zbkc.model.po;

import lombok.Data;

/**
 * @author yangyan
 * @date 2021/12/17
 */
@Data
public class UnitList {

    /**
     * id
     */
    private String id;
    /**
     * 名称
     * */
    private String wyName;
    /**
     * 单元名称
     * */
    private String unitName;
    /**
     *建筑面积
     * */
    private Double buildArea;

    /**
     * 单元状态
     */
    private String status;
    /**
     * 入驻公司
     */
    private String company;
    /**
     * 上一次入驻公司
     */
    private String preCompany;

    /**
     * 内三位
     */
    private String threeDimen;
}
