package com.zbkc.model.po;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 资产处置信息表
 * </p>
 *
 * @author yangyan
 * @since 2021-09-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WyAssetDisposal implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 资产名称id  基础信息表
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long wyBasicInfoId;

    /**
     * 批准文件号
     */
    private String documentCode;

    /**
     * 批准单位
     */
    private String authority;

    /**
     * 批准时间
     */
    private Date approvalTime;

    /**
     * 处置单位
     */
    private String disposalUnit;

    /**
     * 处置时间
     */
    private Date disposalTime;

    /**
     * 处置方式
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long solvingMethodId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 自定义附件id sys_file_path字段db_file_id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long custRightFileId;

    /**
     *
     * 状态(1:有效, 2:无效)
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    /**
     * 修改时间
     */
    private Timestamp updateTime;

    /**
     * 用户id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long userId;


}
