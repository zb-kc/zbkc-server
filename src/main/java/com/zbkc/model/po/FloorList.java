package com.zbkc.model.po;

import lombok.Data;

/**
 *
 * @author gmding
 * @since 2021-08-20
 */
@Data
public class FloorList {


    /**
     * id
     */
    private String id;
    /**
     * 名称
     * */
    private String wyName;
    /**
     * 单元名称
     * */
    private String unitName;
    /**
     *建筑面积
     * */
    private Double buildArea;
    /**
     *已使用面积
     * */
    private Double usedArea;
    /**
     *空值面积
     * */
    private Double emptyArea;

    /**
     *层高
     * */
    private int storeyHeight;
    /**
     *承重
     * */
    private Double loadBearer;

    /**
     * 内三位
     */
    private String threeDimen;

}
