package com.zbkc.model.po;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.zbkc.configure.BasePathPatterns;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * <p>
 * 运营管理——使用登记/委托运营——自定义附件表
 * </p>
 *
 * @author yangyan
 * @since 2021-09-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class YyUseRegFilePath1 implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 主键id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 附件主表id
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long yyId;

    /**
     * 自定义名称
     */
    private String name;

    /**
     * 文件路径
     */
    private String path;

    /**
     * 原文件名称
     */
    private String fileName;

    /**
     * 创建时间
     */
    private Timestamp createTime;

    public String getPath() {
        return BasePathPatterns.urlPath+path;
    }


}
