package com.zbkc.model.po;

import lombok.Data;

/**
 * @author yangyan
 * @date 2021/12/15
 */
@Data
public class UsedList {

    /**
     * 物业名称
     */
    private String wyName;
    /**
     * 建筑面积
     */
    private double buildArea;
    /**
     * 使用对象/单位名称
     */
    private String objName;
    /**
     * 使用开始时间
     */
    private String startDate;
    /**
     * 使用截止时间
     */
    private String endDate;
    /**
     * 使用年限
     */
    private Integer year;
    /**
     * 剩余时间
     */
    private Integer restYear;

}
