package com.zbkc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SendMailTest {
    @Autowired
    private JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String senderFrom;

    @Test
    public void test() {
        SimpleMailMessage message = new SimpleMailMessage();
        // 发送人邮箱
        message.setFrom(senderFrom);
        // 收件人邮箱，多个以逗号分隔
        message.setTo("1649556561@qq.com");
        // 邮件标题
        message.setSubject("SpringBoot整合Mail发送简单邮件");
        // 邮件内容
        message.setText("测试邮件...请勿回复！");
        // 发送
        mailSender.send(message);
    }


}
