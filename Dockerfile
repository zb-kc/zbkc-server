FROM openjdk:8u121-jdk-alpine
MAINTAINER Joe <android_li@sina.cn>

ENV JAR_FILE zbkc-server-1.0.0-SNAPSHOT.jar

#Set Beijing time zone
RUN echo 'Asia/Shanghai' >/etc/timezone 

ADD target/$JAR_FILE /app/
CMD java -Xmx200m -jar /app/$JAR_FILE

EXPOSE 10005